digraph G {

  subgraph cluster_0 {
    style=filled;
    //color=purple;
    node [color=red];
    init -> warningsetperiod [label="period==0"];
    warningsetperiod -> no_condition [label="true"];
    init -> no_condition [label="period!=0"];
    label = "initializer";
  }

  subgraph cluster_1 {
    style=filled;
    //color=blue;
    node [color=blue];
    given_inmethod_reproduction_satisfied -> given_when_attribute_inmethod_reproduction_satisfied [label="Given_attribute_condition==true&&\nWhen_attribute_condition==true&&\nWhen_re_scen==true&&\nWhen_method==true"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_when_satisfied [label="wait(When_wait_time)"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_inmethod_reproduction_satisfied [label="Given_attribute_condition==false || When_attribute_condition==false"];
    given_when_satisfied -> error [label="timeout(2*period)"];
    
    label = "Given_When_process";
    //color=blue
  }
  
  subgraph cluster_2 {
    //style=filled;
    node [];
    ok_attibute;
    label = "Then_process_attribute";
    color=blue
  }
  
  subgraph cluster_4 {
    //style=filled;
    node [];
    ok_sendmethod;
    label = "Then_process_send_method";
    color=blue
  }
  
  subgraph cluster_5 {
    //style=filled;
    node [];
    ok_sendmethod_onchange;
    Wait_for_change[label="Wait_for_change\nduration: pre_value = value"];
    label = "then_process_send_method_onchange";
    color=blue
  }
  
  subgraph cluster_6 {
    //style=filled;
    node [];
    ok_sendmethod_periodically;
    label = "then_process_send_method_periodically";
    color=blue
  }
  
  subgraph cluster_7 {
    //style=filled;
    node [color=yallow];
    ok_sendmethod_timewindow;
    wait_for_values;
    label = "then_process_send_method_timewindow";
    color=blue
  }
  
  subgraph cluster_8 {
    //style=filled;
    node [];
    ok_send_no_method;
    label = "then_process_send_no_method";
    color=blue
  }
  
  subgraph cluster_3 {
    //style=filled;
    //color = gray;
    node [style=filled,color=lightgray];
    given_events->
    given_attributes_cluster->
    when_attributes_cluster->
    given_inmethod_reproduction_cluster->
    when_inmethod_reproduction_cluster->
    given_wait_cluster->
    when_wait_cluster;
    when_events -> given_attributes_cluster;
    label = "clustering";
  }
  
  //start -> init;
  error -> no_condition [label="true"];
  
  ok_attibute -> no_condition [label="true"];
  given_when_satisfied -> ok_attibute [label="Then_attribute_condition"];
  
  ok_sendmethod -> no_condition [label="true"];
  given_when_satisfied -> ok_sendmethod [label="Then_method==true"];
  
  ok_sendmethod_onchange -> given_when_satisfied [label="true"];
  Wait_for_change -> ok_sendmethod_onchange [label="pre_value!=value && Then_method==true"];
  Wait_for_change -> error [label="timeout(2*period)"];
  given_when_satisfied -> Wait_for_change [label="true"];
  
  ok_sendmethod_periodically -> given_when_satisfied [label="wait(period)"];
  given_when_satisfied -> ok_sendmethod_periodically [label="Then_method==true"];
  
  ok_sendmethod_timewindow -> given_when_satisfied [label="true"];
  wait_for_values -> ok_sendmethod_timewindow [label="Then_method==true"];
  wait_for_values -> error [label="timeout(2*period)"]
  given_when_satisfied -> wait_for_values [label="Then_timewindow_condittion==true"];
  
  ok_send_no_method -> no_condition [label="true"];
  given_when_satisfied -> ok [label="Then_method==false"];
  
  no_condition -> given_inmethod_reproduction_satisfied [label="Given_attribute_condition==true&&\nGiven_method==true&&\nGiven_re_scenario==true&&\nwait(Given_wait_time)"];
  given_inmethod_reproduction_satisfied -> no_condition [label="Given_attribute_condition==false"];
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
