digraph G {
  
  subgraph cluster_2 {
    //style=filled;
    node [color=yallow];
    ok;
    label = "then_process";
    color=blue
  }
  
  //start -> init;
  ok -> no_condition [label="true"];
  error -> no_condition [label="true"];
  given_when_satisfied -> ok [label="Then_method==false"];
  given_when_satisfied -> error [label="timeout(2*period)"];
  //no_condition -> given_inmethod_reproduction_satisfied [label="a==1 && re_scen1==1 && setEntry1==1"];
  //given_inmethod_reproduction_satisfied -> no_condition [label="a!=1"];
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
