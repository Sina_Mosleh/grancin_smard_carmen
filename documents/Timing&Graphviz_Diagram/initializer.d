digraph G {

  subgraph cluster_0 {
    style=filled;
    //color=purple;
    node [color=red];
    init -> warning_need_to_set_period [label="period==0"];
    warning_need_to_set_period -> no_condition [label="true"];
    init -> no_condition [label="period!=0"];
    label = "initializer";
  }
  
  //start -> init;
  //no_condition -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
