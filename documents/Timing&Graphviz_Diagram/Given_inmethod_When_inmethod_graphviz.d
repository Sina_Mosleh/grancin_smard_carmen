digraph G {

  subgraph cluster_1 {
    style=filled;
    //color=blue;
    node [color=blue];
    //given_inmethod_reproduction_satisfied -> given_when_attribute_inmethod_reproduction_satisfied [label="wait(5ms) && a==0 && b==0 && re_scen2==1 && setEntry2==1"];
    given_inmethod_satisfied -> when_inmethod_satisfied [label="When_inmethod == true"];
    when_inmethod_satisfied -> given_when_satisfied [label="true"];
    //given_when_attribute_inmethod_reproduction_satisfied -> given_inmethod_reproduction_satisfied [label="a!=0 || b!=0"];
    given_when_satisfied -> error [label="timeout(2*period)"];
    
    label = "given_when_process";
    //color=blue
  }
  
  subgraph cluster_3 {
    //style=filled;
    //color = gray;
    node [style=filled,color=lightgray];
    given_events->
    given_inmethod_cluster->
    when_inmethod_cluster;
    when_events -> given_inmethod_cluster;
    label = "clustering";
  }
  
  no_condition -> given_inmethod_satisfied [label="Given_inmethod==true"];
  //start -> init;
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}