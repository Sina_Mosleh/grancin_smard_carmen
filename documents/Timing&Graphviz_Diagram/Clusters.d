digraph G {
  
  subgraph cluster_3 {
    //style=filled;
    //color = gray;
    node [style=filled,color=lightgray];
    given_events->
    given_attributes_cluster->
    when_attributes_cluster->
    given_inmethod_reproduction_cluster->
    when_inmethod_reproduction_cluster->
    given_wait_cluster->
    when_wait_cluster;
    when_events -> given_attributes_cluster;
    label = "clustering";
  }
  
  //start -> init;
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
