digraph G {
  
  subgraph cluster_2 {
    //style=filled;
    node [color=yallow];
    ok;
    wait_for_values;
    label = "then_process";
    color=blue
  }
  
  //start -> init;
  //ok -> no_condition [label="true"];
  error -> no_condition [label="true"];
  ok -> given_when_satisfied [label="true"];
  wait_for_values -> ok [label="Then_method==true"];
  given_when_satisfied -> error [label="timeout(2*period)"];
  wait_for_values -> error [label="timeout(2*period)"]
  given_when_satisfied -> wait_for_values [label="Then_timewindow_condittion==true"];
  //no_condition -> given_inmethod_reproduction_satisfied [label="a==1 && re_scen1==1 && setEntry1==1"];
  //given_inmethod_reproduction_satisfied -> no_condition [label="a!=1"];
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
