digraph G {

  subgraph cluster_1 {
    style=filled;
    //color=blue;
    node [color=blue];
    given_inmethod_reproduction_satisfied -> given_when_attribute_inmethod_reproduction_satisfied [label="Given_attribute_condition==true&&\nWhen_attribute_condition==true&&\nWhen_re_scen==true&&\nWhen_method==true"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_when_satisfied [label="wait(When_wait_time)"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_inmethod_reproduction_satisfied [label="Given_attribute_condition==false || When_attribute_condition==false"];
    given_when_satisfied -> error [label="timeout(2*period)"];
    
    label = "Given_When_process";
    //color=blue
  }
  
  //start -> init;
  error -> no_condition [label="true"];
  
  no_condition -> given_inmethod_reproduction_satisfied [label="Given_attribute_condition==true&&\nGiven_method==true&&\nGiven_re_scenario==true&&\nwait(Given_wait_time)"];
  given_inmethod_reproduction_satisfied -> no_condition [label="Given_attribute_condition==false"];
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
