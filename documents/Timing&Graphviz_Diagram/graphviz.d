digraph G {

  subgraph cluster_0 {
    style=filled;
    //color=purple;
    node [color=red];
    init -> warningsetperiod [label="period==0"];
    warningsetperiod -> no_condition [label="true"];
    init -> no_condition [label="period!=0"];
    label = "initializer";
  }

  subgraph cluster_1 {
    style=filled;
    //color=blue;
    node [color=blue];
    given_inmethod_reproduction_satisfied -> given_when_attribute_inmethod_reproduction_satisfied [label="wait(5ms) && a==0 && b==0 && re_scen2==1 && setEntry2==1"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_when_satisfied [label="wait(10ms)"];
    given_when_attribute_inmethod_reproduction_satisfied -> given_inmethod_reproduction_satisfied [label="a!=0 || b!=0"];
    given_when_satisfied -> error [label="timeout(2*period)"];
    
    label = "given_when_process";
    //color=blue
  }
  
  subgraph cluster_2 {
    //style=filled;
    node [color=yallow];
    ok;
    label = "then_process";
    color=blue
  }
  
  subgraph cluster_3 {
    //style=filled;
    //color = gray;
    node [style=filled,color=lightgray];
    given_events->
    given_attributes_cluster->
    when_attributes_cluster->
    given_inmethod_reproduction_cluster->
    when_inmethod_reproduction_cluster->
    given_wait_cluster->
    when_wait_cluster;
    when_events -> given_attributes_cluster;
    label = "clustering";
  }
  
  //start -> init;
  ok -> no_condition [label="true"];
  error -> no_condition [label="true"];
  given_when_satisfied -> ok [label="c==1"];
  no_condition -> given_inmethod_reproduction_satisfied [label="a==1 && re_scen1==1 && setEntry1==1"];
  given_inmethod_reproduction_satisfied -> no_condition [label="a!=1"];
  //error -> end;

  //start [shape=Mdiamond];
  //end [shape=Msquare];
}
