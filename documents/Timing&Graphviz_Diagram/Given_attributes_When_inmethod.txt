@startuml
Title Given step is checking attribute and When step is checking in method
robust "Clock" as CK
robust "Given Step Attribute" as GS
robust "When Step Attribute" as WS
robust "Then Step" as TS

@CK
0 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1
+1 is 0
+1 is 1

@GS
0 is 0
10 is 1
20 is 0

@WS
0 is 0
15 is 1
16 is 0

@TS
0 is 0
15 is 1
20 is 0
TS@15 <-> @20

@15
WS -> TS : activated due to when

@20
GS -> TS : deactivated due to given
@enduml