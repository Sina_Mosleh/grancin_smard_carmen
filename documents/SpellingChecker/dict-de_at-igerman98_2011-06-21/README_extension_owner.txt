COMMENT OF THE EXTENSION OWNER
========================================================================================

This extension is based on:

Spell checking:
===============
de-AT_igerman98 
Version: 2011-06-09 (build 2011-06-21)
Author:  Bj�rn Jacke <bjoern@j3e.de>
Licence: GPL v2 or GPL v3 and OASIS
http://www.j3e.de/ispell/igerman98/

Hyphenation:
============
Authors: Marco Huggenberger <marco@by-night.ch> / Daniel Naber <naber at danielnaber de>
Version: 2011-06-03 (bug in hyphenation modul and license problems in Readme file fixed)
License: GNU LGPL v2 or later

Thesaurus:
==========
OpenThesaurus - Deutscher Thesaurus - Synonyme und Assoziationen
Version: 2011-05-04
License: GNU LGPL v2.1 or later
http://www.openthesaurus.de

Please note:
Even though having built and published the dictionary extension, the extension owner has
no responsibility for maintaining related dictionaries. For contacting the author of 
dictionaries, please read his related README files. Updated extension is intended to be 
published, as soon related dictionaries will have been actualized.

For contacting the extension owner write to:
karl<dot>zeiler<at>t-online.de
