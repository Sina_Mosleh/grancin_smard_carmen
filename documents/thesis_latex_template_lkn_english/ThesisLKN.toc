\babel@toc {USenglish}{}
\babel@toc {german}{}
\babel@toc {USenglish}{}
\contentsline {chapter}{Contents}{7}%
\contentsline {chapter}{List of Figures}{9}%
\contentsline {chapter}{List of Tables}{12}%
\contentsline {chapter}{\numberline {1}Introduction}{15}%
\contentsline {section}{\numberline {1.1}Background}{17}%
\contentsline {subsection}{\numberline {1.1.1}System Design Process}{17}%
\contentsline {subsection}{\numberline {1.1.2}Software architecture}{18}%
\contentsline {subsubsection}{Service Oriented Architecture}{18}%
\contentsline {subsubsection}{Application programming Interface}{20}%
\contentsline {subsection}{\numberline {1.1.3}Software Life Cycles}{20}%
\contentsline {subsubsection}{Software Development Life Cycle}{20}%
\contentsline {subsubsection}{Software Testing Life Cycle}{20}%
\contentsline {subsubsection}{Linear-sequential life cycle model}{20}%
\contentsline {subsubsection}{Verification and Validation model}{21}%
\contentsline {subsection}{\numberline {1.1.4}Software Specification, Behavior, and Test}{21}%
\contentsline {subsubsection}{Software Specification}{21}%
\contentsline {subsubsection}{Software behavior}{22}%
\contentsline {subsubsection}{Software Tests}{24}%
\contentsline {section}{\numberline {1.2}Motivation}{25}%
\contentsline {subsection}{\numberline {1.2.1}Software productivity}{25}%
\contentsline {chapter}{\numberline {2}Domain Specific Language/ Tools}{28}%
\contentsline {section}{\numberline {2.1}Domain Specific language}{28}%
\contentsline {subsection}{\numberline {2.1.1}Interface description language}{29}%
\contentsline {subsubsection}{Franca IDL}{29}%
\contentsline {subsection}{\numberline {2.1.2}Interface definition using Franca IDL}{32}%
\contentsline {subsubsection}{Interface name, version and contents}{32}%
\contentsline {subsubsection}{Interface versioning and change compatibility}{33}%
\contentsline {subsubsection}{Attributes}{33}%
\contentsline {subsubsection}{Methods}{34}%
\contentsline {subsubsection}{Broadcasts}{34}%
\contentsline {subsection}{\numberline {2.1.3}Gherkin DSL}{34}%
\contentsline {subsubsection}{Features}{36}%
\contentsline {subsubsection}{Scenarios}{36}%
\contentsline {subsubsection}{Scenario Outlines}{36}%
\contentsline {subsubsection}{Steps}{36}%
\contentsline {subsubsection}{$Given$}{37}%
\contentsline {subsubsection}{$When$}{37}%
\contentsline {subsubsection}{$Then$}{37}%
\contentsline {subsubsection}{Tables}{37}%
\contentsline {subsubsection}{Xtext}{38}%
\contentsline {subsubsection}{Eclipse Modeling Framework (EMF)}{39}%
\contentsline {subsection}{\numberline {2.1.4}Grancin DSL}{39}%
\contentsline {subsubsection}{Imports}{41}%
\contentsline {subsubsection}{No Condition defined in a step}{42}%
\contentsline {subsubsection}{Comparing of an Attribute with a specific value}{42}%
\contentsline {subsubsection}{Comparing an Attribute repetitively with timing}{43}%
\contentsline {subsubsection}{Incoming Method}{43}%
\contentsline {subsubsection}{Incoming Method with parameters}{43}%
\contentsline {subsubsection}{Incoming an In Method with timing}{44}%
\contentsline {subsubsection}{Reproduction of Scenarios}{44}%
\contentsline {subsubsection}{Waiting for amount of time}{44}%
\contentsline {subsubsection}{Comparing an Attribute with a specific value}{45}%
\contentsline {subsubsection}{Comparing an Attribute with timing}{45}%
\contentsline {subsubsection}{Sending a method/ broadcast}{45}%
\contentsline {subsubsection}{Sending a method/ broadcast with parameters}{46}%
\contentsline {subsubsection}{Sending a method/ broadcast with timing}{46}%
\contentsline {subsubsection}{Not Sending a method}{46}%
\contentsline {subsection}{\numberline {2.1.5}Compiler development language}{47}%
\contentsline {subsubsection}{Xtend}{47}%
\contentsline {section}{\numberline {2.2}Tools}{48}%
\contentsline {subsection}{\numberline {2.2.1}SMARD automate tool}{48}%
\contentsline {subsection}{\numberline {2.2.2}CARMEN Topology Editor}{55}%
\contentsline {chapter}{\numberline {3}Implementation}{58}%
\contentsline {section}{\numberline {3.1}Finite State Machine}{58}%
\contentsline {subsection}{\numberline {3.1.1}Mealy/ Moore FSM}{59}%
\contentsline {subsection}{\numberline {3.1.2}Harel State Chart}{60}%
\contentsline {subsubsection}{Clustering and refinement}{60}%
\contentsline {subsubsection}{Orthogonality, Independence and concurrency}{61}%
\contentsline {subsubsection}{Multiple Conditions and selection entrance}{62}%
\contentsline {subsubsection}{Overlapping}{62}%
\contentsline {subsubsection}{Unclustering}{63}%
\contentsline {subsubsection}{Actions and activities}{63}%
\contentsline {section}{\numberline {3.2}Chrisna FSM}{63}%
\contentsline {subsection}{\numberline {3.2.1}Initializer}{68}%
\contentsline {subsection}{\numberline {3.2.2}$Given$/ $When$ steps}{69}%
\contentsline {subsubsection}{$Given$ and $When$ steps both with attribute events}{69}%
\contentsline {subsubsection}{$Given$ step with attribute event and $When$ step within method/ broadcast event}{71}%
\contentsline {subsubsection}{$Given$ step with attribute and $When$ step in method with parameters}{72}%
\contentsline {subsubsection}{$Given$ step with attribute event and $When$ step with reproduction event}{73}%
\contentsline {subsubsection}{$Given$ step with attribute event and $When$ step with wait event}{74}%
\contentsline {subsubsection}{$Given$ step within method event and $When$ step with attribute event}{76}%
\contentsline {subsubsection}{$Given$ step with in method event and $When$ step with in method/ broadcast event}{77}%
\contentsline {subsubsection}{$Given$ step with in method and $When$ step in method with parameters}{79}%
\contentsline {subsubsection}{$Given$ step with in method event and $When$ step with reproduction event}{80}%
\contentsline {subsubsection}{$Given$ step with in method event and $When$ step with wait event}{81}%
\contentsline {subsubsection}{$Given$ step with in method with parameters}{83}%
\contentsline {subsubsection}{$Given$ step with a reproduction of a scenario event}{83}%
\contentsline {subsubsection}{$Given$ step with wait event}{84}%
\contentsline {subsection}{\numberline {3.2.3}$Then$ steps}{88}%
\contentsline {subsubsection}{$Then$ step with attribute event}{88}%
\contentsline {subsubsection}{$Then$ step with send method event}{89}%
\contentsline {subsubsection}{$Then$ step and send method event with parameters}{89}%
\contentsline {subsubsection}{$Then$ step with send method event periodically with specific period}{90}%
\contentsline {subsubsection}{$Then$ step with send method event on change of a specific value}{91}%
\contentsline {subsubsection}{$Then$ step with send method event at a condition}{92}%
\contentsline {subsubsection}{$Then$ step with sending no method event}{93}%
\contentsline {section}{\numberline {3.3}Translating logical to technical parameters}{101}%
\contentsline {section}{\numberline {3.4}Generating SMARD automate state machines}{102}%
\contentsline {subsection}{\numberline {3.4.1}Generating SMARD based on Technical entities and Chrisna FSM}{103}%
\contentsline {subsection}{\numberline {3.4.2}Importing generated State machine into SMARD automate tool}{105}%
\contentsline {section}{\numberline {3.5}Real time test in CARMEN tool}{109}%
\contentsline {subsection}{\numberline {3.5.1}Observers}{110}%
\contentsline {subsection}{\numberline {3.5.2}State Machines}{111}%
\contentsline {chapter}{\numberline {4}Results}{114}%
\contentsline {section}{\numberline {4.1}Interface under test: Assignment Entities}{114}%
\contentsline {chapter}{\numberline {5}Conclusions and Outlook}{122}%
\contentsline {section}{\numberline {5.1}Conclusion}{122}%
\contentsline {section}{\numberline {5.2}Future work}{126}%
\contentsline {chapter}{\numberline {A}Interaction Description of AE interface}{127}%
\contentsline {chapter}{\numberline {B}Notation and Abbreviations}{130}%
\contentsline {chapter}{Bibliography}{132}%
