\contentsline {lstlisting}{\numberline {2.1}A Frana IDL example code}{31}%
\contentsline {lstlisting}{\numberline {2.2}A Gherkin DSL example code}{35}%
\contentsline {lstlisting}{\numberline {2.3}A Grancin DSL example code}{40}%
\contentsline {lstlisting}{\numberline {2.4}Defining initial state}{42}%
\contentsline {lstlisting}{\numberline {2.5}Comparing attributes}{42}%
\contentsline {lstlisting}{\numberline {2.6}Comparing attributes repetitively}{43}%
\contentsline {lstlisting}{\numberline {2.7}Incoming methods}{43}%
\contentsline {lstlisting}{\numberline {2.8}Incoming methods with parameters}{44}%
\contentsline {lstlisting}{\numberline {2.9}Incoming methods with timing}{44}%
\contentsline {lstlisting}{\numberline {2.10}Reproduction of a scenario}{44}%
\contentsline {lstlisting}{\numberline {2.11}Waiting for a specific time duration}{45}%
\contentsline {lstlisting}{\numberline {2.12}Comparing attribute as post-condition}{45}%
\contentsline {lstlisting}{\numberline {2.13}Comparing attribute under the defined condition as post-condition}{45}%
\contentsline {lstlisting}{\numberline {2.14}Sending methods}{45}%
\contentsline {lstlisting}{\numberline {2.15}Sending methods with parameters}{46}%
\contentsline {lstlisting}{\numberline {2.16}Sending methods with timing}{46}%
\contentsline {lstlisting}{\numberline {2.17}Sending no methods}{47}%
\contentsline {lstlisting}{\numberline {3.1}Given and When steps both with attributes}{70}%
\contentsline {lstlisting}{\numberline {3.2}Given step with attribute and When step within method}{71}%
\contentsline {lstlisting}{\numberline {3.3}Given step with attribute and When step in method with parameters (and timing)}{73}%
\contentsline {lstlisting}{\numberline {3.4}Given step with attribute and When step with reproduction}{73}%
\contentsline {lstlisting}{\numberline {3.5}Given step with attribute and When step with wait}{75}%
\contentsline {lstlisting}{\numberline {3.6}Given step within method and When step with attribute}{76}%
\contentsline {lstlisting}{\numberline {3.7}Given step with in method and When step with in method}{78}%
\contentsline {lstlisting}{\numberline {3.8}Given step with in method and When step in method with parameters (and timing)}{79}%
\contentsline {lstlisting}{\numberline {3.9}Given step with in method and When step with reproduction}{80}%
\contentsline {lstlisting}{\numberline {3.10}Given step with in method and When step with wait}{82}%
\contentsline {lstlisting}{\numberline {3.11}Given step with reproduction of a scenario}{84}%
\contentsline {lstlisting}{\numberline {3.12}Given step with attribute and When step with wait}{85}%
\contentsline {lstlisting}{\numberline {3.13}Then step with comparing an attribute}{88}%
\contentsline {lstlisting}{\numberline {3.14}Then step with sending a method}{89}%
\contentsline {lstlisting}{\numberline {3.15}Then step with comparing a parameter}{90}%
\contentsline {lstlisting}{\numberline {3.16}Then step with sending method periodically with specific period}{91}%
\contentsline {lstlisting}{\numberline {3.17}Then step with send method on change of a specific value}{92}%
\contentsline {lstlisting}{\numberline {3.18}Then step with sending method under a condition}{93}%
\contentsline {lstlisting}{\numberline {3.19}Then step with stop sending a method}{93}%
\contentsline {lstlisting}{\numberline {3.20}all possible cases in one scenario}{99}%
\contentsline {lstlisting}{\numberline {A.1}the structure of AE interface in Franca IDL}{127}%
\contentsline {lstlisting}{\numberline {A.2}the behavior of AE interface in Grancin DSL}{128}%
