                              Excel2LaTeX
		             =============
			         V2.0

It's difficult to create tables in LaTeX, especially if some columns are
calculated. Excel2LaTeX allows you to transform the current selection from
Excel to Latex. Most of the formating will be kept (bold, italic, border
lines, multicolumn cells, etc.). The LaTeX code can be copied to the
clipboard or saved as a LaTeX file, which then can be included in an
existing LaTeX document using the \input command .


INSTALLATION
~~~~~~~~~~~~
Just open the file Excel2LaTeX.xla in Excel. Then you will have an additional
menu item in your 'Tools' menu and a new Toolbar with one button on it.
If you plan to use the program frequently you can save it in your addin
directory and add it with tools/add-in. This way it will be loaded whenever 
excel is opened. 


USAGE
~~~~~
Just select the table to convert and hit the button. You will be given the
option to save the result to a file, or send it to the clipboard (so you can
paste it on your Latex editor).


ANNOTATIONS
~~~~~~~~~~~
* Bold and italic are regognized as long as whole cell is in bold or italic.
* Alignment formats (right,left,center) will be recognized for the whole 
  column (not for individual cells)
* Font sizes are not supported, because they are handled completely different in TeX
* Mutlicolumn cells are supported. For this you have to merge the cells
  in excel using the format/cells menu and selecting the alignment tab. Note however 
  that cells merged across rows will not be handled properly.
* To keep the macro reasonably fast, only the characters $ and % are 
  converted to the LaTeX makros \$ and \%.
* You can put additional LaTeX formatting commands in the Excel cells, so you 
  don't have to edit the output, if you want some special chars or formats.
* The default file name for export is the name of the selected range (if it has one),
  otherwise it will be the active excel worksheet.


COPYRIGHT
~~~~~~~~~
Excel2Latex is freeware, that means you can freely use and distribute it.
                      �1996-2001 by Joachim Marder an

CONTACT
~~~~~~~
JAM Software
Joachim Marder
S�dallee 35
54290 Trier
Germany

E-Mail: marder@jam-software.com

WWW-Page for Excel2Latex:
http://www.jam-software.com/software.html


CHANGES
~~~~~~~
Version 2.0: Released on 21 Jul 2001
This version is based on modifications by Germ�n Ria�o
* Graphical user interface
* The LATeX code can be copied to clipboard and then pasted into you editor.
* Better handling of multicolum cells
* doublelines on top border are now handled


Version 1.2: Published on 22 Nov 1998
* The characters % and $ are now converted to the correspondig LaTeX makros

Version 1.1: Published on 12 Apr 1997
* Some small changes to make it run with Excel 97 too

Version 1.0: First published version, Oct 22 1996