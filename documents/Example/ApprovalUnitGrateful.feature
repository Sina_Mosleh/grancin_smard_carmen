# sismic-bdd ApprovalUnitGenerated.yaml --features ApprovalUnitGrateful.feature
Feature: ApprovalUnit
  In order to evaluate if the windows are allowed to move
  As a dedicated service 'ApprovalUnit'
  I evaluate 'window approval' for both, normal mode and panic mode

  Scenario: start-up behavior
    When  I do nothing
    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
    And   event ApprovalResultInterface is fired with hasPanicApproval=False

  Scenario: Car is driving and doors are all closed - full approval is granted
    When  I send event SelectingPWF with PWF="FAHREN"
    And   I send event ResultKeySearchPosition with Key="KeyNotInCar"
    And   I send event SelectingDoor with isDriverDoorOpen = False
    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
    And   event ApprovalResultInterface is fired with hasPanicApproval=True

  Scenario: Car is driving and doors are NOT all closed - panic approval is NOT granted
    When  I send event SelectingPWF with PWF="FAHREN"
    And   I send event ResultKeySearchPosition with Key="KeyNotInCar"
    And   I send event SelectingDoor with isDriverDoorOpen = True
    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
    And   event ApprovalResultInterface is fired with hasPanicApproval=False

  # recognize that the following scenario does not mention any key location
  # in this scenario, key location represents a "don't care".
  Scenario: Car is parking - no approval is granted
    When  I send event SelectingPWF with PWF="PARKEN"
    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
    And   event ApprovalResultInterface is fired with hasPanicApproval=False

  Scenario Outline: Key is not inside the car and car is neither driving nor preparing to drive
    When  I send event ResultKeySearchPosition with Key="KeyNotInCar"
    And   I send event SelectingPWF with PWF=<pwf>
    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
    And   event ApprovalResultInterface is fired with hasPanicApproval=False

    Examples:
      | pwf                                 |
      | "PARKEN_BN_NIO"                     |
      | "PARKEN_BN_IO"                      |
      | "STANDFUNKTION_KUNDE_NICHT_IM_FZG"  |
      | "WOHNEN"                            |
      | "PRUEFEN_ANALYSE_DIAGNOSE"          |
      | "FAHRBEREITSCHAFT_BEENDEN"          |
      | "SIGNAL_UNBEFUELLT"                 |

  Scenario Outline: Key is inside the car, car is not parking and >=1 door is open
    When  I send event ResultKeySearchPosition with Key="KeyInCar"
    And   I send event SelectingPWF with PWF=<pwf>
    And   I send event SelectingDoor with isDriverDoorOpen = True
    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
    And   event ApprovalResultInterface is fired with hasPanicApproval=False

    Examples:
      | pwf                                 |
      | "WOHNEN"                            |
      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
      | "FAHREN"                            |
      | "FAHRBEREITSCHAFT_BEENDEN"          |

  # What happened (or should happen) if the following table contained "FAHREN"
  # This would be redundant information in regard to a previous scenario
  Scenario Outline: Key is inside the car, car is not parking and all doors are closed
    When  I send event ResultKeySearchPosition with Key="KeyInCar"
    And   I send event SelectingPWF with PWF=<pwf>
    And   I send event SelectingDoor with isDriverDoorOpen = False
    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
    And   event ApprovalResultInterface is fired with hasPanicApproval=True

    Examples:
      | pwf                                 |
      | "WOHNEN"                            |
      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
      | "FAHRBEREITSCHAFT_BEENDEN"          |
      | "FAHREN"                            |
