package com.bmw.vv.grancin.codegen.unittest.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.xbase.testing.CompilationTestHelper
//import org.eclipse.xtext.testing.XtextRunner
//import org.junit.runner.RunWith
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(GrancinInjectorProvider)

class GrancinUtGeneratorTest implements TestBench {
	@Inject extension CompilationTestHelper
	
	@Test 
    def dummy() {
    	Assertions.assertEquals(1,1)
    }
    
	@Test 
    def nochDuemmer() {
    	FeatureTitle.key.assertCompilesTo(FeatureTitle.value)
    }
}