package com.bmw.vv.grancin.codegen.unittest.tests

/*
 * '''<input>''' -> '''<output after code generation>'''
 */

interface TestBench {
	val FeatureTitle = 
	'''
	Feature: ApprovalUnit
	''' 
	->
  	'''
	<high performance code ;-)>
	'''
}
