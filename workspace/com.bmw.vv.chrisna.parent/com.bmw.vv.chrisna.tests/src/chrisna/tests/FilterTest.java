/**
 */
package chrisna.tests;

import chrisna.ChrisnaFactory;
import chrisna.Filter;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link chrisna.Filter#GivenToWhenAttr() <em>Given To When Attr</em>}</li>
 *   <li>{@link chrisna.Filter#HierarchieCheck() <em>Hierarchie Check</em>}</li>
 *   <li>{@link chrisna.Filter#OrtogonalCheck() <em>Ortogonal Check</em>}</li>
 *   <li>{@link chrisna.Filter#sevCondCheck() <em>Sev Cond Check</em>}</li>
 *   <li>{@link chrisna.Filter#ParametrizationCheck() <em>Parametrization Check</em>}</li>
 *   <li>{@link chrisna.Filter#RekurAktCheck() <em>Rekur Akt Check</em>}</li>
 *   <li>{@link chrisna.Filter#SetScensWithTime() <em>Set Scens With Time</em>}</li>
 *   <li>{@link chrisna.Filter#GetScensWithTime() <em>Get Scens With Time</em>}</li>
 *   <li>{@link chrisna.Filter#SetScensWithoutTime() <em>Set Scens Without Time</em>}</li>
 *   <li>{@link chrisna.Filter#GetScensWithoutTime() <em>Get Scens Without Time</em>}</li>
 *   <li>{@link chrisna.Filter#OverlappCheck() <em>Overlapp Check</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class FilterTest extends TestCase {

	/**
	 * The fixture for this Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Filter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FilterTest.class);
	}

	/**
	 * Constructs a new Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FilterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Filter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Filter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ChrisnaFactory.eINSTANCE.createFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link chrisna.Filter#GivenToWhenAttr() <em>Given To When Attr</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#GivenToWhenAttr()
	 * @generated
	 */
	public void testGivenToWhenAttr() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#HierarchieCheck() <em>Hierarchie Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#HierarchieCheck()
	 * @generated
	 */
	public void testHierarchieCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#OrtogonalCheck() <em>Ortogonal Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#OrtogonalCheck()
	 * @generated
	 */
	public void testOrtogonalCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#sevCondCheck() <em>Sev Cond Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#sevCondCheck()
	 * @generated
	 */
	public void testSevCondCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#ParametrizationCheck() <em>Parametrization Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#ParametrizationCheck()
	 * @generated
	 */
	public void testParametrizationCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#RekurAktCheck() <em>Rekur Akt Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#RekurAktCheck()
	 * @generated
	 */
	public void testRekurAktCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#SetScensWithTime() <em>Set Scens With Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#SetScensWithTime()
	 * @generated
	 */
	public void testSetScensWithTime() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#GetScensWithTime() <em>Get Scens With Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#GetScensWithTime()
	 * @generated
	 */
	public void testGetScensWithTime() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#SetScensWithoutTime() <em>Set Scens Without Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#SetScensWithoutTime()
	 * @generated
	 */
	public void testSetScensWithoutTime() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#GetScensWithoutTime() <em>Get Scens Without Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#GetScensWithoutTime()
	 * @generated
	 */
	public void testGetScensWithoutTime() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link chrisna.Filter#OverlappCheck() <em>Overlapp Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Filter#OverlappCheck()
	 * @generated
	 */
	public void testOverlappCheck() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //FilterTest
