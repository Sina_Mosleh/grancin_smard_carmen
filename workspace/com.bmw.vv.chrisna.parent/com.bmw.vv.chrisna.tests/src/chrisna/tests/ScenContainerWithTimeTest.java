/**
 */
package chrisna.tests;

import chrisna.ChrisnaFactory;
import chrisna.ScenContainerWithTime;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Scen Container With Time</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenContainerWithTimeTest extends TestCase {

	/**
	 * The fixture for this Scen Container With Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenContainerWithTime fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ScenContainerWithTimeTest.class);
	}

	/**
	 * Constructs a new Scen Container With Time test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainerWithTimeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Scen Container With Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ScenContainerWithTime fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Scen Container With Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenContainerWithTime getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ChrisnaFactory.eINSTANCE.createScenContainerWithTime());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ScenContainerWithTimeTest
