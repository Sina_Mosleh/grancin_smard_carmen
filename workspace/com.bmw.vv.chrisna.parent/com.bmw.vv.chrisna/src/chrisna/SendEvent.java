/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.SendEvent#getMethod <em>Method</em>}</li>
 *   <li>{@link chrisna.SendEvent#getAssigcontainer <em>Assigcontainer</em>}</li>
 *   <li>{@link chrisna.SendEvent#getEvent <em>Event</em>}</li>
 *   <li>{@link chrisna.SendEvent#getTime <em>Time</em>}</li>
 *   <li>{@link chrisna.SendEvent#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getSendEvent()
 * @model
 * @generated
 */
public interface SendEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Method</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method</em>' attribute.
	 * @see #setMethod(String)
	 * @see chrisna.ChrisnaPackage#getSendEvent_Method()
	 * @model required="true"
	 * @generated
	 */
	String getMethod();

	/**
	 * Sets the value of the '{@link chrisna.SendEvent#getMethod <em>Method</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method</em>' attribute.
	 * @see #getMethod()
	 * @generated
	 */
	void setMethod(String value);

	/**
	 * Returns the value of the '<em><b>Assigcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigcontainer</em>' reference.
	 * @see #setAssigcontainer(AssigContainer)
	 * @see chrisna.ChrisnaPackage#getSendEvent_Assigcontainer()
	 * @model
	 * @generated
	 */
	AssigContainer getAssigcontainer();

	/**
	 * Sets the value of the '{@link chrisna.SendEvent#getAssigcontainer <em>Assigcontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigcontainer</em>' reference.
	 * @see #getAssigcontainer()
	 * @generated
	 */
	void setAssigcontainer(AssigContainer value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see chrisna.ChrisnaPackage#getSendEvent_Event()
	 * @model required="true"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link chrisna.SendEvent#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

	/**
	 * Returns the value of the '<em><b>Time</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' reference.
	 * @see #setTime(Time)
	 * @see chrisna.ChrisnaPackage#getSendEvent_Time()
	 * @model
	 * @generated
	 */
	Time getTime();

	/**
	 * Sets the value of the '{@link chrisna.SendEvent#getTime <em>Time</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' reference.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Time value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * The literals are from the enumeration {@link chrisna.Trigger}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see chrisna.Trigger
	 * @see #setTrigger(Trigger)
	 * @see chrisna.ChrisnaPackage#getSendEvent_Trigger()
	 * @model required="true"
	 * @generated
	 */
	Trigger getTrigger();

	/**
	 * Sets the value of the '{@link chrisna.SendEvent#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see chrisna.Trigger
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(Trigger value);

} // SendEvent
