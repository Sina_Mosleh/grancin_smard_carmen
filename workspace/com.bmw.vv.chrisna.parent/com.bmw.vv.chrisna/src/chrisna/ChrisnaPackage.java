/**
 */
package chrisna;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chrisna.ChrisnaFactory
 * @model kind="package"
 * @generated
 */
public interface ChrisnaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "chrisna";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.bmw.com/vv/Chrisna";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "com.bmw.vv.chrisna";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ChrisnaPackage eINSTANCE = chrisna.impl.ChrisnaPackageImpl.init();

	/**
	 * The meta object id for the '{@link chrisna.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.OperationImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Str</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__STR = 0;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.actionImpl <em>action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.actionImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getaction()
	 * @generated
	 */
	int ACTION = 1;

	/**
	 * The feature id for the '<em><b>Sendevent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__SENDEVENT = 0;

	/**
	 * The number of structural features of the '<em>action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.stateImpl <em>state</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.stateImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getstate()
	 * @generated
	 */
	int STATE = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 2;

	/**
	 * The number of structural features of the '<em>state</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>state</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.stateContainerImpl <em>state Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.stateContainerImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getstateContainer()
	 * @generated
	 */
	int STATE_CONTAINER = 3;

	/**
	 * The feature id for the '<em><b>State</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CONTAINER__STATE = 0;

	/**
	 * The number of structural features of the '<em>state Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>state Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.ScenContainerWithTimeImpl <em>Scen Container With Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ScenContainerWithTimeImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainerWithTime()
	 * @generated
	 */
	int SCEN_CONTAINER_WITH_TIME = 4;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITH_TIME__SCENCONTAINER = 0;

	/**
	 * The number of structural features of the '<em>Scen Container With Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITH_TIME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Scen Container With Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITH_TIME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.initStateImpl <em>init State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.initStateImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getinitState()
	 * @generated
	 */
	int INIT_STATE = 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INIT_STATE__DESCRIPTION = STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INIT_STATE__ACTION = STATE__ACTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INIT_STATE__NAME = STATE__NAME;

	/**
	 * The number of structural features of the '<em>init State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INIT_STATE_FEATURE_COUNT = STATE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>init State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INIT_STATE_OPERATION_COUNT = STATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.ScenContainerWithoutTimeImpl <em>Scen Container Without Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ScenContainerWithoutTimeImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainerWithoutTime()
	 * @generated
	 */
	int SCEN_CONTAINER_WITHOUT_TIME = 6;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITHOUT_TIME__SCENCONTAINER = 0;

	/**
	 * The number of structural features of the '<em>Scen Container Without Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITHOUT_TIME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Scen Container Without Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_WITHOUT_TIME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.HistoryDispachImpl <em>History Dispach</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.HistoryDispachImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getHistoryDispach()
	 * @generated
	 */
	int HISTORY_DISPACH = 7;

	/**
	 * The feature id for the '<em><b>Scencontainerwithtime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH__SCENCONTAINERWITHTIME = 0;

	/**
	 * The feature id for the '<em><b>Scencontainerwithouttime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME = 1;

	/**
	 * The feature id for the '<em><b>Nodefinder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH__NODEFINDER = 2;

	/**
	 * The feature id for the '<em><b>Counter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH__COUNTER = 3;

	/**
	 * The number of structural features of the '<em>History Dispach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>History Dispach</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_DISPACH_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.FilterImpl <em>Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.FilterImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getFilter()
	 * @generated
	 */
	int FILTER = 8;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER__SCENCONTAINER = 0;

	/**
	 * The number of structural features of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Given To When Attr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___GIVEN_TO_WHEN_ATTR = 0;

	/**
	 * The operation id for the '<em>Hierarchie Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___HIERARCHIE_CHECK = 1;

	/**
	 * The operation id for the '<em>Ortogonal Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___ORTOGONAL_CHECK = 2;

	/**
	 * The operation id for the '<em>Sev Cond Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___SEV_COND_CHECK = 3;

	/**
	 * The operation id for the '<em>Parametrization Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___PARAMETRIZATION_CHECK = 4;

	/**
	 * The operation id for the '<em>Rekur Akt Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___REKUR_AKT_CHECK = 5;

	/**
	 * The operation id for the '<em>Set Scens With Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___SET_SCENS_WITH_TIME = 6;

	/**
	 * The operation id for the '<em>Get Scens With Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___GET_SCENS_WITH_TIME = 7;

	/**
	 * The operation id for the '<em>Set Scens Without Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___SET_SCENS_WITHOUT_TIME = 8;

	/**
	 * The operation id for the '<em>Get Scens Without Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___GET_SCENS_WITHOUT_TIME = 9;

	/**
	 * The operation id for the '<em>Overlapp Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER___OVERLAPP_CHECK = 10;

	/**
	 * The number of operations of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_OPERATION_COUNT = 11;

	/**
	 * The meta object id for the '{@link chrisna.impl.ChrisnaFSMImpl <em>FSM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ChrisnaFSMImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getChrisnaFSM()
	 * @generated
	 */
	int CHRISNA_FSM = 9;

	/**
	 * The feature id for the '<em><b>Initstate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHRISNA_FSM__INITSTATE = 0;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHRISNA_FSM__SCENCONTAINER = 1;

	/**
	 * The number of structural features of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHRISNA_FSM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>FSM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHRISNA_FSM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.CounterImpl <em>Counter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.CounterImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getCounter()
	 * @generated
	 */
	int COUNTER = 10;

	/**
	 * The feature id for the '<em><b>Scencontainerwithtime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTER__SCENCONTAINERWITHTIME = 0;

	/**
	 * The number of structural features of the '<em>Counter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Counter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COUNTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.WarningErrorImpl <em>Warning Error</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.WarningErrorImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getWarningError()
	 * @generated
	 */
	int WARNING_ERROR = 11;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WARNING_ERROR__SCENCONTAINER = 0;

	/**
	 * The number of structural features of the '<em>Warning Error</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WARNING_ERROR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Warning Error</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WARNING_ERROR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.NodeFinderImpl <em>Node Finder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.NodeFinderImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getNodeFinder()
	 * @generated
	 */
	int NODE_FINDER = 12;

	/**
	 * The feature id for the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FINDER__SCENCONTAINER = 0;

	/**
	 * The number of structural features of the '<em>Node Finder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FINDER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Node Finder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FINDER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.AssignmentImpl <em>Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.AssignmentImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getAssignment()
	 * @generated
	 */
	int ASSIGNMENT = 13;

	/**
	 * The feature id for the '<em><b>Left Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__LEFT_SIDE = 0;

	/**
	 * The feature id for the '<em><b>Right Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__RIGHT_SIDE = 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT__OPERATION = 2;

	/**
	 * The number of structural features of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.AssigContainerImpl <em>Assig Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.AssigContainerImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getAssigContainer()
	 * @generated
	 */
	int ASSIG_CONTAINER = 14;

	/**
	 * The feature id for the '<em><b>Assignment</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIG_CONTAINER__ASSIGNMENT = 0;

	/**
	 * The number of structural features of the '<em>Assig Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIG_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Assig Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIG_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.SendEventImpl <em>Send Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.SendEventImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getSendEvent()
	 * @generated
	 */
	int SEND_EVENT = 15;

	/**
	 * The feature id for the '<em><b>Method</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT__METHOD = 0;

	/**
	 * The feature id for the '<em><b>Assigcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT__ASSIGCONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT__EVENT = 2;

	/**
	 * The feature id for the '<em><b>Time</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT__TIME = 3;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT__TRIGGER = 4;

	/**
	 * The number of structural features of the '<em>Send Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Send Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEND_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.TimeImpl <em>Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.TimeImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getTime()
	 * @generated
	 */
	int TIME = 16;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__TYPE = 0;

	/**
	 * The number of structural features of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.OnChangeImpl <em>On Change</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.OnChangeImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getOnChange()
	 * @generated
	 */
	int ON_CHANGE = 17;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_CHANGE__TYPE = TIME__TYPE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_CHANGE__NAME = TIME_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>On Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_CHANGE_FEATURE_COUNT = TIME_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>On Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ON_CHANGE_OPERATION_COUNT = TIME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.PeriodicallyImpl <em>Periodically</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.PeriodicallyImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getPeriodically()
	 * @generated
	 */
	int PERIODICALLY = 18;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODICALLY__TYPE = TIME__TYPE;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODICALLY__TIME = TIME_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODICALLY__UNIT = TIME_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Periodically</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODICALLY_FEATURE_COUNT = TIME_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Periodically</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODICALLY_OPERATION_COUNT = TIME_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.TimeWindowImpl <em>Time Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.TimeWindowImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getTimeWindow()
	 * @generated
	 */
	int TIME_WINDOW = 19;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__TYPE = PERIODICALLY__TYPE;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__TIME = PERIODICALLY__TIME;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__UNIT = PERIODICALLY__UNIT;

	/**
	 * The feature id for the '<em><b>Assigcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW__ASSIGCONTAINER = PERIODICALLY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Time Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW_FEATURE_COUNT = PERIODICALLY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Time Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_WINDOW_OPERATION_COUNT = PERIODICALLY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.EventContainerImpl <em>Event Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.EventContainerImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getEventContainer()
	 * @generated
	 */
	int EVENT_CONTAINER = 20;

	/**
	 * The feature id for the '<em><b>Sendevent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONTAINER__SENDEVENT = 0;

	/**
	 * The number of structural features of the '<em>Event Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.ScenarioImpl <em>Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ScenarioImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getScenario()
	 * @generated
	 */
	int SCENARIO = 21;

	/**
	 * The feature id for the '<em><b>Current States</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__CURRENT_STATES = 0;

	/**
	 * The feature id for the '<em><b>Next States</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__NEXT_STATES = 1;

	/**
	 * The feature id for the '<em><b>Condcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__CONDCONTAINER = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__NAME = 3;

	/**
	 * The number of structural features of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ConditionImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 22;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__ACTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__NAME = 2;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__TO = 3;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__FROM = 4;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.CondContainerImpl <em>Cond Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.CondContainerImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getCondContainer()
	 * @generated
	 */
	int COND_CONTAINER = 23;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_CONTAINER__CONDITION = 0;

	/**
	 * The number of structural features of the '<em>Cond Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Cond Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COND_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.impl.ScenContainerImpl <em>Scen Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.impl.ScenContainerImpl
	 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainer()
	 * @generated
	 */
	int SCEN_CONTAINER = 24;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER__SCENARIO = 0;

	/**
	 * The number of structural features of the '<em>Scen Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Scen Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCEN_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chrisna.Trigger <em>Trigger</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chrisna.Trigger
	 * @see chrisna.impl.ChrisnaPackageImpl#getTrigger()
	 * @generated
	 */
	int TRIGGER = 25;


	/**
	 * Returns the meta object for class '{@link chrisna.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see chrisna.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Operation#getStr <em>Str</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Str</em>'.
	 * @see chrisna.Operation#getStr()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Str();

	/**
	 * Returns the meta object for class '{@link chrisna.action <em>action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>action</em>'.
	 * @see chrisna.action
	 * @generated
	 */
	EClass getaction();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.action#getSendevent <em>Sendevent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sendevent</em>'.
	 * @see chrisna.action#getSendevent()
	 * @see #getaction()
	 * @generated
	 */
	EReference getaction_Sendevent();

	/**
	 * Returns the meta object for class '{@link chrisna.state <em>state</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>state</em>'.
	 * @see chrisna.state
	 * @generated
	 */
	EClass getstate();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.state#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see chrisna.state#getDescription()
	 * @see #getstate()
	 * @generated
	 */
	EAttribute getstate_Description();

	/**
	 * Returns the meta object for the reference '{@link chrisna.state#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see chrisna.state#getAction()
	 * @see #getstate()
	 * @generated
	 */
	EReference getstate_Action();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.state#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see chrisna.state#getName()
	 * @see #getstate()
	 * @generated
	 */
	EAttribute getstate_Name();

	/**
	 * Returns the meta object for class '{@link chrisna.stateContainer <em>state Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>state Container</em>'.
	 * @see chrisna.stateContainer
	 * @generated
	 */
	EClass getstateContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.stateContainer#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>State</em>'.
	 * @see chrisna.stateContainer#getState()
	 * @see #getstateContainer()
	 * @generated
	 */
	EReference getstateContainer_State();

	/**
	 * Returns the meta object for class '{@link chrisna.ScenContainerWithTime <em>Scen Container With Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scen Container With Time</em>'.
	 * @see chrisna.ScenContainerWithTime
	 * @generated
	 */
	EClass getScenContainerWithTime();

	/**
	 * Returns the meta object for the reference '{@link chrisna.ScenContainerWithTime#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.ScenContainerWithTime#getScencontainer()
	 * @see #getScenContainerWithTime()
	 * @generated
	 */
	EReference getScenContainerWithTime_Scencontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.initState <em>init State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>init State</em>'.
	 * @see chrisna.initState
	 * @generated
	 */
	EClass getinitState();

	/**
	 * Returns the meta object for class '{@link chrisna.ScenContainerWithoutTime <em>Scen Container Without Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scen Container Without Time</em>'.
	 * @see chrisna.ScenContainerWithoutTime
	 * @generated
	 */
	EClass getScenContainerWithoutTime();

	/**
	 * Returns the meta object for the reference '{@link chrisna.ScenContainerWithoutTime#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.ScenContainerWithoutTime#getScencontainer()
	 * @see #getScenContainerWithoutTime()
	 * @generated
	 */
	EReference getScenContainerWithoutTime_Scencontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.HistoryDispach <em>History Dispach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Dispach</em>'.
	 * @see chrisna.HistoryDispach
	 * @generated
	 */
	EClass getHistoryDispach();

	/**
	 * Returns the meta object for the reference '{@link chrisna.HistoryDispach#getScencontainerwithtime <em>Scencontainerwithtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainerwithtime</em>'.
	 * @see chrisna.HistoryDispach#getScencontainerwithtime()
	 * @see #getHistoryDispach()
	 * @generated
	 */
	EReference getHistoryDispach_Scencontainerwithtime();

	/**
	 * Returns the meta object for the reference '{@link chrisna.HistoryDispach#getScencontainerwithouttime <em>Scencontainerwithouttime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainerwithouttime</em>'.
	 * @see chrisna.HistoryDispach#getScencontainerwithouttime()
	 * @see #getHistoryDispach()
	 * @generated
	 */
	EReference getHistoryDispach_Scencontainerwithouttime();

	/**
	 * Returns the meta object for the reference '{@link chrisna.HistoryDispach#getNodefinder <em>Nodefinder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Nodefinder</em>'.
	 * @see chrisna.HistoryDispach#getNodefinder()
	 * @see #getHistoryDispach()
	 * @generated
	 */
	EReference getHistoryDispach_Nodefinder();

	/**
	 * Returns the meta object for the reference '{@link chrisna.HistoryDispach#getCounter <em>Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Counter</em>'.
	 * @see chrisna.HistoryDispach#getCounter()
	 * @see #getHistoryDispach()
	 * @generated
	 */
	EReference getHistoryDispach_Counter();

	/**
	 * Returns the meta object for class '{@link chrisna.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter</em>'.
	 * @see chrisna.Filter
	 * @generated
	 */
	EClass getFilter();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Filter#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.Filter#getScencontainer()
	 * @see #getFilter()
	 * @generated
	 */
	EReference getFilter_Scencontainer();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#GivenToWhenAttr() <em>Given To When Attr</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Given To When Attr</em>' operation.
	 * @see chrisna.Filter#GivenToWhenAttr()
	 * @generated
	 */
	EOperation getFilter__GivenToWhenAttr();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#HierarchieCheck() <em>Hierarchie Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Hierarchie Check</em>' operation.
	 * @see chrisna.Filter#HierarchieCheck()
	 * @generated
	 */
	EOperation getFilter__HierarchieCheck();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#OrtogonalCheck() <em>Ortogonal Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ortogonal Check</em>' operation.
	 * @see chrisna.Filter#OrtogonalCheck()
	 * @generated
	 */
	EOperation getFilter__OrtogonalCheck();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#sevCondCheck() <em>Sev Cond Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Sev Cond Check</em>' operation.
	 * @see chrisna.Filter#sevCondCheck()
	 * @generated
	 */
	EOperation getFilter__SevCondCheck();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#ParametrizationCheck() <em>Parametrization Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Parametrization Check</em>' operation.
	 * @see chrisna.Filter#ParametrizationCheck()
	 * @generated
	 */
	EOperation getFilter__ParametrizationCheck();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#RekurAktCheck() <em>Rekur Akt Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Rekur Akt Check</em>' operation.
	 * @see chrisna.Filter#RekurAktCheck()
	 * @generated
	 */
	EOperation getFilter__RekurAktCheck();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#SetScensWithTime() <em>Set Scens With Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Scens With Time</em>' operation.
	 * @see chrisna.Filter#SetScensWithTime()
	 * @generated
	 */
	EOperation getFilter__SetScensWithTime();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#GetScensWithTime() <em>Get Scens With Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Scens With Time</em>' operation.
	 * @see chrisna.Filter#GetScensWithTime()
	 * @generated
	 */
	EOperation getFilter__GetScensWithTime();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#SetScensWithoutTime() <em>Set Scens Without Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Scens Without Time</em>' operation.
	 * @see chrisna.Filter#SetScensWithoutTime()
	 * @generated
	 */
	EOperation getFilter__SetScensWithoutTime();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#GetScensWithoutTime() <em>Get Scens Without Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Scens Without Time</em>' operation.
	 * @see chrisna.Filter#GetScensWithoutTime()
	 * @generated
	 */
	EOperation getFilter__GetScensWithoutTime();

	/**
	 * Returns the meta object for the '{@link chrisna.Filter#OverlappCheck() <em>Overlapp Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Overlapp Check</em>' operation.
	 * @see chrisna.Filter#OverlappCheck()
	 * @generated
	 */
	EOperation getFilter__OverlappCheck();

	/**
	 * Returns the meta object for class '{@link chrisna.ChrisnaFSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FSM</em>'.
	 * @see chrisna.ChrisnaFSM
	 * @generated
	 */
	EClass getChrisnaFSM();

	/**
	 * Returns the meta object for the reference '{@link chrisna.ChrisnaFSM#getInitstate <em>Initstate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initstate</em>'.
	 * @see chrisna.ChrisnaFSM#getInitstate()
	 * @see #getChrisnaFSM()
	 * @generated
	 */
	EReference getChrisnaFSM_Initstate();

	/**
	 * Returns the meta object for the reference '{@link chrisna.ChrisnaFSM#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.ChrisnaFSM#getScencontainer()
	 * @see #getChrisnaFSM()
	 * @generated
	 */
	EReference getChrisnaFSM_Scencontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.Counter <em>Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Counter</em>'.
	 * @see chrisna.Counter
	 * @generated
	 */
	EClass getCounter();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Counter#getScencontainerwithtime <em>Scencontainerwithtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainerwithtime</em>'.
	 * @see chrisna.Counter#getScencontainerwithtime()
	 * @see #getCounter()
	 * @generated
	 */
	EReference getCounter_Scencontainerwithtime();

	/**
	 * Returns the meta object for class '{@link chrisna.WarningError <em>Warning Error</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Warning Error</em>'.
	 * @see chrisna.WarningError
	 * @generated
	 */
	EClass getWarningError();

	/**
	 * Returns the meta object for the reference '{@link chrisna.WarningError#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.WarningError#getScencontainer()
	 * @see #getWarningError()
	 * @generated
	 */
	EReference getWarningError_Scencontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.NodeFinder <em>Node Finder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Finder</em>'.
	 * @see chrisna.NodeFinder
	 * @generated
	 */
	EClass getNodeFinder();

	/**
	 * Returns the meta object for the reference '{@link chrisna.NodeFinder#getScencontainer <em>Scencontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scencontainer</em>'.
	 * @see chrisna.NodeFinder#getScencontainer()
	 * @see #getNodeFinder()
	 * @generated
	 */
	EReference getNodeFinder_Scencontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment</em>'.
	 * @see chrisna.Assignment
	 * @generated
	 */
	EClass getAssignment();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Assignment#getLeftSide <em>Left Side</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left Side</em>'.
	 * @see chrisna.Assignment#getLeftSide()
	 * @see #getAssignment()
	 * @generated
	 */
	EAttribute getAssignment_LeftSide();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Assignment#getRightSide <em>Right Side</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Right Side</em>'.
	 * @see chrisna.Assignment#getRightSide()
	 * @see #getAssignment()
	 * @generated
	 */
	EAttribute getAssignment_RightSide();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Assignment#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see chrisna.Assignment#getOperation()
	 * @see #getAssignment()
	 * @generated
	 */
	EReference getAssignment_Operation();

	/**
	 * Returns the meta object for class '{@link chrisna.AssigContainer <em>Assig Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assig Container</em>'.
	 * @see chrisna.AssigContainer
	 * @generated
	 */
	EClass getAssigContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.AssigContainer#getAssignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Assignment</em>'.
	 * @see chrisna.AssigContainer#getAssignment()
	 * @see #getAssigContainer()
	 * @generated
	 */
	EReference getAssigContainer_Assignment();

	/**
	 * Returns the meta object for class '{@link chrisna.SendEvent <em>Send Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Send Event</em>'.
	 * @see chrisna.SendEvent
	 * @generated
	 */
	EClass getSendEvent();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.SendEvent#getMethod <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method</em>'.
	 * @see chrisna.SendEvent#getMethod()
	 * @see #getSendEvent()
	 * @generated
	 */
	EAttribute getSendEvent_Method();

	/**
	 * Returns the meta object for the reference '{@link chrisna.SendEvent#getAssigcontainer <em>Assigcontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assigcontainer</em>'.
	 * @see chrisna.SendEvent#getAssigcontainer()
	 * @see #getSendEvent()
	 * @generated
	 */
	EReference getSendEvent_Assigcontainer();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.SendEvent#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see chrisna.SendEvent#getEvent()
	 * @see #getSendEvent()
	 * @generated
	 */
	EAttribute getSendEvent_Event();

	/**
	 * Returns the meta object for the reference '{@link chrisna.SendEvent#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time</em>'.
	 * @see chrisna.SendEvent#getTime()
	 * @see #getSendEvent()
	 * @generated
	 */
	EReference getSendEvent_Time();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.SendEvent#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see chrisna.SendEvent#getTrigger()
	 * @see #getSendEvent()
	 * @generated
	 */
	EAttribute getSendEvent_Trigger();

	/**
	 * Returns the meta object for class '{@link chrisna.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time</em>'.
	 * @see chrisna.Time
	 * @generated
	 */
	EClass getTime();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Time#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see chrisna.Time#getType()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_Type();

	/**
	 * Returns the meta object for class '{@link chrisna.OnChange <em>On Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>On Change</em>'.
	 * @see chrisna.OnChange
	 * @generated
	 */
	EClass getOnChange();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.OnChange#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see chrisna.OnChange#getName()
	 * @see #getOnChange()
	 * @generated
	 */
	EAttribute getOnChange_Name();

	/**
	 * Returns the meta object for class '{@link chrisna.Periodically <em>Periodically</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Periodically</em>'.
	 * @see chrisna.Periodically
	 * @generated
	 */
	EClass getPeriodically();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Periodically#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see chrisna.Periodically#getTime()
	 * @see #getPeriodically()
	 * @generated
	 */
	EAttribute getPeriodically_Time();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Periodically#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see chrisna.Periodically#getUnit()
	 * @see #getPeriodically()
	 * @generated
	 */
	EAttribute getPeriodically_Unit();

	/**
	 * Returns the meta object for class '{@link chrisna.TimeWindow <em>Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time Window</em>'.
	 * @see chrisna.TimeWindow
	 * @generated
	 */
	EClass getTimeWindow();

	/**
	 * Returns the meta object for the reference '{@link chrisna.TimeWindow#getAssigcontainer <em>Assigcontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assigcontainer</em>'.
	 * @see chrisna.TimeWindow#getAssigcontainer()
	 * @see #getTimeWindow()
	 * @generated
	 */
	EReference getTimeWindow_Assigcontainer();

	/**
	 * Returns the meta object for class '{@link chrisna.EventContainer <em>Event Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Container</em>'.
	 * @see chrisna.EventContainer
	 * @generated
	 */
	EClass getEventContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.EventContainer#getSendevent <em>Sendevent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sendevent</em>'.
	 * @see chrisna.EventContainer#getSendevent()
	 * @see #getEventContainer()
	 * @generated
	 */
	EReference getEventContainer_Sendevent();

	/**
	 * Returns the meta object for class '{@link chrisna.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario</em>'.
	 * @see chrisna.Scenario
	 * @generated
	 */
	EClass getScenario();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Scenario#getCurrentStates <em>Current States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current States</em>'.
	 * @see chrisna.Scenario#getCurrentStates()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_CurrentStates();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Scenario#getNextStates <em>Next States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next States</em>'.
	 * @see chrisna.Scenario#getNextStates()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_NextStates();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Scenario#getCondcontainer <em>Condcontainer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Condcontainer</em>'.
	 * @see chrisna.Scenario#getCondcontainer()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_Condcontainer();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Scenario#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see chrisna.Scenario#getName()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Name();

	/**
	 * Returns the meta object for class '{@link chrisna.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see chrisna.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Condition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see chrisna.Condition#getDescription()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Description();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Condition#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see chrisna.Condition#getAction()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_Action();

	/**
	 * Returns the meta object for the attribute '{@link chrisna.Condition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see chrisna.Condition#getName()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Name();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Condition#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see chrisna.Condition#getTo()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_To();

	/**
	 * Returns the meta object for the reference '{@link chrisna.Condition#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see chrisna.Condition#getFrom()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_From();

	/**
	 * Returns the meta object for class '{@link chrisna.CondContainer <em>Cond Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cond Container</em>'.
	 * @see chrisna.CondContainer
	 * @generated
	 */
	EClass getCondContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.CondContainer#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Condition</em>'.
	 * @see chrisna.CondContainer#getCondition()
	 * @see #getCondContainer()
	 * @generated
	 */
	EReference getCondContainer_Condition();

	/**
	 * Returns the meta object for class '{@link chrisna.ScenContainer <em>Scen Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scen Container</em>'.
	 * @see chrisna.ScenContainer
	 * @generated
	 */
	EClass getScenContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link chrisna.ScenContainer#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scenario</em>'.
	 * @see chrisna.ScenContainer#getScenario()
	 * @see #getScenContainer()
	 * @generated
	 */
	EReference getScenContainer_Scenario();

	/**
	 * Returns the meta object for enum '{@link chrisna.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Trigger</em>'.
	 * @see chrisna.Trigger
	 * @generated
	 */
	EEnum getTrigger();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ChrisnaFactory getChrisnaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chrisna.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.OperationImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Str</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__STR = eINSTANCE.getOperation_Str();

		/**
		 * The meta object literal for the '{@link chrisna.impl.actionImpl <em>action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.actionImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getaction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getaction();

		/**
		 * The meta object literal for the '<em><b>Sendevent</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__SENDEVENT = eINSTANCE.getaction_Sendevent();

		/**
		 * The meta object literal for the '{@link chrisna.impl.stateImpl <em>state</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.stateImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getstate()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getstate();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__DESCRIPTION = eINSTANCE.getstate_Description();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE__ACTION = eINSTANCE.getstate_Action();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__NAME = eINSTANCE.getstate_Name();

		/**
		 * The meta object literal for the '{@link chrisna.impl.stateContainerImpl <em>state Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.stateContainerImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getstateContainer()
		 * @generated
		 */
		EClass STATE_CONTAINER = eINSTANCE.getstateContainer();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_CONTAINER__STATE = eINSTANCE.getstateContainer_State();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ScenContainerWithTimeImpl <em>Scen Container With Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ScenContainerWithTimeImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainerWithTime()
		 * @generated
		 */
		EClass SCEN_CONTAINER_WITH_TIME = eINSTANCE.getScenContainerWithTime();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCEN_CONTAINER_WITH_TIME__SCENCONTAINER = eINSTANCE.getScenContainerWithTime_Scencontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.initStateImpl <em>init State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.initStateImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getinitState()
		 * @generated
		 */
		EClass INIT_STATE = eINSTANCE.getinitState();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ScenContainerWithoutTimeImpl <em>Scen Container Without Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ScenContainerWithoutTimeImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainerWithoutTime()
		 * @generated
		 */
		EClass SCEN_CONTAINER_WITHOUT_TIME = eINSTANCE.getScenContainerWithoutTime();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCEN_CONTAINER_WITHOUT_TIME__SCENCONTAINER = eINSTANCE.getScenContainerWithoutTime_Scencontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.HistoryDispachImpl <em>History Dispach</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.HistoryDispachImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getHistoryDispach()
		 * @generated
		 */
		EClass HISTORY_DISPACH = eINSTANCE.getHistoryDispach();

		/**
		 * The meta object literal for the '<em><b>Scencontainerwithtime</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_DISPACH__SCENCONTAINERWITHTIME = eINSTANCE.getHistoryDispach_Scencontainerwithtime();

		/**
		 * The meta object literal for the '<em><b>Scencontainerwithouttime</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME = eINSTANCE.getHistoryDispach_Scencontainerwithouttime();

		/**
		 * The meta object literal for the '<em><b>Nodefinder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_DISPACH__NODEFINDER = eINSTANCE.getHistoryDispach_Nodefinder();

		/**
		 * The meta object literal for the '<em><b>Counter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_DISPACH__COUNTER = eINSTANCE.getHistoryDispach_Counter();

		/**
		 * The meta object literal for the '{@link chrisna.impl.FilterImpl <em>Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.FilterImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getFilter()
		 * @generated
		 */
		EClass FILTER = eINSTANCE.getFilter();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILTER__SCENCONTAINER = eINSTANCE.getFilter_Scencontainer();

		/**
		 * The meta object literal for the '<em><b>Given To When Attr</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___GIVEN_TO_WHEN_ATTR = eINSTANCE.getFilter__GivenToWhenAttr();

		/**
		 * The meta object literal for the '<em><b>Hierarchie Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___HIERARCHIE_CHECK = eINSTANCE.getFilter__HierarchieCheck();

		/**
		 * The meta object literal for the '<em><b>Ortogonal Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___ORTOGONAL_CHECK = eINSTANCE.getFilter__OrtogonalCheck();

		/**
		 * The meta object literal for the '<em><b>Sev Cond Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___SEV_COND_CHECK = eINSTANCE.getFilter__SevCondCheck();

		/**
		 * The meta object literal for the '<em><b>Parametrization Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___PARAMETRIZATION_CHECK = eINSTANCE.getFilter__ParametrizationCheck();

		/**
		 * The meta object literal for the '<em><b>Rekur Akt Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___REKUR_AKT_CHECK = eINSTANCE.getFilter__RekurAktCheck();

		/**
		 * The meta object literal for the '<em><b>Set Scens With Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___SET_SCENS_WITH_TIME = eINSTANCE.getFilter__SetScensWithTime();

		/**
		 * The meta object literal for the '<em><b>Get Scens With Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___GET_SCENS_WITH_TIME = eINSTANCE.getFilter__GetScensWithTime();

		/**
		 * The meta object literal for the '<em><b>Set Scens Without Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___SET_SCENS_WITHOUT_TIME = eINSTANCE.getFilter__SetScensWithoutTime();

		/**
		 * The meta object literal for the '<em><b>Get Scens Without Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___GET_SCENS_WITHOUT_TIME = eINSTANCE.getFilter__GetScensWithoutTime();

		/**
		 * The meta object literal for the '<em><b>Overlapp Check</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILTER___OVERLAPP_CHECK = eINSTANCE.getFilter__OverlappCheck();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ChrisnaFSMImpl <em>FSM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ChrisnaFSMImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getChrisnaFSM()
		 * @generated
		 */
		EClass CHRISNA_FSM = eINSTANCE.getChrisnaFSM();

		/**
		 * The meta object literal for the '<em><b>Initstate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHRISNA_FSM__INITSTATE = eINSTANCE.getChrisnaFSM_Initstate();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHRISNA_FSM__SCENCONTAINER = eINSTANCE.getChrisnaFSM_Scencontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.CounterImpl <em>Counter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.CounterImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getCounter()
		 * @generated
		 */
		EClass COUNTER = eINSTANCE.getCounter();

		/**
		 * The meta object literal for the '<em><b>Scencontainerwithtime</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COUNTER__SCENCONTAINERWITHTIME = eINSTANCE.getCounter_Scencontainerwithtime();

		/**
		 * The meta object literal for the '{@link chrisna.impl.WarningErrorImpl <em>Warning Error</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.WarningErrorImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getWarningError()
		 * @generated
		 */
		EClass WARNING_ERROR = eINSTANCE.getWarningError();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WARNING_ERROR__SCENCONTAINER = eINSTANCE.getWarningError_Scencontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.NodeFinderImpl <em>Node Finder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.NodeFinderImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getNodeFinder()
		 * @generated
		 */
		EClass NODE_FINDER = eINSTANCE.getNodeFinder();

		/**
		 * The meta object literal for the '<em><b>Scencontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_FINDER__SCENCONTAINER = eINSTANCE.getNodeFinder_Scencontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.AssignmentImpl <em>Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.AssignmentImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getAssignment()
		 * @generated
		 */
		EClass ASSIGNMENT = eINSTANCE.getAssignment();

		/**
		 * The meta object literal for the '<em><b>Left Side</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSIGNMENT__LEFT_SIDE = eINSTANCE.getAssignment_LeftSide();

		/**
		 * The meta object literal for the '<em><b>Right Side</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSIGNMENT__RIGHT_SIDE = eINSTANCE.getAssignment_RightSide();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT__OPERATION = eINSTANCE.getAssignment_Operation();

		/**
		 * The meta object literal for the '{@link chrisna.impl.AssigContainerImpl <em>Assig Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.AssigContainerImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getAssigContainer()
		 * @generated
		 */
		EClass ASSIG_CONTAINER = eINSTANCE.getAssigContainer();

		/**
		 * The meta object literal for the '<em><b>Assignment</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIG_CONTAINER__ASSIGNMENT = eINSTANCE.getAssigContainer_Assignment();

		/**
		 * The meta object literal for the '{@link chrisna.impl.SendEventImpl <em>Send Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.SendEventImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getSendEvent()
		 * @generated
		 */
		EClass SEND_EVENT = eINSTANCE.getSendEvent();

		/**
		 * The meta object literal for the '<em><b>Method</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEND_EVENT__METHOD = eINSTANCE.getSendEvent_Method();

		/**
		 * The meta object literal for the '<em><b>Assigcontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEND_EVENT__ASSIGCONTAINER = eINSTANCE.getSendEvent_Assigcontainer();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEND_EVENT__EVENT = eINSTANCE.getSendEvent_Event();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEND_EVENT__TIME = eINSTANCE.getSendEvent_Time();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SEND_EVENT__TRIGGER = eINSTANCE.getSendEvent_Trigger();

		/**
		 * The meta object literal for the '{@link chrisna.impl.TimeImpl <em>Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.TimeImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getTime()
		 * @generated
		 */
		EClass TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__TYPE = eINSTANCE.getTime_Type();

		/**
		 * The meta object literal for the '{@link chrisna.impl.OnChangeImpl <em>On Change</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.OnChangeImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getOnChange()
		 * @generated
		 */
		EClass ON_CHANGE = eINSTANCE.getOnChange();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ON_CHANGE__NAME = eINSTANCE.getOnChange_Name();

		/**
		 * The meta object literal for the '{@link chrisna.impl.PeriodicallyImpl <em>Periodically</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.PeriodicallyImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getPeriodically()
		 * @generated
		 */
		EClass PERIODICALLY = eINSTANCE.getPeriodically();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODICALLY__TIME = eINSTANCE.getPeriodically_Time();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODICALLY__UNIT = eINSTANCE.getPeriodically_Unit();

		/**
		 * The meta object literal for the '{@link chrisna.impl.TimeWindowImpl <em>Time Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.TimeWindowImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getTimeWindow()
		 * @generated
		 */
		EClass TIME_WINDOW = eINSTANCE.getTimeWindow();

		/**
		 * The meta object literal for the '<em><b>Assigcontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIME_WINDOW__ASSIGCONTAINER = eINSTANCE.getTimeWindow_Assigcontainer();

		/**
		 * The meta object literal for the '{@link chrisna.impl.EventContainerImpl <em>Event Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.EventContainerImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getEventContainer()
		 * @generated
		 */
		EClass EVENT_CONTAINER = eINSTANCE.getEventContainer();

		/**
		 * The meta object literal for the '<em><b>Sendevent</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_CONTAINER__SENDEVENT = eINSTANCE.getEventContainer_Sendevent();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ScenarioImpl <em>Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ScenarioImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getScenario()
		 * @generated
		 */
		EClass SCENARIO = eINSTANCE.getScenario();

		/**
		 * The meta object literal for the '<em><b>Current States</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__CURRENT_STATES = eINSTANCE.getScenario_CurrentStates();

		/**
		 * The meta object literal for the '<em><b>Next States</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__NEXT_STATES = eINSTANCE.getScenario_NextStates();

		/**
		 * The meta object literal for the '<em><b>Condcontainer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__CONDCONTAINER = eINSTANCE.getScenario_Condcontainer();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__NAME = eINSTANCE.getScenario_Name();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ConditionImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__DESCRIPTION = eINSTANCE.getCondition_Description();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION__ACTION = eINSTANCE.getCondition_Action();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__NAME = eINSTANCE.getCondition_Name();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION__TO = eINSTANCE.getCondition_To();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION__FROM = eINSTANCE.getCondition_From();

		/**
		 * The meta object literal for the '{@link chrisna.impl.CondContainerImpl <em>Cond Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.CondContainerImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getCondContainer()
		 * @generated
		 */
		EClass COND_CONTAINER = eINSTANCE.getCondContainer();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COND_CONTAINER__CONDITION = eINSTANCE.getCondContainer_Condition();

		/**
		 * The meta object literal for the '{@link chrisna.impl.ScenContainerImpl <em>Scen Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.impl.ScenContainerImpl
		 * @see chrisna.impl.ChrisnaPackageImpl#getScenContainer()
		 * @generated
		 */
		EClass SCEN_CONTAINER = eINSTANCE.getScenContainer();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCEN_CONTAINER__SCENARIO = eINSTANCE.getScenContainer_Scenario();

		/**
		 * The meta object literal for the '{@link chrisna.Trigger <em>Trigger</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chrisna.Trigger
		 * @see chrisna.impl.ChrisnaPackageImpl#getTrigger()
		 * @generated
		 */
		EEnum TRIGGER = eINSTANCE.getTrigger();

	}

} //ChrisnaPackage
