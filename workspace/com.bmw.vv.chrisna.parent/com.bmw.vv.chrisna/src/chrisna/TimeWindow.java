/**
 */
package chrisna;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time Window</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.TimeWindow#getAssigcontainer <em>Assigcontainer</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getTimeWindow()
 * @model
 * @generated
 */
public interface TimeWindow extends Periodically {
	/**
	 * Returns the value of the '<em><b>Assigcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assigcontainer</em>' reference.
	 * @see #setAssigcontainer(AssigContainer)
	 * @see chrisna.ChrisnaPackage#getTimeWindow_Assigcontainer()
	 * @model
	 * @generated
	 */
	AssigContainer getAssigcontainer();

	/**
	 * Sets the value of the '{@link chrisna.TimeWindow#getAssigcontainer <em>Assigcontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assigcontainer</em>' reference.
	 * @see #getAssigcontainer()
	 * @generated
	 */
	void setAssigcontainer(AssigContainer value);

} // TimeWindow
