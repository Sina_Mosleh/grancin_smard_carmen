/**
 */
package chrisna;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cond Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.CondContainer#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getCondContainer()
 * @model
 * @generated
 */
public interface CondContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference list.
	 * The list contents are of type {@link chrisna.Condition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference list.
	 * @see chrisna.ChrisnaPackage#getCondContainer_Condition()
	 * @model containment="true"
	 * @generated
	 */
	EList<Condition> getCondition();

} // CondContainer
