/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scen Container Without Time</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.ScenContainerWithoutTime#getScencontainer <em>Scencontainer</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getScenContainerWithoutTime()
 * @model
 * @generated
 */
public interface ScenContainerWithoutTime extends EObject {
	/**
	 * Returns the value of the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainer</em>' reference.
	 * @see #setScencontainer(ScenContainer)
	 * @see chrisna.ChrisnaPackage#getScenContainerWithoutTime_Scencontainer()
	 * @model
	 * @generated
	 */
	ScenContainer getScencontainer();

	/**
	 * Sets the value of the '{@link chrisna.ScenContainerWithoutTime#getScencontainer <em>Scencontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainer</em>' reference.
	 * @see #getScencontainer()
	 * @generated
	 */
	void setScencontainer(ScenContainer value);

} // ScenContainerWithoutTime
