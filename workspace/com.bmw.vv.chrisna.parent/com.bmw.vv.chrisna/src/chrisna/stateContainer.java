/**
 */
package chrisna;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>state Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.stateContainer#getState <em>State</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getstateContainer()
 * @model
 * @generated
 */
public interface stateContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>State</b></em>' containment reference list.
	 * The list contents are of type {@link chrisna.state}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State</em>' containment reference list.
	 * @see chrisna.ChrisnaPackage#getstateContainer_State()
	 * @model containment="true"
	 * @generated
	 */
	EList<state> getState();

} // stateContainer
