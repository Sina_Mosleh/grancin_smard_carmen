/**
 */
package chrisna.util;

import chrisna.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see chrisna.ChrisnaPackage
 * @generated
 */
public class ChrisnaAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ChrisnaPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChrisnaAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ChrisnaPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChrisnaSwitch<Adapter> modelSwitch =
		new ChrisnaSwitch<Adapter>() {
			@Override
			public Adapter caseOperation(Operation object) {
				return createOperationAdapter();
			}
			@Override
			public Adapter caseaction(action object) {
				return createactionAdapter();
			}
			@Override
			public Adapter casestate(state object) {
				return createstateAdapter();
			}
			@Override
			public Adapter casestateContainer(stateContainer object) {
				return createstateContainerAdapter();
			}
			@Override
			public Adapter caseScenContainerWithTime(ScenContainerWithTime object) {
				return createScenContainerWithTimeAdapter();
			}
			@Override
			public Adapter caseinitState(initState object) {
				return createinitStateAdapter();
			}
			@Override
			public Adapter caseScenContainerWithoutTime(ScenContainerWithoutTime object) {
				return createScenContainerWithoutTimeAdapter();
			}
			@Override
			public Adapter caseHistoryDispach(HistoryDispach object) {
				return createHistoryDispachAdapter();
			}
			@Override
			public Adapter caseFilter(Filter object) {
				return createFilterAdapter();
			}
			@Override
			public Adapter caseChrisnaFSM(ChrisnaFSM object) {
				return createChrisnaFSMAdapter();
			}
			@Override
			public Adapter caseCounter(Counter object) {
				return createCounterAdapter();
			}
			@Override
			public Adapter caseWarningError(WarningError object) {
				return createWarningErrorAdapter();
			}
			@Override
			public Adapter caseNodeFinder(NodeFinder object) {
				return createNodeFinderAdapter();
			}
			@Override
			public Adapter caseAssignment(Assignment object) {
				return createAssignmentAdapter();
			}
			@Override
			public Adapter caseAssigContainer(AssigContainer object) {
				return createAssigContainerAdapter();
			}
			@Override
			public Adapter caseSendEvent(SendEvent object) {
				return createSendEventAdapter();
			}
			@Override
			public Adapter caseTime(Time object) {
				return createTimeAdapter();
			}
			@Override
			public Adapter caseOnChange(OnChange object) {
				return createOnChangeAdapter();
			}
			@Override
			public Adapter casePeriodically(Periodically object) {
				return createPeriodicallyAdapter();
			}
			@Override
			public Adapter caseTimeWindow(TimeWindow object) {
				return createTimeWindowAdapter();
			}
			@Override
			public Adapter caseEventContainer(EventContainer object) {
				return createEventContainerAdapter();
			}
			@Override
			public Adapter caseScenario(Scenario object) {
				return createScenarioAdapter();
			}
			@Override
			public Adapter caseCondition(Condition object) {
				return createConditionAdapter();
			}
			@Override
			public Adapter caseCondContainer(CondContainer object) {
				return createCondContainerAdapter();
			}
			@Override
			public Adapter caseScenContainer(ScenContainer object) {
				return createScenContainerAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.action <em>action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.action
	 * @generated
	 */
	public Adapter createactionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.state <em>state</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.state
	 * @generated
	 */
	public Adapter createstateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.stateContainer <em>state Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.stateContainer
	 * @generated
	 */
	public Adapter createstateContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.ScenContainerWithTime <em>Scen Container With Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.ScenContainerWithTime
	 * @generated
	 */
	public Adapter createScenContainerWithTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.initState <em>init State</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.initState
	 * @generated
	 */
	public Adapter createinitStateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.ScenContainerWithoutTime <em>Scen Container Without Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.ScenContainerWithoutTime
	 * @generated
	 */
	public Adapter createScenContainerWithoutTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.HistoryDispach <em>History Dispach</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.HistoryDispach
	 * @generated
	 */
	public Adapter createHistoryDispachAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Filter
	 * @generated
	 */
	public Adapter createFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.ChrisnaFSM <em>FSM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.ChrisnaFSM
	 * @generated
	 */
	public Adapter createChrisnaFSMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Counter <em>Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Counter
	 * @generated
	 */
	public Adapter createCounterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.WarningError <em>Warning Error</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.WarningError
	 * @generated
	 */
	public Adapter createWarningErrorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.NodeFinder <em>Node Finder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.NodeFinder
	 * @generated
	 */
	public Adapter createNodeFinderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Assignment <em>Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Assignment
	 * @generated
	 */
	public Adapter createAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.AssigContainer <em>Assig Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.AssigContainer
	 * @generated
	 */
	public Adapter createAssigContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.SendEvent <em>Send Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.SendEvent
	 * @generated
	 */
	public Adapter createSendEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Time
	 * @generated
	 */
	public Adapter createTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.OnChange <em>On Change</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.OnChange
	 * @generated
	 */
	public Adapter createOnChangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Periodically <em>Periodically</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Periodically
	 * @generated
	 */
	public Adapter createPeriodicallyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.TimeWindow <em>Time Window</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.TimeWindow
	 * @generated
	 */
	public Adapter createTimeWindowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.EventContainer <em>Event Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.EventContainer
	 * @generated
	 */
	public Adapter createEventContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Scenario
	 * @generated
	 */
	public Adapter createScenarioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.Condition
	 * @generated
	 */
	public Adapter createConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.CondContainer <em>Cond Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.CondContainer
	 * @generated
	 */
	public Adapter createCondContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chrisna.ScenContainer <em>Scen Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chrisna.ScenContainer
	 * @generated
	 */
	public Adapter createScenContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ChrisnaAdapterFactory
