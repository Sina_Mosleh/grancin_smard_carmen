/**
 */
package chrisna;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scen Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.ScenContainer#getScenario <em>Scenario</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getScenContainer()
 * @model
 * @generated
 */
public interface ScenContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Scenario</b></em>' containment reference list.
	 * The list contents are of type {@link chrisna.Scenario}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario</em>' containment reference list.
	 * @see chrisna.ChrisnaPackage#getScenContainer_Scenario()
	 * @model containment="true"
	 * @generated
	 */
	EList<Scenario> getScenario();

} // ScenContainer
