/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.Counter;
import chrisna.ScenContainerWithTime;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Counter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.CounterImpl#getScencontainerwithtime <em>Scencontainerwithtime</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CounterImpl extends MinimalEObjectImpl.Container implements Counter {
	/**
	 * The cached value of the '{@link #getScencontainerwithtime() <em>Scencontainerwithtime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScencontainerwithtime()
	 * @generated
	 * @ordered
	 */
	protected ScenContainerWithTime scencontainerwithtime;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CounterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.COUNTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainerWithTime getScencontainerwithtime() {
		if (scencontainerwithtime != null && scencontainerwithtime.eIsProxy()) {
			InternalEObject oldScencontainerwithtime = (InternalEObject)scencontainerwithtime;
			scencontainerwithtime = (ScenContainerWithTime)eResolveProxy(oldScencontainerwithtime);
			if (scencontainerwithtime != oldScencontainerwithtime) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME, oldScencontainerwithtime, scencontainerwithtime));
			}
		}
		return scencontainerwithtime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainerWithTime basicGetScencontainerwithtime() {
		return scencontainerwithtime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setScencontainerwithtime(ScenContainerWithTime newScencontainerwithtime) {
		ScenContainerWithTime oldScencontainerwithtime = scencontainerwithtime;
		scencontainerwithtime = newScencontainerwithtime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME, oldScencontainerwithtime, scencontainerwithtime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME:
				if (resolve) return getScencontainerwithtime();
				return basicGetScencontainerwithtime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME:
				setScencontainerwithtime((ScenContainerWithTime)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME:
				setScencontainerwithtime((ScenContainerWithTime)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.COUNTER__SCENCONTAINERWITHTIME:
				return scencontainerwithtime != null;
		}
		return super.eIsSet(featureID);
	}

} //CounterImpl
