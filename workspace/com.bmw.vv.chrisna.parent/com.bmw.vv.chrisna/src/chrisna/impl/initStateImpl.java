/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.initState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>init State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class initStateImpl extends stateImpl implements initState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected initStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.INIT_STATE;
	}

} //initStateImpl
