/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.EventContainer;
import chrisna.SendEvent;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.EventContainerImpl#getSendevent <em>Sendevent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventContainerImpl extends MinimalEObjectImpl.Container implements EventContainer {
	/**
	 * The cached value of the '{@link #getSendevent() <em>Sendevent</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSendevent()
	 * @generated
	 * @ordered
	 */
	protected EList<SendEvent> sendevent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.EVENT_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SendEvent> getSendevent() {
		if (sendevent == null) {
			sendevent = new EObjectContainmentEList<SendEvent>(SendEvent.class, this, ChrisnaPackage.EVENT_CONTAINER__SENDEVENT);
		}
		return sendevent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ChrisnaPackage.EVENT_CONTAINER__SENDEVENT:
				return ((InternalEList<?>)getSendevent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.EVENT_CONTAINER__SENDEVENT:
				return getSendevent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.EVENT_CONTAINER__SENDEVENT:
				getSendevent().clear();
				getSendevent().addAll((Collection<? extends SendEvent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.EVENT_CONTAINER__SENDEVENT:
				getSendevent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.EVENT_CONTAINER__SENDEVENT:
				return sendevent != null && !sendevent.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EventContainerImpl
