/**
 *
 * $Id$
 */
package chrisna.validation;

import chrisna.action;
import chrisna.state;

import org.eclipse.emf.common.util.EList;

/**
 * A sample validator interface for {@link chrisna.Condition}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ConditionValidator {
	boolean validate();

	boolean validateDescription(EList value);
	boolean validateAction(action value);
	boolean validateName(String value);
	boolean validateTo(state value);
	boolean validateFrom(state value);
}
