/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Counter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.Counter#getScencontainerwithtime <em>Scencontainerwithtime</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getCounter()
 * @model
 * @generated
 */
public interface Counter extends EObject {
	/**
	 * Returns the value of the '<em><b>Scencontainerwithtime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainerwithtime</em>' reference.
	 * @see #setScencontainerwithtime(ScenContainerWithTime)
	 * @see chrisna.ChrisnaPackage#getCounter_Scencontainerwithtime()
	 * @model
	 * @generated
	 */
	ScenContainerWithTime getScencontainerwithtime();

	/**
	 * Sets the value of the '{@link chrisna.Counter#getScencontainerwithtime <em>Scencontainerwithtime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainerwithtime</em>' reference.
	 * @see #getScencontainerwithtime()
	 * @generated
	 */
	void setScencontainerwithtime(ScenContainerWithTime value);

} // Counter
