/**
 */
package chrisna;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assig Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.AssigContainer#getAssignment <em>Assignment</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getAssigContainer()
 * @model
 * @generated
 */
public interface AssigContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Assignment</b></em>' containment reference list.
	 * The list contents are of type {@link chrisna.Assignment}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment</em>' containment reference list.
	 * @see chrisna.ChrisnaPackage#getAssigContainer_Assignment()
	 * @model containment="true"
	 * @generated
	 */
	EList<Assignment> getAssignment();

} // AssigContainer
