package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.state
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.Condition
import chrisna.Scenario
import chrisna.Time
import com.bmw.vv.grancin.TimeWindow
import chrisna.Assignment
import chrisna.AssigContainer
import com.bmw.vv.grancin.impl.IntConstantImpl
import com.bmw.vv.grancin.impl.FloatConstantImpl
import java.util.ArrayList
import com.bmw.vv.grancin.GenericTimeConstraint
import com.bmw.vv.grancin.impl.PeriodicTimingImpl
import chrisna.Periodically
import com.bmw.vv.grancin.PeriodicTiming
import com.bmw.vv.grancin.IntConstant
import com.bmw.vv.grancin.FloatConstant
import com.bmw.vv.grancin.impl.OnChangeTimingImpl
import chrisna.OnChange
import com.bmw.vv.grancin.impl.TimeWindowImpl

class IGenerator {
	protected var ChrisnaFactoryImpl gen
	protected var ArrayList<Float> period
	
	new(){
		gen = new ChrisnaFactoryImpl()
	}
	
	def Time getTimer(TimeWindow time){
		var chrisna.TimeWindow timer = gen?.createTimeWindow
		var Assignment assign = gen?.createAssignment
		var Assignment assign_tmp = gen?.createAssignment
		var AssigContainer assigncontainer = gen?.createAssigContainer()
		(timer as chrisna.TimeWindow)?.setType("com.bmw.vv.chrisna.TimeWindow")
		switch ((time as TimeWindow)?.getValT()?.getClass){
			case (IntConstantImpl): (timer as chrisna.TimeWindow)?.setTime((((time as TimeWindow)?.getValT() as IntConstantImpl).getConst() as float))
			case FloatConstantImpl: (timer as chrisna.TimeWindow).setTime(((time as TimeWindow)?.getValT() as FloatConstantImpl).getConst())
			default: (timer as chrisna.TimeWindow)?.setTime(0)
		}
		(timer as chrisna.TimeWindow)?.setUnit((time as TimeWindow)?.getUnitT())
		if ((time as TimeWindow)?.getParLeft() !== null){
			assign?.setLeftSide((time as TimeWindow)?.getParLeft())
			assign_tmp?.setLeftSide((time as TimeWindow)?.getParLeft())
		}
		if((time as TimeWindow)?.getParRight()!== null){
			assign?.setRightSide((time as TimeWindow)?.getParRight())
			assign_tmp?.setRightSide((time as TimeWindow)?.getParRight())
		}
		if((time as TimeWindow)?.getOper() !== null){
			assign?.setOperation(gen?.createOperation)
			assign?.getOperation()?.setStr((time as TimeWindow)?.getOper())
			assign_tmp?.setOperation(gen?.createOperation)
			assign_tmp?.getOperation()?.setStr((time as TimeWindow)?.getOper())
		}
		if((time as TimeWindow)?.getParLeft() !== null && (time as TimeWindow)?.getOper() !== null)
		{
			assigncontainer?.getAssignment()?.add(assign)
		}
		
		(timer as chrisna.TimeWindow)?.setAssigcontainer(assigncontainer)
		period.add(timer?.time)
		timer
	}
	
	protected def Time getTimer(GenericTimeConstraint time){
		var Time timer = gen?.createTime()
		switch time?.getType()?.getClass{
			case PeriodicTimingImpl:
			{
				(timer as Periodically)?.setType("com.bmw.vv.chrisna.Periodically")
				if (((time?.getType as PeriodicTiming)?.getVal()).getClass == IntConstantImpl){
					(timer as Periodically)?.setTime((((time?.getType() as PeriodicTimingImpl)?.getVal() as IntConstant).getConst() as float))
				}
				if(((time?.getType as PeriodicTiming)?.getVal()).getClass == FloatConstantImpl){
						(timer as Periodically)?.setTime(((time?.getType() as PeriodicTimingImpl)?.getVal() as FloatConstant).getConst())
				}
				(timer as Periodically)?.setUnit(((time?.getType() as PeriodicTimingImpl)?.getUnitT()))
				period.add((timer as Periodically)?.time)
				//timer
			}
				
			case OnChangeTimingImpl:
			{
				(timer as OnChange)?.setType("com.bmw.vv.chrisna.OnChange")
				(timer as OnChange)?.setName((time?.getType() as OnChangeTimingImpl)?.getName()?.toString())
				//timer	
			}
				
			case (TimeWindowImpl):
			{
				getTimer(time?.getType() as TimeWindowImpl)
			}
		}
		
		timer
	}
	
	protected def state genState(state st){
		var stat = gen?.createstate
		stat.description = null
		stat.name = st.name
		stat.action = gen?.createaction
		for(event : st.action.sendevent){
			var eve = gen?.createSendEvent
			eve.method = event.method
			eve.event = event.event
			eve.assigcontainer = gen?.createAssigContainer
			for(assignment : event.assigcontainer.assignment){
				var assign = gen?.createAssignment
				assign.leftSide = assignment.leftSide
				assign.operation = gen?.createOperation
				assign.operation.str = assignment.operation.str
				assign.rightSide = assignment.rightSide
				eve.assigcontainer.assignment.add(assign)
			}
			stat.action.sendevent.add(eve)
		}
		stat
	}
	
	protected def Condition genCondition(Condition cond){
		var condition = gen?.createCondition 
		condition.description = null
		condition.name = cond.name
		condition.action = gen?.createaction
		for(event : cond.action.sendevent){
			var eve = gen?.createSendEvent
			eve.method = event.method
			eve.event = event.event
			eve.assigcontainer = gen?.createAssigContainer
			for(assignment : event.assigcontainer.assignment){
				var assign = gen?.createAssignment
				assign.leftSide = assignment.leftSide
				assign.operation = gen?.createOperation
				assign.operation.str = assignment.operation.str
				assign.rightSide = assignment.rightSide
				eve.assigcontainer.assignment.add(assign)
			}
			condition.action.sendevent.add(eve)
		}
		condition.from = cond.from
		condition.to = cond.to
		condition
	}
	
	protected def Scenario genScenario(Scenario scen){
		var scenario = gen?.createScenario
		scenario.currentStates = gen?.createstateContainer
		scenario.condcontainer = gen?.createCondContainer
		scenario.nextStates = gen?.createstateContainer
		scenario.name = scen.name
		for(st : scen.currentStates.state)
			scenario.currentStates.state.add(genState(st))
		for(st : scen.nextStates.state)
			scenario.nextStates.state.add(genState(st))
		for(con : scen.condcontainer.condition)
			scenario.condcontainer.condition.add(genCondition(con))
		scenario
	}
}