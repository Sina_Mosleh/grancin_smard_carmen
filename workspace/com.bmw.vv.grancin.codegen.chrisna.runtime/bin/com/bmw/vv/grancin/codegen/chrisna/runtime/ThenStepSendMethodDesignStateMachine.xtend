package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap
import chrisna.Periodically

class ThenStepSendMethodDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genSendMethodDesignStateMachine
	}
	
	def genSendMethodDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			/*scen?.currentStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '1'))
				expressions})))*/
			
			scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check method', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression(thenEvent?.method, '==', '1'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		if(thenEvent?.time?.type?.toLowerCase?.contains('period')){
			if((thenEvent?.time as Periodically)?.time != 0 && (thenEvent?.time as Periodically)?.unit != ''){
				statemachine?.scencontainer?.scenario.add({
					var scen = genScenario(name)
					
					scen?.currentStates?.state.add(0, genState('Ok_' + name, null))
					
					scen?.nextStates?.state.add(0, genState('Set_Period_' + thenEvent?.method + name, genEvent('set period' + name, 'Compute', {
						expressions?.clear
						expressions?.add(genExpression('Period_' + thenEvent?.method + name, '=', (thenEvent.time as Periodically)?.time.toString + ' ' + (thenEvent.time as Periodically)?.unit ?: ''))
						expressions})))
					scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
						expressions?.clear
						expressions?.add(genExpression('true_' + name, '=', 'true'))
						expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						
					scen
				})
				
				statemachine?.scencontainer?.scenario.add({
					var scen = genScenario(name)
					scen?.currentStates?.state.add(0, genState('Set_Period_' + thenEvent?.method + name, genEvent('set period' + name, 'Compute', {
						expressions?.clear
						expressions?.add(genExpression('Period_' + thenEvent?.method + name, '=', (thenEvent.time as Periodically)?.time.toString + ' ' + (thenEvent.time as Periodically)?.unit ?: ''))
						expressions})))
						
					/*scen?.nextStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
						expressions?.clear
						expressions?.add(genExpression('re_' + name, '=', '1'))
						expressions})))*/
					scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
						expressions?.clear
						expressions?.add(genExpression('Period_' + thenEvent?.method + name))
						expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						
					scen
				})
			}
		}
		else {
			statemachine?.scencontainer?.scenario.add({
				var scen = genScenario(name)
				
				scen?.currentStates?.state.add(0, genState('Ok_' + name, null))
				
				/*scen?.nextStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '1'))
					expressions})))*/
				scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
						expressions?.clear
						expressions?.add(genExpression('true_' + name, '=', 'true'))
						expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
					
				scen
			})
		}
		
	}
	
}