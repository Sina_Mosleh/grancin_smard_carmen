package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.OnChange
import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap

class ThenStepOnChangeDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genOnChangeDesignStateMachine
	}
	
	def genOnChangeDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			/*scen?.currentStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '1'))
				expressions})))*/
			
			scen?.nextStates?.state?.add(0, genState('Wait_for_change_' + name, genEvent('set pre value', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('pre_' + (thenEvent?.time as OnChange)?.name, '=', (thenEvent?.time as OnChange)?.name))
				expressions
			})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is changed', 'Not', {
				expressions?.clear
				expressions?.add(genExpression('pre_' + (thenEvent?.time as OnChange)?.name, '!=', (thenEvent?.time as OnChange)?.name))
				expressions
			}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(genState('Wait_for_change_' + name, genEvent('set pre value', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('pre_' + (thenEvent?.time as OnChange)?.name, '=', (thenEvent?.time as OnChange)?.name))
				expressions
			})))
			
			scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check method', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression(thenEvent?.method, '==', '1'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(0, genState('Ok_' + name, null))
			
			/*scen?.nextStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '1'))
				expressions})))*/
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
					expressions?.clear
					expressions?.add(genExpression('true_' + name, '=', 'true'))
					expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
	}
	
}