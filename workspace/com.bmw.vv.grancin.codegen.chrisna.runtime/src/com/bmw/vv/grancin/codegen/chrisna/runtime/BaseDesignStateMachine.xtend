package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.Assignment
import chrisna.ChrisnaFSM
import chrisna.ChrisnaFactory
import chrisna.Condition
import chrisna.SendEvent
import chrisna.Trigger
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.state
import java.util.ArrayList
import java.util.HashMap
import chrisna.Scenario
import chrisna.TimeWindow

class BaseDesignStateMachine{
	
	
	protected val ChrisnaFactory generatorFactory
	protected val ChrisnaFSM statemachine
	protected var int condcounter
	protected var int scencounter
	
	new(){
		generatorFactory = new ChrisnaFactoryImpl
		statemachine = generatorFactory?.createChrisnaFSM
		statemachine.scencontainer = generatorFactory?.createScenContainer
		statemachine.initstate = generatorFactory?.createinitState
		condcounter = 1
		scencounter = 1
	}
	
	def String searchInMap(HashMap<String, String> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def HashMap<String, String> genExpression(String leftSide, String op, String rightSide){
		var map = new HashMap<String, String>()
		map.put('leftSide', leftSide)
		map.put('rightSide', rightSide)
		map.put('operation', op)
		map
	}
	
	def HashMap<String, String> genExpression(String leftSide){
		var map = new HashMap<String, String>()
		map.put('leftSide', leftSide)
		map
	}
	
	def HashMap<String, String> genExpression(String leftSide, String op){
		var map = new HashMap<String, String>()
		map.put('leftSide', leftSide)
		map.put('operation', op)
		map
	}
	
	def Assignment genAssignment(HashMap<String, String> map){
		var assignment = generatorFactory?.createAssignment
		assignment.leftSide = searchInMap(map, 'leftSide')
		assignment.rightSide = searchInMap(map, 'rightSide')
		assignment.operation = generatorFactory.createOperation
		assignment.operation.str = searchInMap(map, 'operation')
		assignment
	}
	
	def TimeWindow genTime(float value, String timeunit){
		var time = generatorFactory?.createTimeWindow
		time?.setTime(value)
		time?.setUnit(timeunit)
		time
	}
	
	def SendEvent genEvent(String name, String method, ArrayList<HashMap<String, String>> list){
		var event = generatorFactory?.createSendEvent
		event.event = name
		event.method = method
		event.assigcontainer = generatorFactory?.createAssigContainer
		for(member : list){
			event.assigcontainer?.assignment.add(genAssignment(member))
			/*if(member.containsKey('rightSide')){
				if(member.get('rightSide').toLowerCase.contains(' ms'))
					event?.setTime(genTime(0, 'ms'))
				else if(member.get('rightSide').toLowerCase.contains(' us'))
					event?.setTime(genTime(0, 'us'))
				else if(member.get('rightSide').toLowerCase.contains(' ns'))
					event?.setTime(genTime(0, 'ns'))
				else if(member.get('rightSide').toLowerCase.contains(' s'))
					event?.setTime(genTime(0, 's'))
				else if(member.get('rightSide').toLowerCase.contains(' min'))
					event?.setTime(genTime(0, 'min'))
				else if(member.get('rightSide').toLowerCase.contains(' h'))
					event?.setTime(genTime(0, 'h'))
				else if(member.get('rightSide').toLowerCase.contains(' day'))
					event?.setTime(genTime(0, 'day'))
			}*/
		}
		event.trigger = Trigger.ON_ENTRY
		event
	}
	
	protected def state genState(state st){
		if(st !== null){
			var stat = generatorFactory?.createstate
			stat.description = null
			stat.name = st.name
			stat.action = generatorFactory?.createaction
			for(event : st.action.sendevent){
				var eve = generatorFactory?.createSendEvent
				eve.method = event?.method
				eve.event = event?.event
				eve.time = event?.time
				eve.assigcontainer = generatorFactory?.createAssigContainer
				for(assignment : event?.assigcontainer?.assignment){
					var assign = generatorFactory?.createAssignment
					assign.leftSide = assignment?.leftSide
					assign.operation = generatorFactory?.createOperation
					assign.operation.str = assignment?.operation?.str
					assign.rightSide = assignment?.rightSide
					eve.assigcontainer.assignment.add(assign)
				}
				stat.action.sendevent.add(eve)
			}
			stat
		}
		else
			null
	}
	
	def state genState(String name, SendEvent event){
		var state = generatorFactory?.createstate
		state.name = name
		//state.description.add(name)
		state.action = generatorFactory?.createaction
		if(event !== null)
			state.action?.sendevent.add(event)
		state
	}
	
	def Condition genCondition(int name, SendEvent event, state currstate, state nextstate){
		var condition = generatorFactory?.createCondition
		condition.name = 'Transition' + Math.random().toString + '_' + name
		//condition.description.add(name)
		condition.action = generatorFactory?.createaction
		if(event !== null)
			condition.action?.sendevent.add(event)
		condition.from = currstate
		condition.to = nextstate
		condcounter = condcounter + 1 
		condition
	}
	
	def Scenario genScenario(int name){
		var scen = generatorFactory?.createScenario
		scen.currentStates = generatorFactory?.createstateContainer
		scen.condcontainer = generatorFactory?.createCondContainer
		scen.nextStates = generatorFactory?.createstateContainer
		scen.name = 'scen' + scencounter + '_' + name
		scencounter = scencounter + 1
		scen
	}
	
	def insertState(int index_scenario, int index_state, int name, state toinsert, Condition condition){
		var scenario_tmp = genScenario(name)
		
		var state = genState(toinsert)
		
		val temp = genState(statemachine?.scencontainer?.scenario?.get(index_scenario)?.nextStates?.state?.get(index_state))
		
		scenario_tmp?.nextStates?.state?.add(0, temp)
		scenario_tmp?.currentStates?.state?.add(0, state)
		condition.from = state
		condition.to = temp
		scenario_tmp?.condcontainer?.condition?.add(0, condition)
		
		//statemachine?.scencontainer?.scenario?.add(scenario_tmp)
		
		//statemachine?.scencontainer?.scenario?.get(index_scenario)?.nextStates?.state?.remove(index_state)
		statemachine?.scencontainer?.scenario?.get(index_scenario)?.nextStates?.state?.set(index_state, toinsert)
		for(cond : statemachine?.scencontainer?.scenario?.get(index_scenario)?.condcontainer?.condition)
		{
			if(cond.to.name == temp.name)
				cond.to = toinsert
		}
		/*if (index_scenario > 0){
			statemachine?.scencontainer?.scenario?.get(index_scenario - 1)?.nextStates?.state?.remove(index_state)
			statemachine?.scencontainer?.scenario?.get(index_scenario - 1)?.nextStates?.state?.set(index_state, toinsert)	
		}
		condition.to = toinsert
		
		if (index_scenario > 0){
			condition.from = statemachine?.scencontainer?.scenario?.get(index_scenario - 1)?.currentStates?.state?.get(index_state)
			statemachine?.scencontainer?.scenario?.get(index_scenario - 1)?.condcontainer?.condition?.remove(index_state)
			statemachine?.scencontainer?.scenario?.get(index_scenario - 1)?.condcontainer?.condition?.set(index_state, condition)
		}*/
		condcounter = condcounter + 1
		scencounter = scencounter + 1
		
		statemachine?.scencontainer.scenario.add(0, scenario_tmp)
	}
}
