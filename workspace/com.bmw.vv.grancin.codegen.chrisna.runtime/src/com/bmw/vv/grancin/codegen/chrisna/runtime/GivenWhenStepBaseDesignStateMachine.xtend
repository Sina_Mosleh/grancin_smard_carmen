package com.bmw.vv.grancin.codegen.chrisna.runtime

import java.util.ArrayList
import java.util.HashMap
import chrisna.TimeWindow

class GivenWhenStepBaseDesignStateMachine extends BaseDesignStateMachine {
	protected val int name
	//protected val EventContainer givenEvents
	//protected val EventContainer whenEvents
	protected val ClusteringGivenWhenEvents clusters
	
	new(String scenario_name, /*EventContainer giveneves, EventContainer wheneves*/ ClusteringGivenWhenEvents clus) {
		name = scenario_name.hashCode
		//givenevents = giveneves
		//whenevents = wheneves
		clusters = clus
		genBaseDesignStateMachine
	}
	
	def genBaseDesignStateMachine(){
		 
		var expressions = new ArrayList<HashMap<String, String>>()	
		
		statemachine?.scencontainer?.scenario.add(0, {
			var scen = genScenario(name)
			
			/*scen?.currentStates?.state.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '0'))
				expressions})))*/
				
			scen?.nextStates?.state?.add(0, genState('Error_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
				expressions?.clear
				expressions?.add(genExpression('Wait_Time_'+ name ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
			
		})
		
		statemachine?.scencontainer?.scenario.add(0, {
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(0, genState('Error_' + name, null))
				
			/*scen?.nextStates?.state?.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '0'))
				expressions})))*/
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('True_'+ name, '==', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
			
		})
		
		
		/*statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			//scen?.currentStates?.state.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
			//	expressions?.clear
			//	expressions?.add(genExpression('re_' + name, '=', '0'))
			//	expressions})))
			
			scen?.nextStates?.state?.add(0, genState('Check_Loopnum_' + name, genEvent('set loop num ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '=', 'loopnum_' + name + ' + 1'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('Wait_Time_' + name, '!=', '0 ms'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
				expressions?.clear
				expressions?.add(genExpression('Wait_Time_' + name ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
		})
		
		statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			//scen?.currentStates?.state.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
			//	expressions?.clear
			//	expressions?.add(genExpression('re_' + name, '=', '0'))
			//	expressions})))
			
			scen?.currentStates?.state?.add(0, genState('QuickError_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '/ (2 ^ loop num ' + name + '))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('reset', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen?.currentStates?.state?.add(0, genState('Error_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '* (2 ^ (loop num ' + name + ' + 1))/ (2 ^ 8))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('reset', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Check_' + name, genEvent('set loop num ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '++'))
				expressions})))
			//scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name , genEvent('is equal', 'Comparison', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.nextStates?.state?.add(0, genState('QuickError_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '/ (2 ^ loopnum_' + name + '))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is smaller', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '<=', '8' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.nextStates?.state?.add(0, genState('Error_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '* (2 ^ (loopnum_' + name + ' + 1))/ (2 ^ 8))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is bigger', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '>', '8' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is smaller', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '<=', '20' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen?.nextStates?.state?.add(0, genState('Error_after_a_while_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is bigger', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '>', '20' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})*/
		
		
		if(clusters?.givenInmethodReproductEvents?.getSendevent.length != 0) {
			insertState(statemachine?.scencontainer?.scenario.length-1, 0, name, genState('Given_InMethod_Reproduction_Satisfied_' + name, 
				genEvent('set re value', 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '0'))
					expressions
			})), genCondition(name, genEvent('check values', 'Comparison', {
				expressions?.clear
				for(event : clusters.givenInmethodReproductEvents.getSendevent)
					expressions?.add(genExpression(event.method, '==', '1'))
				for(event : clusters.givenAttributesEvents.getSendevent)
						for(assign: event.getAssigcontainer.getAssignment)
							expressions?.add(genExpression(assign.leftSide, assign.operation.str, assign.rightSide))
				expressions
			}), null, null))	
			
			statemachine?.scencontainer?.scenario.add(0, {
				var scen = genScenario(name)
				
				scen?.currentStates?.state.add(0, genState('Given_InMethod_Reproduction_Satisfied_' + name, genEvent('set re value', 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '0'))
					expressions
				})))
					
				/*scen?.nextStates?.state?.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '0'))
					expressions})))/*/
				scen?.nextStates?.state?.add(0, statemachine?.scencontainer?.scenario?.head?.nextStates?.state?.head)
				//scen?.nextStates?.state?.add(0, statemachine?.scencontainer?.scenario?.last?.nextStates?.state?.head)
				
				scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check given attributes', 'Not', {
					expressions?.clear
					for(event : clusters?.givenAttributesEvents?.getSendevent)
						for(assign: event?.getAssigcontainer?.getAssignment)
							expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
					expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
				scen
				
			})
		}
		
		
		if(clusters?.whenInmethodReproductEvents?.getSendevent.length != 0 || clusters?.givenAttributesEvents?.getSendevent.length != 0 || 
			clusters?.whenAttributesEvents?.getSendevent.length != 0){
			insertState(statemachine?.scencontainer?.scenario.length-1, 0, name, genState('Given_When_InMethod_Reproduction_Attributes_Satisfied_' + name, 
				genEvent('set re value', 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '0'))
					expressions
			})), genCondition(name, genEvent('check values', 'Comparison', {
				expressions?.clear
				for(event : clusters?.givenWaitEvents?.getSendevent)
					expressions?.add(genExpression('period_'+ name , '=', (event.time as TimeWindow)?.time.toString + ' ' + (event.time as TimeWindow)?.unit ?: ''))
				for(event : clusters?.whenInmethodReproductEvents?.getSendevent)
					expressions?.add(genExpression(event.method ?: '', '==', '1'))
				for(event : clusters?.givenAttributesEvents?.getSendevent)
					for(assign: event?.getAssigcontainer?.getAssignment)
						expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
				for(event : clusters.whenAttributesEvents.getSendevent)
					for(assign: event.getAssigcontainer.getAssignment)
						expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
				expressions
			}), null, null))
			
			statemachine?.scencontainer?.scenario.add(0, {
				var scen = genScenario(name)
				
				scen.currentStates.state.add(0, genState('Given_When_InMethod_Reproduction_Attributes_Satisfied_' + name, 
					genEvent('set re value', 'Compute', {
						expressions?.clear
						expressions?.add(genExpression('re_' + name, '=', '0'))
						expressions
				})))
					
				/*scen?.nextStates?.state?.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '0'))
					expressions})))/*/
				//scen?.nextStates?.state?.add(0, statemachine?.scencontainer?.scenario?.last?.nextStates?.state?.head)
				scen?.nextStates?.state?.add(0, statemachine?.scencontainer?.scenario?.head?.nextStates?.state?.head)
				
				scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check given and when attributes', 'Not', {
					expressions?.clear
					for(event : clusters?.givenAttributesEvents?.getSendevent)
						for(assign: event?.getAssigcontainer?.getAssignment)
							expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
					for(event : clusters?.whenAttributesEvents?.getSendevent)
						for(assign: event?.getAssigcontainer?.getAssignment)
							expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
					expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
				scen
				
			})
		}
		
		if(clusters?.whenWaitEvents?.getSendevent.length != 0){
			insertState(statemachine?.scencontainer?.scenario.length-1, 0, name, genState('Given_When_Satisfied_' + name, 
				genEvent('set re value', 'Compute', {
					expressions?.clear
					expressions?.add(genExpression('re_' + name, '=', '1'))
					expressions
			})), genCondition(name, genEvent('wait', 'Timing', {
				expressions?.clear
				for(event : clusters?.whenWaitEvents?.getSendevent)
					expressions?.add(genExpression('period_'+ name , '=', (event.time as TimeWindow)?.time.toString + ' ' + (event.time as TimeWindow)?.unit ?: ''))
				expressions
			}), null, null))
		}
	}
}