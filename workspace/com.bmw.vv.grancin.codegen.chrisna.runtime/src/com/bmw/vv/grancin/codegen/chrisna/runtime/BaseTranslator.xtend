package com.bmw.vv.grancin.codegen.chrisna.runtime

import com.bmw.vv.grancin.Payload
import chrisna.AssigContainer
import java.util.HashMap
import chrisna.Assignment
import org.franca.core.franca.FTypeCollection
import org.franca.core.franca.FInterface
import com.bmw.vv.grancin.impl.IntConstantImpl
import com.bmw.vv.grancin.Constant
import com.bmw.vv.grancin.impl.FloatConstantImpl
import com.bmw.vv.grancin.impl.BoolConstantImpl
import com.bmw.vv.grancin.impl.StringConstantImpl
import com.bmw.vv.grancin.impl.NotANumberImpl
import com.bmw.vv.grancin.Reference
import com.bmw.vv.grancin.ReferenceID
import static extension org.eclipse.xtext.EcoreUtil2.*
import chrisna.Trigger
import com.bmw.vv.grancin.Attribute
import chrisna.SendEvent
import com.bmw.vv.grancin.impl.AttributeImpl
import com.bmw.vv.grancin.impl.SendMethodImpl
import org.franca.core.franca.impl.FModelElementImpl
import com.bmw.vv.grancin.SendNoMethod
import com.bmw.vv.grancin.impl.SendNoMethodImpl
import com.bmw.vv.grancin.StepOutBody
import com.bmw.vv.grancin.FloatConstant
import com.bmw.vv.grancin.IntConstant
import com.bmw.vv.grancin.impl.WaitImpl
import com.bmw.vv.grancin.impl.ContinuationImpl
import com.bmw.vv.grancin.InMethod
import com.bmw.vv.grancin.impl.InMethodImpl
import com.bmw.vv.grancin.StepInBody

class BaseTranslator extends IGenerator {
	
	protected var String interfacename
	protected var String version
	
	protected var boolean hasGiven = false
	protected var boolean hasWhen = false
	protected var boolean hasThen = false
	
	new(){
		super()
		interfacename = ''
		version = '0'
	}
	
	protected def void PayLoadTranslation(Payload payload, AssigContainer assigncontainer, HashMap<String, String> map){
		payload?.getField()?.forEach[
			var Assignment assign = gen?.createAssignment
			var Assignment assign_tmp = gen?.createAssignment
			assign?.setLeftSide((it?.getName?.getName ?: '') + 
				if(it?.getHasMembers?.getName?.getName !== null)
					'.' + it?.getHasMembers?.getName?.getName
				else
					"")
			assign?.setRightSide(ReferenceTranslation(it?.getVal(), map))
			assign?.setOperation(gen?.createOperation)
			assign?.getOperation()?.setStr(it?.getOper())
			
			assign_tmp?.setLeftSide((it?.getName?.getName ?: '') + 
				if(it?.getHasMembers?.getName?.getName !== null)
					'.' + it?.getHasMembers?.getName?.getName
				else
					"")
			assign_tmp?.setRightSide(ReferenceTranslation(it?.getVal(), map) ?: '')
			assign_tmp?.setOperation(gen?.createOperation)
			assign_tmp?.getOperation()?.setStr(it?.getOper() ?: '')
			
			assigncontainer?.getAssignment()?.add(assign)
			
			interfacename = (it?.name?.getContainerOfType(FTypeCollection) as FInterface)?.name ?: ''
			version = (it?.name?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString ?: ''
		]
	}
	
	protected def String ConstantTranslation(Constant const){
		switch const?.getType?.getClass {
				case IntConstantImpl: (const?.getType as IntConstantImpl).getConst.toString
				case FloatConstantImpl: (const?.getType as FloatConstantImpl).getConst.toString
				case BoolConstantImpl: (const?.getType as BoolConstantImpl).isConst.toString
				case StringConstantImpl: (const?.getType as StringConstantImpl)?.getConst
				case NotANumberImpl: (const?.getType as NotANumberImpl)?.getConst
				default: 'None'
			}
	}
	
	protected def String ReferenceTranslation(Reference ref, HashMap<String, String> map){
		if(ref?.getConst !== null)
		{
			ConstantTranslation(ref?.getConst)
		}
		else if(ref?.getRefe !== null){
			ReferenceidTranslation(ref.getRefe, map)
			//ref?.getRefe?.getRef
		}
		else
			'None'
	}
	
	protected def String ReferenceidTranslation(ReferenceID ref, HashMap<String , String> map){
		if(map.containsKey(ref?.getRef))
				map.get(ref?.getRef)
	}
	
	protected def SendEvent AttributeTranslation(Attribute attribute, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
		var Assignment assign = gen?.createAssignment
		var AssigContainer assigncontainer = gen?.createAssigContainer
		event?.setEvent('attribute')
		
		if (attribute?.getAttribute?.getIdx?.getInt?.const !== 0){
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + attribute?.getAttribute?.getIdx?.getInt.const.toString + "]" +
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				("." + attribute?.getAttribute?.getField?.getName?.getName)
			else
				"" )
		}
		else if(attribute?.getAttribute?.getIdx?.getRef !== null){
			val value = ReferenceidTranslation(attribute?.getAttribute?.getIdx?.getRef, map)
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + value + "]" + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				"." + attribute?.getAttribute?.getField?.getName?.getName
			else
				"" )
		}
		else {
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null)
				'.' + attribute?.getAttribute?.getField?.getName?.getName
			else
				"")
		}	
		assign?.setRightSide(ReferenceTranslation(attribute?.getPayload(), map))
		assign?.setOperation(gen?.createOperation)
		assign?.getOperation()?.setStr(attribute?.getEvent)
		assigncontainer?.getAssignment()?.add(assign)
		
		event?.setAssigcontainer(assigncontainer)
		event?.setMethod("")
		event?.setTime(getTimer((attribute)?.getTw()))
		
		event?.setTrigger(Trigger.ON_ENTRY)
		
		interfacename = (attribute?.attribute?.name?.getContainerOfType(FTypeCollection) as FInterface)?.name ?: ''
		version = (attribute?.attribute?.name?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString ?: '0'
		
		event
	}
	
	protected def SendEvent StepInBodyTranslation(StepInBody stepinbody, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
		if (stepinbody?.getStub() !== null)
		{
			if(stepinbody?.getStub().toLowerCase.contains('no') && stepinbody?.getStub().toLowerCase.contains('precondition')){
				event?.setEvent('no precondition')
				event?.setMethod('')
				event?.setAssigcontainer(gen?.createAssigContainer)
				hasGiven = false
			}
			if(stepinbody?.getStub().toLowerCase.contains('no') && stepinbody?.getStub().toLowerCase.contains('trigger')){
				event?.setEvent('no trigger')
				event?.setMethod('')
				event?.setAssigcontainer(gen?.createAssigContainer)
				hasWhen = false
			}
		}
		else{
			switch stepinbody?.getType()?.getClass {
				case InMethodImpl:
				{
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					event?.setEvent("in method")
					event?.setMethod(((stepinbody?.getType() as InMethodImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					//interfacename = ((stepinbody?.getType as InMethodImpl)?.getMethod?.getContainerOfType(FTypeCollection) as FInterface)?.name
					//version = ((stepinbody?.getType as InMethodImpl)?.getMethod?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString
					
					PayLoadTranslation((stepinbody?.getType() as InMethodImpl)?.getPayload(), assigncontainer, map)
					event?.setAssigcontainer(assigncontainer)
					event?.setTime(getTimer((stepinbody?.getType() as InMethod)?.getTw()))
					
					event?.setTrigger(Trigger.ON_ENTRY)
				}
				case AttributeImpl:
					event = AttributeTranslation(stepinbody?.getType() as AttributeImpl, map)
					
				//case Reproduction:
				
				case ContinuationImpl:
				{
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					event?.setEvent("continuation")
					event?.setMethod(((stepinbody?.getType() as ContinuationImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					//interfacename = ((stepinbody?.type as ContinuationImpl)?.method?.getContainerOfType(FTypeCollection) as FInterface)?.name ?: ''
					//version = ((stepinbody?.type as ContinuationImpl)?.method?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString ?: '0'
					
					PayLoadTranslation((stepinbody?.getType() as ContinuationImpl)?.getPayload(), assigncontainer, map)
					event?.setAssigcontainer(assigncontainer)
					event?.setTrigger(Trigger.ON_ENTRY)
				}
					
				case WaitImpl:
				{
					var chrisna.TimeWindow timer = gen?.createTimeWindow
					event?.setEvent("wait")
					
					switch ((stepinbody?.getType() as WaitImpl)?.getVal()){
						case IntConstant: (timer as chrisna.TimeWindow)?.setTime((((stepinbody?.getType() as WaitImpl)?.getVal() as IntConstant).getConst() as float))
						case FloatConstant: (timer as chrisna.TimeWindow)?.setTime(((stepinbody?.getType() as WaitImpl)?.getVal() as FloatConstant).getConst())
						default: (timer as chrisna.TimeWindow)?.setTime(0)
					}
					(timer as chrisna.TimeWindow)?.setType('com.bmw.vv.chrisna.TimeWindow')
					(timer as chrisna.TimeWindow)?.setUnit((stepinbody?.getType() as WaitImpl)?.getUnit())
					
					period.add(timer?.time)
					
					event?.setTime(timer)
					event?.setTrigger(Trigger.ON_ENTRY)
				}
					
			}
		}
		event
	}
	
	protected def SendEvent StepOutBodyTranslation(StepOutBody stepoutbody, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
			switch stepoutbody?.getType()?.getClass {
				case SendNoMethodImpl:
				{
					event?.setEvent("do not send")
					event?.setMethod(((stepoutbody?.getType() as SendNoMethod)?.getMethod() as FModelElementImpl)?.getName)
					
					interfacename = (((stepoutbody?.type as SendNoMethod)?.method)?.getContainerOfType(FTypeCollection) as FInterface)?.name
					version = ((stepoutbody?.type as SendNoMethod)?.method?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString
					
					event?.setTrigger(Trigger.ON_ENTRY)
				}
					
				case SendMethodImpl:
				{
					event?.setEvent("do send")
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					PayLoadTranslation((stepoutbody?.getType() as SendMethodImpl)?.getPayload(),assigncontainer, map)
					
					event?.setAssigcontainer(assigncontainer)
					event?.setMethod(((stepoutbody?.getType() as SendMethodImpl)?.getMethod() as FModelElementImpl)?.getName ?: '')
					event?.setTime(getTimer((stepoutbody?.getType() as SendMethodImpl)?.getTc()))
					
					//interfacename = ((stepoutbody?.type as SendMethodImpl)?.method?.getContainerOfType(FTypeCollection) as FInterface)?.name ?: ''
					//version = ((stepoutbody?.type as SendMethodImpl)?.method?.getContainerOfType(FTypeCollection) as FInterface)?.version?.major?.toString ?: '0'
					
					event?.setTrigger(Trigger.ON_ENTRY)
				}
					
				case AttributeImpl:
					event = AttributeTranslation(stepoutbody?.getType() as Attribute, map)
			}
		event
	}
	
}