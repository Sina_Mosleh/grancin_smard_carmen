package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import de.bmw.smard.modeller.conditions.DecodeStrategy
import de.bmw.smard.modeller.conditions.EmptyDecodeStrategy
import de.bmw.smard.modeller.conditions.DoubleDecodeStrategy
import de.bmw.smard.modeller.conditions.StringDecodeStrategy
import java.util.HashMap
import java.io.Serializable

class DecodeStrategyCreator {
	
	protected var ConditionsFactoryImpl conditions
	protected var DecodeType dectyp
	
	new (DecodeType dec){
		conditions = new ConditionsFactoryImpl
		dectyp = dec
	}
	
	def DecodeType getDectyp(){
		dectyp
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map?.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def DecodeStrategy createDecodeStrategy(DecodeStrategy decode){
		decode
	}
	
	def EmptyDecodeStrategy createEmptyDecodeStrategy(){
		var emptydecodestrategy = conditions?.createEmptyDecodeStrategy()
		emptydecodestrategy = createDecodeStrategy(emptydecodestrategy) as EmptyDecodeStrategy
		emptydecodestrategy
	}
	
	def DoubleDecodeStrategy createDoubleDecodeStrategy(HashMap<String, Serializable> map) {
		var doubledecodestrategy = conditions?.createDoubleDecodeStrategy()
		doubledecodestrategy = createDecodeStrategy(doubledecodestrategy) as DoubleDecodeStrategy
		doubledecodestrategy?.setFactor(searchInMap(map, 'factor'))
		doubledecodestrategy?.setOffset(searchInMap(map, 'offset'))
		doubledecodestrategy
	}
	
	def StringDecodeStrategy createStringDecodeStrategy() {
		var stringdecodestrategy = conditions?.createStringDecodeStrategy()
		stringdecodestrategy = createDecodeStrategy(stringdecodestrategy) as StringDecodeStrategy
		//stringdecodestrategy.stringTermination = ''
		//stringdecodestrategy.stringTerminationTmplParam = ''
		stringdecodestrategy
	}
}