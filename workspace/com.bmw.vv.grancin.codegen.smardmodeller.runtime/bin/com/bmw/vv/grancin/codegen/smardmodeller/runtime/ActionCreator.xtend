package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import chrisna.Assignment
import de.bmw.smard.modeller.conditions.AbstractObserver
//import conditions.AbstractObserver
import de.bmw.smard.modeller.conditions.ValueVariable
//import conditions.ValueVariable
import de.bmw.smard.modeller.statemachine.AbstractAction
//import statemachine.AbstractAction
import de.bmw.smard.modeller.statemachine.Action
//import statemachine.Action
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
//import statemachine.ActionTypeNotEmpty
import de.bmw.smard.modeller.statemachine.ComputeVariable
//import statemachine.ComputeVariable
import de.bmw.smard.modeller.statemachine.ControlAction
//import statemachine.ControlAction
import de.bmw.smard.modeller.statemachine.ControlType
//import statemachine.ControlType
import de.bmw.smard.modeller.statemachine.ShowVariable
//import statemachine.ShowVariable
import de.bmw.smard.modeller.statemachine.Trigger
//import statemachine.Trigger
import de.bmw.smard.modeller.statemachine.impl.StatemachineFactoryImpl
//import statemachine.impl.StatemachineFactoryImpl
import java.util.UUID
import org.eclipse.emf.common.util.EList

class ActionCreator {
	
	protected var StatemachineFactoryImpl statemachine
	
	new (){
		//statemachine = new StatemachineFactoryImpl
		statemachine = new StatemachineFactoryImpl
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def Trigger convertTriggerToTrigger(chrisna.Trigger trigger){
		if (trigger == chrisna.Trigger.ON_ENTRY)
			Trigger.ON_ENTRY
		else if(trigger == chrisna.Trigger.ON_EXIT)
			Trigger.ON_EXIT
	}
	
	def AbstractAction createAbstractAction(AbstractAction action, Assignment assignment, chrisna.Trigger trigger){
		//action?.setId(createUUID)
		//action.readVariables.add()
		//action.writeVariables.add()
		action?.setTrigger(convertTriggerToTrigger(trigger))
		action?.setDescription(
				//event.event?: '' + ' ' + event.method?: '' + [
				//for(assignment : event.assigcontainer.assignment){
					(assignment?.leftSide?: '') + (assignment?.operation?.str?: '') + (assignment?.rightSide?: '')
				//}]
		)
		action
	}
	
	def Action createAct(Assignment assignment, chrisna.Trigger trigger, ActionTypeNotEmpty typ){
		//var action =  new ActionImpl();
		var action = statemachine.createAction
		action = createAbstractAction(action, assignment, trigger) as Action
		action?.setActionType(typ)
		action?.setActionTypeTmplParam(typ.toString)
		action?.setActionExpression(
			//for(assignment : event.assigcontainer.assignment){
				(assignment?.leftSide?: '') + (assignment?.operation?.str?: '') + (assignment?.rightSide?: '')
			//}
		)
		action
	}
	
	def ComputeVariable createComputeVar(Assignment assignment, chrisna.Trigger trigger, ValueVariable valvar){
		var computevariable = statemachine.createComputeVariable
		computevariable = createAbstractAction(computevariable, assignment, trigger) as ComputeVariable
		computevariable?.setActionExpression(
			//for(assignment : event.assigcontainer.assignment){
				(assignment?.leftSide?: '') + (assignment?.operation?.str?: '') + (assignment?.rightSide?: '')
			//}
			)
		computevariable?.setTarget(valvar)
		//computevariable.operands.add()
		computevariable
	}
	
	def ShowVariable createShowVar(Assignment assignment, chrisna.Trigger trigger){
		var showvariable = statemachine.createShowVariable
		showvariable = createAbstractAction(showvariable, assignment, trigger) as ShowVariable
		showvariable
	}
	
	def ControlAction createControlAct(Assignment assignment, chrisna.Trigger trigger, EList<AbstractObserver> observers, ControlType controltyp){
		var controlaction = statemachine.createControlAction
		controlaction = createAbstractAction(controlaction, assignment, trigger) as ControlAction
		controlaction?.setControlType(controltyp)
		for(observer: observers){
			controlaction?.observers.add(observer)
		}
		//controlaction.stateMachines.add()
		controlaction
	}
}