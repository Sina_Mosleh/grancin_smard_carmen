package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.ExtractStrategy
import de.bmw.smard.modeller.conditions.EmptyExtractStrategy
import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import de.bmw.smard.modeller.conditions.PluginResultExtractStrategy
import de.bmw.smard.modeller.conditions.PluginStateExtractStrategy
import de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy
import java.util.HashMap
import java.io.Serializable
import de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy
import de.bmw.smard.modeller.conditions.AUTOSARDataType
import java.util.List
import java.util.Arrays
import de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy

class ExtractStrategyCreator {
	
	protected var ConditionsFactoryImpl conditions
	protected var ExtractType exttyp
	
	new (ExtractType ext){
		conditions = new ConditionsFactoryImpl
		exttyp = ext
	}
	
	def ExtractType getExttyp(){
		exttyp
	}
	
	def AUTOSARDataType findingDataType(String extractRule){
		if (extractRule.contains('extract')){
			val startpoint = extractRule.indexOf('extract') + 'extract'.length
			val start = extractRule.indexOf('(', startpoint)
			val end = extractRule.indexOf(')',startpoint)
			val size = Integer.parseInt(extractRule.substring(start+1, end))
			if (size < 8)
				return AUTOSARDataType.AUINT8
			else if (size >= 8 && size < 16)
				return AUTOSARDataType.AUINT16
			else if (size >= 16 && size < 32)
				return AUTOSARDataType.AUINT32
			else if (size >= 32 && size < 64)
				return AUTOSARDataType.AUINT64
			else
				return AUTOSARDataType.AUNICODE2STRING
		}
	}
	
	def List<String> diggingIntegers (String str){
		val res = str.replaceAll("[^0-9]+", " ")
		Arrays.asList(res.trim().split(" "))
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def ExtractStrategy createExtractStrategy(ExtractStrategy extract){
		extract
	}
	
	def EmptyExtractStrategy createEmptyExtractStrategy(){
		var emptyextractstrategy = conditions?.createEmptyExtractStrategy
		emptyextractstrategy = createExtractStrategy(emptyextractstrategy) as EmptyExtractStrategy
		emptyextractstrategy
	}
	
	def PluginResultExtractStrategy createPluginResultExtractStrategy(){
		var pluginresultextractstrategy = conditions?.createPluginResultExtractStrategy
		pluginresultextractstrategy = createExtractStrategy(pluginresultextractstrategy) as PluginResultExtractStrategy
		pluginresultextractstrategy
	}
	
	def PluginStateExtractStrategy createPluginStateExtractStrategy(){
		var pluginstateextractstrategy = conditions?.createPluginStateExtractStrategy
		pluginstateextractstrategy = createExtractStrategy(pluginstateextractstrategy) as PluginStateExtractStrategy
		pluginstateextractstrategy
	}
	
	def UniversalPayloadExtractStrategy createUniversalPayloadExtractStrategy(HashMap<String, Serializable> map){
		var universalpayloadextracstrategy = conditions?.createUniversalPayloadExtractStrategy
		universalpayloadextracstrategy = createExtractStrategy(universalpayloadextracstrategy) as UniversalPayloadExtractStrategy
		universalpayloadextracstrategy?.setExtractionRule(searchInMap(map, 'extractionRule'))
		//universalpayloadextracstrategy.byteOrder = ''
		universalpayloadextracstrategy?.setSignalDataType(findingDataType(searchInMap(map, 'extractionRule')))
		//universalpayloadextracstrategy.signalDataTypeTmplParam = ''
		universalpayloadextracstrategy
	}
	
	def UniversalPayloadExtractStrategy createUniversalPayloadExtractStrategy(UniversalPayloadExtractStrategy universalpayloadextracstrategy, HashMap<String, Serializable> map){
		//var universalpayloadextracstrategy = conditions?.createUniversalPayloadExtractStrategy
		var newuniversalpayloadextracstrategy = createExtractStrategy(universalpayloadextracstrategy) as UniversalPayloadExtractStrategy
		newuniversalpayloadextracstrategy?.setExtractionRule(searchInMap(map, 'extractionRule'))
		//universalpayloadextracstrategy.byteOrder = ''
		newuniversalpayloadextracstrategy?.setSignalDataType(findingDataType(searchInMap(map, 'extractionRule')))
		//universalpayloadextracstrategy.signalDataTypeTmplParam = ''
		newuniversalpayloadextracstrategy
	}
	
	def UniversalPayloadWithLegacyExtractStrategy createUniversalPayloadWithLegacyExtractStrategy(HashMap<String, Serializable> map){
		var universalpayloadwithlegacyextracstrategy = conditions?.createUniversalPayloadWithLegacyExtractStrategy
		universalpayloadwithlegacyextracstrategy = createUniversalPayloadExtractStrategy(universalpayloadwithlegacyextracstrategy, map) as UniversalPayloadWithLegacyExtractStrategy
		val listofInts = diggingIntegers(searchInMap(map, 'extractionRule'))
		if (listofInts.length > 2){
			universalpayloadwithlegacyextracstrategy?.setStartBit(listofInts.get(1))
			universalpayloadwithlegacyextracstrategy?.setLength(listofInts.get(2))
		}
		else if (listofInts.length == 2){
			universalpayloadwithlegacyextracstrategy?.setStartBit(listofInts.get(0))
			universalpayloadwithlegacyextracstrategy?.setLength(listofInts.get(1))
		}
		else if (listofInts.length == 1){
			universalpayloadwithlegacyextracstrategy?.setStartBit('0')
			universalpayloadwithlegacyextracstrategy?.setLength(listofInts.get(0))
		}
		else {
			universalpayloadwithlegacyextracstrategy?.setStartBit('0')
			universalpayloadwithlegacyextracstrategy.setLength('0')
		}
		universalpayloadwithlegacyextracstrategy
	}
	
	def VerboseDLTExtractStrategy createVerboseDLTExtractStrategy(){
		var verbosedltextractstrategy = conditions?.createVerboseDLTExtractStrategy
		verbosedltextractstrategy = createExtractStrategy(verbosedltextractstrategy) as VerboseDLTExtractStrategy
		verbosedltextractstrategy
	}
	
}