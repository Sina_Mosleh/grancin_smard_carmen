package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import chrisna.Assignment
import chrisna.SendEvent
import chrisna.TimeWindow
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.state
import com.bmw.vv.client.RequestData
import com.bmw.vv.grancin.codegen.chrisna.runtime.ChrisnaFSMGenerator
import de.bmw.smard.modeller.conditions.AbstractMessage
import de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
import de.bmw.smard.modeller.conditions.ComparatorSignal
import de.bmw.smard.modeller.conditions.Condition
import de.bmw.smard.modeller.conditions.ConditionSet
import de.bmw.smard.modeller.conditions.DataType
import de.bmw.smard.modeller.conditions.Expression
import de.bmw.smard.modeller.conditions.LogicalOperatorType
import de.bmw.smard.modeller.conditions.SignalReferenceSet
import de.bmw.smard.modeller.conditions.SignalVariableLagEnum
import de.bmw.smard.modeller.conditions.Variable
import de.bmw.smard.modeller.conditions.VariableReference
import de.bmw.smard.modeller.conditions.VariableSet
import de.bmw.smard.modeller.statemachine.AbstractState
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
import de.bmw.smard.modeller.statemachine.ControlType
import de.bmw.smard.modeller.statemachine.InitialState
import de.bmw.smard.modeller.statemachine.State
import de.bmw.smard.modeller.statemachine.StateType
import de.bmw.smard.modeller.statemachine.Transition
import java.io.Serializable
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.BasicEList

class MainCreator extends IBaseClassForAllAddMethods{
	
	protected var ChrisnaFSMGenerator generatedchrisnafsms
	protected var RequestData restapireqdata
	//protected val String intername
	protected val String interversion 
	
	protected var ChrisnaFactoryImpl gen
	protected val Long offset = Long.valueOf(1)
	
	public var SignalReferenceSet signalreferenceset
	public var VariableSet variableset
	public var BasicEList<ConditionSet> conditionsets
	
	new(ChrisnaFSMGenerator csa, RequestData reqdata){
		super()
		
		//this.intername = interfaceName
		this.interversion = csa.getVersion
		this.generatedchrisnafsms = csa
		this.restapireqdata = reqdata
		
		gen = new ChrisnaFactoryImpl
		
		signalreferenceset = conditions?.createSignalReferenceSet
		variableset = conditions?.createVariableSet
		conditionsets = new BasicEList<ConditionSet>
	}
	
	def AbstractMessage createMessage(HashMap<String, Serializable> map, BusType bustype){
		var signals = new ArrayList<SignalCreator>()
		signals.add(new SignalCreator(DecodeType.DOUBLE, ExtractType.UNIVERSALPAYLOADWITHLEGACY, SignalType.DOUBLE))
		var filters = new ArrayList<FilterCreator>()
		//filters.add(new FilterCreator(BusType.NONE))
		var MessageCreator messagegen = new MessageCreator(ConvertIntToBusType(restapireqdata.getCon.getbusID),
															this.interversion,
															signals, filters);
		switch bustype {
			case SOMEIP: {
				messagegen?._filters.add(new FilterCreator(BusType.SOMEIP))
				messagegen?.createSomeIPMessage(map)				
			}
			case CAN: {
				messagegen?._filters.add(new FilterCreator(BusType.CAN))
				messagegen?.createCANMessage(map)
			}
			case LIN: {
				messagegen?._filters.add(new FilterCreator(BusType.LIN))
				messagegen?.createLINMessage(map)
			}
			case FLEXRAY: {
				messagegen?._filters.add(new FilterCreator(BusType.FLEXRAY))
				messagegen?.createFlexRayMessage(map)
			}
			case ETHERNET: {
				messagegen?._filters.add(new FilterCreator(BusType.ETHERNET))
				messagegen?.createEthernetMessage(map)
			}
			case IPV4: {
				messagegen?._filters.add(new FilterCreator(BusType.IPV4))
				messagegen?.createIPv4Message(map)
			}
			case NONVERBOSEDLT: {
				messagegen?._filters.add(new FilterCreator(BusType.NONVERBOSEDLT))
				messagegen?.createNonVerboseDLTMessage(map)
			}
			case PLUGIN: {
				messagegen?._filters.add(new FilterCreator(BusType.PLUGIN))
				messagegen?.createPluginMessage(map)
			}
			case SOMEIPSD: {
				messagegen?._filters.add(new FilterCreator(BusType.SOMEIPSD))
				messagegen?.createSomeIPSDMessage(map)
			}
			case UDP: {
				messagegen?._filters.add(new FilterCreator(BusType.UDP))
				messagegen?.createUDPMessage(map)
			}
			case UDPNM: {
				messagegen?._filters.add(new FilterCreator(BusType.UDPNM))
				messagegen?.createUDPNMMessage(map)
			}
			case VERBOSEDLT: {
				messagegen?._filters.add(new FilterCreator(BusType.VERBOSEDLT))
				messagegen?.createVerboseDLTPayloadMessage(map)
			}
			case NONE: {
				messagegen?._filters.add(new FilterCreator(BusType.NONE))
				messagegen?.createAbstractBusMessage(null, map)
			}
		}
		
	}
	
	def Variable createVariable(HashMap<String, Object> map){
		var variablegen = new VariableCreator()
		var vartyp = ConvertStringToVariableType(searchInMap(map, 'variableType'))
		switch(vartyp) {
			case NONE: {
				variablegen?.createAbstractVariable(null, map)
			}
			case SIGNAL: {
				variablegen?.createSignalVariable(map)
			}
			case VALUE: {
				variablegen?.createValueVariable(map)
			}
		}
	}
	
	def ArrayList<VariableReference> createVariablesForExpression(Assignment assignment){
		var ArrayList<VariableReference> variables = new ArrayList<VariableReference>()
		if(searchInVariableSet(this.variableset, assignment.rightSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment.leftSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.leftSide)))
			//foundLeftSide = true
		}
		variables
	}
	
	def createRefSignalsAndVariablesForExpression(Assignment assignment, ArrayList<VariableReference> vars, ArrayList<ComparatorSignal> comparesigs){
		//var boolean foundLeftSide = false
		//var boolean foundRightSide = false
		//var EList<SignalReference> refsignals
		//var EList<VariableReference> variables
		if(searchInVariableSet(this.variableset, assignment?.rightSide) !== null){
			vars.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.rightSide)))
			//foundRightSide = true	
		}
		else {
			comparesigs.add({
				var constcomsig = conditions.createConstantComparatorValue
				constcomsig.description = ''
				constcomsig.interpretedValue = BooleanOrTemplatePlaceholderEnum.FALSE
				constcomsig.value = {
					if(assignment?.rightSide != '0.0')
						assignment?.rightSide?.ExtractDigitFromString
					else
						'0'
				}
				constcomsig
			})
		}
		if(searchInVariableSet(this.variableset, assignment.leftSide) !== null){
			vars.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.leftSide)))
			//foundLeftSide = true
		}
		
		/*if(searchInSignalSet(this.signalreferenceset, assignment?.leftSide) !== null){
			refsignals.add(convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, assignment?.leftSide)))
			//foundLeftSide = true
		}
		if(searchInSignalSet(this.signalreferenceset, assignment?.rightSide) !== null){
			refsignals.add(convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, assignment?.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment?.rightSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment?.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment?.leftSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment?.leftSide)))
			//foundLeftSide = true
		}
		
		/*if(!foundLeftSide){
			var variablegen = new VariableCreator()
			var newvariable = variablegen.createValueVariable()
			variables.add(convertVariableToVariableReference(newvariable as Variable))
			this.variableset.variables.add(newvariable)
		}
		if(!foundRightSide){
			var variablegen = new VariableCreator()
			var newvariable = variablegen.createValueVariable()
			variables.add(convertVariableToVariableReference(newvariable as Variable))
			this.variableset.variables.add(newvariable)
		}*/
	}
	
	def Condition createCondition(EList<chrisna.Condition> conds) {
		var cond = conditions?.createCondition
		var expressiongen = new ExpressionCreator(null)
		var ArrayList<Expression> conditionsexpressions = new ArrayList<Expression>()
		for(condition : conds){
			//cond?.setId(createUUID)
			cond?.setName(condition?.name)// + condition.name
			cond.description = ''//condition?.description + condition?.name + '\n'
			var ArrayList<Expression> eventsexpressions = new ArrayList<Expression>()
			for(event : condition?.action?.sendevent){
				var ArrayList<Expression> assignsexpressions = new ArrayList<Expression>()
				var assignments = event?.getAssigcontainer?.getAssignment
				switch(event?.getMethod){
					case 'Not': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							assign.replaceAssignment
							//var variables = createVariablesForExpression(assign)
							var variables = new ArrayList<VariableReference>()
							var ArrayList<ComparatorSignal> comparesigs = new ArrayList<ComparatorSignal>()
							createRefSignalsAndVariablesForExpression(assign, variables, comparesigs)
							assignsexpressions.add(expressiongen?.createSignalComparisonExpression(variables, comparesigs, 
								convertToComparator(assign?.getOperation.getStr)
							))
						}
						if(assignsexpressions.length > 1)
							eventsexpressions.add(expressiongen.createNotExpression(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND) as Expression, LogicalOperatorType.NOT))
						else if (assignsexpressions.length == 1)
							eventsexpressions.add(expressiongen.createNotExpression(assignsexpressions.get(0), LogicalOperatorType.NOT))
					}
					case 'Comparison': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							assign.replaceAssignment
							//var variables = createVariablesForExpression(assign)
							var variables = new ArrayList<VariableReference>()
							var ArrayList<ComparatorSignal> comparesigs = new ArrayList<ComparatorSignal>()
							createRefSignalsAndVariablesForExpression(assign, variables, comparesigs)
							assignsexpressions.add(expressiongen?.createSignalComparisonExpression(variables, comparesigs, 
								convertToComparator(assign?.getOperation.getStr)
							))
						}
						if (assignsexpressions.length > 1)
							eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
						else if (assignsexpressions.length == 1)
							eventsexpressions.add(assignsexpressions.get(0))
					}
					case 'Timing': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							assign.replaceAssignment
							assignsexpressions.add(expressiongen.createTimingExpression(
								({
									if(assign?.getLeftSide.findingTimeFromString(this.variableset) > offset) 
										assign?.getLeftSide.findingTimeFromString(this.variableset) - offset
									else
										assign?.getLeftSide.findingTimeFromString(this.variableset) + offset
								}).toString,
								({
									if(assign?.getLeftSide.findingTimeFromString(this.variableset) > offset)
										assign?.getLeftSide.findingTimeFromString(this.variableset) + offset
									else
										assign?.getLeftSide.findingTimeFromString(this.variableset) + 2 * offset
								}).toString))
						}
						if (assignsexpressions.length > 1)
							eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
						else if (assignsexpressions.length == 1)
							eventsexpressions.add(assignsexpressions.get(0))
					}
					case 'True': {
						for(var i = 0 ; i< assignments.length; i++){
							assignsexpressions.add(expressiongen?.createTrueExpression)	
						}
						if (assignsexpressions.length > 1)
							eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
						else if (assignsexpressions.length == 1)
							eventsexpressions.add(assignsexpressions.get(0))
					}
					case 'Stop': {
						for(var i = 0 ; i< assignments.length; i++){
							assignsexpressions.add(expressiongen?.createStopExpression(AnalysisTypeOrTemplatePlaceholderEnum.ALL))	
						}
						if (assignsexpressions.length > 1)
							eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
						else if (assignsexpressions.length == 1)
							eventsexpressions.add(assignsexpressions.get(0))
					}
					case 'MessageCheck': {
						/*for(assignment : event?.getAssigcontainer?.getAssignment){
							assignsexpressions.add(expressiongen.createMessageCheckExpression({
								convertVariableToVariableReference(searchInVariableSet(this.variableset, str))
							}))	
						}*/
						/*if(searchInSignalSet(this.signalreferenceset, event?.method) !== null){
							var signal = convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, event?.method))
							//foundLeftSide = true
						}
						if(convertVariableToVariableReference(searchInVariableSet(this.variableset, event?.method)) !== null){
							var variable = convertVariableToVariableReference(searchInVariableSet(this.variableset, event?.method))
							eventsexpressions.add(expressiongen.createMessageCheckExpression(assignsexpressions,LogicalOperatorType.AND))
						}*/
					}
				}
			}
			if (eventsexpressions.length > 1)
				conditionsexpressions.add(expressiongen?.createLogicalExpression(eventsexpressions,LogicalOperatorType.AND))
			else if (eventsexpressions.length == 1)
				conditionsexpressions.add(eventsexpressions.get(0))
		}
		
		if (conditionsexpressions.length > 1)
			cond?.setExpression(expressiongen?.createLogicalExpression(conditionsexpressions, LogicalOperatorType.AND))
		else if (conditionsexpressions.length == 1)
			cond?.setExpression(conditionsexpressions.get(0))
		
		cond
	}
	
	def InitialState createInitialState(state st){
		var state = statemachine?.createInitialState
		//state.setId(createUUID)
		state.setName('initial state')
		//state.readVariables.add()
		//state.writeVariables.add()
		state?.setDescription(st?.description?.toString)
		state?.setStateType(StateType.INIT)
		
		state
	}
	
	def State createState(state st){
		var state = statemachine?.createState
		//state?.setId(createUUID)
		state?.setName(st?.name)
		//state.readVariables.add()
		//state.writeVariables.add()
		state?.setDescription(st?.description?.toString)
		for(event : st?.action?.sendevent){
			val actiongen = new ActionCreator()
			switch(event?.method){
				case 'Compute': {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						//state?.actions.add(actiongen?.createComputeVariable(assignment, event?.trigger, 
							//(searchInVariableSet(this.variableset, assignment?.leftSide)) as ValueVariable))
							
						//state?.actions.add(actiongen?.createAct(assignment, event?.trigger, ActionTypeNotEmpty.COMPUTE)) //TODO
					}
				}
				case 'Control': {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						state?.actions.add(actiongen?.createControlAct(assignment, event?.trigger, null, ControlType.ON))
						//state?.actions.add(statemachine.createAction)
					}
				}
				default: {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						state?.actions.add(actiongen?.createAct(assignment, event?.trigger, ActionTypeNotEmpty.COMPUTE))
						//state?.actions.add(statemachine.createAction)
					}
				}
			}
		}
		
		if (state?.name?.toLowerCase.contains('ok'))
			state?.setStateType(StateType.OK)
		else if (state?.name?.toLowerCase.contains('warning'))
			state?.setStateType(StateType.WARN)
		else if (state?.name?.toLowerCase.contains('error'))
			state?.setStateType(StateType.DEFECT)
		else
			state?.setStateType(StateType.INFO)
		state?.setStateTypeTmplParam(state?.stateType.toString)
		
		state
	}
	
	def Transition createTransition(AbstractState from, AbstractState to, Condition condition, int logLevel){
		var transition = statemachine.createTransition
		//transition?.setId(createUUID)
		transition?.setName(condition?.name)
		//transition.readVariables.add()
		//transition.writeVariables.add()
		transition?.setDescription(condition?.description)
		transition?.setFrom(from)
		transition?.setTo(to)
		transition?.setLogLevel(logLevel.toString)
		transition?.conditions.add(condition)
		transition
	}
	
	protected def HashMap<String, Object> createVariableForAssignmentSide(SendEvent event, EList<AbstractMessage> messages, String leftside, String rightside){
		var map = new HashMap<String, Object>()
		if (searchForSignal(signalreferenceset?.getMessages, leftside) !== null){
			map.put('variableType', VariableType.SIGNAL)
			map.put('signal', searchForSignal(messages, leftside))
			map.put('initialValue', 0 as double)
			map.put('description', '')
			map.put('name', /*searchForSignal(messages, leftside)?.name*/ leftside)
			map.put('dataType', DataType.DOUBLE)
			map.put('format', {
					//var p = Pattern.compile('\\d+')
					//var m = p.matcher(assignment?.getLeftSide)
					//if(rightside.isNumeric){
						//var d = Double.parseDouble(rightside)
						//if(d.toString.contains('.'))
							//if(m.group.contains('.'))
							'double'
						//else
							//'int'		
					//}
					//else 
						//''
				})
			map.put('interpretedValue', BooleanOrTemplatePlaceholderEnum.FALSE)
			map.put('lag', SignalVariableLagEnum.CURRENT)
			if(event?.getMethod.contains('Timing') && event?.getTime?.getType.contains('TimeWindow'))
				map.put('unit', ((event.getTime) as TimeWindow)?.getUnit)
			else if (event?.getMethod.contains('Compute') || event?.getMethod.contains('Timing')/*&& event?.getTime?.getType.contains('TimeWindow')*/){
				if(rightside.toLowerCase.contains(' ms'))
					map.put('unit', 'ms')
				else if(rightside.toLowerCase.contains(' us'))
					map.put('unit', 'us')
				else if(rightside.toLowerCase.contains(' ns'))
					map.put('unit', 'ns')
				else if(rightside.toLowerCase.contains(' s'))
					map.put('unit', 's')
				else if(rightside.toLowerCase.contains(' min'))
					map.put('unit', 'min')
				else if(rightside.toLowerCase.contains(' h'))
					map.put('unit', 'h')
				else if(rightside.toLowerCase.contains(' day'))
					map.put('unit', 'day')
			}
			else
				map.put('unit', '')
		}
		else {
			map.put('variableType', VariableType.VALUE)
			//if (rightside.isNumeric){
				map.put('dataType', DataType.DOUBLE)
				map.put('initialValue', {
				Double.parseDouble(({
					if(rightside != '0.0')
						rightside?.ExtractDigitFromString
					else
						'0'
				}) ?: '0')
			})
				map.put('format', {
					//var p = Pattern.compile('\\d+')
					//var m = p.matcher(assignment?.getLeftSide)
					//var d = Double.parseDouble(rightside)
					//if(d.toString.contains('.'))
						//if(m.group.contains('.'))
						'double'
					//else
						//'int'
				})
			//}
			/*else {
				map.put('dataType', DataType.STRING)
			}*/
			map.put('description', '')
			map.put('name', leftside)
			if((event?.getMethod.contains('Timing') && event?.getTime?.getType?.contains('TimeWindow')))
				map.put('unit', ((event.getTime) as TimeWindow)?.getUnit)
			else if (event?.getMethod.contains('Compute') || event?.getMethod.contains('Timing')/*&& event?.getTime?.getType.contains('TimeWindow')*/){
				if(rightside.toLowerCase.contains(' ms'))
					map.put('unit', 'ms')
				else if(rightside.toLowerCase.contains(' us'))
					map.put('unit', 'us')
				else if(rightside.toLowerCase.contains(' ns'))
					map.put('unit', 'ns')
				else if(rightside.toLowerCase.contains(' s'))
					map.put('unit', 's')
				else if(rightside.toLowerCase.contains(' min'))
					map.put('unit', 'min')
				else if(rightside.toLowerCase.contains(' h'))
					map.put('unit', 'h')
				else if(rightside.toLowerCase.contains(' day'))
					map.put('unit', 'day')
			}
			else
				map.put('unit', '')
		}
		map
	}
}