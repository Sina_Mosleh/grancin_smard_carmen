package com.bmw.vv.grancin.codegen.smardmodeller.runtime;

//import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.service.AbstractGenericModule;

public class SmardModellerGeneratorModule extends AbstractGenericModule {
	public Class<? extends org.eclipse.xtext.generator.IGenerator2> bindIGenerator2(){
	//public Class<? extends IScopeProvider> bindIScopeProvider(){
		return SmardModellerGenerator.class;
	}
}
