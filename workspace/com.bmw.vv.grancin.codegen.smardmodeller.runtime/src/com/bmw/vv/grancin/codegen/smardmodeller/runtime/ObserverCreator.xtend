package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import java.util.HashMap
import java.io.Serializable
import de.bmw.smard.modeller.conditions.AbstractObserver
import java.util.UUID
import de.bmw.smard.modeller.conditions.SignalObserver
import de.bmw.smard.modeller.conditions.ValueVariableObserver
import java.util.ArrayList
import de.bmw.smard.modeller.conditions.ObserverValueType

class ObserverCreator {
	
	protected var ConditionsFactoryImpl conditions
	
	new (){
		conditions = new ConditionsFactoryImpl	
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def ArrayList<HashMap<String, Serializable>> valueToList(HashMap<String, Serializable> map, String key){
		var list = new ArrayList<HashMap<String, Serializable>>()
		if(map.containsKey(key))
			list = map.get(key) as ArrayList<HashMap<String, Serializable>>
		list
			
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractObserver createAbstractObserver(AbstractObserver observer, HashMap<String, Serializable> map){
		//observer.id = createUUID
		//observer.sourceReference = 
		observer.name = searchInMap(map, 'name')
		observer.description = searchInMap(map,'description')
		observer.logLevel = searchInMap(map, 'logLevel')
		var ranges = valueToList(map, 'valueRanges')
		ranges.forEach[
			observer.valueRanges.add({
				var range = conditions.createObserverValueRange
				range.value = searchInMap(it, 'value')
				range.description = searchInMap(it, 'description')
				range.valueType = ObserverValueType.get(searchInMap(it, 'type'))
				range
			})
		]
		//observer.valueRangesTmplParam = ''
		//observer.activeAtStart = false
		observer
	}
	
	def SignalObserver createSignalObserver(HashMap<String, Serializable> map){
		var signalobserver = conditions.createSignalObserver
		signalobserver = createAbstractObserver(signalobserver, map) as SignalObserver
		signalobserver
	}
	
	def ValueVariableObserver createValueVariableObserver(HashMap<String, Serializable> map){
		var valuevariableobserver = conditions.createValueVariableObserver
		valuevariableobserver = createAbstractObserver(valuevariableobserver, map) as ValueVariableObserver
		valuevariableobserver
	}
}