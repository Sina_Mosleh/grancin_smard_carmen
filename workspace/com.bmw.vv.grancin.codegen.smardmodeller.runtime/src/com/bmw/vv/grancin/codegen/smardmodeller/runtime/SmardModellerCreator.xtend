package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import com.bmw.vv.client.RequestData
import com.bmw.vv.grancin.codegen.chrisna.runtime.ChrisnaFSMGenerator
import de.bmw.smard.modeller.conditions.DocumentRoot
import de.bmw.smard.modeller.statemachine.StateMachine
import de.bmw.smard.modeller.statemachine.Transition
import de.bmw.smard.modeller.statemachineset.GeneralInfo
import de.bmw.smard.modeller.util.ConditionUtils
import java.io.Serializable
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.gmf.runtime.notation.Diagram
import org.eclipse.emf.common.util.BasicEList
import de.bmw.smard.modeller.conditions.ConditionSet

class SmardModellerCreator extends MainCreator { 
	
	protected var BasicEList<StateMachine> stmachines
	
	protected val GeneralInfo info
	protected var int length
	protected val int logLevel
	
	new (String aut, String des, String topic, String dep, ChrisnaFSMGenerator csa, RequestData reqdata){
		super(csa, reqdata)
		
		logLevel = 100
		info = statemachineset?.createGeneralInfo
		info.author = aut
		info.description = des
		info.topic = topic
		info.department = dep
		
		stmachines = new BasicEList<StateMachine>
		
		this.CreateAll
	}
	
	def ChrisnaFSMGenerator getChrisnaFsm(){
		return this.generatedchrisnafsms
	}
	
	def void setChrinaFsm(ChrisnaFSMGenerator csa){
		this.generatedchrisnafsms = csa
	}
	
	def RequestData getRequestedData(){
		return this.restapireqdata
	}
	
	def void setRequestedData(RequestData arr){
		this.restapireqdata = arr
	}
	
	def void CreateAll(){
		
		//val bustype = ConvertIntToBusType(restapireqdata.getCon.getbusID)
		val bustype = ConvertIntToBusType(45000)
		var requestedinfo = new ArrayList<HashMap<String, Serializable>>()
		//requestedinfo = restapireqdata?.getParsed?.getListData
		requestedinfo.forEach[
			signalreferenceset?.getMessages.add(createMessage(it, bustype))
		]
			
				
		var additionalvariables = new ArrayList<HashMap<String, Object>>()
		for( scen : this.generatedchrisnafsms?.getScenariocontainer?.getScenario){
			for(state : scen?.getCurrentStates?.getState){
				for( event : state?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						assignment.replaceAssignment
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
			
			for(state : scen?.getNextStates?.getState){
				for( event : state?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						assignment.replaceAssignment
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
			
			for(condition : scen?.getCondcontainer?.getCondition){
				for( event : condition?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						assignment.replaceAssignment
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
		}
		//variableset?.setUuid(createUUID) 
		additionalvariables.forEach[
			variableset?.variables.add(createVariable(it))
		]
			
		
		var index = 0
		for(machine : this.generatedchrisnafsms?.getchrisnafsms){
		
		var conditionset = conditions?.createConditionSet
		var stmachine = statemachine?.createStateMachine
			
		conditionset.baureihe = ''
		conditionset.description = ''
		conditionset.istufe = ''
		conditionset.schemaversion = ''
		//conditionset.uuid = createUUID
		conditionset.version = ''
		var name = this.generatedchrisnafsms?.getFsmnames.get(index)
		conditionset.name = name
		
		//stmachine?.setId(createUUID)
		//stmachine.readVariables.add()
		//stmachine.writeVariables.add()
		stmachine?.setName(name)
		stmachine?.setInitialState(createInitialState(machine?.getInitstate))
		
		for (scen : machine?.getScencontainer?.scenario){
			for(st : scen?.currentStates?.state)
				if (!st?.name.contains('init') && !stmachine?.states.existStateInStatemachine(createState(st)))
					stmachine?.states.add(createState(st))
			for(st : scen?.nextStates?.state)
				if (!st?.name.contains('init') && !stmachine?.states.existStateInStatemachine(createState(st)))
					stmachine?.states.add(createState(st))
			/*for(cond : scen?.condcontainer?.condition){
				var transition = createTransition(cond, statemachine?.states, conditionset?.conditions)
				statemachine?.transitions.add(transition)
				(searchForState(statemachine?.states, cond?.from?.name))?.out?.add(transition)
				(searchForState(statemachine?.states, cond?.to?.name))?.in?.add(transition)
			}*/
		}
			
		var scenarios = machine?.getScencontainer?.getScenario
		for( scen : scenarios){
			var condis = scen?.condcontainer?.condition
			var len = condis.length
			for(var i=0; i<len; i++) {
				
			//}
		//}
			
		//var Bedingungen = condconditions?.condition
		//for (var i = 0; i<Bedingungen.length; i++){
		////for (cond : Bedingungen){
		//	var conditioncontainer = gen?.createCondContainer
		//	var cond = Bedingungen?.get(0)
		//	//i--
		//	conditioncontainer?.getCondition.add(cond)
		//	var Bedingungen_temp = condconditions?.condition
		//	for (var j = 0; j< Bedingungen_temp.length; j++){
		//	//for (con : Bedingungen_temp){
		//		var con = Bedingungen_temp?.get(j)
		//		//j--
		//		if(con?.to?.name == cond?.to?. name && con?.from?.name == cond?.from?.name){
		//			conditioncontainer?.getCondition.add(con)
		//			Bedingungen.remove(con)	
		//		}
		//	}
			var co = condis.get(0)
			conditionset.conditions.add(createCondition({
				var condconditions = gen?.createCondContainer
				condconditions?.condition.add(co)
				condconditions?.condition}))
			var curst = co?.from
			//if (!curst?.name.contains('init') && !stmachine?.states.existStateInStatemachine(createState(curst)))
				//stmachine?.states.add(createState(curst))
				
			var nextst = co?.to
			//if (!nextst?.name.contains('init') && !stmachine?.states.existStateInStatemachine(createState(nextst)))
				//stmachine?.states.add(createState(nextst))
				
			var Transition transition
			/*if(curst?.name.contains('init') || stmachine?.states.existStateInStatemachine(createState(curst))){
				if(curst?.name.contains('init')){
					transition = createTransition(stmachine?.getInitialState, stmachine?.states.get(stmachine?.states.length-1), 
					conditionset?.conditions?.get(conditionset?.conditions.length-1))
					stmachine?.getInitialState?.out?.add(transition)
					stmachine?.states.get(stmachine?.states.length-1)?.in?.add(transition)
				}
				if(stmachine?.states.existStateInStatemachine(createState(curst))){
					transition = createTransition(searchForState(stmachine?.states, curst?.name), stmachine?.states.get(stmachine?.states.length-1), 
					conditionset?.conditions?.get(conditionset?.conditions.length-1))
					searchForState(stmachine?.states, curst?.name)?.out?.add(transition)
					stmachine?.states.get(stmachine?.states.length-1)?.in?.add(transition)	
				}
			}
			else if (nextst?.name.contains('init') || stmachine?.states.existStateInStatemachine(createState(nextst))){
				if(nextst?.name.contains('init')){
					transition = createTransition(stmachine?.states.get(stmachine?.states.length-1), stmachine?.getInitialState, 
					conditionset?.conditions?.get(conditionset?.conditions.length-1))
					stmachine?.states.get(stmachine?.states.length-1)?.out?.add(transition)
					stmachine?.getInitialState?.in?.add(transition)	
				}
				if(stmachine?.states.existStateInStatemachine(createState(nextst))){
					transition = createTransition(stmachine?.states.get(stmachine?.states.length-1),
					searchForState(stmachine?.states, nextst?.name)
					, conditionset?.conditions?.get(conditionset?.conditions.length-1))
					stmachine?.states.get(stmachine?.states.length-1)?.out?.add(transition)
					searchForState(stmachine?.states, nextst?.name)?.in?.add(transition)	
				}
			}
			else {
				transition = createTransition(stmachine?.states.get(stmachine?.states.length-2), 
					stmachine?.states.get(stmachine?.states.length-1), 
					conditionset?.conditions?.get(conditionset?.conditions.length-1))
				stmachine?.states.get(stmachine?.states.length-2)?.out?.add(transition)
				stmachine?.states.get(stmachine?.states.length-1)?.in?.add(transition)	
			}*/
			
			if (curst?.name.contains('init')){
				transition = createTransition(stmachine?.getInitialState,
					searchForState(stmachine?.states, nextst?.name),
					conditionset?.conditions?.get(conditionset?.conditions.length-1), logLevel)	
				stmachine?.getInitialState?.out?.add(transition)
				(searchForState(stmachine?.states, nextst?.name))?.in?.add(transition)
				stmachine?.transitions.add(transition)
			}
			else if (nextst?.name.contains('init')){
				transition = createTransition(searchForState(stmachine?.states, curst?.name),
					stmachine?.getInitialState,
					conditionset?.conditions?.get(conditionset?.conditions.length-1), logLevel)	
				(searchForState(stmachine?.states, curst?.name))?.out?.add(transition)
				stmachine?.getInitialState?.in?.add(transition)
				stmachine?.transitions.add(transition)
			}
			else {
				transition = createTransition(searchForState(stmachine?.states, curst?.name),
					searchForState(stmachine?.states, nextst?.name),
					conditionset?.conditions?.get(conditionset?.conditions.length-1), logLevel)
				(searchForState(stmachine?.states, curst?.name))?.out?.add(transition)
				(searchForState(stmachine?.states, nextst?.name))?.in?.add(transition)
				stmachine?.transitions.add(transition)
			}
		}
		}
		index++
		conditionsets.add(conditionset)
		stmachines.add(stmachine)
		}
		length = index
	}
	
	def getLength(){
		return length
	}
	
	def getConditionsets(){
		if(this.conditionsets === null)
			conditionsets = new BasicEList<ConditionSet>
		return this.conditionsets
	}
	
	def getStmachines(){
		if(this.stmachines === null)
			stmachines = new BasicEList<StateMachine>
		return this.stmachines
	}
	
	def DocumentRoot conditionDocuGenerator(){
		//val document = conditions.createDocumentRoot
		val document = ConditionUtils.createConditionsDocument
		
		val conditionsdocument = conditions?.createConditionsDocument	
		conditionsdocument?.setSignalReferencesSet(signalreferenceset)
		conditionsdocument?.setVariableSet(variableset)
		conditionsdocument?.setConditionSet(null)
		
		document?.setConditionsDocument(conditionsdocument)
		document
	}
	
	def DocumentRoot conditionDocuGenerator(ConditionSet conditionset){
		//val document = conditions.createDocumentRoot
		val document = ConditionUtils.createConditionsDocument
		
		val conditionsdocument = conditions?.createConditionsDocument	
		conditionsdocument?.setSignalReferencesSet(null)
		conditionsdocument?.setVariableSet(null)
		conditionsdocument?.setConditionSet(conditionset)
		
		document?.setConditionsDocument(conditionsdocument)
		document
	}
	
	def de.bmw.smard.modeller.statemachine.DocumentRoot stateMachineDocuGenerator(StateMachine stmachine){
		val document = statemachine?.createDocumentRoot
		document.setStateMachine(stmachine)
		document
	}
	
	def de.bmw.smard.modeller.statemachineset.DocumentRoot stateMachineSetDocuGenerator(String outputFile){
		val document = statemachineset?.createDocumentRoot
		
		val stmachineset = statemachineset?.createStateMachineSet
		stmachineset?.setGeneralInfo(info)
		var output = statemachineset?.createOutput
		output.setTargetFilePath(outputFile)
		stmachineset?.setOutput(output)
		for(stmachine : stmachines)
			stmachineset?.getStateMachines?.add(stmachine)
		stmachineset?.setId(createUUID)
		var exeConfig = statemachineset?.createExecutionConfig
		stmachineset?.setExecutionConfig(exeConfig)
		
		document.setStateMachineSet(stmachineset)
		document
	}
	
	def Diagram stateMachineDiagramGenerator(StateMachine stmachine){
		//var statemachineDiagram = new StatemachineResourceFactoryImpl
		var diagram = notation?.createDiagram
		diagram?.setElement(stmachine)
		diagram?.setMeasurementUnit(notation?.createMeasurementUnitFromString(null, 'Pixel'))
		diagram?.setType('Statemachine')
		diagram?.getStyles().add(notation?.createDiagramStyle)
		diagram
		//return stmachine
	}
}