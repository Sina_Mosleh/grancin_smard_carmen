/**
 */
package statemachineset.impl;

import conditions.ConditionsPackage;

import conditions.impl.ConditionsPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import statemachine.StatemachinePackage;

import statemachine.impl.StatemachinePackageImpl;

import statemachineset.DocumentRoot;
import statemachineset.ExecutionConfig;
import statemachineset.GeneralInfo;
import statemachineset.KeyValuePairUserConfig;
import statemachineset.Output;
import statemachineset.StateMachineSet;
import statemachineset.StatemachinesetFactory;
import statemachineset.StatemachinesetPackage;

import statemachineset.util.StatemachinesetValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachinesetPackageImpl extends EPackageImpl implements StatemachinesetPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass generalInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateMachineSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass keyValuePairUserConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionConfigEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see statemachineset.StatemachinesetPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private StatemachinesetPackageImpl() {
		super(eNS_URI, StatemachinesetFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link StatemachinesetPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static StatemachinesetPackage init() {
		if (isInited) return (StatemachinesetPackage)EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredStatemachinesetPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		StatemachinesetPackageImpl theStatemachinesetPackage = registeredStatemachinesetPackage instanceof StatemachinesetPackageImpl ? (StatemachinesetPackageImpl)registeredStatemachinesetPackage : new StatemachinesetPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);
		ConditionsPackageImpl theConditionsPackage = (ConditionsPackageImpl)(registeredPackage instanceof ConditionsPackageImpl ? registeredPackage : ConditionsPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(registeredPackage instanceof StatemachinePackageImpl ? registeredPackage : StatemachinePackage.eINSTANCE);

		// Load packages
		theConditionsPackage.loadPackage();

		// Create package meta-data objects
		theStatemachinesetPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();

		// Initialize created meta-data
		theStatemachinesetPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();

		// Fix loaded packages
		theConditionsPackage.fixPackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theStatemachinesetPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return StatemachinesetValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theStatemachinesetPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(StatemachinesetPackage.eNS_URI, theStatemachinesetPackage);
		return theStatemachinesetPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDocumentRoot() {
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XMLNSPrefixMap() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XSISchemaLocation() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_StateMachineSet() {
		return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getGeneralInfo() {
		return generalInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeneralInfo_Author() {
		return (EAttribute)generalInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeneralInfo_Description() {
		return (EAttribute)generalInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeneralInfo_Topic() {
		return (EAttribute)generalInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getGeneralInfo_Department() {
		return (EAttribute)generalInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getOutput() {
		return outputEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getOutput_TargetFilePath() {
		return (EAttribute)outputEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStateMachineSet() {
		return stateMachineSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateMachineSet_GeneralInfo() {
		return (EReference)stateMachineSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateMachineSet_Output() {
		return (EReference)stateMachineSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStateMachineSet_ExportVersionCounter() {
		return (EAttribute)stateMachineSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStateMachineSet_Id() {
		return (EAttribute)stateMachineSetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateMachineSet_ExecutionConfig() {
		return (EReference)stateMachineSetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateMachineSet_Observers() {
		return (EReference)stateMachineSetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateMachineSet_StateMachines() {
		return (EReference)stateMachineSetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateMachineSet__IsValid_hasValidDescription__DiagnosticChain_Map() {
		return stateMachineSetEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateMachineSet__UpdateID() {
		return stateMachineSetEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateMachineSet__CreateUUID() {
		return stateMachineSetEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateMachineSet__IncrementExportVersionCounter() {
		return stateMachineSetEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateMachineSet__GetProjectName() {
		return stateMachineSetEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getKeyValuePairUserConfig() {
		return keyValuePairUserConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getKeyValuePairUserConfig_Key() {
		return (EAttribute)keyValuePairUserConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getKeyValuePairUserConfig_Value() {
		return (EAttribute)keyValuePairUserConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExecutionConfig() {
		return executionConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExecutionConfig_NeedsEthernet() {
		return (EAttribute)executionConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExecutionConfig_NeedsErrorFrames() {
		return (EAttribute)executionConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getExecutionConfig_KeyValuePairs() {
		return (EReference)executionConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExecutionConfig_LogLevel() {
		return (EAttribute)executionConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExecutionConfig_SorterValue() {
		return (EAttribute)executionConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getExecutionConfig__IsValid_hasValidLogLevel__DiagnosticChain_Map() {
		return executionConfigEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getExecutionConfig__IsValid_hasValidSorterValue__DiagnosticChain_Map() {
		return executionConfigEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StatemachinesetFactory getStatemachinesetFactory() {
		return (StatemachinesetFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		documentRootEClass = createEClass(DOCUMENT_ROOT);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(documentRootEClass, DOCUMENT_ROOT__STATE_MACHINE_SET);

		generalInfoEClass = createEClass(GENERAL_INFO);
		createEAttribute(generalInfoEClass, GENERAL_INFO__AUTHOR);
		createEAttribute(generalInfoEClass, GENERAL_INFO__DESCRIPTION);
		createEAttribute(generalInfoEClass, GENERAL_INFO__TOPIC);
		createEAttribute(generalInfoEClass, GENERAL_INFO__DEPARTMENT);

		outputEClass = createEClass(OUTPUT);
		createEAttribute(outputEClass, OUTPUT__TARGET_FILE_PATH);

		stateMachineSetEClass = createEClass(STATE_MACHINE_SET);
		createEReference(stateMachineSetEClass, STATE_MACHINE_SET__GENERAL_INFO);
		createEReference(stateMachineSetEClass, STATE_MACHINE_SET__OUTPUT);
		createEAttribute(stateMachineSetEClass, STATE_MACHINE_SET__EXPORT_VERSION_COUNTER);
		createEAttribute(stateMachineSetEClass, STATE_MACHINE_SET__ID);
		createEReference(stateMachineSetEClass, STATE_MACHINE_SET__EXECUTION_CONFIG);
		createEReference(stateMachineSetEClass, STATE_MACHINE_SET__OBSERVERS);
		createEReference(stateMachineSetEClass, STATE_MACHINE_SET__STATE_MACHINES);
		createEOperation(stateMachineSetEClass, STATE_MACHINE_SET___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP);
		createEOperation(stateMachineSetEClass, STATE_MACHINE_SET___UPDATE_ID);
		createEOperation(stateMachineSetEClass, STATE_MACHINE_SET___CREATE_UUID);
		createEOperation(stateMachineSetEClass, STATE_MACHINE_SET___INCREMENT_EXPORT_VERSION_COUNTER);
		createEOperation(stateMachineSetEClass, STATE_MACHINE_SET___GET_PROJECT_NAME);

		keyValuePairUserConfigEClass = createEClass(KEY_VALUE_PAIR_USER_CONFIG);
		createEAttribute(keyValuePairUserConfigEClass, KEY_VALUE_PAIR_USER_CONFIG__KEY);
		createEAttribute(keyValuePairUserConfigEClass, KEY_VALUE_PAIR_USER_CONFIG__VALUE);

		executionConfigEClass = createEClass(EXECUTION_CONFIG);
		createEAttribute(executionConfigEClass, EXECUTION_CONFIG__NEEDS_ETHERNET);
		createEAttribute(executionConfigEClass, EXECUTION_CONFIG__NEEDS_ERROR_FRAMES);
		createEReference(executionConfigEClass, EXECUTION_CONFIG__KEY_VALUE_PAIRS);
		createEAttribute(executionConfigEClass, EXECUTION_CONFIG__LOG_LEVEL);
		createEAttribute(executionConfigEClass, EXECUTION_CONFIG__SORTER_VALUE);
		createEOperation(executionConfigEClass, EXECUTION_CONFIG___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP);
		createEOperation(executionConfigEClass, EXECUTION_CONFIG___IS_VALID_HAS_VALID_SORTER_VALUE__DIAGNOSTICCHAIN_MAP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);
		ConditionsPackage theConditionsPackage = (ConditionsPackage)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);
		StatemachinePackage theStatemachinePackage = (StatemachinePackage)EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocumentRoot_StateMachineSet(), this.getStateMachineSet(), null, "stateMachineSet", null, 0, 1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(generalInfoEClass, GeneralInfo.class, "GeneralInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGeneralInfo_Author(), theXMLTypePackage.getString(), "author", "", 0, 1, GeneralInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeneralInfo_Description(), theXMLTypePackage.getString(), "description", "", 0, 1, GeneralInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeneralInfo_Topic(), theXMLTypePackage.getString(), "topic", "", 0, 1, GeneralInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGeneralInfo_Department(), theXMLTypePackage.getString(), "department", "", 0, 1, GeneralInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputEClass, Output.class, "Output", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOutput_TargetFilePath(), theXMLTypePackage.getString(), "targetFilePath", null, 0, 1, Output.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stateMachineSetEClass, StateMachineSet.class, "StateMachineSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStateMachineSet_GeneralInfo(), this.getGeneralInfo(), null, "generalInfo", null, 1, 1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineSet_Output(), this.getOutput(), null, "output", null, 1, 1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStateMachineSet_ExportVersionCounter(), theXMLTypePackage.getInt(), "exportVersionCounter", "0", 0, 1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getStateMachineSet_Id(), ecorePackage.getEString(), "id", "", 1, 1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineSet_ExecutionConfig(), this.getExecutionConfig(), null, "executionConfig", null, 1, 1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineSet_Observers(), theConditionsPackage.getAbstractObserver(), null, "observers", null, 0, -1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStateMachineSet_StateMachines(), theStatemachinePackage.getStateMachine(), null, "stateMachines", null, 0, -1, StateMachineSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getStateMachineSet__IsValid_hasValidDescription__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getStateMachineSet__UpdateID(), null, "updateID", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getStateMachineSet__CreateUUID(), ecorePackage.getEString(), "createUUID", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getStateMachineSet__IncrementExportVersionCounter(), null, "incrementExportVersionCounter", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getStateMachineSet__GetProjectName(), ecorePackage.getEString(), "getProjectName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(keyValuePairUserConfigEClass, KeyValuePairUserConfig.class, "KeyValuePairUserConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getKeyValuePairUserConfig_Key(), ecorePackage.getEString(), "key", null, 1, 1, KeyValuePairUserConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getKeyValuePairUserConfig_Value(), ecorePackage.getEString(), "value", null, 0, 1, KeyValuePairUserConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(executionConfigEClass, ExecutionConfig.class, "ExecutionConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionConfig_NeedsEthernet(), ecorePackage.getEBoolean(), "needsEthernet", "false", 0, 1, ExecutionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionConfig_NeedsErrorFrames(), ecorePackage.getEBoolean(), "needsErrorFrames", "false", 0, 1, ExecutionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionConfig_KeyValuePairs(), this.getKeyValuePairUserConfig(), null, "keyValuePairs", null, 0, -1, ExecutionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionConfig_LogLevel(), ecorePackage.getEInt(), "logLevel", "40", 1, 1, ExecutionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionConfig_SorterValue(), ecorePackage.getEInt(), "sorterValue", "10000", 1, 1, ExecutionConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getExecutionConfig__IsValid_hasValidLogLevel__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "isValid_hasValidLogLevel", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getExecutionConfig__IsValid_hasValidSorterValue__DiagnosticChain_Map(), ecorePackage.getEBoolean(), "isValid_hasValidSorterValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/edapt
		createEdaptAnnotations();
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/edapt</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEdaptAnnotations() {
		String source = "http://www.eclipse.org/edapt";
		addAnnotation
		  (this,
		   source,
		   new String[] {
			   "historyURI", "modeller.history"
		   });
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
		addAnnotation
		  (documentRootEClass,
		   source,
		   new String[] {
			   "name", "",
			   "kind", "mixed"
		   });
		addAnnotation
		  (getDocumentRoot_XMLNSPrefixMap(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xmlns:prefix"
		   });
		addAnnotation
		  (getDocumentRoot_XSISchemaLocation(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "xsi:schemaLocation"
		   });
		addAnnotation
		  (getDocumentRoot_StateMachineSet(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "stateMachineSet",
			   "namespace", "##targetNamespace"
		   });
		addAnnotation
		  (getGeneralInfo_Author(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "author"
		   });
		addAnnotation
		  (getGeneralInfo_Description(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "description"
		   });
		addAnnotation
		  (getGeneralInfo_Topic(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "topic"
		   });
		addAnnotation
		  (getGeneralInfo_Department(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "department"
		   });
		addAnnotation
		  (outputEClass,
		   source,
		   new String[] {
			   "name", "output",
			   "kind", "empty"
		   });
		addAnnotation
		  (getOutput_TargetFilePath(),
		   source,
		   new String[] {
			   "kind", "attribute",
			   "name", "targetFilePath"
		   });
		addAnnotation
		  (stateMachineSetEClass,
		   source,
		   new String[] {
			   "name", "stateMachineSet",
			   "kind", "element"
		   });
		addAnnotation
		  (getStateMachineSet_Output(),
		   source,
		   new String[] {
			   "kind", "element",
			   "name", "output"
		   });
	}

} //StatemachinesetPackageImpl
