/**
 */
package statemachine.impl;

import conditions.AbstractVariable;
import conditions.Condition;
import conditions.ConditionsPackage;
import conditions.IVariableReaderWriter;

import conditions.impl.BaseClassWithIDImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachine.AbstractAction;
import statemachine.AbstractState;
import statemachine.SmardTraceElement;
import statemachine.StatemachinePackage;
import statemachine.Transition;

import statemachine.util.StatemachineValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.impl.TransitionImpl#getName <em>Name</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getFrom <em>From</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getTo <em>To</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getRootcause <em>Rootcause</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link statemachine.impl.TransitionImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends BaseClassWithIDImpl implements Transition {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFrom() <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrom()
	 * @generated
	 * @ordered
	 */
	protected AbstractState from;

	/**
	 * The cached value of the '{@link #getTo() <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTo()
	 * @generated
	 * @ordered
	 */
	protected AbstractState to;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition> conditions;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String LOG_LEVEL_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected String logLevel = LOG_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getRootcause() <em>Rootcause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootcause()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOTCAUSE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRootcause() <em>Rootcause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootcause()
	 * @generated
	 * @ordered
	 */
	protected String rootcause = ROOTCAUSE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractAction> actions;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected AbstractVariable environment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractState getFrom() {
		if (from != null && from.eIsProxy()) {
			InternalEObject oldFrom = (InternalEObject)from;
			from = (AbstractState)eResolveProxy(oldFrom);
			if (from != oldFrom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.TRANSITION__FROM, oldFrom, from));
			}
		}
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractState basicGetFrom() {
		return from;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFrom(AbstractState newFrom, NotificationChain msgs) {
		AbstractState oldFrom = from;
		from = newFrom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__FROM, oldFrom, newFrom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrom(AbstractState newFrom) {
		if (newFrom != from) {
			NotificationChain msgs = null;
			if (from != null)
				msgs = ((InternalEObject)from).eInverseRemove(this, StatemachinePackage.ABSTRACT_STATE__OUT, AbstractState.class, msgs);
			if (newFrom != null)
				msgs = ((InternalEObject)newFrom).eInverseAdd(this, StatemachinePackage.ABSTRACT_STATE__OUT, AbstractState.class, msgs);
			msgs = basicSetFrom(newFrom, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__FROM, newFrom, newFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractState getTo() {
		if (to != null && to.eIsProxy()) {
			InternalEObject oldTo = (InternalEObject)to;
			to = (AbstractState)eResolveProxy(oldTo);
			if (to != oldTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.TRANSITION__TO, oldTo, to));
			}
		}
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractState basicGetTo() {
		return to;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTo(AbstractState newTo, NotificationChain msgs) {
		AbstractState oldTo = to;
		to = newTo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__TO, oldTo, newTo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTo(AbstractState newTo) {
		if (newTo != to) {
			NotificationChain msgs = null;
			if (to != null)
				msgs = ((InternalEObject)to).eInverseRemove(this, StatemachinePackage.ABSTRACT_STATE__IN, AbstractState.class, msgs);
			if (newTo != null)
				msgs = ((InternalEObject)newTo).eInverseAdd(this, StatemachinePackage.ABSTRACT_STATE__IN, AbstractState.class, msgs);
			msgs = basicSetTo(newTo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__TO, newTo, newTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Condition> getConditions() {
		if (conditions == null) {
			conditions = new EObjectResolvingEList<Condition>(Condition.class, this, StatemachinePackage.TRANSITION__CONDITIONS);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLogLevel() {
		return logLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLogLevel(String newLogLevel) {
		String oldLogLevel = logLevel;
		logLevel = newLogLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__LOG_LEVEL, oldLogLevel, logLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRootcause() {
		return rootcause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRootcause(String newRootcause) {
		String oldRootcause = rootcause;
		rootcause = newRootcause;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__ROOTCAUSE, oldRootcause, rootcause));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractAction> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<AbstractAction>(AbstractAction.class, this, StatemachinePackage.TRANSITION__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractVariable getEnvironment() {
		if (environment != null && environment.eIsProxy()) {
			InternalEObject oldEnvironment = (InternalEObject)environment;
			environment = (AbstractVariable)eResolveProxy(oldEnvironment);
			if (environment != oldEnvironment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.TRANSITION__ENVIRONMENT, oldEnvironment, environment));
			}
		}
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractVariable basicGetEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnvironment(AbstractVariable newEnvironment) {
		AbstractVariable oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.TRANSITION__ENVIRONMENT, oldEnvironment, environment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.TRANSITION__IS_VALID_HAS_VALID_DESCRIPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDescription", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.TRANSITION__IS_VALID_HAS_VALID_NAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidName", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.TRANSITION__IS_VALID_HAS_VALID_LOG_LEVEL,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidLogLevel", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidRootCause(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.TRANSITION__IS_VALID_HAS_VALID_ROOT_CAUSE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidRootCause", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidConditions(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.TRANSITION__IS_VALID_HAS_VALID_CONDITIONS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidConditions", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier(String projectName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__FROM:
				if (from != null)
					msgs = ((InternalEObject)from).eInverseRemove(this, StatemachinePackage.ABSTRACT_STATE__OUT, AbstractState.class, msgs);
				return basicSetFrom((AbstractState)otherEnd, msgs);
			case StatemachinePackage.TRANSITION__TO:
				if (to != null)
					msgs = ((InternalEObject)to).eInverseRemove(this, StatemachinePackage.ABSTRACT_STATE__IN, AbstractState.class, msgs);
				return basicSetTo((AbstractState)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__FROM:
				return basicSetFrom(null, msgs);
			case StatemachinePackage.TRANSITION__TO:
				return basicSetTo(null, msgs);
			case StatemachinePackage.TRANSITION__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__NAME:
				return getName();
			case StatemachinePackage.TRANSITION__FROM:
				if (resolve) return getFrom();
				return basicGetFrom();
			case StatemachinePackage.TRANSITION__TO:
				if (resolve) return getTo();
				return basicGetTo();
			case StatemachinePackage.TRANSITION__CONDITIONS:
				return getConditions();
			case StatemachinePackage.TRANSITION__DESCRIPTION:
				return getDescription();
			case StatemachinePackage.TRANSITION__LOG_LEVEL:
				return getLogLevel();
			case StatemachinePackage.TRANSITION__ROOTCAUSE:
				return getRootcause();
			case StatemachinePackage.TRANSITION__ACTIONS:
				return getActions();
			case StatemachinePackage.TRANSITION__ENVIRONMENT:
				if (resolve) return getEnvironment();
				return basicGetEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__NAME:
				setName((String)newValue);
				return;
			case StatemachinePackage.TRANSITION__FROM:
				setFrom((AbstractState)newValue);
				return;
			case StatemachinePackage.TRANSITION__TO:
				setTo((AbstractState)newValue);
				return;
			case StatemachinePackage.TRANSITION__CONDITIONS:
				getConditions().clear();
				getConditions().addAll((Collection<? extends Condition>)newValue);
				return;
			case StatemachinePackage.TRANSITION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case StatemachinePackage.TRANSITION__LOG_LEVEL:
				setLogLevel((String)newValue);
				return;
			case StatemachinePackage.TRANSITION__ROOTCAUSE:
				setRootcause((String)newValue);
				return;
			case StatemachinePackage.TRANSITION__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends AbstractAction>)newValue);
				return;
			case StatemachinePackage.TRANSITION__ENVIRONMENT:
				setEnvironment((AbstractVariable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StatemachinePackage.TRANSITION__FROM:
				setFrom((AbstractState)null);
				return;
			case StatemachinePackage.TRANSITION__TO:
				setTo((AbstractState)null);
				return;
			case StatemachinePackage.TRANSITION__CONDITIONS:
				getConditions().clear();
				return;
			case StatemachinePackage.TRANSITION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case StatemachinePackage.TRANSITION__LOG_LEVEL:
				setLogLevel(LOG_LEVEL_EDEFAULT);
				return;
			case StatemachinePackage.TRANSITION__ROOTCAUSE:
				setRootcause(ROOTCAUSE_EDEFAULT);
				return;
			case StatemachinePackage.TRANSITION__ACTIONS:
				getActions().clear();
				return;
			case StatemachinePackage.TRANSITION__ENVIRONMENT:
				setEnvironment((AbstractVariable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinePackage.TRANSITION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StatemachinePackage.TRANSITION__FROM:
				return from != null;
			case StatemachinePackage.TRANSITION__TO:
				return to != null;
			case StatemachinePackage.TRANSITION__CONDITIONS:
				return conditions != null && !conditions.isEmpty();
			case StatemachinePackage.TRANSITION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case StatemachinePackage.TRANSITION__LOG_LEVEL:
				return LOG_LEVEL_EDEFAULT == null ? logLevel != null : !LOG_LEVEL_EDEFAULT.equals(logLevel);
			case StatemachinePackage.TRANSITION__ROOTCAUSE:
				return ROOTCAUSE_EDEFAULT == null ? rootcause != null : !ROOTCAUSE_EDEFAULT.equals(rootcause);
			case StatemachinePackage.TRANSITION__ACTIONS:
				return actions != null && !actions.isEmpty();
			case StatemachinePackage.TRANSITION__ENVIRONMENT:
				return environment != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == SmardTraceElement.class) {
			switch (baseOperationID) {
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER: return StatemachinePackage.TRANSITION___GET_IDENTIFIER;
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER__STRING: return StatemachinePackage.TRANSITION___GET_IDENTIFIER__STRING;
				default: return -1;
			}
		}
		if (baseClass == IVariableReaderWriter.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_READ_VARIABLES: return StatemachinePackage.TRANSITION___GET_READ_VARIABLES;
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_WRITE_VARIABLES: return StatemachinePackage.TRANSITION___GET_WRITE_VARIABLES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinePackage.TRANSITION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDescription((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.TRANSITION___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidName((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.TRANSITION___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidLogLevel((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.TRANSITION___IS_VALID_HAS_VALID_ROOT_CAUSE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidRootCause((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.TRANSITION___IS_VALID_HAS_VALID_CONDITIONS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidConditions((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.TRANSITION___GET_READ_VARIABLES:
				return getReadVariables();
			case StatemachinePackage.TRANSITION___GET_WRITE_VARIABLES:
				return getWriteVariables();
			case StatemachinePackage.TRANSITION___GET_IDENTIFIER:
				return getIdentifier();
			case StatemachinePackage.TRANSITION___GET_IDENTIFIER__STRING:
				return getIdentifier((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", logLevel: ");
		result.append(logLevel);
		result.append(", rootcause: ");
		result.append(rootcause);
		result.append(')');
		return result.toString();
	}

} //TransitionImpl
