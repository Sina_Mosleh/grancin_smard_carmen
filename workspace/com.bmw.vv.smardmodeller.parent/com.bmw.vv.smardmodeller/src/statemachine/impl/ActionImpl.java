/**
 */
package statemachine.impl;

import conditions.AbstractVariable;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.Action;
import statemachine.ActionTypeNotEmpty;
import statemachine.SmardTraceElement;
import statemachine.StatemachinePackage;

import statemachine.util.StatemachineValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.impl.ActionImpl#getActionType <em>Action Type</em>}</li>
 *   <li>{@link statemachine.impl.ActionImpl#getActionExpression <em>Action Expression</em>}</li>
 *   <li>{@link statemachine.impl.ActionImpl#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}</li>
 *   <li>{@link statemachine.impl.ActionImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionImpl extends AbstractActionImpl implements Action {
	/**
	 * The default value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionType()
	 * @generated
	 * @ordered
	 */
	protected static final ActionTypeNotEmpty ACTION_TYPE_EDEFAULT = ActionTypeNotEmpty.COMPUTE;

	/**
	 * The cached value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionType()
	 * @generated
	 * @ordered
	 */
	protected ActionTypeNotEmpty actionType = ACTION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTION_EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionExpression()
	 * @generated
	 * @ordered
	 */
	protected String actionExpression = ACTION_EXPRESSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getActionTypeTmplParam() <em>Action Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTION_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActionTypeTmplParam() <em>Action Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String actionTypeTmplParam = ACTION_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected AbstractVariable environment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActionTypeNotEmpty getActionType() {
		return actionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActionType(ActionTypeNotEmpty newActionType) {
		ActionTypeNotEmpty oldActionType = actionType;
		actionType = newActionType == null ? ACTION_TYPE_EDEFAULT : newActionType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_TYPE, oldActionType, actionType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getActionExpression() {
		return actionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActionExpression(String newActionExpression) {
		String oldActionExpression = actionExpression;
		actionExpression = newActionExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_EXPRESSION, oldActionExpression, actionExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getActionTypeTmplParam() {
		return actionTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActionTypeTmplParam(String newActionTypeTmplParam) {
		String oldActionTypeTmplParam = actionTypeTmplParam;
		actionTypeTmplParam = newActionTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM, oldActionTypeTmplParam, actionTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AbstractVariable getEnvironment() {
		if (environment != null && environment.eIsProxy()) {
			InternalEObject oldEnvironment = (InternalEObject)environment;
			environment = (AbstractVariable)eResolveProxy(oldEnvironment);
			if (environment != oldEnvironment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.ACTION__ENVIRONMENT, oldEnvironment, environment));
			}
		}
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractVariable basicGetEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEnvironment(AbstractVariable newEnvironment) {
		AbstractVariable oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ENVIRONMENT, oldEnvironment, environment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.ACTION__IS_VALID_HAS_VALID_ACTION_EXPRESSION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidActionExpression", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidActionType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.ACTION__IS_VALID_HAS_VALID_ACTION_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidActionType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier(String projectName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinePackage.ACTION__ACTION_TYPE:
				return getActionType();
			case StatemachinePackage.ACTION__ACTION_EXPRESSION:
				return getActionExpression();
			case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
				return getActionTypeTmplParam();
			case StatemachinePackage.ACTION__ENVIRONMENT:
				if (resolve) return getEnvironment();
				return basicGetEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinePackage.ACTION__ACTION_TYPE:
				setActionType((ActionTypeNotEmpty)newValue);
				return;
			case StatemachinePackage.ACTION__ACTION_EXPRESSION:
				setActionExpression((String)newValue);
				return;
			case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
				setActionTypeTmplParam((String)newValue);
				return;
			case StatemachinePackage.ACTION__ENVIRONMENT:
				setEnvironment((AbstractVariable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ACTION__ACTION_TYPE:
				setActionType(ACTION_TYPE_EDEFAULT);
				return;
			case StatemachinePackage.ACTION__ACTION_EXPRESSION:
				setActionExpression(ACTION_EXPRESSION_EDEFAULT);
				return;
			case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
				setActionTypeTmplParam(ACTION_TYPE_TMPL_PARAM_EDEFAULT);
				return;
			case StatemachinePackage.ACTION__ENVIRONMENT:
				setEnvironment((AbstractVariable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ACTION__ACTION_TYPE:
				return actionType != ACTION_TYPE_EDEFAULT;
			case StatemachinePackage.ACTION__ACTION_EXPRESSION:
				return ACTION_EXPRESSION_EDEFAULT == null ? actionExpression != null : !ACTION_EXPRESSION_EDEFAULT.equals(actionExpression);
			case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
				return ACTION_TYPE_TMPL_PARAM_EDEFAULT == null ? actionTypeTmplParam != null : !ACTION_TYPE_TMPL_PARAM_EDEFAULT.equals(actionTypeTmplParam);
			case StatemachinePackage.ACTION__ENVIRONMENT:
				return environment != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == SmardTraceElement.class) {
			switch (baseOperationID) {
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER: return StatemachinePackage.ACTION___GET_IDENTIFIER;
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER__STRING: return StatemachinePackage.ACTION___GET_IDENTIFIER__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinePackage.ACTION___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidActionExpression((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.ACTION___IS_VALID_HAS_VALID_ACTION_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidActionType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.ACTION___GET_IDENTIFIER:
				return getIdentifier();
			case StatemachinePackage.ACTION___GET_IDENTIFIER__STRING:
				return getIdentifier((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (actionType: ");
		result.append(actionType);
		result.append(", actionExpression: ");
		result.append(actionExpression);
		result.append(", actionTypeTmplParam: ");
		result.append(actionTypeTmplParam);
		result.append(')');
		return result.toString();
	}

} //ActionImpl
