/**
 */
package statemachine.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see statemachine.util.StatemachineResourceFactoryImpl
 * @generated
 */
public class StatemachineResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public StatemachineResourceImpl(URI uri) {
		super(uri);
	}

} //StatemachineResourceImpl
