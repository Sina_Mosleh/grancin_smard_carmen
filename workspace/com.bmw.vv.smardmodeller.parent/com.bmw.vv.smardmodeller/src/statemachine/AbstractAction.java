/**
 */
package statemachine;

import conditions.BaseClassWithID;
import conditions.IVariableReaderWriter;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.AbstractAction#getDescription <em>Description</em>}</li>
 *   <li>{@link statemachine.AbstractAction#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getAbstractAction()
 * @model abstract="true"
 * @generated
 */
public interface AbstractAction extends BaseClassWithID, IVariableReaderWriter {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see statemachine.StatemachinePackage#getAbstractAction_Description()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link statemachine.AbstractAction#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * The default value is <code>"onEntry"</code>.
	 * The literals are from the enumeration {@link statemachine.Trigger}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see statemachine.Trigger
	 * @see #setTrigger(Trigger)
	 * @see statemachine.StatemachinePackage#getAbstractAction_Trigger()
	 * @model default="onEntry" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	Trigger getTrigger();

	/**
	 * Sets the value of the '{@link statemachine.AbstractAction#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see statemachine.Trigger
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(Trigger value);

	/**
	 * Returns the value of the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger Tmpl Param</em>' attribute.
	 * @see #setTriggerTmplParam(String)
	 * @see statemachine.StatemachinePackage#getAbstractAction_TriggerTmplParam()
	 * @model
	 * @generated
	 */
	String getTriggerTmplParam();

	/**
	 * Sets the value of the '{@link statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger Tmpl Param</em>' attribute.
	 * @see #getTriggerTmplParam()
	 * @generated
	 */
	void setTriggerTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidTrigger(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AbstractAction
