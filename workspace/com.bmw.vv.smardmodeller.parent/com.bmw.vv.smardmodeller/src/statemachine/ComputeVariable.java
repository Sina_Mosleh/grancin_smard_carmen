/**
 */
package statemachine;

import conditions.IComputeVariableActionOperand;
import conditions.IOperation;
import conditions.ValueVariable;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compute Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}</li>
 *   <li>{@link statemachine.ComputeVariable#getTarget <em>Target</em>}</li>
 *   <li>{@link statemachine.ComputeVariable#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getComputeVariable()
 * @model
 * @generated
 */
public interface ComputeVariable extends AbstractAction, IOperation {
	/**
	 * Returns the value of the '<em><b>Action Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action Expression</em>' attribute.
	 * @see #setActionExpression(String)
	 * @see statemachine.StatemachinePackage#getComputeVariable_ActionExpression()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getActionExpression();

	/**
	 * Sets the value of the '{@link statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action Expression</em>' attribute.
	 * @see #getActionExpression()
	 * @generated
	 */
	void setActionExpression(String value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ValueVariable)
	 * @see statemachine.StatemachinePackage#getComputeVariable_Target()
	 * @model required="true"
	 * @generated
	 */
	ValueVariable getTarget();

	/**
	 * Sets the value of the '{@link statemachine.ComputeVariable#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ValueVariable value);

	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.IComputeVariableActionOperand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see statemachine.StatemachinePackage#getComputeVariable_Operands()
	 * @model containment="true"
	 * @generated
	 */
	EList<IComputeVariableActionOperand> getOperands();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComputeVariable
