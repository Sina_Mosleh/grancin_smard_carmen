/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extract</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.Extract#getStringToExtract <em>String To Extract</em>}</li>
 *   <li>{@link conditions.Extract#getGroupIndex <em>Group Index</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getExtract()
 * @model
 * @generated
 */
public interface Extract extends IStringOperation, RegexOperation {
	/**
	 * Returns the value of the '<em><b>String To Extract</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To Extract</em>' containment reference.
	 * @see #setStringToExtract(IStringOperand)
	 * @see conditions.ConditionsPackage#getExtract_StringToExtract()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IStringOperand getStringToExtract();

	/**
	 * Sets the value of the '{@link conditions.Extract#getStringToExtract <em>String To Extract</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String To Extract</em>' containment reference.
	 * @see #getStringToExtract()
	 * @generated
	 */
	void setStringToExtract(IStringOperand value);

	/**
	 * Returns the value of the '<em><b>Group Index</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group Index</em>' attribute.
	 * @see #setGroupIndex(int)
	 * @see conditions.ConditionsPackage#getExtract_GroupIndex()
	 * @model default="0"
	 * @generated
	 */
	int getGroupIndex();

	/**
	 * Sets the value of the '{@link conditions.Extract#getGroupIndex <em>Group Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Group Index</em>' attribute.
	 * @see #getGroupIndex()
	 * @generated
	 */
	void setGroupIndex(int value);

} // Extract
