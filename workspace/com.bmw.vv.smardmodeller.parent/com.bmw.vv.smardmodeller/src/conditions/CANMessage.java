/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CAN Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.CANMessage#getCanFilter <em>Can Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getCANMessage()
 * @model
 * @generated
 */
public interface CANMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Can Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Can Filter</em>' containment reference.
	 * @see #setCanFilter(CANFilter)
	 * @see conditions.ConditionsPackage#getCANMessage_CanFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CANFilter getCanFilter();

	/**
	 * Sets the value of the '{@link conditions.CANMessage#getCanFilter <em>Can Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Can Filter</em>' containment reference.
	 * @see #getCanFilter()
	 * @generated
	 */
	void setCanFilter(CANFilter value);

} // CANMessage
