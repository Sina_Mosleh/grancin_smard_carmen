/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>True Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getTrueExpression()
 * @model extendedMetaData="name='trueCondition' kind='empty'"
 * @generated
 */
public interface TrueExpression extends Expression {
} // TrueExpression
