/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UDP Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getUDPMessage()
 * @model
 * @generated
 */
public interface UDPMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Udp Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Udp Filter</em>' containment reference.
	 * @see #setUdpFilter(UDPFilter)
	 * @see conditions.ConditionsPackage#getUDPMessage_UdpFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	UDPFilter getUdpFilter();

	/**
	 * Sets the value of the '{@link conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Udp Filter</em>' containment reference.
	 * @see #getUdpFilter()
	 * @generated
	 */
	void setUdpFilter(UDPFilter value);

} // UDPMessage
