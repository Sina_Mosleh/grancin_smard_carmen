/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>AUTOSAR Data Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getAUTOSARDataType()
 * @model
 * @generated
 */
public enum AUTOSARDataType implements Enumerator {
	/**
	 * The '<em><b>AUNICODE2STRING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUNICODE2STRING_VALUE
	 * @generated
	 * @ordered
	 */
	AUNICODE2STRING(0, "A_UNICODE2STRING", "A_UNICODE2STRING"),

	/**
	 * The '<em><b>AASCIISTRING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AASCIISTRING_VALUE
	 * @generated
	 * @ordered
	 */
	AASCIISTRING(1, "A_ASCIISTRING", "A_ASCIISTRING"),

	/**
	 * The '<em><b>AINT8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT8_VALUE
	 * @generated
	 * @ordered
	 */
	AINT8(2, "A_INT8", "A_INT8"),

	/**
	 * The '<em><b>AINT16</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT16_VALUE
	 * @generated
	 * @ordered
	 */
	AINT16(3, "A_INT16", "A_INT16"),

	/**
	 * The '<em><b>AINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AINT32(4, "A_INT32", "A_INT32"),

	/**
	 * The '<em><b>AUINT8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT8_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT8(5, "A_UINT8", "A_UINT8"),

	/**
	 * The '<em><b>AUINT16</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT16_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT16(6, "A_UINT16", "A_UINT16"),

	/**
	 * The '<em><b>AUINT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT32(7, "A_UINT32", "A_UINT32"),

	/**
	 * The '<em><b>AFLOAT32</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT32_VALUE
	 * @generated
	 * @ordered
	 */
	AFLOAT32(8, "A_FLOAT32", "A_FLOAT32"),

	/**
	 * The '<em><b>AFLOAT64</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT64_VALUE
	 * @generated
	 * @ordered
	 */
	AFLOAT64(9, "A_FLOAT64", "A_FLOAT64"),

	/**
	 * The '<em><b>AUTF8STRING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUTF8STRING_VALUE
	 * @generated
	 * @ordered
	 */
	AUTF8STRING(10, "A_UTF8STRING", "A_UTF8STRING"),

	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

	/**
	 * The '<em><b>AINT64</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT64_VALUE
	 * @generated
	 * @ordered
	 */
	AINT64(11, "A_INT64", "A_INT64"),

	/**
	 * The '<em><b>AUINT64</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT64_VALUE
	 * @generated
	 * @ordered
	 */
	AUINT64(12, "A_UINT64", "A_UINT64");

	/**
	 * The '<em><b>AUNICODE2STRING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUNICODE2STRING
	 * @model name="A_UNICODE2STRING"
	 * @generated
	 * @ordered
	 */
	public static final int AUNICODE2STRING_VALUE = 0;

	/**
	 * The '<em><b>AASCIISTRING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AASCIISTRING
	 * @model name="A_ASCIISTRING"
	 * @generated
	 * @ordered
	 */
	public static final int AASCIISTRING_VALUE = 1;

	/**
	 * The '<em><b>AINT8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT8
	 * @model name="A_INT8"
	 * @generated
	 * @ordered
	 */
	public static final int AINT8_VALUE = 2;

	/**
	 * The '<em><b>AINT16</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT16
	 * @model name="A_INT16"
	 * @generated
	 * @ordered
	 */
	public static final int AINT16_VALUE = 3;

	/**
	 * The '<em><b>AINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT32
	 * @model name="A_INT32"
	 * @generated
	 * @ordered
	 */
	public static final int AINT32_VALUE = 4;

	/**
	 * The '<em><b>AUINT8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT8
	 * @model name="A_UINT8"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT8_VALUE = 5;

	/**
	 * The '<em><b>AUINT16</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT16
	 * @model name="A_UINT16"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT16_VALUE = 6;

	/**
	 * The '<em><b>AUINT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT32
	 * @model name="A_UINT32"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT32_VALUE = 7;

	/**
	 * The '<em><b>AFLOAT32</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT32
	 * @model name="A_FLOAT32"
	 * @generated
	 * @ordered
	 */
	public static final int AFLOAT32_VALUE = 8;

	/**
	 * The '<em><b>AFLOAT64</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AFLOAT64
	 * @model name="A_FLOAT64"
	 * @generated
	 * @ordered
	 */
	public static final int AFLOAT64_VALUE = 9;

	/**
	 * The '<em><b>AUTF8STRING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUTF8STRING
	 * @model name="A_UTF8STRING"
	 * @generated
	 * @ordered
	 */
	public static final int AUTF8STRING_VALUE = 10;

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * The '<em><b>AINT64</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AINT64
	 * @model name="A_INT64"
	 * @generated
	 * @ordered
	 */
	public static final int AINT64_VALUE = 11;

	/**
	 * The '<em><b>AUINT64</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AUINT64
	 * @model name="A_UINT64"
	 * @generated
	 * @ordered
	 */
	public static final int AUINT64_VALUE = 12;

	/**
	 * An array of all the '<em><b>AUTOSAR Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AUTOSARDataType[] VALUES_ARRAY =
		new AUTOSARDataType[] {
			AUNICODE2STRING,
			AASCIISTRING,
			AINT8,
			AINT16,
			AINT32,
			AUINT8,
			AUINT16,
			AUINT32,
			AFLOAT32,
			AFLOAT64,
			AUTF8STRING,
			TEMPLATE_DEFINED,
			AINT64,
			AUINT64,
		};

	/**
	 * A public read-only list of all the '<em><b>AUTOSAR Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AUTOSARDataType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>AUTOSAR Data Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AUTOSARDataType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AUTOSARDataType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>AUTOSAR Data Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AUTOSARDataType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AUTOSARDataType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>AUTOSAR Data Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static AUTOSARDataType get(int value) {
		switch (value) {
			case AUNICODE2STRING_VALUE: return AUNICODE2STRING;
			case AASCIISTRING_VALUE: return AASCIISTRING;
			case AINT8_VALUE: return AINT8;
			case AINT16_VALUE: return AINT16;
			case AINT32_VALUE: return AINT32;
			case AUINT8_VALUE: return AUINT8;
			case AUINT16_VALUE: return AUINT16;
			case AUINT32_VALUE: return AUINT32;
			case AFLOAT32_VALUE: return AFLOAT32;
			case AFLOAT64_VALUE: return AFLOAT64;
			case AUTF8STRING_VALUE: return AUTF8STRING;
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
			case AINT64_VALUE: return AINT64;
			case AUINT64_VALUE: return AUINT64;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AUTOSARDataType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AUTOSARDataType
