/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IPSD Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSomeIPSDMessage()
 * @model
 * @generated
 */
public interface SomeIPSDMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Some IPSD Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Some IPSD Filter</em>' containment reference.
	 * @see #setSomeIPSDFilter(SomeIPSDFilter)
	 * @see conditions.ConditionsPackage#getSomeIPSDMessage_SomeIPSDFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	SomeIPSDFilter getSomeIPSDFilter();

	/**
	 * Sets the value of the '{@link conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Some IPSD Filter</em>' containment reference.
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	void setSomeIPSDFilter(SomeIPSDFilter value);

} // SomeIPSDMessage
