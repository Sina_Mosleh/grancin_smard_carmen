/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regex Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.RegexOperation#getRegex <em>Regex</em>}</li>
 *   <li>{@link conditions.RegexOperation#getDynamicRegex <em>Dynamic Regex</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getRegexOperation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface RegexOperation extends EObject {
	/**
	 * Returns the value of the '<em><b>Regex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regex</em>' attribute.
	 * @see #setRegex(String)
	 * @see conditions.ConditionsPackage#getRegexOperation_Regex()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getRegex();

	/**
	 * Sets the value of the '{@link conditions.RegexOperation#getRegex <em>Regex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Regex</em>' attribute.
	 * @see #getRegex()
	 * @generated
	 */
	void setRegex(String value);

	/**
	 * Returns the value of the '<em><b>Dynamic Regex</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Regex</em>' containment reference.
	 * @see #setDynamicRegex(IStringOperand)
	 * @see conditions.ConditionsPackage#getRegexOperation_DynamicRegex()
	 * @model containment="true"
	 * @generated
	 */
	IStringOperand getDynamicRegex();

	/**
	 * Sets the value of the '{@link conditions.RegexOperation#getDynamicRegex <em>Dynamic Regex</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dynamic Regex</em>' containment reference.
	 * @see #getDynamicRegex()
	 * @generated
	 */
	void setDynamicRegex(IStringOperand value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context);

} // RegexOperation
