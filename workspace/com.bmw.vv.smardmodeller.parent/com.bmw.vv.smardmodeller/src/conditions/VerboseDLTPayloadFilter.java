/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Verbose DLT Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getVerboseDLTPayloadFilter()
 * @model
 * @generated
 */
public interface VerboseDLTPayloadFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Regex</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regex</em>' attribute.
	 * @see #setRegex(String)
	 * @see conditions.ConditionsPackage#getVerboseDLTPayloadFilter_Regex()
	 * @model default="" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getRegex();

	/**
	 * Sets the value of the '{@link conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Regex</em>' attribute.
	 * @see #getRegex()
	 * @generated
	 */
	void setRegex(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context);

} // VerboseDLTPayloadFilter
