/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Verbose DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}</li>
 *   <li>{@link conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getNonVerboseDLTFilter()
 * @model
 * @generated
 */
public interface NonVerboseDLTFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Message Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Id</em>' attribute.
	 * @see #setMessageId(String)
	 * @see conditions.ConditionsPackage#getNonVerboseDLTFilter_MessageId()
	 * @model dataType="conditions.HexOrIntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getMessageId();

	/**
	 * Sets the value of the '{@link conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Id</em>' attribute.
	 * @see #getMessageId()
	 * @generated
	 */
	void setMessageId(String value);

	/**
	 * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Id Range</em>' attribute.
	 * @see #setMessageIdRange(String)
	 * @see conditions.ConditionsPackage#getNonVerboseDLTFilter_MessageIdRange()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getMessageIdRange();

	/**
	 * Sets the value of the '{@link conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Id Range</em>' attribute.
	 * @see #getMessageIdRange()
	 * @generated
	 */
	void setMessageIdRange(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageIdOrMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

} // NonVerboseDLTFilter
