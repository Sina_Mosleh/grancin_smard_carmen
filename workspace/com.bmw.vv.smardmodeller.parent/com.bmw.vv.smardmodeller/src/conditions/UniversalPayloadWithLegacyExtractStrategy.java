/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Universal Payload With Legacy Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit <em>Start Bit</em>}</li>
 *   <li>{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getLength <em>Length</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getUniversalPayloadWithLegacyExtractStrategy()
 * @model
 * @generated
 */
public interface UniversalPayloadWithLegacyExtractStrategy extends UniversalPayloadExtractStrategy {
	/**
	 * Returns the value of the '<em><b>Start Bit</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Bit</em>' attribute.
	 * @see #setStartBit(String)
	 * @see conditions.ConditionsPackage#getUniversalPayloadWithLegacyExtractStrategy_StartBit()
	 * @model default="0" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getStartBit();

	/**
	 * Sets the value of the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit <em>Start Bit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Bit</em>' attribute.
	 * @see #getStartBit()
	 * @generated
	 */
	void setStartBit(String value);

	/**
	 * Returns the value of the '<em><b>Length</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Length</em>' attribute.
	 * @see #setLength(String)
	 * @see conditions.ConditionsPackage#getUniversalPayloadWithLegacyExtractStrategy_Length()
	 * @model default="1" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getLength();

	/**
	 * Sets the value of the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getLength <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Length</em>' attribute.
	 * @see #getLength()
	 * @generated
	 */
	void setLength(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidStartbit(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDataLength(DiagnosticChain diagnostics, Map<Object, Object> context);

} // UniversalPayloadWithLegacyExtractStrategy
