/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flex Ray Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getFlexRayMessage()
 * @model
 * @generated
 */
public interface FlexRayMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Flex Ray Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flex Ray Filter</em>' containment reference.
	 * @see #setFlexRayFilter(FlexRayFilter)
	 * @see conditions.ConditionsPackage#getFlexRayMessage_FlexRayFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FlexRayFilter getFlexRayFilter();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flex Ray Filter</em>' containment reference.
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	void setFlexRayFilter(FlexRayFilter value);

} // FlexRayMessage
