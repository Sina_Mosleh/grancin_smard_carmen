/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ethernet Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.EthernetFilter#getSourceMAC <em>Source MAC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getDestMAC <em>Dest MAC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getEtherType <em>Ether Type</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getInnerVlanId <em>Inner Vlan Id</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getOuterVlanId <em>Outer Vlan Id</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getInnerVlanCFI <em>Inner Vlan CFI</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getOuterVlanCFI <em>Outer Vlan CFI</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getInnerVlanVID <em>Inner Vlan VID</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getOuterVlanVID <em>Outer Vlan VID</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getCRC <em>CRC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getInnerVlanTPID <em>Inner Vlan TPID</em>}</li>
 *   <li>{@link conditions.EthernetFilter#getOuterVlanTPID <em>Outer Vlan TPID</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getEthernetFilter()
 * @model
 * @generated
 */
public interface EthernetFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Source MAC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source MAC</em>' attribute.
	 * @see #setSourceMAC(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_SourceMAC()
	 * @model dataType="conditions.MACPatternOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSourceMAC();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getSourceMAC <em>Source MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source MAC</em>' attribute.
	 * @see #getSourceMAC()
	 * @generated
	 */
	void setSourceMAC(String value);

	/**
	 * Returns the value of the '<em><b>Dest MAC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest MAC</em>' attribute.
	 * @see #setDestMAC(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_DestMAC()
	 * @model dataType="conditions.MACPatternOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getDestMAC();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getDestMAC <em>Dest MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest MAC</em>' attribute.
	 * @see #getDestMAC()
	 * @generated
	 */
	void setDestMAC(String value);

	/**
	 * Returns the value of the '<em><b>Ether Type</b></em>' attribute.
	 * The default value is <code>"0x0800"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ether Type</em>' attribute.
	 * @see #setEtherType(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_EtherType()
	 * @model default="0x0800" dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getEtherType();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getEtherType <em>Ether Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ether Type</em>' attribute.
	 * @see #getEtherType()
	 * @generated
	 */
	void setEtherType(String value);

	/**
	 * Returns the value of the '<em><b>Inner Vlan Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Vlan Id</em>' attribute.
	 * @see #setInnerVlanId(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_InnerVlanId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getInnerVlanId();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getInnerVlanId <em>Inner Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Vlan Id</em>' attribute.
	 * @see #getInnerVlanId()
	 * @generated
	 */
	void setInnerVlanId(String value);

	/**
	 * Returns the value of the '<em><b>Outer Vlan Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Vlan Id</em>' attribute.
	 * @see #setOuterVlanId(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_OuterVlanId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getOuterVlanId();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getOuterVlanId <em>Outer Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Vlan Id</em>' attribute.
	 * @see #getOuterVlanId()
	 * @generated
	 */
	void setOuterVlanId(String value);

	/**
	 * Returns the value of the '<em><b>Inner Vlan CFI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Vlan CFI</em>' attribute.
	 * @see #setInnerVlanCFI(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_InnerVlanCFI()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getInnerVlanCFI();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getInnerVlanCFI <em>Inner Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Vlan CFI</em>' attribute.
	 * @see #getInnerVlanCFI()
	 * @generated
	 */
	void setInnerVlanCFI(String value);

	/**
	 * Returns the value of the '<em><b>Outer Vlan CFI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Vlan CFI</em>' attribute.
	 * @see #setOuterVlanCFI(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_OuterVlanCFI()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getOuterVlanCFI();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getOuterVlanCFI <em>Outer Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Vlan CFI</em>' attribute.
	 * @see #getOuterVlanCFI()
	 * @generated
	 */
	void setOuterVlanCFI(String value);

	/**
	 * Returns the value of the '<em><b>Inner Vlan VID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Vlan VID</em>' attribute.
	 * @see #setInnerVlanVID(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_InnerVlanVID()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getInnerVlanVID();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getInnerVlanVID <em>Inner Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Vlan VID</em>' attribute.
	 * @see #getInnerVlanVID()
	 * @generated
	 */
	void setInnerVlanVID(String value);

	/**
	 * Returns the value of the '<em><b>Outer Vlan VID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Vlan VID</em>' attribute.
	 * @see #setOuterVlanVID(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_OuterVlanVID()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getOuterVlanVID();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getOuterVlanVID <em>Outer Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Vlan VID</em>' attribute.
	 * @see #getOuterVlanVID()
	 * @generated
	 */
	void setOuterVlanVID(String value);

	/**
	 * Returns the value of the '<em><b>CRC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CRC</em>' attribute.
	 * @see #setCRC(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_CRC()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getCRC();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getCRC <em>CRC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CRC</em>' attribute.
	 * @see #getCRC()
	 * @generated
	 */
	void setCRC(String value);

	/**
	 * Returns the value of the '<em><b>Inner Vlan TPID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Vlan TPID</em>' attribute.
	 * @see #setInnerVlanTPID(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_InnerVlanTPID()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getInnerVlanTPID();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getInnerVlanTPID <em>Inner Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Vlan TPID</em>' attribute.
	 * @see #getInnerVlanTPID()
	 * @generated
	 */
	void setInnerVlanTPID(String value);

	/**
	 * Returns the value of the '<em><b>Outer Vlan TPID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outer Vlan TPID</em>' attribute.
	 * @see #setOuterVlanTPID(String)
	 * @see conditions.ConditionsPackage#getEthernetFilter_OuterVlanTPID()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getOuterVlanTPID();

	/**
	 * Sets the value of the '{@link conditions.EthernetFilter#getOuterVlanTPID <em>Outer Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Outer Vlan TPID</em>' attribute.
	 * @see #getOuterVlanTPID()
	 * @generated
	 */
	void setOuterVlanTPID(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSourceMAC(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDestinationMAC(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidCRC(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidEtherType(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInnerVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOuterVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInnerVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOuterVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInnerVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOuterVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInnerVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOuterVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context);

} // EthernetFilter
