/**
 */
package conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Each Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ForEachExpression#getFilterExpressions <em>Filter Expressions</em>}</li>
 *   <li>{@link conditions.ForEachExpression#getContainerSignal <em>Container Signal</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getForEachExpression()
 * @model
 * @generated
 */
public interface ForEachExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Filter Expressions</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.Expression}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Expressions</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getForEachExpression_FilterExpressions()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Expression> getFilterExpressions();

	/**
	 * Returns the value of the '<em><b>Container Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Signal</em>' reference.
	 * @see #setContainerSignal(ContainerSignal)
	 * @see conditions.ConditionsPackage#getForEachExpression_ContainerSignal()
	 * @model
	 * @generated
	 */
	ContainerSignal getContainerSignal();

	/**
	 * Sets the value of the '{@link conditions.ForEachExpression#getContainerSignal <em>Container Signal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Signal</em>' reference.
	 * @see #getContainerSignal()
	 * @generated
	 */
	void setContainerSignal(ContainerSignal value);

} // ForEachExpression
