/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPv4 Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.IPv4Message#getIPv4Filter <em>IPv4 Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getIPv4Message()
 * @model
 * @generated
 */
public interface IPv4Message extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>IPv4 Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>IPv4 Filter</em>' containment reference.
	 * @see #setIPv4Filter(IPv4Filter)
	 * @see conditions.ConditionsPackage#getIPv4Message_IPv4Filter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IPv4Filter getIPv4Filter();

	/**
	 * Sets the value of the '{@link conditions.IPv4Message#getIPv4Filter <em>IPv4 Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>IPv4 Filter</em>' containment reference.
	 * @see #getIPv4Filter()
	 * @generated
	 */
	void setIPv4Filter(IPv4Filter value);

} // IPv4Message
