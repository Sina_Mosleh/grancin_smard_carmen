/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Header Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.HeaderSignal#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link conditions.HeaderSignal#getAttributeTmplParam <em>Attribute Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getHeaderSignal()
 * @model
 * @generated
 */
public interface HeaderSignal extends AbstractSignal {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' attribute.
	 * @see #setAttribute(String)
	 * @see conditions.ConditionsPackage#getHeaderSignal_Attribute()
	 * @model required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getAttribute();

	/**
	 * Sets the value of the '{@link conditions.HeaderSignal#getAttribute <em>Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' attribute.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(String value);

	/**
	 * Returns the value of the '<em><b>Attribute Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Tmpl Param</em>' attribute.
	 * @see #setAttributeTmplParam(String)
	 * @see conditions.ConditionsPackage#getHeaderSignal_AttributeTmplParam()
	 * @model
	 * @generated
	 */
	String getAttributeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.HeaderSignal#getAttributeTmplParam <em>Attribute Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Tmpl Param</em>' attribute.
	 * @see #getAttributeTmplParam()
	 * @generated
	 */
	void setAttributeTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidAttribute(DiagnosticChain diagnostics, Map<Object, Object> context);

} // HeaderSignal
