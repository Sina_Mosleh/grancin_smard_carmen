/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.DLTFilter#getEcuID_ECU <em>Ecu ID ECU</em>}</li>
 *   <li>{@link conditions.DLTFilter#getSessionID_SEID <em>Session ID SEID</em>}</li>
 *   <li>{@link conditions.DLTFilter#getApplicationID_APID <em>Application ID APID</em>}</li>
 *   <li>{@link conditions.DLTFilter#getContextID_CTID <em>Context ID CTID</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageType_MSTP <em>Message Type MSTP</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageLogInfo_MSLI <em>Message Log Info MSLI</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageTraceInfo_MSTI <em>Message Trace Info MSTI</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageBusInfo_MSBI <em>Message Bus Info MSBI</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageControlInfo_MSCI <em>Message Control Info MSCI</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageLogInfo_MSLITmplParam <em>Message Log Info MSLI Tmpl Param</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam <em>Message Trace Info MSTI Tmpl Param</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageBusInfo_MSBITmplParam <em>Message Bus Info MSBI Tmpl Param</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageControlInfo_MSCITmplParam <em>Message Control Info MSCI Tmpl Param</em>}</li>
 *   <li>{@link conditions.DLTFilter#getMessageType_MSTPTmplParam <em>Message Type MSTP Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getDLTFilter()
 * @model
 * @generated
 */
public interface DLTFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Ecu ID ECU</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ecu ID ECU</em>' attribute.
	 * @see #setEcuID_ECU(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_EcuID_ECU()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
	 * @generated
	 */
	String getEcuID_ECU();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getEcuID_ECU <em>Ecu ID ECU</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ecu ID ECU</em>' attribute.
	 * @see #getEcuID_ECU()
	 * @generated
	 */
	void setEcuID_ECU(String value);

	/**
	 * Returns the value of the '<em><b>Session ID SEID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session ID SEID</em>' attribute.
	 * @see #setSessionID_SEID(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_SessionID_SEID()
	 * @model dataType="conditions.LongOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
	 * @generated
	 */
	String getSessionID_SEID();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getSessionID_SEID <em>Session ID SEID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session ID SEID</em>' attribute.
	 * @see #getSessionID_SEID()
	 * @generated
	 */
	void setSessionID_SEID(String value);

	/**
	 * Returns the value of the '<em><b>Application ID APID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application ID APID</em>' attribute.
	 * @see #setApplicationID_APID(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_ApplicationID_APID()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
	 * @generated
	 */
	String getApplicationID_APID();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getApplicationID_APID <em>Application ID APID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application ID APID</em>' attribute.
	 * @see #getApplicationID_APID()
	 * @generated
	 */
	void setApplicationID_APID(String value);

	/**
	 * Returns the value of the '<em><b>Context ID CTID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context ID CTID</em>' attribute.
	 * @see #setContextID_CTID(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_ContextID_CTID()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
	 * @generated
	 */
	String getContextID_CTID();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getContextID_CTID <em>Context ID CTID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context ID CTID</em>' attribute.
	 * @see #getContextID_CTID()
	 * @generated
	 */
	void setContextID_CTID(String value);

	/**
	 * Returns the value of the '<em><b>Message Type MSTP</b></em>' attribute.
	 * The default value is <code>"NOT_SPECIFIED"</code>.
	 * The literals are from the enumeration {@link conditions.DLT_MessageType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Type MSTP</em>' attribute.
	 * @see conditions.DLT_MessageType
	 * @see #setMessageType_MSTP(DLT_MessageType)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageType_MSTP()
	 * @model default="NOT_SPECIFIED" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	DLT_MessageType getMessageType_MSTP();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageType_MSTP <em>Message Type MSTP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Type MSTP</em>' attribute.
	 * @see conditions.DLT_MessageType
	 * @see #getMessageType_MSTP()
	 * @generated
	 */
	void setMessageType_MSTP(DLT_MessageType value);

	/**
	 * Returns the value of the '<em><b>Message Log Info MSLI</b></em>' attribute.
	 * The default value is <code>"NOT_SPECIFIED"</code>.
	 * The literals are from the enumeration {@link conditions.DLT_MessageLogInfo}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Log Info MSLI</em>' attribute.
	 * @see conditions.DLT_MessageLogInfo
	 * @see #setMessageLogInfo_MSLI(DLT_MessageLogInfo)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageLogInfo_MSLI()
	 * @model default="NOT_SPECIFIED" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	DLT_MessageLogInfo getMessageLogInfo_MSLI();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageLogInfo_MSLI <em>Message Log Info MSLI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Log Info MSLI</em>' attribute.
	 * @see conditions.DLT_MessageLogInfo
	 * @see #getMessageLogInfo_MSLI()
	 * @generated
	 */
	void setMessageLogInfo_MSLI(DLT_MessageLogInfo value);

	/**
	 * Returns the value of the '<em><b>Message Trace Info MSTI</b></em>' attribute.
	 * The default value is <code>"NOT_SPECIFIED"</code>.
	 * The literals are from the enumeration {@link conditions.DLT_MessageTraceInfo}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Trace Info MSTI</em>' attribute.
	 * @see conditions.DLT_MessageTraceInfo
	 * @see #setMessageTraceInfo_MSTI(DLT_MessageTraceInfo)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageTraceInfo_MSTI()
	 * @model default="NOT_SPECIFIED" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	DLT_MessageTraceInfo getMessageTraceInfo_MSTI();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageTraceInfo_MSTI <em>Message Trace Info MSTI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Trace Info MSTI</em>' attribute.
	 * @see conditions.DLT_MessageTraceInfo
	 * @see #getMessageTraceInfo_MSTI()
	 * @generated
	 */
	void setMessageTraceInfo_MSTI(DLT_MessageTraceInfo value);

	/**
	 * Returns the value of the '<em><b>Message Bus Info MSBI</b></em>' attribute.
	 * The default value is <code>"NOT_SPECIFIED"</code>.
	 * The literals are from the enumeration {@link conditions.DLT_MessageBusInfo}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Bus Info MSBI</em>' attribute.
	 * @see conditions.DLT_MessageBusInfo
	 * @see #setMessageBusInfo_MSBI(DLT_MessageBusInfo)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageBusInfo_MSBI()
	 * @model default="NOT_SPECIFIED" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	DLT_MessageBusInfo getMessageBusInfo_MSBI();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageBusInfo_MSBI <em>Message Bus Info MSBI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Bus Info MSBI</em>' attribute.
	 * @see conditions.DLT_MessageBusInfo
	 * @see #getMessageBusInfo_MSBI()
	 * @generated
	 */
	void setMessageBusInfo_MSBI(DLT_MessageBusInfo value);

	/**
	 * Returns the value of the '<em><b>Message Control Info MSCI</b></em>' attribute.
	 * The default value is <code>"NOT_SPECIFIED"</code>.
	 * The literals are from the enumeration {@link conditions.DLT_MessageControlInfo}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Control Info MSCI</em>' attribute.
	 * @see conditions.DLT_MessageControlInfo
	 * @see #setMessageControlInfo_MSCI(DLT_MessageControlInfo)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageControlInfo_MSCI()
	 * @model default="NOT_SPECIFIED" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	DLT_MessageControlInfo getMessageControlInfo_MSCI();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageControlInfo_MSCI <em>Message Control Info MSCI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Control Info MSCI</em>' attribute.
	 * @see conditions.DLT_MessageControlInfo
	 * @see #getMessageControlInfo_MSCI()
	 * @generated
	 */
	void setMessageControlInfo_MSCI(DLT_MessageControlInfo value);

	/**
	 * Returns the value of the '<em><b>Message Log Info MSLI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Log Info MSLI Tmpl Param</em>' attribute.
	 * @see #setMessageLogInfo_MSLITmplParam(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageLogInfo_MSLITmplParam()
	 * @model
	 * @generated
	 */
	String getMessageLogInfo_MSLITmplParam();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageLogInfo_MSLITmplParam <em>Message Log Info MSLI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Log Info MSLI Tmpl Param</em>' attribute.
	 * @see #getMessageLogInfo_MSLITmplParam()
	 * @generated
	 */
	void setMessageLogInfo_MSLITmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Message Trace Info MSTI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Trace Info MSTI Tmpl Param</em>' attribute.
	 * @see #setMessageTraceInfo_MSTITmplParam(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageTraceInfo_MSTITmplParam()
	 * @model
	 * @generated
	 */
	String getMessageTraceInfo_MSTITmplParam();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam <em>Message Trace Info MSTI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Trace Info MSTI Tmpl Param</em>' attribute.
	 * @see #getMessageTraceInfo_MSTITmplParam()
	 * @generated
	 */
	void setMessageTraceInfo_MSTITmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Message Bus Info MSBI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Bus Info MSBI Tmpl Param</em>' attribute.
	 * @see #setMessageBusInfo_MSBITmplParam(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageBusInfo_MSBITmplParam()
	 * @model
	 * @generated
	 */
	String getMessageBusInfo_MSBITmplParam();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageBusInfo_MSBITmplParam <em>Message Bus Info MSBI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Bus Info MSBI Tmpl Param</em>' attribute.
	 * @see #getMessageBusInfo_MSBITmplParam()
	 * @generated
	 */
	void setMessageBusInfo_MSBITmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Message Control Info MSCI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Control Info MSCI Tmpl Param</em>' attribute.
	 * @see #setMessageControlInfo_MSCITmplParam(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageControlInfo_MSCITmplParam()
	 * @model
	 * @generated
	 */
	String getMessageControlInfo_MSCITmplParam();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageControlInfo_MSCITmplParam <em>Message Control Info MSCI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Control Info MSCI Tmpl Param</em>' attribute.
	 * @see #getMessageControlInfo_MSCITmplParam()
	 * @generated
	 */
	void setMessageControlInfo_MSCITmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Message Type MSTP Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Type MSTP Tmpl Param</em>' attribute.
	 * @see #setMessageType_MSTPTmplParam(String)
	 * @see conditions.ConditionsPackage#getDLTFilter_MessageType_MSTPTmplParam()
	 * @model
	 * @generated
	 */
	String getMessageType_MSTPTmplParam();

	/**
	 * Sets the value of the '{@link conditions.DLTFilter#getMessageType_MSTPTmplParam <em>Message Type MSTP Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Type MSTP Tmpl Param</em>' attribute.
	 * @see #getMessageType_MSTPTmplParam()
	 * @generated
	 */
	void setMessageType_MSTPTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageBusInfo(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageControlInfo(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageLogInfo(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageTraceInfo(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // DLTFilter
