/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IString Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getIStringOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IStringOperand extends IOperand, IComputeVariableActionOperand {
} // IStringOperand
