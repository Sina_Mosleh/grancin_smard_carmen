/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ConstantComparatorValue#getValue <em>Value</em>}</li>
 *   <li>{@link conditions.ConstantComparatorValue#getInterpretedValue <em>Interpreted Value</em>}</li>
 *   <li>{@link conditions.ConstantComparatorValue#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getConstantComparatorValue()
 * @model
 * @generated
 */
public interface ConstantComparatorValue extends ComparatorSignal {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see conditions.ConditionsPackage#getConstantComparatorValue_Value()
	 * @model default="0" dataType="conditions.DoubleOrHexOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link conditions.ConstantComparatorValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Interpreted Value</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * The literals are from the enumeration {@link conditions.BooleanOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interpreted Value</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #setInterpretedValue(BooleanOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getConstantComparatorValue_InterpretedValue()
	 * @model default="true" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	BooleanOrTemplatePlaceholderEnum getInterpretedValue();

	/**
	 * Sets the value of the '{@link conditions.ConstantComparatorValue#getInterpretedValue <em>Interpreted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpreted Value</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #getInterpretedValue()
	 * @generated
	 */
	void setInterpretedValue(BooleanOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interpreted Value Tmpl Param</em>' attribute.
	 * @see #setInterpretedValueTmplParam(String)
	 * @see conditions.ConditionsPackage#getConstantComparatorValue_InterpretedValueTmplParam()
	 * @model
	 * @generated
	 */
	String getInterpretedValueTmplParam();

	/**
	 * Sets the value of the '{@link conditions.ConstantComparatorValue#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpreted Value Tmpl Param</em>' attribute.
	 * @see #getInterpretedValueTmplParam()
	 * @generated
	 */
	void setInterpretedValueTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInterpretedValue(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ConstantComparatorValue
