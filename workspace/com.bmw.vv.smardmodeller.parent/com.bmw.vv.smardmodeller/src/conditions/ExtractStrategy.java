/**
 */
package conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ExtractStrategy#getAbstractSignal <em>Abstract Signal</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getExtractStrategy()
 * @model abstract="true"
 * @generated
 */
public interface ExtractStrategy extends EObject {
	/**
	 * Returns the value of the '<em><b>Abstract Signal</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Signal</em>' container reference.
	 * @see #setAbstractSignal(AbstractSignal)
	 * @see conditions.ConditionsPackage#getExtractStrategy_AbstractSignal()
	 * @see conditions.AbstractSignal#getExtractStrategy
	 * @model opposite="extractStrategy" required="true" transient="false"
	 * @generated
	 */
	AbstractSignal getAbstractSignal();

	/**
	 * Sets the value of the '{@link conditions.ExtractStrategy#getAbstractSignal <em>Abstract Signal</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Signal</em>' container reference.
	 * @see #getAbstractSignal()
	 * @generated
	 */
	void setAbstractSignal(AbstractSignal value);

} // ExtractStrategy
