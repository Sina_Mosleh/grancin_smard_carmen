/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Result Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getPluginResultExtractStrategy()
 * @model
 * @generated
 */
public interface PluginResultExtractStrategy extends ExtractStrategy {
	/**
	 * Returns the value of the '<em><b>Result Range</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result Range</em>' attribute.
	 * @see #isSetResultRange()
	 * @see #unsetResultRange()
	 * @see #setResultRange(String)
	 * @see conditions.ConditionsPackage#getPluginResultExtractStrategy_ResultRange()
	 * @model default="" unsettable="true" required="true"
	 *        extendedMetaData="kind='attribute' name='result' namespace='##targetNamespace'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getResultRange();

	/**
	 * Sets the value of the '{@link conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result Range</em>' attribute.
	 * @see #isSetResultRange()
	 * @see #unsetResultRange()
	 * @see #getResultRange()
	 * @generated
	 */
	void setResultRange(String value);

	/**
	 * Unsets the value of the '{@link conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResultRange()
	 * @see #getResultRange()
	 * @see #setResultRange(String)
	 * @generated
	 */
	void unsetResultRange();

	/**
	 * Returns whether the value of the '{@link conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Result Range</em>' attribute is set.
	 * @see #unsetResultRange()
	 * @see #getResultRange()
	 * @see #setResultRange(String)
	 * @generated
	 */
	boolean isSetResultRange();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidResultRange(DiagnosticChain diagnostics, Map<Object, Object> context);

} // PluginResultExtractStrategy
