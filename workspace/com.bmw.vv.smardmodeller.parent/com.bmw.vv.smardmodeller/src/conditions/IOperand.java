/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IOperand</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getIOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IOperand extends IVariableReaderWriter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	DataType get_EvaluationDataType();

} // IOperand
