/**
 */
package conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SourceReference#getSerializedReference <em>Serialized Reference</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSourceReference()
 * @model
 * @generated
 */
public interface SourceReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Serialized Reference</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Serialized Reference</em>' attribute.
	 * @see #setSerializedReference(String)
	 * @see conditions.ConditionsPackage#getSourceReference_SerializedReference()
	 * @model default="" required="true"
	 * @generated
	 */
	String getSerializedReference();

	/**
	 * Sets the value of the '{@link conditions.SourceReference#getSerializedReference <em>Serialized Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Serialized Reference</em>' attribute.
	 * @see #getSerializedReference()
	 * @generated
	 */
	void setSerializedReference(String value);

} // SourceReference
