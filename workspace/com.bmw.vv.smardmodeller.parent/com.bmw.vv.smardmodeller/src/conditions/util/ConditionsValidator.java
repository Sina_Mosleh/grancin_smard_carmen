/**
 */
package conditions.util;

import conditions.*;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage
 * @generated
 */
public class ConditionsValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ConditionsValidator INSTANCE = new ConditionsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "conditions";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Name' of 'Condition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONDITION__IS_VALID_HAS_VALID_NAME = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'Condition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONDITION__IS_VALID_HAS_VALID_DESCRIPTION = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Comparator' of 'Signal Comparison Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_COMPARATOR = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Operands' of 'Signal Comparison Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Bus Id' of 'Can Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Id Range' of 'Can Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID_RANGE = 6;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Check Type' of 'Can Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE = 7;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Rx Tx Flag' of 'Can Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_RX_TX_FLAG = 8;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Ext Identifier' of 'Can Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_EXT_IDENTIFIER = 9;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Bus Id' of 'Lin Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID = 10;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Id Range' of 'Lin Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID_RANGE = 11;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Check Type' of 'Lin Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE = 12;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid States Active' of 'State Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATES_ACTIVE = 13;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid State' of 'State Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATE = 14;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Min Max Times' of 'Timing Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TIMING_EXPRESSION__IS_VALID_HAS_VALID_MIN_MAX_TIMES = 15;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Operator' of 'Logical Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_OPERATOR = 16;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Sub Expressions' of 'Logical Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_SUB_EXPRESSIONS = 17;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Referenced Condition' of 'Reference Condition Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int REFERENCE_CONDITION_EXPRESSION__IS_VALID_HAS_VALID_REFERENCED_CONDITION = 18;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Stay Active' of 'Transition Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STAY_ACTIVE = 19;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Transition' of 'Transition Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_TRANSITION = 20;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Bus Id' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID = 21;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Check Type' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE = 22;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Startup Frame' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STARTUP_FRAME = 23;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Sync Frame' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_SYNC_FRAME = 24;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Zero Frame' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_ZERO_FRAME = 25;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Payload Preamble' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_PAYLOAD_PREAMBLE = 26;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Network Mgmt' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_NETWORK_MGMT = 27;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Id' of 'Flex Ray Message Check Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID = 28;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Interpreted Value' of 'Constant Comparator Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_INTERPRETED_VALUE = 29;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Value' of 'Constant Comparator Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_VALUE = 30;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Expression' of 'Calculation Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CALCULATION_EXPRESSION__IS_VALID_HAS_VALID_EXPRESSION = 31;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Operands' of 'Calculation Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CALCULATION_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS = 32;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'Expression'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int EXPRESSION__IS_VALID_HAS_VALID_DESCRIPTION = 33;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Value Ranges' of 'Variable Set'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE_SET__IS_VALID_HAS_VALID_VALUE_RANGES = 34;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Interpreted Value' of 'Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE__IS_VALID_HAS_VALID_INTERPRETED_VALUE = 35;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Type' of 'Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE__IS_VALID_HAS_VALID_TYPE = 36;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Unit' of 'Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE__IS_VALID_HAS_VALID_UNIT = 37;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Variable Format Tmpl Param' of 'Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE__IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM = 38;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Digits' of 'Variable Format'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VARIABLE_FORMAT__IS_VALID_HAS_VALID_DIGITS = 39;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Name' of 'Abstract Observer'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_NAME = 40;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Log Level' of 'Abstract Observer'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_LOG_LEVEL = 41;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Value Ranges Tmpl Param' of 'Abstract Observer'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM = 42;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Value' of 'Observer Value Range'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int OBSERVER_VALUE_RANGE__IS_VALID_HAS_VALID_VALUE = 43;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'Observer Value Range'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int OBSERVER_VALUE_RANGE__IS_VALID_HAS_VALID_DESCRIPTION = 44;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Signal' of 'Signal Reference'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SIGNAL_REFERENCE__IS_VALID_HAS_VALID_SIGNAL = 45;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Bit Pattern' of 'Bit Pattern Comparator Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int BIT_PATTERN_COMPARATOR_VALUE__IS_VALID_HAS_VALID_BIT_PATTERN = 46;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Startbit' of 'Bit Pattern Comparator Value'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int BIT_PATTERN_COMPARATOR_VALUE__IS_VALID_HAS_VALID_STARTBIT = 47;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid String To Check' of 'Matches'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int MATCHES__IS_VALID_HAS_VALID_STRING_TO_CHECK = 48;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Source MAC' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_SOURCE_MAC = 49;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Destination MAC' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_DESTINATION_MAC = 50;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid CRC' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_CRC = 51;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Ether Type' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_ETHER_TYPE = 52;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Inner Vlan Vid' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_VID = 53;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Outer Vlan Vid' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_VID = 54;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Inner Vlan CFI' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_CFI = 55;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Outer Vlan CFI' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_CFI = 56;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Inner Vlan PCP' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_PCP = 57;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Outer Vlan PCP' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_PCP = 58;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Inner Vlan TPID' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_TPID = 59;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Outer Vlan TPID' of 'Ethernet Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_TPID = 60;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Source Port' of 'UDP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDP_FILTER__IS_VALID_HAS_VALID_SOURCE_PORT = 61;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Destination Port' of 'UDP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDP_FILTER__IS_VALID_HAS_VALID_DESTINATION_PORT = 62;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Checksum' of 'UDP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDP_FILTER__IS_VALID_HAS_VALID_CHECKSUM = 63;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Source Port' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_SOURCE_PORT = 64;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Destination Port' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_DESTINATION_PORT = 65;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Sequence Number' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_SEQUENCE_NUMBER = 66;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Acknowledgement Number' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_ACKNOWLEDGEMENT_NUMBER = 67;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Tcp Flag' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_TCP_FLAG = 68;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Stream Analysis Flag' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_STREAM_ANALYSIS_FLAG = 69;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Stream Valid Payload Offset' of 'TCP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TCP_FILTER__IS_VALID_HAS_VALID_STREAM_VALID_PAYLOAD_OFFSET = 70;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Source Ip Address' of 'IPv4 Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int IPV4_FILTER__IS_VALID_HAS_VALID_SOURCE_IP_ADDRESS = 71;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Destination Ip Address' of 'IPv4 Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int IPV4_FILTER__IS_VALID_HAS_VALID_DESTINATION_IP_ADDRESS = 72;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Time To Live' of 'IPv4 Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int IPV4_FILTER__IS_VALID_HAS_VALID_TIME_TO_LIVE = 73;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Service Id' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_SERVICE_ID = 74;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Method Id' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_METHOD_ID = 75;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Session Id' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_SESSION_ID = 76;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Client Id' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_CLIENT_ID = 77;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Length' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_LENGTH = 78;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Protocol Version' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_PROTOCOL_VERSION = 79;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Interface Version' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_INTERFACE_VERSION = 80;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Type' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_MESSAGE_TYPE = 81;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Return Code' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_RETURN_CODE = 82;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Method Type' of 'Some IP Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IP_FILTER__IS_VALID_HAS_VALID_METHOD_TYPE = 83;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Instance Id' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INSTANCE_ID = 84;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Event Group Id' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_EVENT_GROUP_ID = 85;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Flags' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_FLAGS = 86;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Sd Type' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_SD_TYPE = 87;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Major Version' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_MAJOR_VERSION = 88;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Minor Version' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_MINOR_VERSION = 89;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Index First Option' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INDEX_FIRST_OPTION = 90;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Index Second Option' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INDEX_SECOND_OPTION = 91;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Number First Option' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_NUMBER_FIRST_OPTION = 92;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Number Second Option' of 'Some IPSD Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOME_IPSD_FILTER__IS_VALID_HAS_VALID_NUMBER_SECOND_OPTION = 93;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Factor' of 'Double Decode Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DOUBLE_DECODE_STRATEGY__IS_VALID_HAS_VALID_FACTOR = 94;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Offset' of 'Double Decode Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DOUBLE_DECODE_STRATEGY__IS_VALID_HAS_VALID_OFFSET = 95;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Extract String' of 'Universal Payload Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_EXTRACT_STRING = 96;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Signal Data Type' of 'Universal Payload Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE = 97;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Frame Id Or Frame Id Range' of 'CAN Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE = 98;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Rx Tx Flag' of 'CAN Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_FILTER__IS_VALID_HAS_VALID_RX_TX_FLAG = 99;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Ext Identifier' of 'CAN Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CAN_FILTER__IS_VALID_HAS_VALID_EXT_IDENTIFIER = 100;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Frame Id Or Frame Id Range' of 'LIN Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LIN_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE = 101;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Frame Id Or Frame Id Range' of 'Flex Ray Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE = 102;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Channel Type' of 'Flex Ray Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CHANNEL_TYPE = 103;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Cycle Offset' of 'Flex Ray Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CYCLE_OFFSET = 104;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Cycle Repeat Interval' of 'Flex Ray Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CYCLE_REPEAT_INTERVAL = 105;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Bus Info' of 'DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_BUS_INFO = 106;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Control Info' of 'DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_CONTROL_INFO = 107;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Log Info' of 'DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_LOG_INFO = 108;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Trace Info' of 'DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_TRACE_INFO = 109;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Type' of 'DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_TYPE = 110;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Source Node Identifier' of 'UDPNM Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDPNM_FILTER__IS_VALID_HAS_VALID_SOURCE_NODE_IDENTIFIER = 111;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Control Bit Vector' of 'UDPNM Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDPNM_FILTER__IS_VALID_HAS_VALID_CONTROL_BIT_VECTOR = 112;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Pwf Status' of 'UDPNM Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDPNM_FILTER__IS_VALID_HAS_VALID_PWF_STATUS = 113;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Teilnetz Status' of 'UDPNM Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDPNM_FILTER__IS_VALID_HAS_VALID_TEILNETZ_STATUS = 114;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid UDP Filter' of 'UDPNM Message'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UDPNM_MESSAGE__IS_VALID_HAS_VALID_UDP_FILTER = 115;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Startbit' of 'Universal Payload With Legacy Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STARTBIT = 116;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Data Length' of 'Universal Payload With Legacy Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_DATA_LENGTH = 117;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Bus Id' of 'Abstract Bus Message'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_BUS_MESSAGE__IS_VALID_HAS_VALID_BUS_ID = 118;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid is Osi Layer Conform' of 'Abstract Bus Message'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_BUS_MESSAGE__IS_VALID_IS_OSI_LAYER_CONFORM = 119;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid States Active' of 'Plugin State Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATES_ACTIVE = 120;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid State' of 'Plugin State Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATE = 121;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Result Range' of 'Plugin Result Extract Strategy'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PLUGIN_RESULT_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_RESULT_RANGE = 122;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Attribute' of 'Header Signal'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int HEADER_SIGNAL__IS_VALID_HAS_VALID_ATTRIBUTE = 123;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Message Id Or Message Id Range' of 'Non Verbose DLT Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int NON_VERBOSE_DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_ID_OR_MESSAGE_ID_RANGE = 124;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Regex' of 'Regex Operation'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int REGEX_OPERATION__IS_VALID_HAS_VALID_REGEX = 125;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Index' of 'Payload Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PAYLOAD_FILTER__IS_VALID_HAS_VALID_INDEX = 126;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Mask' of 'Payload Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PAYLOAD_FILTER__IS_VALID_HAS_VALID_MASK = 127;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Value' of 'Payload Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PAYLOAD_FILTER__IS_VALID_HAS_VALID_VALUE = 128;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Regex' of 'Verbose DLT Payload Filter'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int VERBOSE_DLT_PAYLOAD_FILTER__IS_VALID_HAS_VALID_REGEX = 129;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Name' of 'Abstract Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_VARIABLE__IS_VALID_HAS_VALID_NAME = 130;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Display Name' of 'Abstract Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_VARIABLE__IS_VALID_HAS_VALID_DISPLAY_NAME = 131;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Expression' of 'Computed Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPUTED_VARIABLE__IS_VALID_HAS_VALID_EXPRESSION = 132;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Operands' of 'Computed Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPUTED_VARIABLE__IS_VALID_HAS_VALID_OPERANDS = 133;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 133;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ConditionsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ConditionsPackage.DOCUMENT_ROOT:
				return validateDocumentRoot((DocumentRoot)value, diagnostics, context);
			case ConditionsPackage.CONDITION_SET:
				return validateConditionSet((ConditionSet)value, diagnostics, context);
			case ConditionsPackage.CONDITION:
				return validateCondition((Condition)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION:
				return validateSignalComparisonExpression((SignalComparisonExpression)value, diagnostics, context);
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION:
				return validateCanMessageCheckExpression((CanMessageCheckExpression)value, diagnostics, context);
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION:
				return validateLinMessageCheckExpression((LinMessageCheckExpression)value, diagnostics, context);
			case ConditionsPackage.STATE_CHECK_EXPRESSION:
				return validateStateCheckExpression((StateCheckExpression)value, diagnostics, context);
			case ConditionsPackage.TIMING_EXPRESSION:
				return validateTimingExpression((TimingExpression)value, diagnostics, context);
			case ConditionsPackage.TRUE_EXPRESSION:
				return validateTrueExpression((TrueExpression)value, diagnostics, context);
			case ConditionsPackage.LOGICAL_EXPRESSION:
				return validateLogicalExpression((LogicalExpression)value, diagnostics, context);
			case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION:
				return validateReferenceConditionExpression((ReferenceConditionExpression)value, diagnostics, context);
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION:
				return validateTransitionCheckExpression((TransitionCheckExpression)value, diagnostics, context);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION:
				return validateFlexRayMessageCheckExpression((FlexRayMessageCheckExpression)value, diagnostics, context);
			case ConditionsPackage.STOP_EXPRESSION:
				return validateStopExpression((StopExpression)value, diagnostics, context);
			case ConditionsPackage.COMPARATOR_SIGNAL:
				return validateComparatorSignal((ComparatorSignal)value, diagnostics, context);
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE:
				return validateConstantComparatorValue((ConstantComparatorValue)value, diagnostics, context);
			case ConditionsPackage.CALCULATION_EXPRESSION:
				return validateCalculationExpression((CalculationExpression)value, diagnostics, context);
			case ConditionsPackage.EXPRESSION:
				return validateExpression((Expression)value, diagnostics, context);
			case ConditionsPackage.VARIABLE_REFERENCE:
				return validateVariableReference((VariableReference)value, diagnostics, context);
			case ConditionsPackage.CONDITIONS_DOCUMENT:
				return validateConditionsDocument((ConditionsDocument)value, diagnostics, context);
			case ConditionsPackage.VARIABLE_SET:
				return validateVariableSet((VariableSet)value, diagnostics, context);
			case ConditionsPackage.VARIABLE:
				return validateVariable((Variable)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_VARIABLE:
				return validateSignalVariable((SignalVariable)value, diagnostics, context);
			case ConditionsPackage.VALUE_VARIABLE:
				return validateValueVariable((ValueVariable)value, diagnostics, context);
			case ConditionsPackage.VARIABLE_FORMAT:
				return validateVariableFormat((VariableFormat)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_OBSERVER:
				return validateAbstractObserver((AbstractObserver)value, diagnostics, context);
			case ConditionsPackage.OBSERVER_VALUE_RANGE:
				return validateObserverValueRange((ObserverValueRange)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_OBSERVER:
				return validateSignalObserver((SignalObserver)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_REFERENCE_SET:
				return validateSignalReferenceSet((SignalReferenceSet)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_REFERENCE:
				return validateSignalReference((SignalReference)value, diagnostics, context);
			case ConditionsPackage.ISIGNAL_OR_REFERENCE:
				return validateISignalOrReference((ISignalOrReference)value, diagnostics, context);
			case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE:
				return validateBitPatternComparatorValue((BitPatternComparatorValue)value, diagnostics, context);
			case ConditionsPackage.NOT_EXPRESSION:
				return validateNotExpression((NotExpression)value, diagnostics, context);
			case ConditionsPackage.IOPERAND:
				return validateIOperand((IOperand)value, diagnostics, context);
			case ConditionsPackage.ICOMPUTE_VARIABLE_ACTION_OPERAND:
				return validateIComputeVariableActionOperand((IComputeVariableActionOperand)value, diagnostics, context);
			case ConditionsPackage.ISTRING_OPERAND:
				return validateIStringOperand((IStringOperand)value, diagnostics, context);
			case ConditionsPackage.ISIGNAL_COMPARISON_EXPRESSION_OPERAND:
				return validateISignalComparisonExpressionOperand((ISignalComparisonExpressionOperand)value, diagnostics, context);
			case ConditionsPackage.IOPERATION:
				return validateIOperation((IOperation)value, diagnostics, context);
			case ConditionsPackage.ISTRING_OPERATION:
				return validateIStringOperation((IStringOperation)value, diagnostics, context);
			case ConditionsPackage.INUMERIC_OPERAND:
				return validateINumericOperand((INumericOperand)value, diagnostics, context);
			case ConditionsPackage.INUMERIC_OPERATION:
				return validateINumericOperation((INumericOperation)value, diagnostics, context);
			case ConditionsPackage.VALUE_VARIABLE_OBSERVER:
				return validateValueVariableObserver((ValueVariableObserver)value, diagnostics, context);
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE:
				return validateParseStringToDouble((ParseStringToDouble)value, diagnostics, context);
			case ConditionsPackage.STRING_EXPRESSION:
				return validateStringExpression((StringExpression)value, diagnostics, context);
			case ConditionsPackage.MATCHES:
				return validateMatches((Matches)value, diagnostics, context);
			case ConditionsPackage.EXTRACT:
				return validateExtract((Extract)value, diagnostics, context);
			case ConditionsPackage.SUBSTRING:
				return validateSubstring((Substring)value, diagnostics, context);
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING:
				return validateParseNumericToString((ParseNumericToString)value, diagnostics, context);
			case ConditionsPackage.STRING_LENGTH:
				return validateStringLength((StringLength)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_MESSAGE:
				return validateAbstractMessage((AbstractMessage)value, diagnostics, context);
			case ConditionsPackage.SOME_IP_MESSAGE:
				return validateSomeIPMessage((SomeIPMessage)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_SIGNAL:
				return validateAbstractSignal((AbstractSignal)value, diagnostics, context);
			case ConditionsPackage.CONTAINER_SIGNAL:
				return validateContainerSignal((ContainerSignal)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_FILTER:
				return validateAbstractFilter((AbstractFilter)value, diagnostics, context);
			case ConditionsPackage.ETHERNET_FILTER:
				return validateEthernetFilter((EthernetFilter)value, diagnostics, context);
			case ConditionsPackage.UDP_FILTER:
				return validateUDPFilter((UDPFilter)value, diagnostics, context);
			case ConditionsPackage.TCP_FILTER:
				return validateTCPFilter((TCPFilter)value, diagnostics, context);
			case ConditionsPackage.IPV4_FILTER:
				return validateIPv4Filter((IPv4Filter)value, diagnostics, context);
			case ConditionsPackage.SOME_IP_FILTER:
				return validateSomeIPFilter((SomeIPFilter)value, diagnostics, context);
			case ConditionsPackage.FOR_EACH_EXPRESSION:
				return validateForEachExpression((ForEachExpression)value, diagnostics, context);
			case ConditionsPackage.MESSAGE_CHECK_EXPRESSION:
				return validateMessageCheckExpression((MessageCheckExpression)value, diagnostics, context);
			case ConditionsPackage.SOME_IPSD_FILTER:
				return validateSomeIPSDFilter((SomeIPSDFilter)value, diagnostics, context);
			case ConditionsPackage.SOME_IPSD_MESSAGE:
				return validateSomeIPSDMessage((SomeIPSDMessage)value, diagnostics, context);
			case ConditionsPackage.DOUBLE_SIGNAL:
				return validateDoubleSignal((DoubleSignal)value, diagnostics, context);
			case ConditionsPackage.STRING_SIGNAL:
				return validateStringSignal((StringSignal)value, diagnostics, context);
			case ConditionsPackage.DECODE_STRATEGY:
				return validateDecodeStrategy((DecodeStrategy)value, diagnostics, context);
			case ConditionsPackage.DOUBLE_DECODE_STRATEGY:
				return validateDoubleDecodeStrategy((DoubleDecodeStrategy)value, diagnostics, context);
			case ConditionsPackage.EXTRACT_STRATEGY:
				return validateExtractStrategy((ExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY:
				return validateUniversalPayloadExtractStrategy((UniversalPayloadExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.EMPTY_EXTRACT_STRATEGY:
				return validateEmptyExtractStrategy((EmptyExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.CAN_FILTER:
				return validateCANFilter((CANFilter)value, diagnostics, context);
			case ConditionsPackage.LIN_FILTER:
				return validateLINFilter((LINFilter)value, diagnostics, context);
			case ConditionsPackage.FLEX_RAY_FILTER:
				return validateFlexRayFilter((FlexRayFilter)value, diagnostics, context);
			case ConditionsPackage.DLT_FILTER:
				return validateDLTFilter((DLTFilter)value, diagnostics, context);
			case ConditionsPackage.UDPNM_FILTER:
				return validateUDPNMFilter((UDPNMFilter)value, diagnostics, context);
			case ConditionsPackage.CAN_MESSAGE:
				return validateCANMessage((CANMessage)value, diagnostics, context);
			case ConditionsPackage.LIN_MESSAGE:
				return validateLINMessage((LINMessage)value, diagnostics, context);
			case ConditionsPackage.FLEX_RAY_MESSAGE:
				return validateFlexRayMessage((FlexRayMessage)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE:
				return validateDLTMessage((DLTMessage)value, diagnostics, context);
			case ConditionsPackage.UDP_MESSAGE:
				return validateUDPMessage((UDPMessage)value, diagnostics, context);
			case ConditionsPackage.TCP_MESSAGE:
				return validateTCPMessage((TCPMessage)value, diagnostics, context);
			case ConditionsPackage.UDPNM_MESSAGE:
				return validateUDPNMMessage((UDPNMMessage)value, diagnostics, context);
			case ConditionsPackage.VERBOSE_DLT_MESSAGE:
				return validateVerboseDLTMessage((VerboseDLTMessage)value, diagnostics, context);
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY:
				return validateUniversalPayloadWithLegacyExtractStrategy((UniversalPayloadWithLegacyExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE:
				return validateAbstractBusMessage((AbstractBusMessage)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_FILTER:
				return validatePluginFilter((PluginFilter)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_MESSAGE:
				return validatePluginMessage((PluginMessage)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_SIGNAL:
				return validatePluginSignal((PluginSignal)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY:
				return validatePluginStateExtractStrategy((PluginStateExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY:
				return validatePluginResultExtractStrategy((PluginResultExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.EMPTY_DECODE_STRATEGY:
				return validateEmptyDecodeStrategy((EmptyDecodeStrategy)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_CHECK_EXPRESSION:
				return validatePluginCheckExpression((PluginCheckExpression)value, diagnostics, context);
			case ConditionsPackage.BASE_CLASS_WITH_ID:
				return validateBaseClassWithID((BaseClassWithID)value, diagnostics, context);
			case ConditionsPackage.HEADER_SIGNAL:
				return validateHeaderSignal((HeaderSignal)value, diagnostics, context);
			case ConditionsPackage.IVARIABLE_READER_WRITER:
				return validateIVariableReaderWriter((IVariableReaderWriter)value, diagnostics, context);
			case ConditionsPackage.ISTATE_TRANSITION_REFERENCE:
				return validateIStateTransitionReference((IStateTransitionReference)value, diagnostics, context);
			case ConditionsPackage.TP_FILTER:
				return validateTPFilter((TPFilter)value, diagnostics, context);
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE:
				return validateBaseClassWithSourceReference((BaseClassWithSourceReference)value, diagnostics, context);
			case ConditionsPackage.SOURCE_REFERENCE:
				return validateSourceReference((SourceReference)value, diagnostics, context);
			case ConditionsPackage.ETHERNET_MESSAGE:
				return validateEthernetMessage((EthernetMessage)value, diagnostics, context);
			case ConditionsPackage.IPV4_MESSAGE:
				return validateIPv4Message((IPv4Message)value, diagnostics, context);
			case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE:
				return validateNonVerboseDLTMessage((NonVerboseDLTMessage)value, diagnostics, context);
			case ConditionsPackage.STRING_DECODE_STRATEGY:
				return validateStringDecodeStrategy((StringDecodeStrategy)value, diagnostics, context);
			case ConditionsPackage.NON_VERBOSE_DLT_FILTER:
				return validateNonVerboseDLTFilter((NonVerboseDLTFilter)value, diagnostics, context);
			case ConditionsPackage.VERBOSE_DLT_EXTRACT_STRATEGY:
				return validateVerboseDLTExtractStrategy((VerboseDLTExtractStrategy)value, diagnostics, context);
			case ConditionsPackage.REGEX_OPERATION:
				return validateRegexOperation((RegexOperation)value, diagnostics, context);
			case ConditionsPackage.PAYLOAD_FILTER:
				return validatePayloadFilter((PayloadFilter)value, diagnostics, context);
			case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER:
				return validateVerboseDLTPayloadFilter((VerboseDLTPayloadFilter)value, diagnostics, context);
			case ConditionsPackage.ABSTRACT_VARIABLE:
				return validateAbstractVariable((AbstractVariable)value, diagnostics, context);
			case ConditionsPackage.VARIABLE_STRUCTURE:
				return validateVariableStructure((VariableStructure)value, diagnostics, context);
			case ConditionsPackage.COMPUTED_VARIABLE:
				return validateComputedVariable((ComputedVariable)value, diagnostics, context);
			case ConditionsPackage.CHECK_TYPE:
				return validateCheckType((CheckType)value, diagnostics, context);
			case ConditionsPackage.COMPARATOR:
				return validateComparator((Comparator)value, diagnostics, context);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE:
				return validateLogicalOperatorType((LogicalOperatorType)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_DATA_TYPE:
				return validateSignalDataType((SignalDataType)value, diagnostics, context);
			case ConditionsPackage.FLEX_CHANNEL_TYPE:
				return validateFlexChannelType((FlexChannelType)value, diagnostics, context);
			case ConditionsPackage.BYTE_ORDER_TYPE:
				return validateByteOrderType((ByteOrderType)value, diagnostics, context);
			case ConditionsPackage.FORMAT_DATA_TYPE:
				return validateFormatDataType((FormatDataType)value, diagnostics, context);
			case ConditionsPackage.FLEX_RAY_HEADER_FLAG_SELECTION:
				return validateFlexRayHeaderFlagSelection((FlexRayHeaderFlagSelection)value, diagnostics, context);
			case ConditionsPackage.BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateBooleanOrTemplatePlaceholderEnum((BooleanOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateProtocolTypeOrTemplatePlaceholderEnum((ProtocolTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.ETHERNET_PROTOCOL_TYPE_SELECTION:
				return validateEthernetProtocolTypeSelection((EthernetProtocolTypeSelection)value, diagnostics, context);
			case ConditionsPackage.RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateRxTxFlagTypeOrTemplatePlaceholderEnum((RxTxFlagTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.OBSERVER_VALUE_TYPE:
				return validateObserverValueType((ObserverValueType)value, diagnostics, context);
			case ConditionsPackage.PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validatePluginStateValueTypeOrTemplatePlaceholderEnum((PluginStateValueTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateAnalysisTypeOrTemplatePlaceholderEnum((AnalysisTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPMessageTypeOrTemplatePlaceholderEnum((SomeIPMessageTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPMethodTypeOrTemplatePlaceholderEnum((SomeIPMethodTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPReturnCodeOrTemplatePlaceholderEnum((SomeIPReturnCodeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPSDFlagsOrTemplatePlaceholderEnum((SomeIPSDFlagsOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPSDTypeOrTemplatePlaceholderEnum((SomeIPSDTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateStreamAnalysisFlagsOrTemplatePlaceholderEnum((StreamAnalysisFlagsOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateSomeIPSDEntryFlagsOrTemplatePlaceholderEnum((SomeIPSDEntryFlagsOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateCanExtIdentifierOrTemplatePlaceholderEnum((CanExtIdentifierOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.DATA_TYPE:
				return validateDataType((DataType)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE_TYPE:
				return validateDLT_MessageType((DLT_MessageType)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE_TRACE_INFO:
				return validateDLT_MessageTraceInfo((DLT_MessageTraceInfo)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE_LOG_INFO:
				return validateDLT_MessageLogInfo((DLT_MessageLogInfo)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE_CONTROL_INFO:
				return validateDLT_MessageControlInfo((DLT_MessageControlInfo)value, diagnostics, context);
			case ConditionsPackage.DLT_MESSAGE_BUS_INFO:
				return validateDLT_MessageBusInfo((DLT_MessageBusInfo)value, diagnostics, context);
			case ConditionsPackage.FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE:
				return validateForEachExpressionModifierTemplate((ForEachExpressionModifierTemplate)value, diagnostics, context);
			case ConditionsPackage.AUTOSAR_DATA_TYPE:
				return validateAUTOSARDataType((AUTOSARDataType)value, diagnostics, context);
			case ConditionsPackage.CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return validateCANFrameTypeOrTemplatePlaceholderEnum((CANFrameTypeOrTemplatePlaceholderEnum)value, diagnostics, context);
			case ConditionsPackage.EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM:
				return validateEvaluationBehaviourOrTemplateDefinedEnum((EvaluationBehaviourOrTemplateDefinedEnum)value, diagnostics, context);
			case ConditionsPackage.SIGNAL_VARIABLE_LAG_ENUM:
				return validateSignalVariableLagEnum((SignalVariableLagEnum)value, diagnostics, context);
			case ConditionsPackage.TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM:
				return validateTTLOrTemplatePlaceHolderEnum((TTLOrTemplatePlaceHolderEnum)value, diagnostics, context);
			case ConditionsPackage.CHECK_TYPE_OBJECT:
				return validateCheckTypeObject((CheckType)value, diagnostics, context);
			case ConditionsPackage.COMPARATOR_OBJECT:
				return validateComparatorObject((Comparator)value, diagnostics, context);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE_OBJECT:
				return validateLogicalOperatorTypeObject((LogicalOperatorType)value, diagnostics, context);
			case ConditionsPackage.HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER:
				return validateHexOrIntOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.INT_OR_TEMPLATE_PLACEHOLDER:
				return validateIntOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.LONG_OR_TEMPLATE_PLACEHOLDER:
				return validateLongOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return validateDoubleOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER:
				return validateDoubleOrHexOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.IP_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return validateIPPatternOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return validateMACPatternOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return validatePortPatternOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return validateVlanIdPatternOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.IP_OR_TEMPLATE_PLACEHOLDER:
				return validateIPOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.PORT_OR_TEMPLATE_PLACEHOLDER:
				return validatePortOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.VLAN_ID_OR_TEMPLATE_PLACEHOLDER:
				return validateVlanIdOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.MAC_OR_TEMPLATE_PLACEHOLDER:
				return validateMACOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.BITMASK_OR_TEMPLATE_PLACEHOLDER:
				return validateBitmaskOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER:
				return validateHexOrIntOrNullOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return validateNonZeroDoubleOrTemplatePlaceholder((String)value, diagnostics, context);
			case ConditionsPackage.VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING:
				return validateValVarInitialValueDoubleOrTemplatePlaceholderOrString((String)value, diagnostics, context);
			case ConditionsPackage.BYTE_OR_TEMPLATE_PLACEHOLDER:
				return validateByteOrTemplatePlaceholder((String)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConditionSet(ConditionSet conditionSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(conditionSet, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(condition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validateCondition_isValid_hasValidName(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validateCondition_isValid_hasValidDescription(condition, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidName constraint of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition_isValid_hasValidName(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return condition.isValid_hasValidName(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDescription constraint of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition_isValid_hasValidDescription(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return condition.isValid_hasValidDescription(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalComparisonExpression(SignalComparisonExpression signalComparisonExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(signalComparisonExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateSignalComparisonExpression_isValid_hasValidComparator(signalComparisonExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateSignalComparisonExpression_isValid_hasValidOperands(signalComparisonExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidComparator constraint of '<em>Signal Comparison Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalComparisonExpression_isValid_hasValidComparator(SignalComparisonExpression signalComparisonExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return signalComparisonExpression.isValid_hasValidComparator(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOperands constraint of '<em>Signal Comparison Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalComparisonExpression_isValid_hasValidOperands(SignalComparisonExpression signalComparisonExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return signalComparisonExpression.isValid_hasValidOperands(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(canMessageCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCanMessageCheckExpression_isValid_hasValidBusId(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCanMessageCheckExpression_isValid_hasValidMessageIdRange(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCanMessageCheckExpression_isValid_hasValidCheckType(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCanMessageCheckExpression_isValid_hasValidRxTxFlag(canMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCanMessageCheckExpression_isValid_hasValidExtIdentifier(canMessageCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidBusId constraint of '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression_isValid_hasValidBusId(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canMessageCheckExpression.isValid_hasValidBusId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageIdRange constraint of '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression_isValid_hasValidMessageIdRange(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canMessageCheckExpression.isValid_hasValidMessageIdRange(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCheckType constraint of '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression_isValid_hasValidCheckType(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canMessageCheckExpression.isValid_hasValidCheckType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidRxTxFlag constraint of '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression_isValid_hasValidRxTxFlag(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canMessageCheckExpression.isValid_hasValidRxTxFlag(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidExtIdentifier constraint of '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanMessageCheckExpression_isValid_hasValidExtIdentifier(CanMessageCheckExpression canMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canMessageCheckExpression.isValid_hasValidExtIdentifier(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinMessageCheckExpression(LinMessageCheckExpression linMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(linMessageCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinMessageCheckExpression_isValid_hasValidBusId(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinMessageCheckExpression_isValid_hasValidMessageIdRange(linMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinMessageCheckExpression_isValid_hasValidCheckType(linMessageCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidBusId constraint of '<em>Lin Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinMessageCheckExpression_isValid_hasValidBusId(LinMessageCheckExpression linMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linMessageCheckExpression.isValid_hasValidBusId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageIdRange constraint of '<em>Lin Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinMessageCheckExpression_isValid_hasValidMessageIdRange(LinMessageCheckExpression linMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linMessageCheckExpression.isValid_hasValidMessageIdRange(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCheckType constraint of '<em>Lin Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinMessageCheckExpression_isValid_hasValidCheckType(LinMessageCheckExpression linMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linMessageCheckExpression.isValid_hasValidCheckType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateCheckExpression(StateCheckExpression stateCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(stateCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateStateCheckExpression_isValid_hasValidStatesActive(stateCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateStateCheckExpression_isValid_hasValidState(stateCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidStatesActive constraint of '<em>State Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateCheckExpression_isValid_hasValidStatesActive(StateCheckExpression stateCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return stateCheckExpression.isValid_hasValidStatesActive(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidState constraint of '<em>State Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateCheckExpression_isValid_hasValidState(StateCheckExpression stateCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return stateCheckExpression.isValid_hasValidState(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimingExpression(TimingExpression timingExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(timingExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(timingExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateTimingExpression_isValid_hasValidMinMaxTimes(timingExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidMinMaxTimes constraint of '<em>Timing Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimingExpression_isValid_hasValidMinMaxTimes(TimingExpression timingExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return timingExpression.isValid_hasValidMinMaxTimes(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTrueExpression(TrueExpression trueExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(trueExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(trueExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(trueExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalExpression(LogicalExpression logicalExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(logicalExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateLogicalExpression_isValid_hasValidOperator(logicalExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateLogicalExpression_isValid_hasValidSubExpressions(logicalExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidOperator constraint of '<em>Logical Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalExpression_isValid_hasValidOperator(LogicalExpression logicalExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return logicalExpression.isValid_hasValidOperator(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSubExpressions constraint of '<em>Logical Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalExpression_isValid_hasValidSubExpressions(LogicalExpression logicalExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return logicalExpression.isValid_hasValidSubExpressions(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReferenceConditionExpression(ReferenceConditionExpression referenceConditionExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(referenceConditionExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(referenceConditionExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateReferenceConditionExpression_isValid_hasValidReferencedCondition(referenceConditionExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidReferencedCondition constraint of '<em>Reference Condition Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReferenceConditionExpression_isValid_hasValidReferencedCondition(ReferenceConditionExpression referenceConditionExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return referenceConditionExpression.isValid_hasValidReferencedCondition(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransitionCheckExpression(TransitionCheckExpression transitionCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(transitionCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransitionCheckExpression_isValid_hasValidStayActive(transitionCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransitionCheckExpression_isValid_hasValidTransition(transitionCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidStayActive constraint of '<em>Transition Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransitionCheckExpression_isValid_hasValidStayActive(TransitionCheckExpression transitionCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transitionCheckExpression.isValid_hasValidStayActive(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidTransition constraint of '<em>Transition Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransitionCheckExpression_isValid_hasValidTransition(TransitionCheckExpression transitionCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transitionCheckExpression.isValid_hasValidTransition(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(flexRayMessageCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidBusId(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidCheckType(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidStartupFrame(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidSyncFrame(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidZeroFrame(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidPayloadPreamble(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidNetworkMgmt(flexRayMessageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayMessageCheckExpression_isValid_hasValidMessageId(flexRayMessageCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidBusId constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidBusId(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidBusId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCheckType constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidCheckType(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidCheckType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidStartupFrame constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidStartupFrame(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidStartupFrame(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSyncFrame constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidSyncFrame(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidSyncFrame(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidZeroFrame constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidZeroFrame(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidZeroFrame(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidPayloadPreamble constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidPayloadPreamble(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidPayloadPreamble(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidNetworkMgmt constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidNetworkMgmt(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidNetworkMgmt(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageId constraint of '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessageCheckExpression_isValid_hasValidMessageId(FlexRayMessageCheckExpression flexRayMessageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayMessageCheckExpression.isValid_hasValidMessageId(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStopExpression(StopExpression stopExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(stopExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(stopExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(stopExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparatorSignal(ComparatorSignal comparatorSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(comparatorSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstantComparatorValue(ConstantComparatorValue constantComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(constantComparatorValue, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validateConstantComparatorValue_isValid_hasValidInterpretedValue(constantComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validateConstantComparatorValue_isValid_hasValidValue(constantComparatorValue, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidInterpretedValue constraint of '<em>Constant Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstantComparatorValue_isValid_hasValidInterpretedValue(ConstantComparatorValue constantComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return constantComparatorValue.isValid_hasValidInterpretedValue(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidValue constraint of '<em>Constant Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstantComparatorValue_isValid_hasValidValue(ConstantComparatorValue constantComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return constantComparatorValue.isValid_hasValidValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCalculationExpression(CalculationExpression calculationExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(calculationExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCalculationExpression_isValid_hasValidExpression(calculationExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateCalculationExpression_isValid_hasValidOperands(calculationExpression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidExpression constraint of '<em>Calculation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCalculationExpression_isValid_hasValidExpression(CalculationExpression calculationExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return calculationExpression.isValid_hasValidExpression(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOperands constraint of '<em>Calculation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCalculationExpression_isValid_hasValidOperands(CalculationExpression calculationExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return calculationExpression.isValid_hasValidOperands(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExpression(Expression expression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(expression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(expression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(expression, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidDescription constraint of '<em>Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExpression_isValid_hasValidDescription(Expression expression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return expression.isValid_hasValidDescription(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableReference(VariableReference variableReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(variableReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConditionsDocument(ConditionsDocument conditionsDocument, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(conditionsDocument, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableSet(VariableSet variableSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(variableSet, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(variableSet, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariableSet_isValid_hasValidValueRanges(variableSet, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidValueRanges constraint of '<em>Variable Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableSet_isValid_hasValidValueRanges(VariableSet variableSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variableSet.isValid_hasValidValueRanges(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariable(Variable variable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(variable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidInterpretedValue(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidType(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidUnit(variable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidVariableFormatTmplParam(variable, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidInterpretedValue constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariable_isValid_hasValidInterpretedValue(Variable variable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variable.isValid_hasValidInterpretedValue(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidType constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariable_isValid_hasValidType(Variable variable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variable.isValid_hasValidType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidUnit constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariable_isValid_hasValidUnit(Variable variable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variable.isValid_hasValidUnit(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidVariableFormatTmplParam constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariable_isValid_hasValidVariableFormatTmplParam(Variable variable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variable.isValid_hasValidVariableFormatTmplParam(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalVariable(SignalVariable signalVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(signalVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidInterpretedValue(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidType(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidUnit(signalVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidVariableFormatTmplParam(signalVariable, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueVariable(ValueVariable valueVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(valueVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidInterpretedValue(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidType(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidUnit(valueVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidVariableFormatTmplParam(valueVariable, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableFormat(VariableFormat variableFormat, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(variableFormat, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(variableFormat, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariableFormat_isValid_hasValidDigits(variableFormat, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidDigits constraint of '<em>Variable Format</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableFormat_isValid_hasValidDigits(VariableFormat variableFormat, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return variableFormat.isValid_hasValidDigits(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractObserver(AbstractObserver abstractObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(abstractObserver, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidName(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidLogLevel(abstractObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidValueRangesTmplParam(abstractObserver, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidName constraint of '<em>Abstract Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractObserver_isValid_hasValidName(AbstractObserver abstractObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractObserver.isValid_hasValidName(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidLogLevel constraint of '<em>Abstract Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractObserver_isValid_hasValidLogLevel(AbstractObserver abstractObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractObserver.isValid_hasValidLogLevel(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidValueRangesTmplParam constraint of '<em>Abstract Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractObserver_isValid_hasValidValueRangesTmplParam(AbstractObserver abstractObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractObserver.isValid_hasValidValueRangesTmplParam(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObserverValueRange(ObserverValueRange observerValueRange, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(observerValueRange, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validateObserverValueRange_isValid_hasValidValue(observerValueRange, diagnostics, context);
		if (result || diagnostics != null) result &= validateObserverValueRange_isValid_hasValidDescription(observerValueRange, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidValue constraint of '<em>Observer Value Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObserverValueRange_isValid_hasValidValue(ObserverValueRange observerValueRange, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return observerValueRange.isValid_hasValidValue(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDescription constraint of '<em>Observer Value Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObserverValueRange_isValid_hasValidDescription(ObserverValueRange observerValueRange, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return observerValueRange.isValid_hasValidDescription(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalObserver(SignalObserver signalObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(signalObserver, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidName(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidLogLevel(signalObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidValueRangesTmplParam(signalObserver, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalReferenceSet(SignalReferenceSet signalReferenceSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(signalReferenceSet, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalReference(SignalReference signalReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(signalReference, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(signalReference, diagnostics, context);
		if (result || diagnostics != null) result &= validateSignalReference_isValid_hasValidSignal(signalReference, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSignal constraint of '<em>Signal Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalReference_isValid_hasValidSignal(SignalReference signalReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return signalReference.isValid_hasValidSignal(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateISignalOrReference(ISignalOrReference iSignalOrReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iSignalOrReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBitPatternComparatorValue(BitPatternComparatorValue bitPatternComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(bitPatternComparatorValue, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validateBitPatternComparatorValue_isValid_hasValidBitPattern(bitPatternComparatorValue, diagnostics, context);
		if (result || diagnostics != null) result &= validateBitPatternComparatorValue_isValid_hasValidStartbit(bitPatternComparatorValue, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidBitPattern constraint of '<em>Bit Pattern Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBitPatternComparatorValue_isValid_hasValidBitPattern(BitPatternComparatorValue bitPatternComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return bitPatternComparatorValue.isValid_hasValidBitPattern(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidStartbit constraint of '<em>Bit Pattern Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBitPatternComparatorValue_isValid_hasValidStartbit(BitPatternComparatorValue bitPatternComparatorValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return bitPatternComparatorValue.isValid_hasValidStartbit(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNotExpression(NotExpression notExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(notExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(notExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(notExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIOperand(IOperand iOperand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iOperand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIComputeVariableActionOperand(IComputeVariableActionOperand iComputeVariableActionOperand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iComputeVariableActionOperand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIStringOperand(IStringOperand iStringOperand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iStringOperand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateISignalComparisonExpressionOperand(ISignalComparisonExpressionOperand iSignalComparisonExpressionOperand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iSignalComparisonExpressionOperand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIOperation(IOperation iOperation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iOperation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIStringOperation(IStringOperation iStringOperation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iStringOperation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateINumericOperand(INumericOperand iNumericOperand, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iNumericOperand, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateINumericOperation(INumericOperation iNumericOperation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iNumericOperation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueVariableObserver(ValueVariableObserver valueVariableObserver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(valueVariableObserver, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidName(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidLogLevel(valueVariableObserver, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractObserver_isValid_hasValidValueRangesTmplParam(valueVariableObserver, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParseStringToDouble(ParseStringToDouble parseStringToDouble, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parseStringToDouble, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringExpression(StringExpression stringExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(stringExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(stringExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(stringExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMatches(Matches matches, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(matches, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validateRegexOperation_isValid_hasValidRegex(matches, diagnostics, context);
		if (result || diagnostics != null) result &= validateMatches_isValid_hasValidStringToCheck(matches, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidStringToCheck constraint of '<em>Matches</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMatches_isValid_hasValidStringToCheck(Matches matches, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return matches.isValid_hasValidStringToCheck(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExtract(Extract extract, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(extract, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(extract, diagnostics, context);
		if (result || diagnostics != null) result &= validateRegexOperation_isValid_hasValidRegex(extract, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSubstring(Substring substring, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(substring, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParseNumericToString(ParseNumericToString parseNumericToString, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(parseNumericToString, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringLength(StringLength stringLength, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stringLength, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractMessage(AbstractMessage abstractMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPMessage(SomeIPMessage someIPMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(someIPMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(someIPMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(someIPMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractSignal(AbstractSignal abstractSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainerSignal(ContainerSignal containerSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(containerSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractFilter(AbstractFilter abstractFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(abstractFilter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(ethernetFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidSourceMAC(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidDestinationMAC(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidCRC(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidEtherType(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidInnerVlanVid(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidOuterVlanVid(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidInnerVlanCFI(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidOuterVlanCFI(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidInnerVlanPCP(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidOuterVlanPCP(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidInnerVlanTPID(ethernetFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateEthernetFilter_isValid_hasValidOuterVlanTPID(ethernetFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSourceMAC constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidSourceMAC(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidSourceMAC(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDestinationMAC constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidDestinationMAC(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidDestinationMAC(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCRC constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidCRC(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidCRC(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidEtherType constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidEtherType(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidEtherType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidInnerVlanVid constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidInnerVlanVid(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidInnerVlanVid(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOuterVlanVid constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidOuterVlanVid(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidOuterVlanVid(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidInnerVlanCFI constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidInnerVlanCFI(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidInnerVlanCFI(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOuterVlanCFI constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidOuterVlanCFI(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidOuterVlanCFI(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidInnerVlanPCP constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidInnerVlanPCP(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidInnerVlanPCP(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOuterVlanPCP constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidOuterVlanPCP(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidOuterVlanPCP(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidInnerVlanTPID constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidInnerVlanTPID(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidInnerVlanTPID(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOuterVlanTPID constraint of '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetFilter_isValid_hasValidOuterVlanTPID(EthernetFilter ethernetFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return ethernetFilter.isValid_hasValidOuterVlanTPID(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPFilter(UDPFilter udpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(udpFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPFilter_isValid_hasValidSourcePort(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPFilter_isValid_hasValidDestinationPort(udpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPFilter_isValid_hasValidChecksum(udpFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSourcePort constraint of '<em>UDP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPFilter_isValid_hasValidSourcePort(UDPFilter udpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpFilter.isValid_hasValidSourcePort(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDestinationPort constraint of '<em>UDP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPFilter_isValid_hasValidDestinationPort(UDPFilter udpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpFilter.isValid_hasValidDestinationPort(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidChecksum constraint of '<em>UDP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPFilter_isValid_hasValidChecksum(UDPFilter udpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpFilter.isValid_hasValidChecksum(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tcpFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidSourcePort(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidDestinationPort(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidSequenceNumber(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidAcknowledgementNumber(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidTcpFlag(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidStreamAnalysisFlag(tcpFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateTCPFilter_isValid_hasValidStreamValidPayloadOffset(tcpFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSourcePort constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidSourcePort(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidSourcePort(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDestinationPort constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidDestinationPort(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidDestinationPort(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSequenceNumber constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidSequenceNumber(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidSequenceNumber(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidAcknowledgementNumber constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidAcknowledgementNumber(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidAcknowledgementNumber(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidTcpFlag constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidTcpFlag(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidTcpFlag(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidStreamAnalysisFlag constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidStreamAnalysisFlag(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidStreamAnalysisFlag(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidStreamValidPayloadOffset constraint of '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPFilter_isValid_hasValidStreamValidPayloadOffset(TCPFilter tcpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return tcpFilter.isValid_hasValidStreamValidPayloadOffset(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPv4Filter(IPv4Filter iPv4Filter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(iPv4Filter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validateIPv4Filter_isValid_hasValidSourceIpAddress(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validateIPv4Filter_isValid_hasValidDestinationIpAddress(iPv4Filter, diagnostics, context);
		if (result || diagnostics != null) result &= validateIPv4Filter_isValid_hasValidTimeToLive(iPv4Filter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSourceIpAddress constraint of '<em>IPv4 Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPv4Filter_isValid_hasValidSourceIpAddress(IPv4Filter iPv4Filter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return iPv4Filter.isValid_hasValidSourceIpAddress(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDestinationIpAddress constraint of '<em>IPv4 Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPv4Filter_isValid_hasValidDestinationIpAddress(IPv4Filter iPv4Filter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return iPv4Filter.isValid_hasValidDestinationIpAddress(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidTimeToLive constraint of '<em>IPv4 Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPv4Filter_isValid_hasValidTimeToLive(IPv4Filter iPv4Filter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return iPv4Filter.isValid_hasValidTimeToLive(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(someIPFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidServiceId(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidMethodId(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidSessionId(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidClientId(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidLength(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidProtocolVersion(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidInterfaceVersion(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidMessageType(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidReturnCode(someIPFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPFilter_isValid_hasValidMethodType(someIPFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidServiceId constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidServiceId(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidServiceId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMethodId constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidMethodId(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidMethodId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSessionId constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidSessionId(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidSessionId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidClientId constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidClientId(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidClientId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidLength constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidLength(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidLength(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidProtocolVersion constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidProtocolVersion(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidProtocolVersion(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidInterfaceVersion constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidInterfaceVersion(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidInterfaceVersion(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageType constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidMessageType(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidMessageType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidReturnCode constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidReturnCode(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidReturnCode(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMethodType constraint of '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPFilter_isValid_hasValidMethodType(SomeIPFilter someIPFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPFilter.isValid_hasValidMethodType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateForEachExpression(ForEachExpression forEachExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(forEachExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(forEachExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(forEachExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMessageCheckExpression(MessageCheckExpression messageCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(messageCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(messageCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(messageCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(someIPSDFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidInstanceId(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidEventGroupId(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidFlags(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidSdType(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidMajorVersion(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidMinorVersion(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidIndexFirstOption(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidIndexSecondOption(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidNumberFirstOption(someIPSDFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSomeIPSDFilter_isValid_hasValidNumberSecondOption(someIPSDFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidInstanceId constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidInstanceId(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidInstanceId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidEventGroupId constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidEventGroupId(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidEventGroupId(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidFlags constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidFlags(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidFlags(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSdType constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidSdType(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidSdType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMajorVersion constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidMajorVersion(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidMajorVersion(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMinorVersion constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidMinorVersion(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidMinorVersion(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidIndexFirstOption constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidIndexFirstOption(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidIndexFirstOption(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidIndexSecondOption constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidIndexSecondOption(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidIndexSecondOption(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidNumberFirstOption constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidNumberFirstOption(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidNumberFirstOption(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidNumberSecondOption constraint of '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFilter_isValid_hasValidNumberSecondOption(SomeIPSDFilter someIPSDFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return someIPSDFilter.isValid_hasValidNumberSecondOption(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDMessage(SomeIPSDMessage someIPSDMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(someIPSDMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(someIPSDMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(someIPSDMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleSignal(DoubleSignal doubleSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(doubleSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringSignal(StringSignal stringSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stringSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDecodeStrategy(DecodeStrategy decodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(decodeStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleDecodeStrategy(DoubleDecodeStrategy doubleDecodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(doubleDecodeStrategy, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateDoubleDecodeStrategy_isValid_hasValidFactor(doubleDecodeStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateDoubleDecodeStrategy_isValid_hasValidOffset(doubleDecodeStrategy, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidFactor constraint of '<em>Double Decode Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleDecodeStrategy_isValid_hasValidFactor(DoubleDecodeStrategy doubleDecodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return doubleDecodeStrategy.isValid_hasValidFactor(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOffset constraint of '<em>Double Decode Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleDecodeStrategy_isValid_hasValidOffset(DoubleDecodeStrategy doubleDecodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return doubleDecodeStrategy.isValid_hasValidOffset(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExtractStrategy(ExtractStrategy extractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(extractStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadExtractStrategy(UniversalPayloadExtractStrategy universalPayloadExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(universalPayloadExtractStrategy, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadExtractStrategy_isValid_hasValidExtractString(universalPayloadExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadExtractStrategy_isValid_hasValidSignalDataType(universalPayloadExtractStrategy, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidExtractString constraint of '<em>Universal Payload Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadExtractStrategy_isValid_hasValidExtractString(UniversalPayloadExtractStrategy universalPayloadExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return universalPayloadExtractStrategy.isValid_hasValidExtractString(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidSignalDataType constraint of '<em>Universal Payload Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadExtractStrategy_isValid_hasValidSignalDataType(UniversalPayloadExtractStrategy universalPayloadExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return universalPayloadExtractStrategy.isValid_hasValidSignalDataType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEmptyExtractStrategy(EmptyExtractStrategy emptyExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(emptyExtractStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANFilter(CANFilter canFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(canFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateCANFilter_isValid_hasValidFrameIdOrFrameIdRange(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateCANFilter_isValid_hasValidRxTxFlag(canFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateCANFilter_isValid_hasValidExtIdentifier(canFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidFrameIdOrFrameIdRange constraint of '<em>CAN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANFilter_isValid_hasValidFrameIdOrFrameIdRange(CANFilter canFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canFilter.isValid_hasValidFrameIdOrFrameIdRange(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidRxTxFlag constraint of '<em>CAN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANFilter_isValid_hasValidRxTxFlag(CANFilter canFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canFilter.isValid_hasValidRxTxFlag(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidExtIdentifier constraint of '<em>CAN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANFilter_isValid_hasValidExtIdentifier(CANFilter canFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return canFilter.isValid_hasValidExtIdentifier(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLINFilter(LINFilter linFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(linFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateLINFilter_isValid_hasValidFrameIdOrFrameIdRange(linFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidFrameIdOrFrameIdRange constraint of '<em>LIN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLINFilter_isValid_hasValidFrameIdOrFrameIdRange(LINFilter linFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linFilter.isValid_hasValidFrameIdOrFrameIdRange(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayFilter(FlexRayFilter flexRayFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(flexRayFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayFilter_isValid_hasValidFrameIdOrFrameIdRange(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayFilter_isValid_hasValidChannelType(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayFilter_isValid_hasValidCycleOffset(flexRayFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateFlexRayFilter_isValid_hasValidCycleRepeatInterval(flexRayFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidFrameIdOrFrameIdRange constraint of '<em>Flex Ray Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayFilter_isValid_hasValidFrameIdOrFrameIdRange(FlexRayFilter flexRayFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayFilter.isValid_hasValidFrameIdOrFrameIdRange(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidChannelType constraint of '<em>Flex Ray Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayFilter_isValid_hasValidChannelType(FlexRayFilter flexRayFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayFilter.isValid_hasValidChannelType(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCycleOffset constraint of '<em>Flex Ray Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayFilter_isValid_hasValidCycleOffset(FlexRayFilter flexRayFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayFilter.isValid_hasValidCycleOffset(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidCycleRepeatInterval constraint of '<em>Flex Ray Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayFilter_isValid_hasValidCycleRepeatInterval(FlexRayFilter flexRayFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return flexRayFilter.isValid_hasValidCycleRepeatInterval(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dltFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDLTFilter_isValid_hasValidMessageBusInfo(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDLTFilter_isValid_hasValidMessageControlInfo(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDLTFilter_isValid_hasValidMessageLogInfo(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDLTFilter_isValid_hasValidMessageTraceInfo(dltFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDLTFilter_isValid_hasValidMessageType(dltFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidMessageBusInfo constraint of '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter_isValid_hasValidMessageBusInfo(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return dltFilter.isValid_hasValidMessageBusInfo(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageControlInfo constraint of '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter_isValid_hasValidMessageControlInfo(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return dltFilter.isValid_hasValidMessageControlInfo(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageLogInfo constraint of '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter_isValid_hasValidMessageLogInfo(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return dltFilter.isValid_hasValidMessageLogInfo(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageTraceInfo constraint of '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter_isValid_hasValidMessageTraceInfo(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return dltFilter.isValid_hasValidMessageTraceInfo(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMessageType constraint of '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTFilter_isValid_hasValidMessageType(DLTFilter dltFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return dltFilter.isValid_hasValidMessageType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMFilter(UDPNMFilter udpnmFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(udpnmFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPNMFilter_isValid_hasValidSourceNodeIdentifier(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPNMFilter_isValid_hasValidControlBitVector(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPNMFilter_isValid_hasValidPwfStatus(udpnmFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPNMFilter_isValid_hasValidTeilnetzStatus(udpnmFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidSourceNodeIdentifier constraint of '<em>UDPNM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMFilter_isValid_hasValidSourceNodeIdentifier(UDPNMFilter udpnmFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpnmFilter.isValid_hasValidSourceNodeIdentifier(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidControlBitVector constraint of '<em>UDPNM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMFilter_isValid_hasValidControlBitVector(UDPNMFilter udpnmFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpnmFilter.isValid_hasValidControlBitVector(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidPwfStatus constraint of '<em>UDPNM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMFilter_isValid_hasValidPwfStatus(UDPNMFilter udpnmFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpnmFilter.isValid_hasValidPwfStatus(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidTeilnetzStatus constraint of '<em>UDPNM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMFilter_isValid_hasValidTeilnetzStatus(UDPNMFilter udpnmFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpnmFilter.isValid_hasValidTeilnetzStatus(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANMessage(CANMessage canMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(canMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(canMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(canMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLINMessage(LINMessage linMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(linMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(linMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(linMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayMessage(FlexRayMessage flexRayMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(flexRayMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(flexRayMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(flexRayMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLTMessage(DLTMessage dltMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dltMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(dltMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(dltMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPMessage(UDPMessage udpMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(udpMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(udpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(udpMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTCPMessage(TCPMessage tcpMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(tcpMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(tcpMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(tcpMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMMessage(UDPNMMessage udpnmMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(udpnmMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(udpnmMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateUDPNMMessage_isValid_hasValidUDPFilter(udpnmMessage, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidUDPFilter constraint of '<em>UDPNM Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUDPNMMessage_isValid_hasValidUDPFilter(UDPNMMessage udpnmMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return udpnmMessage.isValid_hasValidUDPFilter(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerboseDLTMessage(VerboseDLTMessage verboseDLTMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(verboseDLTMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(verboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(verboseDLTMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadWithLegacyExtractStrategy(UniversalPayloadWithLegacyExtractStrategy universalPayloadWithLegacyExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(universalPayloadWithLegacyExtractStrategy, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadExtractStrategy_isValid_hasValidExtractString(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadExtractStrategy_isValid_hasValidSignalDataType(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadWithLegacyExtractStrategy_isValid_hasValidStartbit(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validateUniversalPayloadWithLegacyExtractStrategy_isValid_hasValidDataLength(universalPayloadWithLegacyExtractStrategy, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidStartbit constraint of '<em>Universal Payload With Legacy Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadWithLegacyExtractStrategy_isValid_hasValidStartbit(UniversalPayloadWithLegacyExtractStrategy universalPayloadWithLegacyExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return universalPayloadWithLegacyExtractStrategy.isValid_hasValidStartbit(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDataLength constraint of '<em>Universal Payload With Legacy Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUniversalPayloadWithLegacyExtractStrategy_isValid_hasValidDataLength(UniversalPayloadWithLegacyExtractStrategy universalPayloadWithLegacyExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return universalPayloadWithLegacyExtractStrategy.isValid_hasValidDataLength(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractBusMessage(AbstractBusMessage abstractBusMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(abstractBusMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(abstractBusMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(abstractBusMessage, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidBusId constraint of '<em>Abstract Bus Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractBusMessage_isValid_hasValidBusId(AbstractBusMessage abstractBusMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractBusMessage.isValid_hasValidBusId(diagnostics, context);
	}

	/**
	 * Validates the isValid_isOsiLayerConform constraint of '<em>Abstract Bus Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractBusMessage_isValid_isOsiLayerConform(AbstractBusMessage abstractBusMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractBusMessage.isValid_isOsiLayerConform(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginFilter(PluginFilter pluginFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(pluginFilter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginMessage(PluginMessage pluginMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(pluginMessage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginSignal(PluginSignal pluginSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(pluginSignal, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginStateExtractStrategy(PluginStateExtractStrategy pluginStateExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(pluginStateExtractStrategy, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validatePluginStateExtractStrategy_isValid_hasValidStatesActive(pluginStateExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validatePluginStateExtractStrategy_isValid_hasValidState(pluginStateExtractStrategy, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidStatesActive constraint of '<em>Plugin State Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginStateExtractStrategy_isValid_hasValidStatesActive(PluginStateExtractStrategy pluginStateExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return pluginStateExtractStrategy.isValid_hasValidStatesActive(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidState constraint of '<em>Plugin State Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginStateExtractStrategy_isValid_hasValidState(PluginStateExtractStrategy pluginStateExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return pluginStateExtractStrategy.isValid_hasValidState(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginResultExtractStrategy(PluginResultExtractStrategy pluginResultExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(pluginResultExtractStrategy, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(pluginResultExtractStrategy, diagnostics, context);
		if (result || diagnostics != null) result &= validatePluginResultExtractStrategy_isValid_hasValidResultRange(pluginResultExtractStrategy, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidResultRange constraint of '<em>Plugin Result Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginResultExtractStrategy_isValid_hasValidResultRange(PluginResultExtractStrategy pluginResultExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return pluginResultExtractStrategy.isValid_hasValidResultRange(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEmptyDecodeStrategy(EmptyDecodeStrategy emptyDecodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(emptyDecodeStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginCheckExpression(PluginCheckExpression pluginCheckExpression, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(pluginCheckExpression, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(pluginCheckExpression, diagnostics, context);
		if (result || diagnostics != null) result &= validateExpression_isValid_hasValidDescription(pluginCheckExpression, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBaseClassWithID(BaseClassWithID baseClassWithID, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(baseClassWithID, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHeaderSignal(HeaderSignal headerSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(headerSignal, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(headerSignal, diagnostics, context);
		if (result || diagnostics != null) result &= validateHeaderSignal_isValid_hasValidAttribute(headerSignal, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidAttribute constraint of '<em>Header Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHeaderSignal_isValid_hasValidAttribute(HeaderSignal headerSignal, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return headerSignal.isValid_hasValidAttribute(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIVariableReaderWriter(IVariableReaderWriter iVariableReaderWriter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iVariableReaderWriter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIStateTransitionReference(IStateTransitionReference iStateTransitionReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(iStateTransitionReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTPFilter(TPFilter tpFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tpFilter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBaseClassWithSourceReference(BaseClassWithSourceReference baseClassWithSourceReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(baseClassWithSourceReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSourceReference(SourceReference sourceReference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sourceReference, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetMessage(EthernetMessage ethernetMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(ethernetMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(ethernetMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(ethernetMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPv4Message(IPv4Message iPv4Message, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(iPv4Message, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(iPv4Message, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(iPv4Message, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonVerboseDLTMessage(NonVerboseDLTMessage nonVerboseDLTMessage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(nonVerboseDLTMessage, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_hasValidBusId(nonVerboseDLTMessage, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractBusMessage_isValid_isOsiLayerConform(nonVerboseDLTMessage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringDecodeStrategy(StringDecodeStrategy stringDecodeStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stringDecodeStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonVerboseDLTFilter(NonVerboseDLTFilter nonVerboseDLTFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(nonVerboseDLTFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(nonVerboseDLTFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateNonVerboseDLTFilter_isValid_hasValidMessageIdOrMessageIdRange(nonVerboseDLTFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidMessageIdOrMessageIdRange constraint of '<em>Non Verbose DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonVerboseDLTFilter_isValid_hasValidMessageIdOrMessageIdRange(NonVerboseDLTFilter nonVerboseDLTFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return nonVerboseDLTFilter.isValid_hasValidMessageIdOrMessageIdRange(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerboseDLTExtractStrategy(VerboseDLTExtractStrategy verboseDLTExtractStrategy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(verboseDLTExtractStrategy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegexOperation(RegexOperation regexOperation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(regexOperation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(regexOperation, diagnostics, context);
		if (result || diagnostics != null) result &= validateRegexOperation_isValid_hasValidRegex(regexOperation, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidRegex constraint of '<em>Regex Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegexOperation_isValid_hasValidRegex(RegexOperation regexOperation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return regexOperation.isValid_hasValidRegex(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePayloadFilter(PayloadFilter payloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(payloadFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validatePayloadFilter_isValid_hasValidIndex(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validatePayloadFilter_isValid_hasValidMask(payloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validatePayloadFilter_isValid_hasValidValue(payloadFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidIndex constraint of '<em>Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePayloadFilter_isValid_hasValidIndex(PayloadFilter payloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return payloadFilter.isValid_hasValidIndex(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidMask constraint of '<em>Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePayloadFilter_isValid_hasValidMask(PayloadFilter payloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return payloadFilter.isValid_hasValidMask(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidValue constraint of '<em>Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePayloadFilter_isValid_hasValidValue(PayloadFilter payloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return payloadFilter.isValid_hasValidValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerboseDLTPayloadFilter(VerboseDLTPayloadFilter verboseDLTPayloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(verboseDLTPayloadFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(verboseDLTPayloadFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateVerboseDLTPayloadFilter_isValid_hasValidRegex(verboseDLTPayloadFilter, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidRegex constraint of '<em>Verbose DLT Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerboseDLTPayloadFilter_isValid_hasValidRegex(VerboseDLTPayloadFilter verboseDLTPayloadFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return verboseDLTPayloadFilter.isValid_hasValidRegex(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractVariable(AbstractVariable abstractVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(abstractVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(abstractVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(abstractVariable, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidName constraint of '<em>Abstract Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractVariable_isValid_hasValidName(AbstractVariable abstractVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractVariable.isValid_hasValidName(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidDisplayName constraint of '<em>Abstract Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractVariable_isValid_hasValidDisplayName(AbstractVariable abstractVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractVariable.isValid_hasValidDisplayName(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVariableStructure(VariableStructure variableStructure, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(variableStructure, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(variableStructure, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(variableStructure, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputedVariable(ComputedVariable computedVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(computedVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidName(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractVariable_isValid_hasValidDisplayName(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidInterpretedValue(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidType(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidUnit(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateVariable_isValid_hasValidVariableFormatTmplParam(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputedVariable_isValid_hasValidExpression(computedVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputedVariable_isValid_hasValidOperands(computedVariable, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidExpression constraint of '<em>Computed Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputedVariable_isValid_hasValidExpression(ComputedVariable computedVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return computedVariable.isValid_hasValidExpression(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidOperands constraint of '<em>Computed Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputedVariable_isValid_hasValidOperands(ComputedVariable computedVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return computedVariable.isValid_hasValidOperands(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCheckType(CheckType checkType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparator(Comparator comparator, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalOperatorType(LogicalOperatorType logicalOperatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalDataType(SignalDataType signalDataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexChannelType(FlexChannelType flexChannelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateByteOrderType(ByteOrderType byteOrderType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFormatDataType(FormatDataType formatDataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFlexRayHeaderFlagSelection(FlexRayHeaderFlagSelection flexRayHeaderFlagSelection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBooleanOrTemplatePlaceholderEnum(BooleanOrTemplatePlaceholderEnum booleanOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProtocolTypeOrTemplatePlaceholderEnum(ProtocolTypeOrTemplatePlaceholderEnum protocolTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEthernetProtocolTypeSelection(EthernetProtocolTypeSelection ethernetProtocolTypeSelection, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRxTxFlagTypeOrTemplatePlaceholderEnum(RxTxFlagTypeOrTemplatePlaceholderEnum rxTxFlagTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObserverValueType(ObserverValueType observerValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePluginStateValueTypeOrTemplatePlaceholderEnum(PluginStateValueTypeOrTemplatePlaceholderEnum pluginStateValueTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAnalysisTypeOrTemplatePlaceholderEnum(AnalysisTypeOrTemplatePlaceholderEnum analysisTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPMessageTypeOrTemplatePlaceholderEnum(SomeIPMessageTypeOrTemplatePlaceholderEnum someIPMessageTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPMethodTypeOrTemplatePlaceholderEnum(SomeIPMethodTypeOrTemplatePlaceholderEnum someIPMethodTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPReturnCodeOrTemplatePlaceholderEnum(SomeIPReturnCodeOrTemplatePlaceholderEnum someIPReturnCodeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDFlagsOrTemplatePlaceholderEnum(SomeIPSDFlagsOrTemplatePlaceholderEnum someIPSDFlagsOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDTypeOrTemplatePlaceholderEnum(SomeIPSDTypeOrTemplatePlaceholderEnum someIPSDTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStreamAnalysisFlagsOrTemplatePlaceholderEnum(StreamAnalysisFlagsOrTemplatePlaceholderEnum streamAnalysisFlagsOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSomeIPSDEntryFlagsOrTemplatePlaceholderEnum(SomeIPSDEntryFlagsOrTemplatePlaceholderEnum someIPSDEntryFlagsOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCanExtIdentifierOrTemplatePlaceholderEnum(CanExtIdentifierOrTemplatePlaceholderEnum canExtIdentifierOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataType(DataType dataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLT_MessageType(DLT_MessageType dlT_MessageType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLT_MessageTraceInfo(DLT_MessageTraceInfo dlT_MessageTraceInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLT_MessageLogInfo(DLT_MessageLogInfo dlT_MessageLogInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLT_MessageControlInfo(DLT_MessageControlInfo dlT_MessageControlInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDLT_MessageBusInfo(DLT_MessageBusInfo dlT_MessageBusInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateForEachExpressionModifierTemplate(ForEachExpressionModifierTemplate forEachExpressionModifierTemplate, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAUTOSARDataType(AUTOSARDataType autosarDataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCANFrameTypeOrTemplatePlaceholderEnum(CANFrameTypeOrTemplatePlaceholderEnum canFrameTypeOrTemplatePlaceholderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvaluationBehaviourOrTemplateDefinedEnum(EvaluationBehaviourOrTemplateDefinedEnum evaluationBehaviourOrTemplateDefinedEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSignalVariableLagEnum(SignalVariableLagEnum signalVariableLagEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTTLOrTemplatePlaceHolderEnum(TTLOrTemplatePlaceHolderEnum ttlOrTemplatePlaceHolderEnum, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCheckTypeObject(CheckType checkTypeObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparatorObject(Comparator comparatorObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalOperatorTypeObject(LogicalOperatorType logicalOperatorTypeObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHexOrIntOrTemplatePlaceholder(String hexOrIntOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntOrTemplatePlaceholder(String intOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLongOrTemplatePlaceholder(String longOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleOrTemplatePlaceholder(String doubleOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleOrHexOrTemplatePlaceholder(String doubleOrHexOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPPatternOrTemplatePlaceholder(String ipPatternOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMACPatternOrTemplatePlaceholder(String macPatternOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortPatternOrTemplatePlaceholder(String portPatternOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVlanIdPatternOrTemplatePlaceholder(String vlanIdPatternOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIPOrTemplatePlaceholder(String ipOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortOrTemplatePlaceholder(String portOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVlanIdOrTemplatePlaceholder(String vlanIdOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMACOrTemplatePlaceholder(String macOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBitmaskOrTemplatePlaceholder(String bitmaskOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHexOrIntOrNullOrTemplatePlaceholder(String hexOrIntOrNullOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonZeroDoubleOrTemplatePlaceholder(String nonZeroDoubleOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValVarInitialValueDoubleOrTemplatePlaceholderOrString(String valVarInitialValueDoubleOrTemplatePlaceholderOrString, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateByteOrTemplatePlaceholder(String byteOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ConditionsValidator
