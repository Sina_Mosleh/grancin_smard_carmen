/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DLT Message Control Info</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getDLT_MessageControlInfo()
 * @model
 * @generated
 */
public enum DLT_MessageControlInfo implements Enumerator {
	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_SPECIFIED(0, "NOT_SPECIFIED", "NOT_SPECIFIED"),

	/**
	 * The '<em><b>DLT CONTROL REQUEST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_REQUEST_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_CONTROL_REQUEST(1, "DLT_CONTROL_REQUEST", "DLT_CONTROL_REQUEST"),

	/**
	 * The '<em><b>DLT CONTROL RESPONSE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_RESPONSE_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_CONTROL_RESPONSE(2, "DLT_CONTROL_RESPONSE", "DLT_CONTROL_RESPONSE"),

	/**
	 * The '<em><b>DLT CONTROL TIME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_TIME_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_CONTROL_TIME(3, "DLT_CONTROL_TIME", "DLT_CONTROL_TIME"),

	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined");

	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOT_SPECIFIED_VALUE = 0;

	/**
	 * The '<em><b>DLT CONTROL REQUEST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_REQUEST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_CONTROL_REQUEST_VALUE = 1;

	/**
	 * The '<em><b>DLT CONTROL RESPONSE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_RESPONSE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_CONTROL_RESPONSE_VALUE = 2;

	/**
	 * The '<em><b>DLT CONTROL TIME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_CONTROL_TIME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_CONTROL_TIME_VALUE = 3;

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * An array of all the '<em><b>DLT Message Control Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DLT_MessageControlInfo[] VALUES_ARRAY =
		new DLT_MessageControlInfo[] {
			NOT_SPECIFIED,
			DLT_CONTROL_REQUEST,
			DLT_CONTROL_RESPONSE,
			DLT_CONTROL_TIME,
			TEMPLATE_DEFINED,
		};

	/**
	 * A public read-only list of all the '<em><b>DLT Message Control Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DLT_MessageControlInfo> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DLT Message Control Info</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageControlInfo get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageControlInfo result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Control Info</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageControlInfo getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageControlInfo result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Control Info</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageControlInfo get(int value) {
		switch (value) {
			case NOT_SPECIFIED_VALUE: return NOT_SPECIFIED;
			case DLT_CONTROL_REQUEST_VALUE: return DLT_CONTROL_REQUEST;
			case DLT_CONTROL_RESPONSE_VALUE: return DLT_CONTROL_RESPONSE;
			case DLT_CONTROL_TIME_VALUE: return DLT_CONTROL_TIME;
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DLT_MessageControlInfo(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DLT_MessageControlInfo
