/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stop Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}</li>
 *   <li>{@link conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getStopExpression()
 * @model extendedMetaData="name='stopCondition' kind='empty'"
 * @generated
 */
public interface StopExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Analyse Art</b></em>' attribute.
	 * The default value is <code>"ALL"</code>.
	 * The literals are from the enumeration {@link conditions.AnalysisTypeOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analyse Art</em>' attribute.
	 * @see conditions.AnalysisTypeOrTemplatePlaceholderEnum
	 * @see #setAnalyseArt(AnalysisTypeOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getStopExpression_AnalyseArt()
	 * @model default="ALL" required="true"
	 *        extendedMetaData="kind='attribute' name='analyseArt' namespace='##targetNamespace'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	AnalysisTypeOrTemplatePlaceholderEnum getAnalyseArt();

	/**
	 * Sets the value of the '{@link conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analyse Art</em>' attribute.
	 * @see conditions.AnalysisTypeOrTemplatePlaceholderEnum
	 * @see #getAnalyseArt()
	 * @generated
	 */
	void setAnalyseArt(AnalysisTypeOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Analyse Art Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Analyse Art Tmpl Param</em>' attribute.
	 * @see #setAnalyseArtTmplParam(String)
	 * @see conditions.ConditionsPackage#getStopExpression_AnalyseArtTmplParam()
	 * @model
	 * @generated
	 */
	String getAnalyseArtTmplParam();

	/**
	 * Sets the value of the '{@link conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Analyse Art Tmpl Param</em>' attribute.
	 * @see #getAnalyseArtTmplParam()
	 * @generated
	 */
	void setAnalyseArtTmplParam(String value);

} // StopExpression
