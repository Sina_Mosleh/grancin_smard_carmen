/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getSomeIPSDTypeOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum SomeIPSDTypeOrTemplatePlaceholderEnum implements Enumerator {
	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

	/**
	 * The '<em><b>FIND SVC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIND_SVC_VALUE
	 * @generated
	 * @ordered
	 */
	FIND_SVC(0, "FIND_SVC", "FIND_SVC"),

	/**
	 * The '<em><b>OFFER SVC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OFFER_SVC_VALUE
	 * @generated
	 * @ordered
	 */
	OFFER_SVC(2, "OFFER_SVC", "OFFER_SVC"),

	/**
	 * The '<em><b>REQUEST SVC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_SVC_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST_SVC(4, "REQUEST_SVC", "REQUEST_SVC"),

	/**
	 * The '<em><b>REQUEST SVC ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_SVC_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST_SVC_ACK(6, "REQUEST_SVC_ACK", "REQUEST_SVC_ACK"),

	/**
	 * The '<em><b>FIND EVENTGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIND_EVENTGROUP_VALUE
	 * @generated
	 * @ordered
	 */
	FIND_EVENTGROUP(8, "FIND_EVENTGROUP", "FIND_EVENTGROUP"),

	/**
	 * The '<em><b>PUBLISH EVENTGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PUBLISH_EVENTGROUP_VALUE
	 * @generated
	 * @ordered
	 */
	PUBLISH_EVENTGROUP(10, "PUBLISH_EVENTGROUP", "PUBLISH_EVENTGROUP"),

	/**
	 * The '<em><b>SUBSCRIBE EVENTGROUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBSCRIBE_EVENTGROUP_VALUE
	 * @generated
	 * @ordered
	 */
	SUBSCRIBE_EVENTGROUP(12, "SUBSCRIBE_EVENTGROUP", "SUBSCRIBE_EVENTGROUP"),

	/**
	 * The '<em><b>SUBSCRIBE EVENTGROUP ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBSCRIBE_EVENTGROUP_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	SUBSCRIBE_EVENTGROUP_ACK(14, "SUBSCRIBE_EVENTGROUP_ACK", "SUBSCRIBE_EVENTGROUP_ACK"),

	/**
	 * The '<em><b>ALL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_VALUE
	 * @generated
	 * @ordered
	 */
	ALL(16, "ALL", "ALL");

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * The '<em><b>FIND SVC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIND_SVC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIND_SVC_VALUE = 0;

	/**
	 * The '<em><b>OFFER SVC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OFFER_SVC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OFFER_SVC_VALUE = 2;

	/**
	 * The '<em><b>REQUEST SVC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_SVC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_SVC_VALUE = 4;

	/**
	 * The '<em><b>REQUEST SVC ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_SVC_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_SVC_ACK_VALUE = 6;

	/**
	 * The '<em><b>FIND EVENTGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIND_EVENTGROUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIND_EVENTGROUP_VALUE = 8;

	/**
	 * The '<em><b>PUBLISH EVENTGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PUBLISH_EVENTGROUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PUBLISH_EVENTGROUP_VALUE = 10;

	/**
	 * The '<em><b>SUBSCRIBE EVENTGROUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBSCRIBE_EVENTGROUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUBSCRIBE_EVENTGROUP_VALUE = 12;

	/**
	 * The '<em><b>SUBSCRIBE EVENTGROUP ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUBSCRIBE_EVENTGROUP_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SUBSCRIBE_EVENTGROUP_ACK_VALUE = 14;

	/**
	 * The '<em><b>ALL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ALL_VALUE = 16;

	/**
	 * An array of all the '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SomeIPSDTypeOrTemplatePlaceholderEnum[] VALUES_ARRAY =
		new SomeIPSDTypeOrTemplatePlaceholderEnum[] {
			TEMPLATE_DEFINED,
			FIND_SVC,
			OFFER_SVC,
			REQUEST_SVC,
			REQUEST_SVC_ACK,
			FIND_EVENTGROUP,
			PUBLISH_EVENTGROUP,
			SUBSCRIBE_EVENTGROUP,
			SUBSCRIBE_EVENTGROUP_ACK,
			ALL,
		};

	/**
	 * A public read-only list of all the '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SomeIPSDTypeOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPSDTypeOrTemplatePlaceholderEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SomeIPSDTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPSDTypeOrTemplatePlaceholderEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SomeIPSDTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Some IPSD Type Or Template Placeholder Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPSDTypeOrTemplatePlaceholderEnum get(int value) {
		switch (value) {
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
			case FIND_SVC_VALUE: return FIND_SVC;
			case OFFER_SVC_VALUE: return OFFER_SVC;
			case REQUEST_SVC_VALUE: return REQUEST_SVC;
			case REQUEST_SVC_ACK_VALUE: return REQUEST_SVC_ACK;
			case FIND_EVENTGROUP_VALUE: return FIND_EVENTGROUP;
			case PUBLISH_EVENTGROUP_VALUE: return PUBLISH_EVENTGROUP;
			case SUBSCRIBE_EVENTGROUP_VALUE: return SUBSCRIBE_EVENTGROUP;
			case SUBSCRIBE_EVENTGROUP_ACK_VALUE: return SUBSCRIBE_EVENTGROUP_ACK;
			case ALL_VALUE: return ALL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SomeIPSDTypeOrTemplatePlaceholderEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SomeIPSDTypeOrTemplatePlaceholderEnum
