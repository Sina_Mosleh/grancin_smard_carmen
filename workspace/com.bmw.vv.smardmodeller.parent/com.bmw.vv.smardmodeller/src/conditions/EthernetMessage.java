/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ethernet Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.EthernetMessage#getEthernetFilter <em>Ethernet Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getEthernetMessage()
 * @model
 * @generated
 */
public interface EthernetMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Ethernet Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ethernet Filter</em>' containment reference.
	 * @see #setEthernetFilter(EthernetFilter)
	 * @see conditions.ConditionsPackage#getEthernetMessage_EthernetFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EthernetFilter getEthernetFilter();

	/**
	 * Sets the value of the '{@link conditions.EthernetMessage#getEthernetFilter <em>Ethernet Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ethernet Filter</em>' containment reference.
	 * @see #getEthernetFilter()
	 * @generated
	 */
	void setEthernetFilter(EthernetFilter value);

} // EthernetMessage
