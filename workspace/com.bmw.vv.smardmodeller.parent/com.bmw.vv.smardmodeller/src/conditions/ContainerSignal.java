/**
 */
package conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ContainerSignal#getContainedSignals <em>Contained Signals</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getContainerSignal()
 * @model
 * @generated
 */
public interface ContainerSignal extends AbstractSignal {
	/**
	 * Returns the value of the '<em><b>Contained Signals</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.AbstractSignal}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Signals</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getContainerSignal_ContainedSignals()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractSignal> getContainedSignals();

} // ContainerSignal
