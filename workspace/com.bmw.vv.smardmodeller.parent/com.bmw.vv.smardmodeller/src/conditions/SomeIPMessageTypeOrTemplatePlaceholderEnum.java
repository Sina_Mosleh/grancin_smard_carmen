/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getSomeIPMessageTypeOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum SomeIPMessageTypeOrTemplatePlaceholderEnum implements Enumerator {
	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

	/**
	 * The '<em><b>REQUEST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST(0, "REQUEST", "REQUEST"),

	/**
	 * The '<em><b>REQUEST NO RETURN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_NO_RETURN_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST_NO_RETURN(1, "REQUEST_NO_RETURN", "REQUEST_NO_RETURN"),

	/**
	 * The '<em><b>NOTIFICATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTIFICATION_VALUE
	 * @generated
	 * @ordered
	 */
	NOTIFICATION(2, "NOTIFICATION", "NOTIFICATION"),

	/**
	 * The '<em><b>REQUEST ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST_ACK(3, "REQUEST_ACK", "REQUEST_ACK"),

	/**
	 * The '<em><b>REQUEST NO RETURN ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_NO_RETURN_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	REQUEST_NO_RETURN_ACK(4, "REQUEST_NO_RETURN_ACK", "REQUEST_NO_RETURN_ACK"),

	/**
	 * The '<em><b>NOTIFICATION ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTIFICATION_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	NOTIFICATION_ACK(5, "NOTIFICATION_ACK", "NOTIFICATION_ACK"),

	/**
	 * The '<em><b>RESPONSE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESPONSE_VALUE
	 * @generated
	 * @ordered
	 */
	RESPONSE(6, "RESPONSE", "RESPONSE"),

	/**
	 * The '<em><b>ERROR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERROR_VALUE
	 * @generated
	 * @ordered
	 */
	ERROR(7, "ERROR", "ERROR"),

	/**
	 * The '<em><b>RESPONSE ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESPONSE_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	RESPONSE_ACK(8, "RESPONSE_ACK", "RESPONSE_ACK"),

	/**
	 * The '<em><b>ERROR ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERROR_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	ERROR_ACK(9, "ERROR_ACK", "ERROR_ACK"),

	/**
	 * The '<em><b>ALL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_VALUE
	 * @generated
	 * @ordered
	 */
	ALL(10, "ALL", "ALL");

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * The '<em><b>REQUEST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_VALUE = 0;

	/**
	 * The '<em><b>REQUEST NO RETURN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_NO_RETURN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_NO_RETURN_VALUE = 1;

	/**
	 * The '<em><b>NOTIFICATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTIFICATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOTIFICATION_VALUE = 2;

	/**
	 * The '<em><b>REQUEST ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_ACK_VALUE = 3;

	/**
	 * The '<em><b>REQUEST NO RETURN ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REQUEST_NO_RETURN_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REQUEST_NO_RETURN_ACK_VALUE = 4;

	/**
	 * The '<em><b>NOTIFICATION ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOTIFICATION_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOTIFICATION_ACK_VALUE = 5;

	/**
	 * The '<em><b>RESPONSE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESPONSE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RESPONSE_VALUE = 6;

	/**
	 * The '<em><b>ERROR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERROR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ERROR_VALUE = 7;

	/**
	 * The '<em><b>RESPONSE ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESPONSE_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RESPONSE_ACK_VALUE = 8;

	/**
	 * The '<em><b>ERROR ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ERROR_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ERROR_ACK_VALUE = 9;

	/**
	 * The '<em><b>ALL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ALL_VALUE = 10;

	/**
	 * An array of all the '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SomeIPMessageTypeOrTemplatePlaceholderEnum[] VALUES_ARRAY =
		new SomeIPMessageTypeOrTemplatePlaceholderEnum[] {
			TEMPLATE_DEFINED,
			REQUEST,
			REQUEST_NO_RETURN,
			NOTIFICATION,
			REQUEST_ACK,
			REQUEST_NO_RETURN_ACK,
			NOTIFICATION_ACK,
			RESPONSE,
			ERROR,
			RESPONSE_ACK,
			ERROR_ACK,
			ALL,
		};

	/**
	 * A public read-only list of all the '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SomeIPMessageTypeOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPMessageTypeOrTemplatePlaceholderEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SomeIPMessageTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPMessageTypeOrTemplatePlaceholderEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SomeIPMessageTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Some IP Message Type Or Template Placeholder Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SomeIPMessageTypeOrTemplatePlaceholderEnum get(int value) {
		switch (value) {
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
			case REQUEST_VALUE: return REQUEST;
			case REQUEST_NO_RETURN_VALUE: return REQUEST_NO_RETURN;
			case NOTIFICATION_VALUE: return NOTIFICATION;
			case REQUEST_ACK_VALUE: return REQUEST_ACK;
			case REQUEST_NO_RETURN_ACK_VALUE: return REQUEST_NO_RETURN_ACK;
			case NOTIFICATION_ACK_VALUE: return NOTIFICATION_ACK;
			case RESPONSE_VALUE: return RESPONSE;
			case ERROR_VALUE: return ERROR;
			case RESPONSE_ACK_VALUE: return RESPONSE_ACK;
			case ERROR_ACK_VALUE: return ERROR_ACK;
			case ALL_VALUE: return ALL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SomeIPMessageTypeOrTemplatePlaceholderEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SomeIPMessageTypeOrTemplatePlaceholderEnum
