/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DLT Message</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getDLTMessage()
 * @model abstract="true"
 * @generated
 */
public interface DLTMessage extends AbstractBusMessage {
} // DLTMessage
