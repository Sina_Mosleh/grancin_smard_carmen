/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getSignalObserver()
 * @model
 * @generated
 */
public interface SignalObserver extends AbstractObserver {
} // SignalObserver
