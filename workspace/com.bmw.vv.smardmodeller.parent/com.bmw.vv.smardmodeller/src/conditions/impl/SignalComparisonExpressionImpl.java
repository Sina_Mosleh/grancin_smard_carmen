/**
 */
package conditions.impl;

import conditions.Comparator;
import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.IOperand;
import conditions.IOperation;
import conditions.ISignalComparisonExpressionOperand;
import conditions.SignalComparisonExpression;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Comparison Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SignalComparisonExpressionImpl#getComparatorSignal <em>Comparator Signal</em>}</li>
 *   <li>{@link conditions.impl.SignalComparisonExpressionImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link conditions.impl.SignalComparisonExpressionImpl#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalComparisonExpressionImpl extends ExpressionImpl implements SignalComparisonExpression {
	/**
	 * The cached value of the '{@link #getComparatorSignal() <em>Comparator Signal</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComparatorSignal()
	 * @generated
	 * @ordered
	 */
	protected EList<ISignalComparisonExpressionOperand> comparatorSignal;

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final Comparator OPERATOR_EDEFAULT = Comparator.EQ;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected Comparator operator = OPERATOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATOR_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String operatorTmplParam = OPERATOR_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalComparisonExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSignalComparisonExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ISignalComparisonExpressionOperand> getComparatorSignal() {
		if (comparatorSignal == null) {
			comparatorSignal = new EObjectContainmentEList<ISignalComparisonExpressionOperand>(ISignalComparisonExpressionOperand.class, this, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL);
		}
		return comparatorSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Comparator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOperator(Comparator newOperator) {
		Comparator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR, oldOperator, operator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOperatorTmplParam() {
		return operatorTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOperatorTmplParam(String newOperatorTmplParam) {
		String oldOperatorTmplParam = operatorTmplParam;
		operatorTmplParam = newOperatorTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM, oldOperatorTmplParam, operatorTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidComparator(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_COMPARATOR,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidComparator", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOperands", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				return ((InternalEList<?>)getComparatorSignal()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				return getComparatorSignal();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
				return getOperator();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
				return getOperatorTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				getComparatorSignal().clear();
				getComparatorSignal().addAll((Collection<? extends ISignalComparisonExpressionOperand>)newValue);
				return;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
				setOperator((Comparator)newValue);
				return;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
				setOperatorTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				getComparatorSignal().clear();
				return;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
				setOperator(OPERATOR_EDEFAULT);
				return;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
				setOperatorTmplParam(OPERATOR_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				return comparatorSignal != null && !comparatorSignal.isEmpty();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
				return operator != OPERATOR_EDEFAULT;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
				return OPERATOR_TMPL_PARAM_EDEFAULT == null ? operatorTmplParam != null : !OPERATOR_TMPL_PARAM_EDEFAULT.equals(operatorTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IOperand.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERAND___GET_EVALUATION_DATA_TYPE: return ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_EVALUATION_DATA_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISignalComparisonExpressionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == IOperation.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERATION___GET_OPERAND_DATA_TYPE: return ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_OPERAND_DATA_TYPE;
				case ConditionsPackage.IOPERATION___GET_OPERANDS: return ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_OPERANDS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___IS_VALID_HAS_VALID_COMPARATOR__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidComparator((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOperands((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_OPERANDS:
				return get_Operands();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (operator: ");
		result.append(operator);
		result.append(", operatorTmplParam: ");
		result.append(operatorTmplParam);
		result.append(')');
		return result.toString();
	}

} //SignalComparisonExpressionImpl
