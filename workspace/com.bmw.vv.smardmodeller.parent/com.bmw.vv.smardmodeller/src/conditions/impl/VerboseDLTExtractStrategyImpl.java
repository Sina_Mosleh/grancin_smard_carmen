/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.VerboseDLTExtractStrategy;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Verbose DLT Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VerboseDLTExtractStrategyImpl extends ExtractStrategyImpl implements VerboseDLTExtractStrategy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VerboseDLTExtractStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getVerboseDLTExtractStrategy();
	}

} //VerboseDLTExtractStrategyImpl
