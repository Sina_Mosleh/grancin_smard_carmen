/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.LINFilter;
import conditions.LINMessage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LIN Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.LINMessageImpl#getLinFilter <em>Lin Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LINMessageImpl extends AbstractBusMessageImpl implements LINMessage {
	/**
	 * The cached value of the '{@link #getLinFilter() <em>Lin Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinFilter()
	 * @generated
	 * @ordered
	 */
	protected LINFilter linFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LINMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getLINMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINFilter getLinFilter() {
		return linFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLinFilter(LINFilter newLinFilter, NotificationChain msgs) {
		LINFilter oldLinFilter = linFilter;
		linFilter = newLinFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE__LIN_FILTER, oldLinFilter, newLinFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLinFilter(LINFilter newLinFilter) {
		if (newLinFilter != linFilter) {
			NotificationChain msgs = null;
			if (linFilter != null)
				msgs = ((InternalEObject)linFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.LIN_MESSAGE__LIN_FILTER, null, msgs);
			if (newLinFilter != null)
				msgs = ((InternalEObject)newLinFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.LIN_MESSAGE__LIN_FILTER, null, msgs);
			msgs = basicSetLinFilter(newLinFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE__LIN_FILTER, newLinFilter, newLinFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.LIN_MESSAGE__LIN_FILTER:
				return basicSetLinFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.LIN_MESSAGE__LIN_FILTER:
				return getLinFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.LIN_MESSAGE__LIN_FILTER:
				setLinFilter((LINFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LIN_MESSAGE__LIN_FILTER:
				setLinFilter((LINFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LIN_MESSAGE__LIN_FILTER:
				return linFilter != null;
		}
		return super.eIsSet(featureID);
	}

} //LINMessageImpl
