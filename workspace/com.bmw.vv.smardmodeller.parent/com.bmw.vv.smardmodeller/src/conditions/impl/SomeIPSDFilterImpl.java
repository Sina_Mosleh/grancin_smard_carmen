/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum;
import conditions.SomeIPSDFilter;
import conditions.SomeIPSDTypeOrTemplatePlaceholderEnum;
import conditions.TTLOrTemplatePlaceHolderEnum;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Some IPSD Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getFlags <em>Flags</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getFlagsTmplParam <em>Flags Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getSdType <em>Sd Type</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getSdTypeTmplParam <em>Sd Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getInstanceId <em>Instance Id</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getTtl <em>Ttl</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getMajorVersion <em>Major Version</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getMinorVersion <em>Minor Version</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getEventGroupId <em>Event Group Id</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getIndexFirstOption <em>Index First Option</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getIndexSecondOption <em>Index Second Option</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getNumberFirstOption <em>Number First Option</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getNumberSecondOption <em>Number Second Option</em>}</li>
 *   <li>{@link conditions.impl.SomeIPSDFilterImpl#getServiceId_SomeIPSD <em>Service Id Some IPSD</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SomeIPSDFilterImpl extends AbstractFilterImpl implements SomeIPSDFilter {
	/**
	 * The default value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected static final SomeIPSDEntryFlagsOrTemplatePlaceholderEnum FLAGS_EDEFAULT = SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getFlags() <em>Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlags()
	 * @generated
	 * @ordered
	 */
	protected SomeIPSDEntryFlagsOrTemplatePlaceholderEnum flags = FLAGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getFlagsTmplParam() <em>Flags Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlagsTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String FLAGS_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFlagsTmplParam() <em>Flags Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlagsTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String flagsTmplParam = FLAGS_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSdType() <em>Sd Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdType()
	 * @generated
	 * @ordered
	 */
	protected static final SomeIPSDTypeOrTemplatePlaceholderEnum SD_TYPE_EDEFAULT = SomeIPSDTypeOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getSdType() <em>Sd Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdType()
	 * @generated
	 * @ordered
	 */
	protected SomeIPSDTypeOrTemplatePlaceholderEnum sdType = SD_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSdTypeTmplParam() <em>Sd Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String SD_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSdTypeTmplParam() <em>Sd Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String sdTypeTmplParam = SD_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getInstanceId() <em>Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceId()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstanceId() <em>Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceId()
	 * @generated
	 * @ordered
	 */
	protected String instanceId = INSTANCE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getTtl() <em>Ttl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTtl()
	 * @generated
	 * @ordered
	 */
	protected static final TTLOrTemplatePlaceHolderEnum TTL_EDEFAULT = TTLOrTemplatePlaceHolderEnum.ZERO;

	/**
	 * The cached value of the '{@link #getTtl() <em>Ttl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTtl()
	 * @generated
	 * @ordered
	 */
	protected TTLOrTemplatePlaceHolderEnum ttl = TTL_EDEFAULT;

	/**
	 * The default value of the '{@link #getMajorVersion() <em>Major Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String MAJOR_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMajorVersion() <em>Major Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMajorVersion()
	 * @generated
	 * @ordered
	 */
	protected String majorVersion = MAJOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinorVersion() <em>Minor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinorVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String MINOR_VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinorVersion() <em>Minor Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinorVersion()
	 * @generated
	 * @ordered
	 */
	protected String minorVersion = MINOR_VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getEventGroupId() <em>Event Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventGroupId()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_GROUP_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEventGroupId() <em>Event Group Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventGroupId()
	 * @generated
	 * @ordered
	 */
	protected String eventGroupId = EVENT_GROUP_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndexFirstOption() <em>Index First Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexFirstOption()
	 * @generated
	 * @ordered
	 */
	protected static final String INDEX_FIRST_OPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIndexFirstOption() <em>Index First Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexFirstOption()
	 * @generated
	 * @ordered
	 */
	protected String indexFirstOption = INDEX_FIRST_OPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndexSecondOption() <em>Index Second Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexSecondOption()
	 * @generated
	 * @ordered
	 */
	protected static final String INDEX_SECOND_OPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIndexSecondOption() <em>Index Second Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexSecondOption()
	 * @generated
	 * @ordered
	 */
	protected String indexSecondOption = INDEX_SECOND_OPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberFirstOption() <em>Number First Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberFirstOption()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_FIRST_OPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNumberFirstOption() <em>Number First Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberFirstOption()
	 * @generated
	 * @ordered
	 */
	protected String numberFirstOption = NUMBER_FIRST_OPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberSecondOption() <em>Number Second Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberSecondOption()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMBER_SECOND_OPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNumberSecondOption() <em>Number Second Option</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberSecondOption()
	 * @generated
	 * @ordered
	 */
	protected String numberSecondOption = NUMBER_SECOND_OPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getServiceId_SomeIPSD() <em>Service Id Some IPSD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceId_SomeIPSD()
	 * @generated
	 * @ordered
	 */
	protected static final String SERVICE_ID_SOME_IPSD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getServiceId_SomeIPSD() <em>Service Id Some IPSD</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceId_SomeIPSD()
	 * @generated
	 * @ordered
	 */
	protected String serviceId_SomeIPSD = SERVICE_ID_SOME_IPSD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SomeIPSDFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSomeIPSDFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPSDEntryFlagsOrTemplatePlaceholderEnum getFlags() {
		return flags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFlags(SomeIPSDEntryFlagsOrTemplatePlaceholderEnum newFlags) {
		SomeIPSDEntryFlagsOrTemplatePlaceholderEnum oldFlags = flags;
		flags = newFlags == null ? FLAGS_EDEFAULT : newFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__FLAGS, oldFlags, flags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFlagsTmplParam() {
		return flagsTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFlagsTmplParam(String newFlagsTmplParam) {
		String oldFlagsTmplParam = flagsTmplParam;
		flagsTmplParam = newFlagsTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM, oldFlagsTmplParam, flagsTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPSDTypeOrTemplatePlaceholderEnum getSdType() {
		return sdType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSdType(SomeIPSDTypeOrTemplatePlaceholderEnum newSdType) {
		SomeIPSDTypeOrTemplatePlaceholderEnum oldSdType = sdType;
		sdType = newSdType == null ? SD_TYPE_EDEFAULT : newSdType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE, oldSdType, sdType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSdTypeTmplParam() {
		return sdTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSdTypeTmplParam(String newSdTypeTmplParam) {
		String oldSdTypeTmplParam = sdTypeTmplParam;
		sdTypeTmplParam = newSdTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM, oldSdTypeTmplParam, sdTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInstanceId() {
		return instanceId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInstanceId(String newInstanceId) {
		String oldInstanceId = instanceId;
		instanceId = newInstanceId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID, oldInstanceId, instanceId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TTLOrTemplatePlaceHolderEnum getTtl() {
		return ttl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTtl(TTLOrTemplatePlaceHolderEnum newTtl) {
		TTLOrTemplatePlaceHolderEnum oldTtl = ttl;
		ttl = newTtl == null ? TTL_EDEFAULT : newTtl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__TTL, oldTtl, ttl));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMajorVersion() {
		return majorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMajorVersion(String newMajorVersion) {
		String oldMajorVersion = majorVersion;
		majorVersion = newMajorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION, oldMajorVersion, majorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMinorVersion() {
		return minorVersion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMinorVersion(String newMinorVersion) {
		String oldMinorVersion = minorVersion;
		minorVersion = newMinorVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION, oldMinorVersion, minorVersion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEventGroupId() {
		return eventGroupId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEventGroupId(String newEventGroupId) {
		String oldEventGroupId = eventGroupId;
		eventGroupId = newEventGroupId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID, oldEventGroupId, eventGroupId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIndexFirstOption() {
		return indexFirstOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIndexFirstOption(String newIndexFirstOption) {
		String oldIndexFirstOption = indexFirstOption;
		indexFirstOption = newIndexFirstOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION, oldIndexFirstOption, indexFirstOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIndexSecondOption() {
		return indexSecondOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIndexSecondOption(String newIndexSecondOption) {
		String oldIndexSecondOption = indexSecondOption;
		indexSecondOption = newIndexSecondOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION, oldIndexSecondOption, indexSecondOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNumberFirstOption() {
		return numberFirstOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberFirstOption(String newNumberFirstOption) {
		String oldNumberFirstOption = numberFirstOption;
		numberFirstOption = newNumberFirstOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION, oldNumberFirstOption, numberFirstOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNumberSecondOption() {
		return numberSecondOption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumberSecondOption(String newNumberSecondOption) {
		String oldNumberSecondOption = numberSecondOption;
		numberSecondOption = newNumberSecondOption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION, oldNumberSecondOption, numberSecondOption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getServiceId_SomeIPSD() {
		return serviceId_SomeIPSD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setServiceId_SomeIPSD(String newServiceId_SomeIPSD) {
		String oldServiceId_SomeIPSD = serviceId_SomeIPSD;
		serviceId_SomeIPSD = newServiceId_SomeIPSD;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD, oldServiceId_SomeIPSD, serviceId_SomeIPSD));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInstanceId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INSTANCE_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInstanceId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidEventGroupId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_EVENT_GROUP_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidEventGroupId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidFlags(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_FLAGS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidFlags", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSdType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_SD_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSdType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMajorVersion(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_MAJOR_VERSION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMajorVersion", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMinorVersion(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_MINOR_VERSION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMinorVersion", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidIndexFirstOption(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INDEX_FIRST_OPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidIndexFirstOption", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidIndexSecondOption(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_INDEX_SECOND_OPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidIndexSecondOption", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidNumberFirstOption(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_NUMBER_FIRST_OPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidNumberFirstOption", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidNumberSecondOption(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.SOME_IPSD_FILTER__IS_VALID_HAS_VALID_NUMBER_SECOND_OPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidNumberSecondOption", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS:
				return getFlags();
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM:
				return getFlagsTmplParam();
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE:
				return getSdType();
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM:
				return getSdTypeTmplParam();
			case ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID:
				return getInstanceId();
			case ConditionsPackage.SOME_IPSD_FILTER__TTL:
				return getTtl();
			case ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION:
				return getMajorVersion();
			case ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION:
				return getMinorVersion();
			case ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID:
				return getEventGroupId();
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION:
				return getIndexFirstOption();
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION:
				return getIndexSecondOption();
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION:
				return getNumberFirstOption();
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION:
				return getNumberSecondOption();
			case ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD:
				return getServiceId_SomeIPSD();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS:
				setFlags((SomeIPSDEntryFlagsOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM:
				setFlagsTmplParam((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE:
				setSdType((SomeIPSDTypeOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM:
				setSdTypeTmplParam((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID:
				setInstanceId((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__TTL:
				setTtl((TTLOrTemplatePlaceHolderEnum)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION:
				setMajorVersion((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION:
				setMinorVersion((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID:
				setEventGroupId((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION:
				setIndexFirstOption((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION:
				setIndexSecondOption((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION:
				setNumberFirstOption((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION:
				setNumberSecondOption((String)newValue);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD:
				setServiceId_SomeIPSD((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS:
				setFlags(FLAGS_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM:
				setFlagsTmplParam(FLAGS_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE:
				setSdType(SD_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM:
				setSdTypeTmplParam(SD_TYPE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID:
				setInstanceId(INSTANCE_ID_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__TTL:
				setTtl(TTL_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION:
				setMajorVersion(MAJOR_VERSION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION:
				setMinorVersion(MINOR_VERSION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID:
				setEventGroupId(EVENT_GROUP_ID_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION:
				setIndexFirstOption(INDEX_FIRST_OPTION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION:
				setIndexSecondOption(INDEX_SECOND_OPTION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION:
				setNumberFirstOption(NUMBER_FIRST_OPTION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION:
				setNumberSecondOption(NUMBER_SECOND_OPTION_EDEFAULT);
				return;
			case ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD:
				setServiceId_SomeIPSD(SERVICE_ID_SOME_IPSD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS:
				return flags != FLAGS_EDEFAULT;
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM:
				return FLAGS_TMPL_PARAM_EDEFAULT == null ? flagsTmplParam != null : !FLAGS_TMPL_PARAM_EDEFAULT.equals(flagsTmplParam);
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE:
				return sdType != SD_TYPE_EDEFAULT;
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM:
				return SD_TYPE_TMPL_PARAM_EDEFAULT == null ? sdTypeTmplParam != null : !SD_TYPE_TMPL_PARAM_EDEFAULT.equals(sdTypeTmplParam);
			case ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID:
				return INSTANCE_ID_EDEFAULT == null ? instanceId != null : !INSTANCE_ID_EDEFAULT.equals(instanceId);
			case ConditionsPackage.SOME_IPSD_FILTER__TTL:
				return ttl != TTL_EDEFAULT;
			case ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION:
				return MAJOR_VERSION_EDEFAULT == null ? majorVersion != null : !MAJOR_VERSION_EDEFAULT.equals(majorVersion);
			case ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION:
				return MINOR_VERSION_EDEFAULT == null ? minorVersion != null : !MINOR_VERSION_EDEFAULT.equals(minorVersion);
			case ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID:
				return EVENT_GROUP_ID_EDEFAULT == null ? eventGroupId != null : !EVENT_GROUP_ID_EDEFAULT.equals(eventGroupId);
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION:
				return INDEX_FIRST_OPTION_EDEFAULT == null ? indexFirstOption != null : !INDEX_FIRST_OPTION_EDEFAULT.equals(indexFirstOption);
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION:
				return INDEX_SECOND_OPTION_EDEFAULT == null ? indexSecondOption != null : !INDEX_SECOND_OPTION_EDEFAULT.equals(indexSecondOption);
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION:
				return NUMBER_FIRST_OPTION_EDEFAULT == null ? numberFirstOption != null : !NUMBER_FIRST_OPTION_EDEFAULT.equals(numberFirstOption);
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION:
				return NUMBER_SECOND_OPTION_EDEFAULT == null ? numberSecondOption != null : !NUMBER_SECOND_OPTION_EDEFAULT.equals(numberSecondOption);
			case ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD:
				return SERVICE_ID_SOME_IPSD_EDEFAULT == null ? serviceId_SomeIPSD != null : !SERVICE_ID_SOME_IPSD_EDEFAULT.equals(serviceId_SomeIPSD);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INSTANCE_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInstanceId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_EVENT_GROUP_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidEventGroupId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_FLAGS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidFlags((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_SD_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSdType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_MAJOR_VERSION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMajorVersion((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_MINOR_VERSION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMinorVersion((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INDEX_FIRST_OPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidIndexFirstOption((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INDEX_SECOND_OPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidIndexSecondOption((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_NUMBER_FIRST_OPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidNumberFirstOption((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.SOME_IPSD_FILTER___IS_VALID_HAS_VALID_NUMBER_SECOND_OPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidNumberSecondOption((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (flags: ");
		result.append(flags);
		result.append(", flagsTmplParam: ");
		result.append(flagsTmplParam);
		result.append(", sdType: ");
		result.append(sdType);
		result.append(", sdTypeTmplParam: ");
		result.append(sdTypeTmplParam);
		result.append(", instanceId: ");
		result.append(instanceId);
		result.append(", ttl: ");
		result.append(ttl);
		result.append(", majorVersion: ");
		result.append(majorVersion);
		result.append(", minorVersion: ");
		result.append(minorVersion);
		result.append(", eventGroupId: ");
		result.append(eventGroupId);
		result.append(", indexFirstOption: ");
		result.append(indexFirstOption);
		result.append(", indexSecondOption: ");
		result.append(indexSecondOption);
		result.append(", numberFirstOption: ");
		result.append(numberFirstOption);
		result.append(", numberSecondOption: ");
		result.append(numberSecondOption);
		result.append(", serviceId_SomeIPSD: ");
		result.append(serviceId_SomeIPSD);
		result.append(')');
		return result.toString();
	}

} //SomeIPSDFilterImpl
