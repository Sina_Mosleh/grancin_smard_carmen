/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.IStringOperand;
import conditions.Matches;
import conditions.RegexOperation;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matches</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.MatchesImpl#getRegex <em>Regex</em>}</li>
 *   <li>{@link conditions.impl.MatchesImpl#getDynamicRegex <em>Dynamic Regex</em>}</li>
 *   <li>{@link conditions.impl.MatchesImpl#getStringToCheck <em>String To Check</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchesImpl extends StringExpressionImpl implements Matches {
	/**
	 * The default value of the '{@link #getRegex() <em>Regex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegex()
	 * @generated
	 * @ordered
	 */
	protected static final String REGEX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRegex() <em>Regex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRegex()
	 * @generated
	 * @ordered
	 */
	protected String regex = REGEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDynamicRegex() <em>Dynamic Regex</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDynamicRegex()
	 * @generated
	 * @ordered
	 */
	protected IStringOperand dynamicRegex;

	/**
	 * The cached value of the '{@link #getStringToCheck() <em>String To Check</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToCheck()
	 * @generated
	 * @ordered
	 */
	protected IStringOperand stringToCheck;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getMatches();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRegex() {
		return regex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRegex(String newRegex) {
		String oldRegex = regex;
		regex = newRegex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__REGEX, oldRegex, regex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IStringOperand getDynamicRegex() {
		return dynamicRegex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDynamicRegex(IStringOperand newDynamicRegex, NotificationChain msgs) {
		IStringOperand oldDynamicRegex = dynamicRegex;
		dynamicRegex = newDynamicRegex;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__DYNAMIC_REGEX, oldDynamicRegex, newDynamicRegex);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDynamicRegex(IStringOperand newDynamicRegex) {
		if (newDynamicRegex != dynamicRegex) {
			NotificationChain msgs = null;
			if (dynamicRegex != null)
				msgs = ((InternalEObject)dynamicRegex).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__DYNAMIC_REGEX, null, msgs);
			if (newDynamicRegex != null)
				msgs = ((InternalEObject)newDynamicRegex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__DYNAMIC_REGEX, null, msgs);
			msgs = basicSetDynamicRegex(newDynamicRegex, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__DYNAMIC_REGEX, newDynamicRegex, newDynamicRegex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IStringOperand getStringToCheck() {
		return stringToCheck;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringToCheck(IStringOperand newStringToCheck, NotificationChain msgs) {
		IStringOperand oldStringToCheck = stringToCheck;
		stringToCheck = newStringToCheck;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__STRING_TO_CHECK, oldStringToCheck, newStringToCheck);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringToCheck(IStringOperand newStringToCheck) {
		if (newStringToCheck != stringToCheck) {
			NotificationChain msgs = null;
			if (stringToCheck != null)
				msgs = ((InternalEObject)stringToCheck).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__STRING_TO_CHECK, null, msgs);
			if (newStringToCheck != null)
				msgs = ((InternalEObject)newStringToCheck).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__STRING_TO_CHECK, null, msgs);
			msgs = basicSetStringToCheck(newStringToCheck, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__STRING_TO_CHECK, newStringToCheck, newStringToCheck));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStringToCheck(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.MATCHES__IS_VALID_HAS_VALID_STRING_TO_CHECK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStringToCheck", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.REGEX_OPERATION__IS_VALID_HAS_VALID_REGEX,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidRegex", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
				return basicSetDynamicRegex(null, msgs);
			case ConditionsPackage.MATCHES__STRING_TO_CHECK:
				return basicSetStringToCheck(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.MATCHES__REGEX:
				return getRegex();
			case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
				return getDynamicRegex();
			case ConditionsPackage.MATCHES__STRING_TO_CHECK:
				return getStringToCheck();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.MATCHES__REGEX:
				setRegex((String)newValue);
				return;
			case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
				setDynamicRegex((IStringOperand)newValue);
				return;
			case ConditionsPackage.MATCHES__STRING_TO_CHECK:
				setStringToCheck((IStringOperand)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.MATCHES__REGEX:
				setRegex(REGEX_EDEFAULT);
				return;
			case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
				setDynamicRegex((IStringOperand)null);
				return;
			case ConditionsPackage.MATCHES__STRING_TO_CHECK:
				setStringToCheck((IStringOperand)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.MATCHES__REGEX:
				return REGEX_EDEFAULT == null ? regex != null : !REGEX_EDEFAULT.equals(regex);
			case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
				return dynamicRegex != null;
			case ConditionsPackage.MATCHES__STRING_TO_CHECK:
				return stringToCheck != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RegexOperation.class) {
			switch (derivedFeatureID) {
				case ConditionsPackage.MATCHES__REGEX: return ConditionsPackage.REGEX_OPERATION__REGEX;
				case ConditionsPackage.MATCHES__DYNAMIC_REGEX: return ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RegexOperation.class) {
			switch (baseFeatureID) {
				case ConditionsPackage.REGEX_OPERATION__REGEX: return ConditionsPackage.MATCHES__REGEX;
				case ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX: return ConditionsPackage.MATCHES__DYNAMIC_REGEX;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == RegexOperation.class) {
			switch (baseOperationID) {
				case ConditionsPackage.REGEX_OPERATION___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP: return ConditionsPackage.MATCHES___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.MATCHES___IS_VALID_HAS_VALID_STRING_TO_CHECK__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStringToCheck((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.MATCHES___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidRegex((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (regex: ");
		result.append(regex);
		result.append(')');
		return result.toString();
	}

} //MatchesImpl
