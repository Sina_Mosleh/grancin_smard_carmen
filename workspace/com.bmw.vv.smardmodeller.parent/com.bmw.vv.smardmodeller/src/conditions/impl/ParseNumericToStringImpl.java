/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.INumericOperand;
import conditions.IOperand;
import conditions.ParseNumericToString;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parse Numeric To String</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ParseNumericToStringImpl#getNumericOperandToBeParsed <em>Numeric Operand To Be Parsed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParseNumericToStringImpl extends MinimalEObjectImpl.Container implements ParseNumericToString {
	/**
	 * The cached value of the '{@link #getNumericOperandToBeParsed() <em>Numeric Operand To Be Parsed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumericOperandToBeParsed()
	 * @generated
	 * @ordered
	 */
	protected INumericOperand numericOperandToBeParsed;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParseNumericToStringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getParseNumericToString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public INumericOperand getNumericOperandToBeParsed() {
		return numericOperandToBeParsed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNumericOperandToBeParsed(INumericOperand newNumericOperandToBeParsed, NotificationChain msgs) {
		INumericOperand oldNumericOperandToBeParsed = numericOperandToBeParsed;
		numericOperandToBeParsed = newNumericOperandToBeParsed;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, oldNumericOperandToBeParsed, newNumericOperandToBeParsed);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNumericOperandToBeParsed(INumericOperand newNumericOperandToBeParsed) {
		if (newNumericOperandToBeParsed != numericOperandToBeParsed) {
			NotificationChain msgs = null;
			if (numericOperandToBeParsed != null)
				msgs = ((InternalEObject)numericOperandToBeParsed).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, null, msgs);
			if (newNumericOperandToBeParsed != null)
				msgs = ((InternalEObject)newNumericOperandToBeParsed).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, null, msgs);
			msgs = basicSetNumericOperandToBeParsed(newNumericOperandToBeParsed, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, newNumericOperandToBeParsed, newNumericOperandToBeParsed));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
				return basicSetNumericOperandToBeParsed(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
				return getNumericOperandToBeParsed();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
				setNumericOperandToBeParsed((INumericOperand)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
				setNumericOperandToBeParsed((INumericOperand)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
				return numericOperandToBeParsed != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING___GET_OPERANDS:
				return get_Operands();
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING___GET_READ_VARIABLES:
				return getReadVariables();
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ParseNumericToStringImpl
