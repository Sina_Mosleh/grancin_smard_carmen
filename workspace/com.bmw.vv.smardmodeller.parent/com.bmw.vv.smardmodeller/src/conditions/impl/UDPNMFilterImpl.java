/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.UDPNMFilter;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UDPNM Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.UDPNMFilterImpl#getSourceNodeIdentifier <em>Source Node Identifier</em>}</li>
 *   <li>{@link conditions.impl.UDPNMFilterImpl#getControlBitVector <em>Control Bit Vector</em>}</li>
 *   <li>{@link conditions.impl.UDPNMFilterImpl#getPwfStatus <em>Pwf Status</em>}</li>
 *   <li>{@link conditions.impl.UDPNMFilterImpl#getTeilnetzStatus <em>Teilnetz Status</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UDPNMFilterImpl extends AbstractFilterImpl implements UDPNMFilter {
	/**
	 * The default value of the '{@link #getSourceNodeIdentifier() <em>Source Node Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceNodeIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_NODE_IDENTIFIER_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getSourceNodeIdentifier() <em>Source Node Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceNodeIdentifier()
	 * @generated
	 * @ordered
	 */
	protected String sourceNodeIdentifier = SOURCE_NODE_IDENTIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getControlBitVector() <em>Control Bit Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlBitVector()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTROL_BIT_VECTOR_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getControlBitVector() <em>Control Bit Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlBitVector()
	 * @generated
	 * @ordered
	 */
	protected String controlBitVector = CONTROL_BIT_VECTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getPwfStatus() <em>Pwf Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPwfStatus()
	 * @generated
	 * @ordered
	 */
	protected static final String PWF_STATUS_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getPwfStatus() <em>Pwf Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPwfStatus()
	 * @generated
	 * @ordered
	 */
	protected String pwfStatus = PWF_STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTeilnetzStatus() <em>Teilnetz Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTeilnetzStatus()
	 * @generated
	 * @ordered
	 */
	protected static final String TEILNETZ_STATUS_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getTeilnetzStatus() <em>Teilnetz Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTeilnetzStatus()
	 * @generated
	 * @ordered
	 */
	protected String teilnetzStatus = TEILNETZ_STATUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UDPNMFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getUDPNMFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceNodeIdentifier() {
		return sourceNodeIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceNodeIdentifier(String newSourceNodeIdentifier) {
		String oldSourceNodeIdentifier = sourceNodeIdentifier;
		sourceNodeIdentifier = newSourceNodeIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_FILTER__SOURCE_NODE_IDENTIFIER, oldSourceNodeIdentifier, sourceNodeIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getControlBitVector() {
		return controlBitVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setControlBitVector(String newControlBitVector) {
		String oldControlBitVector = controlBitVector;
		controlBitVector = newControlBitVector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_FILTER__CONTROL_BIT_VECTOR, oldControlBitVector, controlBitVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPwfStatus() {
		return pwfStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPwfStatus(String newPwfStatus) {
		String oldPwfStatus = pwfStatus;
		pwfStatus = newPwfStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_FILTER__PWF_STATUS, oldPwfStatus, pwfStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTeilnetzStatus() {
		return teilnetzStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTeilnetzStatus(String newTeilnetzStatus) {
		String oldTeilnetzStatus = teilnetzStatus;
		teilnetzStatus = newTeilnetzStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_FILTER__TEILNETZ_STATUS, oldTeilnetzStatus, teilnetzStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSourceNodeIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UDPNM_FILTER__IS_VALID_HAS_VALID_SOURCE_NODE_IDENTIFIER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSourceNodeIdentifier", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidControlBitVector(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UDPNM_FILTER__IS_VALID_HAS_VALID_CONTROL_BIT_VECTOR,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidControlBitVector", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidPwfStatus(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UDPNM_FILTER__IS_VALID_HAS_VALID_PWF_STATUS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidPwfStatus", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidTeilnetzStatus(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UDPNM_FILTER__IS_VALID_HAS_VALID_TEILNETZ_STATUS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidTeilnetzStatus", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_FILTER__SOURCE_NODE_IDENTIFIER:
				return getSourceNodeIdentifier();
			case ConditionsPackage.UDPNM_FILTER__CONTROL_BIT_VECTOR:
				return getControlBitVector();
			case ConditionsPackage.UDPNM_FILTER__PWF_STATUS:
				return getPwfStatus();
			case ConditionsPackage.UDPNM_FILTER__TEILNETZ_STATUS:
				return getTeilnetzStatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_FILTER__SOURCE_NODE_IDENTIFIER:
				setSourceNodeIdentifier((String)newValue);
				return;
			case ConditionsPackage.UDPNM_FILTER__CONTROL_BIT_VECTOR:
				setControlBitVector((String)newValue);
				return;
			case ConditionsPackage.UDPNM_FILTER__PWF_STATUS:
				setPwfStatus((String)newValue);
				return;
			case ConditionsPackage.UDPNM_FILTER__TEILNETZ_STATUS:
				setTeilnetzStatus((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_FILTER__SOURCE_NODE_IDENTIFIER:
				setSourceNodeIdentifier(SOURCE_NODE_IDENTIFIER_EDEFAULT);
				return;
			case ConditionsPackage.UDPNM_FILTER__CONTROL_BIT_VECTOR:
				setControlBitVector(CONTROL_BIT_VECTOR_EDEFAULT);
				return;
			case ConditionsPackage.UDPNM_FILTER__PWF_STATUS:
				setPwfStatus(PWF_STATUS_EDEFAULT);
				return;
			case ConditionsPackage.UDPNM_FILTER__TEILNETZ_STATUS:
				setTeilnetzStatus(TEILNETZ_STATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_FILTER__SOURCE_NODE_IDENTIFIER:
				return SOURCE_NODE_IDENTIFIER_EDEFAULT == null ? sourceNodeIdentifier != null : !SOURCE_NODE_IDENTIFIER_EDEFAULT.equals(sourceNodeIdentifier);
			case ConditionsPackage.UDPNM_FILTER__CONTROL_BIT_VECTOR:
				return CONTROL_BIT_VECTOR_EDEFAULT == null ? controlBitVector != null : !CONTROL_BIT_VECTOR_EDEFAULT.equals(controlBitVector);
			case ConditionsPackage.UDPNM_FILTER__PWF_STATUS:
				return PWF_STATUS_EDEFAULT == null ? pwfStatus != null : !PWF_STATUS_EDEFAULT.equals(pwfStatus);
			case ConditionsPackage.UDPNM_FILTER__TEILNETZ_STATUS:
				return TEILNETZ_STATUS_EDEFAULT == null ? teilnetzStatus != null : !TEILNETZ_STATUS_EDEFAULT.equals(teilnetzStatus);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.UDPNM_FILTER___IS_VALID_HAS_VALID_SOURCE_NODE_IDENTIFIER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSourceNodeIdentifier((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.UDPNM_FILTER___IS_VALID_HAS_VALID_CONTROL_BIT_VECTOR__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidControlBitVector((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.UDPNM_FILTER___IS_VALID_HAS_VALID_PWF_STATUS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidPwfStatus((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.UDPNM_FILTER___IS_VALID_HAS_VALID_TEILNETZ_STATUS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidTeilnetzStatus((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sourceNodeIdentifier: ");
		result.append(sourceNodeIdentifier);
		result.append(", controlBitVector: ");
		result.append(controlBitVector);
		result.append(", pwfStatus: ");
		result.append(pwfStatus);
		result.append(", teilnetzStatus: ");
		result.append(teilnetzStatus);
		result.append(')');
		return result.toString();
	}

} //UDPNMFilterImpl
