/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.SourceReference;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SourceReferenceImpl#getSerializedReference <em>Serialized Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SourceReferenceImpl extends MinimalEObjectImpl.Container implements SourceReference {
	/**
	 * The default value of the '{@link #getSerializedReference() <em>Serialized Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedReference()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIALIZED_REFERENCE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getSerializedReference() <em>Serialized Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSerializedReference()
	 * @generated
	 * @ordered
	 */
	protected String serializedReference = SERIALIZED_REFERENCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SourceReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSourceReference();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSerializedReference() {
		return serializedReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSerializedReference(String newSerializedReference) {
		String oldSerializedReference = serializedReference;
		serializedReference = newSerializedReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOURCE_REFERENCE__SERIALIZED_REFERENCE, oldSerializedReference, serializedReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SOURCE_REFERENCE__SERIALIZED_REFERENCE:
				return getSerializedReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SOURCE_REFERENCE__SERIALIZED_REFERENCE:
				setSerializedReference((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOURCE_REFERENCE__SERIALIZED_REFERENCE:
				setSerializedReference(SERIALIZED_REFERENCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOURCE_REFERENCE__SERIALIZED_REFERENCE:
				return SERIALIZED_REFERENCE_EDEFAULT == null ? serializedReference != null : !SERIALIZED_REFERENCE_EDEFAULT.equals(serializedReference);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (serializedReference: ");
		result.append(serializedReference);
		result.append(')');
		return result.toString();
	}

} //SourceReferenceImpl
