/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.UniversalPayloadWithLegacyExtractStrategy;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Universal Payload With Legacy Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl#getStartBit <em>Start Bit</em>}</li>
 *   <li>{@link conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl#getLength <em>Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversalPayloadWithLegacyExtractStrategyImpl extends UniversalPayloadExtractStrategyImpl implements UniversalPayloadWithLegacyExtractStrategy {
	/**
	 * The default value of the '{@link #getStartBit() <em>Start Bit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartBit()
	 * @generated
	 * @ordered
	 */
	protected static final String START_BIT_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getStartBit() <em>Start Bit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartBit()
	 * @generated
	 * @ordered
	 */
	protected String startBit = START_BIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected static final String LENGTH_EDEFAULT = "1";

	/**
	 * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected String length = LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversalPayloadWithLegacyExtractStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getUniversalPayloadWithLegacyExtractStrategy();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStartBit() {
		return startBit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartBit(String newStartBit) {
		String oldStartBit = startBit;
		startBit = newStartBit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT, oldStartBit, startBit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLength() {
		return length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLength(String newLength) {
		String oldLength = length;
		length = newLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH, oldLength, length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStartbit(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STARTBIT,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStartbit", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDataLength(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_DATA_LENGTH,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDataLength", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
				return getStartBit();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
				return getLength();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
				setStartBit((String)newValue);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
				setLength((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
				setStartBit(START_BIT_EDEFAULT);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
				setLength(LENGTH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
				return START_BIT_EDEFAULT == null ? startBit != null : !START_BIT_EDEFAULT.equals(startBit);
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
				return LENGTH_EDEFAULT == null ? length != null : !LENGTH_EDEFAULT.equals(length);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STARTBIT__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStartbit((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_DATA_LENGTH__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDataLength((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startBit: ");
		result.append(startBit);
		result.append(", length: ");
		result.append(length);
		result.append(')');
		return result.toString();
	}

} //UniversalPayloadWithLegacyExtractStrategyImpl
