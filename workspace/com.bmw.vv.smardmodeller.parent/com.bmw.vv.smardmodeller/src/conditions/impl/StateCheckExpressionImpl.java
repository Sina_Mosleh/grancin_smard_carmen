/**
 */
package conditions.impl;

import conditions.BooleanOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.StateCheckExpression;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.State;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.StateCheckExpressionImpl#getCheckStateActive <em>Check State Active</em>}</li>
 *   <li>{@link conditions.impl.StateCheckExpressionImpl#getCheckState <em>Check State</em>}</li>
 *   <li>{@link conditions.impl.StateCheckExpressionImpl#getCheckStateActiveTmplParam <em>Check State Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateCheckExpressionImpl extends ExpressionImpl implements StateCheckExpression {
	/**
	 * The default value of the '{@link #getCheckStateActive() <em>Check State Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckStateActive()
	 * @generated
	 * @ordered
	 */
	protected static final BooleanOrTemplatePlaceholderEnum CHECK_STATE_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

	/**
	 * The cached value of the '{@link #getCheckStateActive() <em>Check State Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckStateActive()
	 * @generated
	 * @ordered
	 */
	protected BooleanOrTemplatePlaceholderEnum checkStateActive = CHECK_STATE_ACTIVE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCheckState() <em>Check State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckState()
	 * @generated
	 * @ordered
	 */
	protected State checkState;

	/**
	 * The default value of the '{@link #getCheckStateActiveTmplParam() <em>Check State Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckStateActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCheckStateActiveTmplParam() <em>Check State Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckStateActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String checkStateActiveTmplParam = CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateCheckExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getStateCheckExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanOrTemplatePlaceholderEnum getCheckStateActive() {
		return checkStateActive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCheckStateActive(BooleanOrTemplatePlaceholderEnum newCheckStateActive) {
		BooleanOrTemplatePlaceholderEnum oldCheckStateActive = checkStateActive;
		checkStateActive = newCheckStateActive == null ? CHECK_STATE_ACTIVE_EDEFAULT : newCheckStateActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE, oldCheckStateActive, checkStateActive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public State getCheckState() {
		if (checkState != null && checkState.eIsProxy()) {
			InternalEObject oldCheckState = (InternalEObject)checkState;
			checkState = (State)eResolveProxy(oldCheckState);
			if (checkState != oldCheckState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE, oldCheckState, checkState));
			}
		}
		return checkState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetCheckState() {
		return checkState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCheckState(State newCheckState) {
		State oldCheckState = checkState;
		checkState = newCheckState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE, oldCheckState, checkState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCheckStateActiveTmplParam() {
		return checkStateActiveTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCheckStateActiveTmplParam(String newCheckStateActiveTmplParam) {
		String oldCheckStateActiveTmplParam = checkStateActiveTmplParam;
		checkStateActiveTmplParam = newCheckStateActiveTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM, oldCheckStateActiveTmplParam, checkStateActiveTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATES_ACTIVE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStatesActive", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidState", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
				return getCheckStateActive();
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
				if (resolve) return getCheckState();
				return basicGetCheckState();
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
				return getCheckStateActiveTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
				setCheckStateActive((BooleanOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
				setCheckState((State)newValue);
				return;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
				setCheckStateActiveTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
				setCheckStateActive(CHECK_STATE_ACTIVE_EDEFAULT);
				return;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
				setCheckState((State)null);
				return;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
				setCheckStateActiveTmplParam(CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
				return checkStateActive != CHECK_STATE_ACTIVE_EDEFAULT;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
				return checkState != null;
			case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
				return CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT == null ? checkStateActiveTmplParam != null : !CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT.equals(checkStateActiveTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.STATE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STATES_ACTIVE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStatesActive((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.STATE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STATE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidState((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (checkStateActive: ");
		result.append(checkStateActive);
		result.append(", checkStateActiveTmplParam: ");
		result.append(checkStateActiveTmplParam);
		result.append(')');
		return result.toString();
	}

} //StateCheckExpressionImpl
