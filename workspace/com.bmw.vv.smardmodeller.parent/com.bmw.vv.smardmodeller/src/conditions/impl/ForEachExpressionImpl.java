/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.ContainerSignal;
import conditions.Expression;
import conditions.ForEachExpression;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Each Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ForEachExpressionImpl#getFilterExpressions <em>Filter Expressions</em>}</li>
 *   <li>{@link conditions.impl.ForEachExpressionImpl#getContainerSignal <em>Container Signal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForEachExpressionImpl extends ExpressionImpl implements ForEachExpression {
	/**
	 * The cached value of the '{@link #getFilterExpressions() <em>Filter Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> filterExpressions;

	/**
	 * The cached value of the '{@link #getContainerSignal() <em>Container Signal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainerSignal()
	 * @generated
	 * @ordered
	 */
	protected ContainerSignal containerSignal;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForEachExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getForEachExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Expression> getFilterExpressions() {
		if (filterExpressions == null) {
			filterExpressions = new EObjectContainmentEList<Expression>(Expression.class, this, ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS);
		}
		return filterExpressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ContainerSignal getContainerSignal() {
		if (containerSignal != null && containerSignal.eIsProxy()) {
			InternalEObject oldContainerSignal = (InternalEObject)containerSignal;
			containerSignal = (ContainerSignal)eResolveProxy(oldContainerSignal);
			if (containerSignal != oldContainerSignal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL, oldContainerSignal, containerSignal));
			}
		}
		return containerSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainerSignal basicGetContainerSignal() {
		return containerSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setContainerSignal(ContainerSignal newContainerSignal) {
		ContainerSignal oldContainerSignal = containerSignal;
		containerSignal = newContainerSignal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL, oldContainerSignal, containerSignal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS:
				return ((InternalEList<?>)getFilterExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS:
				return getFilterExpressions();
			case ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL:
				if (resolve) return getContainerSignal();
				return basicGetContainerSignal();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS:
				getFilterExpressions().clear();
				getFilterExpressions().addAll((Collection<? extends Expression>)newValue);
				return;
			case ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL:
				setContainerSignal((ContainerSignal)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS:
				getFilterExpressions().clear();
				return;
			case ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL:
				setContainerSignal((ContainerSignal)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS:
				return filterExpressions != null && !filterExpressions.isEmpty();
			case ConditionsPackage.FOR_EACH_EXPRESSION__CONTAINER_SIGNAL:
				return containerSignal != null;
		}
		return super.eIsSet(featureID);
	}

} //ForEachExpressionImpl
