/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DLTFilter;
import conditions.DLT_MessageBusInfo;
import conditions.DLT_MessageControlInfo;
import conditions.DLT_MessageLogInfo;
import conditions.DLT_MessageTraceInfo;
import conditions.DLT_MessageType;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.DLTFilterImpl#getEcuID_ECU <em>Ecu ID ECU</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getSessionID_SEID <em>Session ID SEID</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getApplicationID_APID <em>Application ID APID</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getContextID_CTID <em>Context ID CTID</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageType_MSTP <em>Message Type MSTP</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageLogInfo_MSLI <em>Message Log Info MSLI</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageTraceInfo_MSTI <em>Message Trace Info MSTI</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageBusInfo_MSBI <em>Message Bus Info MSBI</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageControlInfo_MSCI <em>Message Control Info MSCI</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageLogInfo_MSLITmplParam <em>Message Log Info MSLI Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageTraceInfo_MSTITmplParam <em>Message Trace Info MSTI Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageBusInfo_MSBITmplParam <em>Message Bus Info MSBI Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageControlInfo_MSCITmplParam <em>Message Control Info MSCI Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.DLTFilterImpl#getMessageType_MSTPTmplParam <em>Message Type MSTP Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DLTFilterImpl extends AbstractFilterImpl implements DLTFilter {
	/**
	 * The default value of the '{@link #getEcuID_ECU() <em>Ecu ID ECU</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEcuID_ECU()
	 * @generated
	 * @ordered
	 */
	protected static final String ECU_ID_ECU_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEcuID_ECU() <em>Ecu ID ECU</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEcuID_ECU()
	 * @generated
	 * @ordered
	 */
	protected String ecuID_ECU = ECU_ID_ECU_EDEFAULT;

	/**
	 * The default value of the '{@link #getSessionID_SEID() <em>Session ID SEID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessionID_SEID()
	 * @generated
	 * @ordered
	 */
	protected static final String SESSION_ID_SEID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSessionID_SEID() <em>Session ID SEID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessionID_SEID()
	 * @generated
	 * @ordered
	 */
	protected String sessionID_SEID = SESSION_ID_SEID_EDEFAULT;

	/**
	 * The default value of the '{@link #getApplicationID_APID() <em>Application ID APID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationID_APID()
	 * @generated
	 * @ordered
	 */
	protected static final String APPLICATION_ID_APID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getApplicationID_APID() <em>Application ID APID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicationID_APID()
	 * @generated
	 * @ordered
	 */
	protected String applicationID_APID = APPLICATION_ID_APID_EDEFAULT;

	/**
	 * The default value of the '{@link #getContextID_CTID() <em>Context ID CTID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextID_CTID()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTEXT_ID_CTID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContextID_CTID() <em>Context ID CTID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextID_CTID()
	 * @generated
	 * @ordered
	 */
	protected String contextID_CTID = CONTEXT_ID_CTID_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageType_MSTP() <em>Message Type MSTP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageType_MSTP()
	 * @generated
	 * @ordered
	 */
	protected static final DLT_MessageType MESSAGE_TYPE_MSTP_EDEFAULT = DLT_MessageType.NOT_SPECIFIED;

	/**
	 * The cached value of the '{@link #getMessageType_MSTP() <em>Message Type MSTP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageType_MSTP()
	 * @generated
	 * @ordered
	 */
	protected DLT_MessageType messageType_MSTP = MESSAGE_TYPE_MSTP_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageLogInfo_MSLI() <em>Message Log Info MSLI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageLogInfo_MSLI()
	 * @generated
	 * @ordered
	 */
	protected static final DLT_MessageLogInfo MESSAGE_LOG_INFO_MSLI_EDEFAULT = DLT_MessageLogInfo.NOT_SPECIFIED;

	/**
	 * The cached value of the '{@link #getMessageLogInfo_MSLI() <em>Message Log Info MSLI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageLogInfo_MSLI()
	 * @generated
	 * @ordered
	 */
	protected DLT_MessageLogInfo messageLogInfo_MSLI = MESSAGE_LOG_INFO_MSLI_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageTraceInfo_MSTI() <em>Message Trace Info MSTI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageTraceInfo_MSTI()
	 * @generated
	 * @ordered
	 */
	protected static final DLT_MessageTraceInfo MESSAGE_TRACE_INFO_MSTI_EDEFAULT = DLT_MessageTraceInfo.NOT_SPECIFIED;

	/**
	 * The cached value of the '{@link #getMessageTraceInfo_MSTI() <em>Message Trace Info MSTI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageTraceInfo_MSTI()
	 * @generated
	 * @ordered
	 */
	protected DLT_MessageTraceInfo messageTraceInfo_MSTI = MESSAGE_TRACE_INFO_MSTI_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageBusInfo_MSBI() <em>Message Bus Info MSBI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBusInfo_MSBI()
	 * @generated
	 * @ordered
	 */
	protected static final DLT_MessageBusInfo MESSAGE_BUS_INFO_MSBI_EDEFAULT = DLT_MessageBusInfo.NOT_SPECIFIED;

	/**
	 * The cached value of the '{@link #getMessageBusInfo_MSBI() <em>Message Bus Info MSBI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBusInfo_MSBI()
	 * @generated
	 * @ordered
	 */
	protected DLT_MessageBusInfo messageBusInfo_MSBI = MESSAGE_BUS_INFO_MSBI_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageControlInfo_MSCI() <em>Message Control Info MSCI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageControlInfo_MSCI()
	 * @generated
	 * @ordered
	 */
	protected static final DLT_MessageControlInfo MESSAGE_CONTROL_INFO_MSCI_EDEFAULT = DLT_MessageControlInfo.NOT_SPECIFIED;

	/**
	 * The cached value of the '{@link #getMessageControlInfo_MSCI() <em>Message Control Info MSCI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageControlInfo_MSCI()
	 * @generated
	 * @ordered
	 */
	protected DLT_MessageControlInfo messageControlInfo_MSCI = MESSAGE_CONTROL_INFO_MSCI_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageLogInfo_MSLITmplParam() <em>Message Log Info MSLI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageLogInfo_MSLITmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_LOG_INFO_MSLI_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageLogInfo_MSLITmplParam() <em>Message Log Info MSLI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageLogInfo_MSLITmplParam()
	 * @generated
	 * @ordered
	 */
	protected String messageLogInfo_MSLITmplParam = MESSAGE_LOG_INFO_MSLI_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageTraceInfo_MSTITmplParam() <em>Message Trace Info MSTI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageTraceInfo_MSTITmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageTraceInfo_MSTITmplParam() <em>Message Trace Info MSTI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageTraceInfo_MSTITmplParam()
	 * @generated
	 * @ordered
	 */
	protected String messageTraceInfo_MSTITmplParam = MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageBusInfo_MSBITmplParam() <em>Message Bus Info MSBI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBusInfo_MSBITmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_BUS_INFO_MSBI_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageBusInfo_MSBITmplParam() <em>Message Bus Info MSBI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageBusInfo_MSBITmplParam()
	 * @generated
	 * @ordered
	 */
	protected String messageBusInfo_MSBITmplParam = MESSAGE_BUS_INFO_MSBI_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageControlInfo_MSCITmplParam() <em>Message Control Info MSCI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageControlInfo_MSCITmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageControlInfo_MSCITmplParam() <em>Message Control Info MSCI Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageControlInfo_MSCITmplParam()
	 * @generated
	 * @ordered
	 */
	protected String messageControlInfo_MSCITmplParam = MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageType_MSTPTmplParam() <em>Message Type MSTP Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageType_MSTPTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_TYPE_MSTP_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageType_MSTPTmplParam() <em>Message Type MSTP Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageType_MSTPTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String messageType_MSTPTmplParam = MESSAGE_TYPE_MSTP_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DLTFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getDLTFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEcuID_ECU() {
		return ecuID_ECU;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEcuID_ECU(String newEcuID_ECU) {
		String oldEcuID_ECU = ecuID_ECU;
		ecuID_ECU = newEcuID_ECU;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__ECU_ID_ECU, oldEcuID_ECU, ecuID_ECU));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSessionID_SEID() {
		return sessionID_SEID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSessionID_SEID(String newSessionID_SEID) {
		String oldSessionID_SEID = sessionID_SEID;
		sessionID_SEID = newSessionID_SEID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__SESSION_ID_SEID, oldSessionID_SEID, sessionID_SEID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getApplicationID_APID() {
		return applicationID_APID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setApplicationID_APID(String newApplicationID_APID) {
		String oldApplicationID_APID = applicationID_APID;
		applicationID_APID = newApplicationID_APID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID, oldApplicationID_APID, applicationID_APID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getContextID_CTID() {
		return contextID_CTID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setContextID_CTID(String newContextID_CTID) {
		String oldContextID_CTID = contextID_CTID;
		contextID_CTID = newContextID_CTID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID, oldContextID_CTID, contextID_CTID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLT_MessageType getMessageType_MSTP() {
		return messageType_MSTP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageType_MSTP(DLT_MessageType newMessageType_MSTP) {
		DLT_MessageType oldMessageType_MSTP = messageType_MSTP;
		messageType_MSTP = newMessageType_MSTP == null ? MESSAGE_TYPE_MSTP_EDEFAULT : newMessageType_MSTP;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP, oldMessageType_MSTP, messageType_MSTP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLT_MessageLogInfo getMessageLogInfo_MSLI() {
		return messageLogInfo_MSLI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageLogInfo_MSLI(DLT_MessageLogInfo newMessageLogInfo_MSLI) {
		DLT_MessageLogInfo oldMessageLogInfo_MSLI = messageLogInfo_MSLI;
		messageLogInfo_MSLI = newMessageLogInfo_MSLI == null ? MESSAGE_LOG_INFO_MSLI_EDEFAULT : newMessageLogInfo_MSLI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI, oldMessageLogInfo_MSLI, messageLogInfo_MSLI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLT_MessageTraceInfo getMessageTraceInfo_MSTI() {
		return messageTraceInfo_MSTI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageTraceInfo_MSTI(DLT_MessageTraceInfo newMessageTraceInfo_MSTI) {
		DLT_MessageTraceInfo oldMessageTraceInfo_MSTI = messageTraceInfo_MSTI;
		messageTraceInfo_MSTI = newMessageTraceInfo_MSTI == null ? MESSAGE_TRACE_INFO_MSTI_EDEFAULT : newMessageTraceInfo_MSTI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI, oldMessageTraceInfo_MSTI, messageTraceInfo_MSTI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLT_MessageBusInfo getMessageBusInfo_MSBI() {
		return messageBusInfo_MSBI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageBusInfo_MSBI(DLT_MessageBusInfo newMessageBusInfo_MSBI) {
		DLT_MessageBusInfo oldMessageBusInfo_MSBI = messageBusInfo_MSBI;
		messageBusInfo_MSBI = newMessageBusInfo_MSBI == null ? MESSAGE_BUS_INFO_MSBI_EDEFAULT : newMessageBusInfo_MSBI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI, oldMessageBusInfo_MSBI, messageBusInfo_MSBI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLT_MessageControlInfo getMessageControlInfo_MSCI() {
		return messageControlInfo_MSCI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageControlInfo_MSCI(DLT_MessageControlInfo newMessageControlInfo_MSCI) {
		DLT_MessageControlInfo oldMessageControlInfo_MSCI = messageControlInfo_MSCI;
		messageControlInfo_MSCI = newMessageControlInfo_MSCI == null ? MESSAGE_CONTROL_INFO_MSCI_EDEFAULT : newMessageControlInfo_MSCI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI, oldMessageControlInfo_MSCI, messageControlInfo_MSCI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageLogInfo_MSLITmplParam() {
		return messageLogInfo_MSLITmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageLogInfo_MSLITmplParam(String newMessageLogInfo_MSLITmplParam) {
		String oldMessageLogInfo_MSLITmplParam = messageLogInfo_MSLITmplParam;
		messageLogInfo_MSLITmplParam = newMessageLogInfo_MSLITmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM, oldMessageLogInfo_MSLITmplParam, messageLogInfo_MSLITmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageTraceInfo_MSTITmplParam() {
		return messageTraceInfo_MSTITmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageTraceInfo_MSTITmplParam(String newMessageTraceInfo_MSTITmplParam) {
		String oldMessageTraceInfo_MSTITmplParam = messageTraceInfo_MSTITmplParam;
		messageTraceInfo_MSTITmplParam = newMessageTraceInfo_MSTITmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM, oldMessageTraceInfo_MSTITmplParam, messageTraceInfo_MSTITmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageBusInfo_MSBITmplParam() {
		return messageBusInfo_MSBITmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageBusInfo_MSBITmplParam(String newMessageBusInfo_MSBITmplParam) {
		String oldMessageBusInfo_MSBITmplParam = messageBusInfo_MSBITmplParam;
		messageBusInfo_MSBITmplParam = newMessageBusInfo_MSBITmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM, oldMessageBusInfo_MSBITmplParam, messageBusInfo_MSBITmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageControlInfo_MSCITmplParam() {
		return messageControlInfo_MSCITmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageControlInfo_MSCITmplParam(String newMessageControlInfo_MSCITmplParam) {
		String oldMessageControlInfo_MSCITmplParam = messageControlInfo_MSCITmplParam;
		messageControlInfo_MSCITmplParam = newMessageControlInfo_MSCITmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM, oldMessageControlInfo_MSCITmplParam, messageControlInfo_MSCITmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageType_MSTPTmplParam() {
		return messageType_MSTPTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageType_MSTPTmplParam(String newMessageType_MSTPTmplParam) {
		String oldMessageType_MSTPTmplParam = messageType_MSTPTmplParam;
		messageType_MSTPTmplParam = newMessageType_MSTPTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM, oldMessageType_MSTPTmplParam, messageType_MSTPTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageBusInfo(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_BUS_INFO,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageBusInfo", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageControlInfo(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_CONTROL_INFO,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageControlInfo", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageLogInfo(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_LOG_INFO,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageLogInfo", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageTraceInfo(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_TRACE_INFO,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageTraceInfo", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.DLT_FILTER__ECU_ID_ECU:
				return getEcuID_ECU();
			case ConditionsPackage.DLT_FILTER__SESSION_ID_SEID:
				return getSessionID_SEID();
			case ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID:
				return getApplicationID_APID();
			case ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID:
				return getContextID_CTID();
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP:
				return getMessageType_MSTP();
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI:
				return getMessageLogInfo_MSLI();
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI:
				return getMessageTraceInfo_MSTI();
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI:
				return getMessageBusInfo_MSBI();
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI:
				return getMessageControlInfo_MSCI();
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM:
				return getMessageLogInfo_MSLITmplParam();
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM:
				return getMessageTraceInfo_MSTITmplParam();
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM:
				return getMessageBusInfo_MSBITmplParam();
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM:
				return getMessageControlInfo_MSCITmplParam();
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM:
				return getMessageType_MSTPTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.DLT_FILTER__ECU_ID_ECU:
				setEcuID_ECU((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__SESSION_ID_SEID:
				setSessionID_SEID((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID:
				setApplicationID_APID((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID:
				setContextID_CTID((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP:
				setMessageType_MSTP((DLT_MessageType)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI:
				setMessageLogInfo_MSLI((DLT_MessageLogInfo)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI:
				setMessageTraceInfo_MSTI((DLT_MessageTraceInfo)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI:
				setMessageBusInfo_MSBI((DLT_MessageBusInfo)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI:
				setMessageControlInfo_MSCI((DLT_MessageControlInfo)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM:
				setMessageLogInfo_MSLITmplParam((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM:
				setMessageTraceInfo_MSTITmplParam((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM:
				setMessageBusInfo_MSBITmplParam((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM:
				setMessageControlInfo_MSCITmplParam((String)newValue);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM:
				setMessageType_MSTPTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.DLT_FILTER__ECU_ID_ECU:
				setEcuID_ECU(ECU_ID_ECU_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__SESSION_ID_SEID:
				setSessionID_SEID(SESSION_ID_SEID_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID:
				setApplicationID_APID(APPLICATION_ID_APID_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID:
				setContextID_CTID(CONTEXT_ID_CTID_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP:
				setMessageType_MSTP(MESSAGE_TYPE_MSTP_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI:
				setMessageLogInfo_MSLI(MESSAGE_LOG_INFO_MSLI_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI:
				setMessageTraceInfo_MSTI(MESSAGE_TRACE_INFO_MSTI_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI:
				setMessageBusInfo_MSBI(MESSAGE_BUS_INFO_MSBI_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI:
				setMessageControlInfo_MSCI(MESSAGE_CONTROL_INFO_MSCI_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM:
				setMessageLogInfo_MSLITmplParam(MESSAGE_LOG_INFO_MSLI_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM:
				setMessageTraceInfo_MSTITmplParam(MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM:
				setMessageBusInfo_MSBITmplParam(MESSAGE_BUS_INFO_MSBI_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM:
				setMessageControlInfo_MSCITmplParam(MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM:
				setMessageType_MSTPTmplParam(MESSAGE_TYPE_MSTP_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.DLT_FILTER__ECU_ID_ECU:
				return ECU_ID_ECU_EDEFAULT == null ? ecuID_ECU != null : !ECU_ID_ECU_EDEFAULT.equals(ecuID_ECU);
			case ConditionsPackage.DLT_FILTER__SESSION_ID_SEID:
				return SESSION_ID_SEID_EDEFAULT == null ? sessionID_SEID != null : !SESSION_ID_SEID_EDEFAULT.equals(sessionID_SEID);
			case ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID:
				return APPLICATION_ID_APID_EDEFAULT == null ? applicationID_APID != null : !APPLICATION_ID_APID_EDEFAULT.equals(applicationID_APID);
			case ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID:
				return CONTEXT_ID_CTID_EDEFAULT == null ? contextID_CTID != null : !CONTEXT_ID_CTID_EDEFAULT.equals(contextID_CTID);
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP:
				return messageType_MSTP != MESSAGE_TYPE_MSTP_EDEFAULT;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI:
				return messageLogInfo_MSLI != MESSAGE_LOG_INFO_MSLI_EDEFAULT;
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI:
				return messageTraceInfo_MSTI != MESSAGE_TRACE_INFO_MSTI_EDEFAULT;
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI:
				return messageBusInfo_MSBI != MESSAGE_BUS_INFO_MSBI_EDEFAULT;
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI:
				return messageControlInfo_MSCI != MESSAGE_CONTROL_INFO_MSCI_EDEFAULT;
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM:
				return MESSAGE_LOG_INFO_MSLI_TMPL_PARAM_EDEFAULT == null ? messageLogInfo_MSLITmplParam != null : !MESSAGE_LOG_INFO_MSLI_TMPL_PARAM_EDEFAULT.equals(messageLogInfo_MSLITmplParam);
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM:
				return MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM_EDEFAULT == null ? messageTraceInfo_MSTITmplParam != null : !MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM_EDEFAULT.equals(messageTraceInfo_MSTITmplParam);
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM:
				return MESSAGE_BUS_INFO_MSBI_TMPL_PARAM_EDEFAULT == null ? messageBusInfo_MSBITmplParam != null : !MESSAGE_BUS_INFO_MSBI_TMPL_PARAM_EDEFAULT.equals(messageBusInfo_MSBITmplParam);
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM:
				return MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM_EDEFAULT == null ? messageControlInfo_MSCITmplParam != null : !MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM_EDEFAULT.equals(messageControlInfo_MSCITmplParam);
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM:
				return MESSAGE_TYPE_MSTP_TMPL_PARAM_EDEFAULT == null ? messageType_MSTPTmplParam != null : !MESSAGE_TYPE_MSTP_TMPL_PARAM_EDEFAULT.equals(messageType_MSTPTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_BUS_INFO__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageBusInfo((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_CONTROL_INFO__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageControlInfo((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_LOG_INFO__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageLogInfo((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_TRACE_INFO__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageTraceInfo((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (ecuID_ECU: ");
		result.append(ecuID_ECU);
		result.append(", sessionID_SEID: ");
		result.append(sessionID_SEID);
		result.append(", applicationID_APID: ");
		result.append(applicationID_APID);
		result.append(", contextID_CTID: ");
		result.append(contextID_CTID);
		result.append(", messageType_MSTP: ");
		result.append(messageType_MSTP);
		result.append(", messageLogInfo_MSLI: ");
		result.append(messageLogInfo_MSLI);
		result.append(", messageTraceInfo_MSTI: ");
		result.append(messageTraceInfo_MSTI);
		result.append(", messageBusInfo_MSBI: ");
		result.append(messageBusInfo_MSBI);
		result.append(", messageControlInfo_MSCI: ");
		result.append(messageControlInfo_MSCI);
		result.append(", messageLogInfo_MSLITmplParam: ");
		result.append(messageLogInfo_MSLITmplParam);
		result.append(", messageTraceInfo_MSTITmplParam: ");
		result.append(messageTraceInfo_MSTITmplParam);
		result.append(", messageBusInfo_MSBITmplParam: ");
		result.append(messageBusInfo_MSBITmplParam);
		result.append(", messageControlInfo_MSCITmplParam: ");
		result.append(messageControlInfo_MSCITmplParam);
		result.append(", messageType_MSTPTmplParam: ");
		result.append(messageType_MSTPTmplParam);
		result.append(')');
		return result.toString();
	}

} //DLTFilterImpl
