/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.IComputeVariableActionOperand;
import conditions.IOperand;
import conditions.IOperation;
import conditions.ISignalComparisonExpressionOperand;
import conditions.IStringOperand;
import conditions.StringLength;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Length</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.StringLengthImpl#getStringOperand <em>String Operand</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StringLengthImpl extends MinimalEObjectImpl.Container implements StringLength {
	/**
	 * The cached value of the '{@link #getStringOperand() <em>String Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringOperand()
	 * @generated
	 * @ordered
	 */
	protected IStringOperand stringOperand;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringLengthImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getStringLength();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IStringOperand getStringOperand() {
		return stringOperand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringOperand(IStringOperand newStringOperand, NotificationChain msgs) {
		IStringOperand oldStringOperand = stringOperand;
		stringOperand = newStringOperand;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_LENGTH__STRING_OPERAND, oldStringOperand, newStringOperand);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringOperand(IStringOperand newStringOperand) {
		if (newStringOperand != stringOperand) {
			NotificationChain msgs = null;
			if (stringOperand != null)
				msgs = ((InternalEObject)stringOperand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.STRING_LENGTH__STRING_OPERAND, null, msgs);
			if (newStringOperand != null)
				msgs = ((InternalEObject)newStringOperand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.STRING_LENGTH__STRING_OPERAND, null, msgs);
			msgs = basicSetStringOperand(newStringOperand, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_LENGTH__STRING_OPERAND, newStringOperand, newStringOperand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
				return basicSetStringOperand(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
				return getStringOperand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
				setStringOperand((IStringOperand)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
				setStringOperand((IStringOperand)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
				return stringOperand != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IComputeVariableActionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISignalComparisonExpressionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == IOperation.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERATION___GET_OPERAND_DATA_TYPE: return ConditionsPackage.STRING_LENGTH___GET_OPERAND_DATA_TYPE;
				case ConditionsPackage.IOPERATION___GET_OPERANDS: return ConditionsPackage.STRING_LENGTH___GET_OPERANDS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.STRING_LENGTH___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case ConditionsPackage.STRING_LENGTH___GET_OPERANDS:
				return get_Operands();
			case ConditionsPackage.STRING_LENGTH___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
			case ConditionsPackage.STRING_LENGTH___GET_READ_VARIABLES:
				return getReadVariables();
			case ConditionsPackage.STRING_LENGTH___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

} //StringLengthImpl
