/**
 */
package conditions.impl;

import conditions.BooleanOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.TransitionCheckExpression;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.TransitionCheckExpressionImpl#getStayActive <em>Stay Active</em>}</li>
 *   <li>{@link conditions.impl.TransitionCheckExpressionImpl#getCheckTransition <em>Check Transition</em>}</li>
 *   <li>{@link conditions.impl.TransitionCheckExpressionImpl#getStayActiveTmplParam <em>Stay Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionCheckExpressionImpl extends ExpressionImpl implements TransitionCheckExpression {
	/**
	 * The default value of the '{@link #getStayActive() <em>Stay Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStayActive()
	 * @generated
	 * @ordered
	 */
	protected static final BooleanOrTemplatePlaceholderEnum STAY_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.FALSE;

	/**
	 * The cached value of the '{@link #getStayActive() <em>Stay Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStayActive()
	 * @generated
	 * @ordered
	 */
	protected BooleanOrTemplatePlaceholderEnum stayActive = STAY_ACTIVE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCheckTransition() <em>Check Transition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckTransition()
	 * @generated
	 * @ordered
	 */
	protected Transition checkTransition;

	/**
	 * The default value of the '{@link #getStayActiveTmplParam() <em>Stay Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStayActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STAY_ACTIVE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStayActiveTmplParam() <em>Stay Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStayActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String stayActiveTmplParam = STAY_ACTIVE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionCheckExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getTransitionCheckExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanOrTemplatePlaceholderEnum getStayActive() {
		return stayActive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStayActive(BooleanOrTemplatePlaceholderEnum newStayActive) {
		BooleanOrTemplatePlaceholderEnum oldStayActive = stayActive;
		stayActive = newStayActive == null ? STAY_ACTIVE_EDEFAULT : newStayActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE, oldStayActive, stayActive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Transition getCheckTransition() {
		if (checkTransition != null && checkTransition.eIsProxy()) {
			InternalEObject oldCheckTransition = (InternalEObject)checkTransition;
			checkTransition = (Transition)eResolveProxy(oldCheckTransition);
			if (checkTransition != oldCheckTransition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION, oldCheckTransition, checkTransition));
			}
		}
		return checkTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transition basicGetCheckTransition() {
		return checkTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCheckTransition(Transition newCheckTransition) {
		Transition oldCheckTransition = checkTransition;
		checkTransition = newCheckTransition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION, oldCheckTransition, checkTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStayActiveTmplParam() {
		return stayActiveTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStayActiveTmplParam(String newStayActiveTmplParam) {
		String oldStayActiveTmplParam = stayActiveTmplParam;
		stayActiveTmplParam = newStayActiveTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM, oldStayActiveTmplParam, stayActiveTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStayActive(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STAY_ACTIVE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStayActive", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidTransition(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_TRANSITION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidTransition", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
				return getStayActive();
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
				if (resolve) return getCheckTransition();
				return basicGetCheckTransition();
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
				return getStayActiveTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
				setStayActive((BooleanOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
				setCheckTransition((Transition)newValue);
				return;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
				setStayActiveTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
				setStayActive(STAY_ACTIVE_EDEFAULT);
				return;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
				setCheckTransition((Transition)null);
				return;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
				setStayActiveTmplParam(STAY_ACTIVE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
				return stayActive != STAY_ACTIVE_EDEFAULT;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
				return checkTransition != null;
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
				return STAY_ACTIVE_TMPL_PARAM_EDEFAULT == null ? stayActiveTmplParam != null : !STAY_ACTIVE_TMPL_PARAM_EDEFAULT.equals(stayActiveTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STAY_ACTIVE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStayActive((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION___IS_VALID_HAS_VALID_TRANSITION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidTransition((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (stayActive: ");
		result.append(stayActive);
		result.append(", stayActiveTmplParam: ");
		result.append(stayActiveTmplParam);
		result.append(')');
		return result.toString();
	}

} //TransitionCheckExpressionImpl
