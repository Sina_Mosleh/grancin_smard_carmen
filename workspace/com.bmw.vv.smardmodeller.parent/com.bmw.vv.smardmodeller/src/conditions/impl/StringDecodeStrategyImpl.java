/**
 */
package conditions.impl;

import conditions.BooleanOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.StringDecodeStrategy;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.StringDecodeStrategyImpl#getStringTermination <em>String Termination</em>}</li>
 *   <li>{@link conditions.impl.StringDecodeStrategyImpl#getStringTerminationTmplParam <em>String Termination Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StringDecodeStrategyImpl extends DecodeStrategyImpl implements StringDecodeStrategy {
	/**
	 * The default value of the '{@link #getStringTermination() <em>String Termination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringTermination()
	 * @generated
	 * @ordered
	 */
	protected static final BooleanOrTemplatePlaceholderEnum STRING_TERMINATION_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

	/**
	 * The cached value of the '{@link #getStringTermination() <em>String Termination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringTermination()
	 * @generated
	 * @ordered
	 */
	protected BooleanOrTemplatePlaceholderEnum stringTermination = STRING_TERMINATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getStringTerminationTmplParam() <em>String Termination Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringTerminationTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STRING_TERMINATION_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStringTerminationTmplParam() <em>String Termination Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringTerminationTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String stringTerminationTmplParam = STRING_TERMINATION_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringDecodeStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getStringDecodeStrategy();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanOrTemplatePlaceholderEnum getStringTermination() {
		return stringTermination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringTermination(BooleanOrTemplatePlaceholderEnum newStringTermination) {
		BooleanOrTemplatePlaceholderEnum oldStringTermination = stringTermination;
		stringTermination = newStringTermination == null ? STRING_TERMINATION_EDEFAULT : newStringTermination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION, oldStringTermination, stringTermination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStringTerminationTmplParam() {
		return stringTerminationTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringTerminationTmplParam(String newStringTerminationTmplParam) {
		String oldStringTerminationTmplParam = stringTerminationTmplParam;
		stringTerminationTmplParam = newStringTerminationTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM, oldStringTerminationTmplParam, stringTerminationTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION:
				return getStringTermination();
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM:
				return getStringTerminationTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION:
				setStringTermination((BooleanOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM:
				setStringTerminationTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION:
				setStringTermination(STRING_TERMINATION_EDEFAULT);
				return;
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM:
				setStringTerminationTmplParam(STRING_TERMINATION_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION:
				return stringTermination != STRING_TERMINATION_EDEFAULT;
			case ConditionsPackage.STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM:
				return STRING_TERMINATION_TMPL_PARAM_EDEFAULT == null ? stringTerminationTmplParam != null : !STRING_TERMINATION_TMPL_PARAM_EDEFAULT.equals(stringTerminationTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (stringTermination: ");
		result.append(stringTermination);
		result.append(", stringTerminationTmplParam: ");
		result.append(stringTerminationTmplParam);
		result.append(')');
		return result.toString();
	}

} //StringDecodeStrategyImpl
