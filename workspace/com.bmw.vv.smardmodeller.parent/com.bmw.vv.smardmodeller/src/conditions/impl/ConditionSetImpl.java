/**
 */
package conditions.impl;

import conditions.Condition;
import conditions.ConditionSet;
import conditions.ConditionsPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Condition Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ConditionSetImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getBaureihe <em>Baureihe</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getIstufe <em>Istufe</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getSchemaversion <em>Schemaversion</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getUuid <em>Uuid</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getConditions <em>Conditions</em>}</li>
 *   <li>{@link conditions.impl.ConditionSetImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionSetImpl extends MinimalEObjectImpl.Container implements ConditionSet {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getBaureihe() <em>Baureihe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaureihe()
	 * @generated
	 * @ordered
	 */
	protected static final String BAUREIHE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBaureihe() <em>Baureihe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaureihe()
	 * @generated
	 * @ordered
	 */
	protected String baureihe = BAUREIHE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getIstufe() <em>Istufe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIstufe()
	 * @generated
	 * @ordered
	 */
	protected static final String ISTUFE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIstufe() <em>Istufe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIstufe()
	 * @generated
	 * @ordered
	 */
	protected String istufe = ISTUFE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSchemaversion() <em>Schemaversion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaversion()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEMAVERSION_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getSchemaversion() <em>Schemaversion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchemaversion()
	 * @generated
	 * @ordered
	 */
	protected String schemaversion = SCHEMAVERSION_EDEFAULT;

	/**
	 * This is true if the Schemaversion attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean schemaversionESet;

	/**
	 * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUuid()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConditions() <em>Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<Condition> conditions;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getConditionSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ConditionsPackage.CONDITION_SET__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBaureihe() {
		return baureihe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaureihe(String newBaureihe) {
		String oldBaureihe = baureihe;
		baureihe = newBaureihe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__BAUREIHE, oldBaureihe, baureihe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIstufe() {
		return istufe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIstufe(String newIstufe) {
		String oldIstufe = istufe;
		istufe = newIstufe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__ISTUFE, oldIstufe, istufe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSchemaversion() {
		return schemaversion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSchemaversion(String newSchemaversion) {
		String oldSchemaversion = schemaversion;
		schemaversion = newSchemaversion;
		boolean oldSchemaversionESet = schemaversionESet;
		schemaversionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__SCHEMAVERSION, oldSchemaversion, schemaversion, !oldSchemaversionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSchemaversion() {
		String oldSchemaversion = schemaversion;
		boolean oldSchemaversionESet = schemaversionESet;
		schemaversion = SCHEMAVERSION_EDEFAULT;
		schemaversionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.CONDITION_SET__SCHEMAVERSION, oldSchemaversion, SCHEMAVERSION_EDEFAULT, oldSchemaversionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSchemaversion() {
		return schemaversionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUuid() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUuid(String newUuid) {
		String oldUuid = uuid;
		uuid = newUuid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__UUID, oldUuid, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Condition> getConditions() {
		if (conditions == null) {
			conditions = new EObjectContainmentEList<Condition>(Condition.class, this, ConditionsPackage.CONDITION_SET__CONDITIONS);
		}
		return conditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITION_SET__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITION_SET__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ConditionsPackage.CONDITION_SET__CONDITIONS:
				return ((InternalEList<?>)getConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONDITION_SET__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ConditionsPackage.CONDITION_SET__BAUREIHE:
				return getBaureihe();
			case ConditionsPackage.CONDITION_SET__DESCRIPTION:
				return getDescription();
			case ConditionsPackage.CONDITION_SET__ISTUFE:
				return getIstufe();
			case ConditionsPackage.CONDITION_SET__SCHEMAVERSION:
				return getSchemaversion();
			case ConditionsPackage.CONDITION_SET__UUID:
				return getUuid();
			case ConditionsPackage.CONDITION_SET__VERSION:
				return getVersion();
			case ConditionsPackage.CONDITION_SET__CONDITIONS:
				return getConditions();
			case ConditionsPackage.CONDITION_SET__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONDITION_SET__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ConditionsPackage.CONDITION_SET__BAUREIHE:
				setBaureihe((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__ISTUFE:
				setIstufe((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__SCHEMAVERSION:
				setSchemaversion((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__UUID:
				setUuid((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__VERSION:
				setVersion((String)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__CONDITIONS:
				getConditions().clear();
				getConditions().addAll((Collection<? extends Condition>)newValue);
				return;
			case ConditionsPackage.CONDITION_SET__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITION_SET__GROUP:
				getGroup().clear();
				return;
			case ConditionsPackage.CONDITION_SET__BAUREIHE:
				setBaureihe(BAUREIHE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION_SET__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION_SET__ISTUFE:
				setIstufe(ISTUFE_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION_SET__SCHEMAVERSION:
				unsetSchemaversion();
				return;
			case ConditionsPackage.CONDITION_SET__UUID:
				setUuid(UUID_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION_SET__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ConditionsPackage.CONDITION_SET__CONDITIONS:
				getConditions().clear();
				return;
			case ConditionsPackage.CONDITION_SET__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITION_SET__GROUP:
				return group != null && !group.isEmpty();
			case ConditionsPackage.CONDITION_SET__BAUREIHE:
				return BAUREIHE_EDEFAULT == null ? baureihe != null : !BAUREIHE_EDEFAULT.equals(baureihe);
			case ConditionsPackage.CONDITION_SET__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ConditionsPackage.CONDITION_SET__ISTUFE:
				return ISTUFE_EDEFAULT == null ? istufe != null : !ISTUFE_EDEFAULT.equals(istufe);
			case ConditionsPackage.CONDITION_SET__SCHEMAVERSION:
				return isSetSchemaversion();
			case ConditionsPackage.CONDITION_SET__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case ConditionsPackage.CONDITION_SET__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ConditionsPackage.CONDITION_SET__CONDITIONS:
				return conditions != null && !conditions.isEmpty();
			case ConditionsPackage.CONDITION_SET__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", baureihe: ");
		result.append(baureihe);
		result.append(", description: ");
		result.append(description);
		result.append(", istufe: ");
		result.append(istufe);
		result.append(", schemaversion: ");
		if (schemaversionESet) result.append(schemaversion); else result.append("<unset>");
		result.append(", uuid: ");
		result.append(uuid);
		result.append(", version: ");
		result.append(version);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ConditionSetImpl
