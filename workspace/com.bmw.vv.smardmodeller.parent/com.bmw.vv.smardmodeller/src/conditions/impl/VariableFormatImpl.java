/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.FormatDataType;
import conditions.VariableFormat;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Format</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.VariableFormatImpl#getDigits <em>Digits</em>}</li>
 *   <li>{@link conditions.impl.VariableFormatImpl#getBaseDataType <em>Base Data Type</em>}</li>
 *   <li>{@link conditions.impl.VariableFormatImpl#isUpperCase <em>Upper Case</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableFormatImpl extends MinimalEObjectImpl.Container implements VariableFormat {
	/**
	 * The default value of the '{@link #getDigits() <em>Digits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDigits()
	 * @generated
	 * @ordered
	 */
	protected static final int DIGITS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDigits() <em>Digits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDigits()
	 * @generated
	 * @ordered
	 */
	protected int digits = DIGITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getBaseDataType() <em>Base Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDataType()
	 * @generated
	 * @ordered
	 */
	protected static final FormatDataType BASE_DATA_TYPE_EDEFAULT = FormatDataType.FLOAT;

	/**
	 * The cached value of the '{@link #getBaseDataType() <em>Base Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseDataType()
	 * @generated
	 * @ordered
	 */
	protected FormatDataType baseDataType = BASE_DATA_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #isUpperCase() <em>Upper Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpperCase()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UPPER_CASE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUpperCase() <em>Upper Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpperCase()
	 * @generated
	 * @ordered
	 */
	protected boolean upperCase = UPPER_CASE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFormatImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getVariableFormat();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getDigits() {
		return digits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDigits(int newDigits) {
		int oldDigits = digits;
		digits = newDigits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__DIGITS, oldDigits, digits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FormatDataType getBaseDataType() {
		return baseDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBaseDataType(FormatDataType newBaseDataType) {
		FormatDataType oldBaseDataType = baseDataType;
		baseDataType = newBaseDataType == null ? BASE_DATA_TYPE_EDEFAULT : newBaseDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE, oldBaseDataType, baseDataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isUpperCase() {
		return upperCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUpperCase(boolean newUpperCase) {
		boolean oldUpperCase = upperCase;
		upperCase = newUpperCase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE, oldUpperCase, upperCase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDigits(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.VARIABLE_FORMAT__IS_VALID_HAS_VALID_DIGITS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDigits", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
				return getDigits();
			case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
				return getBaseDataType();
			case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
				return isUpperCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
				setDigits((Integer)newValue);
				return;
			case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
				setBaseDataType((FormatDataType)newValue);
				return;
			case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
				setUpperCase((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
				setDigits(DIGITS_EDEFAULT);
				return;
			case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
				setBaseDataType(BASE_DATA_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
				setUpperCase(UPPER_CASE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
				return digits != DIGITS_EDEFAULT;
			case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
				return baseDataType != BASE_DATA_TYPE_EDEFAULT;
			case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
				return upperCase != UPPER_CASE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.VARIABLE_FORMAT___IS_VALID_HAS_VALID_DIGITS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDigits((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (digits: ");
		result.append(digits);
		result.append(", baseDataType: ");
		result.append(baseDataType);
		result.append(", upperCase: ");
		result.append(upperCase);
		result.append(')');
		return result.toString();
	}

} //VariableFormatImpl
