/**
 */
package conditions.impl;

import conditions.CanExtIdentifierOrTemplatePlaceholderEnum;
import conditions.CanMessageCheckExpression;
import conditions.CheckType;
import conditions.ConditionsPackage;
import conditions.RxTxFlagTypeOrTemplatePlaceholderEnum;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Can Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getBusid <em>Busid</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getType <em>Type</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getMessageIDs <em>Message IDs</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getBusidTmplParam <em>Busid Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getRxtxFlag <em>Rxtx Flag</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getRxtxFlagTypeTmplParam <em>Rxtx Flag Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getExtIdentifier <em>Ext Identifier</em>}</li>
 *   <li>{@link conditions.impl.CanMessageCheckExpressionImpl#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CanMessageCheckExpressionImpl extends ExpressionImpl implements CanMessageCheckExpression {
	/**
	 * The default value of the '{@link #getBusid() <em>Busid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusid()
	 * @generated
	 * @ordered
	 */
	protected static final String BUSID_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getBusid() <em>Busid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusid()
	 * @generated
	 * @ordered
	 */
	protected String busid = BUSID_EDEFAULT;

	/**
	 * This is true if the Busid attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean busidESet;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final CheckType TYPE_EDEFAULT = CheckType.ANY;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected CheckType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessageIDs() <em>Message IDs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIDs()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_IDS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageIDs() <em>Message IDs</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIDs()
	 * @generated
	 * @ordered
	 */
	protected String messageIDs = MESSAGE_IDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String typeTmplParam = TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getBusidTmplParam() <em>Busid Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusidTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String BUSID_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBusidTmplParam() <em>Busid Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusidTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String busidTmplParam = BUSID_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxtxFlag() <em>Rxtx Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlag()
	 * @generated
	 * @ordered
	 */
	protected static final RxTxFlagTypeOrTemplatePlaceholderEnum RXTX_FLAG_EDEFAULT = RxTxFlagTypeOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getRxtxFlag() <em>Rxtx Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlag()
	 * @generated
	 * @ordered
	 */
	protected RxTxFlagTypeOrTemplatePlaceholderEnum rxtxFlag = RXTX_FLAG_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxtxFlagTypeTmplParam() <em>Rxtx Flag Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlagTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String RXTX_FLAG_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRxtxFlagTypeTmplParam() <em>Rxtx Flag Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlagTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String rxtxFlagTypeTmplParam = RXTX_FLAG_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtIdentifier() <em>Ext Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final CanExtIdentifierOrTemplatePlaceholderEnum EXT_IDENTIFIER_EDEFAULT = CanExtIdentifierOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getExtIdentifier() <em>Ext Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifier()
	 * @generated
	 * @ordered
	 */
	protected CanExtIdentifierOrTemplatePlaceholderEnum extIdentifier = EXT_IDENTIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtIdentifierTmplParam() <em>Ext Identifier Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifierTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtIdentifierTmplParam() <em>Ext Identifier Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifierTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String extIdentifierTmplParam = EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CanMessageCheckExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getCanMessageCheckExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusid() {
		return busid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusid(String newBusid) {
		String oldBusid = busid;
		busid = newBusid;
		boolean oldBusidESet = busidESet;
		busidESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID, oldBusid, busid, !oldBusidESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetBusid() {
		String oldBusid = busid;
		boolean oldBusidESet = busidESet;
		busid = BUSID_EDEFAULT;
		busidESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID, oldBusid, BUSID_EDEFAULT, oldBusidESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetBusid() {
		return busidESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CheckType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(CheckType newType) {
		CheckType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageIDs() {
		return messageIDs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageIDs(String newMessageIDs) {
		String oldMessageIDs = messageIDs;
		messageIDs = newMessageIDs;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS, oldMessageIDs, messageIDs));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTypeTmplParam() {
		return typeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTypeTmplParam(String newTypeTmplParam) {
		String oldTypeTmplParam = typeTmplParam;
		typeTmplParam = newTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM, oldTypeTmplParam, typeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusidTmplParam() {
		return busidTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusidTmplParam(String newBusidTmplParam) {
		String oldBusidTmplParam = busidTmplParam;
		busidTmplParam = newBusidTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM, oldBusidTmplParam, busidTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RxTxFlagTypeOrTemplatePlaceholderEnum getRxtxFlag() {
		return rxtxFlag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum newRxtxFlag) {
		RxTxFlagTypeOrTemplatePlaceholderEnum oldRxtxFlag = rxtxFlag;
		rxtxFlag = newRxtxFlag == null ? RXTX_FLAG_EDEFAULT : newRxtxFlag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG, oldRxtxFlag, rxtxFlag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRxtxFlagTypeTmplParam() {
		return rxtxFlagTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRxtxFlagTypeTmplParam(String newRxtxFlagTypeTmplParam) {
		String oldRxtxFlagTypeTmplParam = rxtxFlagTypeTmplParam;
		rxtxFlagTypeTmplParam = newRxtxFlagTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM, oldRxtxFlagTypeTmplParam, rxtxFlagTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CanExtIdentifierOrTemplatePlaceholderEnum getExtIdentifier() {
		return extIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum newExtIdentifier) {
		CanExtIdentifierOrTemplatePlaceholderEnum oldExtIdentifier = extIdentifier;
		extIdentifier = newExtIdentifier == null ? EXT_IDENTIFIER_EDEFAULT : newExtIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER, oldExtIdentifier, extIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtIdentifierTmplParam() {
		return extIdentifierTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtIdentifierTmplParam(String newExtIdentifierTmplParam) {
		String oldExtIdentifierTmplParam = extIdentifierTmplParam;
		extIdentifierTmplParam = newExtIdentifierTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM, oldExtIdentifierTmplParam, extIdentifierTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidBusId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID_RANGE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageIdRange", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidCheckType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidRxTxFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_RX_TX_FLAG,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidRxTxFlag", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidExtIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_EXT_IDENTIFIER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidExtIdentifier", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID:
				return getBusid();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE:
				return getType();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
				return getMessageIDs();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				return getTypeTmplParam();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
				return getBusidTmplParam();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG:
				return getRxtxFlag();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM:
				return getRxtxFlagTypeTmplParam();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER:
				return getExtIdentifier();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM:
				return getExtIdentifierTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID:
				setBusid((String)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE:
				setType((CheckType)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
				setMessageIDs((String)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				setTypeTmplParam((String)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
				setBusidTmplParam((String)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG:
				setRxtxFlag((RxTxFlagTypeOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM:
				setRxtxFlagTypeTmplParam((String)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER:
				setExtIdentifier((CanExtIdentifierOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM:
				setExtIdentifierTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID:
				unsetBusid();
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
				setMessageIDs(MESSAGE_IDS_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				setTypeTmplParam(TYPE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
				setBusidTmplParam(BUSID_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG:
				setRxtxFlag(RXTX_FLAG_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM:
				setRxtxFlagTypeTmplParam(RXTX_FLAG_TYPE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER:
				setExtIdentifier(EXT_IDENTIFIER_EDEFAULT);
				return;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM:
				setExtIdentifierTmplParam(EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID:
				return isSetBusid();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE:
				return type != TYPE_EDEFAULT;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
				return MESSAGE_IDS_EDEFAULT == null ? messageIDs != null : !MESSAGE_IDS_EDEFAULT.equals(messageIDs);
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				return TYPE_TMPL_PARAM_EDEFAULT == null ? typeTmplParam != null : !TYPE_TMPL_PARAM_EDEFAULT.equals(typeTmplParam);
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
				return BUSID_TMPL_PARAM_EDEFAULT == null ? busidTmplParam != null : !BUSID_TMPL_PARAM_EDEFAULT.equals(busidTmplParam);
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG:
				return rxtxFlag != RXTX_FLAG_EDEFAULT;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM:
				return RXTX_FLAG_TYPE_TMPL_PARAM_EDEFAULT == null ? rxtxFlagTypeTmplParam != null : !RXTX_FLAG_TYPE_TMPL_PARAM_EDEFAULT.equals(rxtxFlagTypeTmplParam);
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER:
				return extIdentifier != EXT_IDENTIFIER_EDEFAULT;
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM:
				return EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT == null ? extIdentifierTmplParam != null : !EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT.equals(extIdentifierTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidBusId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_MESSAGE_ID_RANGE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageIdRange((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_CHECK_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidCheckType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_RX_TX_FLAG__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidRxTxFlag((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_EXT_IDENTIFIER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidExtIdentifier((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (busid: ");
		if (busidESet) result.append(busid); else result.append("<unset>");
		result.append(", type: ");
		result.append(type);
		result.append(", messageIDs: ");
		result.append(messageIDs);
		result.append(", typeTmplParam: ");
		result.append(typeTmplParam);
		result.append(", busidTmplParam: ");
		result.append(busidTmplParam);
		result.append(", rxtxFlag: ");
		result.append(rxtxFlag);
		result.append(", rxtxFlagTypeTmplParam: ");
		result.append(rxtxFlagTypeTmplParam);
		result.append(", extIdentifier: ");
		result.append(extIdentifier);
		result.append(", extIdentifierTmplParam: ");
		result.append(extIdentifierTmplParam);
		result.append(')');
		return result.toString();
	}

} //CanMessageCheckExpressionImpl
