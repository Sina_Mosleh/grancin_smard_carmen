/**
 */
package conditions.impl;

import conditions.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConditionsFactoryImpl extends EFactoryImpl implements ConditionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ConditionsFactory init() {
		try {
			ConditionsFactory theConditionsFactory = (ConditionsFactory)EPackage.Registry.INSTANCE.getEFactory(ConditionsPackage.eNS_URI);
			if (theConditionsFactory != null) {
				return theConditionsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ConditionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ConditionsPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case ConditionsPackage.CONDITION_SET: return createConditionSet();
			case ConditionsPackage.CONDITION: return createCondition();
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION: return createSignalComparisonExpression();
			case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION: return createCanMessageCheckExpression();
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION: return createLinMessageCheckExpression();
			case ConditionsPackage.STATE_CHECK_EXPRESSION: return createStateCheckExpression();
			case ConditionsPackage.TIMING_EXPRESSION: return createTimingExpression();
			case ConditionsPackage.TRUE_EXPRESSION: return createTrueExpression();
			case ConditionsPackage.LOGICAL_EXPRESSION: return createLogicalExpression();
			case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION: return createReferenceConditionExpression();
			case ConditionsPackage.TRANSITION_CHECK_EXPRESSION: return createTransitionCheckExpression();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION: return createFlexRayMessageCheckExpression();
			case ConditionsPackage.STOP_EXPRESSION: return createStopExpression();
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE: return createConstantComparatorValue();
			case ConditionsPackage.CALCULATION_EXPRESSION: return createCalculationExpression();
			case ConditionsPackage.VARIABLE_REFERENCE: return createVariableReference();
			case ConditionsPackage.CONDITIONS_DOCUMENT: return createConditionsDocument();
			case ConditionsPackage.VARIABLE_SET: return createVariableSet();
			case ConditionsPackage.SIGNAL_VARIABLE: return createSignalVariable();
			case ConditionsPackage.VALUE_VARIABLE: return createValueVariable();
			case ConditionsPackage.VARIABLE_FORMAT: return createVariableFormat();
			case ConditionsPackage.OBSERVER_VALUE_RANGE: return createObserverValueRange();
			case ConditionsPackage.SIGNAL_OBSERVER: return createSignalObserver();
			case ConditionsPackage.SIGNAL_REFERENCE_SET: return createSignalReferenceSet();
			case ConditionsPackage.SIGNAL_REFERENCE: return createSignalReference();
			case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE: return createBitPatternComparatorValue();
			case ConditionsPackage.NOT_EXPRESSION: return createNotExpression();
			case ConditionsPackage.VALUE_VARIABLE_OBSERVER: return createValueVariableObserver();
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE: return createParseStringToDouble();
			case ConditionsPackage.MATCHES: return createMatches();
			case ConditionsPackage.EXTRACT: return createExtract();
			case ConditionsPackage.SUBSTRING: return createSubstring();
			case ConditionsPackage.PARSE_NUMERIC_TO_STRING: return createParseNumericToString();
			case ConditionsPackage.STRING_LENGTH: return createStringLength();
			case ConditionsPackage.SOME_IP_MESSAGE: return createSomeIPMessage();
			case ConditionsPackage.CONTAINER_SIGNAL: return createContainerSignal();
			case ConditionsPackage.ETHERNET_FILTER: return createEthernetFilter();
			case ConditionsPackage.UDP_FILTER: return createUDPFilter();
			case ConditionsPackage.TCP_FILTER: return createTCPFilter();
			case ConditionsPackage.IPV4_FILTER: return createIPv4Filter();
			case ConditionsPackage.SOME_IP_FILTER: return createSomeIPFilter();
			case ConditionsPackage.FOR_EACH_EXPRESSION: return createForEachExpression();
			case ConditionsPackage.MESSAGE_CHECK_EXPRESSION: return createMessageCheckExpression();
			case ConditionsPackage.SOME_IPSD_FILTER: return createSomeIPSDFilter();
			case ConditionsPackage.SOME_IPSD_MESSAGE: return createSomeIPSDMessage();
			case ConditionsPackage.DOUBLE_SIGNAL: return createDoubleSignal();
			case ConditionsPackage.STRING_SIGNAL: return createStringSignal();
			case ConditionsPackage.DOUBLE_DECODE_STRATEGY: return createDoubleDecodeStrategy();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY: return createUniversalPayloadExtractStrategy();
			case ConditionsPackage.EMPTY_EXTRACT_STRATEGY: return createEmptyExtractStrategy();
			case ConditionsPackage.CAN_FILTER: return createCANFilter();
			case ConditionsPackage.LIN_FILTER: return createLINFilter();
			case ConditionsPackage.FLEX_RAY_FILTER: return createFlexRayFilter();
			case ConditionsPackage.DLT_FILTER: return createDLTFilter();
			case ConditionsPackage.UDPNM_FILTER: return createUDPNMFilter();
			case ConditionsPackage.CAN_MESSAGE: return createCANMessage();
			case ConditionsPackage.LIN_MESSAGE: return createLINMessage();
			case ConditionsPackage.FLEX_RAY_MESSAGE: return createFlexRayMessage();
			case ConditionsPackage.UDP_MESSAGE: return createUDPMessage();
			case ConditionsPackage.TCP_MESSAGE: return createTCPMessage();
			case ConditionsPackage.UDPNM_MESSAGE: return createUDPNMMessage();
			case ConditionsPackage.VERBOSE_DLT_MESSAGE: return createVerboseDLTMessage();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY: return createUniversalPayloadWithLegacyExtractStrategy();
			case ConditionsPackage.PLUGIN_FILTER: return createPluginFilter();
			case ConditionsPackage.PLUGIN_MESSAGE: return createPluginMessage();
			case ConditionsPackage.PLUGIN_SIGNAL: return createPluginSignal();
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY: return createPluginStateExtractStrategy();
			case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY: return createPluginResultExtractStrategy();
			case ConditionsPackage.EMPTY_DECODE_STRATEGY: return createEmptyDecodeStrategy();
			case ConditionsPackage.PLUGIN_CHECK_EXPRESSION: return createPluginCheckExpression();
			case ConditionsPackage.HEADER_SIGNAL: return createHeaderSignal();
			case ConditionsPackage.SOURCE_REFERENCE: return createSourceReference();
			case ConditionsPackage.ETHERNET_MESSAGE: return createEthernetMessage();
			case ConditionsPackage.IPV4_MESSAGE: return createIPv4Message();
			case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE: return createNonVerboseDLTMessage();
			case ConditionsPackage.STRING_DECODE_STRATEGY: return createStringDecodeStrategy();
			case ConditionsPackage.NON_VERBOSE_DLT_FILTER: return createNonVerboseDLTFilter();
			case ConditionsPackage.VERBOSE_DLT_EXTRACT_STRATEGY: return createVerboseDLTExtractStrategy();
			case ConditionsPackage.PAYLOAD_FILTER: return createPayloadFilter();
			case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER: return createVerboseDLTPayloadFilter();
			case ConditionsPackage.VARIABLE_STRUCTURE: return createVariableStructure();
			case ConditionsPackage.COMPUTED_VARIABLE: return createComputedVariable();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ConditionsPackage.CHECK_TYPE:
				return createCheckTypeFromString(eDataType, initialValue);
			case ConditionsPackage.COMPARATOR:
				return createComparatorFromString(eDataType, initialValue);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE:
				return createLogicalOperatorTypeFromString(eDataType, initialValue);
			case ConditionsPackage.SIGNAL_DATA_TYPE:
				return createSignalDataTypeFromString(eDataType, initialValue);
			case ConditionsPackage.FLEX_CHANNEL_TYPE:
				return createFlexChannelTypeFromString(eDataType, initialValue);
			case ConditionsPackage.BYTE_ORDER_TYPE:
				return createByteOrderTypeFromString(eDataType, initialValue);
			case ConditionsPackage.FORMAT_DATA_TYPE:
				return createFormatDataTypeFromString(eDataType, initialValue);
			case ConditionsPackage.FLEX_RAY_HEADER_FLAG_SELECTION:
				return createFlexRayHeaderFlagSelectionFromString(eDataType, initialValue);
			case ConditionsPackage.BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createBooleanOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createProtocolTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.ETHERNET_PROTOCOL_TYPE_SELECTION:
				return createEthernetProtocolTypeSelectionFromString(eDataType, initialValue);
			case ConditionsPackage.RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createRxTxFlagTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.OBSERVER_VALUE_TYPE:
				return createObserverValueTypeFromString(eDataType, initialValue);
			case ConditionsPackage.PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createPluginStateValueTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createAnalysisTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPMessageTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPMethodTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPReturnCodeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPSDFlagsOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPSDTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createStreamAnalysisFlagsOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createSomeIPSDEntryFlagsOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createCanExtIdentifierOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.DATA_TYPE:
				return createDataTypeFromString(eDataType, initialValue);
			case ConditionsPackage.DLT_MESSAGE_TYPE:
				return createDLT_MessageTypeFromString(eDataType, initialValue);
			case ConditionsPackage.DLT_MESSAGE_TRACE_INFO:
				return createDLT_MessageTraceInfoFromString(eDataType, initialValue);
			case ConditionsPackage.DLT_MESSAGE_LOG_INFO:
				return createDLT_MessageLogInfoFromString(eDataType, initialValue);
			case ConditionsPackage.DLT_MESSAGE_CONTROL_INFO:
				return createDLT_MessageControlInfoFromString(eDataType, initialValue);
			case ConditionsPackage.DLT_MESSAGE_BUS_INFO:
				return createDLT_MessageBusInfoFromString(eDataType, initialValue);
			case ConditionsPackage.FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE:
				return createForEachExpressionModifierTemplateFromString(eDataType, initialValue);
			case ConditionsPackage.AUTOSAR_DATA_TYPE:
				return createAUTOSARDataTypeFromString(eDataType, initialValue);
			case ConditionsPackage.CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return createCANFrameTypeOrTemplatePlaceholderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM:
				return createEvaluationBehaviourOrTemplateDefinedEnumFromString(eDataType, initialValue);
			case ConditionsPackage.SIGNAL_VARIABLE_LAG_ENUM:
				return createSignalVariableLagEnumFromString(eDataType, initialValue);
			case ConditionsPackage.TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM:
				return createTTLOrTemplatePlaceHolderEnumFromString(eDataType, initialValue);
			case ConditionsPackage.CHECK_TYPE_OBJECT:
				return createCheckTypeObjectFromString(eDataType, initialValue);
			case ConditionsPackage.COMPARATOR_OBJECT:
				return createComparatorObjectFromString(eDataType, initialValue);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE_OBJECT:
				return createLogicalOperatorTypeObjectFromString(eDataType, initialValue);
			case ConditionsPackage.HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER:
				return createHexOrIntOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.INT_OR_TEMPLATE_PLACEHOLDER:
				return createIntOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.LONG_OR_TEMPLATE_PLACEHOLDER:
				return createLongOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return createDoubleOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER:
				return createDoubleOrHexOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.IP_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return createIPPatternOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return createMACPatternOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return createPortPatternOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return createVlanIdPatternOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.IP_OR_TEMPLATE_PLACEHOLDER:
				return createIPOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.PORT_OR_TEMPLATE_PLACEHOLDER:
				return createPortOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.VLAN_ID_OR_TEMPLATE_PLACEHOLDER:
				return createVlanIdOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.MAC_OR_TEMPLATE_PLACEHOLDER:
				return createMACOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.BITMASK_OR_TEMPLATE_PLACEHOLDER:
				return createBitmaskOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER:
				return createHexOrIntOrNullOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return createNonZeroDoubleOrTemplatePlaceholderFromString(eDataType, initialValue);
			case ConditionsPackage.VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING:
				return createValVarInitialValueDoubleOrTemplatePlaceholderOrStringFromString(eDataType, initialValue);
			case ConditionsPackage.BYTE_OR_TEMPLATE_PLACEHOLDER:
				return createByteOrTemplatePlaceholderFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ConditionsPackage.CHECK_TYPE:
				return convertCheckTypeToString(eDataType, instanceValue);
			case ConditionsPackage.COMPARATOR:
				return convertComparatorToString(eDataType, instanceValue);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE:
				return convertLogicalOperatorTypeToString(eDataType, instanceValue);
			case ConditionsPackage.SIGNAL_DATA_TYPE:
				return convertSignalDataTypeToString(eDataType, instanceValue);
			case ConditionsPackage.FLEX_CHANNEL_TYPE:
				return convertFlexChannelTypeToString(eDataType, instanceValue);
			case ConditionsPackage.BYTE_ORDER_TYPE:
				return convertByteOrderTypeToString(eDataType, instanceValue);
			case ConditionsPackage.FORMAT_DATA_TYPE:
				return convertFormatDataTypeToString(eDataType, instanceValue);
			case ConditionsPackage.FLEX_RAY_HEADER_FLAG_SELECTION:
				return convertFlexRayHeaderFlagSelectionToString(eDataType, instanceValue);
			case ConditionsPackage.BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertBooleanOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertProtocolTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.ETHERNET_PROTOCOL_TYPE_SELECTION:
				return convertEthernetProtocolTypeSelectionToString(eDataType, instanceValue);
			case ConditionsPackage.RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertRxTxFlagTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.OBSERVER_VALUE_TYPE:
				return convertObserverValueTypeToString(eDataType, instanceValue);
			case ConditionsPackage.PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertPluginStateValueTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertAnalysisTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPMessageTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPMethodTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPReturnCodeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPSDFlagsOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPSDTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertStreamAnalysisFlagsOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertSomeIPSDEntryFlagsOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertCanExtIdentifierOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.DATA_TYPE:
				return convertDataTypeToString(eDataType, instanceValue);
			case ConditionsPackage.DLT_MESSAGE_TYPE:
				return convertDLT_MessageTypeToString(eDataType, instanceValue);
			case ConditionsPackage.DLT_MESSAGE_TRACE_INFO:
				return convertDLT_MessageTraceInfoToString(eDataType, instanceValue);
			case ConditionsPackage.DLT_MESSAGE_LOG_INFO:
				return convertDLT_MessageLogInfoToString(eDataType, instanceValue);
			case ConditionsPackage.DLT_MESSAGE_CONTROL_INFO:
				return convertDLT_MessageControlInfoToString(eDataType, instanceValue);
			case ConditionsPackage.DLT_MESSAGE_BUS_INFO:
				return convertDLT_MessageBusInfoToString(eDataType, instanceValue);
			case ConditionsPackage.FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE:
				return convertForEachExpressionModifierTemplateToString(eDataType, instanceValue);
			case ConditionsPackage.AUTOSAR_DATA_TYPE:
				return convertAUTOSARDataTypeToString(eDataType, instanceValue);
			case ConditionsPackage.CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM:
				return convertCANFrameTypeOrTemplatePlaceholderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM:
				return convertEvaluationBehaviourOrTemplateDefinedEnumToString(eDataType, instanceValue);
			case ConditionsPackage.SIGNAL_VARIABLE_LAG_ENUM:
				return convertSignalVariableLagEnumToString(eDataType, instanceValue);
			case ConditionsPackage.TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM:
				return convertTTLOrTemplatePlaceHolderEnumToString(eDataType, instanceValue);
			case ConditionsPackage.CHECK_TYPE_OBJECT:
				return convertCheckTypeObjectToString(eDataType, instanceValue);
			case ConditionsPackage.COMPARATOR_OBJECT:
				return convertComparatorObjectToString(eDataType, instanceValue);
			case ConditionsPackage.LOGICAL_OPERATOR_TYPE_OBJECT:
				return convertLogicalOperatorTypeObjectToString(eDataType, instanceValue);
			case ConditionsPackage.HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER:
				return convertHexOrIntOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.INT_OR_TEMPLATE_PLACEHOLDER:
				return convertIntOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.LONG_OR_TEMPLATE_PLACEHOLDER:
				return convertLongOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return convertDoubleOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER:
				return convertDoubleOrHexOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.IP_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return convertIPPatternOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return convertMACPatternOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return convertPortPatternOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER:
				return convertVlanIdPatternOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.IP_OR_TEMPLATE_PLACEHOLDER:
				return convertIPOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.PORT_OR_TEMPLATE_PLACEHOLDER:
				return convertPortOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.VLAN_ID_OR_TEMPLATE_PLACEHOLDER:
				return convertVlanIdOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.MAC_OR_TEMPLATE_PLACEHOLDER:
				return convertMACOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.BITMASK_OR_TEMPLATE_PLACEHOLDER:
				return convertBitmaskOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER:
				return convertHexOrIntOrNullOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER:
				return convertNonZeroDoubleOrTemplatePlaceholderToString(eDataType, instanceValue);
			case ConditionsPackage.VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING:
				return convertValVarInitialValueDoubleOrTemplatePlaceholderOrStringToString(eDataType, instanceValue);
			case ConditionsPackage.BYTE_OR_TEMPLATE_PLACEHOLDER:
				return convertByteOrTemplatePlaceholderToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionSet createConditionSet() {
		ConditionSetImpl conditionSet = new ConditionSetImpl();
		return conditionSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalComparisonExpression createSignalComparisonExpression() {
		SignalComparisonExpressionImpl signalComparisonExpression = new SignalComparisonExpressionImpl();
		return signalComparisonExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CanMessageCheckExpression createCanMessageCheckExpression() {
		CanMessageCheckExpressionImpl canMessageCheckExpression = new CanMessageCheckExpressionImpl();
		return canMessageCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LinMessageCheckExpression createLinMessageCheckExpression() {
		LinMessageCheckExpressionImpl linMessageCheckExpression = new LinMessageCheckExpressionImpl();
		return linMessageCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StateCheckExpression createStateCheckExpression() {
		StateCheckExpressionImpl stateCheckExpression = new StateCheckExpressionImpl();
		return stateCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimingExpression createTimingExpression() {
		TimingExpressionImpl timingExpression = new TimingExpressionImpl();
		return timingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TrueExpression createTrueExpression() {
		TrueExpressionImpl trueExpression = new TrueExpressionImpl();
		return trueExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LogicalExpression createLogicalExpression() {
		LogicalExpressionImpl logicalExpression = new LogicalExpressionImpl();
		return logicalExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ReferenceConditionExpression createReferenceConditionExpression() {
		ReferenceConditionExpressionImpl referenceConditionExpression = new ReferenceConditionExpressionImpl();
		return referenceConditionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TransitionCheckExpression createTransitionCheckExpression() {
		TransitionCheckExpressionImpl transitionCheckExpression = new TransitionCheckExpressionImpl();
		return transitionCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayMessageCheckExpression createFlexRayMessageCheckExpression() {
		FlexRayMessageCheckExpressionImpl flexRayMessageCheckExpression = new FlexRayMessageCheckExpressionImpl();
		return flexRayMessageCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StopExpression createStopExpression() {
		StopExpressionImpl stopExpression = new StopExpressionImpl();
		return stopExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstantComparatorValue createConstantComparatorValue() {
		ConstantComparatorValueImpl constantComparatorValue = new ConstantComparatorValueImpl();
		return constantComparatorValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CalculationExpression createCalculationExpression() {
		CalculationExpressionImpl calculationExpression = new CalculationExpressionImpl();
		return calculationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableReference createVariableReference() {
		VariableReferenceImpl variableReference = new VariableReferenceImpl();
		return variableReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionsDocument createConditionsDocument() {
		ConditionsDocumentImpl conditionsDocument = new ConditionsDocumentImpl();
		return conditionsDocument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableSet createVariableSet() {
		VariableSetImpl variableSet = new VariableSetImpl();
		return variableSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalVariable createSignalVariable() {
		SignalVariableImpl signalVariable = new SignalVariableImpl();
		return signalVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueVariable createValueVariable() {
		ValueVariableImpl valueVariable = new ValueVariableImpl();
		return valueVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableFormat createVariableFormat() {
		VariableFormatImpl variableFormat = new VariableFormatImpl();
		return variableFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ObserverValueRange createObserverValueRange() {
		ObserverValueRangeImpl observerValueRange = new ObserverValueRangeImpl();
		return observerValueRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalObserver createSignalObserver() {
		SignalObserverImpl signalObserver = new SignalObserverImpl();
		return signalObserver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalReferenceSet createSignalReferenceSet() {
		SignalReferenceSetImpl signalReferenceSet = new SignalReferenceSetImpl();
		return signalReferenceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalReference createSignalReference() {
		SignalReferenceImpl signalReference = new SignalReferenceImpl();
		return signalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BitPatternComparatorValue createBitPatternComparatorValue() {
		BitPatternComparatorValueImpl bitPatternComparatorValue = new BitPatternComparatorValueImpl();
		return bitPatternComparatorValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotExpression createNotExpression() {
		NotExpressionImpl notExpression = new NotExpressionImpl();
		return notExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueVariableObserver createValueVariableObserver() {
		ValueVariableObserverImpl valueVariableObserver = new ValueVariableObserverImpl();
		return valueVariableObserver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ParseStringToDouble createParseStringToDouble() {
		ParseStringToDoubleImpl parseStringToDouble = new ParseStringToDoubleImpl();
		return parseStringToDouble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Matches createMatches() {
		MatchesImpl matches = new MatchesImpl();
		return matches;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Extract createExtract() {
		ExtractImpl extract = new ExtractImpl();
		return extract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Substring createSubstring() {
		SubstringImpl substring = new SubstringImpl();
		return substring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ParseNumericToString createParseNumericToString() {
		ParseNumericToStringImpl parseNumericToString = new ParseNumericToStringImpl();
		return parseNumericToString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringLength createStringLength() {
		StringLengthImpl stringLength = new StringLengthImpl();
		return stringLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPMessage createSomeIPMessage() {
		SomeIPMessageImpl someIPMessage = new SomeIPMessageImpl();
		return someIPMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ContainerSignal createContainerSignal() {
		ContainerSignalImpl containerSignal = new ContainerSignalImpl();
		return containerSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EthernetFilter createEthernetFilter() {
		EthernetFilterImpl ethernetFilter = new EthernetFilterImpl();
		return ethernetFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UDPFilter createUDPFilter() {
		UDPFilterImpl udpFilter = new UDPFilterImpl();
		return udpFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TCPFilter createTCPFilter() {
		TCPFilterImpl tcpFilter = new TCPFilterImpl();
		return tcpFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IPv4Filter createIPv4Filter() {
		IPv4FilterImpl iPv4Filter = new IPv4FilterImpl();
		return iPv4Filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPFilter createSomeIPFilter() {
		SomeIPFilterImpl someIPFilter = new SomeIPFilterImpl();
		return someIPFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ForEachExpression createForEachExpression() {
		ForEachExpressionImpl forEachExpression = new ForEachExpressionImpl();
		return forEachExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public MessageCheckExpression createMessageCheckExpression() {
		MessageCheckExpressionImpl messageCheckExpression = new MessageCheckExpressionImpl();
		return messageCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPSDFilter createSomeIPSDFilter() {
		SomeIPSDFilterImpl someIPSDFilter = new SomeIPSDFilterImpl();
		return someIPSDFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPSDMessage createSomeIPSDMessage() {
		SomeIPSDMessageImpl someIPSDMessage = new SomeIPSDMessageImpl();
		return someIPSDMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DoubleSignal createDoubleSignal() {
		DoubleSignalImpl doubleSignal = new DoubleSignalImpl();
		return doubleSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringSignal createStringSignal() {
		StringSignalImpl stringSignal = new StringSignalImpl();
		return stringSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DoubleDecodeStrategy createDoubleDecodeStrategy() {
		DoubleDecodeStrategyImpl doubleDecodeStrategy = new DoubleDecodeStrategyImpl();
		return doubleDecodeStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UniversalPayloadExtractStrategy createUniversalPayloadExtractStrategy() {
		UniversalPayloadExtractStrategyImpl universalPayloadExtractStrategy = new UniversalPayloadExtractStrategyImpl();
		return universalPayloadExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EmptyExtractStrategy createEmptyExtractStrategy() {
		EmptyExtractStrategyImpl emptyExtractStrategy = new EmptyExtractStrategyImpl();
		return emptyExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CANFilter createCANFilter() {
		CANFilterImpl canFilter = new CANFilterImpl();
		return canFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINFilter createLINFilter() {
		LINFilterImpl linFilter = new LINFilterImpl();
		return linFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayFilter createFlexRayFilter() {
		FlexRayFilterImpl flexRayFilter = new FlexRayFilterImpl();
		return flexRayFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DLTFilter createDLTFilter() {
		DLTFilterImpl dltFilter = new DLTFilterImpl();
		return dltFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UDPNMFilter createUDPNMFilter() {
		UDPNMFilterImpl udpnmFilter = new UDPNMFilterImpl();
		return udpnmFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CANMessage createCANMessage() {
		CANMessageImpl canMessage = new CANMessageImpl();
		return canMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LINMessage createLINMessage() {
		LINMessageImpl linMessage = new LINMessageImpl();
		return linMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayMessage createFlexRayMessage() {
		FlexRayMessageImpl flexRayMessage = new FlexRayMessageImpl();
		return flexRayMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UDPMessage createUDPMessage() {
		UDPMessageImpl udpMessage = new UDPMessageImpl();
		return udpMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TCPMessage createTCPMessage() {
		TCPMessageImpl tcpMessage = new TCPMessageImpl();
		return tcpMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UDPNMMessage createUDPNMMessage() {
		UDPNMMessageImpl udpnmMessage = new UDPNMMessageImpl();
		return udpnmMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VerboseDLTMessage createVerboseDLTMessage() {
		VerboseDLTMessageImpl verboseDLTMessage = new VerboseDLTMessageImpl();
		return verboseDLTMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UniversalPayloadWithLegacyExtractStrategy createUniversalPayloadWithLegacyExtractStrategy() {
		UniversalPayloadWithLegacyExtractStrategyImpl universalPayloadWithLegacyExtractStrategy = new UniversalPayloadWithLegacyExtractStrategyImpl();
		return universalPayloadWithLegacyExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginFilter createPluginFilter() {
		PluginFilterImpl pluginFilter = new PluginFilterImpl();
		return pluginFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginMessage createPluginMessage() {
		PluginMessageImpl pluginMessage = new PluginMessageImpl();
		return pluginMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginSignal createPluginSignal() {
		PluginSignalImpl pluginSignal = new PluginSignalImpl();
		return pluginSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginStateExtractStrategy createPluginStateExtractStrategy() {
		PluginStateExtractStrategyImpl pluginStateExtractStrategy = new PluginStateExtractStrategyImpl();
		return pluginStateExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginResultExtractStrategy createPluginResultExtractStrategy() {
		PluginResultExtractStrategyImpl pluginResultExtractStrategy = new PluginResultExtractStrategyImpl();
		return pluginResultExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EmptyDecodeStrategy createEmptyDecodeStrategy() {
		EmptyDecodeStrategyImpl emptyDecodeStrategy = new EmptyDecodeStrategyImpl();
		return emptyDecodeStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginCheckExpression createPluginCheckExpression() {
		PluginCheckExpressionImpl pluginCheckExpression = new PluginCheckExpressionImpl();
		return pluginCheckExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HeaderSignal createHeaderSignal() {
		HeaderSignalImpl headerSignal = new HeaderSignalImpl();
		return headerSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SourceReference createSourceReference() {
		SourceReferenceImpl sourceReference = new SourceReferenceImpl();
		return sourceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EthernetMessage createEthernetMessage() {
		EthernetMessageImpl ethernetMessage = new EthernetMessageImpl();
		return ethernetMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IPv4Message createIPv4Message() {
		IPv4MessageImpl iPv4Message = new IPv4MessageImpl();
		return iPv4Message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NonVerboseDLTMessage createNonVerboseDLTMessage() {
		NonVerboseDLTMessageImpl nonVerboseDLTMessage = new NonVerboseDLTMessageImpl();
		return nonVerboseDLTMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StringDecodeStrategy createStringDecodeStrategy() {
		StringDecodeStrategyImpl stringDecodeStrategy = new StringDecodeStrategyImpl();
		return stringDecodeStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NonVerboseDLTFilter createNonVerboseDLTFilter() {
		NonVerboseDLTFilterImpl nonVerboseDLTFilter = new NonVerboseDLTFilterImpl();
		return nonVerboseDLTFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VerboseDLTExtractStrategy createVerboseDLTExtractStrategy() {
		VerboseDLTExtractStrategyImpl verboseDLTExtractStrategy = new VerboseDLTExtractStrategyImpl();
		return verboseDLTExtractStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PayloadFilter createPayloadFilter() {
		PayloadFilterImpl payloadFilter = new PayloadFilterImpl();
		return payloadFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VerboseDLTPayloadFilter createVerboseDLTPayloadFilter() {
		VerboseDLTPayloadFilterImpl verboseDLTPayloadFilter = new VerboseDLTPayloadFilterImpl();
		return verboseDLTPayloadFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableStructure createVariableStructure() {
		VariableStructureImpl variableStructure = new VariableStructureImpl();
		return variableStructure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ComputedVariable createComputedVariable() {
		ComputedVariableImpl computedVariable = new ComputedVariableImpl();
		return computedVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckType createCheckTypeFromString(EDataType eDataType, String initialValue) {
		CheckType result = CheckType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCheckTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comparator createComparatorFromString(EDataType eDataType, String initialValue) {
		Comparator result = Comparator.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperatorType createLogicalOperatorTypeFromString(EDataType eDataType, String initialValue) {
		LogicalOperatorType result = LogicalOperatorType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLogicalOperatorTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalDataType createSignalDataTypeFromString(EDataType eDataType, String initialValue) {
		SignalDataType result = SignalDataType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignalDataTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlexChannelType createFlexChannelTypeFromString(EDataType eDataType, String initialValue) {
		FlexChannelType result = FlexChannelType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFlexChannelTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ByteOrderType createByteOrderTypeFromString(EDataType eDataType, String initialValue) {
		ByteOrderType result = ByteOrderType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertByteOrderTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormatDataType createFormatDataTypeFromString(EDataType eDataType, String initialValue) {
		FormatDataType result = FormatDataType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFormatDataTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlexRayHeaderFlagSelection createFlexRayHeaderFlagSelectionFromString(EDataType eDataType, String initialValue) {
		FlexRayHeaderFlagSelection result = FlexRayHeaderFlagSelection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFlexRayHeaderFlagSelectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanOrTemplatePlaceholderEnum createBooleanOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		BooleanOrTemplatePlaceholderEnum result = BooleanOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProtocolTypeOrTemplatePlaceholderEnum createProtocolTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		ProtocolTypeOrTemplatePlaceholderEnum result = ProtocolTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertProtocolTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EthernetProtocolTypeSelection createEthernetProtocolTypeSelectionFromString(EDataType eDataType, String initialValue) {
		EthernetProtocolTypeSelection result = EthernetProtocolTypeSelection.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEthernetProtocolTypeSelectionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RxTxFlagTypeOrTemplatePlaceholderEnum createRxTxFlagTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		RxTxFlagTypeOrTemplatePlaceholderEnum result = RxTxFlagTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRxTxFlagTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObserverValueType createObserverValueTypeFromString(EDataType eDataType, String initialValue) {
		ObserverValueType result = ObserverValueType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObserverValueTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginStateValueTypeOrTemplatePlaceholderEnum createPluginStateValueTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		PluginStateValueTypeOrTemplatePlaceholderEnum result = PluginStateValueTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPluginStateValueTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnalysisTypeOrTemplatePlaceholderEnum createAnalysisTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		AnalysisTypeOrTemplatePlaceholderEnum result = AnalysisTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAnalysisTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPMessageTypeOrTemplatePlaceholderEnum createSomeIPMessageTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPMessageTypeOrTemplatePlaceholderEnum result = SomeIPMessageTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPMessageTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPMethodTypeOrTemplatePlaceholderEnum createSomeIPMethodTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPMethodTypeOrTemplatePlaceholderEnum result = SomeIPMethodTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPMethodTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPReturnCodeOrTemplatePlaceholderEnum createSomeIPReturnCodeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPReturnCodeOrTemplatePlaceholderEnum result = SomeIPReturnCodeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPReturnCodeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDFlagsOrTemplatePlaceholderEnum createSomeIPSDFlagsOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPSDFlagsOrTemplatePlaceholderEnum result = SomeIPSDFlagsOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPSDFlagsOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDTypeOrTemplatePlaceholderEnum createSomeIPSDTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPSDTypeOrTemplatePlaceholderEnum result = SomeIPSDTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPSDTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StreamAnalysisFlagsOrTemplatePlaceholderEnum createStreamAnalysisFlagsOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		StreamAnalysisFlagsOrTemplatePlaceholderEnum result = StreamAnalysisFlagsOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStreamAnalysisFlagsOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDEntryFlagsOrTemplatePlaceholderEnum createSomeIPSDEntryFlagsOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		SomeIPSDEntryFlagsOrTemplatePlaceholderEnum result = SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSomeIPSDEntryFlagsOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CanExtIdentifierOrTemplatePlaceholderEnum createCanExtIdentifierOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		CanExtIdentifierOrTemplatePlaceholderEnum result = CanExtIdentifierOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCanExtIdentifierOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataType createDataTypeFromString(EDataType eDataType, String initialValue) {
		DataType result = DataType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLT_MessageType createDLT_MessageTypeFromString(EDataType eDataType, String initialValue) {
		DLT_MessageType result = DLT_MessageType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDLT_MessageTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLT_MessageTraceInfo createDLT_MessageTraceInfoFromString(EDataType eDataType, String initialValue) {
		DLT_MessageTraceInfo result = DLT_MessageTraceInfo.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDLT_MessageTraceInfoToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLT_MessageLogInfo createDLT_MessageLogInfoFromString(EDataType eDataType, String initialValue) {
		DLT_MessageLogInfo result = DLT_MessageLogInfo.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDLT_MessageLogInfoToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLT_MessageControlInfo createDLT_MessageControlInfoFromString(EDataType eDataType, String initialValue) {
		DLT_MessageControlInfo result = DLT_MessageControlInfo.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDLT_MessageControlInfoToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLT_MessageBusInfo createDLT_MessageBusInfoFromString(EDataType eDataType, String initialValue) {
		DLT_MessageBusInfo result = DLT_MessageBusInfo.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDLT_MessageBusInfoToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachExpressionModifierTemplate createForEachExpressionModifierTemplateFromString(EDataType eDataType, String initialValue) {
		ForEachExpressionModifierTemplate result = ForEachExpressionModifierTemplate.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertForEachExpressionModifierTemplateToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AUTOSARDataType createAUTOSARDataTypeFromString(EDataType eDataType, String initialValue) {
		AUTOSARDataType result = AUTOSARDataType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAUTOSARDataTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CANFrameTypeOrTemplatePlaceholderEnum createCANFrameTypeOrTemplatePlaceholderEnumFromString(EDataType eDataType, String initialValue) {
		CANFrameTypeOrTemplatePlaceholderEnum result = CANFrameTypeOrTemplatePlaceholderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCANFrameTypeOrTemplatePlaceholderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationBehaviourOrTemplateDefinedEnum createEvaluationBehaviourOrTemplateDefinedEnumFromString(EDataType eDataType, String initialValue) {
		EvaluationBehaviourOrTemplateDefinedEnum result = EvaluationBehaviourOrTemplateDefinedEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEvaluationBehaviourOrTemplateDefinedEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalVariableLagEnum createSignalVariableLagEnumFromString(EDataType eDataType, String initialValue) {
		SignalVariableLagEnum result = SignalVariableLagEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSignalVariableLagEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TTLOrTemplatePlaceHolderEnum createTTLOrTemplatePlaceHolderEnumFromString(EDataType eDataType, String initialValue) {
		TTLOrTemplatePlaceHolderEnum result = TTLOrTemplatePlaceHolderEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTTLOrTemplatePlaceHolderEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CheckType createCheckTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createCheckTypeFromString(ConditionsPackage.eINSTANCE.getCheckType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCheckTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertCheckTypeToString(ConditionsPackage.eINSTANCE.getCheckType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Comparator createComparatorObjectFromString(EDataType eDataType, String initialValue) {
		return createComparatorFromString(ConditionsPackage.eINSTANCE.getComparator(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComparatorObjectToString(EDataType eDataType, Object instanceValue) {
		return convertComparatorToString(ConditionsPackage.eINSTANCE.getComparator(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperatorType createLogicalOperatorTypeObjectFromString(EDataType eDataType, String initialValue) {
		return createLogicalOperatorTypeFromString(ConditionsPackage.eINSTANCE.getLogicalOperatorType(), initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLogicalOperatorTypeObjectToString(EDataType eDataType, Object instanceValue) {
		return convertLogicalOperatorTypeToString(ConditionsPackage.eINSTANCE.getLogicalOperatorType(), instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createHexOrIntOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHexOrIntOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIntOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createLongOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLongOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createDoubleOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDoubleOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createDoubleOrHexOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDoubleOrHexOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIPPatternOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIPPatternOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createMACPatternOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMACPatternOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createPortPatternOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortPatternOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createVlanIdPatternOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVlanIdPatternOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIPOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIPOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createPortOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createVlanIdOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVlanIdOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createMACOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMACOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createBitmaskOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBitmaskOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createHexOrIntOrNullOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHexOrIntOrNullOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createNonZeroDoubleOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNonZeroDoubleOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createValVarInitialValueDoubleOrTemplatePlaceholderOrStringFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValVarInitialValueDoubleOrTemplatePlaceholderOrStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createByteOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
		return (String)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertByteOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionsPackage getConditionsPackage() {
		return (ConditionsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ConditionsPackage getPackage() {
		return ConditionsPackage.eINSTANCE;
	}

} //ConditionsFactoryImpl
