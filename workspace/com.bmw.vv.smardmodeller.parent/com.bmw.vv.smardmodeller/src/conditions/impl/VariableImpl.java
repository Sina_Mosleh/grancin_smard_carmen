/**
 */
package conditions.impl;

import conditions.AbstractObserver;
import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.Variable;
import conditions.VariableFormat;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.VariableImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link conditions.impl.VariableImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link conditions.impl.VariableImpl#getVariableFormat <em>Variable Format</em>}</li>
 *   <li>{@link conditions.impl.VariableImpl#getVariableFormatTmplParam <em>Variable Format Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class VariableImpl extends AbstractVariableImpl implements Variable {
	/**
	 * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected static final DataType DATA_TYPE_EDEFAULT = DataType.DOUBLE;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataType dataType = DATA_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected String unit = UNIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVariableFormat() <em>Variable Format</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableFormat()
	 * @generated
	 * @ordered
	 */
	protected VariableFormat variableFormat;

	/**
	 * The default value of the '{@link #getVariableFormatTmplParam() <em>Variable Format Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableFormatTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String VARIABLE_FORMAT_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVariableFormatTmplParam() <em>Variable Format Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableFormatTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String variableFormatTmplParam = VARIABLE_FORMAT_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getVariable();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDataType(DataType newDataType) {
		DataType oldDataType = dataType;
		dataType = newDataType == null ? DATA_TYPE_EDEFAULT : newDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE__DATA_TYPE, oldDataType, dataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUnit(String newUnit) {
		String oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableFormat getVariableFormat() {
		return variableFormat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariableFormat(VariableFormat newVariableFormat, NotificationChain msgs) {
		VariableFormat oldVariableFormat = variableFormat;
		variableFormat = newVariableFormat;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE__VARIABLE_FORMAT, oldVariableFormat, newVariableFormat);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVariableFormat(VariableFormat newVariableFormat) {
		if (newVariableFormat != variableFormat) {
			NotificationChain msgs = null;
			if (variableFormat != null)
				msgs = ((InternalEObject)variableFormat).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.VARIABLE__VARIABLE_FORMAT, null, msgs);
			if (newVariableFormat != null)
				msgs = ((InternalEObject)newVariableFormat).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.VARIABLE__VARIABLE_FORMAT, null, msgs);
			msgs = basicSetVariableFormat(newVariableFormat, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE__VARIABLE_FORMAT, newVariableFormat, newVariableFormat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getVariableFormatTmplParam() {
		return variableFormatTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVariableFormatTmplParam(String newVariableFormatTmplParam) {
		String oldVariableFormatTmplParam = variableFormatTmplParam;
		variableFormatTmplParam = newVariableFormatTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE__VARIABLE_FORMAT_TMPL_PARAM, oldVariableFormatTmplParam, variableFormatTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInterpretedValue(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.VARIABLE__IS_VALID_HAS_VALID_INTERPRETED_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInterpretedValue", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.VARIABLE__IS_VALID_HAS_VALID_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidUnit(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.VARIABLE__IS_VALID_HAS_VALID_UNIT,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidUnit", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidVariableFormatTmplParam(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.VARIABLE__IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidVariableFormatTmplParam", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractObserver> getObservers() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT:
				return basicSetVariableFormat(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE__DATA_TYPE:
				return getDataType();
			case ConditionsPackage.VARIABLE__UNIT:
				return getUnit();
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT:
				return getVariableFormat();
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT_TMPL_PARAM:
				return getVariableFormatTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE__DATA_TYPE:
				setDataType((DataType)newValue);
				return;
			case ConditionsPackage.VARIABLE__UNIT:
				setUnit((String)newValue);
				return;
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT:
				setVariableFormat((VariableFormat)newValue);
				return;
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT_TMPL_PARAM:
				setVariableFormatTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE__DATA_TYPE:
				setDataType(DATA_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.VARIABLE__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT:
				setVariableFormat((VariableFormat)null);
				return;
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT_TMPL_PARAM:
				setVariableFormatTmplParam(VARIABLE_FORMAT_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.VARIABLE__DATA_TYPE:
				return dataType != DATA_TYPE_EDEFAULT;
			case ConditionsPackage.VARIABLE__UNIT:
				return UNIT_EDEFAULT == null ? unit != null : !UNIT_EDEFAULT.equals(unit);
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT:
				return variableFormat != null;
			case ConditionsPackage.VARIABLE__VARIABLE_FORMAT_TMPL_PARAM:
				return VARIABLE_FORMAT_TMPL_PARAM_EDEFAULT == null ? variableFormatTmplParam != null : !VARIABLE_FORMAT_TMPL_PARAM_EDEFAULT.equals(variableFormatTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInterpretedValue((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidUnit((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidVariableFormatTmplParam((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.VARIABLE___GET_OBSERVERS:
				return getObservers();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (dataType: ");
		result.append(dataType);
		result.append(", unit: ");
		result.append(unit);
		result.append(", variableFormatTmplParam: ");
		result.append(variableFormatTmplParam);
		result.append(')');
		return result.toString();
	}

} //VariableImpl
