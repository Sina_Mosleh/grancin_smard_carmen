/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.PluginSignal;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PluginSignalImpl extends DoubleSignalImpl implements PluginSignal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginSignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getPluginSignal();
	}

} //PluginSignalImpl
