/**
 */
package conditions.impl;

import conditions.CheckType;
import conditions.ConditionsPackage;
import conditions.FlexRayHeaderFlagSelection;
import conditions.FlexRayMessageCheckExpression;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flex Ray Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getBusId <em>Bus Id</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getPayloadPreamble <em>Payload Preamble</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getZeroFrame <em>Zero Frame</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getSyncFrame <em>Sync Frame</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getStartupFrame <em>Startup Frame</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getNetworkMgmt <em>Network Mgmt</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getFlexrayMessageId <em>Flexray Message Id</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getPayloadPreambleTmplParam <em>Payload Preamble Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getZeroFrameTmplParam <em>Zero Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getSyncFrameTmplParam <em>Sync Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getStartupFrameTmplParam <em>Startup Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getNetworkMgmtTmplParam <em>Network Mgmt Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getType <em>Type</em>}</li>
 *   <li>{@link conditions.impl.FlexRayMessageCheckExpressionImpl#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlexRayMessageCheckExpressionImpl extends ExpressionImpl implements FlexRayMessageCheckExpression {
	/**
	 * The default value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusId()
	 * @generated
	 * @ordered
	 */
	protected static final String BUS_ID_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusId()
	 * @generated
	 * @ordered
	 */
	protected String busId = BUS_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPayloadPreamble() <em>Payload Preamble</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadPreamble()
	 * @generated
	 * @ordered
	 */
	protected static final FlexRayHeaderFlagSelection PAYLOAD_PREAMBLE_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

	/**
	 * The cached value of the '{@link #getPayloadPreamble() <em>Payload Preamble</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadPreamble()
	 * @generated
	 * @ordered
	 */
	protected FlexRayHeaderFlagSelection payloadPreamble = PAYLOAD_PREAMBLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getZeroFrame() <em>Zero Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZeroFrame()
	 * @generated
	 * @ordered
	 */
	protected static final FlexRayHeaderFlagSelection ZERO_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

	/**
	 * The cached value of the '{@link #getZeroFrame() <em>Zero Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZeroFrame()
	 * @generated
	 * @ordered
	 */
	protected FlexRayHeaderFlagSelection zeroFrame = ZERO_FRAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSyncFrame() <em>Sync Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncFrame()
	 * @generated
	 * @ordered
	 */
	protected static final FlexRayHeaderFlagSelection SYNC_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

	/**
	 * The cached value of the '{@link #getSyncFrame() <em>Sync Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncFrame()
	 * @generated
	 * @ordered
	 */
	protected FlexRayHeaderFlagSelection syncFrame = SYNC_FRAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartupFrame() <em>Startup Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartupFrame()
	 * @generated
	 * @ordered
	 */
	protected static final FlexRayHeaderFlagSelection STARTUP_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

	/**
	 * The cached value of the '{@link #getStartupFrame() <em>Startup Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartupFrame()
	 * @generated
	 * @ordered
	 */
	protected FlexRayHeaderFlagSelection startupFrame = STARTUP_FRAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNetworkMgmt() <em>Network Mgmt</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkMgmt()
	 * @generated
	 * @ordered
	 */
	protected static final FlexRayHeaderFlagSelection NETWORK_MGMT_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

	/**
	 * The cached value of the '{@link #getNetworkMgmt() <em>Network Mgmt</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkMgmt()
	 * @generated
	 * @ordered
	 */
	protected FlexRayHeaderFlagSelection networkMgmt = NETWORK_MGMT_EDEFAULT;

	/**
	 * The default value of the '{@link #getFlexrayMessageId() <em>Flexray Message Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlexrayMessageId()
	 * @generated
	 * @ordered
	 */
	protected static final String FLEXRAY_MESSAGE_ID_EDEFAULT = "<Channel>.<SlotID>.<Offset>.<Repetition>";

	/**
	 * The cached value of the '{@link #getFlexrayMessageId() <em>Flexray Message Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFlexrayMessageId()
	 * @generated
	 * @ordered
	 */
	protected String flexrayMessageId = FLEXRAY_MESSAGE_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPayloadPreambleTmplParam() <em>Payload Preamble Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadPreambleTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPayloadPreambleTmplParam() <em>Payload Preamble Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadPreambleTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String payloadPreambleTmplParam = PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getZeroFrameTmplParam() <em>Zero Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZeroFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String ZERO_FRAME_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getZeroFrameTmplParam() <em>Zero Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZeroFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String zeroFrameTmplParam = ZERO_FRAME_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSyncFrameTmplParam() <em>Sync Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String SYNC_FRAME_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSyncFrameTmplParam() <em>Sync Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSyncFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String syncFrameTmplParam = SYNC_FRAME_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartupFrameTmplParam() <em>Startup Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartupFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STARTUP_FRAME_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStartupFrameTmplParam() <em>Startup Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartupFrameTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String startupFrameTmplParam = STARTUP_FRAME_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getNetworkMgmtTmplParam() <em>Network Mgmt Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkMgmtTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String NETWORK_MGMT_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNetworkMgmtTmplParam() <em>Network Mgmt Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNetworkMgmtTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String networkMgmtTmplParam = NETWORK_MGMT_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String BUS_ID_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String busIdTmplParam = BUS_ID_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final CheckType TYPE_EDEFAULT = CheckType.ANY;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected CheckType type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String typeTmplParam = TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FlexRayMessageCheckExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusId() {
		return busId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusId(String newBusId) {
		String oldBusId = busId;
		busId = newBusId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID, oldBusId, busId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayHeaderFlagSelection getPayloadPreamble() {
		return payloadPreamble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPayloadPreamble(FlexRayHeaderFlagSelection newPayloadPreamble) {
		FlexRayHeaderFlagSelection oldPayloadPreamble = payloadPreamble;
		payloadPreamble = newPayloadPreamble == null ? PAYLOAD_PREAMBLE_EDEFAULT : newPayloadPreamble;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE, oldPayloadPreamble, payloadPreamble));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayHeaderFlagSelection getZeroFrame() {
		return zeroFrame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setZeroFrame(FlexRayHeaderFlagSelection newZeroFrame) {
		FlexRayHeaderFlagSelection oldZeroFrame = zeroFrame;
		zeroFrame = newZeroFrame == null ? ZERO_FRAME_EDEFAULT : newZeroFrame;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME, oldZeroFrame, zeroFrame));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayHeaderFlagSelection getSyncFrame() {
		return syncFrame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSyncFrame(FlexRayHeaderFlagSelection newSyncFrame) {
		FlexRayHeaderFlagSelection oldSyncFrame = syncFrame;
		syncFrame = newSyncFrame == null ? SYNC_FRAME_EDEFAULT : newSyncFrame;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME, oldSyncFrame, syncFrame));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayHeaderFlagSelection getStartupFrame() {
		return startupFrame;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartupFrame(FlexRayHeaderFlagSelection newStartupFrame) {
		FlexRayHeaderFlagSelection oldStartupFrame = startupFrame;
		startupFrame = newStartupFrame == null ? STARTUP_FRAME_EDEFAULT : newStartupFrame;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME, oldStartupFrame, startupFrame));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public FlexRayHeaderFlagSelection getNetworkMgmt() {
		return networkMgmt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNetworkMgmt(FlexRayHeaderFlagSelection newNetworkMgmt) {
		FlexRayHeaderFlagSelection oldNetworkMgmt = networkMgmt;
		networkMgmt = newNetworkMgmt == null ? NETWORK_MGMT_EDEFAULT : newNetworkMgmt;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT, oldNetworkMgmt, networkMgmt));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFlexrayMessageId() {
		return flexrayMessageId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFlexrayMessageId(String newFlexrayMessageId) {
		String oldFlexrayMessageId = flexrayMessageId;
		flexrayMessageId = newFlexrayMessageId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID, oldFlexrayMessageId, flexrayMessageId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPayloadPreambleTmplParam() {
		return payloadPreambleTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPayloadPreambleTmplParam(String newPayloadPreambleTmplParam) {
		String oldPayloadPreambleTmplParam = payloadPreambleTmplParam;
		payloadPreambleTmplParam = newPayloadPreambleTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM, oldPayloadPreambleTmplParam, payloadPreambleTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getZeroFrameTmplParam() {
		return zeroFrameTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setZeroFrameTmplParam(String newZeroFrameTmplParam) {
		String oldZeroFrameTmplParam = zeroFrameTmplParam;
		zeroFrameTmplParam = newZeroFrameTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM, oldZeroFrameTmplParam, zeroFrameTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSyncFrameTmplParam() {
		return syncFrameTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSyncFrameTmplParam(String newSyncFrameTmplParam) {
		String oldSyncFrameTmplParam = syncFrameTmplParam;
		syncFrameTmplParam = newSyncFrameTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM, oldSyncFrameTmplParam, syncFrameTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStartupFrameTmplParam() {
		return startupFrameTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartupFrameTmplParam(String newStartupFrameTmplParam) {
		String oldStartupFrameTmplParam = startupFrameTmplParam;
		startupFrameTmplParam = newStartupFrameTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM, oldStartupFrameTmplParam, startupFrameTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNetworkMgmtTmplParam() {
		return networkMgmtTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNetworkMgmtTmplParam(String newNetworkMgmtTmplParam) {
		String oldNetworkMgmtTmplParam = networkMgmtTmplParam;
		networkMgmtTmplParam = newNetworkMgmtTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM, oldNetworkMgmtTmplParam, networkMgmtTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusIdTmplParam() {
		return busIdTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusIdTmplParam(String newBusIdTmplParam) {
		String oldBusIdTmplParam = busIdTmplParam;
		busIdTmplParam = newBusIdTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM, oldBusIdTmplParam, busIdTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CheckType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(CheckType newType) {
		CheckType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTypeTmplParam() {
		return typeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTypeTmplParam(String newTypeTmplParam) {
		String oldTypeTmplParam = typeTmplParam;
		typeTmplParam = newTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM, oldTypeTmplParam, typeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidBusId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidCheckType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStartupFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STARTUP_FRAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStartupFrame", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSyncFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_SYNC_FRAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSyncFrame", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidZeroFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_ZERO_FRAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidZeroFrame", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidPayloadPreamble(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_PAYLOAD_PREAMBLE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidPayloadPreamble", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidNetworkMgmt(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_NETWORK_MGMT,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidNetworkMgmt", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMessageId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMessageId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
				return getBusId();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
				return getPayloadPreamble();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
				return getZeroFrame();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
				return getSyncFrame();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
				return getStartupFrame();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
				return getNetworkMgmt();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
				return getFlexrayMessageId();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
				return getPayloadPreambleTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
				return getZeroFrameTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
				return getSyncFrameTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
				return getStartupFrameTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
				return getNetworkMgmtTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
				return getBusIdTmplParam();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
				return getType();
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				return getTypeTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
				setBusId((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
				setPayloadPreamble((FlexRayHeaderFlagSelection)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
				setZeroFrame((FlexRayHeaderFlagSelection)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
				setSyncFrame((FlexRayHeaderFlagSelection)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
				setStartupFrame((FlexRayHeaderFlagSelection)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
				setNetworkMgmt((FlexRayHeaderFlagSelection)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
				setFlexrayMessageId((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
				setPayloadPreambleTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
				setZeroFrameTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
				setSyncFrameTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
				setStartupFrameTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
				setNetworkMgmtTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
				setBusIdTmplParam((String)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
				setType((CheckType)newValue);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				setTypeTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
				setBusId(BUS_ID_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
				setPayloadPreamble(PAYLOAD_PREAMBLE_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
				setZeroFrame(ZERO_FRAME_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
				setSyncFrame(SYNC_FRAME_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
				setStartupFrame(STARTUP_FRAME_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
				setNetworkMgmt(NETWORK_MGMT_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
				setFlexrayMessageId(FLEXRAY_MESSAGE_ID_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
				setPayloadPreambleTmplParam(PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
				setZeroFrameTmplParam(ZERO_FRAME_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
				setSyncFrameTmplParam(SYNC_FRAME_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
				setStartupFrameTmplParam(STARTUP_FRAME_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
				setNetworkMgmtTmplParam(NETWORK_MGMT_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
				setBusIdTmplParam(BUS_ID_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				setTypeTmplParam(TYPE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
				return BUS_ID_EDEFAULT == null ? busId != null : !BUS_ID_EDEFAULT.equals(busId);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
				return payloadPreamble != PAYLOAD_PREAMBLE_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
				return zeroFrame != ZERO_FRAME_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
				return syncFrame != SYNC_FRAME_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
				return startupFrame != STARTUP_FRAME_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
				return networkMgmt != NETWORK_MGMT_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
				return FLEXRAY_MESSAGE_ID_EDEFAULT == null ? flexrayMessageId != null : !FLEXRAY_MESSAGE_ID_EDEFAULT.equals(flexrayMessageId);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
				return PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT == null ? payloadPreambleTmplParam != null : !PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT.equals(payloadPreambleTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
				return ZERO_FRAME_TMPL_PARAM_EDEFAULT == null ? zeroFrameTmplParam != null : !ZERO_FRAME_TMPL_PARAM_EDEFAULT.equals(zeroFrameTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
				return SYNC_FRAME_TMPL_PARAM_EDEFAULT == null ? syncFrameTmplParam != null : !SYNC_FRAME_TMPL_PARAM_EDEFAULT.equals(syncFrameTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
				return STARTUP_FRAME_TMPL_PARAM_EDEFAULT == null ? startupFrameTmplParam != null : !STARTUP_FRAME_TMPL_PARAM_EDEFAULT.equals(startupFrameTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
				return NETWORK_MGMT_TMPL_PARAM_EDEFAULT == null ? networkMgmtTmplParam != null : !NETWORK_MGMT_TMPL_PARAM_EDEFAULT.equals(networkMgmtTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
				return BUS_ID_TMPL_PARAM_EDEFAULT == null ? busIdTmplParam != null : !BUS_ID_TMPL_PARAM_EDEFAULT.equals(busIdTmplParam);
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
				return type != TYPE_EDEFAULT;
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				return TYPE_TMPL_PARAM_EDEFAULT == null ? typeTmplParam != null : !TYPE_TMPL_PARAM_EDEFAULT.equals(typeTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidBusId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_CHECK_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidCheckType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STARTUP_FRAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStartupFrame((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_SYNC_FRAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSyncFrame((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_ZERO_FRAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidZeroFrame((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_PAYLOAD_PREAMBLE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidPayloadPreamble((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_NETWORK_MGMT__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidNetworkMgmt((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_MESSAGE_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMessageId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (busId: ");
		result.append(busId);
		result.append(", payloadPreamble: ");
		result.append(payloadPreamble);
		result.append(", zeroFrame: ");
		result.append(zeroFrame);
		result.append(", syncFrame: ");
		result.append(syncFrame);
		result.append(", startupFrame: ");
		result.append(startupFrame);
		result.append(", networkMgmt: ");
		result.append(networkMgmt);
		result.append(", flexrayMessageId: ");
		result.append(flexrayMessageId);
		result.append(", payloadPreambleTmplParam: ");
		result.append(payloadPreambleTmplParam);
		result.append(", zeroFrameTmplParam: ");
		result.append(zeroFrameTmplParam);
		result.append(", syncFrameTmplParam: ");
		result.append(syncFrameTmplParam);
		result.append(", startupFrameTmplParam: ");
		result.append(startupFrameTmplParam);
		result.append(", networkMgmtTmplParam: ");
		result.append(networkMgmtTmplParam);
		result.append(", busIdTmplParam: ");
		result.append(busIdTmplParam);
		result.append(", type: ");
		result.append(type);
		result.append(", typeTmplParam: ");
		result.append(typeTmplParam);
		result.append(')');
		return result.toString();
	}

} //FlexRayMessageCheckExpressionImpl
