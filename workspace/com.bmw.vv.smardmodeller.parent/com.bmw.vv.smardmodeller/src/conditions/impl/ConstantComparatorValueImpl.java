/**
 */
package conditions.impl;

import conditions.BooleanOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.ConstantComparatorValue;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ConstantComparatorValueImpl#getValue <em>Value</em>}</li>
 *   <li>{@link conditions.impl.ConstantComparatorValueImpl#getInterpretedValue <em>Interpreted Value</em>}</li>
 *   <li>{@link conditions.impl.ConstantComparatorValueImpl#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantComparatorValueImpl extends ComparatorSignalImpl implements ConstantComparatorValue {
	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpretedValue()
	 * @generated
	 * @ordered
	 */
	protected static final BooleanOrTemplatePlaceholderEnum INTERPRETED_VALUE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

	/**
	 * The cached value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpretedValue()
	 * @generated
	 * @ordered
	 */
	protected BooleanOrTemplatePlaceholderEnum interpretedValue = INTERPRETED_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpretedValueTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpretedValueTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String interpretedValueTmplParam = INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantComparatorValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getConstantComparatorValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanOrTemplatePlaceholderEnum getInterpretedValue() {
		return interpretedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInterpretedValue(BooleanOrTemplatePlaceholderEnum newInterpretedValue) {
		BooleanOrTemplatePlaceholderEnum oldInterpretedValue = interpretedValue;
		interpretedValue = newInterpretedValue == null ? INTERPRETED_VALUE_EDEFAULT : newInterpretedValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE, oldInterpretedValue, interpretedValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInterpretedValueTmplParam() {
		return interpretedValueTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInterpretedValueTmplParam(String newInterpretedValueTmplParam) {
		String oldInterpretedValueTmplParam = interpretedValueTmplParam;
		interpretedValueTmplParam = newInterpretedValueTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM, oldInterpretedValueTmplParam, interpretedValueTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInterpretedValue(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_INTERPRETED_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInterpretedValue", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidValue", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
				return getValue();
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
				return getInterpretedValue();
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
				return getInterpretedValueTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
				setValue((String)newValue);
				return;
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
				setInterpretedValue((BooleanOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
				setInterpretedValueTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
				setInterpretedValue(INTERPRETED_VALUE_EDEFAULT);
				return;
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
				setInterpretedValueTmplParam(INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
				return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
				return interpretedValue != INTERPRETED_VALUE_EDEFAULT;
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
				return INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT == null ? interpretedValueTmplParam != null : !INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT.equals(interpretedValueTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInterpretedValue((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CONSTANT_COMPARATOR_VALUE___IS_VALID_HAS_VALID_VALUE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidValue((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", interpretedValue: ");
		result.append(interpretedValue);
		result.append(", interpretedValueTmplParam: ");
		result.append(interpretedValueTmplParam);
		result.append(')');
		return result.toString();
	}

} //ConstantComparatorValueImpl
