/**
 */
package conditions.impl;

import conditions.AbstractSignal;
import conditions.ConditionsPackage;
import conditions.ContainerSignal;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Container Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ContainerSignalImpl#getContainedSignals <em>Contained Signals</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainerSignalImpl extends AbstractSignalImpl implements ContainerSignal {
	/**
	 * The cached value of the '{@link #getContainedSignals() <em>Contained Signals</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedSignals()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractSignal> containedSignals;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainerSignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getContainerSignal();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractSignal> getContainedSignals() {
		if (containedSignals == null) {
			containedSignals = new EObjectContainmentEList<AbstractSignal>(AbstractSignal.class, this, ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS);
		}
		return containedSignals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS:
				return ((InternalEList<?>)getContainedSignals()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS:
				return getContainedSignals();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS:
				getContainedSignals().clear();
				getContainedSignals().addAll((Collection<? extends AbstractSignal>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS:
				getContainedSignals().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONTAINER_SIGNAL__CONTAINED_SIGNALS:
				return containedSignals != null && !containedSignals.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ContainerSignalImpl
