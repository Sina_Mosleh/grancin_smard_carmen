/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.PluginFilter;
import conditions.PluginMessage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.PluginMessageImpl#getPluginFilter <em>Plugin Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginMessageImpl extends AbstractMessageImpl implements PluginMessage {
	/**
	 * The cached value of the '{@link #getPluginFilter() <em>Plugin Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPluginFilter()
	 * @generated
	 * @ordered
	 */
	protected PluginFilter pluginFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getPluginMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public PluginFilter getPluginFilter() {
		return pluginFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPluginFilter(PluginFilter newPluginFilter, NotificationChain msgs) {
		PluginFilter oldPluginFilter = pluginFilter;
		pluginFilter = newPluginFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER, oldPluginFilter, newPluginFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPluginFilter(PluginFilter newPluginFilter) {
		if (newPluginFilter != pluginFilter) {
			NotificationChain msgs = null;
			if (pluginFilter != null)
				msgs = ((InternalEObject)pluginFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER, null, msgs);
			if (newPluginFilter != null)
				msgs = ((InternalEObject)newPluginFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER, null, msgs);
			msgs = basicSetPluginFilter(newPluginFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER, newPluginFilter, newPluginFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER:
				return basicSetPluginFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER:
				return getPluginFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER:
				setPluginFilter((PluginFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER:
				setPluginFilter((PluginFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_MESSAGE__PLUGIN_FILTER:
				return pluginFilter != null;
		}
		return super.eIsSet(featureID);
	}

} //PluginMessageImpl
