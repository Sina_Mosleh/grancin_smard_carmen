/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.TimingExpression;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timing Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.TimingExpressionImpl#getMaxtime <em>Maxtime</em>}</li>
 *   <li>{@link conditions.impl.TimingExpressionImpl#getMintime <em>Mintime</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimingExpressionImpl extends ExpressionImpl implements TimingExpression {
	/**
	 * The default value of the '{@link #getMaxtime() <em>Maxtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxtime()
	 * @generated
	 * @ordered
	 */
	protected static final String MAXTIME_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getMaxtime() <em>Maxtime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxtime()
	 * @generated
	 * @ordered
	 */
	protected String maxtime = MAXTIME_EDEFAULT;

	/**
	 * This is true if the Maxtime attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean maxtimeESet;

	/**
	 * The default value of the '{@link #getMintime() <em>Mintime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMintime()
	 * @generated
	 * @ordered
	 */
	protected static final String MINTIME_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getMintime() <em>Mintime</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMintime()
	 * @generated
	 * @ordered
	 */
	protected String mintime = MINTIME_EDEFAULT;

	/**
	 * This is true if the Mintime attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mintimeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimingExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getTimingExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMaxtime() {
		return maxtime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMaxtime(String newMaxtime) {
		String oldMaxtime = maxtime;
		maxtime = newMaxtime;
		boolean oldMaxtimeESet = maxtimeESet;
		maxtimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TIMING_EXPRESSION__MAXTIME, oldMaxtime, maxtime, !oldMaxtimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMaxtime() {
		String oldMaxtime = maxtime;
		boolean oldMaxtimeESet = maxtimeESet;
		maxtime = MAXTIME_EDEFAULT;
		maxtimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.TIMING_EXPRESSION__MAXTIME, oldMaxtime, MAXTIME_EDEFAULT, oldMaxtimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMaxtime() {
		return maxtimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMintime() {
		return mintime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMintime(String newMintime) {
		String oldMintime = mintime;
		mintime = newMintime;
		boolean oldMintimeESet = mintimeESet;
		mintimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TIMING_EXPRESSION__MINTIME, oldMintime, mintime, !oldMintimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetMintime() {
		String oldMintime = mintime;
		boolean oldMintimeESet = mintimeESet;
		mintime = MINTIME_EDEFAULT;
		mintimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.TIMING_EXPRESSION__MINTIME, oldMintime, MINTIME_EDEFAULT, oldMintimeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetMintime() {
		return mintimeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidMinMaxTimes(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TIMING_EXPRESSION__IS_VALID_HAS_VALID_MIN_MAX_TIMES,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidMinMaxTimes", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
				return getMaxtime();
			case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
				return getMintime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
				setMaxtime((String)newValue);
				return;
			case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
				setMintime((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
				unsetMaxtime();
				return;
			case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
				unsetMintime();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
				return isSetMaxtime();
			case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
				return isSetMintime();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.TIMING_EXPRESSION___IS_VALID_HAS_VALID_MIN_MAX_TIMES__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidMinMaxTimes((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (maxtime: ");
		if (maxtimeESet) result.append(maxtime); else result.append("<unset>");
		result.append(", mintime: ");
		if (mintimeESet) result.append(mintime); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //TimingExpressionImpl
