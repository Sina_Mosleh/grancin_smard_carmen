/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.LINFilter;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LIN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.LINFilterImpl#getMessageIdRange <em>Message Id Range</em>}</li>
 *   <li>{@link conditions.impl.LINFilterImpl#getFrameId <em>Frame Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LINFilterImpl extends AbstractFilterImpl implements LINFilter {
	/**
	 * The default value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIdRange()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_ID_RANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIdRange()
	 * @generated
	 * @ordered
	 */
	protected String messageIdRange = MESSAGE_ID_RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameId()
	 * @generated
	 * @ordered
	 */
	protected static final String FRAME_ID_EDEFAULT = "0x0";

	/**
	 * The cached value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameId()
	 * @generated
	 * @ordered
	 */
	protected String frameId = FRAME_ID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LINFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getLINFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageIdRange() {
		return messageIdRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageIdRange(String newMessageIdRange) {
		String oldMessageIdRange = messageIdRange;
		messageIdRange = newMessageIdRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE, oldMessageIdRange, messageIdRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFrameId() {
		return frameId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrameId(String newFrameId) {
		String oldFrameId = frameId;
		frameId = newFrameId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_FILTER__FRAME_ID, oldFrameId, frameId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.LIN_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidFrameIdOrFrameIdRange", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
				return getMessageIdRange();
			case ConditionsPackage.LIN_FILTER__FRAME_ID:
				return getFrameId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
				setMessageIdRange((String)newValue);
				return;
			case ConditionsPackage.LIN_FILTER__FRAME_ID:
				setFrameId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
				setMessageIdRange(MESSAGE_ID_RANGE_EDEFAULT);
				return;
			case ConditionsPackage.LIN_FILTER__FRAME_ID:
				setFrameId(FRAME_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
				return MESSAGE_ID_RANGE_EDEFAULT == null ? messageIdRange != null : !MESSAGE_ID_RANGE_EDEFAULT.equals(messageIdRange);
			case ConditionsPackage.LIN_FILTER__FRAME_ID:
				return FRAME_ID_EDEFAULT == null ? frameId != null : !FRAME_ID_EDEFAULT.equals(frameId);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.LIN_FILTER___IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidFrameIdOrFrameIdRange((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (messageIdRange: ");
		result.append(messageIdRange);
		result.append(", frameId: ");
		result.append(frameId);
		result.append(')');
		return result.toString();
	}

} //LINFilterImpl
