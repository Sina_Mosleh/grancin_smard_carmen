/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.SignalObserver;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SignalObserverImpl extends AbstractObserverImpl implements SignalObserver {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SignalObserverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSignalObserver();
	}

} //SignalObserverImpl
