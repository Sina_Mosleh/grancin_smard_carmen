/**
 */
package conditions.impl;

import conditions.ConditionSet;
import conditions.ConditionsDocument;
import conditions.ConditionsPackage;
import conditions.SignalReferenceSet;
import conditions.VariableSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ConditionsDocumentImpl#getSignalReferencesSet <em>Signal References Set</em>}</li>
 *   <li>{@link conditions.impl.ConditionsDocumentImpl#getVariableSet <em>Variable Set</em>}</li>
 *   <li>{@link conditions.impl.ConditionsDocumentImpl#getConditionSet <em>Condition Set</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConditionsDocumentImpl extends MinimalEObjectImpl.Container implements ConditionsDocument {
	/**
	 * The cached value of the '{@link #getSignalReferencesSet() <em>Signal References Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignalReferencesSet()
	 * @generated
	 * @ordered
	 */
	protected SignalReferenceSet signalReferencesSet;

	/**
	 * The cached value of the '{@link #getVariableSet() <em>Variable Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableSet()
	 * @generated
	 * @ordered
	 */
	protected VariableSet variableSet;

	/**
	 * The cached value of the '{@link #getConditionSet() <em>Condition Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionSet()
	 * @generated
	 * @ordered
	 */
	protected ConditionSet conditionSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionsDocumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getConditionsDocument();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SignalReferenceSet getSignalReferencesSet() {
		return signalReferencesSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSignalReferencesSet(SignalReferenceSet newSignalReferencesSet, NotificationChain msgs) {
		SignalReferenceSet oldSignalReferencesSet = signalReferencesSet;
		signalReferencesSet = newSignalReferencesSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET, oldSignalReferencesSet, newSignalReferencesSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSignalReferencesSet(SignalReferenceSet newSignalReferencesSet) {
		if (newSignalReferencesSet != signalReferencesSet) {
			NotificationChain msgs = null;
			if (signalReferencesSet != null)
				msgs = ((InternalEObject)signalReferencesSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET, null, msgs);
			if (newSignalReferencesSet != null)
				msgs = ((InternalEObject)newSignalReferencesSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET, null, msgs);
			msgs = basicSetSignalReferencesSet(newSignalReferencesSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET, newSignalReferencesSet, newSignalReferencesSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public VariableSet getVariableSet() {
		return variableSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVariableSet(VariableSet newVariableSet, NotificationChain msgs) {
		VariableSet oldVariableSet = variableSet;
		variableSet = newVariableSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET, oldVariableSet, newVariableSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setVariableSet(VariableSet newVariableSet) {
		if (newVariableSet != variableSet) {
			NotificationChain msgs = null;
			if (variableSet != null)
				msgs = ((InternalEObject)variableSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET, null, msgs);
			if (newVariableSet != null)
				msgs = ((InternalEObject)newVariableSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET, null, msgs);
			msgs = basicSetVariableSet(newVariableSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET, newVariableSet, newVariableSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionSet getConditionSet() {
		return conditionSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConditionSet(ConditionSet newConditionSet, NotificationChain msgs) {
		ConditionSet oldConditionSet = conditionSet;
		conditionSet = newConditionSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET, oldConditionSet, newConditionSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConditionSet(ConditionSet newConditionSet) {
		if (newConditionSet != conditionSet) {
			NotificationChain msgs = null;
			if (conditionSet != null)
				msgs = ((InternalEObject)conditionSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET, null, msgs);
			if (newConditionSet != null)
				msgs = ((InternalEObject)newConditionSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET, null, msgs);
			msgs = basicSetConditionSet(newConditionSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET, newConditionSet, newConditionSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET:
				return basicSetSignalReferencesSet(null, msgs);
			case ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET:
				return basicSetVariableSet(null, msgs);
			case ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET:
				return basicSetConditionSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET:
				return getSignalReferencesSet();
			case ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET:
				return getVariableSet();
			case ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET:
				return getConditionSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET:
				setSignalReferencesSet((SignalReferenceSet)newValue);
				return;
			case ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET:
				setVariableSet((VariableSet)newValue);
				return;
			case ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET:
				setConditionSet((ConditionSet)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET:
				setSignalReferencesSet((SignalReferenceSet)null);
				return;
			case ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET:
				setVariableSet((VariableSet)null);
				return;
			case ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET:
				setConditionSet((ConditionSet)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET:
				return signalReferencesSet != null;
			case ConditionsPackage.CONDITIONS_DOCUMENT__VARIABLE_SET:
				return variableSet != null;
			case ConditionsPackage.CONDITIONS_DOCUMENT__CONDITION_SET:
				return conditionSet != null;
		}
		return super.eIsSet(featureID);
	}

} //ConditionsDocumentImpl
