/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.UDPNMFilter;
import conditions.UDPNMMessage;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UDPNM Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.UDPNMMessageImpl#getUdpnmFilter <em>Udpnm Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UDPNMMessageImpl extends AbstractBusMessageImpl implements UDPNMMessage {
	/**
	 * The cached value of the '{@link #getUdpnmFilter() <em>Udpnm Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUdpnmFilter()
	 * @generated
	 * @ordered
	 */
	protected UDPNMFilter udpnmFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UDPNMMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getUDPNMMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public UDPNMFilter getUdpnmFilter() {
		return udpnmFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUdpnmFilter(UDPNMFilter newUdpnmFilter, NotificationChain msgs) {
		UDPNMFilter oldUdpnmFilter = udpnmFilter;
		udpnmFilter = newUdpnmFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, oldUdpnmFilter, newUdpnmFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUdpnmFilter(UDPNMFilter newUdpnmFilter) {
		if (newUdpnmFilter != udpnmFilter) {
			NotificationChain msgs = null;
			if (udpnmFilter != null)
				msgs = ((InternalEObject)udpnmFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, null, msgs);
			if (newUdpnmFilter != null)
				msgs = ((InternalEObject)newUdpnmFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, null, msgs);
			msgs = basicSetUdpnmFilter(newUdpnmFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, newUdpnmFilter, newUdpnmFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidUDPFilter(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UDPNM_MESSAGE__IS_VALID_HAS_VALID_UDP_FILTER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidUDPFilter", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
				return basicSetUdpnmFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
				return getUdpnmFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
				setUdpnmFilter((UDPNMFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
				setUdpnmFilter((UDPNMFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
				return udpnmFilter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.UDPNM_MESSAGE___IS_VALID_HAS_VALID_UDP_FILTER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidUDPFilter((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //UDPNMMessageImpl
