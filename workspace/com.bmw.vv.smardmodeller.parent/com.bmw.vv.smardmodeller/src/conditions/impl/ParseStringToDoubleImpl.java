/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.IComputeVariableActionOperand;
import conditions.IOperand;
import conditions.IOperation;
import conditions.ISignalComparisonExpressionOperand;
import conditions.IStringOperand;
import conditions.ParseStringToDouble;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parse String To Double</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.ParseStringToDoubleImpl#getStringToParse <em>String To Parse</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParseStringToDoubleImpl extends MinimalEObjectImpl.Container implements ParseStringToDouble {
	/**
	 * The cached value of the '{@link #getStringToParse() <em>String To Parse</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToParse()
	 * @generated
	 * @ordered
	 */
	protected IStringOperand stringToParse;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ParseStringToDoubleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getParseStringToDouble();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IStringOperand getStringToParse() {
		return stringToParse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringToParse(IStringOperand newStringToParse, NotificationChain msgs) {
		IStringOperand oldStringToParse = stringToParse;
		stringToParse = newStringToParse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, oldStringToParse, newStringToParse);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringToParse(IStringOperand newStringToParse) {
		if (newStringToParse != stringToParse) {
			NotificationChain msgs = null;
			if (stringToParse != null)
				msgs = ((InternalEObject)stringToParse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, null, msgs);
			if (newStringToParse != null)
				msgs = ((InternalEObject)newStringToParse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, null, msgs);
			msgs = basicSetStringToParse(newStringToParse, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, newStringToParse, newStringToParse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
				return basicSetStringToParse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
				return getStringToParse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
				setStringToParse((IStringOperand)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
				setStringToParse((IStringOperand)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
				return stringToParse != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IComputeVariableActionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == ISignalComparisonExpressionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == IOperation.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERATION___GET_OPERAND_DATA_TYPE: return ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_OPERAND_DATA_TYPE;
				case ConditionsPackage.IOPERATION___GET_OPERANDS: return ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_OPERANDS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_OPERANDS:
				return get_Operands();
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_READ_VARIABLES:
				return getReadVariables();
			case ConditionsPackage.PARSE_STRING_TO_DOUBLE___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ParseStringToDoubleImpl
