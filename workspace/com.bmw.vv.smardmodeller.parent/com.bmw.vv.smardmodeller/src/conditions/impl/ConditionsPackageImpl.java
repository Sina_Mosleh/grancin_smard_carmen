/**
 */
package conditions.impl;

import conditions.ConditionsFactory;
import conditions.ConditionsPackage;

import conditions.util.ConditionsValidator;

import java.io.IOException;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import statemachine.StatemachinePackage;

import statemachine.impl.StatemachinePackageImpl;

import statemachineset.StatemachinesetPackage;

import statemachineset.impl.StatemachinesetPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConditionsPackageImpl extends EPackageImpl implements ConditionsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected String packageFilename = "conditions.ecore";

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass documentRootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalComparisonExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass canMessageCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linMessageCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stateCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timingExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass trueExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logicalExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass referenceConditionExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transitionCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flexRayMessageCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stopExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass comparatorSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantComparatorValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass calculationExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionsDocumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableFormatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractObserverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass observerValueRangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalObserverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalReferenceSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass signalReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iSignalOrReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bitPatternComparatorValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iOperandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iComputeVariableActionOperandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iStringOperandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iSignalComparisonExpressionOperandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iStringOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iNumericOperandEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iNumericOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueVariableObserverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parseStringToDoubleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass matchesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extractEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass substringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parseNumericToStringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringLengthEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass someIPMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containerSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ethernetFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass udpFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tcpFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPv4FilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass someIPFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forEachExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass messageCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass someIPSDFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass someIPSDMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decodeStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleDecodeStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass extractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass universalPayloadExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emptyExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass canFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flexRayFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dltFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass udpnmFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass canMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass flexRayMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dltMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass udpMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tcpMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass udpnmMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass verboseDLTMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass universalPayloadWithLegacyExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractBusMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginStateExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginResultExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emptyDecodeStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pluginCheckExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseClassWithIDEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass headerSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVariableReaderWriterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iStateTransitionReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tpFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass baseClassWithSourceReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sourceReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ethernetMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iPv4MessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonVerboseDLTMessageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringDecodeStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonVerboseDLTFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass verboseDLTExtractStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regexOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass payloadFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass verboseDLTPayloadFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableStructureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computedVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum checkTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum comparatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum logicalOperatorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signalDataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum flexChannelTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum byteOrderTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum formatDataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum flexRayHeaderFlagSelectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum booleanOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum protocolTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ethernetProtocolTypeSelectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rxTxFlagTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum observerValueTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pluginStateValueTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum analysisTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPMessageTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPMethodTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPReturnCodeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPSDFlagsOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPSDTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum canExtIdentifierOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dlT_MessageTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dlT_MessageTraceInfoEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dlT_MessageLogInfoEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dlT_MessageControlInfoEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dlT_MessageBusInfoEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum forEachExpressionModifierTemplateEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum autosarDataTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum canFrameTypeOrTemplatePlaceholderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum evaluationBehaviourOrTemplateDefinedEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum signalVariableLagEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ttlOrTemplatePlaceHolderEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType checkTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType comparatorObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType logicalOperatorTypeObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType hexOrIntOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType intOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType longOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType doubleOrHexOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ipPatternOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType macPatternOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType portPatternOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType vlanIdPatternOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType ipOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType portOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType vlanIdOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType macOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType bitmaskOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType hexOrIntOrNullOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType nonZeroDoubleOrTemplatePlaceholderEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType byteOrTemplatePlaceholderEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see conditions.ConditionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ConditionsPackageImpl() {
		super(eNS_URI, ConditionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ConditionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @generated
	 */
	public static ConditionsPackage init() {
		if (isInited) return (ConditionsPackage)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredConditionsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ConditionsPackageImpl theConditionsPackage = registeredConditionsPackage instanceof ConditionsPackageImpl ? (ConditionsPackageImpl)registeredConditionsPackage : new ConditionsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);
		StatemachinesetPackageImpl theStatemachinesetPackage = (StatemachinesetPackageImpl)(registeredPackage instanceof StatemachinesetPackageImpl ? registeredPackage : StatemachinesetPackage.eINSTANCE);
		registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);
		StatemachinePackageImpl theStatemachinePackage = (StatemachinePackageImpl)(registeredPackage instanceof StatemachinePackageImpl ? registeredPackage : StatemachinePackage.eINSTANCE);

		// Load packages
		theConditionsPackage.loadPackage();

		// Create package meta-data objects
		theStatemachinesetPackage.createPackageContents();
		theStatemachinePackage.createPackageContents();

		// Initialize created meta-data
		theStatemachinesetPackage.initializePackageContents();
		theStatemachinePackage.initializePackageContents();

		// Fix loaded packages
		theConditionsPackage.fixPackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theConditionsPackage,
			 new EValidator.Descriptor() {
				 @Override
				 public EValidator getEValidator() {
					 return ConditionsValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theConditionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ConditionsPackage.eNS_URI, theConditionsPackage);
		return theConditionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDocumentRoot() {
		if (documentRootEClass == null) {
			documentRootEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(0);
		}
		return documentRootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute)getDocumentRoot().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_ConditionSet() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getDocumentRoot_ConditionsDocument() {
        return (EReference)getDocumentRoot().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConditionSet() {
		if (conditionSetEClass == null) {
			conditionSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(1);
		}
		return conditionSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Group() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Baureihe() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Description() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Istufe() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Schemaversion() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Uuid() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Version() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConditionSet_Conditions() {
        return (EReference)getConditionSet().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConditionSet_Name() {
        return (EAttribute)getConditionSet().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCondition() {
		if (conditionEClass == null) {
			conditionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(6);
		}
		return conditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCondition_Name() {
        return (EAttribute)getCondition().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCondition_Description() {
        return (EAttribute)getCondition().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCondition_Expression() {
        return (EReference)getCondition().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCondition__IsValid_hasValidName__DiagnosticChain_Map() {
        return getCondition().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCondition__IsValid_hasValidDescription__DiagnosticChain_Map() {
        return getCondition().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSignalComparisonExpression() {
		if (signalComparisonExpressionEClass == null) {
			signalComparisonExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(7);
		}
		return signalComparisonExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSignalComparisonExpression_ComparatorSignal() {
        return (EReference)getSignalComparisonExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSignalComparisonExpression_Operator() {
        return (EAttribute)getSignalComparisonExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSignalComparisonExpression_OperatorTmplParam() {
        return (EAttribute)getSignalComparisonExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSignalComparisonExpression__IsValid_hasValidComparator__DiagnosticChain_Map() {
        return getSignalComparisonExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSignalComparisonExpression__IsValid_hasValidOperands__DiagnosticChain_Map() {
        return getSignalComparisonExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCanMessageCheckExpression() {
		if (canMessageCheckExpressionEClass == null) {
			canMessageCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(8);
		}
		return canMessageCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_Busid() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_Type() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_MessageIDs() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_TypeTmplParam() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_BusidTmplParam() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_RxtxFlag() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_RxtxFlagTypeTmplParam() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_ExtIdentifier() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCanMessageCheckExpression_ExtIdentifierTmplParam() {
        return (EAttribute)getCanMessageCheckExpression().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCanMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map() {
        return getCanMessageCheckExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCanMessageCheckExpression__IsValid_hasValidMessageIdRange__DiagnosticChain_Map() {
        return getCanMessageCheckExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCanMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map() {
        return getCanMessageCheckExpression().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCanMessageCheckExpression__IsValid_hasValidRxTxFlag__DiagnosticChain_Map() {
        return getCanMessageCheckExpression().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCanMessageCheckExpression__IsValid_hasValidExtIdentifier__DiagnosticChain_Map() {
        return getCanMessageCheckExpression().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLinMessageCheckExpression() {
		if (linMessageCheckExpressionEClass == null) {
			linMessageCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(9);
		}
		return linMessageCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLinMessageCheckExpression_Busid() {
        return (EAttribute)getLinMessageCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLinMessageCheckExpression_Type() {
        return (EAttribute)getLinMessageCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLinMessageCheckExpression_MessageIDs() {
        return (EAttribute)getLinMessageCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLinMessageCheckExpression_TypeTmplParam() {
        return (EAttribute)getLinMessageCheckExpression().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLinMessageCheckExpression_BusidTmplParam() {
        return (EAttribute)getLinMessageCheckExpression().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLinMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map() {
        return getLinMessageCheckExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLinMessageCheckExpression__IsValid_hasValidMessageIdRange__DiagnosticChain_Map() {
        return getLinMessageCheckExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLinMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map() {
        return getLinMessageCheckExpression().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStateCheckExpression() {
		if (stateCheckExpressionEClass == null) {
			stateCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(10);
		}
		return stateCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStateCheckExpression_CheckStateActive() {
        return (EAttribute)getStateCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStateCheckExpression_CheckState() {
        return (EReference)getStateCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStateCheckExpression_CheckStateActiveTmplParam() {
        return (EAttribute)getStateCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateCheckExpression__IsValid_hasValidStatesActive__DiagnosticChain_Map() {
        return getStateCheckExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getStateCheckExpression__IsValid_hasValidState__DiagnosticChain_Map() {
        return getStateCheckExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTimingExpression() {
		if (timingExpressionEClass == null) {
			timingExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(11);
		}
		return timingExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimingExpression_Maxtime() {
        return (EAttribute)getTimingExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTimingExpression_Mintime() {
        return (EAttribute)getTimingExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTimingExpression__IsValid_hasValidMinMaxTimes__DiagnosticChain_Map() {
        return getTimingExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTrueExpression() {
		if (trueExpressionEClass == null) {
			trueExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(12);
		}
		return trueExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLogicalExpression() {
		if (logicalExpressionEClass == null) {
			logicalExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(13);
		}
		return logicalExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLogicalExpression_Expressions() {
        return (EReference)getLogicalExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLogicalExpression_Operator() {
        return (EAttribute)getLogicalExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLogicalExpression_OperatorTmplParam() {
        return (EAttribute)getLogicalExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLogicalExpression__IsValid_hasValidOperator__DiagnosticChain_Map() {
        return getLogicalExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLogicalExpression__IsValid_hasValidSubExpressions__DiagnosticChain_Map() {
        return getLogicalExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getReferenceConditionExpression() {
		if (referenceConditionExpressionEClass == null) {
			referenceConditionExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(14);
		}
		return referenceConditionExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getReferenceConditionExpression_Condition() {
        return (EReference)getReferenceConditionExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getReferenceConditionExpression__IsValid_hasValidReferencedCondition__DiagnosticChain_Map() {
        return getReferenceConditionExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTransitionCheckExpression() {
		if (transitionCheckExpressionEClass == null) {
			transitionCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(15);
		}
		return transitionCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTransitionCheckExpression_StayActive() {
        return (EAttribute)getTransitionCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTransitionCheckExpression_CheckTransition() {
        return (EReference)getTransitionCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTransitionCheckExpression_StayActiveTmplParam() {
        return (EAttribute)getTransitionCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTransitionCheckExpression__IsValid_hasValidStayActive__DiagnosticChain_Map() {
        return getTransitionCheckExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTransitionCheckExpression__IsValid_hasValidTransition__DiagnosticChain_Map() {
        return getTransitionCheckExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFlexRayMessageCheckExpression() {
		if (flexRayMessageCheckExpressionEClass == null) {
			flexRayMessageCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(16);
		}
		return flexRayMessageCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_BusId() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_PayloadPreamble() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_ZeroFrame() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_SyncFrame() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_StartupFrame() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_NetworkMgmt() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_FlexrayMessageId() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_PayloadPreambleTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_ZeroFrameTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_SyncFrameTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_StartupFrameTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_NetworkMgmtTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_BusIdTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_Type() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayMessageCheckExpression_TypeTmplParam() {
        return (EAttribute)getFlexRayMessageCheckExpression().getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidStartupFrame__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidSyncFrame__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidZeroFrame__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidPayloadPreamble__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidNetworkMgmt__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayMessageCheckExpression__IsValid_hasValidMessageId__DiagnosticChain_Map() {
        return getFlexRayMessageCheckExpression().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStopExpression() {
		if (stopExpressionEClass == null) {
			stopExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(17);
		}
		return stopExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStopExpression_AnalyseArt() {
        return (EAttribute)getStopExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStopExpression_AnalyseArtTmplParam() {
        return (EAttribute)getStopExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComparatorSignal() {
		if (comparatorSignalEClass == null) {
			comparatorSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(21);
		}
		return comparatorSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComparatorSignal_Description() {
        return (EAttribute)getComparatorSignal().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConstantComparatorValue() {
		if (constantComparatorValueEClass == null) {
			constantComparatorValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(22);
		}
		return constantComparatorValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConstantComparatorValue_Value() {
        return (EAttribute)getConstantComparatorValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConstantComparatorValue_InterpretedValue() {
        return (EAttribute)getConstantComparatorValue().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getConstantComparatorValue_InterpretedValueTmplParam() {
        return (EAttribute)getConstantComparatorValue().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getConstantComparatorValue__IsValid_hasValidInterpretedValue__DiagnosticChain_Map() {
        return getConstantComparatorValue().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getConstantComparatorValue__IsValid_hasValidValue__DiagnosticChain_Map() {
        return getConstantComparatorValue().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCalculationExpression() {
		if (calculationExpressionEClass == null) {
			calculationExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(23);
		}
		return calculationExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCalculationExpression_Expression() {
        return (EAttribute)getCalculationExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCalculationExpression_Operands() {
        return (EReference)getCalculationExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCalculationExpression__IsValid_hasValidExpression__DiagnosticChain_Map() {
        return getCalculationExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCalculationExpression__IsValid_hasValidOperands__DiagnosticChain_Map() {
        return getCalculationExpression().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExpression() {
		if (expressionEClass == null) {
			expressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(24);
		}
		return expressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExpression_Description() {
        return (EAttribute)getExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getExpression__IsValid_hasValidDescription__DiagnosticChain_Map() {
        return getExpression().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariableReference() {
		if (variableReferenceEClass == null) {
			variableReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(27);
		}
		return variableReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariableReference_Variable() {
        return (EReference)getVariableReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getConditionsDocument() {
		if (conditionsDocumentEClass == null) {
			conditionsDocumentEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(28);
		}
		return conditionsDocumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConditionsDocument_SignalReferencesSet() {
        return (EReference)getConditionsDocument().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConditionsDocument_VariableSet() {
        return (EReference)getConditionsDocument().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getConditionsDocument_ConditionSet() {
        return (EReference)getConditionsDocument().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariableSet() {
		if (variableSetEClass == null) {
			variableSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(29);
		}
		return variableSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariableSet_Uuid() {
        return (EAttribute)getVariableSet().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariableSet_Variables() {
        return (EReference)getVariableSet().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariableSet__IsValid_hasValidValueRanges__DiagnosticChain_Map() {
        return getVariableSet().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariable() {
		if (variableEClass == null) {
			variableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(30);
		}
		return variableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariable_DataType() {
        return (EAttribute)getVariable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariable_Unit() {
        return (EAttribute)getVariable().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariable_VariableFormat() {
        return (EReference)getVariable().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariable_VariableFormatTmplParam() {
        return (EAttribute)getVariable().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariable__IsValid_hasValidInterpretedValue__DiagnosticChain_Map() {
        return getVariable().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariable__IsValid_hasValidType__DiagnosticChain_Map() {
        return getVariable().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariable__IsValid_hasValidUnit__DiagnosticChain_Map() {
        return getVariable().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariable__IsValid_hasValidVariableFormatTmplParam__DiagnosticChain_Map() {
        return getVariable().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariable__GetObservers() {
        return getVariable().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSignalVariable() {
		if (signalVariableEClass == null) {
			signalVariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(31);
		}
		return signalVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSignalVariable_InterpretedValue() {
        return (EAttribute)getSignalVariable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSignalVariable_Signal() {
        return (EReference)getSignalVariable().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSignalVariable_InterpretedValueTmplParam() {
        return (EAttribute)getSignalVariable().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSignalVariable_SignalObservers() {
        return (EReference)getSignalVariable().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSignalVariable_Lag() {
        return (EAttribute)getSignalVariable().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getValueVariable() {
		if (valueVariableEClass == null) {
			valueVariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(32);
		}
		return valueVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getValueVariable_InitialValue() {
        return (EAttribute)getValueVariable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getValueVariable_ValueVariableObservers() {
        return (EReference)getValueVariable().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariableFormat() {
		if (variableFormatEClass == null) {
			variableFormatEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(33);
		}
		return variableFormatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariableFormat_Digits() {
        return (EAttribute)getVariableFormat().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariableFormat_BaseDataType() {
        return (EAttribute)getVariableFormat().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVariableFormat_UpperCase() {
        return (EAttribute)getVariableFormat().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVariableFormat__IsValid_hasValidDigits__DiagnosticChain_Map() {
        return getVariableFormat().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractObserver() {
		if (abstractObserverEClass == null) {
			abstractObserverEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(35);
		}
		return abstractObserverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractObserver_Name() {
        return (EAttribute)getAbstractObserver().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractObserver_Description() {
        return (EAttribute)getAbstractObserver().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractObserver_LogLevel() {
        return (EAttribute)getAbstractObserver().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractObserver_ValueRanges() {
        return (EReference)getAbstractObserver().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractObserver_ValueRangesTmplParam() {
        return (EAttribute)getAbstractObserver().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractObserver_ActiveAtStart() {
        return (EAttribute)getAbstractObserver().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractObserver__IsValid_hasValidName__DiagnosticChain_Map() {
        return getAbstractObserver().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractObserver__IsValid_hasValidLogLevel__DiagnosticChain_Map() {
        return getAbstractObserver().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractObserver__IsValid_hasValidValueRanges__EList_EList() {
        return getAbstractObserver().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractObserver__IsValid_hasValidValueRangesTmplParam__DiagnosticChain_Map() {
        return getAbstractObserver().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getObserverValueRange() {
		if (observerValueRangeEClass == null) {
			observerValueRangeEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(36);
		}
		return observerValueRangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getObserverValueRange_Value() {
        return (EAttribute)getObserverValueRange().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getObserverValueRange_Description() {
        return (EAttribute)getObserverValueRange().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getObserverValueRange_ValueType() {
        return (EAttribute)getObserverValueRange().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getObserverValueRange__IsValid_hasValidValue__DiagnosticChain_Map() {
        return getObserverValueRange().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getObserverValueRange__IsValid_hasValidDescription__DiagnosticChain_Map() {
        return getObserverValueRange().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSignalObserver() {
		if (signalObserverEClass == null) {
			signalObserverEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(37);
		}
		return signalObserverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSignalReferenceSet() {
		if (signalReferenceSetEClass == null) {
			signalReferenceSetEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(38);
		}
		return signalReferenceSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSignalReferenceSet_Messages() {
        return (EReference)getSignalReferenceSet().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSignalReference() {
		if (signalReferenceEClass == null) {
			signalReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(39);
		}
		return signalReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSignalReference_Signal() {
        return (EReference)getSignalReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSignalReference__IsValid_hasValidSignal__DiagnosticChain_Map() {
        return getSignalReference().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISignalOrReference() {
		if (iSignalOrReferenceEClass == null) {
			iSignalOrReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(40);
		}
		return iSignalOrReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBitPatternComparatorValue() {
		if (bitPatternComparatorValueEClass == null) {
			bitPatternComparatorValueEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(48);
		}
		return bitPatternComparatorValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBitPatternComparatorValue_Value() {
        return (EAttribute)getBitPatternComparatorValue().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBitPatternComparatorValue_Startbit() {
        return (EAttribute)getBitPatternComparatorValue().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getBitPatternComparatorValue__IsValid_hasValidBitPattern__DiagnosticChain_Map() {
        return getBitPatternComparatorValue().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getBitPatternComparatorValue__IsValid_hasValidStartbit__DiagnosticChain_Map() {
        return getBitPatternComparatorValue().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNotExpression() {
		if (notExpressionEClass == null) {
			notExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(73);
		}
		return notExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNotExpression_Expression() {
        return (EReference)getNotExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNotExpression_Operator() {
        return (EAttribute)getNotExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIOperand() {
		if (iOperandEClass == null) {
			iOperandEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(75);
		}
		return iOperandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIOperand__Get_EvaluationDataType() {
        return getIOperand().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIComputeVariableActionOperand() {
		if (iComputeVariableActionOperandEClass == null) {
			iComputeVariableActionOperandEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(76);
		}
		return iComputeVariableActionOperandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIStringOperand() {
		if (iStringOperandEClass == null) {
			iStringOperandEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(77);
		}
		return iStringOperandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getISignalComparisonExpressionOperand() {
		if (iSignalComparisonExpressionOperandEClass == null) {
			iSignalComparisonExpressionOperandEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(78);
		}
		return iSignalComparisonExpressionOperandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIOperation() {
		if (iOperationEClass == null) {
			iOperationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(79);
		}
		return iOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIOperation__Get_OperandDataType() {
        return getIOperation().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIOperation__Get_Operands() {
        return getIOperation().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIStringOperation() {
		if (iStringOperationEClass == null) {
			iStringOperationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(80);
		}
		return iStringOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINumericOperand() {
		if (iNumericOperandEClass == null) {
			iNumericOperandEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(81);
		}
		return iNumericOperandEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getINumericOperation() {
		if (iNumericOperationEClass == null) {
			iNumericOperationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(82);
		}
		return iNumericOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getValueVariableObserver() {
		if (valueVariableObserverEClass == null) {
			valueVariableObserverEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(83);
		}
		return valueVariableObserverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParseStringToDouble() {
		if (parseStringToDoubleEClass == null) {
			parseStringToDoubleEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(89);
		}
		return parseStringToDoubleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParseStringToDouble_StringToParse() {
        return (EReference)getParseStringToDouble().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringExpression() {
		if (stringExpressionEClass == null) {
			stringExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(90);
		}
		return stringExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMatches() {
		if (matchesEClass == null) {
			matchesEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(91);
		}
		return matchesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMatches_StringToCheck() {
        return (EReference)getMatches().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getMatches__IsValid_hasValidStringToCheck__DiagnosticChain_Map() {
        return getMatches().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExtract() {
		if (extractEClass == null) {
			extractEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(92);
		}
		return extractEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getExtract_StringToExtract() {
        return (EReference)getExtract().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getExtract_GroupIndex() {
        return (EAttribute)getExtract().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSubstring() {
		if (substringEClass == null) {
			substringEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(93);
		}
		return substringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSubstring_StringToExtract() {
        return (EReference)getSubstring().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubstring_StartIndex() {
        return (EAttribute)getSubstring().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSubstring_EndIndex() {
        return (EAttribute)getSubstring().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getParseNumericToString() {
		if (parseNumericToStringEClass == null) {
			parseNumericToStringEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(94);
		}
		return parseNumericToStringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getParseNumericToString_NumericOperandToBeParsed() {
        return (EReference)getParseNumericToString().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringLength() {
		if (stringLengthEClass == null) {
			stringLengthEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(95);
		}
		return stringLengthEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getStringLength_StringOperand() {
        return (EReference)getStringLength().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractMessage() {
		if (abstractMessageEClass == null) {
			abstractMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(98);
		}
		return abstractMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractMessage_Signals() {
        return (EReference)getAbstractMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractMessage_Name() {
        return (EAttribute)getAbstractMessage().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractMessage__GetPrimaryFilter() {
        return getAbstractMessage().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractMessage__GetFilters__boolean() {
        return getAbstractMessage().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSomeIPMessage() {
		if (someIPMessageEClass == null) {
			someIPMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(99);
		}
		return someIPMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSomeIPMessage_SomeIPFilter() {
        return (EReference)getSomeIPMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractSignal() {
		if (abstractSignalEClass == null) {
			abstractSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(100);
		}
		return abstractSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractSignal_Name() {
        return (EAttribute)getAbstractSignal().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractSignal_DecodeStrategy() {
        return (EReference)getAbstractSignal().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractSignal_ExtractStrategy() {
        return (EReference)getAbstractSignal().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractSignal__GetMessage() {
        return getAbstractSignal().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getContainerSignal() {
		if (containerSignalEClass == null) {
			containerSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(101);
		}
		return containerSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getContainerSignal_ContainedSignals() {
        return (EReference)getContainerSignal().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractFilter() {
		if (abstractFilterEClass == null) {
			abstractFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(102);
		}
		return abstractFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractFilter_PayloadLength() {
        return (EAttribute)getAbstractFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEthernetFilter() {
		if (ethernetFilterEClass == null) {
			ethernetFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(103);
		}
		return ethernetFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_SourceMAC() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_DestMAC() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_EtherType() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_InnerVlanId() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_OuterVlanId() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_InnerVlanCFI() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_OuterVlanCFI() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_InnerVlanVID() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_OuterVlanVID() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_CRC() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_InnerVlanTPID() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getEthernetFilter_OuterVlanTPID() {
        return (EAttribute)getEthernetFilter().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidSourceMAC__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidDestinationMAC__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidCRC__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidEtherType__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidInnerVlanVid__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidOuterVlanVid__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidInnerVlanCFI__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidOuterVlanCFI__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidInnerVlanPCP__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidOuterVlanPCP__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidInnerVlanTPID__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getEthernetFilter__IsValid_hasValidOuterVlanTPID__DiagnosticChain_Map() {
        return getEthernetFilter().getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUDPFilter() {
		if (udpFilterEClass == null) {
			udpFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(104);
		}
		return udpFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUDPFilter_Checksum() {
        return (EAttribute)getUDPFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPFilter__IsValid_hasValidSourcePort__DiagnosticChain_Map() {
        return getUDPFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPFilter__IsValid_hasValidDestinationPort__DiagnosticChain_Map() {
        return getUDPFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPFilter__IsValid_hasValidChecksum__DiagnosticChain_Map() {
        return getUDPFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTCPFilter() {
		if (tcpFilterEClass == null) {
			tcpFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(105);
		}
		return tcpFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_SequenceNumber() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_AcknowledgementNumber() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_TcpFlags() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_StreamAnalysisFlags() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_StreamAnalysisFlagsTmplParam() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTCPFilter_StreamValidPayloadOffset() {
        return (EAttribute)getTCPFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidSourcePort__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidDestinationPort__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidSequenceNumber__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidAcknowledgementNumber__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidTcpFlag__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidStreamAnalysisFlag__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getTCPFilter__IsValid_hasValidStreamValidPayloadOffset__DiagnosticChain_Map() {
        return getTCPFilter().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIPv4Filter() {
		if (iPv4FilterEClass == null) {
			iPv4FilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(106);
		}
		return iPv4FilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIPv4Filter_ProtocolType() {
        return (EAttribute)getIPv4Filter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIPv4Filter_ProtocolTypeTmplParam() {
        return (EAttribute)getIPv4Filter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIPv4Filter_SourceIP() {
        return (EAttribute)getIPv4Filter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIPv4Filter_DestIP() {
        return (EAttribute)getIPv4Filter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getIPv4Filter_TimeToLive() {
        return (EAttribute)getIPv4Filter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIPv4Filter__IsValid_hasValidSourceIpAddress__DiagnosticChain_Map() {
        return getIPv4Filter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIPv4Filter__IsValid_hasValidDestinationIpAddress__DiagnosticChain_Map() {
        return getIPv4Filter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIPv4Filter__IsValid_hasValidTimeToLive__DiagnosticChain_Map() {
        return getIPv4Filter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSomeIPFilter() {
		if (someIPFilterEClass == null) {
			someIPFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(107);
		}
		return someIPFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_ServiceId() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_MethodType() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_MethodTypeTmplParam() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_MethodId() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_ClientId() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_SessionId() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_ProtocolVersion() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_InterfaceVersion() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_MessageType() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_MessageTypeTmplParam() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_ReturnCode() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_ReturnCodeTmplParam() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPFilter_SomeIPLength() {
        return (EAttribute)getSomeIPFilter().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidServiceId__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidMethodId__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidSessionId__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidClientId__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidLength__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidProtocolVersion__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidInterfaceVersion__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidMessageType__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidReturnCode__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPFilter__IsValid_hasValidMethodType__DiagnosticChain_Map() {
        return getSomeIPFilter().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getForEachExpression() {
		if (forEachExpressionEClass == null) {
			forEachExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(108);
		}
		return forEachExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getForEachExpression_FilterExpressions() {
        return (EReference)getForEachExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getForEachExpression_ContainerSignal() {
        return (EReference)getForEachExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getMessageCheckExpression() {
		if (messageCheckExpressionEClass == null) {
			messageCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(110);
		}
		return messageCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getMessageCheckExpression_Message() {
        return (EReference)getMessageCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSomeIPSDFilter() {
		if (someIPSDFilterEClass == null) {
			someIPSDFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(111);
		}
		return someIPSDFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_Flags() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_FlagsTmplParam() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_SdType() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_SdTypeTmplParam() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_InstanceId() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_Ttl() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_MajorVersion() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_MinorVersion() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_EventGroupId() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_IndexFirstOption() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_IndexSecondOption() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_NumberFirstOption() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_NumberSecondOption() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSomeIPSDFilter_ServiceId_SomeIPSD() {
        return (EAttribute)getSomeIPSDFilter().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidInstanceId__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidEventGroupId__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidFlags__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidSdType__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidMajorVersion__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidMinorVersion__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidIndexFirstOption__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidIndexSecondOption__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidNumberFirstOption__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getSomeIPSDFilter__IsValid_hasValidNumberSecondOption__DiagnosticChain_Map() {
        return getSomeIPSDFilter().getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSomeIPSDMessage() {
		if (someIPSDMessageEClass == null) {
			someIPSDMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(112);
		}
		return someIPSDMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getSomeIPSDMessage_SomeIPSDFilter() {
        return (EReference)getSomeIPSDMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDoubleSignal() {
		if (doubleSignalEClass == null) {
			doubleSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(113);
		}
		return doubleSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDoubleSignal_IgnoreInvalidValues() {
        return (EAttribute)getDoubleSignal().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDoubleSignal_IgnoreInvalidValuesTmplParam() {
        return (EAttribute)getDoubleSignal().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringSignal() {
		if (stringSignalEClass == null) {
			stringSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(114);
		}
		return stringSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDecodeStrategy() {
		if (decodeStrategyEClass == null) {
			decodeStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(115);
		}
		return decodeStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDoubleDecodeStrategy() {
		if (doubleDecodeStrategyEClass == null) {
			doubleDecodeStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(116);
		}
		return doubleDecodeStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDoubleDecodeStrategy_Factor() {
        return (EAttribute)getDoubleDecodeStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDoubleDecodeStrategy_Offset() {
        return (EAttribute)getDoubleDecodeStrategy().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDoubleDecodeStrategy__IsValid_hasValidFactor__DiagnosticChain_Map() {
        return getDoubleDecodeStrategy().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDoubleDecodeStrategy__IsValid_hasValidOffset__DiagnosticChain_Map() {
        return getDoubleDecodeStrategy().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getExtractStrategy() {
		if (extractStrategyEClass == null) {
			extractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(117);
		}
		return extractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getExtractStrategy_AbstractSignal() {
        return (EReference)getExtractStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUniversalPayloadExtractStrategy() {
		if (universalPayloadExtractStrategyEClass == null) {
			universalPayloadExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(118);
		}
		return universalPayloadExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadExtractStrategy_ExtractionRule() {
        return (EAttribute)getUniversalPayloadExtractStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadExtractStrategy_ByteOrder() {
        return (EAttribute)getUniversalPayloadExtractStrategy().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam() {
        return (EAttribute)getUniversalPayloadExtractStrategy().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadExtractStrategy_SignalDataType() {
        return (EAttribute)getUniversalPayloadExtractStrategy().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam() {
        return (EAttribute)getUniversalPayloadExtractStrategy().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUniversalPayloadExtractStrategy__IsValid_hasValidExtractString__DiagnosticChain_Map() {
        return getUniversalPayloadExtractStrategy().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUniversalPayloadExtractStrategy__IsValid_hasValidSignalDataType__DiagnosticChain_Map() {
        return getUniversalPayloadExtractStrategy().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEmptyExtractStrategy() {
		if (emptyExtractStrategyEClass == null) {
			emptyExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(119);
		}
		return emptyExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCANFilter() {
		if (canFilterEClass == null) {
			canFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(121);
		}
		return canFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_MessageIdRange() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_FrameId() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_RxtxFlag() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_RxtxFlagTmplParam() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_ExtIdentifier() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_ExtIdentifierTmplParam() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_FrameType() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getCANFilter_FrameTypeTmplParam() {
        return (EAttribute)getCANFilter().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCANFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map() {
        return getCANFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCANFilter__IsValid_hasValidRxTxFlag__DiagnosticChain_Map() {
        return getCANFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getCANFilter__IsValid_hasValidExtIdentifier__DiagnosticChain_Map() {
        return getCANFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLINFilter() {
		if (linFilterEClass == null) {
			linFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(122);
		}
		return linFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINFilter_MessageIdRange() {
        return (EAttribute)getLINFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getLINFilter_FrameId() {
        return (EAttribute)getLINFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getLINFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map() {
        return getLINFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFlexRayFilter() {
		if (flexRayFilterEClass == null) {
			flexRayFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(123);
		}
		return flexRayFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_MessageIdRange() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_SlotId() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_CycleOffset() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_CycleRepetition() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_Channel() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getFlexRayFilter_ChannelTmplParam() {
        return (EAttribute)getFlexRayFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map() {
        return getFlexRayFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayFilter__IsValid_hasValidChannelType__DiagnosticChain_Map() {
        return getFlexRayFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayFilter__IsValid_hasValidCycleOffset__DiagnosticChain_Map() {
        return getFlexRayFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getFlexRayFilter__IsValid_hasValidCycleRepeatInterval__DiagnosticChain_Map() {
        return getFlexRayFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDLTFilter() {
		if (dltFilterEClass == null) {
			dltFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(124);
		}
		return dltFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_EcuID_ECU() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_SessionID_SEID() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_ApplicationID_APID() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_ContextID_CTID() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageType_MSTP() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageLogInfo_MSLI() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageTraceInfo_MSTI() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageBusInfo_MSBI() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageControlInfo_MSCI() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageLogInfo_MSLITmplParam() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageTraceInfo_MSTITmplParam() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageBusInfo_MSBITmplParam() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageControlInfo_MSCITmplParam() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getDLTFilter_MessageType_MSTPTmplParam() {
        return (EAttribute)getDLTFilter().getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDLTFilter__IsValid_hasValidMessageBusInfo__DiagnosticChain_Map() {
        return getDLTFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDLTFilter__IsValid_hasValidMessageControlInfo__DiagnosticChain_Map() {
        return getDLTFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDLTFilter__IsValid_hasValidMessageLogInfo__DiagnosticChain_Map() {
        return getDLTFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDLTFilter__IsValid_hasValidMessageTraceInfo__DiagnosticChain_Map() {
        return getDLTFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getDLTFilter__IsValid_hasValidMessageType__DiagnosticChain_Map() {
        return getDLTFilter().getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUDPNMFilter() {
		if (udpnmFilterEClass == null) {
			udpnmFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(125);
		}
		return udpnmFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUDPNMFilter_SourceNodeIdentifier() {
        return (EAttribute)getUDPNMFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUDPNMFilter_ControlBitVector() {
        return (EAttribute)getUDPNMFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUDPNMFilter_PwfStatus() {
        return (EAttribute)getUDPNMFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUDPNMFilter_TeilnetzStatus() {
        return (EAttribute)getUDPNMFilter().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPNMFilter__IsValid_hasValidSourceNodeIdentifier__DiagnosticChain_Map() {
        return getUDPNMFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPNMFilter__IsValid_hasValidControlBitVector__DiagnosticChain_Map() {
        return getUDPNMFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPNMFilter__IsValid_hasValidPwfStatus__DiagnosticChain_Map() {
        return getUDPNMFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPNMFilter__IsValid_hasValidTeilnetzStatus__DiagnosticChain_Map() {
        return getUDPNMFilter().getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getCANMessage() {
		if (canMessageEClass == null) {
			canMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(126);
		}
		return canMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getCANMessage_CanFilter() {
        return (EReference)getCANMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getLINMessage() {
		if (linMessageEClass == null) {
			linMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(127);
		}
		return linMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getLINMessage_LinFilter() {
        return (EReference)getLINMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getFlexRayMessage() {
		if (flexRayMessageEClass == null) {
			flexRayMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(128);
		}
		return flexRayMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getFlexRayMessage_FlexRayFilter() {
        return (EReference)getFlexRayMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getDLTMessage() {
		if (dltMessageEClass == null) {
			dltMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(129);
		}
		return dltMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUDPMessage() {
		if (udpMessageEClass == null) {
			udpMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(130);
		}
		return udpMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUDPMessage_UdpFilter() {
        return (EReference)getUDPMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTCPMessage() {
		if (tcpMessageEClass == null) {
			tcpMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(131);
		}
		return tcpMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTCPMessage_TcpFilter() {
        return (EReference)getTCPMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUDPNMMessage() {
		if (udpnmMessageEClass == null) {
			udpnmMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(132);
		}
		return udpnmMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getUDPNMMessage_UdpnmFilter() {
        return (EReference)getUDPNMMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUDPNMMessage__IsValid_hasValidUDPFilter__DiagnosticChain_Map() {
        return getUDPNMMessage().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVerboseDLTMessage() {
		if (verboseDLTMessageEClass == null) {
			verboseDLTMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(133);
		}
		return verboseDLTMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVerboseDLTMessage_DltFilter() {
        return (EReference)getVerboseDLTMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getUniversalPayloadWithLegacyExtractStrategy() {
		if (universalPayloadWithLegacyExtractStrategyEClass == null) {
			universalPayloadWithLegacyExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(134);
		}
		return universalPayloadWithLegacyExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadWithLegacyExtractStrategy_StartBit() {
        return (EAttribute)getUniversalPayloadWithLegacyExtractStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getUniversalPayloadWithLegacyExtractStrategy_Length() {
        return (EAttribute)getUniversalPayloadWithLegacyExtractStrategy().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUniversalPayloadWithLegacyExtractStrategy__IsValid_hasValidStartbit__DiagnosticChain_Map() {
        return getUniversalPayloadWithLegacyExtractStrategy().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getUniversalPayloadWithLegacyExtractStrategy__IsValid_hasValidDataLength__DiagnosticChain_Map() {
        return getUniversalPayloadWithLegacyExtractStrategy().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractBusMessage() {
		if (abstractBusMessageEClass == null) {
			abstractBusMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(135);
		}
		return abstractBusMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractBusMessage_BusId() {
        return (EAttribute)getAbstractBusMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractBusMessage_BusIdTmplParam() {
        return (EAttribute)getAbstractBusMessage().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractBusMessage_BusIdRange() {
        return (EAttribute)getAbstractBusMessage().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAbstractBusMessage_Filters() {
        return (EReference)getAbstractBusMessage().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractBusMessage__IsValid_hasValidBusId__DiagnosticChain_Map() {
        return getAbstractBusMessage().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractBusMessage__IsValid_isOsiLayerConform__DiagnosticChain_Map() {
        return getAbstractBusMessage().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginFilter() {
		if (pluginFilterEClass == null) {
			pluginFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(136);
		}
		return pluginFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginFilter_PluginName() {
        return (EAttribute)getPluginFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginFilter_PluginVersion() {
        return (EAttribute)getPluginFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginMessage() {
		if (pluginMessageEClass == null) {
			pluginMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(137);
		}
		return pluginMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPluginMessage_PluginFilter() {
        return (EReference)getPluginMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginSignal() {
		if (pluginSignalEClass == null) {
			pluginSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(138);
		}
		return pluginSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginStateExtractStrategy() {
		if (pluginStateExtractStrategyEClass == null) {
			pluginStateExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(139);
		}
		return pluginStateExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginStateExtractStrategy_States() {
        return (EAttribute)getPluginStateExtractStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginStateExtractStrategy_StatesTmplParam() {
        return (EAttribute)getPluginStateExtractStrategy().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginStateExtractStrategy_StatesActive() {
        return (EAttribute)getPluginStateExtractStrategy().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginStateExtractStrategy_StatesActiveTmplParam() {
        return (EAttribute)getPluginStateExtractStrategy().getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPluginStateExtractStrategy__IsValid_hasValidStatesActive__DiagnosticChain_Map() {
        return getPluginStateExtractStrategy().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPluginStateExtractStrategy__IsValid_hasValidState__DiagnosticChain_Map() {
        return getPluginStateExtractStrategy().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginResultExtractStrategy() {
		if (pluginResultExtractStrategyEClass == null) {
			pluginResultExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(140);
		}
		return pluginResultExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginResultExtractStrategy_ResultRange() {
        return (EAttribute)getPluginResultExtractStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPluginResultExtractStrategy__IsValid_hasValidResultRange__DiagnosticChain_Map() {
        return getPluginResultExtractStrategy().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEmptyDecodeStrategy() {
		if (emptyDecodeStrategyEClass == null) {
			emptyDecodeStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(141);
		}
		return emptyDecodeStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPluginCheckExpression() {
		if (pluginCheckExpressionEClass == null) {
			pluginCheckExpressionEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(142);
		}
		return pluginCheckExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginCheckExpression_EvaluationBehaviour() {
        return (EAttribute)getPluginCheckExpression().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getPluginCheckExpression_SignalToCheck() {
        return (EReference)getPluginCheckExpression().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPluginCheckExpression_EvaluationBehaviourTmplParam() {
        return (EAttribute)getPluginCheckExpression().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBaseClassWithID() {
		if (baseClassWithIDEClass == null) {
			baseClassWithIDEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(143);
		}
		return baseClassWithIDEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getBaseClassWithID_Id() {
        return (EAttribute)getBaseClassWithID().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getHeaderSignal() {
		if (headerSignalEClass == null) {
			headerSignalEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(145);
		}
		return headerSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHeaderSignal_Attribute() {
        return (EAttribute)getHeaderSignal().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getHeaderSignal_AttributeTmplParam() {
        return (EAttribute)getHeaderSignal().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getHeaderSignal__IsValid_hasValidAttribute__DiagnosticChain_Map() {
        return getHeaderSignal().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIVariableReaderWriter() {
		if (iVariableReaderWriterEClass == null) {
			iVariableReaderWriterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(146);
		}
		return iVariableReaderWriterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIVariableReaderWriter__GetReadVariables() {
        return getIVariableReaderWriter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIVariableReaderWriter__GetWriteVariables() {
        return getIVariableReaderWriter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIStateTransitionReference() {
		if (iStateTransitionReferenceEClass == null) {
			iStateTransitionReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(147);
		}
		return iStateTransitionReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIStateTransitionReference__GetStateDependencies() {
        return getIStateTransitionReference().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getIStateTransitionReference__GetTransitionDependencies() {
        return getIStateTransitionReference().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTPFilter() {
		if (tpFilterEClass == null) {
			tpFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(149);
		}
		return tpFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTPFilter_SourcePort() {
        return (EAttribute)getTPFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTPFilter_DestPort() {
        return (EAttribute)getTPFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getBaseClassWithSourceReference() {
		if (baseClassWithSourceReferenceEClass == null) {
			baseClassWithSourceReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(150);
		}
		return baseClassWithSourceReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getBaseClassWithSourceReference_SourceReference() {
        return (EReference)getBaseClassWithSourceReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getSourceReference() {
		if (sourceReferenceEClass == null) {
			sourceReferenceEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(151);
		}
		return sourceReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getSourceReference_SerializedReference() {
        return (EAttribute)getSourceReference().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getEthernetMessage() {
		if (ethernetMessageEClass == null) {
			ethernetMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(152);
		}
		return ethernetMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getEthernetMessage_EthernetFilter() {
        return (EReference)getEthernetMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getIPv4Message() {
		if (iPv4MessageEClass == null) {
			iPv4MessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(153);
		}
		return iPv4MessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getIPv4Message_IPv4Filter() {
        return (EReference)getIPv4Message().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNonVerboseDLTMessage() {
		if (nonVerboseDLTMessageEClass == null) {
			nonVerboseDLTMessageEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(154);
		}
		return nonVerboseDLTMessageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getNonVerboseDLTMessage_NonVerboseDltFilter() {
        return (EReference)getNonVerboseDLTMessage().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getStringDecodeStrategy() {
		if (stringDecodeStrategyEClass == null) {
			stringDecodeStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(155);
		}
		return stringDecodeStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringDecodeStrategy_StringTermination() {
        return (EAttribute)getStringDecodeStrategy().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getStringDecodeStrategy_StringTerminationTmplParam() {
        return (EAttribute)getStringDecodeStrategy().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNonVerboseDLTFilter() {
		if (nonVerboseDLTFilterEClass == null) {
			nonVerboseDLTFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(156);
		}
		return nonVerboseDLTFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNonVerboseDLTFilter_MessageId() {
        return (EAttribute)getNonVerboseDLTFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNonVerboseDLTFilter_MessageIdRange() {
        return (EAttribute)getNonVerboseDLTFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getNonVerboseDLTFilter__IsValid_hasValidMessageIdOrMessageIdRange__DiagnosticChain_Map() {
        return getNonVerboseDLTFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVerboseDLTExtractStrategy() {
		if (verboseDLTExtractStrategyEClass == null) {
			verboseDLTExtractStrategyEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(157);
		}
		return verboseDLTExtractStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getRegexOperation() {
		if (regexOperationEClass == null) {
			regexOperationEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(160);
		}
		return regexOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getRegexOperation_Regex() {
        return (EAttribute)getRegexOperation().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getRegexOperation_DynamicRegex() {
        return (EReference)getRegexOperation().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getRegexOperation__IsValid_hasValidRegex__DiagnosticChain_Map() {
        return getRegexOperation().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getPayloadFilter() {
		if (payloadFilterEClass == null) {
			payloadFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(162);
		}
		return payloadFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPayloadFilter_Index() {
        return (EAttribute)getPayloadFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPayloadFilter_Mask() {
        return (EAttribute)getPayloadFilter().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getPayloadFilter_Value() {
        return (EAttribute)getPayloadFilter().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPayloadFilter__IsValid_hasValidIndex__DiagnosticChain_Map() {
        return getPayloadFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPayloadFilter__IsValid_hasValidMask__DiagnosticChain_Map() {
        return getPayloadFilter().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getPayloadFilter__IsValid_hasValidValue__DiagnosticChain_Map() {
        return getPayloadFilter().getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVerboseDLTPayloadFilter() {
		if (verboseDLTPayloadFilterEClass == null) {
			verboseDLTPayloadFilterEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(163);
		}
		return verboseDLTPayloadFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getVerboseDLTPayloadFilter_Regex() {
        return (EAttribute)getVerboseDLTPayloadFilter().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getVerboseDLTPayloadFilter__IsValid_hasValidRegex__DiagnosticChain_Map() {
        return getVerboseDLTPayloadFilter().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAbstractVariable() {
		if (abstractVariableEClass == null) {
			abstractVariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(164);
		}
		return abstractVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractVariable_Name() {
        return (EAttribute)getAbstractVariable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractVariable_DisplayName() {
        return (EAttribute)getAbstractVariable().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAbstractVariable_Description() {
        return (EAttribute)getAbstractVariable().getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractVariable__IsValid_hasValidName__DiagnosticChain_Map() {
        return getAbstractVariable().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAbstractVariable__IsValid_hasValidDisplayName__DiagnosticChain_Map() {
        return getAbstractVariable().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getVariableStructure() {
		if (variableStructureEClass == null) {
			variableStructureEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(165);
		}
		return variableStructureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getVariableStructure_Variables() {
        return (EReference)getVariableStructure().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getComputedVariable() {
		if (computedVariableEClass == null) {
			computedVariableEClass = (EClass)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(166);
		}
		return computedVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getComputedVariable_Operands() {
        return (EReference)getComputedVariable().getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getComputedVariable_Expression() {
        return (EAttribute)getComputedVariable().getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getComputedVariable__IsValid_hasValidExpression__DiagnosticChain_Map() {
        return getComputedVariable().getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getComputedVariable__IsValid_hasValidOperands__DiagnosticChain_Map() {
        return getComputedVariable().getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCheckType() {
		if (checkTypeEEnum == null) {
			checkTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(2);
		}
		return checkTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getComparator() {
		if (comparatorEEnum == null) {
			comparatorEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(4);
		}
		return comparatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getLogicalOperatorType() {
		if (logicalOperatorTypeEEnum == null) {
			logicalOperatorTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(18);
		}
		return logicalOperatorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSignalDataType() {
		if (signalDataTypeEEnum == null) {
			signalDataTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(20);
		}
		return signalDataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFlexChannelType() {
		if (flexChannelTypeEEnum == null) {
			flexChannelTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(25);
		}
		return flexChannelTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getByteOrderType() {
		if (byteOrderTypeEEnum == null) {
			byteOrderTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(26);
		}
		return byteOrderTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFormatDataType() {
		if (formatDataTypeEEnum == null) {
			formatDataTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(34);
		}
		return formatDataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getFlexRayHeaderFlagSelection() {
		if (flexRayHeaderFlagSelectionEEnum == null) {
			flexRayHeaderFlagSelectionEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(41);
		}
		return flexRayHeaderFlagSelectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getBooleanOrTemplatePlaceholderEnum() {
		if (booleanOrTemplatePlaceholderEnumEEnum == null) {
			booleanOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(46);
		}
		return booleanOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getProtocolTypeOrTemplatePlaceholderEnum() {
		if (protocolTypeOrTemplatePlaceholderEnumEEnum == null) {
			protocolTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(50);
		}
		return protocolTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getEthernetProtocolTypeSelection() {
		if (ethernetProtocolTypeSelectionEEnum == null) {
			ethernetProtocolTypeSelectionEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(52);
		}
		return ethernetProtocolTypeSelectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getRxTxFlagTypeOrTemplatePlaceholderEnum() {
		if (rxTxFlagTypeOrTemplatePlaceholderEnumEEnum == null) {
			rxTxFlagTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(59);
		}
		return rxTxFlagTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getObserverValueType() {
		if (observerValueTypeEEnum == null) {
			observerValueTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(60);
		}
		return observerValueTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getPluginStateValueTypeOrTemplatePlaceholderEnum() {
		if (pluginStateValueTypeOrTemplatePlaceholderEnumEEnum == null) {
			pluginStateValueTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(61);
		}
		return pluginStateValueTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAnalysisTypeOrTemplatePlaceholderEnum() {
		if (analysisTypeOrTemplatePlaceholderEnumEEnum == null) {
			analysisTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(62);
		}
		return analysisTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPMessageTypeOrTemplatePlaceholderEnum() {
		if (someIPMessageTypeOrTemplatePlaceholderEnumEEnum == null) {
			someIPMessageTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(63);
		}
		return someIPMessageTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPMethodTypeOrTemplatePlaceholderEnum() {
		if (someIPMethodTypeOrTemplatePlaceholderEnumEEnum == null) {
			someIPMethodTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(64);
		}
		return someIPMethodTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPReturnCodeOrTemplatePlaceholderEnum() {
		if (someIPReturnCodeOrTemplatePlaceholderEnumEEnum == null) {
			someIPReturnCodeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(65);
		}
		return someIPReturnCodeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPSDFlagsOrTemplatePlaceholderEnum() {
		if (someIPSDFlagsOrTemplatePlaceholderEnumEEnum == null) {
			someIPSDFlagsOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(66);
		}
		return someIPSDFlagsOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPSDTypeOrTemplatePlaceholderEnum() {
		if (someIPSDTypeOrTemplatePlaceholderEnumEEnum == null) {
			someIPSDTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(67);
		}
		return someIPSDTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getStreamAnalysisFlagsOrTemplatePlaceholderEnum() {
		if (streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum == null) {
			streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(69);
		}
		return streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum() {
		if (someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum == null) {
			someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(70);
		}
		return someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCanExtIdentifierOrTemplatePlaceholderEnum() {
		if (canExtIdentifierOrTemplatePlaceholderEnumEEnum == null) {
			canExtIdentifierOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(72);
		}
		return canExtIdentifierOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDataType() {
		if (dataTypeEEnum == null) {
			dataTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(74);
		}
		return dataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDLT_MessageType() {
		if (dlT_MessageTypeEEnum == null) {
			dlT_MessageTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(84);
		}
		return dlT_MessageTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDLT_MessageTraceInfo() {
		if (dlT_MessageTraceInfoEEnum == null) {
			dlT_MessageTraceInfoEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(85);
		}
		return dlT_MessageTraceInfoEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDLT_MessageLogInfo() {
		if (dlT_MessageLogInfoEEnum == null) {
			dlT_MessageLogInfoEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(86);
		}
		return dlT_MessageLogInfoEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDLT_MessageControlInfo() {
		if (dlT_MessageControlInfoEEnum == null) {
			dlT_MessageControlInfoEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(87);
		}
		return dlT_MessageControlInfoEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getDLT_MessageBusInfo() {
		if (dlT_MessageBusInfoEEnum == null) {
			dlT_MessageBusInfoEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(88);
		}
		return dlT_MessageBusInfoEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getForEachExpressionModifierTemplate() {
		if (forEachExpressionModifierTemplateEEnum == null) {
			forEachExpressionModifierTemplateEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(109);
		}
		return forEachExpressionModifierTemplateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAUTOSARDataType() {
		if (autosarDataTypeEEnum == null) {
			autosarDataTypeEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(120);
		}
		return autosarDataTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getCANFrameTypeOrTemplatePlaceholderEnum() {
		if (canFrameTypeOrTemplatePlaceholderEnumEEnum == null) {
			canFrameTypeOrTemplatePlaceholderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(144);
		}
		return canFrameTypeOrTemplatePlaceholderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getEvaluationBehaviourOrTemplateDefinedEnum() {
		if (evaluationBehaviourOrTemplateDefinedEnumEEnum == null) {
			evaluationBehaviourOrTemplateDefinedEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(148);
		}
		return evaluationBehaviourOrTemplateDefinedEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getSignalVariableLagEnum() {
		if (signalVariableLagEnumEEnum == null) {
			signalVariableLagEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(158);
		}
		return signalVariableLagEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getTTLOrTemplatePlaceHolderEnum() {
		if (ttlOrTemplatePlaceHolderEnumEEnum == null) {
			ttlOrTemplatePlaceHolderEnumEEnum = (EEnum)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(159);
		}
		return ttlOrTemplatePlaceHolderEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getCheckTypeObject() {
		if (checkTypeObjectEDataType == null) {
			checkTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(3);
		}
		return checkTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getComparatorObject() {
		if (comparatorObjectEDataType == null) {
			comparatorObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(5);
		}
		return comparatorObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getLogicalOperatorTypeObject() {
		if (logicalOperatorTypeObjectEDataType == null) {
			logicalOperatorTypeObjectEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(19);
		}
		return logicalOperatorTypeObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getHexOrIntOrTemplatePlaceholder() {
		if (hexOrIntOrTemplatePlaceholderEDataType == null) {
			hexOrIntOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(42);
		}
		return hexOrIntOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getIntOrTemplatePlaceholder() {
		if (intOrTemplatePlaceholderEDataType == null) {
			intOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(43);
		}
		return intOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getLongOrTemplatePlaceholder() {
		if (longOrTemplatePlaceholderEDataType == null) {
			longOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(44);
		}
		return longOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDoubleOrTemplatePlaceholder() {
		if (doubleOrTemplatePlaceholderEDataType == null) {
			doubleOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(45);
		}
		return doubleOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getDoubleOrHexOrTemplatePlaceholder() {
		if (doubleOrHexOrTemplatePlaceholderEDataType == null) {
			doubleOrHexOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(47);
		}
		return doubleOrHexOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getIPPatternOrTemplatePlaceholder() {
		if (ipPatternOrTemplatePlaceholderEDataType == null) {
			ipPatternOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(49);
		}
		return ipPatternOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getMACPatternOrTemplatePlaceholder() {
		if (macPatternOrTemplatePlaceholderEDataType == null) {
			macPatternOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(51);
		}
		return macPatternOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPortPatternOrTemplatePlaceholder() {
		if (portPatternOrTemplatePlaceholderEDataType == null) {
			portPatternOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(53);
		}
		return portPatternOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getVlanIdPatternOrTemplatePlaceholder() {
		if (vlanIdPatternOrTemplatePlaceholderEDataType == null) {
			vlanIdPatternOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(54);
		}
		return vlanIdPatternOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getIPOrTemplatePlaceholder() {
		if (ipOrTemplatePlaceholderEDataType == null) {
			ipOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(55);
		}
		return ipOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getPortOrTemplatePlaceholder() {
		if (portOrTemplatePlaceholderEDataType == null) {
			portOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(56);
		}
		return portOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getVlanIdOrTemplatePlaceholder() {
		if (vlanIdOrTemplatePlaceholderEDataType == null) {
			vlanIdOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(57);
		}
		return vlanIdOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getMACOrTemplatePlaceholder() {
		if (macOrTemplatePlaceholderEDataType == null) {
			macOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(58);
		}
		return macOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getBitmaskOrTemplatePlaceholder() {
		if (bitmaskOrTemplatePlaceholderEDataType == null) {
			bitmaskOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(68);
		}
		return bitmaskOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getHexOrIntOrNullOrTemplatePlaceholder() {
		if (hexOrIntOrNullOrTemplatePlaceholderEDataType == null) {
			hexOrIntOrNullOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(71);
		}
		return hexOrIntOrNullOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getNonZeroDoubleOrTemplatePlaceholder() {
		if (nonZeroDoubleOrTemplatePlaceholderEDataType == null) {
			nonZeroDoubleOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(96);
		}
		return nonZeroDoubleOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getValVarInitialValueDoubleOrTemplatePlaceholderOrString() {
		if (valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType == null) {
			valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(97);
		}
		return valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EDataType getByteOrTemplatePlaceholder() {
		if (byteOrTemplatePlaceholderEDataType == null) {
			byteOrTemplatePlaceholderEDataType = (EDataType)EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI).getEClassifiers().get(161);
		}
		return byteOrTemplatePlaceholderEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConditionsFactory getConditionsFactory() {
		return (ConditionsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isLoaded = false;

	/**
	 * Laods the package and any sub-packages from their serialized form.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadPackage() {
		if (isLoaded) return;
		isLoaded = true;

		URL url = getClass().getResource(packageFilename);
		if (url == null) {
			throw new RuntimeException("Missing serialized package: " + packageFilename);
		}
		URI uri = URI.createURI(url.toString());
		Resource resource = new EcoreResourceFactoryImpl().createResource(uri);
		try {
			resource.load(null);
		}
		catch (IOException exception) {
			throw new WrappedException(exception);
		}
		initializeFromLoadedEPackage(this, (EPackage)resource.getContents().get(0));
		createResource(eNS_URI);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isFixed = false;

	/**
	 * Fixes up the loaded package, to make it appear as if it had been programmatically built.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fixPackageContents() {
		if (isFixed) return;
		isFixed = true;
		fixEClassifiers();
	}

	/**
	 * Sets the instance class on the given classifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void fixInstanceClass(EClassifier eClassifier) {
		if (eClassifier.getInstanceClassName() == null) {
			eClassifier.setInstanceClassName("conditions." + eClassifier.getName());
			setGeneratedClassName(eClassifier);
		}
	}

} //ConditionsPackageImpl
