/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.EmptyExtractStrategy;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Empty Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmptyExtractStrategyImpl extends ExtractStrategyImpl implements EmptyExtractStrategy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmptyExtractStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getEmptyExtractStrategy();
	}

} //EmptyExtractStrategyImpl
