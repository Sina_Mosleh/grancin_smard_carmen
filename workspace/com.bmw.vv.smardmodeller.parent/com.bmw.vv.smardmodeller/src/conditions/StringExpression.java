/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getStringExpression()
 * @model abstract="true"
 * @generated
 */
public interface StringExpression extends Expression {
} // StringExpression
