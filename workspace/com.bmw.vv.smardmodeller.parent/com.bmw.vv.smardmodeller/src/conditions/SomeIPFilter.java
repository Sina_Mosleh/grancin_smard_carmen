/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IP Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SomeIPFilter#getServiceId <em>Service Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getMethodType <em>Method Type</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getMethodTypeTmplParam <em>Method Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getMethodId <em>Method Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getClientId <em>Client Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getSessionId <em>Session Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getProtocolVersion <em>Protocol Version</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getInterfaceVersion <em>Interface Version</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getMessageType <em>Message Type</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getMessageTypeTmplParam <em>Message Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getReturnCode <em>Return Code</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getReturnCodeTmplParam <em>Return Code Tmpl Param</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#getSomeIPLength <em>Some IP Length</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSomeIPFilter()
 * @model
 * @generated
 */
public interface SomeIPFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Service Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Service Id</em>' attribute.
	 * @see #setServiceId(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_ServiceId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getServiceId();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getServiceId <em>Service Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Service Id</em>' attribute.
	 * @see #getServiceId()
	 * @generated
	 */
	void setServiceId(String value);

	/**
	 * Returns the value of the '<em><b>Method Type</b></em>' attribute.
	 * The default value is <code>"ALL"</code>.
	 * The literals are from the enumeration {@link conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Type</em>' attribute.
	 * @see conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
	 * @see #setMethodType(SomeIPMethodTypeOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_MethodType()
	 * @model default="ALL"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	SomeIPMethodTypeOrTemplatePlaceholderEnum getMethodType();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getMethodType <em>Method Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Type</em>' attribute.
	 * @see conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
	 * @see #getMethodType()
	 * @generated
	 */
	void setMethodType(SomeIPMethodTypeOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Method Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Type Tmpl Param</em>' attribute.
	 * @see #setMethodTypeTmplParam(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_MethodTypeTmplParam()
	 * @model
	 * @generated
	 */
	String getMethodTypeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getMethodTypeTmplParam <em>Method Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Type Tmpl Param</em>' attribute.
	 * @see #getMethodTypeTmplParam()
	 * @generated
	 */
	void setMethodTypeTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Method Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Method Id</em>' attribute.
	 * @see #setMethodId(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_MethodId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getMethodId();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getMethodId <em>Method Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Method Id</em>' attribute.
	 * @see #getMethodId()
	 * @generated
	 */
	void setMethodId(String value);

	/**
	 * Returns the value of the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Client Id</em>' attribute.
	 * @see #setClientId(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_ClientId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getClientId();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getClientId <em>Client Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Client Id</em>' attribute.
	 * @see #getClientId()
	 * @generated
	 */
	void setClientId(String value);

	/**
	 * Returns the value of the '<em><b>Session Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session Id</em>' attribute.
	 * @see #setSessionId(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_SessionId()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSessionId();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getSessionId <em>Session Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session Id</em>' attribute.
	 * @see #getSessionId()
	 * @generated
	 */
	void setSessionId(String value);

	/**
	 * Returns the value of the '<em><b>Protocol Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Version</em>' attribute.
	 * @see #setProtocolVersion(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_ProtocolVersion()
	 * @model dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getProtocolVersion();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getProtocolVersion <em>Protocol Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Version</em>' attribute.
	 * @see #getProtocolVersion()
	 * @generated
	 */
	void setProtocolVersion(String value);

	/**
	 * Returns the value of the '<em><b>Interface Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Version</em>' attribute.
	 * @see #setInterfaceVersion(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_InterfaceVersion()
	 * @model dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getInterfaceVersion();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getInterfaceVersion <em>Interface Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface Version</em>' attribute.
	 * @see #getInterfaceVersion()
	 * @generated
	 */
	void setInterfaceVersion(String value);

	/**
	 * Returns the value of the '<em><b>Message Type</b></em>' attribute.
	 * The default value is <code>"ALL"</code>.
	 * The literals are from the enumeration {@link conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Type</em>' attribute.
	 * @see conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
	 * @see #setMessageType(SomeIPMessageTypeOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_MessageType()
	 * @model default="ALL"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	SomeIPMessageTypeOrTemplatePlaceholderEnum getMessageType();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getMessageType <em>Message Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Type</em>' attribute.
	 * @see conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
	 * @see #getMessageType()
	 * @generated
	 */
	void setMessageType(SomeIPMessageTypeOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Message Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Type Tmpl Param</em>' attribute.
	 * @see #setMessageTypeTmplParam(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_MessageTypeTmplParam()
	 * @model
	 * @generated
	 */
	String getMessageTypeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getMessageTypeTmplParam <em>Message Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Type Tmpl Param</em>' attribute.
	 * @see #getMessageTypeTmplParam()
	 * @generated
	 */
	void setMessageTypeTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Return Code</b></em>' attribute.
	 * The default value is <code>"ALL"</code>.
	 * The literals are from the enumeration {@link conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Code</em>' attribute.
	 * @see conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
	 * @see #setReturnCode(SomeIPReturnCodeOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_ReturnCode()
	 * @model default="ALL"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	SomeIPReturnCodeOrTemplatePlaceholderEnum getReturnCode();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getReturnCode <em>Return Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Code</em>' attribute.
	 * @see conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
	 * @see #getReturnCode()
	 * @generated
	 */
	void setReturnCode(SomeIPReturnCodeOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Return Code Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Code Tmpl Param</em>' attribute.
	 * @see #setReturnCodeTmplParam(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_ReturnCodeTmplParam()
	 * @model
	 * @generated
	 */
	String getReturnCodeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getReturnCodeTmplParam <em>Return Code Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Code Tmpl Param</em>' attribute.
	 * @see #getReturnCodeTmplParam()
	 * @generated
	 */
	void setReturnCodeTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Some IP Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Some IP Length</em>' attribute.
	 * @see #setSomeIPLength(String)
	 * @see conditions.ConditionsPackage#getSomeIPFilter_SomeIPLength()
	 * @model dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSomeIPLength();

	/**
	 * Sets the value of the '{@link conditions.SomeIPFilter#getSomeIPLength <em>Some IP Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Some IP Length</em>' attribute.
	 * @see #getSomeIPLength()
	 * @generated
	 */
	void setSomeIPLength(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidServiceId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMethodId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSessionId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidClientId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidLength(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidProtocolVersion(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidInterfaceVersion(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageType(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidReturnCode(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMethodType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // SomeIPFilter
