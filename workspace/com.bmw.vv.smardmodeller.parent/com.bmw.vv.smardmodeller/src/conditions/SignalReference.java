/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SignalReference#getSignal <em>Signal</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSignalReference()
 * @model
 * @generated
 */
public interface SignalReference extends ComparatorSignal, ISignalOrReference, INumericOperand, IStringOperand {
	/**
	 * Returns the value of the '<em><b>Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal</em>' reference.
	 * @see #setSignal(ISignalOrReference)
	 * @see conditions.ConditionsPackage#getSignalReference_Signal()
	 * @model required="true"
	 * @generated
	 */
	ISignalOrReference getSignal();

	/**
	 * Sets the value of the '{@link conditions.SignalReference#getSignal <em>Signal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signal</em>' reference.
	 * @see #getSignal()
	 * @generated
	 */
	void setSignal(ISignalOrReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSignal(DiagnosticChain diagnostics, Map<Object, Object> context);

} // SignalReference
