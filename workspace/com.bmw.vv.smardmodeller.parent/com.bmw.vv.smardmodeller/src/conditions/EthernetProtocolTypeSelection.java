/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Ethernet Protocol Type Selection</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getEthernetProtocolTypeSelection()
 * @model
 * @generated
 */
public enum EthernetProtocolTypeSelection implements Enumerator {
	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

	/**
	 * The '<em><b>UDP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UDP_VALUE
	 * @generated
	 * @ordered
	 */
	UDP(0, "UDP", "UDP"),

	/**
	 * The '<em><b>TCP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TCP_VALUE
	 * @generated
	 * @ordered
	 */
	TCP(1, "TCP", "TCP");

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * The '<em><b>UDP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UDP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UDP_VALUE = 0;

	/**
	 * The '<em><b>TCP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TCP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TCP_VALUE = 1;

	/**
	 * An array of all the '<em><b>Ethernet Protocol Type Selection</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final EthernetProtocolTypeSelection[] VALUES_ARRAY =
		new EthernetProtocolTypeSelection[] {
			TEMPLATE_DEFINED,
			UDP,
			TCP,
		};

	/**
	 * A public read-only list of all the '<em><b>Ethernet Protocol Type Selection</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<EthernetProtocolTypeSelection> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Ethernet Protocol Type Selection</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EthernetProtocolTypeSelection get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EthernetProtocolTypeSelection result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Ethernet Protocol Type Selection</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EthernetProtocolTypeSelection getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EthernetProtocolTypeSelection result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Ethernet Protocol Type Selection</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static EthernetProtocolTypeSelection get(int value) {
		switch (value) {
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
			case UDP_VALUE: return UDP;
			case TCP_VALUE: return TCP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EthernetProtocolTypeSelection(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //EthernetProtocolTypeSelection
