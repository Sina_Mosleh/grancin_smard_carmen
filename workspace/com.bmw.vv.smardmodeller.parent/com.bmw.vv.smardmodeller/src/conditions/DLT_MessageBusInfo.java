/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DLT Message Bus Info</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getDLT_MessageBusInfo()
 * @model
 * @generated
 */
public enum DLT_MessageBusInfo implements Enumerator {
	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_SPECIFIED(0, "NOT_SPECIFIED", "NOT_SPECIFIED"),

	/**
	 * The '<em><b>DLT NW TRACE IPC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_IPC_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_IPC(1, "DLT_NW_TRACE_IPC", "DLT_NW_TRACE_IPC"),

	/**
	 * The '<em><b>DLT NW TRACE CAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_CAN_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_CAN(2, "DLT_NW_TRACE_CAN", "DLT_NW_TRACE_CAN"),

	/**
	 * The '<em><b>DLT NW TRACE FLEXRAY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_FLEXRAY_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_FLEXRAY(3, "DLT_NW_TRACE_FLEXRAY", "DLT_NW_TRACE_FLEXRAY"),

	/**
	 * The '<em><b>DLT NW TRACE MOST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_MOST_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_MOST(4, "DLT_NW_TRACE_MOST", "DLT_NW_TRACE_MOST"),

	/**
	 * The '<em><b>DLT NW TRACE RESERVED1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED1_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_RESERVED1(5, "DLT_NW_TRACE_RESERVED1", "DLT_NW_TRACE_RESERVED1"),

	/**
	 * The '<em><b>DLT NW TRACE RESERVED2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED2_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_RESERVED2(6, "DLT_NW_TRACE_RESERVED2", "DLT_NW_TRACE_RESERVED2"),

	/**
	 * The '<em><b>DLT NW TRACE RESERVED3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED3_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_RESERVED3(7, "DLT_NW_TRACE_RESERVED3", "DLT_NW_TRACE_RESERVED3"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED1_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED1(8, "DLT_NW_TRACE_USER_DEFINED1", "DLT_NW_TRACE_USER_DEFINED1"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED2_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED2(9, "DLT_NW_TRACE_USER_DEFINED2", "DLT_NW_TRACE_USER_DEFINED2"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED3_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED3(10, "DLT_NW_TRACE_USER_DEFINED3", "DLT_NW_TRACE_USER_DEFINED3"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED4</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED4_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED4(11, "DLT_NW_TRACE_USER_DEFINED4", "DLT_NW_TRACE_USER_DEFINED4"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED5</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED5_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED5(12, "DLT_NW_TRACE_USER_DEFINED5", "DLT_NW_TRACE_USER_DEFINED5"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED6</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED6_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED6(13, "DLT_NW_TRACE_USER_DEFINED6", "DLT_NW_TRACE_USER_DEFINED6"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED7</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED7_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED7(14, "DLT_NW_TRACE_USER_DEFINED7", "DLT_NW_TRACE_USER_DEFINED7"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED8</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED8_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED8(15, "DLT_NW_TRACE_USER_DEFINED8", "DLT_NW_TRACE_USER_DEFINED8"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED9</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED9_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED9(16, "DLT_NW_TRACE_USER_DEFINED9", "DLT_NW_TRACE_USER_DEFINED9"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED10</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED10_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED10(17, "DLT_NW_TRACE_USER_DEFINED10", "DLT_NW_TRACE_USER_DEFINED10"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED11</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED11_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED11(18, "DLT_NW_TRACE_USER_DEFINED11", "DLT_NW_TRACE_USER_DEFINED11"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED12</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED12_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED12(19, "DLT_NW_TRACE_USER_DEFINED12", "DLT_NW_TRACE_USER_DEFINED12"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED13</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED13_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED13(20, "DLT_NW_TRACE_USER_DEFINED13", "DLT_NW_TRACE_USER_DEFINED13"),

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED14</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED14_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_NW_TRACE_USER_DEFINED14(21, "DLT_NW_TRACE_USER_DEFINED14", "DLT_NW_TRACE_USER_DEFINED14"),

	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined");

	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOT_SPECIFIED_VALUE = 0;

	/**
	 * The '<em><b>DLT NW TRACE IPC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_IPC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_IPC_VALUE = 1;

	/**
	 * The '<em><b>DLT NW TRACE CAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_CAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_CAN_VALUE = 2;

	/**
	 * The '<em><b>DLT NW TRACE FLEXRAY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_FLEXRAY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_FLEXRAY_VALUE = 3;

	/**
	 * The '<em><b>DLT NW TRACE MOST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_MOST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_MOST_VALUE = 4;

	/**
	 * The '<em><b>DLT NW TRACE RESERVED1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED1
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_RESERVED1_VALUE = 5;

	/**
	 * The '<em><b>DLT NW TRACE RESERVED2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_RESERVED2_VALUE = 6;

	/**
	 * The '<em><b>DLT NW TRACE RESERVED3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_RESERVED3
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_RESERVED3_VALUE = 7;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED1
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED1_VALUE = 8;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED2_VALUE = 9;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED3
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED3_VALUE = 10;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED4</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED4
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED4_VALUE = 11;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED5</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED5
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED5_VALUE = 12;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED6</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED6
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED6_VALUE = 13;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED7</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED7
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED7_VALUE = 14;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED8</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED8
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED8_VALUE = 15;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED9</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED9
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED9_VALUE = 16;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED10</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED10
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED10_VALUE = 17;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED11</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED11
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED11_VALUE = 18;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED12</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED12
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED12_VALUE = 19;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED13</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED13
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED13_VALUE = 20;

	/**
	 * The '<em><b>DLT NW TRACE USER DEFINED14</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_NW_TRACE_USER_DEFINED14
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_NW_TRACE_USER_DEFINED14_VALUE = 21;

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * An array of all the '<em><b>DLT Message Bus Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DLT_MessageBusInfo[] VALUES_ARRAY =
		new DLT_MessageBusInfo[] {
			NOT_SPECIFIED,
			DLT_NW_TRACE_IPC,
			DLT_NW_TRACE_CAN,
			DLT_NW_TRACE_FLEXRAY,
			DLT_NW_TRACE_MOST,
			DLT_NW_TRACE_RESERVED1,
			DLT_NW_TRACE_RESERVED2,
			DLT_NW_TRACE_RESERVED3,
			DLT_NW_TRACE_USER_DEFINED1,
			DLT_NW_TRACE_USER_DEFINED2,
			DLT_NW_TRACE_USER_DEFINED3,
			DLT_NW_TRACE_USER_DEFINED4,
			DLT_NW_TRACE_USER_DEFINED5,
			DLT_NW_TRACE_USER_DEFINED6,
			DLT_NW_TRACE_USER_DEFINED7,
			DLT_NW_TRACE_USER_DEFINED8,
			DLT_NW_TRACE_USER_DEFINED9,
			DLT_NW_TRACE_USER_DEFINED10,
			DLT_NW_TRACE_USER_DEFINED11,
			DLT_NW_TRACE_USER_DEFINED12,
			DLT_NW_TRACE_USER_DEFINED13,
			DLT_NW_TRACE_USER_DEFINED14,
			TEMPLATE_DEFINED,
		};

	/**
	 * A public read-only list of all the '<em><b>DLT Message Bus Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DLT_MessageBusInfo> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DLT Message Bus Info</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageBusInfo get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageBusInfo result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Bus Info</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageBusInfo getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageBusInfo result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Bus Info</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageBusInfo get(int value) {
		switch (value) {
			case NOT_SPECIFIED_VALUE: return NOT_SPECIFIED;
			case DLT_NW_TRACE_IPC_VALUE: return DLT_NW_TRACE_IPC;
			case DLT_NW_TRACE_CAN_VALUE: return DLT_NW_TRACE_CAN;
			case DLT_NW_TRACE_FLEXRAY_VALUE: return DLT_NW_TRACE_FLEXRAY;
			case DLT_NW_TRACE_MOST_VALUE: return DLT_NW_TRACE_MOST;
			case DLT_NW_TRACE_RESERVED1_VALUE: return DLT_NW_TRACE_RESERVED1;
			case DLT_NW_TRACE_RESERVED2_VALUE: return DLT_NW_TRACE_RESERVED2;
			case DLT_NW_TRACE_RESERVED3_VALUE: return DLT_NW_TRACE_RESERVED3;
			case DLT_NW_TRACE_USER_DEFINED1_VALUE: return DLT_NW_TRACE_USER_DEFINED1;
			case DLT_NW_TRACE_USER_DEFINED2_VALUE: return DLT_NW_TRACE_USER_DEFINED2;
			case DLT_NW_TRACE_USER_DEFINED3_VALUE: return DLT_NW_TRACE_USER_DEFINED3;
			case DLT_NW_TRACE_USER_DEFINED4_VALUE: return DLT_NW_TRACE_USER_DEFINED4;
			case DLT_NW_TRACE_USER_DEFINED5_VALUE: return DLT_NW_TRACE_USER_DEFINED5;
			case DLT_NW_TRACE_USER_DEFINED6_VALUE: return DLT_NW_TRACE_USER_DEFINED6;
			case DLT_NW_TRACE_USER_DEFINED7_VALUE: return DLT_NW_TRACE_USER_DEFINED7;
			case DLT_NW_TRACE_USER_DEFINED8_VALUE: return DLT_NW_TRACE_USER_DEFINED8;
			case DLT_NW_TRACE_USER_DEFINED9_VALUE: return DLT_NW_TRACE_USER_DEFINED9;
			case DLT_NW_TRACE_USER_DEFINED10_VALUE: return DLT_NW_TRACE_USER_DEFINED10;
			case DLT_NW_TRACE_USER_DEFINED11_VALUE: return DLT_NW_TRACE_USER_DEFINED11;
			case DLT_NW_TRACE_USER_DEFINED12_VALUE: return DLT_NW_TRACE_USER_DEFINED12;
			case DLT_NW_TRACE_USER_DEFINED13_VALUE: return DLT_NW_TRACE_USER_DEFINED13;
			case DLT_NW_TRACE_USER_DEFINED14_VALUE: return DLT_NW_TRACE_USER_DEFINED14;
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DLT_MessageBusInfo(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DLT_MessageBusInfo
