/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Check Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.PluginCheckExpression#getEvaluationBehaviour <em>Evaluation Behaviour</em>}</li>
 *   <li>{@link conditions.PluginCheckExpression#getSignalToCheck <em>Signal To Check</em>}</li>
 *   <li>{@link conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam <em>Evaluation Behaviour Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getPluginCheckExpression()
 * @model
 * @generated
 */
public interface PluginCheckExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Evaluation Behaviour</b></em>' attribute.
	 * The default value is <code>"PULL_FROM_PLUGIN"</code>.
	 * The literals are from the enumeration {@link conditions.EvaluationBehaviourOrTemplateDefinedEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Behaviour</em>' attribute.
	 * @see conditions.EvaluationBehaviourOrTemplateDefinedEnum
	 * @see #setEvaluationBehaviour(EvaluationBehaviourOrTemplateDefinedEnum)
	 * @see conditions.ConditionsPackage#getPluginCheckExpression_EvaluationBehaviour()
	 * @model default="PULL_FROM_PLUGIN"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	EvaluationBehaviourOrTemplateDefinedEnum getEvaluationBehaviour();

	/**
	 * Sets the value of the '{@link conditions.PluginCheckExpression#getEvaluationBehaviour <em>Evaluation Behaviour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluation Behaviour</em>' attribute.
	 * @see conditions.EvaluationBehaviourOrTemplateDefinedEnum
	 * @see #getEvaluationBehaviour()
	 * @generated
	 */
	void setEvaluationBehaviour(EvaluationBehaviourOrTemplateDefinedEnum value);

	/**
	 * Returns the value of the '<em><b>Signal To Check</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signal To Check</em>' reference.
	 * @see #setSignalToCheck(PluginSignal)
	 * @see conditions.ConditionsPackage#getPluginCheckExpression_SignalToCheck()
	 * @model required="true"
	 * @generated
	 */
	PluginSignal getSignalToCheck();

	/**
	 * Sets the value of the '{@link conditions.PluginCheckExpression#getSignalToCheck <em>Signal To Check</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Signal To Check</em>' reference.
	 * @see #getSignalToCheck()
	 * @generated
	 */
	void setSignalToCheck(PluginSignal value);

	/**
	 * Returns the value of the '<em><b>Evaluation Behaviour Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation Behaviour Tmpl Param</em>' attribute.
	 * @see #setEvaluationBehaviourTmplParam(String)
	 * @see conditions.ConditionsPackage#getPluginCheckExpression_EvaluationBehaviourTmplParam()
	 * @model
	 * @generated
	 */
	String getEvaluationBehaviourTmplParam();

	/**
	 * Sets the value of the '{@link conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam <em>Evaluation Behaviour Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluation Behaviour Tmpl Param</em>' attribute.
	 * @see #getEvaluationBehaviourTmplParam()
	 * @generated
	 */
	void setEvaluationBehaviourTmplParam(String value);

} // PluginCheckExpression
