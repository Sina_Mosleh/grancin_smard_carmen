/**
 */
package conditions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see conditions.ConditionsFactory
 * @model kind="package"
 *        extendedMetaData="qualified='true'"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore' ecore.xml.type='http://www.eclipse.org/emf/2003/XMLType'"
 * @generated
 */
public interface ConditionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "conditions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/conditions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "conditions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConditionsPackage eINSTANCE = conditions.impl.ConditionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link conditions.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DocumentRootImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 0;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Condition Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CONDITION_SET = 3;

	/**
	 * The feature id for the '<em><b>Conditions Document</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__CONDITIONS_DOCUMENT = 4;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ConditionSetImpl <em>Condition Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ConditionSetImpl
	 * @see conditions.impl.ConditionsPackageImpl#getConditionSet()
	 * @generated
	 */
	int CONDITION_SET = 1;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Baureihe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__BAUREIHE = 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__DESCRIPTION = 2;

	/**
	 * The feature id for the '<em><b>Istufe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__ISTUFE = 3;

	/**
	 * The feature id for the '<em><b>Schemaversion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__SCHEMAVERSION = 4;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__UUID = 5;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__VERSION = 6;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__CONDITIONS = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET__NAME = 8;

	/**
	 * The number of structural features of the '<em>Condition Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Condition Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.BaseClassWithIDImpl <em>Base Class With ID</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.BaseClassWithIDImpl
	 * @see conditions.impl.ConditionsPackageImpl#getBaseClassWithID()
	 * @generated
	 */
	int BASE_CLASS_WITH_ID = 92;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_ID__ID = 0;

	/**
	 * The number of structural features of the '<em>Base Class With ID</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_ID_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Base Class With ID</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_ID_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ConditionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__ID = BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__EXPRESSION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___GET_READ_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___GET_WRITE_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___GET_STATE_DEPENDENCIES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___GET_TRANSITION_DEPENDENCIES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_ID_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_ID_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_OPERATION_COUNT = BASE_CLASS_WITH_ID_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link conditions.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__ID = BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION___GET_READ_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION___GET_WRITE_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION___GET_STATE_DEPENDENCIES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION___GET_TRANSITION_DEPENDENCIES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_ID_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_OPERATION_COUNT = BASE_CLASS_WITH_ID_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link conditions.impl.SignalComparisonExpressionImpl <em>Signal Comparison Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SignalComparisonExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSignalComparisonExpression()
	 * @generated
	 */
	int SIGNAL_COMPARISON_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Comparator Signal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Signal Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_EVALUATION_DATA_TYPE = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_OPERAND_DATA_TYPE = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___GET_OPERANDS = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Comparator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___IS_VALID_HAS_VALID_COMPARATOR__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Signal Comparison Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_COMPARISON_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link conditions.impl.CanMessageCheckExpressionImpl <em>Can Message Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.CanMessageCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getCanMessageCheckExpression()
	 * @generated
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Busid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__BUSID = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message IDs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Busid Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Rxtx Flag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG = EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Rxtx Flag Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Ext Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER = EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Can Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_MESSAGE_ID_RANGE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_CHECK_TYPE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Rx Tx Flag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_RX_TX_FLAG__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Ext Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_EXT_IDENTIFIER__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Can Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link conditions.impl.LinMessageCheckExpressionImpl <em>Lin Message Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.LinMessageCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getLinMessageCheckExpression()
	 * @generated
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Busid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__BUSID = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message IDs</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Busid Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Lin Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_MESSAGE_ID_RANGE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_CHECK_TYPE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Lin Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link conditions.impl.StateCheckExpressionImpl <em>State Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StateCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStateCheckExpression()
	 * @generated
	 */
	int STATE_CHECK_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Check State Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Check State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION__CHECK_STATE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Check State Active Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>State Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid States Active</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STATES_ACTIVE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STATE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>State Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.TimingExpressionImpl <em>Timing Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TimingExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTimingExpression()
	 * @generated
	 */
	int TIMING_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Maxtime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION__MAXTIME = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mintime</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION__MINTIME = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Timing Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Min Max Times</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION___IS_VALID_HAS_VALID_MIN_MAX_TIMES__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Timing Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.TrueExpressionImpl <em>True Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TrueExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTrueExpression()
	 * @generated
	 */
	int TRUE_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>True Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>True Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUE_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.LogicalExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getLogicalExpression()
	 * @generated
	 */
	int LOGICAL_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__EXPRESSIONS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Logical Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___IS_VALID_HAS_VALID_OPERATOR__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Sub Expressions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION___IS_VALID_HAS_VALID_SUB_EXPRESSIONS__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Logical Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.ReferenceConditionExpressionImpl <em>Reference Condition Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ReferenceConditionExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getReferenceConditionExpression()
	 * @generated
	 */
	int REFERENCE_CONDITION_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION__CONDITION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Condition Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Referenced Condition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION___IS_VALID_HAS_VALID_REFERENCED_CONDITION__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Reference Condition Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_CONDITION_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.TransitionCheckExpressionImpl <em>Transition Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TransitionCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTransitionCheckExpression()
	 * @generated
	 */
	int TRANSITION_CHECK_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Stay Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Check Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Stay Active Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Transition Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Stay Active</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STAY_ACTIVE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Transition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION___IS_VALID_HAS_VALID_TRANSITION__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Transition Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.FlexRayMessageCheckExpressionImpl <em>Flex Ray Message Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.FlexRayMessageCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getFlexRayMessageCheckExpression()
	 * @generated
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Payload Preamble</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Zero Frame</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sync Frame</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Startup Frame</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME = EXPRESSION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Network Mgmt</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT = EXPRESSION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Flexray Message Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID = EXPRESSION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Payload Preamble Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Zero Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Sync Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Startup Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Network Mgmt Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>Flex Ray Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 15;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_CHECK_TYPE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Startup Frame</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_STARTUP_FRAME__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Sync Frame</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_SYNC_FRAME__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Zero Frame</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_ZERO_FRAME__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Payload Preamble</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_PAYLOAD_PREAMBLE__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Network Mgmt</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_NETWORK_MGMT__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_MESSAGE_ID__DIAGNOSTICCHAIN_MAP = EXPRESSION_OPERATION_COUNT + 7;

	/**
	 * The number of operations of the '<em>Flex Ray Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 8;

	/**
	 * The meta object id for the '{@link conditions.impl.StopExpressionImpl <em>Stop Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StopExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStopExpression()
	 * @generated
	 */
	int STOP_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Analyse Art</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION__ANALYSE_ART = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Analyse Art Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Stop Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Stop Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter
	 * @see conditions.impl.ConditionsPackageImpl#getIVariableReaderWriter()
	 * @generated
	 */
	int IVARIABLE_READER_WRITER = 94;

	/**
	 * The number of structural features of the '<em>IVariable Reader Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVARIABLE_READER_WRITER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVARIABLE_READER_WRITER___GET_READ_VARIABLES = 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVARIABLE_READER_WRITER___GET_WRITE_VARIABLES = 1;

	/**
	 * The number of operations of the '<em>IVariable Reader Writer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVARIABLE_READER_WRITER_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link conditions.IOperand <em>IOperand</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperand
	 * @see conditions.impl.ConditionsPackageImpl#getIOperand()
	 * @generated
	 */
	int IOPERAND = 33;

	/**
	 * The number of structural features of the '<em>IOperand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERAND_FEATURE_COUNT = IVARIABLE_READER_WRITER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERAND___GET_READ_VARIABLES = IVARIABLE_READER_WRITER___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERAND___GET_WRITE_VARIABLES = IVARIABLE_READER_WRITER___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERAND___GET_EVALUATION_DATA_TYPE = IVARIABLE_READER_WRITER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>IOperand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERAND_OPERATION_COUNT = IVARIABLE_READER_WRITER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison Expression Operand</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ISignalComparisonExpressionOperand
	 * @see conditions.impl.ConditionsPackageImpl#getISignalComparisonExpressionOperand()
	 * @generated
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND = 36;

	/**
	 * The number of structural features of the '<em>ISignal Comparison Expression Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_READ_VARIABLES = IOPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_WRITE_VARIABLES = IOPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_EVALUATION_DATA_TYPE = IOPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>ISignal Comparison Expression Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_COMPARISON_EXPRESSION_OPERAND_OPERATION_COUNT = IOPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ComparatorSignalImpl <em>Comparator Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ComparatorSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getComparatorSignal()
	 * @generated
	 */
	int COMPARATOR_SIGNAL = 14;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL__DESCRIPTION = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Comparator Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL_FEATURE_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL___GET_READ_VARIABLES = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL___GET_WRITE_VARIABLES = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>Comparator Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPARATOR_SIGNAL_OPERATION_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ConstantComparatorValueImpl <em>Constant Comparator Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ConstantComparatorValueImpl
	 * @see conditions.impl.ConditionsPackageImpl#getConstantComparatorValue()
	 * @generated
	 */
	int CONSTANT_COMPARATOR_VALUE = 15;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE__VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interpreted Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Constant Comparator Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE___GET_READ_VARIABLES = COMPARATOR_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE___GET_WRITE_VARIABLES = COMPARATOR_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE___GET_EVALUATION_DATA_TYPE = COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE___IS_VALID_HAS_VALID_VALUE__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Constant Comparator Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_COMPARATOR_VALUE_OPERATION_COUNT = COMPARATOR_SIGNAL_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.CalculationExpressionImpl <em>Calculation Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.CalculationExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getCalculationExpression()
	 * @generated
	 */
	int CALCULATION_EXPRESSION = 16;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION__EXPRESSION = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION__OPERANDS = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Calculation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___GET_READ_VARIABLES = COMPARATOR_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___GET_WRITE_VARIABLES = COMPARATOR_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___GET_EVALUATION_DATA_TYPE = COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___GET_OPERAND_DATA_TYPE = COMPARATOR_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___GET_OPERANDS = COMPARATOR_SIGNAL_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___IS_VALID_HAS_VALID_EXPRESSION__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Calculation Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALCULATION_EXPRESSION_OPERATION_COUNT = COMPARATOR_SIGNAL_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link conditions.impl.VariableReferenceImpl <em>Variable Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VariableReferenceImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVariableReference()
	 * @generated
	 */
	int VARIABLE_REFERENCE = 18;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE__VARIABLE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE___GET_READ_VARIABLES = COMPARATOR_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE___GET_WRITE_VARIABLES = COMPARATOR_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE___GET_EVALUATION_DATA_TYPE = COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>Variable Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_OPERATION_COUNT = COMPARATOR_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ConditionsDocumentImpl <em>Document</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ConditionsDocumentImpl
	 * @see conditions.impl.ConditionsPackageImpl#getConditionsDocument()
	 * @generated
	 */
	int CONDITIONS_DOCUMENT = 19;

	/**
	 * The feature id for the '<em><b>Signal References Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET = 0;

	/**
	 * The feature id for the '<em><b>Variable Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_DOCUMENT__VARIABLE_SET = 1;

	/**
	 * The feature id for the '<em><b>Condition Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_DOCUMENT__CONDITION_SET = 2;

	/**
	 * The number of structural features of the '<em>Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_DOCUMENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONS_DOCUMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.VariableSetImpl <em>Variable Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VariableSetImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVariableSet()
	 * @generated
	 */
	int VARIABLE_SET = 20;

	/**
	 * The feature id for the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SET__UUID = 0;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SET__VARIABLES = 1;

	/**
	 * The number of structural features of the '<em>Variable Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SET_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SET___IS_VALID_HAS_VALID_VALUE_RANGES__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The number of operations of the '<em>Variable Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_SET_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractVariableImpl <em>Abstract Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractVariableImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractVariable()
	 * @generated
	 */
	int ABSTRACT_VARIABLE = 108;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE__ID = BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE__NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE__DISPLAY_NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE___GET_READ_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE___GET_WRITE_VARIABLES = BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_VARIABLE_OPERATION_COUNT = BASE_CLASS_WITH_ID_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link conditions.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VariableImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 21;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__ID = ABSTRACT_VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = ABSTRACT_VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__DISPLAY_NAME = ABSTRACT_VARIABLE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__DESCRIPTION = ABSTRACT_VARIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__DATA_TYPE = ABSTRACT_VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__UNIT = ABSTRACT_VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__VARIABLE_FORMAT = ABSTRACT_VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = ABSTRACT_VARIABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = ABSTRACT_VARIABLE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___GET_READ_VARIABLES = ABSTRACT_VARIABLE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___GET_WRITE_VARIABLES = ABSTRACT_VARIABLE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Variable Format Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Observers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE___GET_OBSERVERS = ABSTRACT_VARIABLE_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_OPERATION_COUNT = ABSTRACT_VARIABLE_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link conditions.impl.SignalVariableImpl <em>Signal Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SignalVariableImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSignalVariable()
	 * @generated
	 */
	int SIGNAL_VARIABLE = 22;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__UNIT = VARIABLE__UNIT;

	/**
	 * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;

	/**
	 * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Interpreted Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__INTERPRETED_VALUE = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__SIGNAL = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM = VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Signal Observers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__SIGNAL_OBSERVERS = VARIABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Lag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE__LAG = VARIABLE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Signal Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___GET_READ_VARIABLES = VARIABLE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___GET_WRITE_VARIABLES = VARIABLE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Variable Format Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Observers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE___GET_OBSERVERS = VARIABLE___GET_OBSERVERS;

	/**
	 * The number of operations of the '<em>Signal Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ValueVariableImpl <em>Value Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ValueVariableImpl
	 * @see conditions.impl.ConditionsPackageImpl#getValueVariable()
	 * @generated
	 */
	int VALUE_VARIABLE = 23;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__UNIT = VARIABLE__UNIT;

	/**
	 * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;

	/**
	 * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__INITIAL_VALUE = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Variable Observers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Value Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___GET_READ_VARIABLES = VARIABLE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___GET_WRITE_VARIABLES = VARIABLE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Variable Format Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Observers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE___GET_OBSERVERS = VARIABLE___GET_OBSERVERS;

	/**
	 * The number of operations of the '<em>Value Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.VariableFormatImpl <em>Variable Format</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VariableFormatImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVariableFormat()
	 * @generated
	 */
	int VARIABLE_FORMAT = 24;

	/**
	 * The feature id for the '<em><b>Digits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT__DIGITS = 0;

	/**
	 * The feature id for the '<em><b>Base Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT__BASE_DATA_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Upper Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT__UPPER_CASE = 2;

	/**
	 * The number of structural features of the '<em>Variable Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Digits</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT___IS_VALID_HAS_VALID_DIGITS__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The number of operations of the '<em>Variable Format</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FORMAT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link conditions.impl.BaseClassWithSourceReferenceImpl <em>Base Class With Source Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.BaseClassWithSourceReferenceImpl
	 * @see conditions.impl.ConditionsPackageImpl#getBaseClassWithSourceReference()
	 * @generated
	 */
	int BASE_CLASS_WITH_SOURCE_REFERENCE = 97;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_SOURCE_REFERENCE__ID = BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Base Class With Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Base Class With Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT = BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractObserverImpl <em>Abstract Observer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractObserverImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractObserver()
	 * @generated
	 */
	int ABSTRACT_OBSERVER = 25;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__DESCRIPTION = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__LOG_LEVEL = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__VALUE_RANGES = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Active At Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER__ACTIVE_AT_START = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Abstract Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___GET_IDENTIFIER = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___GET_IDENTIFIER__STRING = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 5;

	/**
	 * The number of operations of the '<em>Abstract Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OBSERVER_OPERATION_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link conditions.impl.ObserverValueRangeImpl <em>Observer Value Range</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ObserverValueRangeImpl
	 * @see conditions.impl.ConditionsPackageImpl#getObserverValueRange()
	 * @generated
	 */
	int OBSERVER_VALUE_RANGE = 26;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE__VALUE = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE__VALUE_TYPE = 2;

	/**
	 * The number of structural features of the '<em>Observer Value Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE___IS_VALID_HAS_VALID_VALUE__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = 1;

	/**
	 * The number of operations of the '<em>Observer Value Range</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_VALUE_RANGE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link conditions.impl.SignalObserverImpl <em>Signal Observer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SignalObserverImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSignalObserver()
	 * @generated
	 */
	int SIGNAL_OBSERVER = 27;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__ID = ABSTRACT_OBSERVER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__SOURCE_REFERENCE = ABSTRACT_OBSERVER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__NAME = ABSTRACT_OBSERVER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__DESCRIPTION = ABSTRACT_OBSERVER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__LOG_LEVEL = ABSTRACT_OBSERVER__LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__VALUE_RANGES = ABSTRACT_OBSERVER__VALUE_RANGES;

	/**
	 * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__VALUE_RANGES_TMPL_PARAM = ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Active At Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER__ACTIVE_AT_START = ABSTRACT_OBSERVER__ACTIVE_AT_START;

	/**
	 * The number of structural features of the '<em>Signal Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER_FEATURE_COUNT = ABSTRACT_OBSERVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___GET_IDENTIFIER = ABSTRACT_OBSERVER___GET_IDENTIFIER;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___GET_IDENTIFIER__STRING = ABSTRACT_OBSERVER___GET_IDENTIFIER__STRING;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Signal Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_OBSERVER_OPERATION_COUNT = ABSTRACT_OBSERVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.SignalReferenceSetImpl <em>Signal Reference Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SignalReferenceSetImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSignalReferenceSet()
	 * @generated
	 */
	int SIGNAL_REFERENCE_SET = 28;

	/**
	 * The feature id for the '<em><b>Messages</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE_SET__MESSAGES = 0;

	/**
	 * The number of structural features of the '<em>Signal Reference Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE_SET_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Signal Reference Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.SignalReferenceImpl <em>Signal Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SignalReferenceImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSignalReference()
	 * @generated
	 */
	int SIGNAL_REFERENCE = 29;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE__SIGNAL = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Signal Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE___GET_READ_VARIABLES = COMPARATOR_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE___GET_WRITE_VARIABLES = COMPARATOR_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE___GET_EVALUATION_DATA_TYPE = COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Is Valid has Valid Signal</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE___IS_VALID_HAS_VALID_SIGNAL__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Signal Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIGNAL_REFERENCE_OPERATION_COUNT = COMPARATOR_SIGNAL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.ISignalOrReference <em>ISignal Or Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ISignalOrReference
	 * @see conditions.impl.ConditionsPackageImpl#getISignalOrReference()
	 * @generated
	 */
	int ISIGNAL_OR_REFERENCE = 30;

	/**
	 * The number of structural features of the '<em>ISignal Or Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_OR_REFERENCE_FEATURE_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_OR_REFERENCE___GET_READ_VARIABLES = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_OR_REFERENCE___GET_WRITE_VARIABLES = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_OR_REFERENCE___GET_EVALUATION_DATA_TYPE = ISIGNAL_COMPARISON_EXPRESSION_OPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>ISignal Or Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISIGNAL_OR_REFERENCE_OPERATION_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.BitPatternComparatorValueImpl <em>Bit Pattern Comparator Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.BitPatternComparatorValueImpl
	 * @see conditions.impl.ConditionsPackageImpl#getBitPatternComparatorValue()
	 * @generated
	 */
	int BIT_PATTERN_COMPARATOR_VALUE = 31;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE__VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Startbit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE__STARTBIT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Bit Pattern Comparator Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE___GET_READ_VARIABLES = COMPARATOR_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE___GET_WRITE_VARIABLES = COMPARATOR_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE___GET_EVALUATION_DATA_TYPE = COMPARATOR_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bit Pattern</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE___IS_VALID_HAS_VALID_BIT_PATTERN__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Startbit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE___IS_VALID_HAS_VALID_STARTBIT__DIAGNOSTICCHAIN_MAP = COMPARATOR_SIGNAL_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Bit Pattern Comparator Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BIT_PATTERN_COMPARATOR_VALUE_OPERATION_COUNT = COMPARATOR_SIGNAL_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.NotExpressionImpl <em>Not Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.NotExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getNotExpression()
	 * @generated
	 */
	int NOT_EXPRESSION = 32;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Not Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.IComputeVariableActionOperand <em>ICompute Variable Action Operand</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IComputeVariableActionOperand
	 * @see conditions.impl.ConditionsPackageImpl#getIComputeVariableActionOperand()
	 * @generated
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND = 34;

	/**
	 * The number of structural features of the '<em>ICompute Variable Action Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND___GET_READ_VARIABLES = IOPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND___GET_WRITE_VARIABLES = IOPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND___GET_EVALUATION_DATA_TYPE = IOPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>ICompute Variable Action Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ICOMPUTE_VARIABLE_ACTION_OPERAND_OPERATION_COUNT = IOPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.IStringOperand <em>IString Operand</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStringOperand
	 * @see conditions.impl.ConditionsPackageImpl#getIStringOperand()
	 * @generated
	 */
	int ISTRING_OPERAND = 35;

	/**
	 * The number of structural features of the '<em>IString Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERAND___GET_READ_VARIABLES = IOPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERAND___GET_WRITE_VARIABLES = IOPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERAND___GET_EVALUATION_DATA_TYPE = IOPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>IString Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERAND_OPERATION_COUNT = IOPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.IOperation <em>IOperation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation
	 * @see conditions.impl.ConditionsPackageImpl#getIOperation()
	 * @generated
	 */
	int IOPERATION = 37;

	/**
	 * The number of structural features of the '<em>IOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION___GET_READ_VARIABLES = IOPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION___GET_WRITE_VARIABLES = IOPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION___GET_EVALUATION_DATA_TYPE = IOPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION___GET_OPERAND_DATA_TYPE = IOPERAND_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION___GET_OPERANDS = IOPERAND_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>IOperation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOPERATION_OPERATION_COUNT = IOPERAND_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.IStringOperation <em>IString Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStringOperation
	 * @see conditions.impl.ConditionsPackageImpl#getIStringOperation()
	 * @generated
	 */
	int ISTRING_OPERATION = 38;

	/**
	 * The number of structural features of the '<em>IString Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION_FEATURE_COUNT = IOPERATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION___GET_READ_VARIABLES = IOPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION___GET_WRITE_VARIABLES = IOPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION___GET_EVALUATION_DATA_TYPE = IOPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION___GET_OPERAND_DATA_TYPE = IOPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION___GET_OPERANDS = IOPERATION___GET_OPERANDS;

	/**
	 * The number of operations of the '<em>IString Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTRING_OPERATION_OPERATION_COUNT = IOPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.INumericOperand <em>INumeric Operand</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.INumericOperand
	 * @see conditions.impl.ConditionsPackageImpl#getINumericOperand()
	 * @generated
	 */
	int INUMERIC_OPERAND = 39;

	/**
	 * The number of structural features of the '<em>INumeric Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERAND___GET_READ_VARIABLES = IOPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERAND___GET_WRITE_VARIABLES = IOPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERAND___GET_EVALUATION_DATA_TYPE = IOPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The number of operations of the '<em>INumeric Operand</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERAND_OPERATION_COUNT = IOPERAND_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.INumericOperation <em>INumeric Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.INumericOperation
	 * @see conditions.impl.ConditionsPackageImpl#getINumericOperation()
	 * @generated
	 */
	int INUMERIC_OPERATION = 40;

	/**
	 * The number of structural features of the '<em>INumeric Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION_FEATURE_COUNT = INUMERIC_OPERAND_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION___GET_READ_VARIABLES = INUMERIC_OPERAND___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION___GET_WRITE_VARIABLES = INUMERIC_OPERAND___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION___GET_EVALUATION_DATA_TYPE = INUMERIC_OPERAND___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION___GET_OPERAND_DATA_TYPE = INUMERIC_OPERAND_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION___GET_OPERANDS = INUMERIC_OPERAND_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>INumeric Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INUMERIC_OPERATION_OPERATION_COUNT = INUMERIC_OPERAND_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.ValueVariableObserverImpl <em>Value Variable Observer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ValueVariableObserverImpl
	 * @see conditions.impl.ConditionsPackageImpl#getValueVariableObserver()
	 * @generated
	 */
	int VALUE_VARIABLE_OBSERVER = 41;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__ID = ABSTRACT_OBSERVER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__SOURCE_REFERENCE = ABSTRACT_OBSERVER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__NAME = ABSTRACT_OBSERVER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__DESCRIPTION = ABSTRACT_OBSERVER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__LOG_LEVEL = ABSTRACT_OBSERVER__LOG_LEVEL;

	/**
	 * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__VALUE_RANGES = ABSTRACT_OBSERVER__VALUE_RANGES;

	/**
	 * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__VALUE_RANGES_TMPL_PARAM = ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Active At Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER__ACTIVE_AT_START = ABSTRACT_OBSERVER__ACTIVE_AT_START;

	/**
	 * The number of structural features of the '<em>Value Variable Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER_FEATURE_COUNT = ABSTRACT_OBSERVER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___GET_IDENTIFIER = ABSTRACT_OBSERVER___GET_IDENTIFIER;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___GET_IDENTIFIER__STRING = ABSTRACT_OBSERVER___GET_IDENTIFIER__STRING;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value Ranges Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Value Variable Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_VARIABLE_OBSERVER_OPERATION_COUNT = ABSTRACT_OBSERVER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ParseStringToDoubleImpl <em>Parse String To Double</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ParseStringToDoubleImpl
	 * @see conditions.impl.ConditionsPackageImpl#getParseStringToDouble()
	 * @generated
	 */
	int PARSE_STRING_TO_DOUBLE = 42;

	/**
	 * The feature id for the '<em><b>String To Parse</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE = INUMERIC_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parse String To Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE_FEATURE_COUNT = INUMERIC_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE___GET_READ_VARIABLES = INUMERIC_OPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE___GET_WRITE_VARIABLES = INUMERIC_OPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE___GET_EVALUATION_DATA_TYPE = INUMERIC_OPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE___GET_OPERAND_DATA_TYPE = INUMERIC_OPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE___GET_OPERANDS = INUMERIC_OPERATION___GET_OPERANDS;

	/**
	 * The number of operations of the '<em>Parse String To Double</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_STRING_TO_DOUBLE_OPERATION_COUNT = INUMERIC_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.StringExpressionImpl <em>String Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StringExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStringExpression()
	 * @generated
	 */
	int STRING_EXPRESSION = 43;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The number of structural features of the '<em>String Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>String Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.MatchesImpl <em>Matches</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.MatchesImpl
	 * @see conditions.impl.ConditionsPackageImpl#getMatches()
	 * @generated
	 */
	int MATCHES = 44;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES__ID = STRING_EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES__DESCRIPTION = STRING_EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Regex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES__REGEX = STRING_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES__DYNAMIC_REGEX = STRING_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>String To Check</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES__STRING_TO_CHECK = STRING_EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Matches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_FEATURE_COUNT = STRING_EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___GET_READ_VARIABLES = STRING_EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___GET_WRITE_VARIABLES = STRING_EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___GET_STATE_DEPENDENCIES = STRING_EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___GET_TRANSITION_DEPENDENCIES = STRING_EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = STRING_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Regex</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP = STRING_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid String To Check</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES___IS_VALID_HAS_VALID_STRING_TO_CHECK__DIAGNOSTICCHAIN_MAP = STRING_EXPRESSION_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Matches</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCHES_OPERATION_COUNT = STRING_EXPRESSION_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.ExtractImpl <em>Extract</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ExtractImpl
	 * @see conditions.impl.ConditionsPackageImpl#getExtract()
	 * @generated
	 */
	int EXTRACT = 45;

	/**
	 * The feature id for the '<em><b>Regex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT__REGEX = ISTRING_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT__DYNAMIC_REGEX = ISTRING_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>String To Extract</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT__STRING_TO_EXTRACT = ISTRING_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Group Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT__GROUP_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Extract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___GET_READ_VARIABLES = ISTRING_OPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___GET_WRITE_VARIABLES = ISTRING_OPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___GET_EVALUATION_DATA_TYPE = ISTRING_OPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___GET_OPERAND_DATA_TYPE = ISTRING_OPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___GET_OPERANDS = ISTRING_OPERATION___GET_OPERANDS;

	/**
	 * The operation id for the '<em>Is Valid has Valid Regex</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP = ISTRING_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Extract</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_OPERATION_COUNT = ISTRING_OPERATION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.SubstringImpl <em>Substring</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SubstringImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSubstring()
	 * @generated
	 */
	int SUBSTRING = 46;

	/**
	 * The feature id for the '<em><b>String To Extract</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__STRING_TO_EXTRACT = ISTRING_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__START_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING__END_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Substring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING___GET_READ_VARIABLES = ISTRING_OPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING___GET_WRITE_VARIABLES = ISTRING_OPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING___GET_EVALUATION_DATA_TYPE = ISTRING_OPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING___GET_OPERAND_DATA_TYPE = ISTRING_OPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING___GET_OPERANDS = ISTRING_OPERATION___GET_OPERANDS;

	/**
	 * The number of operations of the '<em>Substring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSTRING_OPERATION_COUNT = ISTRING_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ParseNumericToStringImpl <em>Parse Numeric To String</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ParseNumericToStringImpl
	 * @see conditions.impl.ConditionsPackageImpl#getParseNumericToString()
	 * @generated
	 */
	int PARSE_NUMERIC_TO_STRING = 47;

	/**
	 * The feature id for the '<em><b>Numeric Operand To Be Parsed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED = ISTRING_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parse Numeric To String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING___GET_READ_VARIABLES = ISTRING_OPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING___GET_WRITE_VARIABLES = ISTRING_OPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING___GET_EVALUATION_DATA_TYPE = ISTRING_OPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING___GET_OPERAND_DATA_TYPE = ISTRING_OPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING___GET_OPERANDS = ISTRING_OPERATION___GET_OPERANDS;

	/**
	 * The number of operations of the '<em>Parse Numeric To String</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARSE_NUMERIC_TO_STRING_OPERATION_COUNT = ISTRING_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.StringLengthImpl <em>String Length</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StringLengthImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStringLength()
	 * @generated
	 */
	int STRING_LENGTH = 48;

	/**
	 * The feature id for the '<em><b>String Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH__STRING_OPERAND = INUMERIC_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Length</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH_FEATURE_COUNT = INUMERIC_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH___GET_READ_VARIABLES = INUMERIC_OPERATION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH___GET_WRITE_VARIABLES = INUMERIC_OPERATION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH___GET_EVALUATION_DATA_TYPE = INUMERIC_OPERATION___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH___GET_OPERAND_DATA_TYPE = INUMERIC_OPERATION___GET_OPERAND_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH___GET_OPERANDS = INUMERIC_OPERATION___GET_OPERANDS;

	/**
	 * The number of operations of the '<em>String Length</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_LENGTH_OPERATION_COUNT = INUMERIC_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractMessageImpl <em>Abstract Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractMessage()
	 * @generated
	 */
	int ABSTRACT_MESSAGE = 49;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE__SIGNALS = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE___GET_PRIMARY_FILTER = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE___GET_FILTERS__BOOLEAN = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_MESSAGE_OPERATION_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractBusMessageImpl <em>Abstract Bus Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractBusMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractBusMessage()
	 * @generated
	 */
	int ABSTRACT_BUS_MESSAGE = 84;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__ID = ABSTRACT_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE = ABSTRACT_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__SIGNALS = ABSTRACT_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__NAME = ABSTRACT_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__BUS_ID = ABSTRACT_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE = ABSTRACT_MESSAGE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE__FILTERS = ABSTRACT_MESSAGE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Abstract Bus Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE_FEATURE_COUNT = ABSTRACT_MESSAGE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_MESSAGE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Bus Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_BUS_MESSAGE_OPERATION_COUNT = ABSTRACT_MESSAGE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.SomeIPMessageImpl <em>Some IP Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SomeIPMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPMessage()
	 * @generated
	 */
	int SOME_IP_MESSAGE = 50;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Some IP Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE__SOME_IP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Some IP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Some IP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractSignalImpl <em>Abstract Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractSignal()
	 * @generated
	 */
	int ABSTRACT_SIGNAL = 51;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL__DECODE_STRATEGY = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL__EXTRACT_STRATEGY = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL___GET_READ_VARIABLES = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL___GET_WRITE_VARIABLES = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL___GET_EVALUATION_DATA_TYPE = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL___GET_MESSAGE = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_SIGNAL_OPERATION_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link conditions.impl.ContainerSignalImpl <em>Container Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ContainerSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getContainerSignal()
	 * @generated
	 */
	int CONTAINER_SIGNAL = 52;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__ID = ABSTRACT_SIGNAL__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;

	/**
	 * The feature id for the '<em><b>Contained Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL__CONTAINED_SIGNALS = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Container Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL___GET_READ_VARIABLES = ABSTRACT_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL___GET_WRITE_VARIABLES = ABSTRACT_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL___GET_EVALUATION_DATA_TYPE = ABSTRACT_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL___GET_MESSAGE = ABSTRACT_SIGNAL___GET_MESSAGE;

	/**
	 * The number of operations of the '<em>Container Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_SIGNAL_OPERATION_COUNT = ABSTRACT_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.AbstractFilterImpl <em>Abstract Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.AbstractFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getAbstractFilter()
	 * @generated
	 */
	int ABSTRACT_FILTER = 53;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FILTER__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FILTER__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FILTER__PAYLOAD_LENGTH = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Abstract Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FILTER_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Abstract Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FILTER_OPERATION_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.EthernetFilterImpl <em>Ethernet Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.EthernetFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getEthernetFilter()
	 * @generated
	 */
	int ETHERNET_FILTER = 54;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Source MAC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__SOURCE_MAC = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dest MAC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__DEST_MAC = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ether Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__ETHER_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Inner Vlan Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__INNER_VLAN_ID = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Outer Vlan Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__OUTER_VLAN_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Inner Vlan CFI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__INNER_VLAN_CFI = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Outer Vlan CFI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__OUTER_VLAN_CFI = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Inner Vlan VID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__INNER_VLAN_VID = ABSTRACT_FILTER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Outer Vlan VID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__OUTER_VLAN_VID = ABSTRACT_FILTER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>CRC</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__CRC = ABSTRACT_FILTER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Inner Vlan TPID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__INNER_VLAN_TPID = ABSTRACT_FILTER_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Outer Vlan TPID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER__OUTER_VLAN_TPID = ABSTRACT_FILTER_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Ethernet Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 12;

	/**
	 * The operation id for the '<em>Is Valid has Valid Source MAC</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_SOURCE_MAC__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Destination MAC</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_DESTINATION_MAC__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid CRC</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_CRC__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Ether Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_ETHER_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Inner Vlan Vid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_VID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Outer Vlan Vid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_VID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Inner Vlan CFI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_CFI__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Outer Vlan CFI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_CFI__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is Valid has Valid Inner Vlan PCP</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_PCP__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Is Valid has Valid Outer Vlan PCP</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_PCP__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Is Valid has Valid Inner Vlan TPID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_TPID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Is Valid has Valid Outer Vlan TPID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_TPID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 11;

	/**
	 * The number of operations of the '<em>Ethernet Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 12;

	/**
	 * The meta object id for the '{@link conditions.impl.TPFilterImpl <em>TP Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TPFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTPFilter()
	 * @generated
	 */
	int TP_FILTER = 96;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Source Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER__SOURCE_PORT = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dest Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER__DEST_PORT = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>TP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>TP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TP_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.UDPFilterImpl <em>UDP Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UDPFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUDPFilter()
	 * @generated
	 */
	int UDP_FILTER = 55;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__ID = TP_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__SOURCE_REFERENCE = TP_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__PAYLOAD_LENGTH = TP_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Source Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__SOURCE_PORT = TP_FILTER__SOURCE_PORT;

	/**
	 * The feature id for the '<em><b>Dest Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__DEST_PORT = TP_FILTER__DEST_PORT;

	/**
	 * The feature id for the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER__CHECKSUM = TP_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>UDP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER_FEATURE_COUNT = TP_FILTER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Source Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER___IS_VALID_HAS_VALID_SOURCE_PORT__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Destination Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER___IS_VALID_HAS_VALID_DESTINATION_PORT__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Checksum</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER___IS_VALID_HAS_VALID_CHECKSUM__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>UDP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_FILTER_OPERATION_COUNT = TP_FILTER_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link conditions.impl.TCPFilterImpl <em>TCP Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TCPFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTCPFilter()
	 * @generated
	 */
	int TCP_FILTER = 56;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__ID = TP_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__SOURCE_REFERENCE = TP_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__PAYLOAD_LENGTH = TP_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Source Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__SOURCE_PORT = TP_FILTER__SOURCE_PORT;

	/**
	 * The feature id for the '<em><b>Dest Port</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__DEST_PORT = TP_FILTER__DEST_PORT;

	/**
	 * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__SEQUENCE_NUMBER = TP_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Acknowledgement Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__ACKNOWLEDGEMENT_NUMBER = TP_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tcp Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__TCP_FLAGS = TP_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Stream Analysis Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__STREAM_ANALYSIS_FLAGS = TP_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Stream Analysis Flags Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM = TP_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Stream Valid Payload Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET = TP_FILTER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>TCP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER_FEATURE_COUNT = TP_FILTER_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Source Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_SOURCE_PORT__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Destination Port</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_DESTINATION_PORT__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Sequence Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_SEQUENCE_NUMBER__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Acknowledgement Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_ACKNOWLEDGEMENT_NUMBER__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Tcp Flag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_TCP_FLAG__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Stream Analysis Flag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_STREAM_ANALYSIS_FLAG__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Stream Valid Payload Offset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER___IS_VALID_HAS_VALID_STREAM_VALID_PAYLOAD_OFFSET__DIAGNOSTICCHAIN_MAP = TP_FILTER_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>TCP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_FILTER_OPERATION_COUNT = TP_FILTER_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link conditions.impl.IPv4FilterImpl <em>IPv4 Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.IPv4FilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getIPv4Filter()
	 * @generated
	 */
	int IPV4_FILTER = 57;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Protocol Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__PROTOCOL_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Protocol Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source IP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__SOURCE_IP = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dest IP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__DEST_IP = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Time To Live</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER__TIME_TO_LIVE = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>IPv4 Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Source Ip Address</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER___IS_VALID_HAS_VALID_SOURCE_IP_ADDRESS__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Destination Ip Address</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER___IS_VALID_HAS_VALID_DESTINATION_IP_ADDRESS__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Time To Live</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER___IS_VALID_HAS_VALID_TIME_TO_LIVE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>IPv4 Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link conditions.impl.SomeIPFilterImpl <em>Some IP Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SomeIPFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPFilter()
	 * @generated
	 */
	int SOME_IP_FILTER = 58;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Service Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__SERVICE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Method Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__METHOD_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Method Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Method Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__METHOD_ID = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Client Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__CLIENT_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Session Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__SESSION_ID = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Protocol Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__PROTOCOL_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Interface Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__INTERFACE_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Message Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__MESSAGE_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Message Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Return Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__RETURN_CODE = ABSTRACT_FILTER_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Return Code Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Some IP Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER__SOME_IP_LENGTH = ABSTRACT_FILTER_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Some IP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Is Valid has Valid Service Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_SERVICE_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Method Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_METHOD_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Session Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_SESSION_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Client Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_CLIENT_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Length</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_LENGTH__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Protocol Version</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_PROTOCOL_VERSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interface Version</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_INTERFACE_VERSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_MESSAGE_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is Valid has Valid Return Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_RETURN_CODE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Is Valid has Valid Method Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER___IS_VALID_HAS_VALID_METHOD_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>Some IP Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IP_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 10;

	/**
	 * The meta object id for the '{@link conditions.impl.ForEachExpressionImpl <em>For Each Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ForEachExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getForEachExpression()
	 * @generated
	 */
	int FOR_EACH_EXPRESSION = 59;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Filter Expressions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Container Signal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION__CONTAINER_SIGNAL = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>For Each Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>For Each Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.MessageCheckExpressionImpl <em>Message Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.MessageCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getMessageCheckExpression()
	 * @generated
	 */
	int MESSAGE_CHECK_EXPRESSION = 60;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION__MESSAGE = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Message Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.SomeIPSDFilterImpl <em>Some IPSD Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SomeIPSDFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPSDFilter()
	 * @generated
	 */
	int SOME_IPSD_FILTER = 61;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Flags</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__FLAGS = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Flags Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__FLAGS_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sd Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__SD_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sd Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Instance Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__INSTANCE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ttl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__TTL = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Major Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__MAJOR_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Minor Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__MINOR_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Event Group Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__EVENT_GROUP_ID = ABSTRACT_FILTER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Index First Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__INDEX_FIRST_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Index Second Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__INDEX_SECOND_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Number First Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__NUMBER_FIRST_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Number Second Option</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__NUMBER_SECOND_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Service Id Some IPSD</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD = ABSTRACT_FILTER_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>Some IPSD Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>Is Valid has Valid Instance Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INSTANCE_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Event Group Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_EVENT_GROUP_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Flags</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_FLAGS__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Sd Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_SD_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Major Version</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_MAJOR_VERSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Minor Version</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_MINOR_VERSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Index First Option</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INDEX_FIRST_OPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Index Second Option</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_INDEX_SECOND_OPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is Valid has Valid Number First Option</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_NUMBER_FIRST_OPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Is Valid has Valid Number Second Option</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER___IS_VALID_HAS_VALID_NUMBER_SECOND_OPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 9;

	/**
	 * The number of operations of the '<em>Some IPSD Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 10;

	/**
	 * The meta object id for the '{@link conditions.impl.SomeIPSDMessageImpl <em>Some IPSD Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SomeIPSDMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPSDMessage()
	 * @generated
	 */
	int SOME_IPSD_MESSAGE = 62;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Some IPSD Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE__SOME_IPSD_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Some IPSD Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Some IPSD Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOME_IPSD_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.DoubleSignalImpl <em>Double Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DoubleSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDoubleSignal()
	 * @generated
	 */
	int DOUBLE_SIGNAL = 63;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__ID = ABSTRACT_SIGNAL__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;

	/**
	 * The feature id for the '<em><b>Ignore Invalid Values</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__IGNORE_INVALID_VALUES = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Double Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL___GET_READ_VARIABLES = ABSTRACT_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL___GET_WRITE_VARIABLES = ABSTRACT_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL___GET_EVALUATION_DATA_TYPE = ABSTRACT_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL___GET_MESSAGE = ABSTRACT_SIGNAL___GET_MESSAGE;

	/**
	 * The number of operations of the '<em>Double Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_SIGNAL_OPERATION_COUNT = ABSTRACT_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.StringSignalImpl <em>String Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StringSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStringSignal()
	 * @generated
	 */
	int STRING_SIGNAL = 64;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL__ID = ABSTRACT_SIGNAL__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;

	/**
	 * The number of structural features of the '<em>String Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL___GET_READ_VARIABLES = ABSTRACT_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL___GET_WRITE_VARIABLES = ABSTRACT_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL___GET_EVALUATION_DATA_TYPE = ABSTRACT_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL___GET_MESSAGE = ABSTRACT_SIGNAL___GET_MESSAGE;

	/**
	 * The number of operations of the '<em>String Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_SIGNAL_OPERATION_COUNT = ABSTRACT_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.DecodeStrategyImpl <em>Decode Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DecodeStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDecodeStrategy()
	 * @generated
	 */
	int DECODE_STRATEGY = 65;

	/**
	 * The number of structural features of the '<em>Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECODE_STRATEGY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECODE_STRATEGY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.DoubleDecodeStrategyImpl <em>Double Decode Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DoubleDecodeStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDoubleDecodeStrategy()
	 * @generated
	 */
	int DOUBLE_DECODE_STRATEGY = 66;

	/**
	 * The feature id for the '<em><b>Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY__FACTOR = DECODE_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY__OFFSET = DECODE_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Double Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Factor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY___IS_VALID_HAS_VALID_FACTOR__DIAGNOSTICCHAIN_MAP = DECODE_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Offset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY___IS_VALID_HAS_VALID_OFFSET__DIAGNOSTICCHAIN_MAP = DECODE_STRATEGY_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Double Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_DECODE_STRATEGY_OPERATION_COUNT = DECODE_STRATEGY_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.ExtractStrategyImpl <em>Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getExtractStrategy()
	 * @generated
	 */
	int EXTRACT_STRATEGY = 67;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_STRATEGY__ABSTRACT_SIGNAL = 0;

	/**
	 * The number of structural features of the '<em>Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_STRATEGY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRACT_STRATEGY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.UniversalPayloadExtractStrategyImpl <em>Universal Payload Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UniversalPayloadExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY = 68;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The feature id for the '<em><b>Extraction Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE = EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Byte Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER = EXTRACT_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Signal Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE = EXTRACT_STRATEGY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Universal Payload Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Extract String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_EXTRACT_STRING__DIAGNOSTICCHAIN_MAP = EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Signal Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE__DIAGNOSTICCHAIN_MAP = EXTRACT_STRATEGY_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Universal Payload Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_OPERATION_COUNT = EXTRACT_STRATEGY_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.EmptyExtractStrategyImpl <em>Empty Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.EmptyExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getEmptyExtractStrategy()
	 * @generated
	 */
	int EMPTY_EXTRACT_STRATEGY = 69;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The number of structural features of the '<em>Empty Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Empty Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_EXTRACT_STRATEGY_OPERATION_COUNT = EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.CANFilterImpl <em>CAN Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.CANFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getCANFilter()
	 * @generated
	 */
	int CAN_FILTER = 70;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Frame Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__FRAME_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Rxtx Flag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__RXTX_FLAG = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Rxtx Flag Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__RXTX_FLAG_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Ext Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__EXT_IDENTIFIER = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Frame Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__FRAME_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Frame Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER__FRAME_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>CAN Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER___IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Rx Tx Flag</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER___IS_VALID_HAS_VALID_RX_TX_FLAG__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Ext Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER___IS_VALID_HAS_VALID_EXT_IDENTIFIER__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>CAN Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link conditions.impl.LINFilterImpl <em>LIN Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.LINFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getLINFilter()
	 * @generated
	 */
	int LIN_FILTER = 71;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Frame Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER__FRAME_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>LIN Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER___IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>LIN Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.FlexRayFilterImpl <em>Flex Ray Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.FlexRayFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getFlexRayFilter()
	 * @generated
	 */
	int FLEX_RAY_FILTER = 72;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Slot Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__SLOT_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cycle Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__CYCLE_OFFSET = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cycle Repetition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__CYCLE_REPETITION = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Channel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__CHANNEL = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Channel Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Flex Ray Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER___IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Channel Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER___IS_VALID_HAS_VALID_CHANNEL_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Cycle Offset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER___IS_VALID_HAS_VALID_CYCLE_OFFSET__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Cycle Repeat Interval</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER___IS_VALID_HAS_VALID_CYCLE_REPEAT_INTERVAL__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Flex Ray Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link conditions.impl.DLTFilterImpl <em>DLT Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DLTFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDLTFilter()
	 * @generated
	 */
	int DLT_FILTER = 73;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Ecu ID ECU</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__ECU_ID_ECU = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Session ID SEID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__SESSION_ID_SEID = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Application ID APID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__APPLICATION_ID_APID = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Context ID CTID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__CONTEXT_ID_CTID = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Message Type MSTP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_TYPE_MSTP = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Message Log Info MSLI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_LOG_INFO_MSLI = ABSTRACT_FILTER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Message Trace Info MSTI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_TRACE_INFO_MSTI = ABSTRACT_FILTER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Message Bus Info MSBI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_BUS_INFO_MSBI = ABSTRACT_FILTER_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Message Control Info MSCI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI = ABSTRACT_FILTER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Message Log Info MSLI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Message Trace Info MSTI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Message Bus Info MSBI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Message Control Info MSCI Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Message Type MSTP Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>DLT Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 14;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Bus Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_BUS_INFO__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Control Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_CONTROL_INFO__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Log Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_LOG_INFO__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Trace Info</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_TRACE_INFO__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>DLT Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link conditions.impl.UDPNMFilterImpl <em>UDPNM Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UDPNMFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUDPNMFilter()
	 * @generated
	 */
	int UDPNM_FILTER = 74;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Source Node Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__SOURCE_NODE_IDENTIFIER = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Control Bit Vector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__CONTROL_BIT_VECTOR = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Pwf Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__PWF_STATUS = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Teilnetz Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER__TEILNETZ_STATUS = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>UDPNM Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Source Node Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER___IS_VALID_HAS_VALID_SOURCE_NODE_IDENTIFIER__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Control Bit Vector</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER___IS_VALID_HAS_VALID_CONTROL_BIT_VECTOR__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Pwf Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER___IS_VALID_HAS_VALID_PWF_STATUS__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Teilnetz Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER___IS_VALID_HAS_VALID_TEILNETZ_STATUS__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>UDPNM Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link conditions.impl.CANMessageImpl <em>CAN Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.CANMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getCANMessage()
	 * @generated
	 */
	int CAN_MESSAGE = 75;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Can Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE__CAN_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>CAN Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>CAN Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAN_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.LINMessageImpl <em>LIN Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.LINMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getLINMessage()
	 * @generated
	 */
	int LIN_MESSAGE = 76;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Lin Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE__LIN_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>LIN Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>LIN Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIN_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.FlexRayMessageImpl <em>Flex Ray Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.FlexRayMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getFlexRayMessage()
	 * @generated
	 */
	int FLEX_RAY_MESSAGE = 77;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Flex Ray Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE__FLEX_RAY_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Flex Ray Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Flex Ray Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLEX_RAY_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.DLTMessageImpl <em>DLT Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.DLTMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getDLTMessage()
	 * @generated
	 */
	int DLT_MESSAGE = 78;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The number of structural features of the '<em>DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DLT_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.UDPMessageImpl <em>UDP Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UDPMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUDPMessage()
	 * @generated
	 */
	int UDP_MESSAGE = 79;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Udp Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE__UDP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>UDP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>UDP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDP_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.TCPMessageImpl <em>TCP Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.TCPMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getTCPMessage()
	 * @generated
	 */
	int TCP_MESSAGE = 80;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Tcp Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE__TCP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>TCP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>TCP Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TCP_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.UDPNMMessageImpl <em>UDPNM Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UDPNMMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUDPNMMessage()
	 * @generated
	 */
	int UDPNM_MESSAGE = 81;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Udpnm Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE__UDPNM_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>UDPNM Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid UDP Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE___IS_VALID_HAS_VALID_UDP_FILTER__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>UDPNM Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UDPNM_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.VerboseDLTMessageImpl <em>Verbose DLT Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VerboseDLTMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVerboseDLTMessage()
	 * @generated
	 */
	int VERBOSE_DLT_MESSAGE = 82;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__ID = DLT_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__SOURCE_REFERENCE = DLT_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__SIGNALS = DLT_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__NAME = DLT_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__BUS_ID = DLT_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__BUS_ID_TMPL_PARAM = DLT_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__BUS_ID_RANGE = DLT_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__FILTERS = DLT_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Dlt Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE__DLT_FILTER = DLT_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Verbose DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE_FEATURE_COUNT = DLT_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE___GET_PRIMARY_FILTER = DLT_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE___GET_FILTERS__BOOLEAN = DLT_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = DLT_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = DLT_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Verbose DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_MESSAGE_OPERATION_COUNT = DLT_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl <em>Universal Payload With Legacy Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getUniversalPayloadWithLegacyExtractStrategy()
	 * @generated
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY = 83;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The feature id for the '<em><b>Extraction Rule</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__EXTRACTION_RULE = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE;

	/**
	 * The feature id for the '<em><b>Byte Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__BYTE_ORDER = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER;

	/**
	 * The feature id for the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Signal Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Start Bit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Universal Payload With Legacy Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY_FEATURE_COUNT = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Extract String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_EXTRACT_STRING__DIAGNOSTICCHAIN_MAP = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_EXTRACT_STRING__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Signal Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE__DIAGNOSTICCHAIN_MAP = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Startbit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STARTBIT__DIAGNOSTICCHAIN_MAP = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Data Length</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_DATA_LENGTH__DIAGNOSTICCHAIN_MAP = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Universal Payload With Legacy Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY_OPERATION_COUNT = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginFilterImpl <em>Plugin Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginFilter()
	 * @generated
	 */
	int PLUGIN_FILTER = 85;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Plugin Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER__PLUGIN_NAME = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Plugin Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER__PLUGIN_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Plugin Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Plugin Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginMessageImpl <em>Plugin Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginMessage()
	 * @generated
	 */
	int PLUGIN_MESSAGE = 86;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE__ID = ABSTRACT_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE__SIGNALS = ABSTRACT_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE__NAME = ABSTRACT_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Plugin Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE__PLUGIN_FILTER = ABSTRACT_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Plugin Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE_FEATURE_COUNT = ABSTRACT_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The number of operations of the '<em>Plugin Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_MESSAGE_OPERATION_COUNT = ABSTRACT_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginSignalImpl <em>Plugin Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginSignal()
	 * @generated
	 */
	int PLUGIN_SIGNAL = 87;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__ID = DOUBLE_SIGNAL__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__SOURCE_REFERENCE = DOUBLE_SIGNAL__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__NAME = DOUBLE_SIGNAL__NAME;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__DECODE_STRATEGY = DOUBLE_SIGNAL__DECODE_STRATEGY;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__EXTRACT_STRATEGY = DOUBLE_SIGNAL__EXTRACT_STRATEGY;

	/**
	 * The feature id for the '<em><b>Ignore Invalid Values</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__IGNORE_INVALID_VALUES = DOUBLE_SIGNAL__IGNORE_INVALID_VALUES;

	/**
	 * The feature id for the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM = DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM;

	/**
	 * The number of structural features of the '<em>Plugin Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL_FEATURE_COUNT = DOUBLE_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL___GET_READ_VARIABLES = DOUBLE_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL___GET_WRITE_VARIABLES = DOUBLE_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL___GET_EVALUATION_DATA_TYPE = DOUBLE_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL___GET_MESSAGE = DOUBLE_SIGNAL___GET_MESSAGE;

	/**
	 * The number of operations of the '<em>Plugin Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_SIGNAL_OPERATION_COUNT = DOUBLE_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginStateExtractStrategyImpl <em>Plugin State Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginStateExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginStateExtractStrategy()
	 * @generated
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY = 88;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The feature id for the '<em><b>States</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY__STATES = EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>States Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>States Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE = EXTRACT_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>States Active Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Plugin State Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid States Active</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STATES_ACTIVE__DIAGNOSTICCHAIN_MAP = EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STATE__DIAGNOSTICCHAIN_MAP = EXTRACT_STRATEGY_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Plugin State Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_STATE_EXTRACT_STRATEGY_OPERATION_COUNT = EXTRACT_STRATEGY_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginResultExtractStrategyImpl <em>Plugin Result Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginResultExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginResultExtractStrategy()
	 * @generated
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY = 89;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The feature id for the '<em><b>Result Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE = EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Plugin Result Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Result Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_RESULT_RANGE__DIAGNOSTICCHAIN_MAP = EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Plugin Result Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_RESULT_EXTRACT_STRATEGY_OPERATION_COUNT = EXTRACT_STRATEGY_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.EmptyDecodeStrategyImpl <em>Empty Decode Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.EmptyDecodeStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getEmptyDecodeStrategy()
	 * @generated
	 */
	int EMPTY_DECODE_STRATEGY = 90;

	/**
	 * The number of structural features of the '<em>Empty Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Empty Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_DECODE_STRATEGY_OPERATION_COUNT = DECODE_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.PluginCheckExpressionImpl <em>Plugin Check Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PluginCheckExpressionImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPluginCheckExpression()
	 * @generated
	 */
	int PLUGIN_CHECK_EXPRESSION = 91;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION__ID = EXPRESSION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Evaluation Behaviour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Signal To Check</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Evaluation Behaviour Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Plugin Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION___GET_READ_VARIABLES = EXPRESSION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION___GET_WRITE_VARIABLES = EXPRESSION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION___GET_STATE_DEPENDENCIES = EXPRESSION___GET_STATE_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION___GET_TRANSITION_DEPENDENCIES = EXPRESSION___GET_TRANSITION_DEPENDENCIES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = EXPRESSION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Plugin Check Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_CHECK_EXPRESSION_OPERATION_COUNT = EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.HeaderSignalImpl <em>Header Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.HeaderSignalImpl
	 * @see conditions.impl.ConditionsPackageImpl#getHeaderSignal()
	 * @generated
	 */
	int HEADER_SIGNAL = 93;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__ID = ABSTRACT_SIGNAL__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;

	/**
	 * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;

	/**
	 * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__ATTRIBUTE = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Header Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL___GET_READ_VARIABLES = ABSTRACT_SIGNAL___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL___GET_WRITE_VARIABLES = ABSTRACT_SIGNAL___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL___GET_EVALUATION_DATA_TYPE = ABSTRACT_SIGNAL___GET_EVALUATION_DATA_TYPE;

	/**
	 * The operation id for the '<em>Get Message</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL___GET_MESSAGE = ABSTRACT_SIGNAL___GET_MESSAGE;

	/**
	 * The operation id for the '<em>Is Valid has Valid Attribute</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL___IS_VALID_HAS_VALID_ATTRIBUTE__DIAGNOSTICCHAIN_MAP = ABSTRACT_SIGNAL_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Header Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HEADER_SIGNAL_OPERATION_COUNT = ABSTRACT_SIGNAL_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.IStateTransitionReferenceImpl <em>IState Transition Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.IStateTransitionReferenceImpl
	 * @see conditions.impl.ConditionsPackageImpl#getIStateTransitionReference()
	 * @generated
	 */
	int ISTATE_TRANSITION_REFERENCE = 95;

	/**
	 * The number of structural features of the '<em>IState Transition Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTATE_TRANSITION_REFERENCE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get State Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTATE_TRANSITION_REFERENCE___GET_STATE_DEPENDENCIES = 0;

	/**
	 * The operation id for the '<em>Get Transition Dependencies</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTATE_TRANSITION_REFERENCE___GET_TRANSITION_DEPENDENCIES = 1;

	/**
	 * The number of operations of the '<em>IState Transition Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ISTATE_TRANSITION_REFERENCE_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link conditions.impl.SourceReferenceImpl <em>Source Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.SourceReferenceImpl
	 * @see conditions.impl.ConditionsPackageImpl#getSourceReference()
	 * @generated
	 */
	int SOURCE_REFERENCE = 98;

	/**
	 * The feature id for the '<em><b>Serialized Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE__SERIALIZED_REFERENCE = 0;

	/**
	 * The number of structural features of the '<em>Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Source Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOURCE_REFERENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link conditions.impl.EthernetMessageImpl <em>Ethernet Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.EthernetMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getEthernetMessage()
	 * @generated
	 */
	int ETHERNET_MESSAGE = 99;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Ethernet Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE__ETHERNET_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ethernet Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Ethernet Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ETHERNET_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.IPv4MessageImpl <em>IPv4 Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.IPv4MessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getIPv4Message()
	 * @generated
	 */
	int IPV4_MESSAGE = 100;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>IPv4 Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE__IPV4_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IPv4 Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE___GET_PRIMARY_FILTER = ABSTRACT_BUS_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE___GET_FILTERS__BOOLEAN = ABSTRACT_BUS_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>IPv4 Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IPV4_MESSAGE_OPERATION_COUNT = ABSTRACT_BUS_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.NonVerboseDLTMessageImpl <em>Non Verbose DLT Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.NonVerboseDLTMessageImpl
	 * @see conditions.impl.ConditionsPackageImpl#getNonVerboseDLTMessage()
	 * @generated
	 */
	int NON_VERBOSE_DLT_MESSAGE = 101;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__ID = DLT_MESSAGE__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__SOURCE_REFERENCE = DLT_MESSAGE__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Signals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__SIGNALS = DLT_MESSAGE__SIGNALS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__NAME = DLT_MESSAGE__NAME;

	/**
	 * The feature id for the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__BUS_ID = DLT_MESSAGE__BUS_ID;

	/**
	 * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__BUS_ID_TMPL_PARAM = DLT_MESSAGE__BUS_ID_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__BUS_ID_RANGE = DLT_MESSAGE__BUS_ID_RANGE;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__FILTERS = DLT_MESSAGE__FILTERS;

	/**
	 * The feature id for the '<em><b>Non Verbose Dlt Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER = DLT_MESSAGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Non Verbose DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE_FEATURE_COUNT = DLT_MESSAGE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Primary Filter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE___GET_PRIMARY_FILTER = DLT_MESSAGE___GET_PRIMARY_FILTER;

	/**
	 * The operation id for the '<em>Get Filters</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE___GET_FILTERS__BOOLEAN = DLT_MESSAGE___GET_FILTERS__BOOLEAN;

	/**
	 * The operation id for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP = DLT_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP = DLT_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Non Verbose DLT Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_MESSAGE_OPERATION_COUNT = DLT_MESSAGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.StringDecodeStrategyImpl <em>String Decode Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.StringDecodeStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getStringDecodeStrategy()
	 * @generated
	 */
	int STRING_DECODE_STRATEGY = 102;

	/**
	 * The feature id for the '<em><b>String Termination</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECODE_STRATEGY__STRING_TERMINATION = DECODE_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>String Termination Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM = DECODE_STRATEGY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>String Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>String Decode Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_DECODE_STRATEGY_OPERATION_COUNT = DECODE_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.NonVerboseDLTFilterImpl <em>Non Verbose DLT Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.NonVerboseDLTFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getNonVerboseDLTFilter()
	 * @generated
	 */
	int NON_VERBOSE_DLT_FILTER = 103;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Message Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER__MESSAGE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Non Verbose DLT Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Message Id Or Message Id Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER___IS_VALID_HAS_VALID_MESSAGE_ID_OR_MESSAGE_ID_RANGE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Non Verbose DLT Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_VERBOSE_DLT_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.VerboseDLTExtractStrategyImpl <em>Verbose DLT Extract Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VerboseDLTExtractStrategyImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVerboseDLTExtractStrategy()
	 * @generated
	 */
	int VERBOSE_DLT_EXTRACT_STRATEGY = 104;

	/**
	 * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;

	/**
	 * The number of structural features of the '<em>Verbose DLT Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Verbose DLT Extract Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_EXTRACT_STRATEGY_OPERATION_COUNT = EXTRACT_STRATEGY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.RegexOperation <em>Regex Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.RegexOperation
	 * @see conditions.impl.ConditionsPackageImpl#getRegexOperation()
	 * @generated
	 */
	int REGEX_OPERATION = 105;

	/**
	 * The feature id for the '<em><b>Regex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGEX_OPERATION__REGEX = 0;

	/**
	 * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGEX_OPERATION__DYNAMIC_REGEX = 1;

	/**
	 * The number of structural features of the '<em>Regex Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGEX_OPERATION_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Regex</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGEX_OPERATION___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The number of operations of the '<em>Regex Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGEX_OPERATION_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link conditions.impl.PayloadFilterImpl <em>Payload Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.PayloadFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getPayloadFilter()
	 * @generated
	 */
	int PAYLOAD_FILTER = 106;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__INDEX = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mask</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__MASK = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER__VALUE = ABSTRACT_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Payload Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Index</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER___IS_VALID_HAS_VALID_INDEX__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Mask</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER___IS_VALID_HAS_VALID_MASK__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER___IS_VALID_HAS_VALID_VALUE__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Payload Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAYLOAD_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link conditions.impl.VerboseDLTPayloadFilterImpl <em>Verbose DLT Payload Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VerboseDLTPayloadFilterImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVerboseDLTPayloadFilter()
	 * @generated
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER = 107;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER__ID = ABSTRACT_FILTER__ID;

	/**
	 * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;

	/**
	 * The feature id for the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;

	/**
	 * The feature id for the '<em><b>Regex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER__REGEX = ABSTRACT_FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Verbose DLT Payload Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Regex</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER___IS_VALID_HAS_VALID_REGEX__DIAGNOSTICCHAIN_MAP = ABSTRACT_FILTER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Verbose DLT Payload Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERBOSE_DLT_PAYLOAD_FILTER_OPERATION_COUNT = ABSTRACT_FILTER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link conditions.impl.VariableStructureImpl <em>Variable Structure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.VariableStructureImpl
	 * @see conditions.impl.ConditionsPackageImpl#getVariableStructure()
	 * @generated
	 */
	int VARIABLE_STRUCTURE = 109;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE__ID = ABSTRACT_VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE__NAME = ABSTRACT_VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE__DISPLAY_NAME = ABSTRACT_VARIABLE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE__DESCRIPTION = ABSTRACT_VARIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE__VARIABLES = ABSTRACT_VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variable Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE_FEATURE_COUNT = ABSTRACT_VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE___GET_READ_VARIABLES = ABSTRACT_VARIABLE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE___GET_WRITE_VARIABLES = ABSTRACT_VARIABLE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Variable Structure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_STRUCTURE_OPERATION_COUNT = ABSTRACT_VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link conditions.impl.ComputedVariableImpl <em>Computed Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.impl.ComputedVariableImpl
	 * @see conditions.impl.ConditionsPackageImpl#getComputedVariable()
	 * @generated
	 */
	int COMPUTED_VARIABLE = 110;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__ID = VARIABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__NAME = VARIABLE__NAME;

	/**
	 * The feature id for the '<em><b>Display Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__UNIT = VARIABLE__UNIT;

	/**
	 * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;

	/**
	 * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__OPERANDS = VARIABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE__EXPRESSION = VARIABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Computed Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___GET_READ_VARIABLES = VARIABLE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___GET_WRITE_VARIABLES = VARIABLE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_DISPLAY_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_INTERPRETED_VALUE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_UNIT__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Variable Format Tmpl Param</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP = VARIABLE___IS_VALID_HAS_VALID_VARIABLE_FORMAT_TMPL_PARAM__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Observers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___GET_OBSERVERS = VARIABLE___GET_OBSERVERS;

	/**
	 * The operation id for the '<em>Is Valid has Valid Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_EXPRESSION__DIAGNOSTICCHAIN_MAP = VARIABLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP = VARIABLE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Computed Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_VARIABLE_OPERATION_COUNT = VARIABLE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link conditions.CheckType <em>Check Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CheckType
	 * @see conditions.impl.ConditionsPackageImpl#getCheckType()
	 * @generated
	 */
	int CHECK_TYPE = 111;

	/**
	 * The meta object id for the '{@link conditions.Comparator <em>Comparator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Comparator
	 * @see conditions.impl.ConditionsPackageImpl#getComparator()
	 * @generated
	 */
	int COMPARATOR = 112;

	/**
	 * The meta object id for the '{@link conditions.LogicalOperatorType <em>Logical Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LogicalOperatorType
	 * @see conditions.impl.ConditionsPackageImpl#getLogicalOperatorType()
	 * @generated
	 */
	int LOGICAL_OPERATOR_TYPE = 113;

	/**
	 * The meta object id for the '{@link conditions.SignalDataType <em>Signal Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SignalDataType
	 * @see conditions.impl.ConditionsPackageImpl#getSignalDataType()
	 * @generated
	 */
	int SIGNAL_DATA_TYPE = 114;

	/**
	 * The meta object id for the '{@link conditions.FlexChannelType <em>Flex Channel Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexChannelType
	 * @see conditions.impl.ConditionsPackageImpl#getFlexChannelType()
	 * @generated
	 */
	int FLEX_CHANNEL_TYPE = 115;

	/**
	 * The meta object id for the '{@link conditions.ByteOrderType <em>Byte Order Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ByteOrderType
	 * @see conditions.impl.ConditionsPackageImpl#getByteOrderType()
	 * @generated
	 */
	int BYTE_ORDER_TYPE = 116;

	/**
	 * The meta object id for the '{@link conditions.FormatDataType <em>Format Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FormatDataType
	 * @see conditions.impl.ConditionsPackageImpl#getFormatDataType()
	 * @generated
	 */
	int FORMAT_DATA_TYPE = 117;

	/**
	 * The meta object id for the '{@link conditions.FlexRayHeaderFlagSelection <em>Flex Ray Header Flag Selection</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see conditions.impl.ConditionsPackageImpl#getFlexRayHeaderFlagSelection()
	 * @generated
	 */
	int FLEX_RAY_HEADER_FLAG_SELECTION = 118;

	/**
	 * The meta object id for the '{@link conditions.BooleanOrTemplatePlaceholderEnum <em>Boolean Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getBooleanOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM = 119;

	/**
	 * The meta object id for the '{@link conditions.ProtocolTypeOrTemplatePlaceholderEnum <em>Protocol Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ProtocolTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getProtocolTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 120;

	/**
	 * The meta object id for the '{@link conditions.EthernetProtocolTypeSelection <em>Ethernet Protocol Type Selection</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetProtocolTypeSelection
	 * @see conditions.impl.ConditionsPackageImpl#getEthernetProtocolTypeSelection()
	 * @generated
	 */
	int ETHERNET_PROTOCOL_TYPE_SELECTION = 121;

	/**
	 * The meta object id for the '{@link conditions.RxTxFlagTypeOrTemplatePlaceholderEnum <em>Rx Tx Flag Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getRxTxFlagTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 122;

	/**
	 * The meta object id for the '{@link conditions.ObserverValueType <em>Observer Value Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ObserverValueType
	 * @see conditions.impl.ConditionsPackageImpl#getObserverValueType()
	 * @generated
	 */
	int OBSERVER_VALUE_TYPE = 123;

	/**
	 * The meta object id for the '{@link conditions.PluginStateValueTypeOrTemplatePlaceholderEnum <em>Plugin State Value Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.PluginStateValueTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getPluginStateValueTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 124;

	/**
	 * The meta object id for the '{@link conditions.AnalysisTypeOrTemplatePlaceholderEnum <em>Analysis Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AnalysisTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getAnalysisTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 125;

	/**
	 * The meta object id for the '{@link conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum <em>Some IP Message Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPMessageTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 126;

	/**
	 * The meta object id for the '{@link conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum <em>Some IP Method Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPMethodTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 127;

	/**
	 * The meta object id for the '{@link conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum <em>Some IP Return Code Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPReturnCodeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM = 128;

	/**
	 * The meta object id for the '{@link conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum <em>Some IPSD Flags Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPSDFlagsOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 129;

	/**
	 * The meta object id for the '{@link conditions.SomeIPSDTypeOrTemplatePlaceholderEnum <em>Some IPSD Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPSDTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 130;

	/**
	 * The meta object id for the '{@link conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum <em>Stream Analysis Flags Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getStreamAnalysisFlagsOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 131;

	/**
	 * The meta object id for the '{@link conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum <em>Some IPSD Entry Flags Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 132;

	/**
	 * The meta object id for the '{@link conditions.CanExtIdentifierOrTemplatePlaceholderEnum <em>Can Ext Identifier Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanExtIdentifierOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getCanExtIdentifierOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM = 133;

	/**
	 * The meta object id for the '{@link conditions.DataType <em>Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DataType
	 * @see conditions.impl.ConditionsPackageImpl#getDataType()
	 * @generated
	 */
	int DATA_TYPE = 134;

	/**
	 * The meta object id for the '{@link conditions.DLT_MessageType <em>DLT Message Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLT_MessageType
	 * @see conditions.impl.ConditionsPackageImpl#getDLT_MessageType()
	 * @generated
	 */
	int DLT_MESSAGE_TYPE = 135;

	/**
	 * The meta object id for the '{@link conditions.DLT_MessageTraceInfo <em>DLT Message Trace Info</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLT_MessageTraceInfo
	 * @see conditions.impl.ConditionsPackageImpl#getDLT_MessageTraceInfo()
	 * @generated
	 */
	int DLT_MESSAGE_TRACE_INFO = 136;

	/**
	 * The meta object id for the '{@link conditions.DLT_MessageLogInfo <em>DLT Message Log Info</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLT_MessageLogInfo
	 * @see conditions.impl.ConditionsPackageImpl#getDLT_MessageLogInfo()
	 * @generated
	 */
	int DLT_MESSAGE_LOG_INFO = 137;

	/**
	 * The meta object id for the '{@link conditions.DLT_MessageControlInfo <em>DLT Message Control Info</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLT_MessageControlInfo
	 * @see conditions.impl.ConditionsPackageImpl#getDLT_MessageControlInfo()
	 * @generated
	 */
	int DLT_MESSAGE_CONTROL_INFO = 138;

	/**
	 * The meta object id for the '{@link conditions.DLT_MessageBusInfo <em>DLT Message Bus Info</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLT_MessageBusInfo
	 * @see conditions.impl.ConditionsPackageImpl#getDLT_MessageBusInfo()
	 * @generated
	 */
	int DLT_MESSAGE_BUS_INFO = 139;

	/**
	 * The meta object id for the '{@link conditions.ForEachExpressionModifierTemplate <em>For Each Expression Modifier Template</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ForEachExpressionModifierTemplate
	 * @see conditions.impl.ConditionsPackageImpl#getForEachExpressionModifierTemplate()
	 * @generated
	 */
	int FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE = 140;

	/**
	 * The meta object id for the '{@link conditions.AUTOSARDataType <em>AUTOSAR Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AUTOSARDataType
	 * @see conditions.impl.ConditionsPackageImpl#getAUTOSARDataType()
	 * @generated
	 */
	int AUTOSAR_DATA_TYPE = 141;

	/**
	 * The meta object id for the '{@link conditions.CANFrameTypeOrTemplatePlaceholderEnum <em>CAN Frame Type Or Template Placeholder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CANFrameTypeOrTemplatePlaceholderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getCANFrameTypeOrTemplatePlaceholderEnum()
	 * @generated
	 */
	int CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 142;

	/**
	 * The meta object id for the '{@link conditions.EvaluationBehaviourOrTemplateDefinedEnum <em>Evaluation Behaviour Or Template Defined Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EvaluationBehaviourOrTemplateDefinedEnum
	 * @see conditions.impl.ConditionsPackageImpl#getEvaluationBehaviourOrTemplateDefinedEnum()
	 * @generated
	 */
	int EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM = 143;

	/**
	 * The meta object id for the '{@link conditions.SignalVariableLagEnum <em>Signal Variable Lag Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SignalVariableLagEnum
	 * @see conditions.impl.ConditionsPackageImpl#getSignalVariableLagEnum()
	 * @generated
	 */
	int SIGNAL_VARIABLE_LAG_ENUM = 144;

	/**
	 * The meta object id for the '{@link conditions.TTLOrTemplatePlaceHolderEnum <em>TTL Or Template Place Holder Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TTLOrTemplatePlaceHolderEnum
	 * @see conditions.impl.ConditionsPackageImpl#getTTLOrTemplatePlaceHolderEnum()
	 * @generated
	 */
	int TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM = 145;

	/**
	 * The meta object id for the '<em>Check Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CheckType
	 * @see conditions.impl.ConditionsPackageImpl#getCheckTypeObject()
	 * @generated
	 */
	int CHECK_TYPE_OBJECT = 146;

	/**
	 * The meta object id for the '<em>Comparator Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Comparator
	 * @see conditions.impl.ConditionsPackageImpl#getComparatorObject()
	 * @generated
	 */
	int COMPARATOR_OBJECT = 147;

	/**
	 * The meta object id for the '<em>Logical Operator Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LogicalOperatorType
	 * @see conditions.impl.ConditionsPackageImpl#getLogicalOperatorTypeObject()
	 * @generated
	 */
	int LOGICAL_OPERATOR_TYPE_OBJECT = 148;

	/**
	 * The meta object id for the '<em>Hex Or Int Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getHexOrIntOrTemplatePlaceholder()
	 * @generated
	 */
	int HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER = 149;

	/**
	 * The meta object id for the '<em>Int Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getIntOrTemplatePlaceholder()
	 * @generated
	 */
	int INT_OR_TEMPLATE_PLACEHOLDER = 150;

	/**
	 * The meta object id for the '<em>Long Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getLongOrTemplatePlaceholder()
	 * @generated
	 */
	int LONG_OR_TEMPLATE_PLACEHOLDER = 151;

	/**
	 * The meta object id for the '<em>Double Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getDoubleOrTemplatePlaceholder()
	 * @generated
	 */
	int DOUBLE_OR_TEMPLATE_PLACEHOLDER = 152;

	/**
	 * The meta object id for the '<em>Double Or Hex Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getDoubleOrHexOrTemplatePlaceholder()
	 * @generated
	 */
	int DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER = 153;

	/**
	 * The meta object id for the '<em>IP Pattern Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getIPPatternOrTemplatePlaceholder()
	 * @generated
	 */
	int IP_PATTERN_OR_TEMPLATE_PLACEHOLDER = 154;

	/**
	 * The meta object id for the '<em>MAC Pattern Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getMACPatternOrTemplatePlaceholder()
	 * @generated
	 */
	int MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER = 155;

	/**
	 * The meta object id for the '<em>Port Pattern Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getPortPatternOrTemplatePlaceholder()
	 * @generated
	 */
	int PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER = 156;

	/**
	 * The meta object id for the '<em>Vlan Id Pattern Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getVlanIdPatternOrTemplatePlaceholder()
	 * @generated
	 */
	int VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER = 157;

	/**
	 * The meta object id for the '<em>IP Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getIPOrTemplatePlaceholder()
	 * @generated
	 */
	int IP_OR_TEMPLATE_PLACEHOLDER = 158;

	/**
	 * The meta object id for the '<em>Port Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getPortOrTemplatePlaceholder()
	 * @generated
	 */
	int PORT_OR_TEMPLATE_PLACEHOLDER = 159;

	/**
	 * The meta object id for the '<em>Vlan Id Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getVlanIdOrTemplatePlaceholder()
	 * @generated
	 */
	int VLAN_ID_OR_TEMPLATE_PLACEHOLDER = 160;

	/**
	 * The meta object id for the '<em>MAC Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getMACOrTemplatePlaceholder()
	 * @generated
	 */
	int MAC_OR_TEMPLATE_PLACEHOLDER = 161;

	/**
	 * The meta object id for the '<em>Bitmask Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getBitmaskOrTemplatePlaceholder()
	 * @generated
	 */
	int BITMASK_OR_TEMPLATE_PLACEHOLDER = 162;

	/**
	 * The meta object id for the '<em>Hex Or Int Or Null Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getHexOrIntOrNullOrTemplatePlaceholder()
	 * @generated
	 */
	int HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER = 163;

	/**
	 * The meta object id for the '<em>Non Zero Double Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getNonZeroDoubleOrTemplatePlaceholder()
	 * @generated
	 */
	int NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER = 164;

	/**
	 * The meta object id for the '<em>Val Var Initial Value Double Or Template Placeholder Or String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getValVarInitialValueDoubleOrTemplatePlaceholderOrString()
	 * @generated
	 */
	int VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING = 165;

	/**
	 * The meta object id for the '<em>Byte Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see conditions.impl.ConditionsPackageImpl#getByteOrTemplatePlaceholder()
	 * @generated
	 */
	int BYTE_OR_TEMPLATE_PLACEHOLDER = 166;


	/**
	 * Returns the meta object for class '{@link conditions.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see conditions.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link conditions.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see conditions.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link conditions.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see conditions.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link conditions.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see conditions.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.DocumentRoot#getConditionSet <em>Condition Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition Set</em>'.
	 * @see conditions.DocumentRoot#getConditionSet()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ConditionSet();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.DocumentRoot#getConditionsDocument <em>Conditions Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conditions Document</em>'.
	 * @see conditions.DocumentRoot#getConditionsDocument()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_ConditionsDocument();

	/**
	 * Returns the meta object for class '{@link conditions.ConditionSet <em>Condition Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Set</em>'.
	 * @see conditions.ConditionSet
	 * @generated
	 */
	EClass getConditionSet();

	/**
	 * Returns the meta object for the attribute list '{@link conditions.ConditionSet#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see conditions.ConditionSet#getGroup()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Group();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getBaureihe <em>Baureihe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Baureihe</em>'.
	 * @see conditions.ConditionSet#getBaureihe()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Baureihe();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.ConditionSet#getDescription()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Description();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getIstufe <em>Istufe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Istufe</em>'.
	 * @see conditions.ConditionSet#getIstufe()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Istufe();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Schemaversion</em>'.
	 * @see conditions.ConditionSet#getSchemaversion()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Schemaversion();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see conditions.ConditionSet#getUuid()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Uuid();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see conditions.ConditionSet#getVersion()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Version();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.ConditionSet#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Conditions</em>'.
	 * @see conditions.ConditionSet#getConditions()
	 * @see #getConditionSet()
	 * @generated
	 */
	EReference getConditionSet_Conditions();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConditionSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.ConditionSet#getName()
	 * @see #getConditionSet()
	 * @generated
	 */
	EAttribute getConditionSet_Name();

	/**
	 * Returns the meta object for class '{@link conditions.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see conditions.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Condition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.Condition#getName()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Name();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Condition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.Condition#getDescription()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Description();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.Condition#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see conditions.Condition#getExpression()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_Expression();

	/**
	 * Returns the meta object for the '{@link conditions.Condition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Name</em>' operation.
	 * @see conditions.Condition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCondition__IsValid_hasValidName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.Condition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see conditions.Condition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCondition__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.SignalComparisonExpression <em>Signal Comparison Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Comparison Expression</em>'.
	 * @see conditions.SignalComparisonExpression
	 * @generated
	 */
	EClass getSignalComparisonExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.SignalComparisonExpression#getComparatorSignal <em>Comparator Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Comparator Signal</em>'.
	 * @see conditions.SignalComparisonExpression#getComparatorSignal()
	 * @see #getSignalComparisonExpression()
	 * @generated
	 */
	EReference getSignalComparisonExpression_ComparatorSignal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SignalComparisonExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see conditions.SignalComparisonExpression#getOperator()
	 * @see #getSignalComparisonExpression()
	 * @generated
	 */
	EAttribute getSignalComparisonExpression_Operator();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SignalComparisonExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator Tmpl Param</em>'.
	 * @see conditions.SignalComparisonExpression#getOperatorTmplParam()
	 * @see #getSignalComparisonExpression()
	 * @generated
	 */
	EAttribute getSignalComparisonExpression_OperatorTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.SignalComparisonExpression#isValid_hasValidComparator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Comparator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Comparator</em>' operation.
	 * @see conditions.SignalComparisonExpression#isValid_hasValidComparator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSignalComparisonExpression__IsValid_hasValidComparator__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SignalComparisonExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Operands</em>' operation.
	 * @see conditions.SignalComparisonExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSignalComparisonExpression__IsValid_hasValidOperands__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.CanMessageCheckExpression <em>Can Message Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Can Message Check Expression</em>'.
	 * @see conditions.CanMessageCheckExpression
	 * @generated
	 */
	EClass getCanMessageCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Busid</em>'.
	 * @see conditions.CanMessageCheckExpression#getBusid()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_Busid();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see conditions.CanMessageCheckExpression#getType()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_Type();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getMessageIDs <em>Message IDs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message IDs</em>'.
	 * @see conditions.CanMessageCheckExpression#getMessageIDs()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_MessageIDs();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
	 * @see conditions.CanMessageCheckExpression#getTypeTmplParam()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_TypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Busid Tmpl Param</em>'.
	 * @see conditions.CanMessageCheckExpression#getBusidTmplParam()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_BusidTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getRxtxFlag <em>Rxtx Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rxtx Flag</em>'.
	 * @see conditions.CanMessageCheckExpression#getRxtxFlag()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_RxtxFlag();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam <em>Rxtx Flag Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rxtx Flag Type Tmpl Param</em>'.
	 * @see conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_RxtxFlagTypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getExtIdentifier <em>Ext Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ext Identifier</em>'.
	 * @see conditions.CanMessageCheckExpression#getExtIdentifier()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_ExtIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CanMessageCheckExpression#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ext Identifier Tmpl Param</em>'.
	 * @see conditions.CanMessageCheckExpression#getExtIdentifierTmplParam()
	 * @see #getCanMessageCheckExpression()
	 * @generated
	 */
	EAttribute getCanMessageCheckExpression_ExtIdentifierTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.CanMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCanMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CanMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Id Range</em>' operation.
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCanMessageCheckExpression__IsValid_hasValidMessageIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CanMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCanMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CanMessageCheckExpression#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Rx Tx Flag</em>' operation.
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCanMessageCheckExpression__IsValid_hasValidRxTxFlag__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CanMessageCheckExpression#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Ext Identifier</em>' operation.
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCanMessageCheckExpression__IsValid_hasValidExtIdentifier__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.LinMessageCheckExpression <em>Lin Message Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lin Message Check Expression</em>'.
	 * @see conditions.LinMessageCheckExpression
	 * @generated
	 */
	EClass getLinMessageCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Busid</em>'.
	 * @see conditions.LinMessageCheckExpression#getBusid()
	 * @see #getLinMessageCheckExpression()
	 * @generated
	 */
	EAttribute getLinMessageCheckExpression_Busid();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LinMessageCheckExpression#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see conditions.LinMessageCheckExpression#getType()
	 * @see #getLinMessageCheckExpression()
	 * @generated
	 */
	EAttribute getLinMessageCheckExpression_Type();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LinMessageCheckExpression#getMessageIDs <em>Message IDs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message IDs</em>'.
	 * @see conditions.LinMessageCheckExpression#getMessageIDs()
	 * @see #getLinMessageCheckExpression()
	 * @generated
	 */
	EAttribute getLinMessageCheckExpression_MessageIDs();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LinMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
	 * @see conditions.LinMessageCheckExpression#getTypeTmplParam()
	 * @see #getLinMessageCheckExpression()
	 * @generated
	 */
	EAttribute getLinMessageCheckExpression_TypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LinMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Busid Tmpl Param</em>'.
	 * @see conditions.LinMessageCheckExpression#getBusidTmplParam()
	 * @see #getLinMessageCheckExpression()
	 * @generated
	 */
	EAttribute getLinMessageCheckExpression_BusidTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.LinMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLinMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.LinMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Id Range</em>' operation.
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLinMessageCheckExpression__IsValid_hasValidMessageIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.LinMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLinMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.StateCheckExpression <em>State Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Check Expression</em>'.
	 * @see conditions.StateCheckExpression
	 * @generated
	 */
	EClass getStateCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StateCheckExpression#getCheckStateActive <em>Check State Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check State Active</em>'.
	 * @see conditions.StateCheckExpression#getCheckStateActive()
	 * @see #getStateCheckExpression()
	 * @generated
	 */
	EAttribute getStateCheckExpression_CheckStateActive();

	/**
	 * Returns the meta object for the reference '{@link conditions.StateCheckExpression#getCheckState <em>Check State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Check State</em>'.
	 * @see conditions.StateCheckExpression#getCheckState()
	 * @see #getStateCheckExpression()
	 * @generated
	 */
	EReference getStateCheckExpression_CheckState();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StateCheckExpression#getCheckStateActiveTmplParam <em>Check State Active Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Check State Active Tmpl Param</em>'.
	 * @see conditions.StateCheckExpression#getCheckStateActiveTmplParam()
	 * @see #getStateCheckExpression()
	 * @generated
	 */
	EAttribute getStateCheckExpression_CheckStateActiveTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.StateCheckExpression#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid States Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid States Active</em>' operation.
	 * @see conditions.StateCheckExpression#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getStateCheckExpression__IsValid_hasValidStatesActive__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.StateCheckExpression#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid State</em>' operation.
	 * @see conditions.StateCheckExpression#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getStateCheckExpression__IsValid_hasValidState__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.TimingExpression <em>Timing Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timing Expression</em>'.
	 * @see conditions.TimingExpression
	 * @generated
	 */
	EClass getTimingExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TimingExpression#getMaxtime <em>Maxtime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maxtime</em>'.
	 * @see conditions.TimingExpression#getMaxtime()
	 * @see #getTimingExpression()
	 * @generated
	 */
	EAttribute getTimingExpression_Maxtime();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TimingExpression#getMintime <em>Mintime</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mintime</em>'.
	 * @see conditions.TimingExpression#getMintime()
	 * @see #getTimingExpression()
	 * @generated
	 */
	EAttribute getTimingExpression_Mintime();

	/**
	 * Returns the meta object for the '{@link conditions.TimingExpression#isValid_hasValidMinMaxTimes(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Min Max Times</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Min Max Times</em>' operation.
	 * @see conditions.TimingExpression#isValid_hasValidMinMaxTimes(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTimingExpression__IsValid_hasValidMinMaxTimes__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.TrueExpression <em>True Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>True Expression</em>'.
	 * @see conditions.TrueExpression
	 * @generated
	 */
	EClass getTrueExpression();

	/**
	 * Returns the meta object for class '{@link conditions.LogicalExpression <em>Logical Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Expression</em>'.
	 * @see conditions.LogicalExpression
	 * @generated
	 */
	EClass getLogicalExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.LogicalExpression#getExpressions <em>Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Expressions</em>'.
	 * @see conditions.LogicalExpression#getExpressions()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EReference getLogicalExpression_Expressions();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LogicalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see conditions.LogicalExpression#getOperator()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EAttribute getLogicalExpression_Operator();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LogicalExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator Tmpl Param</em>'.
	 * @see conditions.LogicalExpression#getOperatorTmplParam()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EAttribute getLogicalExpression_OperatorTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.LogicalExpression#isValid_hasValidOperator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Operator</em>' operation.
	 * @see conditions.LogicalExpression#isValid_hasValidOperator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLogicalExpression__IsValid_hasValidOperator__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.LogicalExpression#isValid_hasValidSubExpressions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sub Expressions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Sub Expressions</em>' operation.
	 * @see conditions.LogicalExpression#isValid_hasValidSubExpressions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLogicalExpression__IsValid_hasValidSubExpressions__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.ReferenceConditionExpression <em>Reference Condition Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Condition Expression</em>'.
	 * @see conditions.ReferenceConditionExpression
	 * @generated
	 */
	EClass getReferenceConditionExpression();

	/**
	 * Returns the meta object for the reference '{@link conditions.ReferenceConditionExpression#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Condition</em>'.
	 * @see conditions.ReferenceConditionExpression#getCondition()
	 * @see #getReferenceConditionExpression()
	 * @generated
	 */
	EReference getReferenceConditionExpression_Condition();

	/**
	 * Returns the meta object for the '{@link conditions.ReferenceConditionExpression#isValid_hasValidReferencedCondition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Referenced Condition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Referenced Condition</em>' operation.
	 * @see conditions.ReferenceConditionExpression#isValid_hasValidReferencedCondition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getReferenceConditionExpression__IsValid_hasValidReferencedCondition__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.TransitionCheckExpression <em>Transition Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition Check Expression</em>'.
	 * @see conditions.TransitionCheckExpression
	 * @generated
	 */
	EClass getTransitionCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TransitionCheckExpression#getStayActive <em>Stay Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stay Active</em>'.
	 * @see conditions.TransitionCheckExpression#getStayActive()
	 * @see #getTransitionCheckExpression()
	 * @generated
	 */
	EAttribute getTransitionCheckExpression_StayActive();

	/**
	 * Returns the meta object for the reference '{@link conditions.TransitionCheckExpression#getCheckTransition <em>Check Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Check Transition</em>'.
	 * @see conditions.TransitionCheckExpression#getCheckTransition()
	 * @see #getTransitionCheckExpression()
	 * @generated
	 */
	EReference getTransitionCheckExpression_CheckTransition();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TransitionCheckExpression#getStayActiveTmplParam <em>Stay Active Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stay Active Tmpl Param</em>'.
	 * @see conditions.TransitionCheckExpression#getStayActiveTmplParam()
	 * @see #getTransitionCheckExpression()
	 * @generated
	 */
	EAttribute getTransitionCheckExpression_StayActiveTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.TransitionCheckExpression#isValid_hasValidStayActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stay Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Stay Active</em>' operation.
	 * @see conditions.TransitionCheckExpression#isValid_hasValidStayActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransitionCheckExpression__IsValid_hasValidStayActive__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TransitionCheckExpression#isValid_hasValidTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Transition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Transition</em>' operation.
	 * @see conditions.TransitionCheckExpression#isValid_hasValidTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransitionCheckExpression__IsValid_hasValidTransition__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.FlexRayMessageCheckExpression <em>Flex Ray Message Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flex Ray Message Check Expression</em>'.
	 * @see conditions.FlexRayMessageCheckExpression
	 * @generated
	 */
	EClass getFlexRayMessageCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getBusId <em>Bus Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bus Id</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getBusId()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_BusId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getPayloadPreamble <em>Payload Preamble</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Payload Preamble</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getPayloadPreamble()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_PayloadPreamble();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getZeroFrame <em>Zero Frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zero Frame</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getZeroFrame()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_ZeroFrame();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getSyncFrame <em>Sync Frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sync Frame</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getSyncFrame()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_SyncFrame();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getStartupFrame <em>Startup Frame</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Startup Frame</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getStartupFrame()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_StartupFrame();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmt <em>Network Mgmt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Network Mgmt</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getNetworkMgmt()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_NetworkMgmt();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getFlexrayMessageId <em>Flexray Message Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flexray Message Id</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getFlexrayMessageId()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_FlexrayMessageId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam <em>Payload Preamble Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Payload Preamble Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_PayloadPreambleTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam <em>Zero Frame Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Zero Frame Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_ZeroFrameTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam <em>Sync Frame Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sync Frame Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_SyncFrameTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam <em>Startup Frame Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Startup Frame Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_StartupFrameTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam <em>Network Mgmt Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Network Mgmt Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_NetworkMgmtTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bus Id Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getBusIdTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_BusIdTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getType()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_Type();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
	 * @see conditions.FlexRayMessageCheckExpression#getTypeTmplParam()
	 * @see #getFlexRayMessageCheckExpression()
	 * @generated
	 */
	EAttribute getFlexRayMessageCheckExpression_TypeTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidBusId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Check Type</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidCheckType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidStartupFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startup Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Startup Frame</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidStartupFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidStartupFrame__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidSyncFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sync Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Sync Frame</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidSyncFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidSyncFrame__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidZeroFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Zero Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Zero Frame</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidZeroFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidZeroFrame__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidPayloadPreamble(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Payload Preamble</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Payload Preamble</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidPayloadPreamble(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidPayloadPreamble__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidNetworkMgmt(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Network Mgmt</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Network Mgmt</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidNetworkMgmt(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidNetworkMgmt__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidMessageId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Id</em>' operation.
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidMessageId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayMessageCheckExpression__IsValid_hasValidMessageId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.StopExpression <em>Stop Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stop Expression</em>'.
	 * @see conditions.StopExpression
	 * @generated
	 */
	EClass getStopExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Analyse Art</em>'.
	 * @see conditions.StopExpression#getAnalyseArt()
	 * @see #getStopExpression()
	 * @generated
	 */
	EAttribute getStopExpression_AnalyseArt();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Analyse Art Tmpl Param</em>'.
	 * @see conditions.StopExpression#getAnalyseArtTmplParam()
	 * @see #getStopExpression()
	 * @generated
	 */
	EAttribute getStopExpression_AnalyseArtTmplParam();

	/**
	 * Returns the meta object for class '{@link conditions.ComparatorSignal <em>Comparator Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comparator Signal</em>'.
	 * @see conditions.ComparatorSignal
	 * @generated
	 */
	EClass getComparatorSignal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ComparatorSignal#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.ComparatorSignal#getDescription()
	 * @see #getComparatorSignal()
	 * @generated
	 */
	EAttribute getComparatorSignal_Description();

	/**
	 * Returns the meta object for class '{@link conditions.ConstantComparatorValue <em>Constant Comparator Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Comparator Value</em>'.
	 * @see conditions.ConstantComparatorValue
	 * @generated
	 */
	EClass getConstantComparatorValue();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConstantComparatorValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see conditions.ConstantComparatorValue#getValue()
	 * @see #getConstantComparatorValue()
	 * @generated
	 */
	EAttribute getConstantComparatorValue_Value();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConstantComparatorValue#getInterpretedValue <em>Interpreted Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpreted Value</em>'.
	 * @see conditions.ConstantComparatorValue#getInterpretedValue()
	 * @see #getConstantComparatorValue()
	 * @generated
	 */
	EAttribute getConstantComparatorValue_InterpretedValue();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ConstantComparatorValue#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpreted Value Tmpl Param</em>'.
	 * @see conditions.ConstantComparatorValue#getInterpretedValueTmplParam()
	 * @see #getConstantComparatorValue()
	 * @generated
	 */
	EAttribute getConstantComparatorValue_InterpretedValueTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.ConstantComparatorValue#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * @see conditions.ConstantComparatorValue#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getConstantComparatorValue__IsValid_hasValidInterpretedValue__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.ConstantComparatorValue#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value</em>' operation.
	 * @see conditions.ConstantComparatorValue#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getConstantComparatorValue__IsValid_hasValidValue__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.CalculationExpression <em>Calculation Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Calculation Expression</em>'.
	 * @see conditions.CalculationExpression
	 * @generated
	 */
	EClass getCalculationExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CalculationExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see conditions.CalculationExpression#getExpression()
	 * @see #getCalculationExpression()
	 * @generated
	 */
	EAttribute getCalculationExpression_Expression();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.CalculationExpression#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see conditions.CalculationExpression#getOperands()
	 * @see #getCalculationExpression()
	 * @generated
	 */
	EReference getCalculationExpression_Operands();

	/**
	 * Returns the meta object for the '{@link conditions.CalculationExpression#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Expression</em>' operation.
	 * @see conditions.CalculationExpression#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCalculationExpression__IsValid_hasValidExpression__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CalculationExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Operands</em>' operation.
	 * @see conditions.CalculationExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCalculationExpression__IsValid_hasValidOperands__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see conditions.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Expression#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.Expression#getDescription()
	 * @see #getExpression()
	 * @generated
	 */
	EAttribute getExpression_Description();

	/**
	 * Returns the meta object for the '{@link conditions.Expression#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see conditions.Expression#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getExpression__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.VariableReference <em>Variable Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Reference</em>'.
	 * @see conditions.VariableReference
	 * @generated
	 */
	EClass getVariableReference();

	/**
	 * Returns the meta object for the reference '{@link conditions.VariableReference#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see conditions.VariableReference#getVariable()
	 * @see #getVariableReference()
	 * @generated
	 */
	EReference getVariableReference_Variable();

	/**
	 * Returns the meta object for class '{@link conditions.ConditionsDocument <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document</em>'.
	 * @see conditions.ConditionsDocument
	 * @generated
	 */
	EClass getConditionsDocument();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.ConditionsDocument#getSignalReferencesSet <em>Signal References Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Signal References Set</em>'.
	 * @see conditions.ConditionsDocument#getSignalReferencesSet()
	 * @see #getConditionsDocument()
	 * @generated
	 */
	EReference getConditionsDocument_SignalReferencesSet();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.ConditionsDocument#getVariableSet <em>Variable Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Variable Set</em>'.
	 * @see conditions.ConditionsDocument#getVariableSet()
	 * @see #getConditionsDocument()
	 * @generated
	 */
	EReference getConditionsDocument_VariableSet();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.ConditionsDocument#getConditionSet <em>Condition Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition Set</em>'.
	 * @see conditions.ConditionsDocument#getConditionSet()
	 * @see #getConditionsDocument()
	 * @generated
	 */
	EReference getConditionsDocument_ConditionSet();

	/**
	 * Returns the meta object for class '{@link conditions.VariableSet <em>Variable Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Set</em>'.
	 * @see conditions.VariableSet
	 * @generated
	 */
	EClass getVariableSet();

	/**
	 * Returns the meta object for the attribute '{@link conditions.VariableSet#getUuid <em>Uuid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid</em>'.
	 * @see conditions.VariableSet#getUuid()
	 * @see #getVariableSet()
	 * @generated
	 */
	EAttribute getVariableSet_Uuid();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.VariableSet#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see conditions.VariableSet#getVariables()
	 * @see #getVariableSet()
	 * @generated
	 */
	EReference getVariableSet_Variables();

	/**
	 * Returns the meta object for the '{@link conditions.VariableSet#isValid_hasValidValueRanges(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value Ranges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * @see conditions.VariableSet#isValid_hasValidValueRanges(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariableSet__IsValid_hasValidValueRanges__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see conditions.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Variable#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see conditions.Variable#getDataType()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_DataType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Variable#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see conditions.Variable#getUnit()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_Unit();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.Variable#getVariableFormat <em>Variable Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Variable Format</em>'.
	 * @see conditions.Variable#getVariableFormat()
	 * @see #getVariable()
	 * @generated
	 */
	EReference getVariable_VariableFormat();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Variable#getVariableFormatTmplParam <em>Variable Format Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable Format Tmpl Param</em>'.
	 * @see conditions.Variable#getVariableFormatTmplParam()
	 * @see #getVariable()
	 * @generated
	 */
	EAttribute getVariable_VariableFormatTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.Variable#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Interpreted Value</em>' operation.
	 * @see conditions.Variable#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariable__IsValid_hasValidInterpretedValue__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.Variable#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Type</em>' operation.
	 * @see conditions.Variable#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariable__IsValid_hasValidType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.Variable#isValid_hasValidUnit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Unit</em>' operation.
	 * @see conditions.Variable#isValid_hasValidUnit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariable__IsValid_hasValidUnit__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.Variable#isValid_hasValidVariableFormatTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Variable Format Tmpl Param</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Variable Format Tmpl Param</em>' operation.
	 * @see conditions.Variable#isValid_hasValidVariableFormatTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariable__IsValid_hasValidVariableFormatTmplParam__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.Variable#getObservers() <em>Get Observers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Observers</em>' operation.
	 * @see conditions.Variable#getObservers()
	 * @generated
	 */
	EOperation getVariable__GetObservers();

	/**
	 * Returns the meta object for class '{@link conditions.SignalVariable <em>Signal Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Variable</em>'.
	 * @see conditions.SignalVariable
	 * @generated
	 */
	EClass getSignalVariable();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpreted Value</em>'.
	 * @see conditions.SignalVariable#getInterpretedValue()
	 * @see #getSignalVariable()
	 * @generated
	 */
	EAttribute getSignalVariable_InterpretedValue();

	/**
	 * Returns the meta object for the reference '{@link conditions.SignalVariable#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signal</em>'.
	 * @see conditions.SignalVariable#getSignal()
	 * @see #getSignalVariable()
	 * @generated
	 */
	EReference getSignalVariable_Signal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SignalVariable#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpreted Value Tmpl Param</em>'.
	 * @see conditions.SignalVariable#getInterpretedValueTmplParam()
	 * @see #getSignalVariable()
	 * @generated
	 */
	EAttribute getSignalVariable_InterpretedValueTmplParam();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.SignalVariable#getSignalObservers <em>Signal Observers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signal Observers</em>'.
	 * @see conditions.SignalVariable#getSignalObservers()
	 * @see #getSignalVariable()
	 * @generated
	 */
	EReference getSignalVariable_SignalObservers();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SignalVariable#getLag <em>Lag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lag</em>'.
	 * @see conditions.SignalVariable#getLag()
	 * @see #getSignalVariable()
	 * @generated
	 */
	EAttribute getSignalVariable_Lag();

	/**
	 * Returns the meta object for class '{@link conditions.ValueVariable <em>Value Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Variable</em>'.
	 * @see conditions.ValueVariable
	 * @generated
	 */
	EClass getValueVariable();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ValueVariable#getInitialValue <em>Initial Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Value</em>'.
	 * @see conditions.ValueVariable#getInitialValue()
	 * @see #getValueVariable()
	 * @generated
	 */
	EAttribute getValueVariable_InitialValue();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.ValueVariable#getValueVariableObservers <em>Value Variable Observers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Variable Observers</em>'.
	 * @see conditions.ValueVariable#getValueVariableObservers()
	 * @see #getValueVariable()
	 * @generated
	 */
	EReference getValueVariable_ValueVariableObservers();

	/**
	 * Returns the meta object for class '{@link conditions.VariableFormat <em>Variable Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Format</em>'.
	 * @see conditions.VariableFormat
	 * @generated
	 */
	EClass getVariableFormat();

	/**
	 * Returns the meta object for the attribute '{@link conditions.VariableFormat#getDigits <em>Digits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Digits</em>'.
	 * @see conditions.VariableFormat#getDigits()
	 * @see #getVariableFormat()
	 * @generated
	 */
	EAttribute getVariableFormat_Digits();

	/**
	 * Returns the meta object for the attribute '{@link conditions.VariableFormat#getBaseDataType <em>Base Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Data Type</em>'.
	 * @see conditions.VariableFormat#getBaseDataType()
	 * @see #getVariableFormat()
	 * @generated
	 */
	EAttribute getVariableFormat_BaseDataType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.VariableFormat#isUpperCase <em>Upper Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Case</em>'.
	 * @see conditions.VariableFormat#isUpperCase()
	 * @see #getVariableFormat()
	 * @generated
	 */
	EAttribute getVariableFormat_UpperCase();

	/**
	 * Returns the meta object for the '{@link conditions.VariableFormat#isValid_hasValidDigits(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Digits</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Digits</em>' operation.
	 * @see conditions.VariableFormat#isValid_hasValidDigits(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVariableFormat__IsValid_hasValidDigits__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractObserver <em>Abstract Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Observer</em>'.
	 * @see conditions.AbstractObserver
	 * @generated
	 */
	EClass getAbstractObserver();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractObserver#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.AbstractObserver#getName()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EAttribute getAbstractObserver_Name();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractObserver#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.AbstractObserver#getDescription()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EAttribute getAbstractObserver_Description();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractObserver#getLogLevel <em>Log Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log Level</em>'.
	 * @see conditions.AbstractObserver#getLogLevel()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EAttribute getAbstractObserver_LogLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.AbstractObserver#getValueRanges <em>Value Ranges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value Ranges</em>'.
	 * @see conditions.AbstractObserver#getValueRanges()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EReference getAbstractObserver_ValueRanges();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractObserver#getValueRangesTmplParam <em>Value Ranges Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Ranges Tmpl Param</em>'.
	 * @see conditions.AbstractObserver#getValueRangesTmplParam()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EAttribute getAbstractObserver_ValueRangesTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractObserver#isActiveAtStart <em>Active At Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active At Start</em>'.
	 * @see conditions.AbstractObserver#isActiveAtStart()
	 * @see #getAbstractObserver()
	 * @generated
	 */
	EAttribute getAbstractObserver_ActiveAtStart();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractObserver#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Name</em>' operation.
	 * @see conditions.AbstractObserver#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractObserver__IsValid_hasValidName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractObserver#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * @see conditions.AbstractObserver#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractObserver__IsValid_hasValidLogLevel__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractObserver#isValid_hasValidValueRanges(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList) <em>Is Valid has Valid Value Ranges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value Ranges</em>' operation.
	 * @see conditions.AbstractObserver#isValid_hasValidValueRanges(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAbstractObserver__IsValid_hasValidValueRanges__EList_EList();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractObserver#isValid_hasValidValueRangesTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value Ranges Tmpl Param</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value Ranges Tmpl Param</em>' operation.
	 * @see conditions.AbstractObserver#isValid_hasValidValueRangesTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractObserver__IsValid_hasValidValueRangesTmplParam__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.ObserverValueRange <em>Observer Value Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observer Value Range</em>'.
	 * @see conditions.ObserverValueRange
	 * @generated
	 */
	EClass getObserverValueRange();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ObserverValueRange#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see conditions.ObserverValueRange#getValue()
	 * @see #getObserverValueRange()
	 * @generated
	 */
	EAttribute getObserverValueRange_Value();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ObserverValueRange#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.ObserverValueRange#getDescription()
	 * @see #getObserverValueRange()
	 * @generated
	 */
	EAttribute getObserverValueRange_Description();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ObserverValueRange#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Type</em>'.
	 * @see conditions.ObserverValueRange#getValueType()
	 * @see #getObserverValueRange()
	 * @generated
	 */
	EAttribute getObserverValueRange_ValueType();

	/**
	 * Returns the meta object for the '{@link conditions.ObserverValueRange#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value</em>' operation.
	 * @see conditions.ObserverValueRange#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getObserverValueRange__IsValid_hasValidValue__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.ObserverValueRange#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see conditions.ObserverValueRange#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getObserverValueRange__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.SignalObserver <em>Signal Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Observer</em>'.
	 * @see conditions.SignalObserver
	 * @generated
	 */
	EClass getSignalObserver();

	/**
	 * Returns the meta object for class '{@link conditions.SignalReferenceSet <em>Signal Reference Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Reference Set</em>'.
	 * @see conditions.SignalReferenceSet
	 * @generated
	 */
	EClass getSignalReferenceSet();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.SignalReferenceSet#getMessages <em>Messages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Messages</em>'.
	 * @see conditions.SignalReferenceSet#getMessages()
	 * @see #getSignalReferenceSet()
	 * @generated
	 */
	EReference getSignalReferenceSet_Messages();

	/**
	 * Returns the meta object for class '{@link conditions.SignalReference <em>Signal Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Signal Reference</em>'.
	 * @see conditions.SignalReference
	 * @generated
	 */
	EClass getSignalReference();

	/**
	 * Returns the meta object for the reference '{@link conditions.SignalReference#getSignal <em>Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signal</em>'.
	 * @see conditions.SignalReference#getSignal()
	 * @see #getSignalReference()
	 * @generated
	 */
	EReference getSignalReference_Signal();

	/**
	 * Returns the meta object for the '{@link conditions.SignalReference#isValid_hasValidSignal(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Signal</em>' operation.
	 * @see conditions.SignalReference#isValid_hasValidSignal(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSignalReference__IsValid_hasValidSignal__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.ISignalOrReference <em>ISignal Or Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISignal Or Reference</em>'.
	 * @see conditions.ISignalOrReference
	 * @generated
	 */
	EClass getISignalOrReference();

	/**
	 * Returns the meta object for class '{@link conditions.BitPatternComparatorValue <em>Bit Pattern Comparator Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bit Pattern Comparator Value</em>'.
	 * @see conditions.BitPatternComparatorValue
	 * @generated
	 */
	EClass getBitPatternComparatorValue();

	/**
	 * Returns the meta object for the attribute '{@link conditions.BitPatternComparatorValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see conditions.BitPatternComparatorValue#getValue()
	 * @see #getBitPatternComparatorValue()
	 * @generated
	 */
	EAttribute getBitPatternComparatorValue_Value();

	/**
	 * Returns the meta object for the attribute '{@link conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Startbit</em>'.
	 * @see conditions.BitPatternComparatorValue#getStartbit()
	 * @see #getBitPatternComparatorValue()
	 * @generated
	 */
	EAttribute getBitPatternComparatorValue_Startbit();

	/**
	 * Returns the meta object for the '{@link conditions.BitPatternComparatorValue#isValid_hasValidBitPattern(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bit Pattern</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Bit Pattern</em>' operation.
	 * @see conditions.BitPatternComparatorValue#isValid_hasValidBitPattern(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getBitPatternComparatorValue__IsValid_hasValidBitPattern__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.BitPatternComparatorValue#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Startbit</em>' operation.
	 * @see conditions.BitPatternComparatorValue#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getBitPatternComparatorValue__IsValid_hasValidStartbit__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.NotExpression <em>Not Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not Expression</em>'.
	 * @see conditions.NotExpression
	 * @generated
	 */
	EClass getNotExpression();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.NotExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see conditions.NotExpression#getExpression()
	 * @see #getNotExpression()
	 * @generated
	 */
	EReference getNotExpression_Expression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.NotExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see conditions.NotExpression#getOperator()
	 * @see #getNotExpression()
	 * @generated
	 */
	EAttribute getNotExpression_Operator();

	/**
	 * Returns the meta object for class '{@link conditions.IOperand <em>IOperand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IOperand</em>'.
	 * @see conditions.IOperand
	 * @generated
	 */
	EClass getIOperand();

	/**
	 * Returns the meta object for the '{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Evaluation Data Type</em>' operation.
	 * @see conditions.IOperand#get_EvaluationDataType()
	 * @generated
	 */
	EOperation getIOperand__Get_EvaluationDataType();

	/**
	 * Returns the meta object for class '{@link conditions.IComputeVariableActionOperand <em>ICompute Variable Action Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ICompute Variable Action Operand</em>'.
	 * @see conditions.IComputeVariableActionOperand
	 * @generated
	 */
	EClass getIComputeVariableActionOperand();

	/**
	 * Returns the meta object for class '{@link conditions.IStringOperand <em>IString Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IString Operand</em>'.
	 * @see conditions.IStringOperand
	 * @generated
	 */
	EClass getIStringOperand();

	/**
	 * Returns the meta object for class '{@link conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison Expression Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ISignal Comparison Expression Operand</em>'.
	 * @see conditions.ISignalComparisonExpressionOperand
	 * @generated
	 */
	EClass getISignalComparisonExpressionOperand();

	/**
	 * Returns the meta object for class '{@link conditions.IOperation <em>IOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IOperation</em>'.
	 * @see conditions.IOperation
	 * @generated
	 */
	EClass getIOperation();

	/**
	 * Returns the meta object for the '{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operand Data Type</em>' operation.
	 * @see conditions.IOperation#get_OperandDataType()
	 * @generated
	 */
	EOperation getIOperation__Get_OperandDataType();

	/**
	 * Returns the meta object for the '{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Operands</em>' operation.
	 * @see conditions.IOperation#get_Operands()
	 * @generated
	 */
	EOperation getIOperation__Get_Operands();

	/**
	 * Returns the meta object for class '{@link conditions.IStringOperation <em>IString Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IString Operation</em>'.
	 * @see conditions.IStringOperation
	 * @generated
	 */
	EClass getIStringOperation();

	/**
	 * Returns the meta object for class '{@link conditions.INumericOperand <em>INumeric Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>INumeric Operand</em>'.
	 * @see conditions.INumericOperand
	 * @generated
	 */
	EClass getINumericOperand();

	/**
	 * Returns the meta object for class '{@link conditions.INumericOperation <em>INumeric Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>INumeric Operation</em>'.
	 * @see conditions.INumericOperation
	 * @generated
	 */
	EClass getINumericOperation();

	/**
	 * Returns the meta object for class '{@link conditions.ValueVariableObserver <em>Value Variable Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value Variable Observer</em>'.
	 * @see conditions.ValueVariableObserver
	 * @generated
	 */
	EClass getValueVariableObserver();

	/**
	 * Returns the meta object for class '{@link conditions.ParseStringToDouble <em>Parse String To Double</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parse String To Double</em>'.
	 * @see conditions.ParseStringToDouble
	 * @generated
	 */
	EClass getParseStringToDouble();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.ParseStringToDouble#getStringToParse <em>String To Parse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String To Parse</em>'.
	 * @see conditions.ParseStringToDouble#getStringToParse()
	 * @see #getParseStringToDouble()
	 * @generated
	 */
	EReference getParseStringToDouble_StringToParse();

	/**
	 * Returns the meta object for class '{@link conditions.StringExpression <em>String Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Expression</em>'.
	 * @see conditions.StringExpression
	 * @generated
	 */
	EClass getStringExpression();

	/**
	 * Returns the meta object for class '{@link conditions.Matches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Matches</em>'.
	 * @see conditions.Matches
	 * @generated
	 */
	EClass getMatches();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.Matches#getStringToCheck <em>String To Check</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String To Check</em>'.
	 * @see conditions.Matches#getStringToCheck()
	 * @see #getMatches()
	 * @generated
	 */
	EReference getMatches_StringToCheck();

	/**
	 * Returns the meta object for the '{@link conditions.Matches#isValid_hasValidStringToCheck(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid String To Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid String To Check</em>' operation.
	 * @see conditions.Matches#isValid_hasValidStringToCheck(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getMatches__IsValid_hasValidStringToCheck__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.Extract <em>Extract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extract</em>'.
	 * @see conditions.Extract
	 * @generated
	 */
	EClass getExtract();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.Extract#getStringToExtract <em>String To Extract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String To Extract</em>'.
	 * @see conditions.Extract#getStringToExtract()
	 * @see #getExtract()
	 * @generated
	 */
	EReference getExtract_StringToExtract();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Extract#getGroupIndex <em>Group Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Group Index</em>'.
	 * @see conditions.Extract#getGroupIndex()
	 * @see #getExtract()
	 * @generated
	 */
	EAttribute getExtract_GroupIndex();

	/**
	 * Returns the meta object for class '{@link conditions.Substring <em>Substring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Substring</em>'.
	 * @see conditions.Substring
	 * @generated
	 */
	EClass getSubstring();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.Substring#getStringToExtract <em>String To Extract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String To Extract</em>'.
	 * @see conditions.Substring#getStringToExtract()
	 * @see #getSubstring()
	 * @generated
	 */
	EReference getSubstring_StringToExtract();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Substring#getStartIndex <em>Start Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Index</em>'.
	 * @see conditions.Substring#getStartIndex()
	 * @see #getSubstring()
	 * @generated
	 */
	EAttribute getSubstring_StartIndex();

	/**
	 * Returns the meta object for the attribute '{@link conditions.Substring#getEndIndex <em>End Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Index</em>'.
	 * @see conditions.Substring#getEndIndex()
	 * @see #getSubstring()
	 * @generated
	 */
	EAttribute getSubstring_EndIndex();

	/**
	 * Returns the meta object for class '{@link conditions.ParseNumericToString <em>Parse Numeric To String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parse Numeric To String</em>'.
	 * @see conditions.ParseNumericToString
	 * @generated
	 */
	EClass getParseNumericToString();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.ParseNumericToString#getNumericOperandToBeParsed <em>Numeric Operand To Be Parsed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Numeric Operand To Be Parsed</em>'.
	 * @see conditions.ParseNumericToString#getNumericOperandToBeParsed()
	 * @see #getParseNumericToString()
	 * @generated
	 */
	EReference getParseNumericToString_NumericOperandToBeParsed();

	/**
	 * Returns the meta object for class '{@link conditions.StringLength <em>String Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Length</em>'.
	 * @see conditions.StringLength
	 * @generated
	 */
	EClass getStringLength();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.StringLength#getStringOperand <em>String Operand</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>String Operand</em>'.
	 * @see conditions.StringLength#getStringOperand()
	 * @see #getStringLength()
	 * @generated
	 */
	EReference getStringLength_StringOperand();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractMessage <em>Abstract Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Message</em>'.
	 * @see conditions.AbstractMessage
	 * @generated
	 */
	EClass getAbstractMessage();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.AbstractMessage#getSignals <em>Signals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Signals</em>'.
	 * @see conditions.AbstractMessage#getSignals()
	 * @see #getAbstractMessage()
	 * @generated
	 */
	EReference getAbstractMessage_Signals();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractMessage#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.AbstractMessage#getName()
	 * @see #getAbstractMessage()
	 * @generated
	 */
	EAttribute getAbstractMessage_Name();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractMessage#getPrimaryFilter() <em>Get Primary Filter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Primary Filter</em>' operation.
	 * @see conditions.AbstractMessage#getPrimaryFilter()
	 * @generated
	 */
	EOperation getAbstractMessage__GetPrimaryFilter();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractMessage#getFilters(boolean) <em>Get Filters</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Filters</em>' operation.
	 * @see conditions.AbstractMessage#getFilters(boolean)
	 * @generated
	 */
	EOperation getAbstractMessage__GetFilters__boolean();

	/**
	 * Returns the meta object for class '{@link conditions.SomeIPMessage <em>Some IP Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Some IP Message</em>'.
	 * @see conditions.SomeIPMessage
	 * @generated
	 */
	EClass getSomeIPMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Some IP Filter</em>'.
	 * @see conditions.SomeIPMessage#getSomeIPFilter()
	 * @see #getSomeIPMessage()
	 * @generated
	 */
	EReference getSomeIPMessage_SomeIPFilter();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractSignal <em>Abstract Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Signal</em>'.
	 * @see conditions.AbstractSignal
	 * @generated
	 */
	EClass getAbstractSignal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractSignal#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.AbstractSignal#getName()
	 * @see #getAbstractSignal()
	 * @generated
	 */
	EAttribute getAbstractSignal_Name();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.AbstractSignal#getDecodeStrategy <em>Decode Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Decode Strategy</em>'.
	 * @see conditions.AbstractSignal#getDecodeStrategy()
	 * @see #getAbstractSignal()
	 * @generated
	 */
	EReference getAbstractSignal_DecodeStrategy();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Extract Strategy</em>'.
	 * @see conditions.AbstractSignal#getExtractStrategy()
	 * @see #getAbstractSignal()
	 * @generated
	 */
	EReference getAbstractSignal_ExtractStrategy();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractSignal#getMessage() <em>Get Message</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Message</em>' operation.
	 * @see conditions.AbstractSignal#getMessage()
	 * @generated
	 */
	EOperation getAbstractSignal__GetMessage();

	/**
	 * Returns the meta object for class '{@link conditions.ContainerSignal <em>Container Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Signal</em>'.
	 * @see conditions.ContainerSignal
	 * @generated
	 */
	EClass getContainerSignal();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.ContainerSignal#getContainedSignals <em>Contained Signals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Contained Signals</em>'.
	 * @see conditions.ContainerSignal#getContainedSignals()
	 * @see #getContainerSignal()
	 * @generated
	 */
	EReference getContainerSignal_ContainedSignals();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractFilter <em>Abstract Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Filter</em>'.
	 * @see conditions.AbstractFilter
	 * @generated
	 */
	EClass getAbstractFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractFilter#getPayloadLength <em>Payload Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Payload Length</em>'.
	 * @see conditions.AbstractFilter#getPayloadLength()
	 * @see #getAbstractFilter()
	 * @generated
	 */
	EAttribute getAbstractFilter_PayloadLength();

	/**
	 * Returns the meta object for class '{@link conditions.EthernetFilter <em>Ethernet Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ethernet Filter</em>'.
	 * @see conditions.EthernetFilter
	 * @generated
	 */
	EClass getEthernetFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getSourceMAC <em>Source MAC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source MAC</em>'.
	 * @see conditions.EthernetFilter#getSourceMAC()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_SourceMAC();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getDestMAC <em>Dest MAC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dest MAC</em>'.
	 * @see conditions.EthernetFilter#getDestMAC()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_DestMAC();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getEtherType <em>Ether Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ether Type</em>'.
	 * @see conditions.EthernetFilter#getEtherType()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_EtherType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getInnerVlanId <em>Inner Vlan Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner Vlan Id</em>'.
	 * @see conditions.EthernetFilter#getInnerVlanId()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_InnerVlanId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getOuterVlanId <em>Outer Vlan Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outer Vlan Id</em>'.
	 * @see conditions.EthernetFilter#getOuterVlanId()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_OuterVlanId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getInnerVlanCFI <em>Inner Vlan CFI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner Vlan CFI</em>'.
	 * @see conditions.EthernetFilter#getInnerVlanCFI()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_InnerVlanCFI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getOuterVlanCFI <em>Outer Vlan CFI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outer Vlan CFI</em>'.
	 * @see conditions.EthernetFilter#getOuterVlanCFI()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_OuterVlanCFI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getInnerVlanVID <em>Inner Vlan VID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner Vlan VID</em>'.
	 * @see conditions.EthernetFilter#getInnerVlanVID()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_InnerVlanVID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getOuterVlanVID <em>Outer Vlan VID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outer Vlan VID</em>'.
	 * @see conditions.EthernetFilter#getOuterVlanVID()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_OuterVlanVID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getCRC <em>CRC</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>CRC</em>'.
	 * @see conditions.EthernetFilter#getCRC()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_CRC();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getInnerVlanTPID <em>Inner Vlan TPID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner Vlan TPID</em>'.
	 * @see conditions.EthernetFilter#getInnerVlanTPID()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_InnerVlanTPID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.EthernetFilter#getOuterVlanTPID <em>Outer Vlan TPID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Outer Vlan TPID</em>'.
	 * @see conditions.EthernetFilter#getOuterVlanTPID()
	 * @see #getEthernetFilter()
	 * @generated
	 */
	EAttribute getEthernetFilter_OuterVlanTPID();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidSourceMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source MAC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Source MAC</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidSourceMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidSourceMAC__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidDestinationMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination MAC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Destination MAC</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidDestinationMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidDestinationMAC__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidCRC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid CRC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid CRC</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidCRC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidCRC__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidEtherType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ether Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Ether Type</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidEtherType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidEtherType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan Vid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Inner Vlan Vid</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidInnerVlanVid__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan Vid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Outer Vlan Vid</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidOuterVlanVid__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan CFI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Inner Vlan CFI</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidInnerVlanCFI__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan CFI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Outer Vlan CFI</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidOuterVlanCFI__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan PCP</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Inner Vlan PCP</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidInnerVlanPCP__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan PCP</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Outer Vlan PCP</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidOuterVlanPCP__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan TPID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Inner Vlan TPID</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidInnerVlanTPID__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan TPID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Outer Vlan TPID</em>' operation.
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getEthernetFilter__IsValid_hasValidOuterVlanTPID__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.UDPFilter <em>UDP Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UDP Filter</em>'.
	 * @see conditions.UDPFilter
	 * @generated
	 */
	EClass getUDPFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UDPFilter#getChecksum <em>Checksum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Checksum</em>'.
	 * @see conditions.UDPFilter#getChecksum()
	 * @see #getUDPFilter()
	 * @generated
	 */
	EAttribute getUDPFilter_Checksum();

	/**
	 * Returns the meta object for the '{@link conditions.UDPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Source Port</em>' operation.
	 * @see conditions.UDPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPFilter__IsValid_hasValidSourcePort__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UDPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Destination Port</em>' operation.
	 * @see conditions.UDPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPFilter__IsValid_hasValidDestinationPort__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UDPFilter#isValid_hasValidChecksum(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Checksum</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Checksum</em>' operation.
	 * @see conditions.UDPFilter#isValid_hasValidChecksum(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPFilter__IsValid_hasValidChecksum__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.TCPFilter <em>TCP Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TCP Filter</em>'.
	 * @see conditions.TCPFilter
	 * @generated
	 */
	EClass getTCPFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getSequenceNumber <em>Sequence Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sequence Number</em>'.
	 * @see conditions.TCPFilter#getSequenceNumber()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_SequenceNumber();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getAcknowledgementNumber <em>Acknowledgement Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Acknowledgement Number</em>'.
	 * @see conditions.TCPFilter#getAcknowledgementNumber()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_AcknowledgementNumber();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getTcpFlags <em>Tcp Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tcp Flags</em>'.
	 * @see conditions.TCPFilter#getTcpFlags()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_TcpFlags();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stream Analysis Flags</em>'.
	 * @see conditions.TCPFilter#getStreamAnalysisFlags()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_StreamAnalysisFlags();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stream Analysis Flags Tmpl Param</em>'.
	 * @see conditions.TCPFilter#getStreamAnalysisFlagsTmplParam()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_StreamAnalysisFlagsTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TCPFilter#getStreamValidPayloadOffset <em>Stream Valid Payload Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stream Valid Payload Offset</em>'.
	 * @see conditions.TCPFilter#getStreamValidPayloadOffset()
	 * @see #getTCPFilter()
	 * @generated
	 */
	EAttribute getTCPFilter_StreamValidPayloadOffset();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Source Port</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidSourcePort__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Destination Port</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidDestinationPort__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidSequenceNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sequence Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Sequence Number</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidSequenceNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidSequenceNumber__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidAcknowledgementNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Acknowledgement Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Acknowledgement Number</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidAcknowledgementNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidAcknowledgementNumber__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidTcpFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Tcp Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Tcp Flag</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidTcpFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidTcpFlag__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidStreamAnalysisFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Analysis Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Stream Analysis Flag</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidStreamAnalysisFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidStreamAnalysisFlag__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.TCPFilter#isValid_hasValidStreamValidPayloadOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Valid Payload Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Stream Valid Payload Offset</em>' operation.
	 * @see conditions.TCPFilter#isValid_hasValidStreamValidPayloadOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTCPFilter__IsValid_hasValidStreamValidPayloadOffset__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.IPv4Filter <em>IPv4 Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPv4 Filter</em>'.
	 * @see conditions.IPv4Filter
	 * @generated
	 */
	EClass getIPv4Filter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.IPv4Filter#getProtocolType <em>Protocol Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Type</em>'.
	 * @see conditions.IPv4Filter#getProtocolType()
	 * @see #getIPv4Filter()
	 * @generated
	 */
	EAttribute getIPv4Filter_ProtocolType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.IPv4Filter#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Type Tmpl Param</em>'.
	 * @see conditions.IPv4Filter#getProtocolTypeTmplParam()
	 * @see #getIPv4Filter()
	 * @generated
	 */
	EAttribute getIPv4Filter_ProtocolTypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.IPv4Filter#getSourceIP <em>Source IP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source IP</em>'.
	 * @see conditions.IPv4Filter#getSourceIP()
	 * @see #getIPv4Filter()
	 * @generated
	 */
	EAttribute getIPv4Filter_SourceIP();

	/**
	 * Returns the meta object for the attribute '{@link conditions.IPv4Filter#getDestIP <em>Dest IP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dest IP</em>'.
	 * @see conditions.IPv4Filter#getDestIP()
	 * @see #getIPv4Filter()
	 * @generated
	 */
	EAttribute getIPv4Filter_DestIP();

	/**
	 * Returns the meta object for the attribute '{@link conditions.IPv4Filter#getTimeToLive <em>Time To Live</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time To Live</em>'.
	 * @see conditions.IPv4Filter#getTimeToLive()
	 * @see #getIPv4Filter()
	 * @generated
	 */
	EAttribute getIPv4Filter_TimeToLive();

	/**
	 * Returns the meta object for the '{@link conditions.IPv4Filter#isValid_hasValidSourceIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Ip Address</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Source Ip Address</em>' operation.
	 * @see conditions.IPv4Filter#isValid_hasValidSourceIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getIPv4Filter__IsValid_hasValidSourceIpAddress__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.IPv4Filter#isValid_hasValidDestinationIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Ip Address</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Destination Ip Address</em>' operation.
	 * @see conditions.IPv4Filter#isValid_hasValidDestinationIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getIPv4Filter__IsValid_hasValidDestinationIpAddress__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.IPv4Filter#isValid_hasValidTimeToLive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Time To Live</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Time To Live</em>' operation.
	 * @see conditions.IPv4Filter#isValid_hasValidTimeToLive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getIPv4Filter__IsValid_hasValidTimeToLive__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.SomeIPFilter <em>Some IP Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Some IP Filter</em>'.
	 * @see conditions.SomeIPFilter
	 * @generated
	 */
	EClass getSomeIPFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getServiceId <em>Service Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Service Id</em>'.
	 * @see conditions.SomeIPFilter#getServiceId()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_ServiceId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getMethodType <em>Method Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Type</em>'.
	 * @see conditions.SomeIPFilter#getMethodType()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_MethodType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getMethodTypeTmplParam <em>Method Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Type Tmpl Param</em>'.
	 * @see conditions.SomeIPFilter#getMethodTypeTmplParam()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_MethodTypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getMethodId <em>Method Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Method Id</em>'.
	 * @see conditions.SomeIPFilter#getMethodId()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_MethodId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getClientId <em>Client Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Client Id</em>'.
	 * @see conditions.SomeIPFilter#getClientId()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_ClientId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getSessionId <em>Session Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Session Id</em>'.
	 * @see conditions.SomeIPFilter#getSessionId()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_SessionId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getProtocolVersion <em>Protocol Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Protocol Version</em>'.
	 * @see conditions.SomeIPFilter#getProtocolVersion()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_ProtocolVersion();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getInterfaceVersion <em>Interface Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interface Version</em>'.
	 * @see conditions.SomeIPFilter#getInterfaceVersion()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_InterfaceVersion();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getMessageType <em>Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Type</em>'.
	 * @see conditions.SomeIPFilter#getMessageType()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_MessageType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getMessageTypeTmplParam <em>Message Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Type Tmpl Param</em>'.
	 * @see conditions.SomeIPFilter#getMessageTypeTmplParam()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_MessageTypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getReturnCode <em>Return Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Code</em>'.
	 * @see conditions.SomeIPFilter#getReturnCode()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_ReturnCode();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getReturnCodeTmplParam <em>Return Code Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Code Tmpl Param</em>'.
	 * @see conditions.SomeIPFilter#getReturnCodeTmplParam()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_ReturnCodeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPFilter#getSomeIPLength <em>Some IP Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Some IP Length</em>'.
	 * @see conditions.SomeIPFilter#getSomeIPLength()
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	EAttribute getSomeIPFilter_SomeIPLength();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidServiceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Service Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Service Id</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidServiceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidServiceId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidMethodId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Method Id</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidMethodId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidMethodId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidSessionId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Session Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Session Id</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidSessionId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidSessionId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidClientId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Client Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Client Id</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidClientId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidClientId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Length</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidLength__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidProtocolVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Protocol Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Protocol Version</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidProtocolVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidProtocolVersion__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidInterfaceVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interface Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Interface Version</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidInterfaceVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidInterfaceVersion__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Type</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidMessageType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidReturnCode(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Return Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Return Code</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidReturnCode(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidReturnCode__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPFilter#isValid_hasValidMethodType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Method Type</em>' operation.
	 * @see conditions.SomeIPFilter#isValid_hasValidMethodType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPFilter__IsValid_hasValidMethodType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.ForEachExpression <em>For Each Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Each Expression</em>'.
	 * @see conditions.ForEachExpression
	 * @generated
	 */
	EClass getForEachExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.ForEachExpression#getFilterExpressions <em>Filter Expressions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filter Expressions</em>'.
	 * @see conditions.ForEachExpression#getFilterExpressions()
	 * @see #getForEachExpression()
	 * @generated
	 */
	EReference getForEachExpression_FilterExpressions();

	/**
	 * Returns the meta object for the reference '{@link conditions.ForEachExpression#getContainerSignal <em>Container Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Container Signal</em>'.
	 * @see conditions.ForEachExpression#getContainerSignal()
	 * @see #getForEachExpression()
	 * @generated
	 */
	EReference getForEachExpression_ContainerSignal();

	/**
	 * Returns the meta object for class '{@link conditions.MessageCheckExpression <em>Message Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Check Expression</em>'.
	 * @see conditions.MessageCheckExpression
	 * @generated
	 */
	EClass getMessageCheckExpression();

	/**
	 * Returns the meta object for the reference '{@link conditions.MessageCheckExpression#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Message</em>'.
	 * @see conditions.MessageCheckExpression#getMessage()
	 * @see #getMessageCheckExpression()
	 * @generated
	 */
	EReference getMessageCheckExpression_Message();

	/**
	 * Returns the meta object for class '{@link conditions.SomeIPSDFilter <em>Some IPSD Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Some IPSD Filter</em>'.
	 * @see conditions.SomeIPSDFilter
	 * @generated
	 */
	EClass getSomeIPSDFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getFlags <em>Flags</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flags</em>'.
	 * @see conditions.SomeIPSDFilter#getFlags()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_Flags();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getFlagsTmplParam <em>Flags Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Flags Tmpl Param</em>'.
	 * @see conditions.SomeIPSDFilter#getFlagsTmplParam()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_FlagsTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getSdType <em>Sd Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sd Type</em>'.
	 * @see conditions.SomeIPSDFilter#getSdType()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_SdType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getSdTypeTmplParam <em>Sd Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sd Type Tmpl Param</em>'.
	 * @see conditions.SomeIPSDFilter#getSdTypeTmplParam()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_SdTypeTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getInstanceId <em>Instance Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance Id</em>'.
	 * @see conditions.SomeIPSDFilter#getInstanceId()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_InstanceId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getTtl <em>Ttl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ttl</em>'.
	 * @see conditions.SomeIPSDFilter#getTtl()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_Ttl();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getMajorVersion <em>Major Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Major Version</em>'.
	 * @see conditions.SomeIPSDFilter#getMajorVersion()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_MajorVersion();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getMinorVersion <em>Minor Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minor Version</em>'.
	 * @see conditions.SomeIPSDFilter#getMinorVersion()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_MinorVersion();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getEventGroupId <em>Event Group Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Group Id</em>'.
	 * @see conditions.SomeIPSDFilter#getEventGroupId()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_EventGroupId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getIndexFirstOption <em>Index First Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index First Option</em>'.
	 * @see conditions.SomeIPSDFilter#getIndexFirstOption()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_IndexFirstOption();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getIndexSecondOption <em>Index Second Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index Second Option</em>'.
	 * @see conditions.SomeIPSDFilter#getIndexSecondOption()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_IndexSecondOption();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getNumberFirstOption <em>Number First Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number First Option</em>'.
	 * @see conditions.SomeIPSDFilter#getNumberFirstOption()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_NumberFirstOption();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getNumberSecondOption <em>Number Second Option</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Second Option</em>'.
	 * @see conditions.SomeIPSDFilter#getNumberSecondOption()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_NumberSecondOption();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SomeIPSDFilter#getServiceId_SomeIPSD <em>Service Id Some IPSD</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Service Id Some IPSD</em>'.
	 * @see conditions.SomeIPSDFilter#getServiceId_SomeIPSD()
	 * @see #getSomeIPSDFilter()
	 * @generated
	 */
	EAttribute getSomeIPSDFilter_ServiceId_SomeIPSD();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidInstanceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Instance Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Instance Id</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidInstanceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidInstanceId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidEventGroupId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Event Group Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Event Group Id</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidEventGroupId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidEventGroupId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidFlags(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Flags</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Flags</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidFlags(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidFlags__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidSdType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sd Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Sd Type</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidSdType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidSdType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidMajorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Major Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Major Version</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidMajorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidMajorVersion__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidMinorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Minor Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Minor Version</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidMinorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidMinorVersion__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidIndexFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index First Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Index First Option</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidIndexFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidIndexFirstOption__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidIndexSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index Second Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Index Second Option</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidIndexSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidIndexSecondOption__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidNumberFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number First Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Number First Option</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidNumberFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidNumberFirstOption__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.SomeIPSDFilter#isValid_hasValidNumberSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number Second Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Number Second Option</em>' operation.
	 * @see conditions.SomeIPSDFilter#isValid_hasValidNumberSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getSomeIPSDFilter__IsValid_hasValidNumberSecondOption__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.SomeIPSDMessage <em>Some IPSD Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Some IPSD Message</em>'.
	 * @see conditions.SomeIPSDMessage
	 * @generated
	 */
	EClass getSomeIPSDMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Some IPSD Filter</em>'.
	 * @see conditions.SomeIPSDMessage#getSomeIPSDFilter()
	 * @see #getSomeIPSDMessage()
	 * @generated
	 */
	EReference getSomeIPSDMessage_SomeIPSDFilter();

	/**
	 * Returns the meta object for class '{@link conditions.DoubleSignal <em>Double Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Signal</em>'.
	 * @see conditions.DoubleSignal
	 * @generated
	 */
	EClass getDoubleSignal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ignore Invalid Values</em>'.
	 * @see conditions.DoubleSignal#getIgnoreInvalidValues()
	 * @see #getDoubleSignal()
	 * @generated
	 */
	EAttribute getDoubleSignal_IgnoreInvalidValues();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ignore Invalid Values Tmpl Param</em>'.
	 * @see conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam()
	 * @see #getDoubleSignal()
	 * @generated
	 */
	EAttribute getDoubleSignal_IgnoreInvalidValuesTmplParam();

	/**
	 * Returns the meta object for class '{@link conditions.StringSignal <em>String Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Signal</em>'.
	 * @see conditions.StringSignal
	 * @generated
	 */
	EClass getStringSignal();

	/**
	 * Returns the meta object for class '{@link conditions.DecodeStrategy <em>Decode Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decode Strategy</em>'.
	 * @see conditions.DecodeStrategy
	 * @generated
	 */
	EClass getDecodeStrategy();

	/**
	 * Returns the meta object for class '{@link conditions.DoubleDecodeStrategy <em>Double Decode Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Decode Strategy</em>'.
	 * @see conditions.DoubleDecodeStrategy
	 * @generated
	 */
	EClass getDoubleDecodeStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DoubleDecodeStrategy#getFactor <em>Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Factor</em>'.
	 * @see conditions.DoubleDecodeStrategy#getFactor()
	 * @see #getDoubleDecodeStrategy()
	 * @generated
	 */
	EAttribute getDoubleDecodeStrategy_Factor();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DoubleDecodeStrategy#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see conditions.DoubleDecodeStrategy#getOffset()
	 * @see #getDoubleDecodeStrategy()
	 * @generated
	 */
	EAttribute getDoubleDecodeStrategy_Offset();

	/**
	 * Returns the meta object for the '{@link conditions.DoubleDecodeStrategy#isValid_hasValidFactor(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Factor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Factor</em>' operation.
	 * @see conditions.DoubleDecodeStrategy#isValid_hasValidFactor(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDoubleDecodeStrategy__IsValid_hasValidFactor__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.DoubleDecodeStrategy#isValid_hasValidOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Offset</em>' operation.
	 * @see conditions.DoubleDecodeStrategy#isValid_hasValidOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDoubleDecodeStrategy__IsValid_hasValidOffset__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.ExtractStrategy <em>Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extract Strategy</em>'.
	 * @see conditions.ExtractStrategy
	 * @generated
	 */
	EClass getExtractStrategy();

	/**
	 * Returns the meta object for the container reference '{@link conditions.ExtractStrategy#getAbstractSignal <em>Abstract Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Abstract Signal</em>'.
	 * @see conditions.ExtractStrategy#getAbstractSignal()
	 * @see #getExtractStrategy()
	 * @generated
	 */
	EReference getExtractStrategy_AbstractSignal();

	/**
	 * Returns the meta object for class '{@link conditions.UniversalPayloadExtractStrategy <em>Universal Payload Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Universal Payload Extract Strategy</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy
	 * @generated
	 */
	EClass getUniversalPayloadExtractStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadExtractStrategy#getExtractionRule <em>Extraction Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extraction Rule</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy#getExtractionRule()
	 * @see #getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadExtractStrategy_ExtractionRule();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadExtractStrategy#getByteOrder <em>Byte Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Byte Order</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy#getByteOrder()
	 * @see #getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadExtractStrategy_ByteOrder();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam <em>Extraction Rule Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extraction Rule Tmpl Param</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam()
	 * @see #getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadExtractStrategy#getSignalDataType <em>Signal Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signal Data Type</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy#getSignalDataType()
	 * @see #getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadExtractStrategy_SignalDataType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam <em>Signal Data Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signal Data Type Tmpl Param</em>'.
	 * @see conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam()
	 * @see #getUniversalPayloadExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidExtractString(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Extract String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Extract String</em>' operation.
	 * @see conditions.UniversalPayloadExtractStrategy#isValid_hasValidExtractString(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUniversalPayloadExtractStrategy__IsValid_hasValidExtractString__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidSignalDataType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Signal Data Type</em>' operation.
	 * @see conditions.UniversalPayloadExtractStrategy#isValid_hasValidSignalDataType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUniversalPayloadExtractStrategy__IsValid_hasValidSignalDataType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.EmptyExtractStrategy <em>Empty Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Empty Extract Strategy</em>'.
	 * @see conditions.EmptyExtractStrategy
	 * @generated
	 */
	EClass getEmptyExtractStrategy();

	/**
	 * Returns the meta object for class '{@link conditions.CANFilter <em>CAN Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CAN Filter</em>'.
	 * @see conditions.CANFilter
	 * @generated
	 */
	EClass getCANFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getMessageIdRange <em>Message Id Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Id Range</em>'.
	 * @see conditions.CANFilter#getMessageIdRange()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_MessageIdRange();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getFrameId <em>Frame Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Id</em>'.
	 * @see conditions.CANFilter#getFrameId()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_FrameId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getRxtxFlag <em>Rxtx Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rxtx Flag</em>'.
	 * @see conditions.CANFilter#getRxtxFlag()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_RxtxFlag();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getRxtxFlagTmplParam <em>Rxtx Flag Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rxtx Flag Tmpl Param</em>'.
	 * @see conditions.CANFilter#getRxtxFlagTmplParam()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_RxtxFlagTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getExtIdentifier <em>Ext Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ext Identifier</em>'.
	 * @see conditions.CANFilter#getExtIdentifier()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_ExtIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ext Identifier Tmpl Param</em>'.
	 * @see conditions.CANFilter#getExtIdentifierTmplParam()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_ExtIdentifierTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getFrameType <em>Frame Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Type</em>'.
	 * @see conditions.CANFilter#getFrameType()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_FrameType();

	/**
	 * Returns the meta object for the attribute '{@link conditions.CANFilter#getFrameTypeTmplParam <em>Frame Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Type Tmpl Param</em>'.
	 * @see conditions.CANFilter#getFrameTypeTmplParam()
	 * @see #getCANFilter()
	 * @generated
	 */
	EAttribute getCANFilter_FrameTypeTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.CANFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * @see conditions.CANFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCANFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CANFilter#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Rx Tx Flag</em>' operation.
	 * @see conditions.CANFilter#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCANFilter__IsValid_hasValidRxTxFlag__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.CANFilter#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Ext Identifier</em>' operation.
	 * @see conditions.CANFilter#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getCANFilter__IsValid_hasValidExtIdentifier__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.LINFilter <em>LIN Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LIN Filter</em>'.
	 * @see conditions.LINFilter
	 * @generated
	 */
	EClass getLINFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LINFilter#getMessageIdRange <em>Message Id Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Id Range</em>'.
	 * @see conditions.LINFilter#getMessageIdRange()
	 * @see #getLINFilter()
	 * @generated
	 */
	EAttribute getLINFilter_MessageIdRange();

	/**
	 * Returns the meta object for the attribute '{@link conditions.LINFilter#getFrameId <em>Frame Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Frame Id</em>'.
	 * @see conditions.LINFilter#getFrameId()
	 * @see #getLINFilter()
	 * @generated
	 */
	EAttribute getLINFilter_FrameId();

	/**
	 * Returns the meta object for the '{@link conditions.LINFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * @see conditions.LINFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getLINFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.FlexRayFilter <em>Flex Ray Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flex Ray Filter</em>'.
	 * @see conditions.FlexRayFilter
	 * @generated
	 */
	EClass getFlexRayFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Id Range</em>'.
	 * @see conditions.FlexRayFilter#getMessageIdRange()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_MessageIdRange();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Slot Id</em>'.
	 * @see conditions.FlexRayFilter#getSlotId()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_SlotId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cycle Offset</em>'.
	 * @see conditions.FlexRayFilter#getCycleOffset()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_CycleOffset();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cycle Repetition</em>'.
	 * @see conditions.FlexRayFilter#getCycleRepetition()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_CycleRepetition();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getChannel <em>Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Channel</em>'.
	 * @see conditions.FlexRayFilter#getChannel()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_Channel();

	/**
	 * Returns the meta object for the attribute '{@link conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Channel Tmpl Param</em>'.
	 * @see conditions.FlexRayFilter#getChannelTmplParam()
	 * @see #getFlexRayFilter()
	 * @generated
	 */
	EAttribute getFlexRayFilter_ChannelTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Frame Id Or Frame Id Range</em>' operation.
	 * @see conditions.FlexRayFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayFilter__IsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayFilter#isValid_hasValidChannelType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Channel Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Channel Type</em>' operation.
	 * @see conditions.FlexRayFilter#isValid_hasValidChannelType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayFilter__IsValid_hasValidChannelType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayFilter#isValid_hasValidCycleOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Cycle Offset</em>' operation.
	 * @see conditions.FlexRayFilter#isValid_hasValidCycleOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayFilter__IsValid_hasValidCycleOffset__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.FlexRayFilter#isValid_hasValidCycleRepeatInterval(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Repeat Interval</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Cycle Repeat Interval</em>' operation.
	 * @see conditions.FlexRayFilter#isValid_hasValidCycleRepeatInterval(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getFlexRayFilter__IsValid_hasValidCycleRepeatInterval__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.DLTFilter <em>DLT Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DLT Filter</em>'.
	 * @see conditions.DLTFilter
	 * @generated
	 */
	EClass getDLTFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getEcuID_ECU <em>Ecu ID ECU</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ecu ID ECU</em>'.
	 * @see conditions.DLTFilter#getEcuID_ECU()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_EcuID_ECU();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getSessionID_SEID <em>Session ID SEID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Session ID SEID</em>'.
	 * @see conditions.DLTFilter#getSessionID_SEID()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_SessionID_SEID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getApplicationID_APID <em>Application ID APID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Application ID APID</em>'.
	 * @see conditions.DLTFilter#getApplicationID_APID()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_ApplicationID_APID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getContextID_CTID <em>Context ID CTID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context ID CTID</em>'.
	 * @see conditions.DLTFilter#getContextID_CTID()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_ContextID_CTID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageType_MSTP <em>Message Type MSTP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Type MSTP</em>'.
	 * @see conditions.DLTFilter#getMessageType_MSTP()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageType_MSTP();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageLogInfo_MSLI <em>Message Log Info MSLI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Log Info MSLI</em>'.
	 * @see conditions.DLTFilter#getMessageLogInfo_MSLI()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageLogInfo_MSLI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageTraceInfo_MSTI <em>Message Trace Info MSTI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Trace Info MSTI</em>'.
	 * @see conditions.DLTFilter#getMessageTraceInfo_MSTI()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageTraceInfo_MSTI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageBusInfo_MSBI <em>Message Bus Info MSBI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Bus Info MSBI</em>'.
	 * @see conditions.DLTFilter#getMessageBusInfo_MSBI()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageBusInfo_MSBI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageControlInfo_MSCI <em>Message Control Info MSCI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Control Info MSCI</em>'.
	 * @see conditions.DLTFilter#getMessageControlInfo_MSCI()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageControlInfo_MSCI();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageLogInfo_MSLITmplParam <em>Message Log Info MSLI Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Log Info MSLI Tmpl Param</em>'.
	 * @see conditions.DLTFilter#getMessageLogInfo_MSLITmplParam()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageLogInfo_MSLITmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam <em>Message Trace Info MSTI Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Trace Info MSTI Tmpl Param</em>'.
	 * @see conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageTraceInfo_MSTITmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageBusInfo_MSBITmplParam <em>Message Bus Info MSBI Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Bus Info MSBI Tmpl Param</em>'.
	 * @see conditions.DLTFilter#getMessageBusInfo_MSBITmplParam()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageBusInfo_MSBITmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageControlInfo_MSCITmplParam <em>Message Control Info MSCI Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Control Info MSCI Tmpl Param</em>'.
	 * @see conditions.DLTFilter#getMessageControlInfo_MSCITmplParam()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageControlInfo_MSCITmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.DLTFilter#getMessageType_MSTPTmplParam <em>Message Type MSTP Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Type MSTP Tmpl Param</em>'.
	 * @see conditions.DLTFilter#getMessageType_MSTPTmplParam()
	 * @see #getDLTFilter()
	 * @generated
	 */
	EAttribute getDLTFilter_MessageType_MSTPTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.DLTFilter#isValid_hasValidMessageBusInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Bus Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Bus Info</em>' operation.
	 * @see conditions.DLTFilter#isValid_hasValidMessageBusInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDLTFilter__IsValid_hasValidMessageBusInfo__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.DLTFilter#isValid_hasValidMessageControlInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Control Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Control Info</em>' operation.
	 * @see conditions.DLTFilter#isValid_hasValidMessageControlInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDLTFilter__IsValid_hasValidMessageControlInfo__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.DLTFilter#isValid_hasValidMessageLogInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Log Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Log Info</em>' operation.
	 * @see conditions.DLTFilter#isValid_hasValidMessageLogInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDLTFilter__IsValid_hasValidMessageLogInfo__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.DLTFilter#isValid_hasValidMessageTraceInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Trace Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Trace Info</em>' operation.
	 * @see conditions.DLTFilter#isValid_hasValidMessageTraceInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDLTFilter__IsValid_hasValidMessageTraceInfo__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.DLTFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Type</em>' operation.
	 * @see conditions.DLTFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getDLTFilter__IsValid_hasValidMessageType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.UDPNMFilter <em>UDPNM Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UDPNM Filter</em>'.
	 * @see conditions.UDPNMFilter
	 * @generated
	 */
	EClass getUDPNMFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UDPNMFilter#getSourceNodeIdentifier <em>Source Node Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Node Identifier</em>'.
	 * @see conditions.UDPNMFilter#getSourceNodeIdentifier()
	 * @see #getUDPNMFilter()
	 * @generated
	 */
	EAttribute getUDPNMFilter_SourceNodeIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UDPNMFilter#getControlBitVector <em>Control Bit Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Control Bit Vector</em>'.
	 * @see conditions.UDPNMFilter#getControlBitVector()
	 * @see #getUDPNMFilter()
	 * @generated
	 */
	EAttribute getUDPNMFilter_ControlBitVector();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UDPNMFilter#getPwfStatus <em>Pwf Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pwf Status</em>'.
	 * @see conditions.UDPNMFilter#getPwfStatus()
	 * @see #getUDPNMFilter()
	 * @generated
	 */
	EAttribute getUDPNMFilter_PwfStatus();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UDPNMFilter#getTeilnetzStatus <em>Teilnetz Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Teilnetz Status</em>'.
	 * @see conditions.UDPNMFilter#getTeilnetzStatus()
	 * @see #getUDPNMFilter()
	 * @generated
	 */
	EAttribute getUDPNMFilter_TeilnetzStatus();

	/**
	 * Returns the meta object for the '{@link conditions.UDPNMFilter#isValid_hasValidSourceNodeIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Node Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Source Node Identifier</em>' operation.
	 * @see conditions.UDPNMFilter#isValid_hasValidSourceNodeIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPNMFilter__IsValid_hasValidSourceNodeIdentifier__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UDPNMFilter#isValid_hasValidControlBitVector(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Control Bit Vector</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Control Bit Vector</em>' operation.
	 * @see conditions.UDPNMFilter#isValid_hasValidControlBitVector(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPNMFilter__IsValid_hasValidControlBitVector__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UDPNMFilter#isValid_hasValidPwfStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Pwf Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Pwf Status</em>' operation.
	 * @see conditions.UDPNMFilter#isValid_hasValidPwfStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPNMFilter__IsValid_hasValidPwfStatus__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UDPNMFilter#isValid_hasValidTeilnetzStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Teilnetz Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Teilnetz Status</em>' operation.
	 * @see conditions.UDPNMFilter#isValid_hasValidTeilnetzStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPNMFilter__IsValid_hasValidTeilnetzStatus__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.CANMessage <em>CAN Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CAN Message</em>'.
	 * @see conditions.CANMessage
	 * @generated
	 */
	EClass getCANMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.CANMessage#getCanFilter <em>Can Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Can Filter</em>'.
	 * @see conditions.CANMessage#getCanFilter()
	 * @see #getCANMessage()
	 * @generated
	 */
	EReference getCANMessage_CanFilter();

	/**
	 * Returns the meta object for class '{@link conditions.LINMessage <em>LIN Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LIN Message</em>'.
	 * @see conditions.LINMessage
	 * @generated
	 */
	EClass getLINMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.LINMessage#getLinFilter <em>Lin Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lin Filter</em>'.
	 * @see conditions.LINMessage#getLinFilter()
	 * @see #getLINMessage()
	 * @generated
	 */
	EReference getLINMessage_LinFilter();

	/**
	 * Returns the meta object for class '{@link conditions.FlexRayMessage <em>Flex Ray Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Flex Ray Message</em>'.
	 * @see conditions.FlexRayMessage
	 * @generated
	 */
	EClass getFlexRayMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Flex Ray Filter</em>'.
	 * @see conditions.FlexRayMessage#getFlexRayFilter()
	 * @see #getFlexRayMessage()
	 * @generated
	 */
	EReference getFlexRayMessage_FlexRayFilter();

	/**
	 * Returns the meta object for class '{@link conditions.DLTMessage <em>DLT Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DLT Message</em>'.
	 * @see conditions.DLTMessage
	 * @generated
	 */
	EClass getDLTMessage();

	/**
	 * Returns the meta object for class '{@link conditions.UDPMessage <em>UDP Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UDP Message</em>'.
	 * @see conditions.UDPMessage
	 * @generated
	 */
	EClass getUDPMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Udp Filter</em>'.
	 * @see conditions.UDPMessage#getUdpFilter()
	 * @see #getUDPMessage()
	 * @generated
	 */
	EReference getUDPMessage_UdpFilter();

	/**
	 * Returns the meta object for class '{@link conditions.TCPMessage <em>TCP Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TCP Message</em>'.
	 * @see conditions.TCPMessage
	 * @generated
	 */
	EClass getTCPMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.TCPMessage#getTcpFilter <em>Tcp Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tcp Filter</em>'.
	 * @see conditions.TCPMessage#getTcpFilter()
	 * @see #getTCPMessage()
	 * @generated
	 */
	EReference getTCPMessage_TcpFilter();

	/**
	 * Returns the meta object for class '{@link conditions.UDPNMMessage <em>UDPNM Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UDPNM Message</em>'.
	 * @see conditions.UDPNMMessage
	 * @generated
	 */
	EClass getUDPNMMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.UDPNMMessage#getUdpnmFilter <em>Udpnm Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Udpnm Filter</em>'.
	 * @see conditions.UDPNMMessage#getUdpnmFilter()
	 * @see #getUDPNMMessage()
	 * @generated
	 */
	EReference getUDPNMMessage_UdpnmFilter();

	/**
	 * Returns the meta object for the '{@link conditions.UDPNMMessage#isValid_hasValidUDPFilter(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid UDP Filter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid UDP Filter</em>' operation.
	 * @see conditions.UDPNMMessage#isValid_hasValidUDPFilter(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUDPNMMessage__IsValid_hasValidUDPFilter__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.VerboseDLTMessage <em>Verbose DLT Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Verbose DLT Message</em>'.
	 * @see conditions.VerboseDLTMessage
	 * @generated
	 */
	EClass getVerboseDLTMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.VerboseDLTMessage#getDltFilter <em>Dlt Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dlt Filter</em>'.
	 * @see conditions.VerboseDLTMessage#getDltFilter()
	 * @see #getVerboseDLTMessage()
	 * @generated
	 */
	EReference getVerboseDLTMessage_DltFilter();

	/**
	 * Returns the meta object for class '{@link conditions.UniversalPayloadWithLegacyExtractStrategy <em>Universal Payload With Legacy Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Universal Payload With Legacy Extract Strategy</em>'.
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy
	 * @generated
	 */
	EClass getUniversalPayloadWithLegacyExtractStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit <em>Start Bit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Bit</em>'.
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit()
	 * @see #getUniversalPayloadWithLegacyExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadWithLegacyExtractStrategy_StartBit();

	/**
	 * Returns the meta object for the attribute '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#getLength()
	 * @see #getUniversalPayloadWithLegacyExtractStrategy()
	 * @generated
	 */
	EAttribute getUniversalPayloadWithLegacyExtractStrategy_Length();

	/**
	 * Returns the meta object for the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Startbit</em>' operation.
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUniversalPayloadWithLegacyExtractStrategy__IsValid_hasValidStartbit__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidDataLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Data Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Data Length</em>' operation.
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidDataLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getUniversalPayloadWithLegacyExtractStrategy__IsValid_hasValidDataLength__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractBusMessage <em>Abstract Bus Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Bus Message</em>'.
	 * @see conditions.AbstractBusMessage
	 * @generated
	 */
	EClass getAbstractBusMessage();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bus Id</em>'.
	 * @see conditions.AbstractBusMessage#getBusId()
	 * @see #getAbstractBusMessage()
	 * @generated
	 */
	EAttribute getAbstractBusMessage_BusId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bus Id Tmpl Param</em>'.
	 * @see conditions.AbstractBusMessage#getBusIdTmplParam()
	 * @see #getAbstractBusMessage()
	 * @generated
	 */
	EAttribute getAbstractBusMessage_BusIdTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bus Id Range</em>'.
	 * @see conditions.AbstractBusMessage#getBusIdRange()
	 * @see #getAbstractBusMessage()
	 * @generated
	 */
	EAttribute getAbstractBusMessage_BusIdRange();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.AbstractBusMessage#getFilters <em>Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filters</em>'.
	 * @see conditions.AbstractBusMessage#getFilters()
	 * @see #getAbstractBusMessage()
	 * @generated
	 */
	EReference getAbstractBusMessage_Filters();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractBusMessage#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Bus Id</em>' operation.
	 * @see conditions.AbstractBusMessage#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractBusMessage__IsValid_hasValidBusId__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractBusMessage#isValid_isOsiLayerConform(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid is Osi Layer Conform</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid is Osi Layer Conform</em>' operation.
	 * @see conditions.AbstractBusMessage#isValid_isOsiLayerConform(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractBusMessage__IsValid_isOsiLayerConform__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.PluginFilter <em>Plugin Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin Filter</em>'.
	 * @see conditions.PluginFilter
	 * @generated
	 */
	EClass getPluginFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginFilter#getPluginName <em>Plugin Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plugin Name</em>'.
	 * @see conditions.PluginFilter#getPluginName()
	 * @see #getPluginFilter()
	 * @generated
	 */
	EAttribute getPluginFilter_PluginName();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginFilter#getPluginVersion <em>Plugin Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Plugin Version</em>'.
	 * @see conditions.PluginFilter#getPluginVersion()
	 * @see #getPluginFilter()
	 * @generated
	 */
	EAttribute getPluginFilter_PluginVersion();

	/**
	 * Returns the meta object for class '{@link conditions.PluginMessage <em>Plugin Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin Message</em>'.
	 * @see conditions.PluginMessage
	 * @generated
	 */
	EClass getPluginMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.PluginMessage#getPluginFilter <em>Plugin Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Plugin Filter</em>'.
	 * @see conditions.PluginMessage#getPluginFilter()
	 * @see #getPluginMessage()
	 * @generated
	 */
	EReference getPluginMessage_PluginFilter();

	/**
	 * Returns the meta object for class '{@link conditions.PluginSignal <em>Plugin Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin Signal</em>'.
	 * @see conditions.PluginSignal
	 * @generated
	 */
	EClass getPluginSignal();

	/**
	 * Returns the meta object for class '{@link conditions.PluginStateExtractStrategy <em>Plugin State Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin State Extract Strategy</em>'.
	 * @see conditions.PluginStateExtractStrategy
	 * @generated
	 */
	EClass getPluginStateExtractStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginStateExtractStrategy#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>States</em>'.
	 * @see conditions.PluginStateExtractStrategy#getStates()
	 * @see #getPluginStateExtractStrategy()
	 * @generated
	 */
	EAttribute getPluginStateExtractStrategy_States();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginStateExtractStrategy#getStatesTmplParam <em>States Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>States Tmpl Param</em>'.
	 * @see conditions.PluginStateExtractStrategy#getStatesTmplParam()
	 * @see #getPluginStateExtractStrategy()
	 * @generated
	 */
	EAttribute getPluginStateExtractStrategy_StatesTmplParam();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginStateExtractStrategy#getStatesActive <em>States Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>States Active</em>'.
	 * @see conditions.PluginStateExtractStrategy#getStatesActive()
	 * @see #getPluginStateExtractStrategy()
	 * @generated
	 */
	EAttribute getPluginStateExtractStrategy_StatesActive();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginStateExtractStrategy#getStatesActiveTmplParam <em>States Active Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>States Active Tmpl Param</em>'.
	 * @see conditions.PluginStateExtractStrategy#getStatesActiveTmplParam()
	 * @see #getPluginStateExtractStrategy()
	 * @generated
	 */
	EAttribute getPluginStateExtractStrategy_StatesActiveTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.PluginStateExtractStrategy#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid States Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid States Active</em>' operation.
	 * @see conditions.PluginStateExtractStrategy#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPluginStateExtractStrategy__IsValid_hasValidStatesActive__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.PluginStateExtractStrategy#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid State</em>' operation.
	 * @see conditions.PluginStateExtractStrategy#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPluginStateExtractStrategy__IsValid_hasValidState__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.PluginResultExtractStrategy <em>Plugin Result Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin Result Extract Strategy</em>'.
	 * @see conditions.PluginResultExtractStrategy
	 * @generated
	 */
	EClass getPluginResultExtractStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result Range</em>'.
	 * @see conditions.PluginResultExtractStrategy#getResultRange()
	 * @see #getPluginResultExtractStrategy()
	 * @generated
	 */
	EAttribute getPluginResultExtractStrategy_ResultRange();

	/**
	 * Returns the meta object for the '{@link conditions.PluginResultExtractStrategy#isValid_hasValidResultRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Result Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Result Range</em>' operation.
	 * @see conditions.PluginResultExtractStrategy#isValid_hasValidResultRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPluginResultExtractStrategy__IsValid_hasValidResultRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.EmptyDecodeStrategy <em>Empty Decode Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Empty Decode Strategy</em>'.
	 * @see conditions.EmptyDecodeStrategy
	 * @generated
	 */
	EClass getEmptyDecodeStrategy();

	/**
	 * Returns the meta object for class '{@link conditions.PluginCheckExpression <em>Plugin Check Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Plugin Check Expression</em>'.
	 * @see conditions.PluginCheckExpression
	 * @generated
	 */
	EClass getPluginCheckExpression();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginCheckExpression#getEvaluationBehaviour <em>Evaluation Behaviour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluation Behaviour</em>'.
	 * @see conditions.PluginCheckExpression#getEvaluationBehaviour()
	 * @see #getPluginCheckExpression()
	 * @generated
	 */
	EAttribute getPluginCheckExpression_EvaluationBehaviour();

	/**
	 * Returns the meta object for the reference '{@link conditions.PluginCheckExpression#getSignalToCheck <em>Signal To Check</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Signal To Check</em>'.
	 * @see conditions.PluginCheckExpression#getSignalToCheck()
	 * @see #getPluginCheckExpression()
	 * @generated
	 */
	EReference getPluginCheckExpression_SignalToCheck();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam <em>Evaluation Behaviour Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Evaluation Behaviour Tmpl Param</em>'.
	 * @see conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam()
	 * @see #getPluginCheckExpression()
	 * @generated
	 */
	EAttribute getPluginCheckExpression_EvaluationBehaviourTmplParam();

	/**
	 * Returns the meta object for class '{@link conditions.BaseClassWithID <em>Base Class With ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Class With ID</em>'.
	 * @see conditions.BaseClassWithID
	 * @generated
	 */
	EClass getBaseClassWithID();

	/**
	 * Returns the meta object for the attribute '{@link conditions.BaseClassWithID#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see conditions.BaseClassWithID#getId()
	 * @see #getBaseClassWithID()
	 * @generated
	 */
	EAttribute getBaseClassWithID_Id();

	/**
	 * Returns the meta object for class '{@link conditions.HeaderSignal <em>Header Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Header Signal</em>'.
	 * @see conditions.HeaderSignal
	 * @generated
	 */
	EClass getHeaderSignal();

	/**
	 * Returns the meta object for the attribute '{@link conditions.HeaderSignal#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute</em>'.
	 * @see conditions.HeaderSignal#getAttribute()
	 * @see #getHeaderSignal()
	 * @generated
	 */
	EAttribute getHeaderSignal_Attribute();

	/**
	 * Returns the meta object for the attribute '{@link conditions.HeaderSignal#getAttributeTmplParam <em>Attribute Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute Tmpl Param</em>'.
	 * @see conditions.HeaderSignal#getAttributeTmplParam()
	 * @see #getHeaderSignal()
	 * @generated
	 */
	EAttribute getHeaderSignal_AttributeTmplParam();

	/**
	 * Returns the meta object for the '{@link conditions.HeaderSignal#isValid_hasValidAttribute(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Attribute</em>' operation.
	 * @see conditions.HeaderSignal#isValid_hasValidAttribute(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getHeaderSignal__IsValid_hasValidAttribute__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVariable Reader Writer</em>'.
	 * @see conditions.IVariableReaderWriter
	 * @generated
	 */
	EClass getIVariableReaderWriter();

	/**
	 * Returns the meta object for the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Read Variables</em>' operation.
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	EOperation getIVariableReaderWriter__GetReadVariables();

	/**
	 * Returns the meta object for the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Write Variables</em>' operation.
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	EOperation getIVariableReaderWriter__GetWriteVariables();

	/**
	 * Returns the meta object for class '{@link conditions.IStateTransitionReference <em>IState Transition Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IState Transition Reference</em>'.
	 * @see conditions.IStateTransitionReference
	 * @generated
	 */
	EClass getIStateTransitionReference();

	/**
	 * Returns the meta object for the '{@link conditions.IStateTransitionReference#getStateDependencies() <em>Get State Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get State Dependencies</em>' operation.
	 * @see conditions.IStateTransitionReference#getStateDependencies()
	 * @generated
	 */
	EOperation getIStateTransitionReference__GetStateDependencies();

	/**
	 * Returns the meta object for the '{@link conditions.IStateTransitionReference#getTransitionDependencies() <em>Get Transition Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Transition Dependencies</em>' operation.
	 * @see conditions.IStateTransitionReference#getTransitionDependencies()
	 * @generated
	 */
	EOperation getIStateTransitionReference__GetTransitionDependencies();

	/**
	 * Returns the meta object for class '{@link conditions.TPFilter <em>TP Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TP Filter</em>'.
	 * @see conditions.TPFilter
	 * @generated
	 */
	EClass getTPFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TPFilter#getSourcePort <em>Source Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Port</em>'.
	 * @see conditions.TPFilter#getSourcePort()
	 * @see #getTPFilter()
	 * @generated
	 */
	EAttribute getTPFilter_SourcePort();

	/**
	 * Returns the meta object for the attribute '{@link conditions.TPFilter#getDestPort <em>Dest Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dest Port</em>'.
	 * @see conditions.TPFilter#getDestPort()
	 * @see #getTPFilter()
	 * @generated
	 */
	EAttribute getTPFilter_DestPort();

	/**
	 * Returns the meta object for class '{@link conditions.BaseClassWithSourceReference <em>Base Class With Source Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Class With Source Reference</em>'.
	 * @see conditions.BaseClassWithSourceReference
	 * @generated
	 */
	EClass getBaseClassWithSourceReference();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.BaseClassWithSourceReference#getSourceReference <em>Source Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Reference</em>'.
	 * @see conditions.BaseClassWithSourceReference#getSourceReference()
	 * @see #getBaseClassWithSourceReference()
	 * @generated
	 */
	EReference getBaseClassWithSourceReference_SourceReference();

	/**
	 * Returns the meta object for class '{@link conditions.SourceReference <em>Source Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Source Reference</em>'.
	 * @see conditions.SourceReference
	 * @generated
	 */
	EClass getSourceReference();

	/**
	 * Returns the meta object for the attribute '{@link conditions.SourceReference#getSerializedReference <em>Serialized Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Serialized Reference</em>'.
	 * @see conditions.SourceReference#getSerializedReference()
	 * @see #getSourceReference()
	 * @generated
	 */
	EAttribute getSourceReference_SerializedReference();

	/**
	 * Returns the meta object for class '{@link conditions.EthernetMessage <em>Ethernet Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ethernet Message</em>'.
	 * @see conditions.EthernetMessage
	 * @generated
	 */
	EClass getEthernetMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.EthernetMessage#getEthernetFilter <em>Ethernet Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ethernet Filter</em>'.
	 * @see conditions.EthernetMessage#getEthernetFilter()
	 * @see #getEthernetMessage()
	 * @generated
	 */
	EReference getEthernetMessage_EthernetFilter();

	/**
	 * Returns the meta object for class '{@link conditions.IPv4Message <em>IPv4 Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IPv4 Message</em>'.
	 * @see conditions.IPv4Message
	 * @generated
	 */
	EClass getIPv4Message();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.IPv4Message#getIPv4Filter <em>IPv4 Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>IPv4 Filter</em>'.
	 * @see conditions.IPv4Message#getIPv4Filter()
	 * @see #getIPv4Message()
	 * @generated
	 */
	EReference getIPv4Message_IPv4Filter();

	/**
	 * Returns the meta object for class '{@link conditions.NonVerboseDLTMessage <em>Non Verbose DLT Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Verbose DLT Message</em>'.
	 * @see conditions.NonVerboseDLTMessage
	 * @generated
	 */
	EClass getNonVerboseDLTMessage();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.NonVerboseDLTMessage#getNonVerboseDltFilter <em>Non Verbose Dlt Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Non Verbose Dlt Filter</em>'.
	 * @see conditions.NonVerboseDLTMessage#getNonVerboseDltFilter()
	 * @see #getNonVerboseDLTMessage()
	 * @generated
	 */
	EReference getNonVerboseDLTMessage_NonVerboseDltFilter();

	/**
	 * Returns the meta object for class '{@link conditions.StringDecodeStrategy <em>String Decode Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Decode Strategy</em>'.
	 * @see conditions.StringDecodeStrategy
	 * @generated
	 */
	EClass getStringDecodeStrategy();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StringDecodeStrategy#getStringTermination <em>String Termination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Termination</em>'.
	 * @see conditions.StringDecodeStrategy#getStringTermination()
	 * @see #getStringDecodeStrategy()
	 * @generated
	 */
	EAttribute getStringDecodeStrategy_StringTermination();

	/**
	 * Returns the meta object for the attribute '{@link conditions.StringDecodeStrategy#getStringTerminationTmplParam <em>String Termination Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Termination Tmpl Param</em>'.
	 * @see conditions.StringDecodeStrategy#getStringTerminationTmplParam()
	 * @see #getStringDecodeStrategy()
	 * @generated
	 */
	EAttribute getStringDecodeStrategy_StringTerminationTmplParam();

	/**
	 * Returns the meta object for class '{@link conditions.NonVerboseDLTFilter <em>Non Verbose DLT Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Verbose DLT Filter</em>'.
	 * @see conditions.NonVerboseDLTFilter
	 * @generated
	 */
	EClass getNonVerboseDLTFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Id</em>'.
	 * @see conditions.NonVerboseDLTFilter#getMessageId()
	 * @see #getNonVerboseDLTFilter()
	 * @generated
	 */
	EAttribute getNonVerboseDLTFilter_MessageId();

	/**
	 * Returns the meta object for the attribute '{@link conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Id Range</em>'.
	 * @see conditions.NonVerboseDLTFilter#getMessageIdRange()
	 * @see #getNonVerboseDLTFilter()
	 * @generated
	 */
	EAttribute getNonVerboseDLTFilter_MessageIdRange();

	/**
	 * Returns the meta object for the '{@link conditions.NonVerboseDLTFilter#isValid_hasValidMessageIdOrMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Or Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Message Id Or Message Id Range</em>' operation.
	 * @see conditions.NonVerboseDLTFilter#isValid_hasValidMessageIdOrMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getNonVerboseDLTFilter__IsValid_hasValidMessageIdOrMessageIdRange__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.VerboseDLTExtractStrategy <em>Verbose DLT Extract Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Verbose DLT Extract Strategy</em>'.
	 * @see conditions.VerboseDLTExtractStrategy
	 * @generated
	 */
	EClass getVerboseDLTExtractStrategy();

	/**
	 * Returns the meta object for class '{@link conditions.RegexOperation <em>Regex Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regex Operation</em>'.
	 * @see conditions.RegexOperation
	 * @generated
	 */
	EClass getRegexOperation();

	/**
	 * Returns the meta object for the attribute '{@link conditions.RegexOperation#getRegex <em>Regex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Regex</em>'.
	 * @see conditions.RegexOperation#getRegex()
	 * @see #getRegexOperation()
	 * @generated
	 */
	EAttribute getRegexOperation_Regex();

	/**
	 * Returns the meta object for the containment reference '{@link conditions.RegexOperation#getDynamicRegex <em>Dynamic Regex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Dynamic Regex</em>'.
	 * @see conditions.RegexOperation#getDynamicRegex()
	 * @see #getRegexOperation()
	 * @generated
	 */
	EReference getRegexOperation_DynamicRegex();

	/**
	 * Returns the meta object for the '{@link conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Regex</em>' operation.
	 * @see conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getRegexOperation__IsValid_hasValidRegex__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.PayloadFilter <em>Payload Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Payload Filter</em>'.
	 * @see conditions.PayloadFilter
	 * @generated
	 */
	EClass getPayloadFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PayloadFilter#getIndex <em>Index</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Index</em>'.
	 * @see conditions.PayloadFilter#getIndex()
	 * @see #getPayloadFilter()
	 * @generated
	 */
	EAttribute getPayloadFilter_Index();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PayloadFilter#getMask <em>Mask</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mask</em>'.
	 * @see conditions.PayloadFilter#getMask()
	 * @see #getPayloadFilter()
	 * @generated
	 */
	EAttribute getPayloadFilter_Mask();

	/**
	 * Returns the meta object for the attribute '{@link conditions.PayloadFilter#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see conditions.PayloadFilter#getValue()
	 * @see #getPayloadFilter()
	 * @generated
	 */
	EAttribute getPayloadFilter_Value();

	/**
	 * Returns the meta object for the '{@link conditions.PayloadFilter#isValid_hasValidIndex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Index</em>' operation.
	 * @see conditions.PayloadFilter#isValid_hasValidIndex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPayloadFilter__IsValid_hasValidIndex__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.PayloadFilter#isValid_hasValidMask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Mask</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Mask</em>' operation.
	 * @see conditions.PayloadFilter#isValid_hasValidMask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPayloadFilter__IsValid_hasValidMask__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.PayloadFilter#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Value</em>' operation.
	 * @see conditions.PayloadFilter#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getPayloadFilter__IsValid_hasValidValue__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.VerboseDLTPayloadFilter <em>Verbose DLT Payload Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Verbose DLT Payload Filter</em>'.
	 * @see conditions.VerboseDLTPayloadFilter
	 * @generated
	 */
	EClass getVerboseDLTPayloadFilter();

	/**
	 * Returns the meta object for the attribute '{@link conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Regex</em>'.
	 * @see conditions.VerboseDLTPayloadFilter#getRegex()
	 * @see #getVerboseDLTPayloadFilter()
	 * @generated
	 */
	EAttribute getVerboseDLTPayloadFilter_Regex();

	/**
	 * Returns the meta object for the '{@link conditions.VerboseDLTPayloadFilter#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Regex</em>' operation.
	 * @see conditions.VerboseDLTPayloadFilter#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getVerboseDLTPayloadFilter__IsValid_hasValidRegex__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.AbstractVariable <em>Abstract Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Variable</em>'.
	 * @see conditions.AbstractVariable
	 * @generated
	 */
	EClass getAbstractVariable();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see conditions.AbstractVariable#getName()
	 * @see #getAbstractVariable()
	 * @generated
	 */
	EAttribute getAbstractVariable_Name();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractVariable#getDisplayName <em>Display Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Name</em>'.
	 * @see conditions.AbstractVariable#getDisplayName()
	 * @see #getAbstractVariable()
	 * @generated
	 */
	EAttribute getAbstractVariable_DisplayName();

	/**
	 * Returns the meta object for the attribute '{@link conditions.AbstractVariable#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see conditions.AbstractVariable#getDescription()
	 * @see #getAbstractVariable()
	 * @generated
	 */
	EAttribute getAbstractVariable_Description();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractVariable#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Name</em>' operation.
	 * @see conditions.AbstractVariable#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractVariable__IsValid_hasValidName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.AbstractVariable#isValid_hasValidDisplayName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Display Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Display Name</em>' operation.
	 * @see conditions.AbstractVariable#isValid_hasValidDisplayName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractVariable__IsValid_hasValidDisplayName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link conditions.VariableStructure <em>Variable Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Structure</em>'.
	 * @see conditions.VariableStructure
	 * @generated
	 */
	EClass getVariableStructure();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.VariableStructure#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see conditions.VariableStructure#getVariables()
	 * @see #getVariableStructure()
	 * @generated
	 */
	EReference getVariableStructure_Variables();

	/**
	 * Returns the meta object for class '{@link conditions.ComputedVariable <em>Computed Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computed Variable</em>'.
	 * @see conditions.ComputedVariable
	 * @generated
	 */
	EClass getComputedVariable();

	/**
	 * Returns the meta object for the containment reference list '{@link conditions.ComputedVariable#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see conditions.ComputedVariable#getOperands()
	 * @see #getComputedVariable()
	 * @generated
	 */
	EReference getComputedVariable_Operands();

	/**
	 * Returns the meta object for the attribute '{@link conditions.ComputedVariable#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see conditions.ComputedVariable#getExpression()
	 * @see #getComputedVariable()
	 * @generated
	 */
	EAttribute getComputedVariable_Expression();

	/**
	 * Returns the meta object for the '{@link conditions.ComputedVariable#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Expression</em>' operation.
	 * @see conditions.ComputedVariable#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getComputedVariable__IsValid_hasValidExpression__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link conditions.ComputedVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Operands</em>' operation.
	 * @see conditions.ComputedVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getComputedVariable__IsValid_hasValidOperands__DiagnosticChain_Map();

	/**
	 * Returns the meta object for enum '{@link conditions.CheckType <em>Check Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Check Type</em>'.
	 * @see conditions.CheckType
	 * @generated
	 */
	EEnum getCheckType();

	/**
	 * Returns the meta object for enum '{@link conditions.Comparator <em>Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparator</em>'.
	 * @see conditions.Comparator
	 * @generated
	 */
	EEnum getComparator();

	/**
	 * Returns the meta object for enum '{@link conditions.LogicalOperatorType <em>Logical Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator Type</em>'.
	 * @see conditions.LogicalOperatorType
	 * @generated
	 */
	EEnum getLogicalOperatorType();

	/**
	 * Returns the meta object for enum '{@link conditions.SignalDataType <em>Signal Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signal Data Type</em>'.
	 * @see conditions.SignalDataType
	 * @generated
	 */
	EEnum getSignalDataType();

	/**
	 * Returns the meta object for enum '{@link conditions.FlexChannelType <em>Flex Channel Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Flex Channel Type</em>'.
	 * @see conditions.FlexChannelType
	 * @generated
	 */
	EEnum getFlexChannelType();

	/**
	 * Returns the meta object for enum '{@link conditions.ByteOrderType <em>Byte Order Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Byte Order Type</em>'.
	 * @see conditions.ByteOrderType
	 * @generated
	 */
	EEnum getByteOrderType();

	/**
	 * Returns the meta object for enum '{@link conditions.FormatDataType <em>Format Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Format Data Type</em>'.
	 * @see conditions.FormatDataType
	 * @generated
	 */
	EEnum getFormatDataType();

	/**
	 * Returns the meta object for enum '{@link conditions.FlexRayHeaderFlagSelection <em>Flex Ray Header Flag Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Flex Ray Header Flag Selection</em>'.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @generated
	 */
	EEnum getFlexRayHeaderFlagSelection();

	/**
	 * Returns the meta object for enum '{@link conditions.BooleanOrTemplatePlaceholderEnum <em>Boolean Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Boolean Or Template Placeholder Enum</em>'.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getBooleanOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.ProtocolTypeOrTemplatePlaceholderEnum <em>Protocol Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Protocol Type Or Template Placeholder Enum</em>'.
	 * @see conditions.ProtocolTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getProtocolTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.EthernetProtocolTypeSelection <em>Ethernet Protocol Type Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ethernet Protocol Type Selection</em>'.
	 * @see conditions.EthernetProtocolTypeSelection
	 * @generated
	 */
	EEnum getEthernetProtocolTypeSelection();

	/**
	 * Returns the meta object for enum '{@link conditions.RxTxFlagTypeOrTemplatePlaceholderEnum <em>Rx Tx Flag Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Rx Tx Flag Type Or Template Placeholder Enum</em>'.
	 * @see conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getRxTxFlagTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.ObserverValueType <em>Observer Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Observer Value Type</em>'.
	 * @see conditions.ObserverValueType
	 * @generated
	 */
	EEnum getObserverValueType();

	/**
	 * Returns the meta object for enum '{@link conditions.PluginStateValueTypeOrTemplatePlaceholderEnum <em>Plugin State Value Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Plugin State Value Type Or Template Placeholder Enum</em>'.
	 * @see conditions.PluginStateValueTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getPluginStateValueTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.AnalysisTypeOrTemplatePlaceholderEnum <em>Analysis Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Analysis Type Or Template Placeholder Enum</em>'.
	 * @see conditions.AnalysisTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getAnalysisTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum <em>Some IP Message Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IP Message Type Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPMessageTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum <em>Some IP Method Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IP Method Type Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPMethodTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum <em>Some IP Return Code Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IP Return Code Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPReturnCodeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum <em>Some IPSD Flags Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IPSD Flags Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPSDFlagsOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPSDTypeOrTemplatePlaceholderEnum <em>Some IPSD Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IPSD Type Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPSDTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum <em>Stream Analysis Flags Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Stream Analysis Flags Or Template Placeholder Enum</em>'.
	 * @see conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getStreamAnalysisFlagsOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum <em>Some IPSD Entry Flags Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Some IPSD Entry Flags Or Template Placeholder Enum</em>'.
	 * @see conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.CanExtIdentifierOrTemplatePlaceholderEnum <em>Can Ext Identifier Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Can Ext Identifier Or Template Placeholder Enum</em>'.
	 * @see conditions.CanExtIdentifierOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getCanExtIdentifierOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.DataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Type</em>'.
	 * @see conditions.DataType
	 * @generated
	 */
	EEnum getDataType();

	/**
	 * Returns the meta object for enum '{@link conditions.DLT_MessageType <em>DLT Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DLT Message Type</em>'.
	 * @see conditions.DLT_MessageType
	 * @generated
	 */
	EEnum getDLT_MessageType();

	/**
	 * Returns the meta object for enum '{@link conditions.DLT_MessageTraceInfo <em>DLT Message Trace Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DLT Message Trace Info</em>'.
	 * @see conditions.DLT_MessageTraceInfo
	 * @generated
	 */
	EEnum getDLT_MessageTraceInfo();

	/**
	 * Returns the meta object for enum '{@link conditions.DLT_MessageLogInfo <em>DLT Message Log Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DLT Message Log Info</em>'.
	 * @see conditions.DLT_MessageLogInfo
	 * @generated
	 */
	EEnum getDLT_MessageLogInfo();

	/**
	 * Returns the meta object for enum '{@link conditions.DLT_MessageControlInfo <em>DLT Message Control Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DLT Message Control Info</em>'.
	 * @see conditions.DLT_MessageControlInfo
	 * @generated
	 */
	EEnum getDLT_MessageControlInfo();

	/**
	 * Returns the meta object for enum '{@link conditions.DLT_MessageBusInfo <em>DLT Message Bus Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>DLT Message Bus Info</em>'.
	 * @see conditions.DLT_MessageBusInfo
	 * @generated
	 */
	EEnum getDLT_MessageBusInfo();

	/**
	 * Returns the meta object for enum '{@link conditions.ForEachExpressionModifierTemplate <em>For Each Expression Modifier Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>For Each Expression Modifier Template</em>'.
	 * @see conditions.ForEachExpressionModifierTemplate
	 * @generated
	 */
	EEnum getForEachExpressionModifierTemplate();

	/**
	 * Returns the meta object for enum '{@link conditions.AUTOSARDataType <em>AUTOSAR Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>AUTOSAR Data Type</em>'.
	 * @see conditions.AUTOSARDataType
	 * @generated
	 */
	EEnum getAUTOSARDataType();

	/**
	 * Returns the meta object for enum '{@link conditions.CANFrameTypeOrTemplatePlaceholderEnum <em>CAN Frame Type Or Template Placeholder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>CAN Frame Type Or Template Placeholder Enum</em>'.
	 * @see conditions.CANFrameTypeOrTemplatePlaceholderEnum
	 * @generated
	 */
	EEnum getCANFrameTypeOrTemplatePlaceholderEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.EvaluationBehaviourOrTemplateDefinedEnum <em>Evaluation Behaviour Or Template Defined Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Evaluation Behaviour Or Template Defined Enum</em>'.
	 * @see conditions.EvaluationBehaviourOrTemplateDefinedEnum
	 * @generated
	 */
	EEnum getEvaluationBehaviourOrTemplateDefinedEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.SignalVariableLagEnum <em>Signal Variable Lag Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Signal Variable Lag Enum</em>'.
	 * @see conditions.SignalVariableLagEnum
	 * @generated
	 */
	EEnum getSignalVariableLagEnum();

	/**
	 * Returns the meta object for enum '{@link conditions.TTLOrTemplatePlaceHolderEnum <em>TTL Or Template Place Holder Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>TTL Or Template Place Holder Enum</em>'.
	 * @see conditions.TTLOrTemplatePlaceHolderEnum
	 * @generated
	 */
	EEnum getTTLOrTemplatePlaceHolderEnum();

	/**
	 * Returns the meta object for data type '{@link conditions.CheckType <em>Check Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Check Type Object</em>'.
	 * @see conditions.CheckType
	 * @model instanceClass="conditions.CheckType"
	 *        extendedMetaData="name='checkType:Object' baseType='checkType'"
	 * @generated
	 */
	EDataType getCheckTypeObject();

	/**
	 * Returns the meta object for data type '{@link conditions.Comparator <em>Comparator Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Comparator Object</em>'.
	 * @see conditions.Comparator
	 * @model instanceClass="conditions.Comparator"
	 *        extendedMetaData="name='comparator:Object' baseType='comparator'"
	 * @generated
	 */
	EDataType getComparatorObject();

	/**
	 * Returns the meta object for data type '{@link conditions.LogicalOperatorType <em>Logical Operator Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Logical Operator Type Object</em>'.
	 * @see conditions.LogicalOperatorType
	 * @model instanceClass="conditions.LogicalOperatorType"
	 *        extendedMetaData="name='logicalOperatorType:Object' baseType='logicalOperatorType'"
	 * @generated
	 */
	EDataType getLogicalOperatorTypeObject();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Hex Or Int Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Hex Or Int Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getHexOrIntOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Int Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Int Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getIntOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Long Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Long Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getLongOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Double Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Double Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getDoubleOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Double Or Hex Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Double Or Hex Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getDoubleOrHexOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>IP Pattern Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IP Pattern Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getIPPatternOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>MAC Pattern Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>MAC Pattern Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getMACPatternOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Port Pattern Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Port Pattern Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getPortPatternOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Vlan Id Pattern Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Vlan Id Pattern Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getVlanIdPatternOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>IP Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>IP Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getIPOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Port Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Port Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getPortOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Vlan Id Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Vlan Id Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getVlanIdOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>MAC Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>MAC Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getMACOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Bitmask Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Bitmask Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getBitmaskOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Hex Or Int Or Null Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Hex Or Int Or Null Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getHexOrIntOrNullOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Non Zero Double Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Non Zero Double Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getNonZeroDoubleOrTemplatePlaceholder();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Val Var Initial Value Double Or Template Placeholder Or String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Val Var Initial Value Double Or Template Placeholder Or String</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getValVarInitialValueDoubleOrTemplatePlaceholderOrString();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Byte Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Byte Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getByteOrTemplatePlaceholder();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConditionsFactory getConditionsFactory();

} //ConditionsPackage
