/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.TCPFilter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.TCPFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TCPFilterItemProvider extends TPFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TCPFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSequenceNumberPropertyDescriptor(object);
			addAcknowledgementNumberPropertyDescriptor(object);
			addTcpFlagsPropertyDescriptor(object);
			addStreamAnalysisFlagsPropertyDescriptor(object);
			addStreamAnalysisFlagsTmplParamPropertyDescriptor(object);
			addStreamValidPayloadOffsetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Sequence Number feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSequenceNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_sequenceNumber_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_sequenceNumber_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_SequenceNumber(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Acknowledgement Number feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAcknowledgementNumberPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_acknowledgementNumber_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_acknowledgementNumber_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_AcknowledgementNumber(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tcp Flags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTcpFlagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_tcpFlags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_tcpFlags_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_TcpFlags(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stream Analysis Flags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStreamAnalysisFlagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_streamAnalysisFlags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_streamAnalysisFlags_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_StreamAnalysisFlags(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stream Analysis Flags Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStreamAnalysisFlagsTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_streamAnalysisFlagsTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_streamAnalysisFlagsTmplParam_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_StreamAnalysisFlagsTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Stream Valid Payload Offset feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStreamValidPayloadOffsetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TCPFilter_streamValidPayloadOffset_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TCPFilter_streamValidPayloadOffset_feature", "_UI_TCPFilter_type"),
				 ConditionsPackage.eINSTANCE.getTCPFilter_StreamValidPayloadOffset(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns TCPFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TCPFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((TCPFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_TCPFilter_type") :
			getString("_UI_TCPFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TCPFilter.class)) {
			case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
			case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
			case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
			case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
