/**
 */
package conditions.provider;


import conditions.AbstractBusMessage;
import conditions.ConditionsFactory;
import conditions.ConditionsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.AbstractBusMessage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractBusMessageItemProvider extends AbstractMessageItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBusMessageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBusIdPropertyDescriptor(object);
			addBusIdTmplParamPropertyDescriptor(object);
			addBusIdRangePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Bus Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractBusMessage_busId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractBusMessage_busId_feature", "_UI_AbstractBusMessage_type"),
				 ConditionsPackage.eINSTANCE.getAbstractBusMessage_BusId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bus Id Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusIdTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractBusMessage_busIdTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractBusMessage_busIdTmplParam_feature", "_UI_AbstractBusMessage_type"),
				 ConditionsPackage.eINSTANCE.getAbstractBusMessage_BusIdTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bus Id Range feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusIdRangePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractBusMessage_busIdRange_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractBusMessage_busIdRange_feature", "_UI_AbstractBusMessage_type"),
				 ConditionsPackage.eINSTANCE.getAbstractBusMessage_BusIdRange(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AbstractBusMessage)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AbstractBusMessage_type") :
			getString("_UI_AbstractBusMessage_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractBusMessage.class)) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createEthernetFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createUDPFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createTCPFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createIPv4Filter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createSomeIPFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createSomeIPSDFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createCANFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createLINFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createFlexRayFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createDLTFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createUDPNMFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createPluginFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createNonVerboseDLTFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createPayloadFilter()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractBusMessage_Filters(),
				 ConditionsFactory.eINSTANCE.createVerboseDLTPayloadFilter()));
	}

}
