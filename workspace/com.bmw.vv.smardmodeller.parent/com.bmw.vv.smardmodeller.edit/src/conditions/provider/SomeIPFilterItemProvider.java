/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.SomeIPFilter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.SomeIPFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SomeIPFilterItemProvider extends AbstractFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addServiceIdPropertyDescriptor(object);
			addMethodTypePropertyDescriptor(object);
			addMethodTypeTmplParamPropertyDescriptor(object);
			addMethodIdPropertyDescriptor(object);
			addClientIdPropertyDescriptor(object);
			addSessionIdPropertyDescriptor(object);
			addProtocolVersionPropertyDescriptor(object);
			addInterfaceVersionPropertyDescriptor(object);
			addMessageTypePropertyDescriptor(object);
			addMessageTypeTmplParamPropertyDescriptor(object);
			addReturnCodePropertyDescriptor(object);
			addReturnCodeTmplParamPropertyDescriptor(object);
			addSomeIPLengthPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Service Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addServiceIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_serviceId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_serviceId_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_ServiceId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Method Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMethodTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_methodType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_methodType_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_MethodType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Method Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMethodTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_methodTypeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_methodTypeTmplParam_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_MethodTypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Method Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMethodIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_methodId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_methodId_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_MethodId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Client Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClientIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_clientId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_clientId_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_ClientId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Session Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSessionIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_sessionId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_sessionId_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_SessionId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Protocol Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProtocolVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_protocolVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_protocolVersion_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_ProtocolVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Interface Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInterfaceVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_interfaceVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_interfaceVersion_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_InterfaceVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_messageType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_messageType_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_MessageType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_messageTypeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_messageTypeTmplParam_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_MessageTypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Return Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReturnCodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_returnCode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_returnCode_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_ReturnCode(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Return Code Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReturnCodeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_returnCodeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_returnCodeTmplParam_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_ReturnCodeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Some IP Length feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSomeIPLengthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPFilter_someIPLength_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPFilter_someIPLength_feature", "_UI_SomeIPFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPFilter_SomeIPLength(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns SomeIPFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SomeIPFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SomeIPFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_SomeIPFilter_type") :
			getString("_UI_SomeIPFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SomeIPFilter.class)) {
			case ConditionsPackage.SOME_IP_FILTER__SERVICE_ID:
			case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE:
			case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM:
			case ConditionsPackage.SOME_IP_FILTER__METHOD_ID:
			case ConditionsPackage.SOME_IP_FILTER__CLIENT_ID:
			case ConditionsPackage.SOME_IP_FILTER__SESSION_ID:
			case ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION:
			case ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION:
			case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE:
			case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM:
			case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE:
			case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM:
			case ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
