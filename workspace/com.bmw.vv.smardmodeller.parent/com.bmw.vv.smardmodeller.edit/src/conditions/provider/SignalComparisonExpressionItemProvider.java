/**
 */
package conditions.provider;


import conditions.ConditionsFactory;
import conditions.ConditionsPackage;
import conditions.SignalComparisonExpression;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import statemachine.StatemachineFactory;

/**
 * This is the item provider adapter for a {@link conditions.SignalComparisonExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SignalComparisonExpressionItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalComparisonExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addOperatorPropertyDescriptor(object);
			addOperatorTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Operator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperatorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SignalComparisonExpression_operator_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SignalComparisonExpression_operator_feature", "_UI_SignalComparisonExpression_type"),
				 ConditionsPackage.eINSTANCE.getSignalComparisonExpression_Operator(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Operator Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperatorTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SignalComparisonExpression_operatorTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SignalComparisonExpression_operatorTmplParam_feature", "_UI_SignalComparisonExpression_type"),
				 ConditionsPackage.eINSTANCE.getSignalComparisonExpression_OperatorTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SignalComparisonExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SignalComparisonExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SignalComparisonExpression)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_SignalComparisonExpression_type") :
			getString("_UI_SignalComparisonExpression_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SignalComparisonExpression.class)) {
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createSignalComparisonExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createConstantComparatorValue()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createCalculationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createVariableReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createSignalReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createBitPatternComparatorValue()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createParseStringToDouble()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createExtract()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createSubstring()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createParseNumericToString()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createStringLength()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createContainerSignal()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createDoubleSignal()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createStringSignal()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createPluginSignal()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 ConditionsFactory.eINSTANCE.createHeaderSignal()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getSignalComparisonExpression_ComparatorSignal(),
				 StatemachineFactory.eINSTANCE.createComputeVariable()));
	}

}
