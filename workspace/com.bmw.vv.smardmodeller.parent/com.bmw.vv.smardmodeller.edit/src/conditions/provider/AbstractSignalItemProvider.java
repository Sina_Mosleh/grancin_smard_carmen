/**
 */
package conditions.provider;


import conditions.AbstractSignal;
import conditions.ConditionsFactory;
import conditions.ConditionsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.AbstractSignal} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractSignalItemProvider extends BaseClassWithSourceReferenceItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractSignalItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractSignal_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractSignal_name_feature", "_UI_AbstractSignal_type"),
				 ConditionsPackage.eINSTANCE.getAbstractSignal_Name(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getAbstractSignal_DecodeStrategy());
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AbstractSignal)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AbstractSignal_type") :
			getString("_UI_AbstractSignal_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractSignal.class)) {
			case ConditionsPackage.ABSTRACT_SIGNAL__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
			case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_DecodeStrategy(),
				 ConditionsFactory.eINSTANCE.createDoubleDecodeStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_DecodeStrategy(),
				 ConditionsFactory.eINSTANCE.createEmptyDecodeStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_DecodeStrategy(),
				 ConditionsFactory.eINSTANCE.createStringDecodeStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createUniversalPayloadExtractStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createEmptyExtractStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createUniversalPayloadWithLegacyExtractStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createPluginStateExtractStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createPluginResultExtractStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getAbstractSignal_ExtractStrategy(),
				 ConditionsFactory.eINSTANCE.createVerboseDLTExtractStrategy()));
	}

}
