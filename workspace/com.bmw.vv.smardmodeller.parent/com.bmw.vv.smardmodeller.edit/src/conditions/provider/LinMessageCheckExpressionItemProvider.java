/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.LinMessageCheckExpression;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.LinMessageCheckExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LinMessageCheckExpressionItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinMessageCheckExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBusidPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addMessageIDsPropertyDescriptor(object);
			addTypeTmplParamPropertyDescriptor(object);
			addBusidTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Busid feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusidPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinMessageCheckExpression_busid_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinMessageCheckExpression_busid_feature", "_UI_LinMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getLinMessageCheckExpression_Busid(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinMessageCheckExpression_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinMessageCheckExpression_type_feature", "_UI_LinMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getLinMessageCheckExpression_Type(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message IDs feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageIDsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinMessageCheckExpression_messageIDs_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinMessageCheckExpression_messageIDs_feature", "_UI_LinMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getLinMessageCheckExpression_MessageIDs(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinMessageCheckExpression_typeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinMessageCheckExpression_typeTmplParam_feature", "_UI_LinMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getLinMessageCheckExpression_TypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Busid Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusidTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinMessageCheckExpression_busidTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinMessageCheckExpression_busidTmplParam_feature", "_UI_LinMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getLinMessageCheckExpression_BusidTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns LinMessageCheckExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LinMessageCheckExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LinMessageCheckExpression)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_LinMessageCheckExpression_type") :
			getString("_UI_LinMessageCheckExpression_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LinMessageCheckExpression.class)) {
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID:
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE:
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
			case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
