/**
 */
package statemachine.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>statemachine</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachineTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new StatemachineTests("statemachine Tests");
		suite.addTestSuite(DocumentRootTest.class);
		suite.addTestSuite(TransitionTest.class);
		suite.addTestSuite(StateMachineTest.class);
		suite.addTestSuite(ActionTest.class);
		suite.addTestSuite(InitialStateTest.class);
		suite.addTestSuite(StateTest.class);
		suite.addTestSuite(ComputeVariableTest.class);
		suite.addTestSuite(ShowVariableTest.class);
		suite.addTestSuite(ControlActionTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineTests(String name) {
		super(name);
	}

} //StatemachineTests
