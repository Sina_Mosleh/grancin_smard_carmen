/**
 */
package statemachine.tests;

import junit.framework.TestCase;

import statemachine.SmardTraceElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Smard Trace Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}</li>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class SmardTraceElementTest extends TestCase {

	/**
	 * The fixture for this Smard Trace Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SmardTraceElement fixture = null;

	/**
	 * Constructs a new Smard Trace Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmardTraceElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Smard Trace Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(SmardTraceElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Smard Trace Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SmardTraceElement getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier()
	 * @generated
	 */
	public void testGetIdentifier() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier(java.lang.String)
	 * @generated
	 */
	public void testGetIdentifier__String() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //SmardTraceElementTest
