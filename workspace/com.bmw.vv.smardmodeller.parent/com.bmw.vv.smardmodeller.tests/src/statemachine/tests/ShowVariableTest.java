/**
 */
package statemachine.tests;

import junit.textui.TestRunner;

import statemachine.ShowVariable;
import statemachine.StatemachineFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Show Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}</li>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ShowVariableTest extends AbstractActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ShowVariableTest.class);
	}

	/**
	 * Constructs a new Show Variable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ShowVariableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Show Variable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ShowVariable getFixture() {
		return (ShowVariable)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachineFactory.eINSTANCE.createShowVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier()
	 * @generated
	 */
	public void testGetIdentifier() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier(java.lang.String)
	 * @generated
	 */
	public void testGetIdentifier__String() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ShowVariableTest
