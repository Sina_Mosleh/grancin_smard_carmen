/**
 */
package statemachineset.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import statemachineset.StateMachineSet;
import statemachineset.StatemachinesetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>State Machine Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link statemachineset.StateMachineSet#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#updateID() <em>Update ID</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#createUUID() <em>Create UUID</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#incrementExportVersionCounter() <em>Increment Export Version Counter</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getProjectName() <em>Get Project Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StateMachineSetTest extends TestCase {

	/**
	 * The fixture for this State Machine Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachineSet fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StateMachineSetTest.class);
	}

	/**
	 * Constructs a new State Machine Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachineSetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this State Machine Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StateMachineSet fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this State Machine Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachineSet getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachinesetFactory.eINSTANCE.createStateMachineSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link statemachineset.StateMachineSet#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.StateMachineSet#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDescription__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachineset.StateMachineSet#updateID() <em>Update ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.StateMachineSet#updateID()
	 * @generated
	 */
	public void testUpdateID() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachineset.StateMachineSet#createUUID() <em>Create UUID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.StateMachineSet#createUUID()
	 * @generated
	 */
	public void testCreateUUID() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachineset.StateMachineSet#incrementExportVersionCounter() <em>Increment Export Version Counter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.StateMachineSet#incrementExportVersionCounter()
	 * @generated
	 */
	public void testIncrementExportVersionCounter() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachineset.StateMachineSet#getProjectName() <em>Get Project Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.StateMachineSet#getProjectName()
	 * @generated
	 */
	public void testGetProjectName() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //StateMachineSetTest
