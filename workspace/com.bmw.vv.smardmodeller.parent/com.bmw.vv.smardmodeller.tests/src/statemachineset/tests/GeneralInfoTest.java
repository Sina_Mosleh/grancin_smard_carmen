/**
 */
package statemachineset.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import statemachineset.GeneralInfo;
import statemachineset.StatemachinesetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>General Info</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class GeneralInfoTest extends TestCase {

	/**
	 * The fixture for this General Info test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeneralInfo fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(GeneralInfoTest.class);
	}

	/**
	 * Constructs a new General Info test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralInfoTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this General Info test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GeneralInfo fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this General Info test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeneralInfo getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachinesetFactory.eINSTANCE.createGeneralInfo());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //GeneralInfoTest
