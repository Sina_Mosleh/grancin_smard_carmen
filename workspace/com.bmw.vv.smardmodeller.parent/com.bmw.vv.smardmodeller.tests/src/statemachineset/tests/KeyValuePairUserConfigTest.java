/**
 */
package statemachineset.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import statemachineset.KeyValuePairUserConfig;
import statemachineset.StatemachinesetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Key Value Pair User Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class KeyValuePairUserConfigTest extends TestCase {

	/**
	 * The fixture for this Key Value Pair User Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeyValuePairUserConfig fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(KeyValuePairUserConfigTest.class);
	}

	/**
	 * Constructs a new Key Value Pair User Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KeyValuePairUserConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Key Value Pair User Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(KeyValuePairUserConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Key Value Pair User Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KeyValuePairUserConfig getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachinesetFactory.eINSTANCE.createKeyValuePairUserConfig());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //KeyValuePairUserConfigTest
