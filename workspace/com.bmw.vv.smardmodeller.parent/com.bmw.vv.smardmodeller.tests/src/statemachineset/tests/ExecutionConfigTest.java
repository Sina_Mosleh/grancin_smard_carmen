/**
 */
package statemachineset.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import statemachineset.ExecutionConfig;
import statemachineset.StatemachinesetFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Execution Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link statemachineset.ExecutionConfig#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}</li>
 *   <li>{@link statemachineset.ExecutionConfig#isValid_hasValidSorterValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sorter Value</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ExecutionConfigTest extends TestCase {

	/**
	 * The fixture for this Execution Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionConfig fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ExecutionConfigTest.class);
	}

	/**
	 * Constructs a new Execution Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Execution Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ExecutionConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Execution Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionConfig getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachinesetFactory.eINSTANCE.createExecutionConfig());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link statemachineset.ExecutionConfig#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.ExecutionConfig#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidLogLevel__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachineset.ExecutionConfig#isValid_hasValidSorterValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sorter Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.ExecutionConfig#isValid_hasValidSorterValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSorterValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ExecutionConfigTest
