/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.EthernetFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ethernet Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidSourceMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source MAC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidDestinationMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination MAC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidCRC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid CRC</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidEtherType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ether Type</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidInnerVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan Vid</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidOuterVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan Vid</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidInnerVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan CFI</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidOuterVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan CFI</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidInnerVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan PCP</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidOuterVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan PCP</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidInnerVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan TPID</em>}</li>
 *   <li>{@link conditions.EthernetFilter#isValid_hasValidOuterVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan TPID</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class EthernetFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EthernetFilterTest.class);
	}

	/**
	 * Constructs a new Ethernet Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EthernetFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ethernet Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EthernetFilter getFixture() {
		return (EthernetFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createEthernetFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidSourceMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source MAC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidSourceMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSourceMAC__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidDestinationMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination MAC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidDestinationMAC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDestinationMAC__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidCRC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid CRC</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidCRC(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCRC__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidEtherType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ether Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidEtherType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidEtherType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan Vid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInnerVlanVid__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan Vid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanVid(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOuterVlanVid__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan CFI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInnerVlanCFI__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan CFI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanCFI(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOuterVlanCFI__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan PCP</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInnerVlanPCP__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan PCP</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanPCP(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOuterVlanPCP__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidInnerVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Inner Vlan TPID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidInnerVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInnerVlanTPID__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.EthernetFilter#isValid_hasValidOuterVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Outer Vlan TPID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.EthernetFilter#isValid_hasValidOuterVlanTPID(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOuterVlanTPID__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //EthernetFilterTest
