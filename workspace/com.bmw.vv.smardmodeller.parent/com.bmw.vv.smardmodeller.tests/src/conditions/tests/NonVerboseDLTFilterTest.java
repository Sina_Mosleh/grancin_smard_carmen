/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.NonVerboseDLTFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Non Verbose DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.NonVerboseDLTFilter#isValid_hasValidMessageIdOrMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Or Message Id Range</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class NonVerboseDLTFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NonVerboseDLTFilterTest.class);
	}

	/**
	 * Constructs a new Non Verbose DLT Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonVerboseDLTFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Non Verbose DLT Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NonVerboseDLTFilter getFixture() {
		return (NonVerboseDLTFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createNonVerboseDLTFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.NonVerboseDLTFilter#isValid_hasValidMessageIdOrMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Or Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.NonVerboseDLTFilter#isValid_hasValidMessageIdOrMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageIdOrMessageIdRange__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //NonVerboseDLTFilterTest
