/**
 */
package conditions.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>conditions</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConditionsTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ConditionsTests("conditions Tests");
		suite.addTestSuite(DocumentRootTest.class);
		suite.addTestSuite(ConditionTest.class);
		suite.addTestSuite(SignalComparisonExpressionTest.class);
		suite.addTestSuite(CanMessageCheckExpressionTest.class);
		suite.addTestSuite(LinMessageCheckExpressionTest.class);
		suite.addTestSuite(StateCheckExpressionTest.class);
		suite.addTestSuite(TimingExpressionTest.class);
		suite.addTestSuite(TrueExpressionTest.class);
		suite.addTestSuite(LogicalExpressionTest.class);
		suite.addTestSuite(ReferenceConditionExpressionTest.class);
		suite.addTestSuite(TransitionCheckExpressionTest.class);
		suite.addTestSuite(FlexRayMessageCheckExpressionTest.class);
		suite.addTestSuite(StopExpressionTest.class);
		suite.addTestSuite(ConstantComparatorValueTest.class);
		suite.addTestSuite(CalculationExpressionTest.class);
		suite.addTestSuite(VariableReferenceTest.class);
		suite.addTestSuite(VariableSetTest.class);
		suite.addTestSuite(SignalVariableTest.class);
		suite.addTestSuite(ValueVariableTest.class);
		suite.addTestSuite(VariableFormatTest.class);
		suite.addTestSuite(ObserverValueRangeTest.class);
		suite.addTestSuite(SignalObserverTest.class);
		suite.addTestSuite(SignalReferenceTest.class);
		suite.addTestSuite(BitPatternComparatorValueTest.class);
		suite.addTestSuite(NotExpressionTest.class);
		suite.addTestSuite(ValueVariableObserverTest.class);
		suite.addTestSuite(ParseStringToDoubleTest.class);
		suite.addTestSuite(MatchesTest.class);
		suite.addTestSuite(ExtractTest.class);
		suite.addTestSuite(SubstringTest.class);
		suite.addTestSuite(ParseNumericToStringTest.class);
		suite.addTestSuite(StringLengthTest.class);
		suite.addTestSuite(SomeIPMessageTest.class);
		suite.addTestSuite(ContainerSignalTest.class);
		suite.addTestSuite(EthernetFilterTest.class);
		suite.addTestSuite(UDPFilterTest.class);
		suite.addTestSuite(TCPFilterTest.class);
		suite.addTestSuite(IPv4FilterTest.class);
		suite.addTestSuite(SomeIPFilterTest.class);
		suite.addTestSuite(ForEachExpressionTest.class);
		suite.addTestSuite(MessageCheckExpressionTest.class);
		suite.addTestSuite(SomeIPSDFilterTest.class);
		suite.addTestSuite(SomeIPSDMessageTest.class);
		suite.addTestSuite(DoubleSignalTest.class);
		suite.addTestSuite(StringSignalTest.class);
		suite.addTestSuite(DoubleDecodeStrategyTest.class);
		suite.addTestSuite(UniversalPayloadExtractStrategyTest.class);
		suite.addTestSuite(CANFilterTest.class);
		suite.addTestSuite(LINFilterTest.class);
		suite.addTestSuite(FlexRayFilterTest.class);
		suite.addTestSuite(DLTFilterTest.class);
		suite.addTestSuite(UDPNMFilterTest.class);
		suite.addTestSuite(CANMessageTest.class);
		suite.addTestSuite(LINMessageTest.class);
		suite.addTestSuite(FlexRayMessageTest.class);
		suite.addTestSuite(UDPMessageTest.class);
		suite.addTestSuite(TCPMessageTest.class);
		suite.addTestSuite(UDPNMMessageTest.class);
		suite.addTestSuite(VerboseDLTMessageTest.class);
		suite.addTestSuite(UniversalPayloadWithLegacyExtractStrategyTest.class);
		suite.addTestSuite(PluginFilterTest.class);
		suite.addTestSuite(PluginMessageTest.class);
		suite.addTestSuite(PluginSignalTest.class);
		suite.addTestSuite(PluginStateExtractStrategyTest.class);
		suite.addTestSuite(PluginResultExtractStrategyTest.class);
		suite.addTestSuite(PluginCheckExpressionTest.class);
		suite.addTestSuite(HeaderSignalTest.class);
		suite.addTestSuite(EthernetMessageTest.class);
		suite.addTestSuite(IPv4MessageTest.class);
		suite.addTestSuite(NonVerboseDLTMessageTest.class);
		suite.addTestSuite(NonVerboseDLTFilterTest.class);
		suite.addTestSuite(PayloadFilterTest.class);
		suite.addTestSuite(VerboseDLTPayloadFilterTest.class);
		suite.addTestSuite(VariableStructureTest.class);
		suite.addTestSuite(ComputedVariableTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionsTests(String name) {
		super(name);
	}

} //ConditionsTests
