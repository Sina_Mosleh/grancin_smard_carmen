/**
 */
package conditions.tests;

import conditions.TPFilter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>TP Filter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TPFilterTest extends AbstractFilterTest {

	/**
	 * Constructs a new TP Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TPFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this TP Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TPFilter getFixture() {
		return (TPFilter)fixture;
	}

} //TPFilterTest
