/**
 */
package conditions.tests;

import conditions.Variable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.Variable#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}</li>
 *   <li>{@link conditions.Variable#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Type</em>}</li>
 *   <li>{@link conditions.Variable#isValid_hasValidUnit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Unit</em>}</li>
 *   <li>{@link conditions.Variable#isValid_hasValidVariableFormatTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Variable Format Tmpl Param</em>}</li>
 *   <li>{@link conditions.Variable#getObservers() <em>Get Observers</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class VariableTest extends AbstractVariableTest {

	/**
	 * Constructs a new Variable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Variable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Variable getFixture() {
		return (Variable)fixture;
	}

	/**
	 * Tests the '{@link conditions.Variable#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Variable#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInterpretedValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.Variable#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Variable#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.Variable#isValid_hasValidUnit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Variable#isValid_hasValidUnit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidUnit__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.Variable#isValid_hasValidVariableFormatTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Variable Format Tmpl Param</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Variable#isValid_hasValidVariableFormatTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidVariableFormatTmplParam__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.Variable#getObservers() <em>Get Observers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Variable#getObservers()
	 * @generated
	 */
	public void testGetObservers() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //VariableTest
