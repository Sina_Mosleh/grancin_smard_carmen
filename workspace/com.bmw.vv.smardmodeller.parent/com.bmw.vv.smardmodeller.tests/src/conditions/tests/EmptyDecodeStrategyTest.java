/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.EmptyDecodeStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Empty Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmptyDecodeStrategyTest extends DecodeStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmptyDecodeStrategyTest.class);
	}

	/**
	 * Constructs a new Empty Decode Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmptyDecodeStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Empty Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmptyDecodeStrategy getFixture() {
		return (EmptyDecodeStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createEmptyDecodeStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmptyDecodeStrategyTest
