/**
 */
package conditions.tests;

import conditions.IStateTransitionReference;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IState Transition Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.IStateTransitionReference#getStateDependencies() <em>Get State Dependencies</em>}</li>
 *   <li>{@link conditions.IStateTransitionReference#getTransitionDependencies() <em>Get Transition Dependencies</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IStateTransitionReferenceTest extends TestCase {

	/**
	 * The fixture for this IState Transition Reference test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IStateTransitionReference fixture = null;

	/**
	 * Constructs a new IState Transition Reference test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IStateTransitionReferenceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IState Transition Reference test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IStateTransitionReference fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IState Transition Reference test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IStateTransitionReference getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link conditions.IStateTransitionReference#getStateDependencies() <em>Get State Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStateTransitionReference#getStateDependencies()
	 * @generated
	 */
	public void testGetStateDependencies() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IStateTransitionReference#getTransitionDependencies() <em>Get Transition Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStateTransitionReference#getTransitionDependencies()
	 * @generated
	 */
	public void testGetTransitionDependencies() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IStateTransitionReferenceTest
