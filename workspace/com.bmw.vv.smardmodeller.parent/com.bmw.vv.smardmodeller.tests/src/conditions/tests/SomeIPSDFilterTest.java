/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.SomeIPSDFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Some IPSD Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidInstanceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Instance Id</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidEventGroupId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Event Group Id</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidFlags(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Flags</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidSdType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sd Type</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidMajorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Major Version</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidMinorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Minor Version</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidIndexFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index First Option</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidIndexSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index Second Option</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidNumberFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number First Option</em>}</li>
 *   <li>{@link conditions.SomeIPSDFilter#isValid_hasValidNumberSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number Second Option</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SomeIPSDFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SomeIPSDFilterTest.class);
	}

	/**
	 * Constructs a new Some IPSD Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Some IPSD Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SomeIPSDFilter getFixture() {
		return (SomeIPSDFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createSomeIPSDFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidInstanceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Instance Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidInstanceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInstanceId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidEventGroupId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Event Group Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidEventGroupId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidEventGroupId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidFlags(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Flags</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidFlags(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidFlags__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidSdType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sd Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidSdType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSdType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidMajorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Major Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidMajorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMajorVersion__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidMinorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Minor Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidMinorVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMinorVersion__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidIndexFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index First Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidIndexFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidIndexFirstOption__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidIndexSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index Second Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidIndexSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidIndexSecondOption__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidNumberFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number First Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidNumberFirstOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidNumberFirstOption__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPSDFilter#isValid_hasValidNumberSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Number Second Option</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPSDFilter#isValid_hasValidNumberSecondOption(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidNumberSecondOption__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //SomeIPSDFilterTest
