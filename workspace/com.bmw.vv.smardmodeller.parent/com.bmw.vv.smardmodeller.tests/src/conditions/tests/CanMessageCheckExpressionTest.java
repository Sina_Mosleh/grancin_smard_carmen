/**
 */
package conditions.tests;

import conditions.CanMessageCheckExpression;
import conditions.ConditionsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Can Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.CanMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}</li>
 *   <li>{@link conditions.CanMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}</li>
 *   <li>{@link conditions.CanMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}</li>
 *   <li>{@link conditions.CanMessageCheckExpression#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}</li>
 *   <li>{@link conditions.CanMessageCheckExpression#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CanMessageCheckExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CanMessageCheckExpressionTest.class);
	}

	/**
	 * Constructs a new Can Message Check Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CanMessageCheckExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Can Message Check Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CanMessageCheckExpression getFixture() {
		return (CanMessageCheckExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createCanMessageCheckExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.CanMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidBusId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CanMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageIdRange__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CanMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCheckType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CanMessageCheckExpression#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidRxTxFlag__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CanMessageCheckExpression#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CanMessageCheckExpression#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidExtIdentifier__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //CanMessageCheckExpressionTest
