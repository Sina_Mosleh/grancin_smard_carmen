/**
 */
package conditions.tests;

import conditions.INumericOperand;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>INumeric Operand</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class INumericOperandTest extends TestCase {

	/**
	 * The fixture for this INumeric Operand test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INumericOperand fixture = null;

	/**
	 * Constructs a new INumeric Operand test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public INumericOperandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this INumeric Operand test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(INumericOperand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this INumeric Operand test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected INumericOperand getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperand#get_EvaluationDataType()
	 * @generated
	 */
	public void testGet_EvaluationDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	public void testGetReadVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	public void testGetWriteVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //INumericOperandTest
