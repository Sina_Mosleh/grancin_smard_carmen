/**
 */
package conditions.tests;

import conditions.ExtractStrategy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ExtractStrategyTest extends TestCase {

	/**
	 * The fixture for this Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtractStrategy fixture = null;

	/**
	 * Constructs a new Extract Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtractStrategyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ExtractStrategy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtractStrategy getFixture() {
		return fixture;
	}

} //ExtractStrategyTest
