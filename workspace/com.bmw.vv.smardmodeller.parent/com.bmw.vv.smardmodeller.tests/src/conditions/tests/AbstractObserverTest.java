/**
 */
package conditions.tests;

import conditions.AbstractObserver;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Observer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.AbstractObserver#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}</li>
 *   <li>{@link conditions.AbstractObserver#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}</li>
 *   <li>{@link conditions.AbstractObserver#isValid_hasValidValueRanges(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList) <em>Is Valid has Valid Value Ranges</em>}</li>
 *   <li>{@link conditions.AbstractObserver#isValid_hasValidValueRangesTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value Ranges Tmpl Param</em>}</li>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}</li>
 *   <li>{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractObserverTest extends BaseClassWithSourceReferenceTest {

	/**
	 * Constructs a new Abstract Observer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractObserverTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Observer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractObserver getFixture() {
		return (AbstractObserver)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractObserver#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractObserver#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidName__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractObserver#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractObserver#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidLogLevel__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractObserver#isValid_hasValidValueRanges(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList) <em>Is Valid has Valid Value Ranges</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractObserver#isValid_hasValidValueRanges(org.eclipse.emf.common.util.EList, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	public void testIsValid_hasValidValueRanges__EList_EList() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractObserver#isValid_hasValidValueRangesTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value Ranges Tmpl Param</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractObserver#isValid_hasValidValueRangesTmplParam(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidValueRangesTmplParam__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier()
	 * @generated
	 */
	public void testGetIdentifier() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement#getIdentifier(java.lang.String)
	 * @generated
	 */
	public void testGetIdentifier__String() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractObserverTest
