/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.DLTFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.DLTFilter#isValid_hasValidMessageBusInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Bus Info</em>}</li>
 *   <li>{@link conditions.DLTFilter#isValid_hasValidMessageControlInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Control Info</em>}</li>
 *   <li>{@link conditions.DLTFilter#isValid_hasValidMessageLogInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Log Info</em>}</li>
 *   <li>{@link conditions.DLTFilter#isValid_hasValidMessageTraceInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Trace Info</em>}</li>
 *   <li>{@link conditions.DLTFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class DLTFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DLTFilterTest.class);
	}

	/**
	 * Constructs a new DLT Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLTFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this DLT Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DLTFilter getFixture() {
		return (DLTFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createDLTFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.DLTFilter#isValid_hasValidMessageBusInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Bus Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLTFilter#isValid_hasValidMessageBusInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageBusInfo__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.DLTFilter#isValid_hasValidMessageControlInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Control Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLTFilter#isValid_hasValidMessageControlInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageControlInfo__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.DLTFilter#isValid_hasValidMessageLogInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Log Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLTFilter#isValid_hasValidMessageLogInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageLogInfo__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.DLTFilter#isValid_hasValidMessageTraceInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Trace Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLTFilter#isValid_hasValidMessageTraceInfo(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageTraceInfo__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.DLTFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DLTFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //DLTFilterTest
