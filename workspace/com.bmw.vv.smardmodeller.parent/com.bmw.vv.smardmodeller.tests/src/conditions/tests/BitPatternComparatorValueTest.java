/**
 */
package conditions.tests;

import conditions.BitPatternComparatorValue;
import conditions.ConditionsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Bit Pattern Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.BitPatternComparatorValue#isValid_hasValidBitPattern(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bit Pattern</em>}</li>
 *   <li>{@link conditions.BitPatternComparatorValue#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class BitPatternComparatorValueTest extends ComparatorSignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BitPatternComparatorValueTest.class);
	}

	/**
	 * Constructs a new Bit Pattern Comparator Value test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BitPatternComparatorValueTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Bit Pattern Comparator Value test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BitPatternComparatorValue getFixture() {
		return (BitPatternComparatorValue)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createBitPatternComparatorValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.BitPatternComparatorValue#isValid_hasValidBitPattern(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bit Pattern</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.BitPatternComparatorValue#isValid_hasValidBitPattern(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidBitPattern__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.BitPatternComparatorValue#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.BitPatternComparatorValue#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStartbit__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //BitPatternComparatorValueTest
