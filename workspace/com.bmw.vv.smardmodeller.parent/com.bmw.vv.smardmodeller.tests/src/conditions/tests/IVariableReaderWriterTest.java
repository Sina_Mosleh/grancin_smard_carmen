/**
 */
package conditions.tests;

import conditions.IVariableReaderWriter;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IVariable Reader Writer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class IVariableReaderWriterTest extends TestCase {

	/**
	 * The fixture for this IVariable Reader Writer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IVariableReaderWriter fixture = null;

	/**
	 * Constructs a new IVariable Reader Writer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVariableReaderWriterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this IVariable Reader Writer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IVariableReaderWriter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this IVariable Reader Writer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IVariableReaderWriter getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	public void testGetReadVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	public void testGetWriteVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IVariableReaderWriterTest
