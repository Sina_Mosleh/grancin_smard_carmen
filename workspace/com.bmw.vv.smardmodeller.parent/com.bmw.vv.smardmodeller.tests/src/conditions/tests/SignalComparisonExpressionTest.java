/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.SignalComparisonExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Signal Comparison Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.SignalComparisonExpression#isValid_hasValidComparator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Comparator</em>}</li>
 *   <li>{@link conditions.SignalComparisonExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}</li>
 *   <li>{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}</li>
 *   <li>{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}</li>
 *   <li>{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SignalComparisonExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SignalComparisonExpressionTest.class);
	}

	/**
	 * Constructs a new Signal Comparison Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalComparisonExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Signal Comparison Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SignalComparisonExpression getFixture() {
		return (SignalComparisonExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createSignalComparisonExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.SignalComparisonExpression#isValid_hasValidComparator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Comparator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SignalComparisonExpression#isValid_hasValidComparator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidComparator__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SignalComparisonExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SignalComparisonExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOperands__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_OperandDataType()
	 * @generated
	 */
	public void testGet_OperandDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_Operands()
	 * @generated
	 */
	public void testGet_Operands() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperand#get_EvaluationDataType()
	 * @generated
	 */
	public void testGet_EvaluationDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //SignalComparisonExpressionTest
