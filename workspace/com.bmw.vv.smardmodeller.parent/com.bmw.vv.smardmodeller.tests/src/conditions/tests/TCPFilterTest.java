/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.TCPFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>TCP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidSequenceNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sequence Number</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidAcknowledgementNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Acknowledgement Number</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidTcpFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Tcp Flag</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidStreamAnalysisFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Analysis Flag</em>}</li>
 *   <li>{@link conditions.TCPFilter#isValid_hasValidStreamValidPayloadOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Valid Payload Offset</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class TCPFilterTest extends TPFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TCPFilterTest.class);
	}

	/**
	 * Constructs a new TCP Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TCPFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this TCP Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TCPFilter getFixture() {
		return (TCPFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createTCPFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSourcePort__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDestinationPort__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidSequenceNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sequence Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidSequenceNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSequenceNumber__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidAcknowledgementNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Acknowledgement Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidAcknowledgementNumber(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidAcknowledgementNumber__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidTcpFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Tcp Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidTcpFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidTcpFlag__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidStreamAnalysisFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Analysis Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidStreamAnalysisFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStreamAnalysisFlag__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TCPFilter#isValid_hasValidStreamValidPayloadOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stream Valid Payload Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TCPFilter#isValid_hasValidStreamValidPayloadOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStreamValidPayloadOffset__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //TCPFilterTest
