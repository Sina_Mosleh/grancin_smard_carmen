/**
 */
package conditions.tests;

import conditions.BaseClassWithID;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Base Class With ID</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BaseClassWithIDTest extends TestCase {

	/**
	 * The fixture for this Base Class With ID test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseClassWithID fixture = null;

	/**
	 * Constructs a new Base Class With ID test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseClassWithIDTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Base Class With ID test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(BaseClassWithID fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Base Class With ID test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseClassWithID getFixture() {
		return fixture;
	}

} //BaseClassWithIDTest
