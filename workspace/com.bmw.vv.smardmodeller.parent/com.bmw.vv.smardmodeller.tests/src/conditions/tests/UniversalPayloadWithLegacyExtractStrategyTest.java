/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.UniversalPayloadWithLegacyExtractStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Universal Payload With Legacy Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}</li>
 *   <li>{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidDataLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Data Length</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UniversalPayloadWithLegacyExtractStrategyTest extends UniversalPayloadExtractStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UniversalPayloadWithLegacyExtractStrategyTest.class);
	}

	/**
	 * Constructs a new Universal Payload With Legacy Extract Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalPayloadWithLegacyExtractStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Universal Payload With Legacy Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UniversalPayloadWithLegacyExtractStrategy getFixture() {
		return (UniversalPayloadWithLegacyExtractStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createUniversalPayloadWithLegacyExtractStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startbit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidStartbit(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStartbit__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidDataLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Data Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UniversalPayloadWithLegacyExtractStrategy#isValid_hasValidDataLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDataLength__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UniversalPayloadWithLegacyExtractStrategyTest
