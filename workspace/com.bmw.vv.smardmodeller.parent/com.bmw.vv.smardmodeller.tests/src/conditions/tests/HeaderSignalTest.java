/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.HeaderSignal;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Header Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.HeaderSignal#isValid_hasValidAttribute(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Attribute</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class HeaderSignalTest extends AbstractSignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(HeaderSignalTest.class);
	}

	/**
	 * Constructs a new Header Signal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HeaderSignalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Header Signal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected HeaderSignal getFixture() {
		return (HeaderSignal)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createHeaderSignal());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.HeaderSignal#isValid_hasValidAttribute(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Attribute</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.HeaderSignal#isValid_hasValidAttribute(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidAttribute__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //HeaderSignalTest
