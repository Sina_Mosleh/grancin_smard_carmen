/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.StringDecodeStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>String Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StringDecodeStrategyTest extends DecodeStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StringDecodeStrategyTest.class);
	}

	/**
	 * Constructs a new String Decode Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringDecodeStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this String Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StringDecodeStrategy getFixture() {
		return (StringDecodeStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createStringDecodeStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StringDecodeStrategyTest
