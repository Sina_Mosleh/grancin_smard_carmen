/**
 */
package conditions.tests;

import conditions.CalculationExpression;
import conditions.ConditionsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Calculation Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.CalculationExpression#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Expression</em>}</li>
 *   <li>{@link conditions.CalculationExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}</li>
 *   <li>{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}</li>
 *   <li>{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CalculationExpressionTest extends ComparatorSignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CalculationExpressionTest.class);
	}

	/**
	 * Constructs a new Calculation Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CalculationExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Calculation Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CalculationExpression getFixture() {
		return (CalculationExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createCalculationExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.CalculationExpression#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CalculationExpression#isValid_hasValidExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidExpression__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CalculationExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CalculationExpression#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOperands__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_OperandDataType()
	 * @generated
	 */
	public void testGet_OperandDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_Operands()
	 * @generated
	 */
	public void testGet_Operands() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //CalculationExpressionTest
