/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.ConstantComparatorValue;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Constant Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.ConstantComparatorValue#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}</li>
 *   <li>{@link conditions.ConstantComparatorValue#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ConstantComparatorValueTest extends ComparatorSignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ConstantComparatorValueTest.class);
	}

	/**
	 * Constructs a new Constant Comparator Value test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantComparatorValueTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Constant Comparator Value test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ConstantComparatorValue getFixture() {
		return (ConstantComparatorValue)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createConstantComparatorValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.ConstantComparatorValue#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interpreted Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ConstantComparatorValue#isValid_hasValidInterpretedValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInterpretedValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.ConstantComparatorValue#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ConstantComparatorValue#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ConstantComparatorValueTest
