/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.Matches;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Matches</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.Matches#isValid_hasValidStringToCheck(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid String To Check</em>}</li>
 *   <li>{@link conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class MatchesTest extends StringExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MatchesTest.class);
	}

	/**
	 * Constructs a new Matches test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MatchesTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Matches test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Matches getFixture() {
		return (Matches)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createMatches());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.Matches#isValid_hasValidStringToCheck(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid String To Check</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Matches#isValid_hasValidStringToCheck(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStringToCheck__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidRegex__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //MatchesTest
