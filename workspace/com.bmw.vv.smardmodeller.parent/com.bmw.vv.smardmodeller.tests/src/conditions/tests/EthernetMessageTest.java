/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.EthernetMessage;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Ethernet Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EthernetMessageTest extends AbstractBusMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EthernetMessageTest.class);
	}

	/**
	 * Constructs a new Ethernet Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EthernetMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Ethernet Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EthernetMessage getFixture() {
		return (EthernetMessage)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createEthernetMessage());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EthernetMessageTest
