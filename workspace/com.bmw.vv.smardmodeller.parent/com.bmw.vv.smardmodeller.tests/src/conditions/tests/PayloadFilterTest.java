/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.PayloadFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.PayloadFilter#isValid_hasValidIndex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index</em>}</li>
 *   <li>{@link conditions.PayloadFilter#isValid_hasValidMask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Mask</em>}</li>
 *   <li>{@link conditions.PayloadFilter#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class PayloadFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PayloadFilterTest.class);
	}

	/**
	 * Constructs a new Payload Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PayloadFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Payload Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected PayloadFilter getFixture() {
		return (PayloadFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createPayloadFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.PayloadFilter#isValid_hasValidIndex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Index</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.PayloadFilter#isValid_hasValidIndex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidIndex__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.PayloadFilter#isValid_hasValidMask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Mask</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.PayloadFilter#isValid_hasValidMask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMask__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.PayloadFilter#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.PayloadFilter#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //PayloadFilterTest
