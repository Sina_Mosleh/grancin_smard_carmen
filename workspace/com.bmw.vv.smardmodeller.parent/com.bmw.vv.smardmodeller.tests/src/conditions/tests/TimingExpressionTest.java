/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.TimingExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Timing Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.TimingExpression#isValid_hasValidMinMaxTimes(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Min Max Times</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class TimingExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TimingExpressionTest.class);
	}

	/**
	 * Constructs a new Timing Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimingExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Timing Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TimingExpression getFixture() {
		return (TimingExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createTimingExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.TimingExpression#isValid_hasValidMinMaxTimes(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Min Max Times</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TimingExpression#isValid_hasValidMinMaxTimes(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMinMaxTimes__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //TimingExpressionTest
