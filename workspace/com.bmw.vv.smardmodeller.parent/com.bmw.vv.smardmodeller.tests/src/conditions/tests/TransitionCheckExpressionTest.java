/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.TransitionCheckExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Transition Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.TransitionCheckExpression#isValid_hasValidStayActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stay Active</em>}</li>
 *   <li>{@link conditions.TransitionCheckExpression#isValid_hasValidTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Transition</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class TransitionCheckExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TransitionCheckExpressionTest.class);
	}

	/**
	 * Constructs a new Transition Check Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionCheckExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Transition Check Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TransitionCheckExpression getFixture() {
		return (TransitionCheckExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createTransitionCheckExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.TransitionCheckExpression#isValid_hasValidStayActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Stay Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TransitionCheckExpression#isValid_hasValidStayActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStayActive__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.TransitionCheckExpression#isValid_hasValidTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Transition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.TransitionCheckExpression#isValid_hasValidTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidTransition__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //TransitionCheckExpressionTest
