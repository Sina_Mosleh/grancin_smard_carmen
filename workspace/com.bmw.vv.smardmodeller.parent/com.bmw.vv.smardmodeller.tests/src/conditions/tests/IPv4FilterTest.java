/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.IPv4Filter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IPv4 Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.IPv4Filter#isValid_hasValidSourceIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Ip Address</em>}</li>
 *   <li>{@link conditions.IPv4Filter#isValid_hasValidDestinationIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Ip Address</em>}</li>
 *   <li>{@link conditions.IPv4Filter#isValid_hasValidTimeToLive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Time To Live</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class IPv4FilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IPv4FilterTest.class);
	}

	/**
	 * Constructs a new IPv4 Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPv4FilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this IPv4 Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IPv4Filter getFixture() {
		return (IPv4Filter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createIPv4Filter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.IPv4Filter#isValid_hasValidSourceIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Ip Address</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IPv4Filter#isValid_hasValidSourceIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSourceIpAddress__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IPv4Filter#isValid_hasValidDestinationIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Ip Address</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IPv4Filter#isValid_hasValidDestinationIpAddress(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDestinationIpAddress__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IPv4Filter#isValid_hasValidTimeToLive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Time To Live</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IPv4Filter#isValid_hasValidTimeToLive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidTimeToLive__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //IPv4FilterTest
