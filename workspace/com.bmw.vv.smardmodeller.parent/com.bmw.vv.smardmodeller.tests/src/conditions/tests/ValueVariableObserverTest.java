/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.ValueVariableObserver;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Value Variable Observer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ValueVariableObserverTest extends AbstractObserverTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ValueVariableObserverTest.class);
	}

	/**
	 * Constructs a new Value Variable Observer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueVariableObserverTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Value Variable Observer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ValueVariableObserver getFixture() {
		return (ValueVariableObserver)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createValueVariableObserver());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ValueVariableObserverTest
