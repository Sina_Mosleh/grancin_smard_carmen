/**
 */
package conditions.tests;

import conditions.CANFilter;
import conditions.ConditionsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CAN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.CANFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}</li>
 *   <li>{@link conditions.CANFilter#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}</li>
 *   <li>{@link conditions.CANFilter#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class CANFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CANFilterTest.class);
	}

	/**
	 * Constructs a new CAN Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CANFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CAN Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CANFilter getFixture() {
		return (CANFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createCANFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.CANFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CANFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CANFilter#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Rx Tx Flag</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CANFilter#isValid_hasValidRxTxFlag(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidRxTxFlag__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.CANFilter#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Ext Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.CANFilter#isValid_hasValidExtIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidExtIdentifier__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //CANFilterTest
