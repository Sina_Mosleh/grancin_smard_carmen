/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.VariableFormat;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Variable Format</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.VariableFormat#isValid_hasValidDigits(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Digits</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class VariableFormatTest extends TestCase {

	/**
	 * The fixture for this Variable Format test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFormat fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VariableFormatTest.class);
	}

	/**
	 * Constructs a new Variable Format test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableFormatTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Variable Format test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(VariableFormat fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Variable Format test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFormat getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createVariableFormat());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.VariableFormat#isValid_hasValidDigits(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Digits</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.VariableFormat#isValid_hasValidDigits(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDigits__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //VariableFormatTest
