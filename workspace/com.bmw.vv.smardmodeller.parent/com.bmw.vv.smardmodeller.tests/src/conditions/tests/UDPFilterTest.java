/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.UDPFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>UDP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.UDPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}</li>
 *   <li>{@link conditions.UDPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}</li>
 *   <li>{@link conditions.UDPFilter#isValid_hasValidChecksum(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Checksum</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UDPFilterTest extends TPFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UDPFilterTest.class);
	}

	/**
	 * Constructs a new UDP Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UDPFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this UDP Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UDPFilter getFixture() {
		return (UDPFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createUDPFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.UDPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPFilter#isValid_hasValidSourcePort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSourcePort__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UDPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Destination Port</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPFilter#isValid_hasValidDestinationPort(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDestinationPort__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UDPFilter#isValid_hasValidChecksum(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Checksum</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPFilter#isValid_hasValidChecksum(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidChecksum__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UDPFilterTest
