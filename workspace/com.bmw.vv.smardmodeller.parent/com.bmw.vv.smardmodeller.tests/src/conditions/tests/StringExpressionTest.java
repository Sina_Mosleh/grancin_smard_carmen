/**
 */
package conditions.tests;

import conditions.StringExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>String Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class StringExpressionTest extends ExpressionTest {

	/**
	 * Constructs a new String Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StringExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this String Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StringExpression getFixture() {
		return (StringExpression)fixture;
	}

} //StringExpressionTest
