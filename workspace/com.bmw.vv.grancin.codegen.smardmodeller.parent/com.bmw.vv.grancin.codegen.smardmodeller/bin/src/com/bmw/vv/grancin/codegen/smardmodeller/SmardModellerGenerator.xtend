package com.bmw.vv.grancin.codegen.smardmodeller

import com.bmw.vv.grancin.codegen.chrisna.ChrisnaFSMGenerator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
//import org.eclipse.xtext.builder.ParallelFileSystemAccess
import org.eclipse.xtext.generator.IGeneratorContext
import com.bmw.vv.client.RequestData
//import org.eclipse.xtext.generator.GeneratorDelegate
import com.bmw.vv.grancin.codegen.chrisna.WriteToFile

class SmardModellerGenerator extends AbstractGenerator {	
    override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context){
    	println("SmardModeller is on board")
    	
    	val csa = new ChrisnaFSMGenerator(resource)
    	val wf = new WriteToFile(csa).WriteFile
		//System.out.println(wf)
		fsa.generateFile('ChrisnaFSM/' + csa.name + '_chrisna_fsm' + '.csa', wf)
	
    	
    	val interfaceDigger = new InterfaceDigging(resource)
    	val interface = interfaceDigger.searchForInterface
    	
    	val requestData = new RequestData("q486705", "NTFuYTU4cmE=", "SP2021", "SP2021", "21-07-240", 45000);
    	//requestData.request
    	
    	println('SmardModeller is ready to Hack')
    	val sdmodel = new SmardModellerCreator(interface, csa, requestData)
    	println('System is Hacked')
    	
    	println('SmardModeller is ready to destroy and rock the system')
    	val xmlwriter = new XMIWriter
    	//println(xmlwriter.WriteConditionsXML(sdmodel.conditionDocuGenerator))
    	//println(xmlwriter.WriteStatemachineXML(sdmodel.stateMachineDocuGenerator))
    	
    	//var parfiles = new ParallelFileSystemAccess()
		fsa.generateFile('SMARDAutomat/Conditions/' + csa.name + '_conditions' + '.conditions', xmlwriter.WriteConditionsXML(sdmodel.conditionDocuGenerator))
		fsa.generateFile('SMARDAutomat/Statemachine/' + csa.name + '_statemachine' + '.statemachine', xmlwriter.WriteStatemachineXML(sdmodel.stateMachineDocuGenerator))
		println('System is destroyed')
    }
} 