package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import chrisna.ScenContainer
import de.bmw.smard.modeller.conditions.DocumentRoot
import de.bmw.smard.modeller.conditions.util.ConditionsResourceFactoryImpl
import de.bmw.smard.modeller.conditions.util.ConditionsValidator
import de.bmw.smard.modeller.statemachine.util.StatemachineResourceFactoryImpl
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator
import de.bmw.smard.modeller.statemachineset.util.StatemachinesetResourceFactoryImpl
import de.bmw.smard.modeller.statemachineset.util.StatemachinesetValidator
import java.io.IOException
import java.io.StringWriter
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.URIConverter
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.XMLResource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.gmf.runtime.notation.Diagram
import de.bmw.smard.modeller.statemachine.StateMachine
import org.eclipse.emf.common.util.BasicEList

class ProjectCreator {
	

		
	new(){
	}
	
	protected def String makeproject(String name) {
		return 
'<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
	<name>' + name + '</name>
	<comment></comment>
	<projects>
	</projects>
	<buildSpec>
	</buildSpec>
	<natures>
	</natures>
</projectDescription>
'
	}
	
	protected def String makesetting(BasicEList<StateMachine> machines){
		var output = 'eclipse.preferences.version=1\n'
		for (var index=0; index<machines.length; index++){
			output += 'encoding//StateMachines/' + machines?.get(index)?.name + '_index' + index.toString + '.statemachine=ISO-8859-1\n'
			output += 'encoding//StateMachines/' + machines?.get(index)?.name + '_index' + index.toString + '.statemachine_diagram=ISO-8859-1\n'
		}
		output
	}
	
	protected def String WriteConfig(int logLevel, int sorterValue, boolean isEthernetneeded, boolean isErrorFramenedded, ArrayList<Double> keyValues){
		return 
'logLevel: ' + logLevel.toString + '
needsEthernet: ' + isEthernetneeded.toString + '
sorterValue: ' + sorterValue.toString + '
keyValues: {' + {
	var String str = ''
	for (key : keyValues)
		str = str + key.toString
	str
} + '}
needsErrorFrames: ' + isErrorFramenedded.toString + '
'
	}
	
	protected def String WriteChrisnaFSMXML(ScenContainer scencontainer){
		try {
			val reg = Resource.Factory.Registry.INSTANCE
        	var m = reg.getExtensionToFactoryMap()
        	m.put("scencontainer", new ConditionsXMIFactoryImpl())
        	//var parameters = new HashMap<String, String>
			//m.put("scencontainer", new SafeRootXMLContentHandlerImpl(parameters))
			
        	var ResourceSet resSet = new ResourceSetImpl()
			
        	var resource = resSet.createResource(URI.createURI("chrisna.scencontainer"))
        	resource.getContents().add(scencontainer)
			
			var stringWriter = new StringWriter()
			var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
			var options = new HashMap<String, String>()
			resource.save(outputStream, options)
			stringWriter.toString()
			
		}
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
	
	protected def String WriteConditionsXML(DocumentRoot docu, String path){
		try {
			val validator = new ConditionsValidator
			if(validator.validateDocumentRoot(docu, null, null)){
				val reg = Resource.Factory.Registry.INSTANCE;
	        	var m = reg.getExtensionToFactoryMap();
	        	//var condDoc = docu.conditionsDocument
	        	//var cond = condDoc
	        	m.put("docu", new ConditionsXMIFactoryImpl());
	
	        	//var ResourceSet resSet = new ResourceSetImpl();
	
	        	val resFactory = new ConditionsResourceFactoryImpl
	        	var resource = resFactory.createResource(URI.createURI(path))
	        	resource.getContents().add(docu);
	
				var stringWriter = new StringWriter()
				var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
				val options = new HashMap<Object, Object>()
				options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER)
				options.put(XMLResource.OPTION_ENCODING, "ISO-8859-1")
				resource.save(outputStream, options)
				stringWriter.toString()
			}
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
	
	protected def String WriteStatemachineXML(de.bmw.smard.modeller.statemachine.DocumentRoot docu, String path){
		try {
			val validator = new StatemachineValidator
			if(validator.validateDocumentRoot(docu, null, null)){
				val reg = Resource.Factory.Registry.INSTANCE;
	        	var m = reg.getExtensionToFactoryMap();
	        	m.put("docu", new ConditionsXMIFactoryImpl());
	
	        	//var ResourceSet resSet = new ResourceSetImpl();
	
	        	//var resource = resSet.createResource(URI.createURI("de.bmw.smard.modeller.statemachineset.docu"));
	        	var resFactory = new StatemachineResourceFactoryImpl
	        	var resource = resFactory.createResource(URI.createURI(path))
	        	resource.getContents().add(docu);
	
				var stringWriter = new StringWriter()
				var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
				val options = new HashMap<String, String>()
				options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER)
				options.put(XMLResource.OPTION_ENCODING, "ISO-8859-1")
				resource.save(outputStream, options)
				stringWriter.toString()
			}
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
	
	protected def WriteStatemachineDiagram(Diagram diagram, String path){
		try {
			//val validator = new StatemachineValidator
			//if(validator.validateStateMachine(diagram, null, null)){
				val reg = Resource.Factory.Registry.INSTANCE;
	        	var m = reg.getExtensionToFactoryMap();
	        	m.put("diagram", new XMIResourceFactoryImpl());
	
	        	//var ResourceSet resSet = new ResourceSetImpl();
	
	        	//var resource = resSet.createResource(URI.createURI("de.bmw.smard.modeller.statemachineset.docu"));
	        	//var resFactory = new StatemachineResourceFactoryImpl
	        	//var resource = resFactory.createResource(URI.createURI(path))
	        	//resource.getContents().add(docu)
	        	
	        	//var editingDomain = WorkspaceEditingDomainFactory.INSTANCE.createEditingDomain()
				//var resFactory = editingDomain.getResourceSet()
				var resFactory = new ResourceSetImpl
				var resource = resFactory.createResource(URI.createPlatformResourceURI(path, true))
				resource.getContents().add(diagram)
				
				var stringWriter = new StringWriter()
				var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
				val options = new HashMap<String, String>()
				options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER)
				options.put(XMLResource.OPTION_ENCODING, "ISO-8859-1")
				resource.save(outputStream, options)
				stringWriter.toString()
			//}
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
	
	protected def String WriteStatemachineSetXML(de.bmw.smard.modeller.statemachineset.DocumentRoot docu, String path){
		try {
			val validator = new StatemachinesetValidator
			if(validator.validateDocumentRoot(docu, null, null)){
				val reg = Resource.Factory.Registry.INSTANCE;
	        	var m = reg.getExtensionToFactoryMap();
	        	m.put("docu", new ConditionsXMIFactoryImpl());
	
	        	//var ResourceSet resSet = new ResourceSetImpl();
	
	        	//var resource = resSet.createResource(URI.createURI(path));
	        	var resFactory = new StatemachinesetResourceFactoryImpl
	        	var resource = resFactory.createResource(URI.createURI(path))
	        	resource.getContents().add(docu);
	
				var stringWriter = new StringWriter()
				var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
				var options = new HashMap<String, String>()
				options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER)
				options.put(XMLResource.OPTION_ENCODING, "ASCII")
				resource.save(outputStream, options)
				stringWriter.toString()
			}
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
}