package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import de.bmw.smard.modeller.conditions.AbstractSignal
import de.bmw.smard.modeller.conditions.DoubleSignal
import java.util.HashMap
import java.io.Serializable
import java.util.UUID
import de.bmw.smard.modeller.conditions.HeaderSignal
import de.bmw.smard.modeller.conditions.ContainerSignal
import de.bmw.smard.modeller.conditions.ComparatorSignal
import de.bmw.smard.modeller.conditions.StringSignal
import de.bmw.smard.modeller.conditions.PluginSignal

class SignalCreator{
	protected var ConditionsFactoryImpl conditions
	protected var DecodeStrategyCreator decodeStrategy
	protected var ExtractStrategyCreator extractStrategy
	protected var SignalType sigtyp
	
	new (DecodeType dec, ExtractType ext, SignalType sig){
		conditions = new ConditionsFactoryImpl
		decodeStrategy = new DecodeStrategyCreator(dec)
		extractStrategy = new ExtractStrategyCreator(ext)
		sigtyp = sig
	}
	
	def SignalType getSigtyp(){
		sigtyp
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map?.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractSignal createAbstractSignal(AbstractSignal signal, HashMap<String, Serializable> map){
		//signal?.setId('sig.' + createUUID)
		//signal.sourceReference = ''
		//signal.getWriteVariables.add()
		signal?.setName(searchInMap(map, 'path'))
		switch (decodeStrategy?.getDectyp) {
			case NONE: {
				signal?.setDecodeStrategy(decodeStrategy?.createDecodeStrategy(null))
			}
			case DOUBLE: {
				signal?.setDecodeStrategy(decodeStrategy?.createDoubleDecodeStrategy(map))
			}
			case EMPTY: {
				signal?.setDecodeStrategy(decodeStrategy?.createEmptyDecodeStrategy)
			}
			case STRING: {
				signal?.setDecodeStrategy(decodeStrategy?.createStringDecodeStrategy)
			}
		}
		
		switch(extractStrategy?.getExttyp){
			case NONE: {
				signal?.setExtractStrategy(extractStrategy?.createExtractStrategy(null))
			}
			case EMPTY: {
				signal.setExtractStrategy(extractStrategy?.createEmptyExtractStrategy)
			}
			case PLUGINRESULT: {
				signal?.setExtractStrategy(extractStrategy?.createPluginResultExtractStrategy)
			}
			case PLUGINSTATE: {
				signal?.setExtractStrategy(extractStrategy?.createPluginStateExtractStrategy)
			}
			case UNIVERSALPAYLOAD: {
				signal?.setExtractStrategy(extractStrategy?.createUniversalPayloadExtractStrategy(map))
			}
			case UNIVERSALPAYLOADWITHLEGACY: {
				signal?.setExtractStrategy(extractStrategy?.createUniversalPayloadWithLegacyExtractStrategy(map))
			}
			case VERBOSEDLT: {
				signal?.setExtractStrategy(extractStrategy?.createVerboseDLTExtractStrategy)
			}
		}
		signal
	}
	
	def DoubleSignal createDoubleSignal(HashMap <String, Serializable> map){
		var doublesignal = conditions?.createDoubleSignal
		doublesignal = createAbstractSignal(doublesignal, map) as DoubleSignal 
		//doublesignal.ignoreInvalidValues = ''
		//doublesignal.ignoredInvalidValuesImplParam = ''
		doublesignal
	}
	
	def HeaderSignal createHeaderSignal(HashMap <String, Serializable> map){
		var headersignal = conditions?.createHeaderSignal
		headersignal = createAbstractSignal(headersignal, map) as HeaderSignal
		//headersignal.attribute = ''
		//headersignal.attributeTmplParam = ''
		headersignal
	}
	
	def ComparatorSignal createComparatorSignal(HashMap <String, Serializable> map){
		//var comparatorsignal = conditions.createComparatorSignal
		//comparatorsignal = createAbstractSignal(comparatorsignal, map, dectyp, exttype) as HeaderSignal
		//comparatorsignal.description = ''
		//comparatorsignal
	}
	
	def StringSignal createStringSignal(HashMap <String, Serializable> map){
		var stringsignal = conditions?.createStringSignal
		stringsignal = createAbstractSignal(stringsignal, map) as StringSignal
		stringsignal
	}
	
	def PluginSignal createPluginSignal(HashMap <String, Serializable> map){
		var pluginsignal = conditions?.createPluginSignal
		pluginsignal = createAbstractSignal(pluginsignal, map) as PluginSignal
		pluginsignal
	} 
	
	def ContainerSignal createContainerSignal(HashMap <String, Serializable> map, int numberofsignals){
		var containersignal = conditions?.createContainerSignal
		for(var i = 0; i < numberofsignals; i++){
			var doublesignal = createDoubleSignal(map)
			//var comparatorsignal = createComparatorSignal(map)
			var headersignal = createHeaderSignal(map)
			var stringsignal = createStringSignal(map)
			var pluginsignal = createPluginSignal(map)
			
			containersignal?.containedSignals.add(doublesignal)
			//containersignal.containedSignals.add(comparatorsignal)
			containersignal?.containedSignals.add(headersignal)
			containersignal?.containedSignals.add(stringsignal)
			containersignal?.containedSignals.add(pluginsignal)
		}
		containersignal
	}
}