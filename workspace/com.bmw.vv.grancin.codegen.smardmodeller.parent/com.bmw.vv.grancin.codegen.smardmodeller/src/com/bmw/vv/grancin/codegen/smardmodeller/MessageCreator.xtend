package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import java.util.UUID
import java.util.HashMap
import java.io.Serializable
import de.bmw.smard.modeller.conditions.AbstractMessage
import de.bmw.smard.modeller.conditions.SomeIPMessage
import de.bmw.smard.modeller.conditions.CANMessage
import de.bmw.smard.modeller.conditions.LINMessage
import de.bmw.smard.modeller.conditions.FlexRayMessage
import de.bmw.smard.modeller.conditions.EthernetMessage
import de.bmw.smard.modeller.conditions.IPv4Message
import de.bmw.smard.modeller.conditions.NonVerboseDLTMessage
import de.bmw.smard.modeller.conditions.PluginMessage
import de.bmw.smard.modeller.conditions.AbstractBusMessage
import de.bmw.smard.modeller.conditions.AbstractSignal
import de.bmw.smard.modeller.conditions.SomeIPSDMessage
import de.bmw.smard.modeller.conditions.TCPMessage
import de.bmw.smard.modeller.conditions.UDPMessage
import de.bmw.smard.modeller.conditions.UDPNMMessage
import de.bmw.smard.modeller.conditions.VerboseDLTMessage
import de.bmw.smard.modeller.conditions.DLTMessage
import java.util.ArrayList

class MessageCreator {
	
	protected var ConditionsFactoryImpl conditions
	protected var ArrayList<SignalCreator> _signals
	protected var ArrayList<FilterCreator> _filters
	var String _busid
	var String _interface_ver
	
	new (BusType busid, String interfacever, ArrayList<SignalCreator> signals, ArrayList<FilterCreator> filters){
		conditions = new ConditionsFactoryImpl
		convertBusTypeToString(busid)
		_signals = signals
		_filters = filters
		_interface_ver = interfacever
	}
	
	def String convertBusTypeToString(BusType bustyp){
		switch(bustyp){
			case SOMEIP:{
				this._busid = '45000'
			}
			case NONE:{
				this._busid = 'NONE'
			}
			case CAN: {
				this._busid = 'CAN'
			}
			case LIN: {
				this._busid = 'LIN'
			}
			case FLEXRAY: {
				this._busid = 'FLEXRAY'
			}
			case ETHERNET: {
				this._busid = 'ETHERNET'
			}
			case IPV4: {
				this._busid = 'IPV4'
			}
			case NONVERBOSEDLT: {
				this._busid = 'NONVERBOSEDLT'
			}
			case PLUGIN: {
				this._busid = 'PLUGIN'
			}
			case SOMEIPSD: {
				this._busid = 'SOMEIPSD'
			}
			case UDP: {
				this._busid = 'UDP'
			}
			case UDPNM: {
				this._busid = 'UDPNM'
			}
			case VERBOSEDLT: {
				this._busid = 'VERBOSEDLT'
			}
		}
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractMessage createAbstractMessage(AbstractMessage message, HashMap<String, Serializable> map){
		//message?.setId(createUUID)
		//message.sourceReference = ''
		_signals?.forEach[
			switch(it?.getSigtyp){
				case NONE:{
					message?.getSignals.add(it?.createAbstractSignal(null, map))
				}
				case COMPARATOR: {
					message?.getSignals.add(it?.createComparatorSignal(map) as AbstractSignal)
				}
				case CONTAINER: {
					//message.getSignals.add(it.createComparatorSignal(map) as AbstractSignal)
				}
				case DOUBLE: {
					message?.getSignals.add(it?.createDoubleSignal(map) as AbstractSignal)
				}
				case HEADER: {
					message?.getSignals.add(it?.createHeaderSignal(map) as AbstractSignal)
				}
				case PLUGIN: {
					message?.getSignals.add(it?.createPluginSignal(map) as AbstractSignal)
				}
				case STRING: {
					message?.getSignals.add(it?.createPluginSignal(map) as AbstractSignal)
				}
			}]
		
		/*_filters.forEach[
			switch(it.getFiltertyp){
				case NONE:{
					message?.getFilters(true).add(it?.createAbstractFilter(null))
				}
				case CAN: {
					message?.getFilters(false).add(it?.createCANFilter(map))
				}
				case LIN: {
					message?.getFilters(false).add(it?.createLINFilter(map))
				}
				case FLEXRAY: {
					message?.getFilters(false).add(it?.createFlexRayFilter(map))
				}
				case ETHERNET: {
					message?.getFilters(false).add(it?.createEthernetFilter(map))
				}
				case IPV4: {
					message?.getFilters(false).add(it?.createIPv4Filter(map))
				}
				case NONVERBOSEDLT: {
					message?.getFilters(false).add(it?.createNonVerboseDLTFilter(map))
				}
				case PLUGIN: {
					message?.getFilters(false).add(it?.createPluginFilter(map))
				}
				case SOMEIP: {
					message?.getFilters(true).add(it?.createSomeIPFilter(map, _interface_ver))
				}
				case SOMEIPSD: {
					message?.getFilters(false).add(it?.createSomeIPSDFilter(map))
				}
				case UDP: {
					message?.getFilters(false).add(it?.createUDPFilter(map))
				}
				case UDPNM: {
					message?.getFilters(false).add(it?.createUDPNMFilter(map))
				}
				case VERBOSEDLT: {
					message?.getFilters(false).add(it?.createVerboseDLTPayloadFilter(map))
				}
			}]*/
		message.name = ''
		message
	}
	
	def AbstractBusMessage createAbstractBusMessage(AbstractBusMessage busmessage, HashMap<String, Serializable> map){
		var setbusmessage = createAbstractMessage(busmessage, map) as AbstractBusMessage
		setbusmessage?.setBusId(this._busid)//restapireqdata.getCon.getbusID
		//setbusmessage.busIdImpParam = ''
		//setbusmessage.busIdRange = ''
		setbusmessage
	}
	
	def CANMessage createCANMessage(HashMap<String, Serializable> map){
		var canmessage = conditions?.createCANMessage
		canmessage = createAbstractBusMessage(canmessage, map) as CANMessage
		//
		canmessage
	}
	
	def LINMessage createLINMessage(HashMap<String, Serializable> map){
		var linmessage = conditions?.createLINMessage
		linmessage = createAbstractBusMessage(linmessage, map) as LINMessage
		//
		linmessage
	}
	
	def FlexRayMessage createFlexRayMessage(HashMap<String, Serializable> map){
		var flexraymessage = conditions?.createFlexRayMessage
		flexraymessage = createAbstractBusMessage(flexraymessage, map) as FlexRayMessage
		//
		flexraymessage
	}
	
	def EthernetMessage createEthernetMessage(HashMap<String, Serializable> map){
		var ethernetmessage = conditions?.createEthernetMessage
		ethernetmessage = createAbstractBusMessage(ethernetmessage, map) as EthernetMessage
		//
		ethernetmessage
	}
	
	def IPv4Message createIPv4Message(HashMap<String, Serializable> map){
		var ipv4message = conditions?.createIPv4Message
		ipv4message = createAbstractBusMessage(ipv4message, map) as IPv4Message
		//
		ipv4message
	}
	
	def NonVerboseDLTMessage createNonVerboseDLTMessage(HashMap<String, Serializable> map){
		var nonverbosedltmessage = conditions?.createNonVerboseDLTMessage
		nonverbosedltmessage = createAbstractBusMessage(nonverbosedltmessage, map) as NonVerboseDLTMessage
		//
		nonverbosedltmessage
	}
	
	def PluginMessage createPluginMessage(HashMap<String, Serializable> map){
		var pluginmessage = conditions?.createPluginMessage
		pluginmessage = createAbstractMessage(pluginmessage, map) as PluginMessage
		//
		pluginmessage
	}
	
	def SomeIPMessage createSomeIPMessage(HashMap<String, Serializable> map){
		var someipmessage = conditions?.createSomeIPMessage
		someipmessage = createAbstractBusMessage(someipmessage, map) as SomeIPMessage
		someipmessage?.setSomeIPFilter(_filters?.head?.createSomeIPFilter(map, this._interface_ver))
		
		someipmessage
	}
	
	def SomeIPSDMessage createSomeIPSDMessage(HashMap<String, Serializable> map){
		var someipsdmessage = conditions?.createSomeIPSDMessage
		someipsdmessage = createAbstractBusMessage(someipsdmessage, map) as SomeIPSDMessage
		//
		someipsdmessage
	}
	
	def TCPMessage createTCPFilter(HashMap<String, Serializable> map){
		var tcpmessage = conditions?.createTCPMessage
		tcpmessage = createAbstractBusMessage(tcpmessage, map) as TCPMessage
		//
		tcpmessage
	}
	
	def UDPMessage createUDPMessage(HashMap<String, Serializable> map){
		var udpmessage = conditions?.createUDPMessage
		udpmessage = createAbstractBusMessage(udpmessage, map) as UDPMessage
		//
		udpmessage
	}
	
	def UDPNMMessage createUDPNMMessage(HashMap<String, Serializable> map){
		var udpnmmessage = conditions?.createUDPNMMessage
		udpnmmessage = createAbstractBusMessage(udpnmmessage, map) as UDPNMMessage
		//
		udpnmmessage
	}
	
	def DLTMessage createDLTMessage(DLTMessage busmessage, HashMap<String, Serializable> map){
		var newbusmessage = createAbstractBusMessage(busmessage, map) as DLTMessage
		newbusmessage
	}
	
	def VerboseDLTMessage createVerboseDLTPayloadMessage(HashMap<String, Serializable> map){
		var verbosedltpayloadmessage = conditions?.createVerboseDLTMessage
		verbosedltpayloadmessage = createDLTMessage(verbosedltpayloadmessage, map) as VerboseDLTMessage
		//
		verbosedltpayloadmessage
	}
}