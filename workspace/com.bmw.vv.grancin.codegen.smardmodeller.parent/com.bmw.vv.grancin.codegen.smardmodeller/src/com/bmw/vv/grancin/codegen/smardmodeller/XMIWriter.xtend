package com.bmw.vv.grancin.codegen.smardmodeller

import java.io.IOException
import java.io.StringWriter
import java.util.HashMap
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.URIConverter
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
//import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import conditions.ConditionsXMIFactoryImpl;
import conditions.ConditionsDocument
import statemachine.StateMachine

class XMIWriter {
	//protected val SmardModellerCreator smResource
	
	/*new(SmardModellerCreator smardmodeller){
		smResource = smardmodeller
	}*/
	
	new(){
	}
	
	/*protected def String WriteConditionsXML(){
		try {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl())
			var resourceSet = new ResourceSetImpl()
			var resource = resourceSet.createResource(URI.createFileURI("../../../../../../../com.bmw.vv.smardmodeller/model/conditions.ecore"))
			resource.getContents().add(smResource.conditionspackage)
			var stringWriter = new StringWriter()
			var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
			var options = new HashMap<String, String>()
			resource.save(outputStream, options)
			stringWriter.toString()
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}*/
	
	protected def String WriteConditionsXML(ConditionsDocument docu){
		try {
			val reg = Resource.Factory.Registry.INSTANCE;
        	var m = reg.getExtensionToFactoryMap();
        	m.put("docu", new ConditionsXMIFactoryImpl());

        	var ResourceSet resSet = new ResourceSetImpl();

        	var resource = resSet.createResource(URI.createURI("conditions.docu"));
        	resource.getContents().add(docu);

			var stringWriter = new StringWriter()
			var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
			var options = new HashMap<String, String>()
			resource.save(outputStream, options)
			stringWriter.toString()
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
	
	/*protected def String WriteStatemachineXML(){
		try {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl())
			var resourceSet = new ResourceSetImpl()
			var resource = resourceSet.createResource(URI.createFileURI("../../../../../../../com.bmw.vv.smardmodeller/model/statemachine.ecore"))
			resource.getContents().add(smResource.statemachinepackage)
			var stringWriter = new StringWriter()
			var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
			var options = new HashMap<String, String>()
			resource.save(outputStream, options)
			stringWriter.toString()
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}*/
	
	protected def String WriteStatemachineXML(StateMachine docu){
		try {
			val reg = Resource.Factory.Registry.INSTANCE;
        	var m = reg.getExtensionToFactoryMap();
        	m.put("docu", new ConditionsXMIFactoryImpl());

        	var ResourceSet resSet = new ResourceSetImpl();

        	var resource = resSet.createResource(URI.createURI("statemachine.docu"));
        	resource.getContents().add(docu);

			var stringWriter = new StringWriter()
			var outputStream = new URIConverter.WriteableOutputStream(stringWriter, "UTF-8")
			var options = new HashMap<String, String>()
			resource.save(outputStream, options)
			stringWriter.toString()
		} 
		catch (IOException ioe) {
			ioe.printStackTrace()
			''
		}
	}
}