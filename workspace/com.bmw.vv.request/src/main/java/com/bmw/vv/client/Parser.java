package com.bmw.vv.client;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

/*class DataAccess {
	private static String ident;
	private static String clazz;
	
	public DataAccess(String id, String clas) {
		ident = id;
		clazz = clas;
	}
	
	public String getid() {
		return this.ident;
	}
	
	public String getclazz() {
		return this.clazz;
	}
}*/

public class Parser {
	
	public static ArrayList<HashMap<String, Serializable>> listdata;
	private static ArrayList<HashMap<String, Serializable>> listadditiondatainterface;
	private static ArrayList<HashMap<String, Serializable>> listadditiondatamethods;
	//public static HashMap<String, String> extractedRules;
	
	private HashMap<String, Serializable> foundele = null;
	private String strData = null;
	private String stradditionalinfointerface = null;
	private String stradditionalinfomethode = null;
	
	public Parser(String response) throws IOException
    {
        JSONObject object = new JSONObject(response);
        JSONObject obj = object.getJSONObject("_embedded");
        JSONArray array = obj.getJSONArray("BnSignals");
        //String strData = obj.toString().replace(obj2.toString(), "");
        listdata = new ArrayList<HashMap<String, Serializable>>();
        for (int i = 0; i < array.length(); i++)
        {
        	HashMap<String, Serializable> map = new HashMap<String, Serializable>();
        	JSONObject element = array.getJSONObject(i);
        	map.put("modelLine",element.getString("modelLine"));
        	map.put("model",element.getString("model"));
        	map.put("integration",element.getString("integration"));
        	map.put("shortName",element.getString("shortName"));
        	map.put("clazz",element.getString("clazz"));
        	map.put("path",element.getString("path"));
        	map.put("bnIdPath",element.getString("bnIdPath"));
        	map.put("extractionRule",element.getString("extractionRule"));
        	map.put("longNameDe",element.getString("longNameDe"));
        	map.put("longNameEn",element.getString("longNameEn"));
        	map.put("busBNID",element.getString("busBNID"));
        	map.put("BNID",element.getString("BNID"));
        	JSONArray arr =element.getJSONArray("ids");
        	ArrayList<HashMap<String, String>> listdataccess = new ArrayList<HashMap<String, String>>();
        	for (int j = 0; j < arr.length(); j++) {
        		JSONObject ele = arr.getJSONObject(j);
        		HashMap<String, String> Hmap = new HashMap<String, String>();
        		//DataAccess dataacc = new DataAccess(ele.getString("ident"), ele.getString("clazz"));
        		Hmap.put("ident", ele.getString("ident"));
        		Hmap.put("clazz", ele.getString("clazz"));
        		listdataccess.add(Hmap);
        	}
        	map.put("ids", listdataccess);
			//HashMap<String, Serializable> finale = new HashMap<String, Serializable>();
			//finale.put(element.getString("path"), map);
        	listdata.add(map);
        }
		//this.extractRules();
    }
	
	/*public void extractRules(){
		extractedRules = new HashMap<String, String>();
		listdata.forEach(element -> {
			if(element.containsKey("path") && element.containsKey("extractionRule"))
				extractedRules.put(element.get("path").toString(), element.get("extractionRule").toString());
		});
	}*/
	
	public void parseServiceInterface(String serviceinterfaceres) {
		JSONObject interfc = new JSONObject(serviceinterfaceres);
		listadditiondatainterface = new ArrayList<HashMap<String, Serializable>>();
		HashMap<String, Serializable> map = new HashMap<String, Serializable>();
		map.put("shortName",interfc.getString("shortName"));
		map.put("serviceId",interfc.getInt("serviceId"));
		listadditiondatainterface.add(map);
	}
	
	public void parseMethods(String methodres) {
		JSONObject methodobj = new JSONObject(methodres);
		listadditiondatamethods = new ArrayList<HashMap<String, Serializable>>();
		HashMap<String, Serializable> map = new HashMap<String, Serializable>();
		map.put("shortName",methodobj.getString("shortName"));
		map.put("methodId",methodobj.getInt("methodId"));
		map.put("messageType",methodobj.getString("messageType"));
		map.put("debounceTime",methodobj.getInt("debounceTime"));
		listadditiondatamethods.add(map);
	}
	
	public void parseMethodsinServiceInterface(String methodres) {
		JSONObject object = new JSONObject(methodres);
		JSONObject obj = object.getJSONObject("_embedded");
		JSONArray array = obj.getJSONArray("Methods");
		listadditiondatamethods = new ArrayList<HashMap<String, Serializable>>();
		for (int i = 0; i < array.length(); i++)
        {
        	HashMap<String, Serializable> map = new HashMap<String, Serializable>();
        	JSONObject element = array.getJSONObject(i);
        	map.put("shortName",element.getString("shortName"));
			map.put("methodId",element.getInt("methodId"));
			map.put("messageType",element.getString("messageType"));
			map.put("debounceTime",element.getInt("debounceTime"));
			listadditiondatamethods.add(map);
		}
	}
	
	protected void searchelement(String shortname) {
		HashMap<String, Serializable> prefoundele = new HashMap<String, Serializable>();
		prefoundele = foundele;
		listdata.forEach(element -> {
			element.forEach((key, value) -> {
				if ((key == "shortName" && value.toString().contains(shortname)) || (key == "path" && value.toString().contains(shortname))) {
					foundele = new HashMap<String, Serializable>();
					foundele = element;
					return;
				}
			});
		});
		if(foundele == prefoundele)
			foundele = null;
		return;
	}
	
	public String search(String shortname, String Data) {
		foundele = null;
		searchelement(shortname);
		if (foundele != null) {
			if(foundele.containsKey(Data))
				return foundele.get(Data).toString();
			else {
				ArrayList<HashMap<String, String>> listids = new ArrayList<HashMap<String, String>>();
				listids = (ArrayList<HashMap<String, String>>) foundele.get("ids");
				listids.forEach(element -> {
					if (element.containsValue(Data))
						strData = element.get("ident").toString();
				});
				return strData;
			}	
		}
		else
			return null;
	}
	
	public String searchAdditionalDataInterface(String shortname, String AdditionalData){
		stradditionalinfointerface = null;
		listadditiondatainterface.forEach(element -> {
			/*element.forEach((key, value) ->{
				if(key.toString() == "shortName" && value.toString().contains(shortname)) {
					stradditionalinfointerface = element.get(AdditionalData).toString();
				}
			});*/
			if(element.containsKey("shortName"))
			{
				if(element.get("shortName").toString().contains(shortname)){
					if(element.containsKey(AdditionalData)){
						stradditionalinfointerface =  element.get(AdditionalData).toString();
					}
				}
			}
		});
		return this.stradditionalinfointerface;
	}
	
	public String searchAdditionalDataMethods(String shortname, String AdditionalData){
		stradditionalinfomethode = null;
		listadditiondatamethods.forEach(element -> {
			if(element.containsKey("shortName"))
			{
				if(element.get("shortName").toString().contains(shortname)){
					if(element.containsKey(AdditionalData)){
						stradditionalinfomethode =  element.get(AdditionalData).toString();
					}
				}
			}
		});
		return this.stradditionalinfomethode;
	}
	
	public ArrayList<HashMap<String, Serializable>> getListData(){
		return listdata;
	}
	
	public ArrayList<HashMap<String, Serializable>> getListAdditionDataInterface(){
		return listadditiondatainterface;
	}
	
	public ArrayList<HashMap<String, Serializable>> getListAdditionDataMethods(){
		return listadditiondatamethods;
	}
	
	/*public HashMap<String, String> getExtractedRules(){
		return extractedRules;
	}*/
}
