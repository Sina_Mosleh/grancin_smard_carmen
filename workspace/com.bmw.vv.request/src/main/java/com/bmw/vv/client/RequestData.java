package com.bmw.vv.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;

public class RequestData
{
	
	private RestApiConnector con;
	public Parser parsed;
	
	
	public RestApiConnector getCon() {
		return this.con;
	}
	
	public void setCon(RestApiConnector conn) {
		this.con = conn; 
	}
	
	public Parser getParsed() {
		return this.parsed;
	}
	
	public void setParsed(Parser parse) {
		this.parsed = parse; 
	}
	 
    @SuppressWarnings({ "unused", "unchecked" })
    public RequestData(String username, String pass, String modelLine, String Model, String integration, int busId) {
    	this.con = new RestApiConnector();
    	con.setusername(username);//?
        con.setpassword(pass);//?
        con.setmodelLine(modelLine);//?
        con.setmodel(Model);//?
        con.setintegration(integration);//?
        con.setbusID(busId);//?
    }
    
    public Parser request() throws IOException
    {
        //con.setmodelLine("SP2021");//?
		con.requestToken();
		//String ModelLine = con.request("ModelLines", "accessToken" , "");
		String ModelLine = con.requestModelLines();
        //String modelLineident = parser();//?
		String modelLineident = "5d5e9b6ef695510001141fa7";
		
		//con.setmodel("SP2021");//?
		con.requestToken();
		//String ModelLine = con.request("Models", modelLineident. "");
		String Model = con.requestModels(modelLineident);
		//String modelident = parser();//?
		String modelident = "5d5e9b6ef695510001141f9d";
		
		//con.setintegration("21-07-240");//?
		con.requestToken();
		//String Integration = con.request("FileNames", modelident, "");
		String Integration = con.requestFileNames(modelident);
		//String fileident = parser();//?
		String fileident = "5d5f1d84f695510001464a31";
		
		//con.setbusID(45000);//?
		con.requestToken();
		//String BusBNID = con.request("Buses", fileident, "");
		String BusBNID = con.requestBuses(fileident);
		//String busbnident = parser();//?
		String busbnident = "_9EFBCFCBF21C490AA5705F6FF3E3F085";
		
		//con.requestToken();
		//String data_temp = con.request("Data", busbnident, "DriverSelectionModule");
		con.requestToken();
		String data = con.requestData("DriverSelectionModule", busbnident);
		parsed = new Parser(data);
		
		parsed.getListData().forEach(element -> {
			if(element.containsKey("ids")) {
				ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
				list = (ArrayList<HashMap<String, String>>) element.get("ids");
				list.forEach(ele -> {
					if(ele.containsKey("clazz")){
						if(ele.get("clazz").toString().contains("ServiceInterface")){
							con.requestToken();
							String serviceInterfaceResponse = con.requestServiceInterface(ele.get("ident").toString());
							parsed.parseServiceInterface(serviceInterfaceResponse);
							String serviceID = parsed.searchAdditionalDataInterface("DriverSelectionModule", "serviceId");
							element.put("serviceId", serviceID);
						}
						if(ele.get("clazz").toString().contains("Method")){
							con.requestToken();
							String methodResponse = con.requestMethod(ele.get("ident").toString());
							parsed.parseMethods(methodResponse);
							String methodID = parsed.searchAdditionalDataMethods("setEntryInMappingTable", "methodId");
							element.put("methodId", methodID);
						}
						if(ele.get("clazz").toString().contains("Member")){
							con.requestToken();
							String memberResponse = con.requestMember(ele.get("ident").toString(), "");
							String utilizationResponse = con.requestMember(ele.get("ident").toString(), "utilization");
							//parsed.parseUtilization(utilizationResponse);
							//String factor = parsed.searchAdditionalDataUtil("setEntryInMappingTable", "methodId");
							//String offset = parsed.searchAdditionalDataUtil("setEntryInMappingTable", "methodId");
							//element.put("factor", factor);
							//element.put("offset", offset);
							
							
							//String memberResponse1 = con.requestMember(ele.get("ident").toString(), "dataType");
							//String memberResponse2 = con.requestMember(ele.get("ident").toString(), "signal");
						}
					}
				});
			}
			System.out.println(element.toString());
		});
		return parsed;
		
		
		///String extractionRule = parsed.search("PiaProfileId", "extractionRule");
        // String serviceidident = parsed.search("PiaProfileId", "ServiceInterface");
        // String methodidident = parsed.search("setEntryInMappingTable", "Method");
        
        ///con.requestToken();
        ///String ServiceId = con.request("ServiceID", serviceidident, "");
        // con.requestToken();
        // String serviceInterfaceResponse = con.requestServiceInterface(serviceidident);
        // parsed.parseServiceInterface(serviceInterfaceResponse);
		// String serviceID = parsed.searchAdditionalDataInterface("DriverSelectionModule", "serviceId");
        
        ///con.requestToken();
        ///String MethodId = con.request("MethodID", methodidident, "");
        // con.requestToken();
        // String methodResponse = con.requestMethod(methodidident);
        // parsed.parseMethods(methodResponse);
		// String methodID = parsed.searchAdditionalDataMethods("setEntryInMappingTable", "methodId");
		
		
		
		
		
		
		
        
		/*con.requestToken();
        String modelLines = con.requestModelLines();
		
		con.requestToken();
        String models = con.requestModels();
		
		con.requestToken();
		String fileNames = con.requestFileNames();
        
        con.requestToken();
        String buses = con.requestBuses();
		
		con.requestToken();
        String data = con.requestData("DriverSelectionModule");
        Parser parsed = new Parser(data);
        String extraction_rule = parsed.search("PiaId", "ServiceInterface");
		
        con.requestToken();
        String method = con.requestMethod();
		
		con.requestToken();
        String member = con.requestMember();
        
        con.requestToken();
        String serviceID = con.requestServiceID();
        
		con.requestToken();
        String methodID = con.requestMethodID();
        
        con.requestToken();
        String memberID = con.requestMemberID();
        
		con.requestToken();
        String factor = con.requestFactor();
        
		con.requestToken();
        String offset = con.requestOffset();*/

        //con.send("/ServiceInterfaces/5d5e9d07f6955100011662a6");

    }
}
