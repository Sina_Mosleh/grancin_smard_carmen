package com.bmw.vv.clientTest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.bmw.vv.client.RestApiConnector;


public class TestApiConnector
{

    @Test
    public void testRequestData()
    {
        RestApiConnector con = new RestApiConnector();
        //con.requestToken();
        //String data = con.requestData("EDR_ID_38_ST_WWA");
        String id = con.getmodelLine();
        assertEquals(null, id);
    }

    @Test
    public void testRequestServiceID()
    {
        RestApiConnector con = new RestApiConnector();
        //con.requestToken();
        //String id = con.requestServiceID();
        String id = con.getmodelLine();
        assertEquals(null, id);
    }

    @Test
    public void testRequestMethodID()
    {
        RestApiConnector con = new RestApiConnector();
        //con.requestToken();
        //String id = con.requestMethodID();
        String id = con.getmodelLine();
        assertEquals(null, id);
    }

    @Test
    public void testRequestFactor()
    {
        RestApiConnector con = new RestApiConnector();
        //con.requestToken();
        //String id = con.requestFactor();
        String id = con.getmodelLine();
        assertEquals(null, id);
    }

    @Test
    public void testRequestOffset()
    {
        RestApiConnector con = new RestApiConnector();
        //con.requestToken();
        //String id = con.requestOffset();
        String id = con.getmodelLine();
        assertEquals(null, id);
    }
}
