package com.bmw.vv.generator

import com.bmw.vv.fsm.FsmFactory

interface StatechartGenerator {
	val DISPATCH_STATE_ID = "dispatch"
  val FsmFactory factory = FsmFactory.eINSTANCE  
  
  def FsmGeneration.GenStrategy isWhichGenStrategy()
  def com.bmw.vv.fsm.Statechart generate()
}