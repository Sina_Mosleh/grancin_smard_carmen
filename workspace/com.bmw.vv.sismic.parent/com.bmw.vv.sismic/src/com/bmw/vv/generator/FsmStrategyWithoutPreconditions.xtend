package com.bmw.vv.generator

import com.bmw.vv.fsm.FsmFactory
import com.bmw.vv.fsm.State
import java.util.Arrays
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.function.Consumer
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import com.bmw.vv.gherkin.TableRows
import com.bmw.vv.sismic.Reference
import com.bmw.vv.sismic.SendEvent
import com.bmw.vv.sismic.StepInBody
import com.bmw.vv.sismic.TableCells

import static extension org.eclipse.emf.ecore.util.EcoreUtil.*

class FsmStrategyWithoutPreconditionsProvider extends FsmStrategyWithoutPreconditions {
  protected new(Resource resource) {
    super(resource)
  }
}

abstract class FsmStrategyWithoutPreconditions extends FsmGeneration implements StatechartGenerator{
  protected new(Resource resource) {
    super(resource)
  }

  override isWhichGenStrategy() {
    GenStrategy.WITHOUT_PRECONDITIONS
  }
	
	override generate() {
		val statechart = initStatechart(factory, feature) 
		loadDescription(statechart,feature)		
		
		FsmGeneration.identifyStatesFromThenSteps(factory,feature,statechart)
		FsmGeneration.addDispatchAsDedicatedState(factory, feature, statechart)
		
		FsmGeneration.defineInitState(feature, statechart)
		evaluateAcceptedEvents(feature, statechart)
		evaluateTransitions(factory, feature, statechart)
		FsmGeneration.definePreamble(factory, feature, statechart)
		statechart
	}
	
	private def evaluateAcceptedEvents(Resource feature, com.bmw.vv.fsm.Statechart sc) {
		val eMap = new HashMap<String, EList<String>>()
		sc.acceptedEvents = eMap
		
		feature.allContents.toIterable.filter(StepInBody).map[ type ]?.forEach[
			switch it {
				SendEvent: eMap.addEventAndParameter(it.event, it.payload.parameter)
			}
		]
	}
	
	private def void addParameterAndValues(Map<String, EList<String>> m, String parameter, EList<String> values) {
		if(parameter !== null)
			m.put(parameter, values)
	}
	
	private def EList<String> references2String(Reference r, Resource feature) {
		if(r === null) 
			return null
		
		val listOfValues = new BasicEList<String>()
		
		if(r.v !== null) 
			listOfValues.add(r.v.value2String)
		
		if(r.ref !== null) {
			val allParamsInThisScenario = 
				feature.allContents.toIterable
				.filter(TableRows)
				.findFirst[ r.whichScenario.equals(header.whichScenario) ] 
	
			val parameterList  = allParamsInThisScenario.header.cells.name
			val parameterIndex = parameterList.indexOf(r.ref) 
			
			if(parameterIndex < 0)
				throw new IllegalArgumentException("There is no correspondence in the example table for parameter <" + r.ref + ">")
			
			val List<String> allRowsOfGivenColumn = 
				allParamsInThisScenario
				.rows
				.map[ it.cells as EObject]
				.map[ it as TableCells ]
				.map[ it.cell.get(parameterIndex) ]
				.map[ value2String ]
  		
  		listOfValues.addAll(allRowsOfGivenColumn)
  	}		
		return listOfValues
	}
	
	private def evaluateTransitions(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
		var prevScenarioID  = "<invalid>" 
		var Map<String, EList<String>> tMap = null
		for (sib : feature.allContents.toIterable.filter(StepInBody)) {
			val t = sib.type
			val stepType   = sib.whichStepID
			val scenarioID = sib.whichScenario
			
			if(!prevScenarioID.equals(stepType)) {
				tMap = new HashMap<String, EList<String>>()
				prevScenarioID = stepType
				}
			
			if(t !== null) { 
				switch t {
					SendEvent: tMap.addParameterAndValues(t?.payload.parameter, t?.payload.value.references2String(feature))
					default: {println("THIS PRINT SHOULD NOT BE DISPLAYED")}
				}
			}
			if (tMap.size > 0) {
				val transition = sc.transitions.filter[ origin.equals(stepType) ].head
				transition?.trigger?.putAll(tMap)

				if(transition === null) {
					val states = sc.states
					val State targetState = switch states {
						case states?.findFirst[ name.contains(scenarioID) ] !== null : states?.findFirst[ name.contains(scenarioID) ]   
						default: states?.findFirst[ foldedScenarios.findFirst[it.contains(scenarioID)] !== null ]   
					}
					val newT = factory.createTransition
					newT.trigger = tMap
					newT.origin = stepType
					newT.target = targetState
					sc.transitions.add(newT)
				} 
			}	
		}
		duplicateTransitions(sc)
	}
	
	private def duplicateTransitions(com.bmw.vv.fsm.Statechart sc) {
		val transitHere = new Consumer<State>() {
		    override accept(State s) {
		        val transitions = sc.transitions.clone
		        for(t : transitions) {
		    		if(!equals(s,t.target)) {
		    			if (t.source === null)
		    				t.source = s
		    			else {
							val newTransition = t.copy
							sc.transitions.add(newTransition)
							newTransition.source = s
		    			}
		    		}     	
		        }
		    }
		}
		sc.states.forEach(transitHere)
	}

	private static def void addEventAndParameter(Map<String, EList<String>> m, String event, String parameter) {
		if (m.containsKey(event)) {
			if(!m.get(event).contains(parameter))
				m.get(event).add(parameter)
		} else {
			m.put(event, new BasicEList(Arrays.asList(parameter)))
		}
	}
}