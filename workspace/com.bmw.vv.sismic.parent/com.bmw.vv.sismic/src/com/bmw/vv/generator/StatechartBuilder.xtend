package com.bmw.vv.generator

import com.bmw.vv.fsm.FsmFactory
import com.bmw.vv.fsm.FsmPackage
import com.bmw.vv.fsm.OnEntry
import com.bmw.vv.fsm.State
import com.bmw.vv.fsm.impl.OnEntryImpl
import com.bmw.vv.fsm.impl.SendEventImpl
import com.bmw.vv.fsm.impl.StatechartImpl
//import com.bmw.vv.resource.InMemoryResourceFactoryImpl
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.Iterator
import java.util.List
import java.util.Map
import java.util.TreeMap
import java.util.function.Consumer
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import com.bmw.vv.gherkin.AbstractScenario
import com.bmw.vv.gherkin.Feature
import com.bmw.vv.gherkin.GivenStep
import com.bmw.vv.gherkin.TableRows
import com.bmw.vv.gherkin.WhenStep
import com.bmw.vv.sismic.BoolConstant
import com.bmw.vv.sismic.EventIsFired
import com.bmw.vv.sismic.FloatConstant
import com.bmw.vv.sismic.IntConstant
import com.bmw.vv.sismic.NotANumber
import com.bmw.vv.sismic.Reference
import com.bmw.vv.sismic.SendEvent
import com.bmw.vv.sismic.StepInBody
import com.bmw.vv.sismic.StepOutBody
import com.bmw.vv.sismic.StringConstant
import com.bmw.vv.sismic.TableCells
import com.bmw.vv.sismic.Value

//__CB__
import org.eclipse.xtext.resource.XtextResourceSet
import com.google.inject.Inject
import com.google.inject.Provider

import static extension org.eclipse.emf.ecore.util.EcoreUtil.*
import static extension org.eclipse.xtext.EcoreUtil2.*

class StatechartBuilder {
	@Inject Provider<XtextResourceSet> resourceSetProvider
	
	var GenStrategy gs = GenStrategy.UNKNOWN
	val Resource fsm
	val Resource feature
	val FsmFactory factory 	= FsmFactory.eINSTANCE
	
	static enum GenStrategy {
		INVALID,
		UNKNOWN,
		TRIVIAL,
		WITHOUT_PRECONDITIONS
	}
	
	new(Resource f){
		feature = f
		fsm = createResource()
		
		if(feature === null) 
			throw new IllegalArgumentException("The resource representing the feature must not be empty")
			
		if(fsm === null)
			throw new IllegalArgumentException("The resource representing the statemachine must not be empty")
	}
	
	def buildStatechart() {
		gs = whichGenStrategy
		val statechart = initStatechart 
		loadDescription(statechart)
		evaluateFinalStates(statechart)
		defineInitState(statechart)
		evaluateAcceptedEvents(statechart)
		evaluateTransitions(statechart)
		definePreamble(statechart)
		
		printStates(statechart)
		printTransitions(statechart)
		statechart
	}
	
	private def Resource createResource() {
		val resourceSet = resourceSetProvider.get
		val resource	= resourceSet.createResource(URI.createURI("in_memory"))
		//__CB__ !?
		//val resourceFactory = new InMemoryResourceFactoryImpl
		//val resource = resourceFactory.createResource(URI.createURI("memory"))
		resource
	}
	
	private def com.bmw.vv.fsm.Statechart initStatechart() {
		val statechart = factory.createStatechart
		fsm.contents.add(statechart)
		statechart.name 		= feature.allContents.toIterable.filter(Feature).take(1).map[ title.name ].join.toString
		statechart
	}
	
	def GenStrategy whichGenStrategy() {
		if(gs != GenStrategy.UNKNOWN)
			return gs
		
		print("\nEvaluation of generation strategy .. ")
		val i = feature.allContents.toList.filter(GivenStep)
		if (i.empty) {
			println("selected generation strategy is " + GenStrategy.WITHOUT_PRECONDITIONS.toString)
			return GenStrategy.WITHOUT_PRECONDITIONS
		} else {
			throw new IllegalStateException("For this kind of text input there is no generation strategy available.")
		} 
	}
	
	def loadDescription(com.bmw.vv.fsm.Statechart sc) {
		sc.description = new BasicEList<String>()
		val f = feature.allContents.toList.filter(Feature).map[ elements ].flatten
		f.forEach[ 
			sc.description
			.add(NodeModelUtils.getTokenText(NodeModelUtils.getNode(it)).replace("\n", " ").replace("\r", ""))
		]
	}
	
	def String value2String(Value v) {
		if(v === null) return null
		
		val type = v.type
		switch type {
		    IntConstant : 	type.const.toString
		    FloatConstant : type.const.toString
		    BoolConstant: 	type.const.toString
		    StringConstant: '"' + type.const + '"'
		    NotANumber: 	type.const
		    default : "<unknown type>"
  		}		
	}
	
	def String whichScenario(EObject eo){
			eo.getContainerOfType(AbstractScenario).URIFragment
	}
		
	def EList<String> references2String(Reference r) {
		if(r === null) 
			return null
		
		val listOfValues = new BasicEList<String>()
		
		if(r.v !== null) 
			listOfValues.add(r.v.value2String)
		
		if(r.ref !== null) {
			val allParamsInThisScenario = 
				feature.allContents.toIterable
				.filter(TableRows)
				.findFirst[ r.whichScenario.equals(header.whichScenario) ] 
	
			val parameterList  = allParamsInThisScenario.header.cells.name
			val parameterIndex = parameterList.indexOf(r.ref) 
			
			if(parameterIndex < 0)
				throw new IllegalArgumentException("There is no correspondence in the example table for parameter <" + r.ref + ">")
			
			val List<String> allRowsOfGivenColumn = 
				allParamsInThisScenario
				.rows
				.map[ it.cells as EObject]
				.map[ it as TableCells ]
				.map[ it.cell.get(parameterIndex) ]
				.map[ value2String ]
  		
  		listOfValues.addAll(allRowsOfGivenColumn)
  	}		
		return listOfValues
	}
	
	def eventIsFired2SendEvent(EventIsFired e) {
		val sendEvent 	= factory.createSendEvent
		sendEvent.event = e.event
		
		// TODO: a definition of multiple values in form of a 'Given table' is currently not supported 
		val m = newHashMap( e.payload.parameter  -> value2String(e?.payload.value?.v) )
		(sendEvent as SendEventImpl).eSet(FsmPackage.SEND_EVENT__PAYLOAD,m)
		sendEvent		
	} 
	
	def List<State> foldStates(TreeMap<String, OnEntryImpl> m) {
		val List<State> foldedStates = new ArrayList<State>();
		val List<OnEntry> foundOEs = new ArrayList<OnEntry>();
		
		while(m.size > 0) {
			val EList<String> foundSCs = new BasicEList<String>();
			foundOEs.clear
			foundSCs.clear
			
			val lastItem = m.pollLastEntry
			val Iterator<Map.Entry<String, OnEntryImpl>> it = m.entrySet().iterator();
			while (it.hasNext()) {
				val current			= it.next()
				val corrspdSC 	= current.key
				val currentOE 	= current.value
				if (equals(currentOE, lastItem.value)) {
					foundOEs.add(currentOE)
					foundSCs.add(corrspdSC)
				}
			}
			
			for(e : foundSCs) {
				if(m.containsKey(e))
					m.remove(e)
			}
			
			val newState = factory.createState 
			newState.name = lastItem.key
			newState.foldedScenarios = foundSCs
			newState.on_entry = lastItem.value
			 
			foldedStates.add(newState)
		}
		foldedStates
	}
	
	private def evaluateFinalStates(com.bmw.vv.fsm.Statechart sc) {
		val stateMap = new TreeMap<String, OnEntryImpl>();
		
		for (sob : feature.allContents.toIterable.filter(StepOutBody)) {
			val com.bmw.vv.fsm.SendEvent sendEvent = sob.type.eventIsFired2SendEvent
			val scenarioID 	= sob.whichScenario
			
			if(stateMap.containsKey(scenarioID)) {
				stateMap.get(scenarioID).operations.forEach[
					switch it {
						com.bmw.vv.fsm.SendEvent case it.event.equals(sendEvent.event) : 
							(it as com.bmw.vv.fsm.SendEvent).payload.putAll(sendEvent.payload)
							
						com.bmw.vv.fsm.SendEvent case !it.event.equals(sendEvent.event):
							stateMap.get(scenarioID).operations.add(sendEvent)
					}
				]
			} else {
				val oe = factory.createOnEntry as OnEntryImpl
				oe.eSet(FsmPackage.ON_ENTRY__OPERATIONS, Arrays.asList(sendEvent))
				stateMap.put(scenarioID, oe)
			}
		}
		
		val List<State> l = foldStates(stateMap)
		(sc as StatechartImpl).eSet(FsmPackage.STATECHART__STATES, l)
	}
	
	def void addEventAndParameter(Map<String, EList<String>> m, String event, String parameter) {
		if (m.containsKey(event)) {
			if(!m.get(event).contains(parameter))
				m.get(event).add(parameter)
		} else {
			m.put(event, new BasicEList(Arrays.asList(parameter)))
		}
	}
	
	def void addParameterAndValues(Map<String, EList<String>> m, String parameter, EList<String> values) {
		if(parameter !== null)
			m.put(parameter, values)
	}
	
	def evaluateAcceptedEvents(com.bmw.vv.fsm.Statechart sc) {
		val eMap = new HashMap<String, EList<String>>()
		sc.acceptedEvents = eMap
		
		feature.allContents.toIterable.filter(StepInBody).map[ type ]?.forEach[
			switch it {
				SendEvent: eMap.addEventAndParameter(it.event, it.payload.parameter)
			}
		]
	}
		
	def duplicateTransitions(com.bmw.vv.fsm.Statechart sc) {
		val transitHere = new Consumer<State>() {
		    override accept(State s) {
		        val transitions = sc.transitions.clone
		        for(t : transitions) {
		    		if(!equals(s,t.target)) {
		    			if (t.source === null)
		    				t.source = s
		    			else {
							val newTransition = t.copy
							sc.transitions.add(newTransition)
							newTransition.source = s
		    			}
		    		}     	
		        }
		    }
		}
		sc.states.forEach(transitHere)
	}
		
	def evaluateTransitions(com.bmw.vv.fsm.Statechart sc) {
		var prevScenarioID  = "<invalid>" 
		var Map<String, EList<String>> tMap = null
		for (sib : feature.allContents.toIterable.filter(StepInBody)) {
			val t = sib.type
			val scenarioID = sib.whichScenario
			
			if(!prevScenarioID.equals(scenarioID)) {
				tMap = new HashMap<String, EList<String>>()
				prevScenarioID = scenarioID
				}
			
			if(t !== null) { 
				switch t {
					SendEvent: tMap.addParameterAndValues(t?.payload.parameter, t?.payload.value.references2String)
					default: {println("THIS PRINT SHOULD NOT BE DISPLAYED")}
				}
			}
			if (tMap.size > 0) {
				val transition = sc.transitions.filter[ origin.equals(scenarioID) ].head
				transition?.trigger?.putAll(tMap)

				if(transition === null) {
					val states = sc.states
					val State targetState = switch states {
						case states?.findFirst[ name.equals(scenarioID) ] !== null : states?.findFirst[ name.equals(scenarioID) ]   
						default: states?.findFirst[ foldedScenarios.contains(scenarioID) ]   
					}
					val newT = factory.createTransition
					newT.trigger = tMap
					newT.origin = scenarioID
					newT.target = targetState
					sc.transitions.add(newT)
				} 
			}	
		}
		duplicateTransitions(sc)
	}
	
	def StepInBody whichStepIsInit(Iterable<StepInBody> isb) {
		isb.filter[ (it.eContainer.class == WhenStep) && (it.eContainer.getSiblingsOfType(GivenStep).empty) ].head
	}
	
	def State defineInitState(com.bmw.vv.fsm.Statechart sc) {
		if(sc.initState !== null)
			return sc.initState
			
		if(sc.states.size < 1)
			throw new IllegalStateException("States must be evaluated before this method is called")
			
		val doNothingSteps 	= feature.allContents.toIterable.filter(StepInBody).filter[ stub !== null ]
		val initStep 		= if(whichGenStrategy==GenStrategy.WITHOUT_PRECONDITIONS) doNothingSteps.head else whichStepIsInit(doNothingSteps)
		val stateOfInitStep = sc.states.findFirst[ name.equals(initStep.whichScenario) || (foldedScenarios.contains(initStep.whichScenario)) ]
		sc.initState 		= stateOfInitStep 
		return stateOfInitStep
	}
	
	def definePreamble(com.bmw.vv.fsm.Statechart sc) {
		val stateOfInitStep = defineInitState(sc)  
		val m = new HashMap<String,EList<String>>()
		val allParameters  = sc.transitions.map[ trigger.filter[k, v | v.length == 1] ]
		allParameters.forEach[ m.putAll(it) ]
		
		val initParameters = sc.transitions.filter[ target == stateOfInitStep ].map[ trigger.filter[k, v | v.length == 1] ]
		initParameters.forEach[ m.putAll(it) ]
		
		val p = factory.createPreamble 
		sc.preamble = p
		
		m.forEach[ k, v| 
			val assignment = factory.createAssignment
			assignment.leftSide  = k
			assignment.operand   = "="
			assignment.rightSide = v.head
			p.operations.add(assignment)
		]
	}
	
	def printTransitions(com.bmw.vv.fsm.Statechart sc) {
		println("\nAll Transitions")
		sc.transitions.forEach[ println("Origin: " + origin + ", Source: " + source.name + ", Target: " + target.name) ]
	}
	
	def printStates(com.bmw.vv.fsm.Statechart sc) {
		println("\nAll States")
		sc.states.forEach[ println( "name:\t" + name ) foldedScenarios.forEach[ println("folded:\t" + it) ] println() ]	
	}
}