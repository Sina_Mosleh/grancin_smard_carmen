package com.bmw.vv.generator

import org.eclipse.emf.ecore.resource.Resource

class FsmStrategyWithPreconditionsProvider extends FsmStrategyWithPreconditions {
    protected new(Resource resource) {
    super(resource)
  }
}

abstract class FsmStrategyWithPreconditions extends FsmGeneration implements StatechartGenerator{

  protected new(Resource resource) {
    super(resource)
  }

  override isWhichGenStrategy() {
    GenStrategy.WITH_PRECONDITIONS
  }

/*
 * STATUS QUO
 * in FsmGeneration.whichScenario
 *  - eo.getContainerOfType(StepType).URIFragment
 *  - daraufhin gehen einige Tests vom Typ Intermediate nicht mehr
 *  - Rationale: Es reicht nicht mehr, wenn states ihren Namen aus dem Scenario ableiten,
 *    da es mit Given, When and Then m�glich ist, dass es mehrere States pro Scenario gibt
 * 
 * Voraussetzung:
 *    -  Fuer alle Steps eines gegebenen Scenarios muss die Reihenfolge bekannt sein.
 * 
 * Zustand "Dispatch" verweist auf alle Scenarios, die in Zust�nde resultieren
 * Zustand "Dispatch" ist nicht notwendiger Weise der Initialzustand
 * 
 * Initialzustand wird das Scenario, bei dem ohne weitere Bedingungen ein Then Step ausgef�hrt wird, beispielsweise
 * # keine 'Given' step 
 * When I do nothing
 * Then I send event e
 * 
 * Given I do nothing
 * # kein 'When' step
 * Then I send event e
 * 
 * Gibt es kein solches Szenario, dann ist Zustand "Dispatch" der Initialzustand
 */

	
	override generate() {
		
		val statechart = initStatechart(factory, feature) 
		loadDescription(statechart,feature)		

   	FsmGeneration.identifyStatesFromThenSteps(factory,feature,statechart)
	  FsmGeneration.identifyStatesFromGivenSteps(factory,feature,statechart)
    FsmGeneration.addDispatchAsDedicatedState(factory, feature, statechart)
   
    FsmGeneration.defineInitState(feature, statechart)
    
		// TODO evaluateAcceptedEvents hat hier keinen globalen Charakter, sondern muss f�r jeden Zustand evaluiert werden
//		evaluateAcceptedEvents(feature, statechart)

		// TODO evaluateTransitions, same or different than in WITHOUT_PRECONDITIONS
//		evaluateTransitions(factory, feature, statechart)

		// TODO definePreamble, same or different than in WITHOUT_PRECONDITIONS
//		definePreamble(factory, feature, statechart)

		statechart
	}
  
//  private def State defineInitState(fsm.Statechart sc) {
//  	val initStateCandidate 	= FsmGeneration.defineInitState(feature, sc)
//     
//		sc.initState = switch initStateCandidate {
//			case  initStateCandidate !== null: 																																	initStateCandidate 
//     	case initStateCandidate === null && (sc.states.findFirst[name.equals(DISPATCH_STATE_ID)] !== null): sc.states.findFirst[name.equals(DISPATCH_STATE_ID)]
//     	case initStateCandidate === null && (sc.states.findFirst[name.equals(DISPATCH_STATE_ID)] === null): sc.states.head
//     	}
//    sc.initState
//  }

//	private def void addDispatchState(fsm.Statechart sc) {
//    if(sc.states === null || sc.states.size < 2)
//      return 
//      
//    val dispatch = factory.createState
//    dispatch.name = DISPATCH_STATE_ID
//    sc.states.add(dispatch)	  
//	}
}