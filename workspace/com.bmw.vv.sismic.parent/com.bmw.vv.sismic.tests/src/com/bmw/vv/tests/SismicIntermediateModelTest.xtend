
package com.bmw.vv.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import com.bmw.vv.fsm.Statechart
import com.bmw.vv.generator.FsmGeneration
import com.bmw.vv.generator.StatechartGenerator
import com.bmw.vv.sismic.File

@ExtendWith(InjectionExtension)
@InjectWith(SismicInjectorProvider)
class SismicIntermediateModelTest implements completeScenarioWithFrancaReferences {
	@Inject ParseHelper<File> parseHelper
   
	@Test
	def void titleOnly() {
		val model = parseHelper.parse(justAFeatureTitle.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 0)
		Assertions.assertEquals(sc.name,"ApprovalUnit")
	}
	
	@Test
	def void titleAndDescription() {
		val model = parseHelper.parse(
		'''
		@bla @blaBla
		Feature: ApprovalUnit
		Some description is informative''')		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 0)
		Assertions.assertTrue(sc.name.equals("ApprovalUnit"))
		Assertions.assertTrue(sc.description.length == 1)
		Assertions.assertEquals(sc.description.head,"Some description is informative")
	}

	@Test
	def void oneScenarioOnly() {
		val model = parseHelper.parse(oneScenarioWithOneGiven.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 0)
	}
	
	@Test
	def void oneScenarioResultingToInitState() {
		val model = parseHelper.parse(oneScenarioResultingToInitState.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 1)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.acceptedEvents.size == 0)
		Assertions.assertTrue(sc.preamble.operations.empty)
	}
	
	@Test
	def void twoScenariosResultingToInitState() {
		val model = parseHelper.parse(twoScenariosResultingToInitState.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 2)
		Assertions.assertTrue(sc.states.head.foldedScenarios.size == 1)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.acceptedEvents.size == 1)
		Assertions.assertTrue(sc.preamble.operations.size == 1)
	}
	
	@Test
	def void featureWithOneScenarioWithoutDoNothing() {
		val model = parseHelper.parse(completeScenarioWithFrancaReferences.featureWithOneScenarioWithoutDoNothing.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 2)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
	}
	
	@Test
	def void featureWithOneScenarioWithDoNothing() {
		val model = parseHelper.parse(completeScenarioWithFrancaReferences.featureWithOneScenarioWithDoNothing.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 2)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
	}
	
	@Test
	def void featureWithOneScenarioAlsoWithDoNothing() {
		val model = parseHelper.parse(completeScenarioWithFrancaReferences.featureWithOneScenarioAlsoWithDoNothing.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 3)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
	}
	
	@Test
	def void featureWithOneScenarioWithTwoDoNothing() {
		val model = parseHelper.parse(completeScenarioWithFrancaReferences.featureWithOneScenarioWithTwoDoNothing.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 1)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(sc.states.head.name))
	}
	
	@Test
	def void featureWithTwoScenariosWithoutDoNothing() {
		val model = parseHelper.parse(featureWithTwoScenariosWithoutDoNothing.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		FsmGeneration.printStates(sc)
		Assertions.assertTrue(sc.states.length == 3)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
	}
	
	@Test
	def void featureWithOnlyAThenStep() {
		val model = parseHelper.parse(featureWithOnlyAThenStep.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		FsmGeneration.printStates(sc)
		Assertions.assertTrue(sc.states.length == 1)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(sc.states.head.name))
	}
	
	@Test
	def void featureWithOnlyAGivenStep() {
		val model = parseHelper.parse(featureWithOnlyAGivenStep.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		FsmGeneration.printStates(sc)
		Assertions.assertTrue(sc.states.length == 0)
		Assertions.assertNull(sc.initState)
	}
	
	@Test
	def void featureWithOneScenarioGivenWhenThen() {
		val model = parseHelper.parse(featureWithOneScenarioGivenWhenThen.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 3)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
	}
	
	@Test
	def void featureWithGiven() {
		val model = parseHelper.parse(featureWithGiven.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 4)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.states.map[name].findFirst[ equals(StatechartGenerator.DISPATCH_STATE_ID)] !== null)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
//		Assertions.assertTrue(sc.acceptedEvents.size == 5)
//		Assertions.assertTrue(sc.preamble.operations.size == 3)
	}
	
	@Test
	def void featureWithGivenAndSimpleWhen() {
		val model = parseHelper.parse(featureWithGivenAndComplexWhen.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 4)
		Assertions.assertNotNull(sc.initState)
		Assertions.assertTrue(sc.initState.name.equals(StatechartGenerator.DISPATCH_STATE_ID))
//		Assertions.assertTrue(sc.acceptedEvents.size == 5)
//		Assertions.assertTrue(sc.preamble.operations.size == 3)
	}
	
	@Test
	def void featureWithGivenAndComplexWhen() {
		val model = parseHelper.parse(featureWithGivenAndComplexWhen.key)		
		val Statechart sc = FsmGeneration.createGenerator(model.eResource).generate
		Assertions.assertTrue(sc.states.length == 4)
		Assertions.assertNotNull(sc.initState)
//		Assertions.assertTrue(sc.acceptedEvents.size == 5)
//		Assertions.assertTrue(sc.preamble.operations.size == 3)
	}
}
