package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.AssigContainer
import chrisna.ChrisnaFSM
import chrisna.CondContainer
import chrisna.Counter
import chrisna.EventContainer
import chrisna.Filter
import chrisna.HistoryDispach
import chrisna.NodeFinder
import chrisna.Periodically
import chrisna.ScenContainer
import chrisna.ScenContainerWithTime
import chrisna.ScenContainerWithoutTime
import chrisna.Scenario
import chrisna.SendEvent
import chrisna.WarningError
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.initState
import chrisna.state
import chrisna.stateContainer
import com.bmw.vv.gherkin.AbstractScenario
import com.bmw.vv.gherkin.AndInputStep
import com.bmw.vv.gherkin.AndOutputStep
import com.bmw.vv.gherkin.Feature
import com.bmw.vv.gherkin.GivenStep
import com.bmw.vv.gherkin.ScenarioOutline
import com.bmw.vv.gherkin.ThenStep
import com.bmw.vv.gherkin.WhenStep
import com.bmw.vv.gherkin.impl.GherkinFactoryImpl
import com.bmw.vv.gherkin.impl.GivenStepImpl
import com.bmw.vv.gherkin.impl.ScenarioImpl
import com.bmw.vv.gherkin.impl.ScenarioOutlineImpl
import com.bmw.vv.gherkin.impl.ThenStepImpl
import com.bmw.vv.gherkin.impl.WhenStepImpl
import com.bmw.vv.grancin.StepInBody
import com.bmw.vv.grancin.StepOutBody
import com.bmw.vv.grancin.TimeWindow
import com.bmw.vv.grancin.impl.TableCellsImpl
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.common.util.BasicEList

class ChrisnaFSMGenerator extends BaseTranslator {
	
	static final HashMap<String, String> NoTable = null
	protected var String name
	protected var ScenContainer scenariocontainer
	protected var ScenContainerWithTime scencontainerwithtime
	protected var state initst
	protected var ScenContainerWithoutTime scencontainerwithouttime
	protected var HistoryDispach historydispach
	protected var Filter filter
	protected var BasicEList<ChrisnaFSM> chrisnafsms
	protected var ArrayList<String> fsmnames
	protected var Counter counter
	protected var WarningError warningerrors
	protected var NodeFinder nodes
	protected var Scenario scen
	
	protected val Resource Res
	protected var ChrisnaFactoryImpl gen = new ChrisnaFactoryImpl()
	protected var GherkinFactoryImpl gher = new GherkinFactoryImpl()
	
	protected var int index
	
	
	new (Resource R){
		this.Res = R
		this.index = 0
		this.CreateAll()
		generator(this.Res)
	}
	
	def String getInterfaceName(){
		return this.interfacename
	}
	
	def String getVersion(){
		return this.version
	}
	
	def String getName(){
		return this.name
	}
	
	def ScenContainer getScenariocontainer(){
		return this.scenariocontainer
	}
	
	def BasicEList<ChrisnaFSM> getchrisnafsms(){
		if(this.chrisnafsms === null)
			chrisnafsms = new BasicEList<ChrisnaFSM>
		return this.chrisnafsms
	}
	
	def ArrayList<String> getFsmnames(){
		if(this.fsmnames === null)
			fsmnames = new ArrayList<String>
		return this.fsmnames
	}
	
	def state getInitState(){
		return this.initst
	}
	
	def setInitState(state initialstate){
		this.initst = initialstate
	}
	
	def void CreateAll(){
		scenariocontainer = gen?.createScenContainer()
		scencontainerwithtime = gen?.createScenContainerWithTime()
		scencontainerwithouttime = gen?.createScenContainerWithoutTime()
		historydispach = gen?.createHistoryDispach()
		filter = gen?.createFilter()
		chrisnafsms = new BasicEList<ChrisnaFSM>
		fsmnames = new ArrayList<String>
		counter = gen?.createCounter()
		warningerrors = gen?.createWarningError()
		nodes = gen?.createNodeFinder()
	}
	
	protected def linkCurrentNextStatesOfScenario(ChrisnaFSM statemachine, stateContainer initStates, stateContainer givenwhenStates){
		if (givenwhenStates?.getState.length != 0){
			for(st : givenwhenStates?.getState) {
				statemachine?.scencontainer?.scenario?.head?.currentStates?.state.add(genState(st))
				statemachine?.scencontainer?.scenario?.last?.nextStates?.state.add(genState(st))
			}
			
			for (cond : statemachine?.scencontainer?.scenario?.head?.condcontainer?.condition)
				cond?.setFrom(genState(givenwhenStates?.getState?.head))
			for (cond : statemachine?.scencontainer?.scenario?.last?.condcontainer?.condition)
				cond?.setTo(genState(givenwhenStates?.getState?.head))
		}
		else {
			for(st : initStates?.getState) {
				statemachine?.scencontainer?.scenario?.head?.currentStates?.state.add(genState(st))
				statemachine?.scencontainer?.scenario?.last?.nextStates?.state.add(genState(st))	
			}
			
			for (cond : statemachine?.scencontainer?.scenario?.head?.condcontainer?.condition)
				cond?.setFrom(genState(initStates?.getState?.head))
			for (cond : statemachine?.scencontainer?.scenario?.last?.condcontainer?.condition)
				cond?.setTo(genState(initStates?.getState?.head))
		}
	}
	
	protected def ChrisnaFSM decideWhichStateMachineToGen(stateContainer initStates, stateContainer givenwhenStates, SendEvent event, String scenname){
		switch (event?.event){
			case 'attribute':{
				var design = new ThenStepAttrArguDesignStateMachine(scenname, event)
				if (givenwhenStates?.getState.length != 0){
					for(st : givenwhenStates?.getState)
						design?.statemachine?.scencontainer?.scenario?.head?.currentStates?.state.add(genState(st))
					
					for (cond : design?.statemachine?.scencontainer?.scenario?.head?.condcontainer?.condition)
						cond?.setFrom(genState(givenwhenStates?.getState?.head))
				}
				else {
					for(st : initStates?.getState)
						design?.statemachine?.scencontainer?.scenario?.head?.currentStates?.state.add(genState(st))
					
					for (cond : design?.statemachine?.scencontainer?.scenario?.head?.condcontainer?.condition)
						cond?.setFrom(genState(initStates?.getState?.head))
				}
				
				for(st : initStates?.getState)
					design?.statemachine?.scencontainer?.scenario?.last?.nextStates?.state.add(genState(st))
				
				for (cond : design?.statemachine?.scencontainer?.scenario?.last?.condcontainer?.condition)
					cond?.setTo(genState(initStates?.getState?.head))
					
				design?.statemachine
			}
			case 'do not send':{
				var design = new ThenStepSendNoMethodDesignStateMachine(scenname, event)
				design?.statemachine.linkCurrentNextStatesOfScenario(initStates, givenwhenStates)
				design?.statemachine
			}
			case 'do send' :{
				if (event?.time?.type?.contains('Periodically') && (event?.time as Periodically)?.time != 0) {
					var design = new ThenStepSendMethodDesignStateMachine(scenname, event)
					design?.statemachine.linkCurrentNextStatesOfScenario(initStates, givenwhenStates)
					design?.statemachine
				}
				else if (event?.time?.type?.contains('OnChange')) {
					var design = new ThenStepOnChangeDesignStateMachine(scenname, event)
					design?.statemachine.linkCurrentNextStatesOfScenario(initStates, givenwhenStates)
					design?.statemachine
				}
				else if (event?.time?.type?.contains('TimeWindow') && (event?.time as TimeWindow)?.valT != 0){
					var design = new ThenStepTimeWindowDesignStateMachine(scenname, event)
					design?.statemachine.linkCurrentNextStatesOfScenario(initStates, givenwhenStates)
					design?.statemachine
				}
				else {
					var design = new ThenStepSendMethodDesignStateMachine(scenname, event)
					design?.statemachine.linkCurrentNextStatesOfScenario(initStates, givenwhenStates)
					design?.statemachine	
				}
			}
		}
	}
	
	protected def EventContainer ThenStepTranslation(ThenStep thenstep, stateContainer statcontainer, HashMap<String, String> map, String scenname){
		
		var listevents = gen?.createEventContainer 
		listevents?.sendevent?.add(StepOutBodyTranslation(thenstep?.getAction() as StepOutBody, map))
		for(AndOutputStep element : thenstep?.getAnd()){	
			listevents?.sendevent?.add(StepOutBodyTranslation(element?.getAction() as StepOutBody, map))
		}
		listevents
	}
	
	protected def EventContainer WhenStepTranslation(WhenStep whenstep, CondContainer concontainer, HashMap<String, String> map){
		var listevents = gen?.createEventContainer 
		listevents?.sendevent?.add(StepInBodyTranslation(whenstep?.getAction() as StepInBody, map))
		for( AndInputStep element : whenstep?.getAnd()){
			listevents?.sendevent?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
		}
		listevents
	}
	
	protected def EventContainer GivenStepTranslation(GivenStep givenstep, stateContainer statcontainer, HashMap<String, String> map){
		var listevents = gen?.createEventContainer 
		listevents?.sendevent?.add(StepInBodyTranslation(givenstep?.getAction() as StepInBody, map))
		for (AndInputStep element : givenstep?.getAnd()){
			listevents?.sendevent?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
			
		}
		listevents
	}
	
	protected def void ScenarioTranslation(ScenarioImpl scenario, HashMap<String, String> map){
		var scen = gen?.createScenario
		scen?.setCurrentStates(gen?.createstateContainer())
		scen?.setCondcontainer(gen?.createCondContainer())
		scen?.setNextStates(gen?.createstateContainer())
		hasGiven = false
		hasWhen = false
		hasThen = false
		scen?.setName((scenario)?.getName?.getName)
		var allSteps = ((scenario)?.getSteps())
		var givenevents = gen?.createEventContainer
		var whenevents = gen?.createEventContainer
		var thenevents = gen?.createEventContainer
		var thenstatemachines = new ArrayList<ChrisnaFSM>()
		period = new ArrayList<Float>()
		for (step : allSteps){
			switch(step?.getWhich()?.getClass){
				case(GivenStepImpl):
				{
					hasGiven = true
					givenevents = GivenStepTranslation(step?.getWhich() as GivenStepImpl, scen?.getCurrentStates, map)
				}
					
				case(WhenStepImpl):
				{
					hasWhen = true
					whenevents = WhenStepTranslation(step?.getWhich() as WhenStepImpl, scen?.getCondcontainer, map)
				}
					
				case(ThenStepImpl):
				{
					hasThen = true
					thenevents = ThenStepTranslation(step?.getWhich() as ThenStepImpl, scen?.getNextStates, map, scen?.name)
				}
			}
		}
		
		var designInitializer = new InitializingStateMachine(scen?.name, {
			if(period.size != 0)
				period?.max
			else
				0.0
		})
		var initstatemachine = designInitializer?.statemachine
		
		var designGivenWhen = new GivenWhenStepBaseDesignStateMachine(scen?.name, new ClusteringGivenWhenEvents(givenevents, whenevents))
		var givenwhenstatemachine = designGivenWhen?.statemachine
		
		var sta = initstatemachine?.scencontainer?.scenario?.last?.nextStates?.state?.head
		//givenwhenstatemachine?.scencontainer?.scenario?.get(0)?.currentStates?.state?.add(0, genState(sta))
		givenwhenstatemachine?.scencontainer?.scenario?.get(
			givenwhenstatemachine?.scencontainer?.scenario.length-1
		)?.currentStates?.state?.add(0, genState(sta))
		
		for (con : givenwhenstatemachine?.scencontainer?.scenario?.get(
			givenwhenstatemachine?.scencontainer?.scenario.length-1
		)?.condcontainer?.condition)
			con?.setFrom(genState(sta))
		
		givenwhenstatemachine?.scencontainer?.scenario?.get(
			givenwhenstatemachine?.scencontainer?.scenario.length-2
		)?.nextStates?.state?.add(0, genState(sta))
		for (con : givenwhenstatemachine?.scencontainer?.scenario?.get(
			givenwhenstatemachine?.scencontainer?.scenario.length-2
		)?.condcontainer?.condition)
			con?.setTo(genState(sta))
		
		for(scena : givenwhenstatemachine?.scencontainer?.scenario) {
			if(scena?.nextStates?.state.length == 0){
				scena?.nextStates?.state.add(genState(sta))
				for (con : scena?.condcontainer?.condition)
					con?.setTo(genState(sta))
			}
		}
		
		var statesInit = gen?.createstateContainer
		for(st : initstatemachine?.scencontainer?.scenario?.last?.nextStates?.state){
			statesInit?.state.add(genState(st))
		}
		
		var statesGivenWhen = gen?.createstateContainer
		if(givenwhenstatemachine?.scencontainer?.scenario.length <= 2){
			for(st : givenwhenstatemachine?.scencontainer?.scenario?.head?.nextStates?.state){
				statesGivenWhen?.state.add(genState(st))
			}
		}
		else {
			for(st : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates?.state){
				statesGivenWhen?.state.add(genState(st))
			}
		}
		
		for(event : thenevents?.getSendevent) {
			thenstatemachines.add(decideWhichStateMachineToGen(statesInit, statesGivenWhen, event, scen?.name))
		}
			
		for (element : initstatemachine?.scencontainer?.scenario)
			this.scenariocontainer?.getScenario?.add(genScenario(element))
		for (element : givenwhenstatemachine?.scencontainer?.scenario)
			this.scenariocontainer?.getScenario?.add(genScenario(element))
		for (statemachine : thenstatemachines)
			for (element : statemachine?.scencontainer?.scenario)
				this.scenariocontainer?.scenario?.add(genScenario(element))
		var index_state_machine = 0
		for (stmachine : thenstatemachines){
			index_state_machine++
			var fsm = gen?.createChrisnaFSM
			var ScenarioContainer = gen?.createScenContainer
			for (ele : initstatemachine?.scencontainer?.scenario)
				ScenarioContainer?.getScenario?.add(genScenario(ele))
			for (ele : givenwhenstatemachine?.scencontainer?.scenario)
				ScenarioContainer?.getScenario?.add(genScenario(ele))
			for (ele : stmachine?.scencontainer?.scenario)
				ScenarioContainer?.getScenario?.add(genScenario(ele))
			fsm?.setScencontainer(ScenarioContainer)
			fsm?.setInitstate(this.initst as initState)
			this?.chrisnafsms.add(fsm)
			this?.fsmnames.add(scenario.name.name.replaceAll('[ -.]','_').replaceAll('\\[','_').replaceAll('\\]','_')
				+ '_statemachine_' + index_state_machine.toString + '_index' + index.toString)
			this.index++
		}
	}
	
	protected def void ScenarioOutlineTranslation(ScenarioOutline scenarioOutline){
		
		val scenario = (gher?.createScenario as ScenarioImpl)
		scenario.setName(gher?.createScenarioTitle)
		if(scenario?.getTags !== null)
		{
			for(i : scenarioOutline?.getTags.size >.. 0){
				scenario?.getTags?.add(scenarioOutline?.getTags.get(i))
			}
		}
		scenario?.getName?.setName(scenarioOutline?.getName?.getName)
		if(scenario?.getElements !== null){
			for(i : scenarioOutline?.getElements.size >.. 0){
				scenario?.getElements?.add(scenarioOutline?.getElements.get(i))
			}
		}
		if(scenario?.getSteps !== null){
				for(i : scenarioOutline?.getSteps.size >.. 0){
					scenario?.getSteps?.add(scenarioOutline?.getSteps.get(i)) 
				}
		}
		
		scenarioOutline?.getExample?.getTable?.getRows?.forEach[
			var map = new HashMap<String, String>()
			var i = 0
			for(cell : (it?.getCells as TableCellsImpl).getCell){
				val key = scenarioOutline?.getExample?.getTable?.getHeader?.getCells?.getName.get(i)
				i++
				map.put(key, ConstantTranslation(cell))
			}
			ScenarioTranslation(scenario as ScenarioImpl, map)
		]
	}
	
	/*protected def void runfiltering(){
		this.filter?.GivenToWhenAttr()
		synchronized(this.lock){
			this.filter?.HierarchieCheck()
			this.filter?.OrtogonalCheck()
		}
		this.filter?.sevCondCheck()
		this.filter?.ParametrizationCheck()
		this.filter?.RekurAktCheck()
		this.filter?.OverlappCheck()
	}*/
	protected def void generator(Resource feature){
		
		this.name = feature.allContents.filter(Feature)?.map[title?.name].join.replace(' ', '_')
		 
		/*initst = gen?.createinitState()
		var EList<String> des = new BasicEList<String>(1) 
		des.add('initial state')
		initst.description = des
		initst.name = 'initial state'
		initst.action = gen?.createaction*/
		
		for(scenario : feature.allContents.toIterable.filter(AbstractScenario)){//?.forEach[
			switch(scenario?.getWhich()?.getClass){
				case(ScenarioImpl):
				{
					ScenarioTranslation(scenario?.getWhich as ScenarioImpl, NoTable)
				}
				case(ScenarioOutlineImpl):
				{
					ScenarioOutlineTranslation(scenario?.getWhich as ScenarioOutline)
				}
			}
		}
		
		//this.filter?.setScencontainer(scenariocontainer)
		//runfiltering()
		//this.warningerrors?.setScencontainer(this.filter?.getScencontainer)
		
		//this.chrisnafsm?.setInitstate(this.initst as initState)
		//this.chrisnafsm?.setScencontainer(scenariocontainer)
	}
}