package com.bmw.vv.grancin.codegen.chrisna

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap
import chrisna.Periodically

class ThenStepPeriodicallySendMethodDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genAttriArguDesignStateMachine
	}
	
	def genAttriArguDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Ok_' + name, null))
			//scen.nextStates.state.add(statemachine?.initstate)
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('doing again', 'Timing', {
				expressions?.clear
				if((sendedevent?.time as Periodically)?.unit !== null)
					expressions?.add(genExpression('const_time_' + name, '==', (sendedevent?.time as Periodically).time.toString + (sendedevent?.time as Periodically)?.unit ?: 'ms' + ')'))
				expressions}), scen.currentStates.state.head, scen.nextStates.state.head))
				
			scen
		})
	}
	
}