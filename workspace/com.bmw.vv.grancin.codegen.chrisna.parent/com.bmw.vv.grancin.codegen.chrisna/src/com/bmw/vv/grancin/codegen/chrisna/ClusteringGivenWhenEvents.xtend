package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.EventContainer
import chrisna.ChrisnaFactory
import chrisna.impl.ChrisnaFactoryImpl

class ClusteringGivenWhenEvents {
	
	protected val ChrisnaFactory generatorFactory
	
	protected val EventContainer givenEvents
	protected val EventContainer whenEvents
	
	protected val EventContainer givenAttributesEvents
	protected val EventContainer whenAttributesEvents
	protected val EventContainer givenInmethodReproductEvents
	protected val EventContainer whenInmethodReproductEvents
	protected val EventContainer givenWaitEvents
	protected val EventContainer whenWaitEvents
	
	new(EventContainer givenEves, EventContainer whenEves){
		givenEvents = givenEves
		whenEvents = whenEves
		
		generatorFactory = new ChrisnaFactoryImpl
		
		givenAttributesEvents = generatorFactory.createEventContainer
		whenAttributesEvents = generatorFactory.createEventContainer
		givenInmethodReproductEvents = generatorFactory.createEventContainer
		whenInmethodReproductEvents = generatorFactory.createEventContainer
		givenWaitEvents = generatorFactory.createEventContainer
		whenWaitEvents = generatorFactory.createEventContainer
		
		clustring
	}
	
	def clustring() {
		var givenlist = givenEvents?.getSendevent
		for(var i=0; i<givenlist.length; i++){
			switch (givenlist?.get(i)?.event.toLowerCase) {
				case 'attribute':
					givenAttributesEvents?.getSendevent.add(givenlist?.get(i))
				case 'in method':{
					if(givenlist?.get(i)?.getAssigcontainer?.getAssignment.length != 0) {
						givenAttributesEvents?.getSendevent.add(givenlist?.get(i))
					}
					else
						givenInmethodReproductEvents?.getSendevent.add(givenlist?.get(i))	
				}
				case 'reproduction':
					givenInmethodReproductEvents?.getSendevent.add(givenlist?.get(i))
				case 'wait':
					givenWaitEvents?.getSendevent.add(givenlist?.get(i))
			}
		}
		var whenlist = whenEvents?.getSendevent
		for(var i=0; i<whenlist.length ; i++){
			switch (whenlist?.get(i)?.event.toLowerCase) {
				case 'attribute':
					whenAttributesEvents?.getSendevent.add(whenlist?.get(i))
				case 'in method':{
					if(whenlist?.get(i)?.getAssigcontainer?.getAssignment.length != 0) {
						whenAttributesEvents?.getSendevent.add(whenlist?.get(i))
					}
					else
						whenAttributesEvents?.getSendevent.add(whenlist?.get(i))	
				}
				case 'reproduction':
					whenInmethodReproductEvents?.getSendevent.add(whenlist?.get(i))
				case 'wait':
					whenWaitEvents?.getSendevent.add(whenlist?.get(i))
			}
		}
	}
}