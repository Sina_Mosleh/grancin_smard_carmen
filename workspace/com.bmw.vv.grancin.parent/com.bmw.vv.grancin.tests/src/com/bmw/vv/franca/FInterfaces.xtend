package com.bmw.vv.franca

interface FInterfaces {
	val fKeySearchPosition = 
	'''
		package KeySearch
		
		interface KeySearchPos {
		
			attribute UInt8 ResultKeySearchPos readonly
			
			method REQREPKeySearchPos {
				in {
					UInt8 REQ
				}
				out {
					UInt8 REP				
				}
			}
		
			broadcast PUBKeySearchPos {
				out {
					String KeySearchPos
				}
			}
		}
	'''
	
	val fAlienDetection = 
	'''
		package ADAR
		
		interface AlienDetection {
			version {major 1 minor 0}
		
			attribute Boolean setActive	 
			
			<** @description: ConfirmationStatus shall be
				UNDEFINED	if setActive is false
				VALID 		if the related ID is contained in detectionIDs
				INVALID 	otherwise
			**> 
			enumeration ConfirmationStatus {
				VALID
				INVALID
				UNDEFINED
			}
			
			method isContactConfirmed {
				in {
					Int32   ReqByID
				}
				out {
					Boolean RepByID
					ConfirmationStatus Qualifier				
				}
			}
		
			<** @description: ADAR - Alien Detection and Ranging, 
			    allows a detection and location of aliens **>
			broadcast publishADARResults {
				out {
					Int32[] detectionIDs
					Int32[] detectionLocation
				}
			}
		}
	'''
	
	val fDriverSelectionModule = 
	'''
		package someip.BMW.INFRASTRUCTURE
		
		import someip.generalTypes.* from "GeneralTypes.fidl"
		
		interface DriverSelectionModule {
		
		  version {
		    major 1
		    minor 0
		  }
		
		  method setEntryInMappingTable {
		    in {
		      BdcIdentityMappingEntry newEntry
		  	}
		  }
		
		  attribute BdcIdentityMappingEntry[] bdcIdentityMappingTable readonly
		  attribute PiaProfileId piaProfileId readonly
	}
	'''
	
	val fGeneralTypes = 
	'''
		package someip
		
		typeCollection generalTypes {
					
		  struct BdcIdentityMappingEntry {
		    TokenId tokenId
		    PiaProfileId piaProfileId
		  }
		
		  typedef BOOL is UInt8
		  typedef TokenId is UINT8
		  typedef PiaProfileId is UINT8
		
		  typedef UINT16 is UInt16
		  typedef UINT32 is UInt32
		  typedef UINT64 is UInt64
		  typedef UINT8 is UInt8
		}
	'''
}
