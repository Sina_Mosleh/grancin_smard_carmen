package de.bmw.smard.modeller.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.util.TemplateUtils.TemplateCategorization;


/**
 * Helper class that gathers log levels on a per project basis and caches them for easy access.
 * When the user selects or enters a new log level, it is added to the "active" log levels in a project.
 * The active log levels are displayed in the popup menu for setting a log level.
 * When the user saves any state machine in a project, all cached but actually unused log levels are cleared away.
 * <P>
 * To access the information about log levels, you can use {@link #getProjectLogLevels(Resource)}.
 * <P>
 * The log cache is loaded and built upon demand per project as instances of this class. 
 * Access to the functionality is provided by static helper methods.
 * <P>
 * To make this work, this class should be notified when:
 * <UL>
 * <LI>The user changes the value of a log level (either in properties or using the popup menu or dialog):
 * {@link #notifyLogLevelChanged(Transition, boolean)}.
 * </UL>
 * The following situations are detected by the LogLevelManagerWorkbenchAddOn and forwarded to us:
 * <UL>
 * <LI>The user saves a state machine.
 * <LI>The user closes a statemachine diagram editor. (Needed to catch discards)
 * <LI>A state machine is generated from a template.
 * </UL>
 */
public class LogLevelManager {
	
	/**
	 * the value for the predefined log level "INFO"
	 */
	public static final String LOG_LEVEL_INFO = "25";
	
	/**
	 * the textual name for the predefined log level "INFO"
	 */
	public static final String LOG_LEVEL_INFO_TEXT = "INFO";
	
	/**
	 * the value for the predefined log level "OK"
	 */
	public static final String LOG_LEVEL_OK = "50";
	
	/**
	 * the textual name for the predefined log level "OK"
	 */
	public static final String LOG_LEVEL_OK_TEXT = "OK";
	
	/**
	 * the value for the predefined log level "WARN"
	 */
	public static final String LOG_LEVEL_WARN = "75";
	
	/**
	 * the textual name for the predefined log level "WARN"
	 */
	public static final String LOG_LEVEL_WARN_TEXT = "WARN";
	
	/**
	 * the value for the predefined log level "DEFECT"
	 */
	public static final String LOG_LEVEL_DEFECT = "100";
	
	/**
	 * the textual name for the predefined log level "DEFECT"
	 */
	public static final String LOG_LEVEL_DEFECT_TEXT = "DEFECT";
	
	/**
	 * A simple data object with public access, 
	 * contains the usage of a log level, 
	 * used as a value in a map where the key is the log level.
	 */
	public static class LogLevelUsage
	{
		
		/**
		 * the absolute file paths (canonical) of the state machines where the user entered
		 * this value explicitly in the extra dialog box. This value is updated upon each explicit input
		 * by adding the state machine where the new value is set. 
		 * The value is cleared for a state machine when it is saved.
		 * This value is separate from {@link #mStateMachinesWhereEnteredInProperties}
		 * because there are some hints that the explicit entry might cause different behavior.
		 */
		public Set<String> mStateMachinesWhereEnteredExplicitly = new HashSet<String>();
		
		/**
		 * the absolute file paths (canonical) of the state machines where the user entered
		 * the value in the transition properties. This value is updated upon each change 
		 * by adding the state machine where the new value is set. 
		 * The value is cleared for a state machine when it is saved. 
		 */
		public Set<String> mStateMachinesWhereEnteredInProperties = new HashSet<String>();
		
		/**
		 * the absolute file paths (canonical) of the state machines where the log level was present
		 * in the file. This value is updated when loading a state machine file when 
		 * it is opened by the user (before changing it)
		 * or we initialize this cache for a project (on demand).
		 * The value may be cleared when a state machine is saved and the log level is no longer used. 
		 */
		public Set<String> mStateMachinesWhereLoadedFromFile = new HashSet<String>();
	}
	
	/**
	 * contains the {@link LogLevelManager}s which have already been initialized for their projects.
	 * The key is the full path name to the project directory.
	 * If a project is present in this map, then we are tracking file changes for it 
	 * (e.g. adding and deleting of state machines).
	 */
	private static Map<String,LogLevelManager> sManagersByProject = new HashMap<String, LogLevelManager>();
	
	/**
	 * the eclipse project instance.
	 */
	private IProject mEclipseProject;
	
	/**
	 * the canonical paths of the known state machines in the project 
	 */
	private Set<String> mKnownStateMachines;
	
	/**
	 * Contains the classification of log level usage ({@link LogLevelUsage}) for numbered log levels. 
	 */
	private Map<Integer,LogLevelUsage> mRealLogLevels;
	
	/**
	 * Contains the classification of log level usage for template parameter log levels.
	 * Key is the template parameter name including the '#' at the beginning and the end.
	 */
	private Map<String,LogLevelUsage> mTemplateLogLevels;
	
	/**
	 * Returns the log levels that are used in the project including those that were explicitly (by dialog) 
	 * added which are no longer used (until saving).
	 * If the template categorization of the state machine is {@link TemplateCategorization#TEMPLATE}, 
	 * template parameter values are included in the result.
	 * The result list is ordered by Strings with number (increasing) and then template parameter names (increasing).
	 * Template parameter names will include the '#' at both ends.
	 * The entries for the default log level values 
	 * ({@link #LOG_LEVEL_INFO}, {@link #LOG_LEVEL_OK}, {@link #LOG_LEVEL_WARN}, 
	 * and {@link #LOG_LEVEL_DEFECT}) are excluded.
	 * <P>
	 * Note: The first call to this method for a project will scan the complete project for state machine
	 * files and analyze them if they haven't been loaded yet (this may take some time).
	 * 
	 * @param pStateMachineResource the state machine resource where the log level should be used, must not be null 
	 * @return the list of log level values that are used in the project (see method comment for details)
	 */
	public static List<String> getProjectLogLevels( Resource pStateMachineResource)
	{
		if (pStateMachineResource==null)
			throw new IllegalArgumentException("pStateMachineResource must not be null");
		LogLevelManager manager = getOrCreateManagerForResource(pStateMachineResource);
		manager.updateStateMachines();
		// add the numeric ones
		List<Integer> numericLogLevels = new ArrayList<Integer>(manager.getUsedNumericLogLevels());
		Collections.sort(numericLogLevels);
		List<String> result = new ArrayList<String>();
		for (Integer logLevel : numericLogLevels) {
			result.add(logLevel.toString());
		}
		// remove the default levels
		result.remove(LOG_LEVEL_INFO);
		result.remove(LOG_LEVEL_OK);
		result.remove(LOG_LEVEL_WARN);
		result.remove(LOG_LEVEL_DEFECT);
		// template parameter
		if (TemplateUtils.getTemplateCategorization(pStateMachineResource)==TemplateCategorization.TEMPLATE)
		{
			List<String> templateLogLevels = new ArrayList<String>(manager.getUsedTemplateLogLevels());
			Collections.sort(templateLogLevels);
			result.addAll(templateLogLevels);
		}
		return result;
	}
	
	/**
	 * should be called when a new state machine is added to the workspace
	 * @param pStateMachineEclipseResource the state machine resource
	 */
	public static void notifyStateMachineAdded(IResource pStateMachineEclipseResource) {
		LogLevelManager manager = getOrCreateManagerForResource(pStateMachineEclipseResource);
		manager.discardStateMachineInformation(pStateMachineEclipseResource); // to be safe discard the data we believe as known
		manager.scanOpenedStateMachineFile(pStateMachineEclipseResource);
	}

	/**
	 * should be called when a state machine is removed from the workspace
	 * @param pStateMachineEclipseResource the state machine resource
	 */
	public static void notifyStateMachineRemoved(IResource pStateMachineEclipseResource) {
		LogLevelManager manager = getOrCreateManagerForResource(pStateMachineEclipseResource);
		if ( manager != null )
			manager.discardStateMachineInformation(pStateMachineEclipseResource);
	}

	/**
	 * should be called when a state machine is saved in the workspace
	 * @param pStateMachineEclipseResource the state machine resource
	 */
	public static void notifyStateMachineChanged(IResource pStateMachineEclipseResource) {
		LogLevelManager manager = getOrCreateManagerForResource(pStateMachineEclipseResource);
		manager.discardStateMachineInformation(pStateMachineEclipseResource); // to be safe discard the data we believe as known
		manager.scanOpenedStateMachineFile(pStateMachineEclipseResource);
	}
	
	/**
	 * should be called when a state machine editor is closed.
	 * This is needed to detect the closing without saving (thus discarding changes).
	 * @param pStateMachineEclipseResource the state machine resource
	 */
	public static void notifyStateMachineEditorClosed(IResource pStateMachineEclipseResource) {
		LogLevelManager manager = getOrCreateManagerForResource(pStateMachineEclipseResource);
		manager.discardStateMachineInformation(pStateMachineEclipseResource);
		manager.scanOpenedStateMachineFile(pStateMachineEclipseResource);
	}

	/**
	 * this method must be called after the user changes a log level value for a transition.
	 * If more than one transition is changed, it must be called for every transition.
	 * 
	 * @param pTransition the transition where the log level has been changed, never null
	 * @param pExplicitlyEntered true if the user entered the new value in the extra dialog, 
	 * false otherwise, e.g. if the user selected the value from the menu or entered it in the properties page. 
	 */
	public static void notifyLogLevelChanged(Transition pTransition, boolean pExplicitlyEntered)
	{
		if (pTransition==null)
			throw new IllegalArgumentException("pTransition must not be null");
		Resource emfResource = pTransition.eResource();
		if (emfResource==null) 
			throw new IllegalStateException("Could not get resource for transition: "+pTransition);
		LogLevelManager manager = getOrCreateManagerForResource(emfResource);
		manager.internalNotifyLogLevelChanged(pTransition,pExplicitlyEntered);
	}
	
	/**
	 * Returns the {@link LogLevelManager} for project where the resource is located.
	 * If it doesn't exist yet, it will be created. 
	 * @param pEclipseResource the resource (e.g. a state machine file), must not be null
	 * @return the {@link LogLevelManager}, may be null if resource could not be associated
	 */
	private static LogLevelManager getOrCreateManagerForResource( IResource pEclipseResource)
	{
		IProject project = pEclipseResource.getProject();
		if (project==null)
			return null;
		IPath location = project.getLocation();
		if (location==null)
			return null;
		File file = location.toFile();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			PluginActivator.logException("Could not get canonical path for "+file, e);
			return null;
		}
		LogLevelManager manager = sManagersByProject.get(path);
		if (manager==null)
		{
			// create and add the manager
			manager = new LogLevelManager( project);
			sManagersByProject.put(path, manager);
		}
		return manager;
	}
	
	/**
	 * Returns the {@link LogLevelManager} for project where the resource is located.
	 * If it doesn't exist yet, it will be created. 
	 * @param pEmfResource the resource (e.g. a state machine file), must not be null
	 * @return the {@link LogLevelManager}, may be null if resource could not be associated
	 */
	private static LogLevelManager getOrCreateManagerForResource( Resource pEmfResource)
	{
		IProject project = EMFUtils.getEclipseProject(pEmfResource);
		if (project==null)
		{
			return null;
		}
		return getOrCreateManagerForResource(project);
	}

	/**
	 * only private instantiation
	 * @param pProject the project
	 */
	private LogLevelManager( IProject pProject)
	{
		mEclipseProject = pProject;
		mKnownStateMachines = new HashSet<String>();
		mRealLogLevels = new HashMap<Integer, LogLevelUsage>();
		mTemplateLogLevels = new HashMap<String, LogLevelUsage>();
	}
	
	/**
	 * scans the project for state machine files and updates our log level cache.
	 * If a previously unknown state machine is encountered, it is scanned.
	 * If a known state machine does not exist any longer, it is discarded.
	 * If a state machine is in {@link #mKnownStateMachines}, we assume that our information is up to date
	 * (since we're notified when it is modified, saved, or reverted).
	 */
	private void updateStateMachines()
	{
		HashSet<String> missingStateMachines = new HashSet<String>(mKnownStateMachines);
        List<IResource> stateMachineFiles;
		try {
			stateMachineFiles = DataUtils.getStateMachinesInProject(mEclipseProject);
		} catch (CoreException e) {
			throw new RuntimeException("Could not scan project for state machines: "+mEclipseProject, e);
		}
        ResourceSet resourceSet = new ResourceSetImpl();
        for (IResource resource : stateMachineFiles)
        {
        	String path;
			try {
				path = resource.getLocation().toFile().getCanonicalPath();
			} catch (IOException e) {
				throw new RuntimeException("Could not get canonical path for "+resource, e);
			}
        	if (mKnownStateMachines.contains(path))
        	{
        		// we already know about it, nothing to do
        		missingStateMachines.remove(path);
        	}
        	else
        	{
        		// unknown state machine - load and scan it
                URI fileURI = URI.createPlatformResourceURI(resource.getFullPath().toString(), true);
                Resource emfResource = resourceSet.getResource(fileURI, false);
                if (emfResource != null && emfResource.isLoaded())
                	emfResource.unload();
                emfResource = resourceSet.getResource(fileURI, true);
        		scanOpenedStateMachineFile(emfResource);
        	}
        }
        // now clear the data concerning those state machines that no longer exists
        for (String path : missingStateMachines) {
			discardStateMachineInformation(path);
		}
	}
	
	/**
	 * Returns the used numeric log levels in the project. 
	 * A log level is considered used when it is either present in the original file 
	 * or was used by the user since the last save of a state machine 
	 * (even if it is currently no longer used).
	 * @return the used log levels
	 */
	private Set<Integer> getUsedNumericLogLevels() 
	{
		Set<Integer> result = new HashSet<Integer>();
		for (Entry<Integer, LogLevelUsage> entry : mRealLogLevels.entrySet()) 
		{
			if (!entry.getValue().mStateMachinesWhereEnteredExplicitly.isEmpty()
					|| !entry.getValue().mStateMachinesWhereEnteredInProperties.isEmpty()
					|| !entry.getValue().mStateMachinesWhereLoadedFromFile.isEmpty())
			{
				result.add(entry.getKey());
			}
		}
		return result;
	}

	/**
	 * Returns the used template parameter log levels in the project. 
	 * A log level is considered used when it is either present in the original file 
	 * or was used by the user since the last save of a state machine 
	 * (even if it is currently no longer used).
	 * @return the used log levels
	 */
	private Set<String> getUsedTemplateLogLevels() 
	{
		Set<String> result = new HashSet<String>();
		for (Entry<String, LogLevelUsage> entry : mTemplateLogLevels.entrySet()) 
		{
			if (!entry.getValue().mStateMachinesWhereEnteredExplicitly.isEmpty()
					|| !entry.getValue().mStateMachinesWhereEnteredInProperties.isEmpty()
					|| !entry.getValue().mStateMachinesWhereLoadedFromFile.isEmpty())
			{
				result.add(entry.getKey());
			}
		}
		return result;
	}

	/**
	 * Discards all log level information with respect to the given state machine.
	 * This means that the state machine is removed from all {@link LogLevelUsage} values
	 * and from {@link #mKnownStateMachines}.
	 * 
	 * @param pStateMachinePath the canonical path to the state machine file, must not be null
	 */
	private void discardStateMachineInformation(String pStateMachinePath)
	{
		mKnownStateMachines.remove(pStateMachinePath);
		for (LogLevelUsage usage : mRealLogLevels.values()) {
			usage.mStateMachinesWhereEnteredExplicitly.remove(pStateMachinePath);
			usage.mStateMachinesWhereEnteredInProperties.remove(pStateMachinePath);
			usage.mStateMachinesWhereLoadedFromFile.remove(pStateMachinePath);
		}
		for (LogLevelUsage usage : mTemplateLogLevels.values()) {
			usage.mStateMachinesWhereEnteredExplicitly.remove(pStateMachinePath);
			usage.mStateMachinesWhereEnteredInProperties.remove(pStateMachinePath);
			usage.mStateMachinesWhereLoadedFromFile.remove(pStateMachinePath);
		}
	}
	
	/**
	 * Discards all log level information with respect to the given state machine.
	 * This means that the state machine is removed from all {@link LogLevelUsage} values
	 * and from {@link #mKnownStateMachines}.
	 * 
	 * @param pStateMachineResource the state machine resource, must not be null
	 */
	private void discardStateMachineInformation(IResource pStateMachineResource)
	{
		File file = pStateMachineResource.getLocation().toFile();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Could not get path for "+file,e);
		}
		discardStateMachineInformation(path);
	}

	/**
	 * Scans the given resource (based on the assumption that it is a state machine file)
	 * and records the currently used log levels as initial and unmodified levels used in the file.
	 * @param pStateMachineResource the state machine resource, must not be null
	 */
	private void scanOpenedStateMachineFile(IResource pStateMachineResource)
	{
        ResourceSet resourceSet = new ResourceSetImpl();
        URI fileURI = URI.createPlatformResourceURI(pStateMachineResource.getFullPath().toString(), true);
        Resource emfResource = resourceSet.getResource(fileURI, false);
        if (emfResource != null && emfResource.isLoaded())
        	emfResource.unload();
        try {
        	emfResource = resourceSet.getResource(fileURI, true);
        } catch (Exception e) {
        	PluginActivator.logMessage(IStatus.WARNING, "logLevelDetection failed - need to migrate to current model version first");
        	return;
        }
		StateMachine stateMachine = DataUtils.getStateMachine(emfResource);
		File file = pStateMachineResource.getLocation().toFile();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Could not get path for "+file,e);
		}
		scanOpenedStateMachine(path, stateMachine);
	}
	
	/**
	 * Scans the given resource (based on the assumption that it is a state machine file)
	 * and records the currently used log levels as initial and unmodified levels used in the file.
	 * @param pStateMachineResource the state machine resource, must not be null
	 */
	private void scanOpenedStateMachineFile(Resource pStateMachineResource)
	{
		StateMachine stateMachine = DataUtils.getStateMachine(pStateMachineResource);
		File file = EMFUtils.getEclipseResource(pStateMachineResource).getLocation().toFile();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Could not get path for "+file,e);
		}
		scanOpenedStateMachine(path, stateMachine);
	}
	
	/**
	 * Scans the given state machine and records the currently used log levels 
	 * as initial and unmodified levels used in the file.
	 * @param pPath the canonical path to the state machine resource, must not be null
	 * @param pStateMachine the state machine instance, must not be null
	 */
	private void scanOpenedStateMachine(String pPath, StateMachine pStateMachine)
	{
		mKnownStateMachines.add(pPath);
        EList<Transition> transitions = pStateMachine.getTransitions();
        for (Transition transition : transitions)
        {
        	LogLevelUsage usage = getOrCreateLogLevelUsageForValue(transition.getLogLevel());
        	if (usage!=null)
        	{
       			usage.mStateMachinesWhereLoadedFromFile.add(pPath);
        	}
        }
	}

	/**
	 * returns the {@link LogLevelUsage} for the given log level value.
	 * If the log level value is valid, and the {@link LogLevelUsage} does not exist yet, it will be created.
	 * If the log level value invalid, this method will return null
	 * @param pLogLevelValue the log level value, may be null or invalid
	 * @return the {@link LogLevelUsage} or null if the log level value is invalid
	 */
	private LogLevelUsage getOrCreateLogLevelUsageForValue(String pLogLevelValue) {
		if (pLogLevelValue==null)
			return null;
   		if (TemplateUtils.isPlaceholder(pLogLevelValue))
		{
   			LogLevelUsage usage = mTemplateLogLevels.get(pLogLevelValue);
   			if (usage==null) 
   			{
   				usage = new LogLevelUsage();
   				mTemplateLogLevels.put(pLogLevelValue, usage);
   			}
   			return usage;
		}
		if (TemplateUtils.checkIntNotPlaceholder(pLogLevelValue, false)==null)
		{
			// no error -> valid int
			Integer intValue = TemplateUtils.getIntNotPlaceholder(pLogLevelValue, false);
			LogLevelUsage usage = mTemplateLogLevels.get(intValue);
			if (usage==null) 
			{
				usage = new LogLevelUsage();
				mRealLogLevels.put(intValue, usage);
			}
			return usage;
		}
		// invalid int
		return null;
	}

	/**
	 * adds the state machine to the log level's {@link LogLevelUsage}
	 * @param pTransition the transition whose log level should be processed
	 * @param pExplicitlyEntered true if the value was explicitly entered in the extra dialog
	 */
	private void internalNotifyLogLevelChanged(Transition pTransition,
			boolean pExplicitlyEntered) 
	{
		File file = EMFUtils.getEclipseResource(pTransition.eResource()).getLocation().toFile();
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException("Could not get path for "+file,e);
		}
		if (!mKnownStateMachines.contains(path))
		{
			// somehow we were not notified of this file, simply scan it
			scanOpenedStateMachineFile(pTransition.eResource());
		}
		LogLevelUsage usage = getOrCreateLogLevelUsageForValue(pTransition.getLogLevel());
		if (usage!=null)
		{
			if (pExplicitlyEntered)
			{
				usage.mStateMachinesWhereEnteredExplicitly.add(path);
			}
			else
			{
				usage.mStateMachinesWhereEnteredInProperties.add(path);
			}
		}
	}
}
