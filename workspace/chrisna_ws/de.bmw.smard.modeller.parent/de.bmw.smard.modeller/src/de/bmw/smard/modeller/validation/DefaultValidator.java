package de.bmw.smard.modeller.validation;

import de.bmw.smard.base.exception.PluginLoadException;
import de.bmw.smard.common.plugin.PluginLoader;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.util.validation.DiagnosticConverter;
import de.bmw.smard.modeller.util.validation.PluginValidator;
import de.bmw.smard.modeller.util.validation.SMARDModelValidator;
import org.eclipse.emf.common.util.BasicDiagnostic;

import java.io.IOException;

class DefaultValidator implements Validator {

    private final ValidatorConfig config;

    DefaultValidator(ValidatorConfig validatorConfig) {
        this.config = validatorConfig;
    }

    public ValidationResult validate() {

        boolean success = false;
        BasicDiagnostic diagnostic = new BasicDiagnostic(config.getSource().getAbsolutePath(), 0, getClass().getName(), new Object[]{config.getStateMachineSet()});

        try {
            SMARDModelValidator modelValidator = new SMARDModelValidator();

            PluginLoader pluginLoader = new PluginLoader();
            PluginValidator pluginValidator = new PluginValidator(pluginLoader.loadPlugins(config.getSource()));
            modelValidator.putValidator(pluginValidator);

            success = modelValidator.isValid(config.getStateMachineSet(), diagnostic, Severity.WARNING);

        } catch (PluginLoadException | IOException e) {
            diagnostic.add(DiagnosticBuilder.error()
                    .source(DefaultValidator.class.getName())
                    .message(e.getMessage())
                    .data(e)
                    .build());
        }

        return new ValidationResult(success, DiagnosticConverter.convert(diagnostic.getChildren()));
    }
}
