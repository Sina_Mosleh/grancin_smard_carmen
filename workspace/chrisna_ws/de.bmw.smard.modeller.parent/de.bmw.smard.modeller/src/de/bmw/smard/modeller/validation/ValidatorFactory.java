package de.bmw.smard.modeller.validation;

public class ValidatorFactory {

    private ValidatorFactory() {}

    public static Validator newInstance(ValidatorConfig validatorConfig) {
        return new DefaultValidator(validatorConfig);
    }
}
