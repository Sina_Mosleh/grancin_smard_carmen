package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UDP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUDPMessage()
 * @model
 * @generated
 */
public interface UDPMessage extends AbstractBusMessage {
    /**
     * Returns the value of the '<em><b>Udp Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Udp Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Udp Filter</em>' containment reference.
     * @see #setUdpFilter(UDPFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUDPMessage_UdpFilter()
     * @model containment="true" required="true"
     * @generated
     */
    UDPFilter getUdpFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Udp Filter</em>' containment reference.
     * @see #getUdpFilter()
     * @generated
     */
    void setUdpFilter(UDPFilter value);

} // UDPMessage
