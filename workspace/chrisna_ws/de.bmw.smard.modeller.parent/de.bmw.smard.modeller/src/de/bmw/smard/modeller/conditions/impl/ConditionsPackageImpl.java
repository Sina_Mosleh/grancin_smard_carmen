package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class ConditionsPackageImpl extends EPackageImpl implements ConditionsPackage {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass documentRootEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass conditionSetEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass conditionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass signalComparisonExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass canMessageCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass linMessageCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stateCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass timingExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass trueExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass logicalExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass referenceConditionExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass transitionCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass flexRayMessageCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stopExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass comparatorSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass constantComparatorValueEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass calculationExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass expressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass variableReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass conditionsDocumentEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass variableSetEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass variableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass signalVariableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass valueVariableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractObserverEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass observerValueRangeEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass signalObserverEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass signalReferenceSetEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass signalReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iSignalOrReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass bitPatternComparatorValueEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass notExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iOperandEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iComputeVariableActionOperandEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iStringOperandEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iSignalComparisonExpressionOperandEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iOperationEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iStringOperationEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iNumericOperandEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iNumericOperationEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass valueVariableObserverEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass parseStringToDoubleEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stringExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass matchesEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass extractEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass substringEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass parseNumericToStringEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stringLengthEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass someIPMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass containerSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass ethernetFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass udpFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass tcpFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iPv4FilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass someIPFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass forEachExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass messageCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass someIPSDFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass someIPSDMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass doubleSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stringSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass decodeStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass doubleDecodeStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass extractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass universalPayloadExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass emptyExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass canFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass linFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass flexRayFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass dltFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass udpnmFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass canMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass linMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass flexRayMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass dltMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass udpMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass tcpMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass udpnmMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass verboseDLTMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass universalPayloadWithLegacyExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractBusMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginStateExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginResultExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass emptyDecodeStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass pluginCheckExpressionEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass baseClassWithIDEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass headerSignalEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iVariableReaderWriterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iStateTransitionReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass tpFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass baseClassWithSourceReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass sourceReferenceEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass ethernetMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass iPv4MessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass nonVerboseDLTMessageEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stringDecodeStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass nonVerboseDLTFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass verboseDLTExtractStrategyEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass regexOperationEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass payloadFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass verboseDLTPayloadFilterEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractVariableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass structureVariableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass variableFormatEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass computedVariableEClass = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum checkTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum comparatorEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum logicalOperatorTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum signalDataTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum flexChannelTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum byteOrderTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum flexRayHeaderFlagSelectionEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum booleanOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum protocolTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum ethernetProtocolTypeSelectionEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum rxTxFlagTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum observerValueTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum pluginStateValueTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum analysisTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPMessageTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPMethodTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPReturnCodeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPSDFlagsOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPSDTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum canExtIdentifierOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dataTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dlT_MessageTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dlT_MessageTraceInfoEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dlT_MessageLogInfoEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dlT_MessageControlInfoEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum dlT_MessageBusInfoEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum forEachExpressionModifierTemplateEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum autosarDataTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum canFrameTypeOrTemplatePlaceholderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum evaluationBehaviourOrTemplateDefinedEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum signalVariableLagEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum ttlOrTemplatePlaceHolderEnumEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum formatDataTypeEEnum = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType checkTypeObjectEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType comparatorObjectEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType logicalOperatorTypeObjectEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType hexOrIntOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType intOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType longOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType doubleOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType doubleOrHexOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType ipPatternOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType macPatternOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType portPatternOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType vlanIdPatternOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType ipOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType portOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType vlanIdOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType macOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType bitmaskOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType hexOrIntOrNullOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType nonZeroDoubleOrTemplatePlaceholderEDataType = null;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType byteOrTemplatePlaceholderEDataType = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ConditionsPackageImpl() {
        super(eNS_URI, ConditionsFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link ConditionsPackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ConditionsPackage init() {
        if (isInited) return (ConditionsPackage) EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredConditionsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        ConditionsPackageImpl theConditionsPackage =
                registeredConditionsPackage instanceof ConditionsPackageImpl ? (ConditionsPackageImpl) registeredConditionsPackage : new ConditionsPackageImpl();

        isInited = true;

        // Initialize simple dependencies
        XMLTypePackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);
        StatemachinePackageImpl theStatemachinePackage =
                (StatemachinePackageImpl) (registeredPackage instanceof StatemachinePackageImpl ? registeredPackage : StatemachinePackage.eINSTANCE);
        registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);
        StatemachinesetPackageImpl theStatemachinesetPackage =
                (StatemachinesetPackageImpl) (registeredPackage instanceof StatemachinesetPackageImpl ? registeredPackage : StatemachinesetPackage.eINSTANCE);

        // Create package meta-data objects
        theConditionsPackage.createPackageContents();
        theStatemachinePackage.createPackageContents();
        theStatemachinesetPackage.createPackageContents();

        // Initialize created meta-data
        theConditionsPackage.initializePackageContents();
        theStatemachinePackage.initializePackageContents();
        theStatemachinesetPackage.initializePackageContents();

        // Register package validator
        EValidator.Registry.INSTANCE.put(theConditionsPackage,
                new EValidator.Descriptor() {
                    @Override
                    public EValidator getEValidator() {
                        return ConditionsValidator.INSTANCE;
                    }
                });

        // Mark meta-data to indicate it can't be changed
        theConditionsPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ConditionsPackage.eNS_URI, theConditionsPackage);
        return theConditionsPackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDocumentRoot() {
        return documentRootEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute) documentRootEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_ConditionSet() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_ConditionsDocument() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getConditionSet() {
        return conditionSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Group() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Baureihe() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Description() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Istufe() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Schemaversion() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Uuid() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Version() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getConditionSet_Conditions() {
        return (EReference) conditionSetEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConditionSet_Name() {
        return (EAttribute) conditionSetEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCondition() {
        return conditionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCondition_Name() {
        return (EAttribute) conditionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCondition_Description() {
        return (EAttribute) conditionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getCondition_Expression() {
        return (EReference) conditionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSignalComparisonExpression() {
        return signalComparisonExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSignalComparisonExpression_ComparatorSignal() {
        return (EReference) signalComparisonExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSignalComparisonExpression_Operator() {
        return (EAttribute) signalComparisonExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSignalComparisonExpression_OperatorTmplParam() {
        return (EAttribute) signalComparisonExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCanMessageCheckExpression() {
        return canMessageCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_Busid() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_Type() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_MessageIDs() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_TypeTmplParam() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_BusidTmplParam() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_RxtxFlag() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_RxtxFlagTypeTmplParam() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_ExtIdentifier() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCanMessageCheckExpression_ExtIdentifierTmplParam() {
        return (EAttribute) canMessageCheckExpressionEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLinMessageCheckExpression() {
        return linMessageCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLinMessageCheckExpression_Busid() {
        return (EAttribute) linMessageCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLinMessageCheckExpression_Type() {
        return (EAttribute) linMessageCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLinMessageCheckExpression_MessageIDs() {
        return (EAttribute) linMessageCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLinMessageCheckExpression_TypeTmplParam() {
        return (EAttribute) linMessageCheckExpressionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLinMessageCheckExpression_BusidTmplParam() {
        return (EAttribute) linMessageCheckExpressionEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStateCheckExpression() {
        return stateCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStateCheckExpression_CheckStateActive() {
        return (EAttribute) stateCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStateCheckExpression_CheckState() {
        return (EReference) stateCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStateCheckExpression_CheckStateActiveTmplParam() {
        return (EAttribute) stateCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTimingExpression() {
        return timingExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTimingExpression_Maxtime() {
        return (EAttribute) timingExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTimingExpression_Mintime() {
        return (EAttribute) timingExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTrueExpression() {
        return trueExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLogicalExpression() {
        return logicalExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getLogicalExpression_Expressions() {
        return (EReference) logicalExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLogicalExpression_Operator() {
        return (EAttribute) logicalExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLogicalExpression_OperatorTmplParam() {
        return (EAttribute) logicalExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getReferenceConditionExpression() {
        return referenceConditionExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getReferenceConditionExpression_Condition() {
        return (EReference) referenceConditionExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTransitionCheckExpression() {
        return transitionCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransitionCheckExpression_StayActive() {
        return (EAttribute) transitionCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransitionCheckExpression_CheckTransition() {
        return (EReference) transitionCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransitionCheckExpression_StayActiveTmplParam() {
        return (EAttribute) transitionCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFlexRayMessageCheckExpression() {
        return flexRayMessageCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_BusId() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_PayloadPreamble() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_ZeroFrame() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_SyncFrame() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_StartupFrame() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_NetworkMgmt() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_FlexrayMessageId() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_PayloadPreambleTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_ZeroFrameTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_SyncFrameTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_StartupFrameTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_NetworkMgmtTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_BusIdTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(12);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_Type() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(13);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayMessageCheckExpression_TypeTmplParam() {
        return (EAttribute) flexRayMessageCheckExpressionEClass.getEStructuralFeatures().get(14);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStopExpression() {
        return stopExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStopExpression_AnalyseArt() {
        return (EAttribute) stopExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStopExpression_AnalyseArtTmplParam() {
        return (EAttribute) stopExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getComparatorSignal() {
        return comparatorSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getComparatorSignal_Description() {
        return (EAttribute) comparatorSignalEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getConstantComparatorValue() {
        return constantComparatorValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConstantComparatorValue_Value() {
        return (EAttribute) constantComparatorValueEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConstantComparatorValue_InterpretedValue() {
        return (EAttribute) constantComparatorValueEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getConstantComparatorValue_InterpretedValueTmplParam() {
        return (EAttribute) constantComparatorValueEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCalculationExpression() {
        return calculationExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCalculationExpression_Expression() {
        return (EAttribute) calculationExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getCalculationExpression_Operands() {
        return (EReference) calculationExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getExpression() {
        return expressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getExpression_Description() {
        return (EAttribute) expressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVariableReference() {
        return variableReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getVariableReference_Variable() {
        return (EReference) variableReferenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getConditionsDocument() {
        return conditionsDocumentEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getConditionsDocument_SignalReferencesSet() {
        return (EReference) conditionsDocumentEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getConditionsDocument_VariableSet() {
        return (EReference) conditionsDocumentEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getConditionsDocument_ConditionSet() {
        return (EReference) conditionsDocumentEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVariableSet() {
        return variableSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariableSet_Uuid() {
        return (EAttribute) variableSetEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getVariableSet_Variables() {
        return (EReference) variableSetEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVariable() {
        return variableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariable_DataType() {
        return (EAttribute) variableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariable_Unit() {
        return (EAttribute) variableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getVariable_VariableFormat() {
        return (EReference) variableEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariable_VariableFormatTmplParam() {
        return (EAttribute) variableEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSignalVariable() {
        return signalVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSignalVariable_InterpretedValue() {
        return (EAttribute) signalVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSignalVariable_Signal() {
        return (EReference) signalVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSignalVariable_InterpretedValueTmplParam() {
        return (EAttribute) signalVariableEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSignalVariable_SignalObservers() {
        return (EReference) signalVariableEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSignalVariable_Lag() {
        return (EAttribute) signalVariableEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getValueVariable() {
        return valueVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getValueVariable_InitialValue() {
        return (EAttribute) valueVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getValueVariable_ValueVariableObservers() {
        return (EReference) valueVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractObserver() {
        return abstractObserverEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractObserver_Name() {
        return (EAttribute) abstractObserverEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractObserver_Description() {
        return (EAttribute) abstractObserverEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractObserver_LogLevel() {
        return (EAttribute) abstractObserverEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractObserver_ValueRanges() {
        return (EReference) abstractObserverEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractObserver_ValueRangesTmplParam() {
        return (EAttribute) abstractObserverEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractObserver_ActiveAtStart() {
        return (EAttribute) abstractObserverEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getObserverValueRange() {
        return observerValueRangeEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getObserverValueRange_Value() {
        return (EAttribute) observerValueRangeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getObserverValueRange_Description() {
        return (EAttribute) observerValueRangeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getObserverValueRange_ValueType() {
        return (EAttribute) observerValueRangeEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSignalObserver() {
        return signalObserverEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSignalReferenceSet() {
        return signalReferenceSetEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSignalReferenceSet_Messages() {
        return (EReference) signalReferenceSetEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSignalReference() {
        return signalReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSignalReference_Signal() {
        return (EReference) signalReferenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getISignalOrReference() {
        return iSignalOrReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBitPatternComparatorValue() {
        return bitPatternComparatorValueEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBitPatternComparatorValue_Value() {
        return (EAttribute) bitPatternComparatorValueEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBitPatternComparatorValue_Startbit() {
        return (EAttribute) bitPatternComparatorValueEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getNotExpression() {
        return notExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getNotExpression_Expression() {
        return (EReference) notExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getNotExpression_Operator() {
        return (EAttribute) notExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIOperand() {
        return iOperandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIComputeVariableActionOperand() {
        return iComputeVariableActionOperandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIStringOperand() {
        return iStringOperandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getISignalComparisonExpressionOperand() {
        return iSignalComparisonExpressionOperandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIOperation() {
        return iOperationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIStringOperation() {
        return iStringOperationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getINumericOperand() {
        return iNumericOperandEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getINumericOperation() {
        return iNumericOperationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getValueVariableObserver() {
        return valueVariableObserverEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getParseStringToDouble() {
        return parseStringToDoubleEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getParseStringToDouble_StringToParse() {
        return (EReference) parseStringToDoubleEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStringExpression() {
        return stringExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMatches() {
        return matchesEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getMatches_StringToCheck() {
        return (EReference) matchesEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getExtract() {
        return extractEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getExtract_StringToExtract() {
        return (EReference) extractEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getExtract_GroupIndex() {
        return (EAttribute) extractEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSubstring() {
        return substringEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSubstring_StringToExtract() {
        return (EReference) substringEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSubstring_StartIndex() {
        return (EAttribute) substringEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSubstring_EndIndex() {
        return (EAttribute) substringEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getParseNumericToString() {
        return parseNumericToStringEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getParseNumericToString_NumericOperandToBeParsed() {
        return (EReference) parseNumericToStringEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStringLength() {
        return stringLengthEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStringLength_StringOperand() {
        return (EReference) stringLengthEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractMessage() {
        return abstractMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractMessage_Signals() {
        return (EReference) abstractMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractMessage_Name() {
        return (EAttribute) abstractMessageEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSomeIPMessage() {
        return someIPMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSomeIPMessage_SomeIPFilter() {
        return (EReference) someIPMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractSignal() {
        return abstractSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractSignal_Name() {
        return (EAttribute) abstractSignalEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractSignal_DecodeStrategy() {
        return (EReference) abstractSignalEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractSignal_ExtractStrategy() {
        return (EReference) abstractSignalEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContainerSignal() {
        return containerSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getContainerSignal_ContainedSignals() {
        return (EReference) containerSignalEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractFilter() {
        return abstractFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractFilter_PayloadLength() {
        return (EAttribute) abstractFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEthernetFilter() {
        return ethernetFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_SourceMAC() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_DestMAC() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_EtherType() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_InnerVlanId() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_OuterVlanId() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_InnerVlanCFI() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_OuterVlanCFI() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_InnerVlanVID() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_OuterVlanVID() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_CRC() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_InnerVlanTPID() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEthernetFilter_OuterVlanTPID() {
        return (EAttribute) ethernetFilterEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUDPFilter() {
        return udpFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUDPFilter_Checksum() {
        return (EAttribute) udpFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTCPFilter() {
        return tcpFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_SequenceNumber() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_AcknowledgementNumber() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_TcpFlags() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_StreamAnalysisFlags() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_StreamAnalysisFlagsTmplParam() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTCPFilter_StreamValidPayloadOffset() {
        return (EAttribute) tcpFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIPv4Filter() {
        return iPv4FilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getIPv4Filter_ProtocolType() {
        return (EAttribute) iPv4FilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getIPv4Filter_ProtocolTypeTmplParam() {
        return (EAttribute) iPv4FilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getIPv4Filter_SourceIP() {
        return (EAttribute) iPv4FilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getIPv4Filter_DestIP() {
        return (EAttribute) iPv4FilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getIPv4Filter_TimeToLive() {
        return (EAttribute) iPv4FilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSomeIPFilter() {
        return someIPFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_ServiceId() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_MethodType() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_MethodTypeTmplParam() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_MethodId() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_ClientId() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_SessionId() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_ProtocolVersion() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_InterfaceVersion() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_MessageType() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_MessageTypeTmplParam() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_ReturnCode() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_ReturnCodeTmplParam() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPFilter_SomeIPLength() {
        return (EAttribute) someIPFilterEClass.getEStructuralFeatures().get(12);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getForEachExpression() {
        return forEachExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getForEachExpression_FilterExpressions() {
        return (EReference) forEachExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getForEachExpression_ContainerSignal() {
        return (EReference) forEachExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMessageCheckExpression() {
        return messageCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getMessageCheckExpression_Message() {
        return (EReference) messageCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSomeIPSDFilter() {
        return someIPSDFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_Flags() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_FlagsTmplParam() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_SdType() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_SdTypeTmplParam() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_InstanceId() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_Ttl() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_MajorVersion() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_MinorVersion() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_EventGroupId() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_IndexFirstOption() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_IndexSecondOption() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_NumberFirstOption() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_NumberSecondOption() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(12);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSomeIPSDFilter_ServiceId_SomeIPSD() {
        return (EAttribute) someIPSDFilterEClass.getEStructuralFeatures().get(13);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSomeIPSDMessage() {
        return someIPSDMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getSomeIPSDMessage_SomeIPSDFilter() {
        return (EReference) someIPSDMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDoubleSignal() {
        return doubleSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDoubleSignal_IgnoreInvalidValues() {
        return (EAttribute) doubleSignalEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDoubleSignal_IgnoreInvalidValuesTmplParam() {
        return (EAttribute) doubleSignalEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStringSignal() {
        return stringSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDecodeStrategy() {
        return decodeStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDoubleDecodeStrategy() {
        return doubleDecodeStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDoubleDecodeStrategy_Factor() {
        return (EAttribute) doubleDecodeStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDoubleDecodeStrategy_Offset() {
        return (EAttribute) doubleDecodeStrategyEClass.getEStructuralFeatures().get(1);
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getExtractStrategy() {
        return extractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getExtractStrategy_AbstractSignal() {
        return (EReference) extractStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUniversalPayloadExtractStrategy() {
        return universalPayloadExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadExtractStrategy_ExtractionRule() {
        return (EAttribute) universalPayloadExtractStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadExtractStrategy_ByteOrder() {
        return (EAttribute) universalPayloadExtractStrategyEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam() {
        return (EAttribute) universalPayloadExtractStrategyEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadExtractStrategy_SignalDataType() {
        return (EAttribute) universalPayloadExtractStrategyEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam() {
        return (EAttribute) universalPayloadExtractStrategyEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEmptyExtractStrategy() {
        return emptyExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCANFilter() {
        return canFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_MessageIdRange() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_FrameId() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_RxtxFlag() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_RxtxFlagTmplParam() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_ExtIdentifier() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_ExtIdentifierTmplParam() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_FrameType() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCANFilter_FrameTypeTmplParam() {
        return (EAttribute) canFilterEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLINFilter() {
        return linFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLINFilter_MessageIdRange() {
        return (EAttribute) linFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLINFilter_FrameId() {
        return (EAttribute) linFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFlexRayFilter() {
        return flexRayFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_MessageIdRange() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_SlotId() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_CycleOffset() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_CycleRepetition() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_Channel() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getFlexRayFilter_ChannelTmplParam() {
        return (EAttribute) flexRayFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDLTFilter() {
        return dltFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_EcuID_ECU() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_SessionID_SEID() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_ApplicationID_APID() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_ContextID_CTID() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageType_MSTP() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageLogInfo_MSLI() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageTraceInfo_MSTI() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageBusInfo_MSBI() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageControlInfo_MSCI() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageLogInfo_MSLITmplParam() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(9);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageTraceInfo_MSTITmplParam() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(10);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageBusInfo_MSBITmplParam() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(11);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageControlInfo_MSCITmplParam() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(12);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDLTFilter_MessageType_MSTPTmplParam() {
        return (EAttribute) dltFilterEClass.getEStructuralFeatures().get(13);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUDPNMFilter() {
        return udpnmFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUDPNMFilter_SourceNodeIdentifier() {
        return (EAttribute) udpnmFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUDPNMFilter_ControlBitVector() {
        return (EAttribute) udpnmFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUDPNMFilter_PwfStatus() {
        return (EAttribute) udpnmFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUDPNMFilter_TeilnetzStatus() {
        return (EAttribute) udpnmFilterEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCANMessage() {
        return canMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getCANMessage_CanFilter() {
        return (EReference) canMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLINMessage() {
        return linMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getLINMessage_LinFilter() {
        return (EReference) linMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getFlexRayMessage() {
        return flexRayMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getFlexRayMessage_FlexRayFilter() {
        return (EReference) flexRayMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDLTMessage() {
        return dltMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUDPMessage() {
        return udpMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getUDPMessage_UdpFilter() {
        return (EReference) udpMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTCPMessage() {
        return tcpMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTCPMessage_TcpFilter() {
        return (EReference) tcpMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUDPNMMessage() {
        return udpnmMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getUDPNMMessage_UdpnmFilter() {
        return (EReference) udpnmMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVerboseDLTMessage() {
        return verboseDLTMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getVerboseDLTMessage_DltFilter() {
        return (EReference) verboseDLTMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUniversalPayloadWithLegacyExtractStrategy() {
        return universalPayloadWithLegacyExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadWithLegacyExtractStrategy_StartBit() {
        return (EAttribute) universalPayloadWithLegacyExtractStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUniversalPayloadWithLegacyExtractStrategy_Length() {
        return (EAttribute) universalPayloadWithLegacyExtractStrategyEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractBusMessage() {
        return abstractBusMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractBusMessage_BusId() {
        return (EAttribute) abstractBusMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractBusMessage_BusIdTmplParam() {
        return (EAttribute) abstractBusMessageEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractBusMessage_BusIdRange() {
        return (EAttribute) abstractBusMessageEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractBusMessage_Filters() {
        return (EReference) abstractBusMessageEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginFilter() {
        return pluginFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginFilter_PluginName() {
        return (EAttribute) pluginFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginFilter_PluginVersion() {
        return (EAttribute) pluginFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginMessage() {
        return pluginMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getPluginMessage_PluginFilter() {
        return (EReference) pluginMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginSignal() {
        return pluginSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginStateExtractStrategy() {
        return pluginStateExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginStateExtractStrategy_States() {
        return (EAttribute) pluginStateExtractStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginStateExtractStrategy_StatesTmplParam() {
        return (EAttribute) pluginStateExtractStrategyEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginStateExtractStrategy_StatesActive() {
        return (EAttribute) pluginStateExtractStrategyEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginStateExtractStrategy_StatesActiveTmplParam() {
        return (EAttribute) pluginStateExtractStrategyEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginResultExtractStrategy() {
        return pluginResultExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginResultExtractStrategy_ResultRange() {
        return (EAttribute) pluginResultExtractStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEmptyDecodeStrategy() {
        return emptyDecodeStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPluginCheckExpression() {
        return pluginCheckExpressionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginCheckExpression_EvaluationBehaviour() {
        return (EAttribute) pluginCheckExpressionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getPluginCheckExpression_SignalToCheck() {
        return (EReference) pluginCheckExpressionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPluginCheckExpression_EvaluationBehaviourTmplParam() {
        return (EAttribute) pluginCheckExpressionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBaseClassWithID() {
        return baseClassWithIDEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getBaseClassWithID_Id() {
        return (EAttribute) baseClassWithIDEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getHeaderSignal() {
        return headerSignalEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getHeaderSignal_Attribute() {
        return (EAttribute) headerSignalEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getHeaderSignal_AttributeTmplParam() {
        return (EAttribute) headerSignalEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIVariableReaderWriter() {
        return iVariableReaderWriterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIStateTransitionReference() {
        return iStateTransitionReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTPFilter() {
        return tpFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTPFilter_SourcePort() {
        return (EAttribute) tpFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTPFilter_DestPort() {
        return (EAttribute) tpFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getBaseClassWithSourceReference() {
        return baseClassWithSourceReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getBaseClassWithSourceReference_SourceReference() {
        return (EReference) baseClassWithSourceReferenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSourceReference() {
        return sourceReferenceEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getSourceReference_SerializedReference() {
        return (EAttribute) sourceReferenceEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEthernetMessage() {
        return ethernetMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getEthernetMessage_EthernetFilter() {
        return (EReference) ethernetMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getIPv4Message() {
        return iPv4MessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getIPv4Message_IPv4Filter() {
        return (EReference) iPv4MessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getNonVerboseDLTMessage() {
        return nonVerboseDLTMessageEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getNonVerboseDLTMessage_NonVerboseDltFilter() {
        return (EReference) nonVerboseDLTMessageEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStringDecodeStrategy() {
        return stringDecodeStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStringDecodeStrategy_StringTermination() {
        return (EAttribute) stringDecodeStrategyEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStringDecodeStrategy_StringTerminationTmplParam() {
        return (EAttribute) stringDecodeStrategyEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getNonVerboseDLTFilter() {
        return nonVerboseDLTFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getNonVerboseDLTFilter_MessageId() {
        return (EAttribute) nonVerboseDLTFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getNonVerboseDLTFilter_MessageIdRange() {
        return (EAttribute) nonVerboseDLTFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVerboseDLTExtractStrategy() {
        return verboseDLTExtractStrategyEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getRegexOperation() {
        return regexOperationEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getRegexOperation_Regex() {
        return (EAttribute) regexOperationEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getRegexOperation_DynamicRegex() {
        return (EReference) regexOperationEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getPayloadFilter() {
        return payloadFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPayloadFilter_Index() {
        return (EAttribute) payloadFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPayloadFilter_Mask() {
        return (EAttribute) payloadFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getPayloadFilter_Value() {
        return (EAttribute) payloadFilterEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVerboseDLTPayloadFilter() {
        return verboseDLTPayloadFilterEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVerboseDLTPayloadFilter_Regex() {
        return (EAttribute) verboseDLTPayloadFilterEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVerboseDLTPayloadFilter_ContainsAny() {
        return (EAttribute) verboseDLTPayloadFilterEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractVariable() {
        return abstractVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractVariable_Name() {
        return (EAttribute) abstractVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractVariable_DisplayName() {
        return (EAttribute) abstractVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractVariable_Description() {
        return (EAttribute) abstractVariableEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStructureVariable() {
        return structureVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStructureVariable_Variables() {
        return (EReference) structureVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStructureVariable_VariableReferences() {
        return (EReference) structureVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getComputedVariable() {
        return computedVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getComputedVariable_Operands() {
        return (EReference) computedVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getComputedVariable_Expression() {
        return (EAttribute) computedVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVariableFormat() {
        return variableFormatEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariableFormat_Digits() {
        return (EAttribute) variableFormatEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariableFormat_BaseDataType() {
        return (EAttribute) variableFormatEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVariableFormat_UpperCase() {
        return (EAttribute) variableFormatEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getCheckType() {
        return checkTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getComparator() {
        return comparatorEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getLogicalOperatorType() {
        return logicalOperatorTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSignalDataType() {
        return signalDataTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getFlexChannelType() {
        return flexChannelTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getByteOrderType() {
        return byteOrderTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getFlexRayHeaderFlagSelection() {
        return flexRayHeaderFlagSelectionEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getBooleanOrTemplatePlaceholderEnum() {
        return booleanOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getProtocolTypeOrTemplatePlaceholderEnum() {
        return protocolTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getEthernetProtocolTypeSelection() {
        return ethernetProtocolTypeSelectionEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getRxTxFlagTypeOrTemplatePlaceholderEnum() {
        return rxTxFlagTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getObserverValueType() {
        return observerValueTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getPluginStateValueTypeOrTemplatePlaceholderEnum() {
        return pluginStateValueTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getAnalysisTypeOrTemplatePlaceholderEnum() {
        return analysisTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPMessageTypeOrTemplatePlaceholderEnum() {
        return someIPMessageTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPMethodTypeOrTemplatePlaceholderEnum() {
        return someIPMethodTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPReturnCodeOrTemplatePlaceholderEnum() {
        return someIPReturnCodeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPSDFlagsOrTemplatePlaceholderEnum() {
        return someIPSDFlagsOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPSDTypeOrTemplatePlaceholderEnum() {
        return someIPSDTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getStreamAnalysisFlagsOrTemplatePlaceholderEnum() {
        return streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum() {
        return someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getCanExtIdentifierOrTemplatePlaceholderEnum() {
        return canExtIdentifierOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDataType() {
        return dataTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDLT_MessageType() {
        return dlT_MessageTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDLT_MessageTraceInfo() {
        return dlT_MessageTraceInfoEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDLT_MessageLogInfo() {
        return dlT_MessageLogInfoEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDLT_MessageControlInfo() {
        return dlT_MessageControlInfoEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getDLT_MessageBusInfo() {
        return dlT_MessageBusInfoEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getForEachExpressionModifierTemplate() {
        return forEachExpressionModifierTemplateEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getAUTOSARDataType() {
        return autosarDataTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getCANFrameTypeOrTemplatePlaceholderEnum() {
        return canFrameTypeOrTemplatePlaceholderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getEvaluationBehaviourOrTemplateDefinedEnum() {
        return evaluationBehaviourOrTemplateDefinedEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getSignalVariableLagEnum() {
        return signalVariableLagEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getTTLOrTemplatePlaceHolderEnum() {
        return ttlOrTemplatePlaceHolderEnumEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getFormatDataType() {
        return formatDataTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getCheckTypeObject() {
        return checkTypeObjectEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getComparatorObject() {
        return comparatorObjectEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getLogicalOperatorTypeObject() {
        return logicalOperatorTypeObjectEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getHexOrIntOrTemplatePlaceholder() {
        return hexOrIntOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getIntOrTemplatePlaceholder() {
        return intOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getLongOrTemplatePlaceholder() {
        return longOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getDoubleOrTemplatePlaceholder() {
        return doubleOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getDoubleOrHexOrTemplatePlaceholder() {
        return doubleOrHexOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getIPPatternOrTemplatePlaceholder() {
        return ipPatternOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getMACPatternOrTemplatePlaceholder() {
        return macPatternOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getPortPatternOrTemplatePlaceholder() {
        return portPatternOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getVlanIdPatternOrTemplatePlaceholder() {
        return vlanIdPatternOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getIPOrTemplatePlaceholder() {
        return ipOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getPortOrTemplatePlaceholder() {
        return portOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getVlanIdOrTemplatePlaceholder() {
        return vlanIdOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getMACOrTemplatePlaceholder() {
        return macOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getBitmaskOrTemplatePlaceholder() {
        return bitmaskOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getHexOrIntOrNullOrTemplatePlaceholder() {
        return hexOrIntOrNullOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getNonZeroDoubleOrTemplatePlaceholder() {
        return nonZeroDoubleOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getValVarInitialValueDoubleOrTemplatePlaceholderOrString() {
        return valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getByteOrTemplatePlaceholder() {
        return byteOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ConditionsFactory getConditionsFactory() {
        return (ConditionsFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        documentRootEClass = createEClass(DOCUMENT_ROOT);
        createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
        createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
        createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
        createEReference(documentRootEClass, DOCUMENT_ROOT__CONDITION_SET);
        createEReference(documentRootEClass, DOCUMENT_ROOT__CONDITIONS_DOCUMENT);

        conditionSetEClass = createEClass(CONDITION_SET);
        createEAttribute(conditionSetEClass, CONDITION_SET__GROUP);
        createEAttribute(conditionSetEClass, CONDITION_SET__BAUREIHE);
        createEAttribute(conditionSetEClass, CONDITION_SET__DESCRIPTION);
        createEAttribute(conditionSetEClass, CONDITION_SET__ISTUFE);
        createEAttribute(conditionSetEClass, CONDITION_SET__SCHEMAVERSION);
        createEAttribute(conditionSetEClass, CONDITION_SET__UUID);
        createEAttribute(conditionSetEClass, CONDITION_SET__VERSION);
        createEReference(conditionSetEClass, CONDITION_SET__CONDITIONS);
        createEAttribute(conditionSetEClass, CONDITION_SET__NAME);

        conditionEClass = createEClass(CONDITION);
        createEAttribute(conditionEClass, CONDITION__NAME);
        createEAttribute(conditionEClass, CONDITION__DESCRIPTION);
        createEReference(conditionEClass, CONDITION__EXPRESSION);

        signalComparisonExpressionEClass = createEClass(SIGNAL_COMPARISON_EXPRESSION);
        createEReference(signalComparisonExpressionEClass, SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL);
        createEAttribute(signalComparisonExpressionEClass, SIGNAL_COMPARISON_EXPRESSION__OPERATOR);
        createEAttribute(signalComparisonExpressionEClass, SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM);

        canMessageCheckExpressionEClass = createEClass(CAN_MESSAGE_CHECK_EXPRESSION);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__BUSID);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__TYPE);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER);
        createEAttribute(canMessageCheckExpressionEClass, CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM);

        linMessageCheckExpressionEClass = createEClass(LIN_MESSAGE_CHECK_EXPRESSION);
        createEAttribute(linMessageCheckExpressionEClass, LIN_MESSAGE_CHECK_EXPRESSION__BUSID);
        createEAttribute(linMessageCheckExpressionEClass, LIN_MESSAGE_CHECK_EXPRESSION__TYPE);
        createEAttribute(linMessageCheckExpressionEClass, LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS);
        createEAttribute(linMessageCheckExpressionEClass, LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM);
        createEAttribute(linMessageCheckExpressionEClass, LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM);

        stateCheckExpressionEClass = createEClass(STATE_CHECK_EXPRESSION);
        createEAttribute(stateCheckExpressionEClass, STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE);
        createEReference(stateCheckExpressionEClass, STATE_CHECK_EXPRESSION__CHECK_STATE);
        createEAttribute(stateCheckExpressionEClass, STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM);

        timingExpressionEClass = createEClass(TIMING_EXPRESSION);
        createEAttribute(timingExpressionEClass, TIMING_EXPRESSION__MAXTIME);
        createEAttribute(timingExpressionEClass, TIMING_EXPRESSION__MINTIME);

        trueExpressionEClass = createEClass(TRUE_EXPRESSION);

        logicalExpressionEClass = createEClass(LOGICAL_EXPRESSION);
        createEReference(logicalExpressionEClass, LOGICAL_EXPRESSION__EXPRESSIONS);
        createEAttribute(logicalExpressionEClass, LOGICAL_EXPRESSION__OPERATOR);
        createEAttribute(logicalExpressionEClass, LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM);

        referenceConditionExpressionEClass = createEClass(REFERENCE_CONDITION_EXPRESSION);
        createEReference(referenceConditionExpressionEClass, REFERENCE_CONDITION_EXPRESSION__CONDITION);

        transitionCheckExpressionEClass = createEClass(TRANSITION_CHECK_EXPRESSION);
        createEAttribute(transitionCheckExpressionEClass, TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE);
        createEReference(transitionCheckExpressionEClass, TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION);
        createEAttribute(transitionCheckExpressionEClass, TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM);

        flexRayMessageCheckExpressionEClass = createEClass(FLEX_RAY_MESSAGE_CHECK_EXPRESSION);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE);
        createEAttribute(flexRayMessageCheckExpressionEClass, FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM);

        stopExpressionEClass = createEClass(STOP_EXPRESSION);
        createEAttribute(stopExpressionEClass, STOP_EXPRESSION__ANALYSE_ART);
        createEAttribute(stopExpressionEClass, STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM);

        comparatorSignalEClass = createEClass(COMPARATOR_SIGNAL);
        createEAttribute(comparatorSignalEClass, COMPARATOR_SIGNAL__DESCRIPTION);

        constantComparatorValueEClass = createEClass(CONSTANT_COMPARATOR_VALUE);
        createEAttribute(constantComparatorValueEClass, CONSTANT_COMPARATOR_VALUE__VALUE);
        createEAttribute(constantComparatorValueEClass, CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE);
        createEAttribute(constantComparatorValueEClass, CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM);

        calculationExpressionEClass = createEClass(CALCULATION_EXPRESSION);
        createEAttribute(calculationExpressionEClass, CALCULATION_EXPRESSION__EXPRESSION);
        createEReference(calculationExpressionEClass, CALCULATION_EXPRESSION__OPERANDS);

        expressionEClass = createEClass(EXPRESSION);
        createEAttribute(expressionEClass, EXPRESSION__DESCRIPTION);

        variableReferenceEClass = createEClass(VARIABLE_REFERENCE);
        createEReference(variableReferenceEClass, VARIABLE_REFERENCE__VARIABLE);

        conditionsDocumentEClass = createEClass(CONDITIONS_DOCUMENT);
        createEReference(conditionsDocumentEClass, CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET);
        createEReference(conditionsDocumentEClass, CONDITIONS_DOCUMENT__VARIABLE_SET);
        createEReference(conditionsDocumentEClass, CONDITIONS_DOCUMENT__CONDITION_SET);

        variableSetEClass = createEClass(VARIABLE_SET);
        createEAttribute(variableSetEClass, VARIABLE_SET__UUID);
        createEReference(variableSetEClass, VARIABLE_SET__VARIABLES);

        variableEClass = createEClass(VARIABLE);
        createEAttribute(variableEClass, VARIABLE__DATA_TYPE);
        createEAttribute(variableEClass, VARIABLE__UNIT);
        createEReference(variableEClass, VARIABLE__VARIABLE_FORMAT);
        createEAttribute(variableEClass, VARIABLE__VARIABLE_FORMAT_TMPL_PARAM);

        signalVariableEClass = createEClass(SIGNAL_VARIABLE);
        createEAttribute(signalVariableEClass, SIGNAL_VARIABLE__INTERPRETED_VALUE);
        createEReference(signalVariableEClass, SIGNAL_VARIABLE__SIGNAL);
        createEAttribute(signalVariableEClass, SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM);
        createEReference(signalVariableEClass, SIGNAL_VARIABLE__SIGNAL_OBSERVERS);
        createEAttribute(signalVariableEClass, SIGNAL_VARIABLE__LAG);

        valueVariableEClass = createEClass(VALUE_VARIABLE);
        createEAttribute(valueVariableEClass, VALUE_VARIABLE__INITIAL_VALUE);
        createEReference(valueVariableEClass, VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS);

        variableFormatEClass = createEClass(VARIABLE_FORMAT);
        createEAttribute(variableFormatEClass, VARIABLE_FORMAT__DIGITS);
        createEAttribute(variableFormatEClass, VARIABLE_FORMAT__BASE_DATA_TYPE);
        createEAttribute(variableFormatEClass, VARIABLE_FORMAT__UPPER_CASE);

        abstractObserverEClass = createEClass(ABSTRACT_OBSERVER);
        createEAttribute(abstractObserverEClass, ABSTRACT_OBSERVER__NAME);
        createEAttribute(abstractObserverEClass, ABSTRACT_OBSERVER__DESCRIPTION);
        createEAttribute(abstractObserverEClass, ABSTRACT_OBSERVER__LOG_LEVEL);
        createEReference(abstractObserverEClass, ABSTRACT_OBSERVER__VALUE_RANGES);
        createEAttribute(abstractObserverEClass, ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM);
        createEAttribute(abstractObserverEClass, ABSTRACT_OBSERVER__ACTIVE_AT_START);

        observerValueRangeEClass = createEClass(OBSERVER_VALUE_RANGE);
        createEAttribute(observerValueRangeEClass, OBSERVER_VALUE_RANGE__VALUE);
        createEAttribute(observerValueRangeEClass, OBSERVER_VALUE_RANGE__DESCRIPTION);
        createEAttribute(observerValueRangeEClass, OBSERVER_VALUE_RANGE__VALUE_TYPE);

        signalObserverEClass = createEClass(SIGNAL_OBSERVER);

        signalReferenceSetEClass = createEClass(SIGNAL_REFERENCE_SET);
        createEReference(signalReferenceSetEClass, SIGNAL_REFERENCE_SET__MESSAGES);

        signalReferenceEClass = createEClass(SIGNAL_REFERENCE);
        createEReference(signalReferenceEClass, SIGNAL_REFERENCE__SIGNAL);

        iSignalOrReferenceEClass = createEClass(ISIGNAL_OR_REFERENCE);

        bitPatternComparatorValueEClass = createEClass(BIT_PATTERN_COMPARATOR_VALUE);
        createEAttribute(bitPatternComparatorValueEClass, BIT_PATTERN_COMPARATOR_VALUE__VALUE);
        createEAttribute(bitPatternComparatorValueEClass, BIT_PATTERN_COMPARATOR_VALUE__STARTBIT);

        notExpressionEClass = createEClass(NOT_EXPRESSION);
        createEReference(notExpressionEClass, NOT_EXPRESSION__EXPRESSION);
        createEAttribute(notExpressionEClass, NOT_EXPRESSION__OPERATOR);

        iOperandEClass = createEClass(IOPERAND);

        iComputeVariableActionOperandEClass = createEClass(ICOMPUTE_VARIABLE_ACTION_OPERAND);

        iStringOperandEClass = createEClass(ISTRING_OPERAND);

        iSignalComparisonExpressionOperandEClass = createEClass(ISIGNAL_COMPARISON_EXPRESSION_OPERAND);

        iOperationEClass = createEClass(IOPERATION);

        iStringOperationEClass = createEClass(ISTRING_OPERATION);

        iNumericOperandEClass = createEClass(INUMERIC_OPERAND);

        iNumericOperationEClass = createEClass(INUMERIC_OPERATION);

        valueVariableObserverEClass = createEClass(VALUE_VARIABLE_OBSERVER);

        parseStringToDoubleEClass = createEClass(PARSE_STRING_TO_DOUBLE);
        createEReference(parseStringToDoubleEClass, PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE);

        stringExpressionEClass = createEClass(STRING_EXPRESSION);

        matchesEClass = createEClass(MATCHES);
        createEReference(matchesEClass, MATCHES__STRING_TO_CHECK);

        extractEClass = createEClass(EXTRACT);
        createEReference(extractEClass, EXTRACT__STRING_TO_EXTRACT);
        createEAttribute(extractEClass, EXTRACT__GROUP_INDEX);

        substringEClass = createEClass(SUBSTRING);
        createEReference(substringEClass, SUBSTRING__STRING_TO_EXTRACT);
        createEAttribute(substringEClass, SUBSTRING__START_INDEX);
        createEAttribute(substringEClass, SUBSTRING__END_INDEX);

        parseNumericToStringEClass = createEClass(PARSE_NUMERIC_TO_STRING);
        createEReference(parseNumericToStringEClass, PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED);

        stringLengthEClass = createEClass(STRING_LENGTH);
        createEReference(stringLengthEClass, STRING_LENGTH__STRING_OPERAND);

        abstractMessageEClass = createEClass(ABSTRACT_MESSAGE);
        createEReference(abstractMessageEClass, ABSTRACT_MESSAGE__SIGNALS);
        createEAttribute(abstractMessageEClass, ABSTRACT_MESSAGE__NAME);

        someIPMessageEClass = createEClass(SOME_IP_MESSAGE);
        createEReference(someIPMessageEClass, SOME_IP_MESSAGE__SOME_IP_FILTER);

        abstractSignalEClass = createEClass(ABSTRACT_SIGNAL);
        createEAttribute(abstractSignalEClass, ABSTRACT_SIGNAL__NAME);
        createEReference(abstractSignalEClass, ABSTRACT_SIGNAL__DECODE_STRATEGY);
        createEReference(abstractSignalEClass, ABSTRACT_SIGNAL__EXTRACT_STRATEGY);

        containerSignalEClass = createEClass(CONTAINER_SIGNAL);
        createEReference(containerSignalEClass, CONTAINER_SIGNAL__CONTAINED_SIGNALS);

        abstractFilterEClass = createEClass(ABSTRACT_FILTER);
        createEAttribute(abstractFilterEClass, ABSTRACT_FILTER__PAYLOAD_LENGTH);

        ethernetFilterEClass = createEClass(ETHERNET_FILTER);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__SOURCE_MAC);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__DEST_MAC);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__ETHER_TYPE);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__INNER_VLAN_ID);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__OUTER_VLAN_ID);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__INNER_VLAN_CFI);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__OUTER_VLAN_CFI);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__INNER_VLAN_VID);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__OUTER_VLAN_VID);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__CRC);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__INNER_VLAN_TPID);
        createEAttribute(ethernetFilterEClass, ETHERNET_FILTER__OUTER_VLAN_TPID);

        udpFilterEClass = createEClass(UDP_FILTER);
        createEAttribute(udpFilterEClass, UDP_FILTER__CHECKSUM);

        tcpFilterEClass = createEClass(TCP_FILTER);
        createEAttribute(tcpFilterEClass, TCP_FILTER__SEQUENCE_NUMBER);
        createEAttribute(tcpFilterEClass, TCP_FILTER__ACKNOWLEDGEMENT_NUMBER);
        createEAttribute(tcpFilterEClass, TCP_FILTER__TCP_FLAGS);
        createEAttribute(tcpFilterEClass, TCP_FILTER__STREAM_ANALYSIS_FLAGS);
        createEAttribute(tcpFilterEClass, TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM);
        createEAttribute(tcpFilterEClass, TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET);

        iPv4FilterEClass = createEClass(IPV4_FILTER);
        createEAttribute(iPv4FilterEClass, IPV4_FILTER__PROTOCOL_TYPE);
        createEAttribute(iPv4FilterEClass, IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM);
        createEAttribute(iPv4FilterEClass, IPV4_FILTER__SOURCE_IP);
        createEAttribute(iPv4FilterEClass, IPV4_FILTER__DEST_IP);
        createEAttribute(iPv4FilterEClass, IPV4_FILTER__TIME_TO_LIVE);

        someIPFilterEClass = createEClass(SOME_IP_FILTER);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__SERVICE_ID);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__METHOD_TYPE);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__METHOD_ID);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__CLIENT_ID);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__SESSION_ID);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__PROTOCOL_VERSION);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__INTERFACE_VERSION);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__MESSAGE_TYPE);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__RETURN_CODE);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM);
        createEAttribute(someIPFilterEClass, SOME_IP_FILTER__SOME_IP_LENGTH);

        forEachExpressionEClass = createEClass(FOR_EACH_EXPRESSION);
        createEReference(forEachExpressionEClass, FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS);
        createEReference(forEachExpressionEClass, FOR_EACH_EXPRESSION__CONTAINER_SIGNAL);

        messageCheckExpressionEClass = createEClass(MESSAGE_CHECK_EXPRESSION);
        createEReference(messageCheckExpressionEClass, MESSAGE_CHECK_EXPRESSION__MESSAGE);

        someIPSDFilterEClass = createEClass(SOME_IPSD_FILTER);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__FLAGS);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__FLAGS_TMPL_PARAM);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__SD_TYPE);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__INSTANCE_ID);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__TTL);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__MAJOR_VERSION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__MINOR_VERSION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__EVENT_GROUP_ID);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__INDEX_FIRST_OPTION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__INDEX_SECOND_OPTION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__NUMBER_FIRST_OPTION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__NUMBER_SECOND_OPTION);
        createEAttribute(someIPSDFilterEClass, SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD);

        someIPSDMessageEClass = createEClass(SOME_IPSD_MESSAGE);
        createEReference(someIPSDMessageEClass, SOME_IPSD_MESSAGE__SOME_IPSD_FILTER);

        doubleSignalEClass = createEClass(DOUBLE_SIGNAL);
        createEAttribute(doubleSignalEClass, DOUBLE_SIGNAL__IGNORE_INVALID_VALUES);
        createEAttribute(doubleSignalEClass, DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM);

        stringSignalEClass = createEClass(STRING_SIGNAL);

        decodeStrategyEClass = createEClass(DECODE_STRATEGY);

        doubleDecodeStrategyEClass = createEClass(DOUBLE_DECODE_STRATEGY);
        createEAttribute(doubleDecodeStrategyEClass, DOUBLE_DECODE_STRATEGY__FACTOR);
        createEAttribute(doubleDecodeStrategyEClass, DOUBLE_DECODE_STRATEGY__OFFSET);

        extractStrategyEClass = createEClass(EXTRACT_STRATEGY);
        createEReference(extractStrategyEClass, EXTRACT_STRATEGY__ABSTRACT_SIGNAL);

        universalPayloadExtractStrategyEClass = createEClass(UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY);
        createEAttribute(universalPayloadExtractStrategyEClass, UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE);
        createEAttribute(universalPayloadExtractStrategyEClass, UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER);
        createEAttribute(universalPayloadExtractStrategyEClass, UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM);
        createEAttribute(universalPayloadExtractStrategyEClass, UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE);
        createEAttribute(universalPayloadExtractStrategyEClass, UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM);

        emptyExtractStrategyEClass = createEClass(EMPTY_EXTRACT_STRATEGY);

        canFilterEClass = createEClass(CAN_FILTER);
        createEAttribute(canFilterEClass, CAN_FILTER__MESSAGE_ID_RANGE);
        createEAttribute(canFilterEClass, CAN_FILTER__FRAME_ID);
        createEAttribute(canFilterEClass, CAN_FILTER__RXTX_FLAG);
        createEAttribute(canFilterEClass, CAN_FILTER__RXTX_FLAG_TMPL_PARAM);
        createEAttribute(canFilterEClass, CAN_FILTER__EXT_IDENTIFIER);
        createEAttribute(canFilterEClass, CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM);
        createEAttribute(canFilterEClass, CAN_FILTER__FRAME_TYPE);
        createEAttribute(canFilterEClass, CAN_FILTER__FRAME_TYPE_TMPL_PARAM);

        linFilterEClass = createEClass(LIN_FILTER);
        createEAttribute(linFilterEClass, LIN_FILTER__MESSAGE_ID_RANGE);
        createEAttribute(linFilterEClass, LIN_FILTER__FRAME_ID);

        flexRayFilterEClass = createEClass(FLEX_RAY_FILTER);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__MESSAGE_ID_RANGE);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__SLOT_ID);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__CYCLE_OFFSET);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__CYCLE_REPETITION);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__CHANNEL);
        createEAttribute(flexRayFilterEClass, FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM);

        dltFilterEClass = createEClass(DLT_FILTER);
        createEAttribute(dltFilterEClass, DLT_FILTER__ECU_ID_ECU);
        createEAttribute(dltFilterEClass, DLT_FILTER__SESSION_ID_SEID);
        createEAttribute(dltFilterEClass, DLT_FILTER__APPLICATION_ID_APID);
        createEAttribute(dltFilterEClass, DLT_FILTER__CONTEXT_ID_CTID);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_TYPE_MSTP);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_LOG_INFO_MSLI);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_TRACE_INFO_MSTI);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_BUS_INFO_MSBI);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM);
        createEAttribute(dltFilterEClass, DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM);

        udpnmFilterEClass = createEClass(UDPNM_FILTER);
        createEAttribute(udpnmFilterEClass, UDPNM_FILTER__SOURCE_NODE_IDENTIFIER);
        createEAttribute(udpnmFilterEClass, UDPNM_FILTER__CONTROL_BIT_VECTOR);
        createEAttribute(udpnmFilterEClass, UDPNM_FILTER__PWF_STATUS);
        createEAttribute(udpnmFilterEClass, UDPNM_FILTER__TEILNETZ_STATUS);

        canMessageEClass = createEClass(CAN_MESSAGE);
        createEReference(canMessageEClass, CAN_MESSAGE__CAN_FILTER);

        linMessageEClass = createEClass(LIN_MESSAGE);
        createEReference(linMessageEClass, LIN_MESSAGE__LIN_FILTER);

        flexRayMessageEClass = createEClass(FLEX_RAY_MESSAGE);
        createEReference(flexRayMessageEClass, FLEX_RAY_MESSAGE__FLEX_RAY_FILTER);

        dltMessageEClass = createEClass(DLT_MESSAGE);

        udpMessageEClass = createEClass(UDP_MESSAGE);
        createEReference(udpMessageEClass, UDP_MESSAGE__UDP_FILTER);

        tcpMessageEClass = createEClass(TCP_MESSAGE);
        createEReference(tcpMessageEClass, TCP_MESSAGE__TCP_FILTER);

        udpnmMessageEClass = createEClass(UDPNM_MESSAGE);
        createEReference(udpnmMessageEClass, UDPNM_MESSAGE__UDPNM_FILTER);

        verboseDLTMessageEClass = createEClass(VERBOSE_DLT_MESSAGE);
        createEReference(verboseDLTMessageEClass, VERBOSE_DLT_MESSAGE__DLT_FILTER);

        universalPayloadWithLegacyExtractStrategyEClass = createEClass(UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY);
        createEAttribute(universalPayloadWithLegacyExtractStrategyEClass, UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT);
        createEAttribute(universalPayloadWithLegacyExtractStrategyEClass, UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH);

        abstractBusMessageEClass = createEClass(ABSTRACT_BUS_MESSAGE);
        createEAttribute(abstractBusMessageEClass, ABSTRACT_BUS_MESSAGE__BUS_ID);
        createEAttribute(abstractBusMessageEClass, ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM);
        createEAttribute(abstractBusMessageEClass, ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE);
        createEReference(abstractBusMessageEClass, ABSTRACT_BUS_MESSAGE__FILTERS);

        pluginFilterEClass = createEClass(PLUGIN_FILTER);
        createEAttribute(pluginFilterEClass, PLUGIN_FILTER__PLUGIN_NAME);
        createEAttribute(pluginFilterEClass, PLUGIN_FILTER__PLUGIN_VERSION);

        pluginMessageEClass = createEClass(PLUGIN_MESSAGE);
        createEReference(pluginMessageEClass, PLUGIN_MESSAGE__PLUGIN_FILTER);

        pluginSignalEClass = createEClass(PLUGIN_SIGNAL);

        pluginStateExtractStrategyEClass = createEClass(PLUGIN_STATE_EXTRACT_STRATEGY);
        createEAttribute(pluginStateExtractStrategyEClass, PLUGIN_STATE_EXTRACT_STRATEGY__STATES);
        createEAttribute(pluginStateExtractStrategyEClass, PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM);
        createEAttribute(pluginStateExtractStrategyEClass, PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE);
        createEAttribute(pluginStateExtractStrategyEClass, PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM);

        pluginResultExtractStrategyEClass = createEClass(PLUGIN_RESULT_EXTRACT_STRATEGY);
        createEAttribute(pluginResultExtractStrategyEClass, PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE);

        emptyDecodeStrategyEClass = createEClass(EMPTY_DECODE_STRATEGY);

        pluginCheckExpressionEClass = createEClass(PLUGIN_CHECK_EXPRESSION);
        createEAttribute(pluginCheckExpressionEClass, PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR);
        createEReference(pluginCheckExpressionEClass, PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK);
        createEAttribute(pluginCheckExpressionEClass, PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM);

        baseClassWithIDEClass = createEClass(BASE_CLASS_WITH_ID);
        createEAttribute(baseClassWithIDEClass, BASE_CLASS_WITH_ID__ID);

        headerSignalEClass = createEClass(HEADER_SIGNAL);
        createEAttribute(headerSignalEClass, HEADER_SIGNAL__ATTRIBUTE);
        createEAttribute(headerSignalEClass, HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM);

        iVariableReaderWriterEClass = createEClass(IVARIABLE_READER_WRITER);

        iStateTransitionReferenceEClass = createEClass(ISTATE_TRANSITION_REFERENCE);

        tpFilterEClass = createEClass(TP_FILTER);
        createEAttribute(tpFilterEClass, TP_FILTER__SOURCE_PORT);
        createEAttribute(tpFilterEClass, TP_FILTER__DEST_PORT);

        baseClassWithSourceReferenceEClass = createEClass(BASE_CLASS_WITH_SOURCE_REFERENCE);
        createEReference(baseClassWithSourceReferenceEClass, BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE);

        sourceReferenceEClass = createEClass(SOURCE_REFERENCE);
        createEAttribute(sourceReferenceEClass, SOURCE_REFERENCE__SERIALIZED_REFERENCE);

        ethernetMessageEClass = createEClass(ETHERNET_MESSAGE);
        createEReference(ethernetMessageEClass, ETHERNET_MESSAGE__ETHERNET_FILTER);

        iPv4MessageEClass = createEClass(IPV4_MESSAGE);
        createEReference(iPv4MessageEClass, IPV4_MESSAGE__IPV4_FILTER);

        nonVerboseDLTMessageEClass = createEClass(NON_VERBOSE_DLT_MESSAGE);
        createEReference(nonVerboseDLTMessageEClass, NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER);

        stringDecodeStrategyEClass = createEClass(STRING_DECODE_STRATEGY);
        createEAttribute(stringDecodeStrategyEClass, STRING_DECODE_STRATEGY__STRING_TERMINATION);
        createEAttribute(stringDecodeStrategyEClass, STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM);

        nonVerboseDLTFilterEClass = createEClass(NON_VERBOSE_DLT_FILTER);
        createEAttribute(nonVerboseDLTFilterEClass, NON_VERBOSE_DLT_FILTER__MESSAGE_ID);
        createEAttribute(nonVerboseDLTFilterEClass, NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE);

        verboseDLTExtractStrategyEClass = createEClass(VERBOSE_DLT_EXTRACT_STRATEGY);

        regexOperationEClass = createEClass(REGEX_OPERATION);
        createEAttribute(regexOperationEClass, REGEX_OPERATION__REGEX);
        createEReference(regexOperationEClass, REGEX_OPERATION__DYNAMIC_REGEX);

        payloadFilterEClass = createEClass(PAYLOAD_FILTER);
        createEAttribute(payloadFilterEClass, PAYLOAD_FILTER__INDEX);
        createEAttribute(payloadFilterEClass, PAYLOAD_FILTER__MASK);
        createEAttribute(payloadFilterEClass, PAYLOAD_FILTER__VALUE);

        verboseDLTPayloadFilterEClass = createEClass(VERBOSE_DLT_PAYLOAD_FILTER);
        createEAttribute(verboseDLTPayloadFilterEClass, VERBOSE_DLT_PAYLOAD_FILTER__REGEX);
        createEAttribute(verboseDLTPayloadFilterEClass, VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY);

        abstractVariableEClass = createEClass(ABSTRACT_VARIABLE);
        createEAttribute(abstractVariableEClass, ABSTRACT_VARIABLE__NAME);
        createEAttribute(abstractVariableEClass, ABSTRACT_VARIABLE__DISPLAY_NAME);
        createEAttribute(abstractVariableEClass, ABSTRACT_VARIABLE__DESCRIPTION);

        computedVariableEClass = createEClass(COMPUTED_VARIABLE);
        createEReference(computedVariableEClass, COMPUTED_VARIABLE__OPERANDS);
        createEAttribute(computedVariableEClass, COMPUTED_VARIABLE__EXPRESSION);

        structureVariableEClass = createEClass(STRUCTURE_VARIABLE);
        createEReference(structureVariableEClass, STRUCTURE_VARIABLE__VARIABLES);
        createEReference(structureVariableEClass, STRUCTURE_VARIABLE__VARIABLE_REFERENCES);

        // Create enums
        checkTypeEEnum = createEEnum(CHECK_TYPE);
        comparatorEEnum = createEEnum(COMPARATOR);
        logicalOperatorTypeEEnum = createEEnum(LOGICAL_OPERATOR_TYPE);
        signalDataTypeEEnum = createEEnum(SIGNAL_DATA_TYPE);
        flexChannelTypeEEnum = createEEnum(FLEX_CHANNEL_TYPE);
        byteOrderTypeEEnum = createEEnum(BYTE_ORDER_TYPE);
        formatDataTypeEEnum = createEEnum(FORMAT_DATA_TYPE);
        flexRayHeaderFlagSelectionEEnum = createEEnum(FLEX_RAY_HEADER_FLAG_SELECTION);
        booleanOrTemplatePlaceholderEnumEEnum = createEEnum(BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM);
        protocolTypeOrTemplatePlaceholderEnumEEnum = createEEnum(PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        ethernetProtocolTypeSelectionEEnum = createEEnum(ETHERNET_PROTOCOL_TYPE_SELECTION);
        rxTxFlagTypeOrTemplatePlaceholderEnumEEnum = createEEnum(RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        observerValueTypeEEnum = createEEnum(OBSERVER_VALUE_TYPE);
        pluginStateValueTypeOrTemplatePlaceholderEnumEEnum = createEEnum(PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        analysisTypeOrTemplatePlaceholderEnumEEnum = createEEnum(ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPMessageTypeOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPMethodTypeOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPReturnCodeOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPSDFlagsOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPSDTypeOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum = createEEnum(STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM);
        someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum = createEEnum(SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM);
        canExtIdentifierOrTemplatePlaceholderEnumEEnum = createEEnum(CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM);
        dataTypeEEnum = createEEnum(DATA_TYPE);
        dlT_MessageTypeEEnum = createEEnum(DLT_MESSAGE_TYPE);
        dlT_MessageTraceInfoEEnum = createEEnum(DLT_MESSAGE_TRACE_INFO);
        dlT_MessageLogInfoEEnum = createEEnum(DLT_MESSAGE_LOG_INFO);
        dlT_MessageControlInfoEEnum = createEEnum(DLT_MESSAGE_CONTROL_INFO);
        dlT_MessageBusInfoEEnum = createEEnum(DLT_MESSAGE_BUS_INFO);
        forEachExpressionModifierTemplateEEnum = createEEnum(FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE);
        autosarDataTypeEEnum = createEEnum(AUTOSAR_DATA_TYPE);
        canFrameTypeOrTemplatePlaceholderEnumEEnum = createEEnum(CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM);
        evaluationBehaviourOrTemplateDefinedEnumEEnum = createEEnum(EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM);
        signalVariableLagEnumEEnum = createEEnum(SIGNAL_VARIABLE_LAG_ENUM);
        ttlOrTemplatePlaceHolderEnumEEnum = createEEnum(TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM);

        // Create data types
        checkTypeObjectEDataType = createEDataType(CHECK_TYPE_OBJECT);
        comparatorObjectEDataType = createEDataType(COMPARATOR_OBJECT);
        logicalOperatorTypeObjectEDataType = createEDataType(LOGICAL_OPERATOR_TYPE_OBJECT);
        hexOrIntOrTemplatePlaceholderEDataType = createEDataType(HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER);
        intOrTemplatePlaceholderEDataType = createEDataType(INT_OR_TEMPLATE_PLACEHOLDER);
        longOrTemplatePlaceholderEDataType = createEDataType(LONG_OR_TEMPLATE_PLACEHOLDER);
        doubleOrTemplatePlaceholderEDataType = createEDataType(DOUBLE_OR_TEMPLATE_PLACEHOLDER);
        doubleOrHexOrTemplatePlaceholderEDataType = createEDataType(DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER);
        ipPatternOrTemplatePlaceholderEDataType = createEDataType(IP_PATTERN_OR_TEMPLATE_PLACEHOLDER);
        macPatternOrTemplatePlaceholderEDataType = createEDataType(MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER);
        portPatternOrTemplatePlaceholderEDataType = createEDataType(PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER);
        vlanIdPatternOrTemplatePlaceholderEDataType = createEDataType(VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER);
        ipOrTemplatePlaceholderEDataType = createEDataType(IP_OR_TEMPLATE_PLACEHOLDER);
        portOrTemplatePlaceholderEDataType = createEDataType(PORT_OR_TEMPLATE_PLACEHOLDER);
        vlanIdOrTemplatePlaceholderEDataType = createEDataType(VLAN_ID_OR_TEMPLATE_PLACEHOLDER);
        macOrTemplatePlaceholderEDataType = createEDataType(MAC_OR_TEMPLATE_PLACEHOLDER);
        bitmaskOrTemplatePlaceholderEDataType = createEDataType(BITMASK_OR_TEMPLATE_PLACEHOLDER);
        hexOrIntOrNullOrTemplatePlaceholderEDataType = createEDataType(HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER);
        nonZeroDoubleOrTemplatePlaceholderEDataType = createEDataType(NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER);
        valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType = createEDataType(VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING);
        byteOrTemplatePlaceholderEDataType = createEDataType(BYTE_OR_TEMPLATE_PLACEHOLDER);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);
        StatemachinesetPackage theStatemachinesetPackage = (StatemachinesetPackage) EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);
        StatemachinePackage theStatemachinePackage = (StatemachinePackage) EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        conditionEClass.getESuperTypes().add(this.getBaseClassWithID());
        conditionEClass.getESuperTypes().add(this.getIVariableReaderWriter());
        conditionEClass.getESuperTypes().add(this.getIStateTransitionReference());
        conditionEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        signalComparisonExpressionEClass.getESuperTypes().add(this.getExpression());
        signalComparisonExpressionEClass.getESuperTypes().add(this.getIOperation());
        canMessageCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        linMessageCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        stateCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        timingExpressionEClass.getESuperTypes().add(this.getExpression());
        trueExpressionEClass.getESuperTypes().add(this.getExpression());
        logicalExpressionEClass.getESuperTypes().add(this.getExpression());
        referenceConditionExpressionEClass.getESuperTypes().add(this.getExpression());
        transitionCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        flexRayMessageCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        stopExpressionEClass.getESuperTypes().add(this.getExpression());
        comparatorSignalEClass.getESuperTypes().add(this.getISignalComparisonExpressionOperand());
        constantComparatorValueEClass.getESuperTypes().add(this.getComparatorSignal());
        calculationExpressionEClass.getESuperTypes().add(this.getComparatorSignal());
        calculationExpressionEClass.getESuperTypes().add(this.getIOperation());
        expressionEClass.getESuperTypes().add(this.getBaseClassWithID());
        expressionEClass.getESuperTypes().add(this.getIVariableReaderWriter());
        expressionEClass.getESuperTypes().add(this.getIStateTransitionReference());
        expressionEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        variableReferenceEClass.getESuperTypes().add(this.getComparatorSignal());
        variableReferenceEClass.getESuperTypes().add(this.getINumericOperand());
        variableReferenceEClass.getESuperTypes().add(this.getIStringOperand());
        variableReferenceEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        variableEClass.getESuperTypes().add(this.getAbstractVariable());
        signalVariableEClass.getESuperTypes().add(this.getVariable());
        valueVariableEClass.getESuperTypes().add(this.getVariable());
        abstractObserverEClass.getESuperTypes().add(this.getBaseClassWithSourceReference());
        abstractObserverEClass.getESuperTypes().add(theStatemachinePackage.getSmardTraceElement());
        abstractObserverEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        signalObserverEClass.getESuperTypes().add(this.getAbstractObserver());
        signalReferenceEClass.getESuperTypes().add(this.getComparatorSignal());
        signalReferenceEClass.getESuperTypes().add(this.getISignalOrReference());
        signalReferenceEClass.getESuperTypes().add(this.getINumericOperand());
        signalReferenceEClass.getESuperTypes().add(this.getIStringOperand());
        iSignalOrReferenceEClass.getESuperTypes().add(this.getISignalComparisonExpressionOperand());
        iSignalOrReferenceEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        bitPatternComparatorValueEClass.getESuperTypes().add(this.getComparatorSignal());
        notExpressionEClass.getESuperTypes().add(this.getExpression());
        iOperandEClass.getESuperTypes().add(this.getIVariableReaderWriter());
        iComputeVariableActionOperandEClass.getESuperTypes().add(this.getIOperand());
        iStringOperandEClass.getESuperTypes().add(this.getIOperand());
        iStringOperandEClass.getESuperTypes().add(this.getIComputeVariableActionOperand());
        iSignalComparisonExpressionOperandEClass.getESuperTypes().add(this.getIOperand());
        iSignalComparisonExpressionOperandEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        iOperationEClass.getESuperTypes().add(this.getIOperand());
        iOperationEClass.getESuperTypes().add(this.getISignalComparisonExpressionOperand());
        iStringOperationEClass.getESuperTypes().add(this.getIOperation());
        iStringOperationEClass.getESuperTypes().add(this.getIStringOperand());
        iNumericOperandEClass.getESuperTypes().add(this.getIOperand());
        iNumericOperandEClass.getESuperTypes().add(this.getIComputeVariableActionOperand());
        iNumericOperationEClass.getESuperTypes().add(this.getINumericOperand());
        iNumericOperationEClass.getESuperTypes().add(this.getIOperation());
        valueVariableObserverEClass.getESuperTypes().add(this.getAbstractObserver());
        parseStringToDoubleEClass.getESuperTypes().add(this.getINumericOperation());
        stringExpressionEClass.getESuperTypes().add(this.getExpression());
        matchesEClass.getESuperTypes().add(this.getStringExpression());
        matchesEClass.getESuperTypes().add(this.getRegexOperation());
        extractEClass.getESuperTypes().add(this.getIStringOperation());
        extractEClass.getESuperTypes().add(this.getRegexOperation());
        substringEClass.getESuperTypes().add(this.getIStringOperation());
        parseNumericToStringEClass.getESuperTypes().add(this.getIStringOperation());
        stringLengthEClass.getESuperTypes().add(this.getINumericOperation());
        abstractMessageEClass.getESuperTypes().add(this.getBaseClassWithSourceReference());
        someIPMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        abstractSignalEClass.getESuperTypes().add(this.getBaseClassWithSourceReference());
        abstractSignalEClass.getESuperTypes().add(this.getISignalOrReference());
        abstractSignalEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        containerSignalEClass.getESuperTypes().add(this.getAbstractSignal());
        abstractFilterEClass.getESuperTypes().add(this.getBaseClassWithSourceReference());
        ethernetFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        udpFilterEClass.getESuperTypes().add(this.getTPFilter());
        tcpFilterEClass.getESuperTypes().add(this.getTPFilter());
        iPv4FilterEClass.getESuperTypes().add(this.getAbstractFilter());
        someIPFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        forEachExpressionEClass.getESuperTypes().add(this.getExpression());
        messageCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        someIPSDFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        someIPSDMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        doubleSignalEClass.getESuperTypes().add(this.getAbstractSignal());
        stringSignalEClass.getESuperTypes().add(this.getAbstractSignal());
        doubleDecodeStrategyEClass.getESuperTypes().add(this.getDecodeStrategy());
        universalPayloadExtractStrategyEClass.getESuperTypes().add(this.getExtractStrategy());
        emptyExtractStrategyEClass.getESuperTypes().add(this.getExtractStrategy());
        canFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        linFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        flexRayFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        dltFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        udpnmFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        canMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        linMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        flexRayMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        dltMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        udpMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        tcpMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        udpnmMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        verboseDLTMessageEClass.getESuperTypes().add(this.getDLTMessage());
        universalPayloadWithLegacyExtractStrategyEClass.getESuperTypes().add(this.getUniversalPayloadExtractStrategy());
        abstractBusMessageEClass.getESuperTypes().add(this.getAbstractMessage());
        pluginFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        pluginMessageEClass.getESuperTypes().add(this.getAbstractMessage());
        pluginSignalEClass.getESuperTypes().add(this.getDoubleSignal());
        pluginStateExtractStrategyEClass.getESuperTypes().add(this.getExtractStrategy());
        pluginResultExtractStrategyEClass.getESuperTypes().add(this.getExtractStrategy());
        emptyDecodeStrategyEClass.getESuperTypes().add(this.getDecodeStrategy());
        pluginCheckExpressionEClass.getESuperTypes().add(this.getExpression());
        headerSignalEClass.getESuperTypes().add(this.getAbstractSignal());
        tpFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        baseClassWithSourceReferenceEClass.getESuperTypes().add(this.getBaseClassWithID());
        ethernetMessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        iPv4MessageEClass.getESuperTypes().add(this.getAbstractBusMessage());
        nonVerboseDLTMessageEClass.getESuperTypes().add(this.getDLTMessage());
        stringDecodeStrategyEClass.getESuperTypes().add(this.getDecodeStrategy());
        nonVerboseDLTFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        verboseDLTExtractStrategyEClass.getESuperTypes().add(this.getExtractStrategy());
        payloadFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        verboseDLTPayloadFilterEClass.getESuperTypes().add(this.getAbstractFilter());
        abstractVariableEClass.getESuperTypes().add(this.getBaseClassWithID());
        abstractVariableEClass.getESuperTypes().add(this.getIVariableReaderWriter());
        abstractVariableEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        computedVariableEClass.getESuperTypes().add(this.getVariable());
        structureVariableEClass.getESuperTypes().add(this.getAbstractVariable());

        // Initialize classes and features; add operations and parameters
        initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null,
                IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_ConditionSet(), this.getConditionSet(), null, "conditionSet", null, 0, 1, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
                IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_ConditionsDocument(), this.getConditionsDocument(), null, "conditionsDocument", null, 0, 1, null, IS_TRANSIENT,
                IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(conditionSetEClass, ConditionSet.class, "ConditionSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getConditionSet_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Baureihe(), ecorePackage.getEString(), "baureihe", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Description(), ecorePackage.getEString(), "description", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Istufe(), ecorePackage.getEString(), "istufe", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Schemaversion(), ecorePackage.getEString(), "schemaversion", "0", 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Uuid(), theXMLTypePackage.getID(), "uuid", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Version(), ecorePackage.getEString(), "version", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getConditionSet_Conditions(), this.getCondition(), null, "conditions", null, 0, -1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConditionSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, ConditionSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCondition_Name(), ecorePackage.getEString(), "name", null, 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCondition_Description(), ecorePackage.getEString(), "description", null, 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCondition_Expression(), this.getExpression(), null, "expression", null, 0, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        EOperation op = addEOperation(conditionEClass, ecorePackage.getEBoolean(), "isValid_hasValidName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        EGenericType g1 = createEGenericType(ecorePackage.getEMap());
        EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(conditionEClass, ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(signalComparisonExpressionEClass, SignalComparisonExpression.class, "SignalComparisonExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSignalComparisonExpression_ComparatorSignal(), this.getISignalComparisonExpressionOperand(), null, "comparatorSignal", null, 2, 2,
                SignalComparisonExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSignalComparisonExpression_Operator(), this.getComparator(), "operator", "EQ", 1, 1, SignalComparisonExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSignalComparisonExpression_OperatorTmplParam(), ecorePackage.getEString(), "operatorTmplParam", null, 0, 1,
                SignalComparisonExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(signalComparisonExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidComparator", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(signalComparisonExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidOperands", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(canMessageCheckExpressionEClass, CanMessageCheckExpression.class, "CanMessageCheckExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCanMessageCheckExpression_Busid(), this.getLongOrTemplatePlaceholder(), "busid", "0", 1, 1, CanMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_Type(), this.getCheckType(), "type", "ANY", 1, 1, CanMessageCheckExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_MessageIDs(), ecorePackage.getEString(), "messageIDs", null, 0, 1, CanMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_TypeTmplParam(), ecorePackage.getEString(), "typeTmplParam", null, 0, 1, CanMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_BusidTmplParam(), ecorePackage.getEString(), "busidTmplParam", null, 0, 1, CanMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_RxtxFlag(), this.getRxTxFlagTypeOrTemplatePlaceholderEnum(), "rxtxFlag", "ALL", 0, 1,
                CanMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_RxtxFlagTypeTmplParam(), ecorePackage.getEString(), "rxtxFlagTypeTmplParam", null, 0, 1,
                CanMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_ExtIdentifier(), this.getCanExtIdentifierOrTemplatePlaceholderEnum(), "extIdentifier", "ALL", 0, 1,
                CanMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCanMessageCheckExpression_ExtIdentifierTmplParam(), ecorePackage.getEString(), "extIdentifierTmplParam", null, 0, 1,
                CanMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(canMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidBusId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidCheckType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidRxTxFlag", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidExtIdentifier", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(linMessageCheckExpressionEClass, LinMessageCheckExpression.class, "LinMessageCheckExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLinMessageCheckExpression_Busid(), this.getLongOrTemplatePlaceholder(), "busid", "0", 1, 1, LinMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLinMessageCheckExpression_Type(), this.getCheckType(), "type", "ANY", 1, 1, LinMessageCheckExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLinMessageCheckExpression_MessageIDs(), ecorePackage.getEString(), "messageIDs", null, 0, 1, LinMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLinMessageCheckExpression_TypeTmplParam(), ecorePackage.getEString(), "typeTmplParam", null, 0, 1, LinMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLinMessageCheckExpression_BusidTmplParam(), ecorePackage.getEString(), "busidTmplParam", null, 0, 1, LinMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(linMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidBusId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(linMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(linMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidCheckType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(stateCheckExpressionEClass, StateCheckExpression.class, "StateCheckExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStateCheckExpression_CheckStateActive(), this.getBooleanOrTemplatePlaceholderEnum(), "checkStateActive", "true", 1, 1,
                StateCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStateCheckExpression_CheckState(), theStatemachinePackage.getState(), null, "checkState", null, 1, 1, StateCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStateCheckExpression_CheckStateActiveTmplParam(), ecorePackage.getEString(), "checkStateActiveTmplParam", null, 0, 1,
                StateCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(stateCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidStatesActive", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(stateCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidState", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(timingExpressionEClass, TimingExpression.class, "TimingExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTimingExpression_Maxtime(), this.getLongOrTemplatePlaceholder(), "maxtime", "0", 0, 1, TimingExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTimingExpression_Mintime(), this.getLongOrTemplatePlaceholder(), "mintime", "0", 0, 1, TimingExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(timingExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidMinMaxTimes", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(trueExpressionEClass, TrueExpression.class, "TrueExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(logicalExpressionEClass, LogicalExpression.class, "LogicalExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLogicalExpression_Expressions(), this.getExpression(), null, "expressions", null, 1, -1, LogicalExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLogicalExpression_Operator(), this.getLogicalOperatorType(), "operator", "AND", 1, 1, LogicalExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLogicalExpression_OperatorTmplParam(), ecorePackage.getEString(), "operatorTmplParam", null, 0, 1, LogicalExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(logicalExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidOperator", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(logicalExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidSubExpressions", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(referenceConditionExpressionEClass, ReferenceConditionExpression.class, "ReferenceConditionExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEReference(getReferenceConditionExpression_Condition(), this.getCondition(), null, "condition", null, 1, 1, ReferenceConditionExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(referenceConditionExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidReferencedCondition", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(transitionCheckExpressionEClass, TransitionCheckExpression.class, "TransitionCheckExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTransitionCheckExpression_StayActive(), this.getBooleanOrTemplatePlaceholderEnum(), "stayActive", "false", 1, 1,
                TransitionCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransitionCheckExpression_CheckTransition(), theStatemachinePackage.getTransition(), null, "checkTransition", null, 1, 1,
                TransitionCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTransitionCheckExpression_StayActiveTmplParam(), ecorePackage.getEString(), "stayActiveTmplParam", null, 0, 1,
                TransitionCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(transitionCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidStayActive", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(transitionCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidTransition", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(flexRayMessageCheckExpressionEClass, FlexRayMessageCheckExpression.class, "FlexRayMessageCheckExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getFlexRayMessageCheckExpression_BusId(), this.getIntOrTemplatePlaceholder(), "busId", "0", 0, 1, FlexRayMessageCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_PayloadPreamble(), this.getFlexRayHeaderFlagSelection(), "payloadPreamble", "ignore", 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_ZeroFrame(), this.getFlexRayHeaderFlagSelection(), "zeroFrame", "ignore", 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_SyncFrame(), this.getFlexRayHeaderFlagSelection(), "syncFrame", "ignore", 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_StartupFrame(), this.getFlexRayHeaderFlagSelection(), "startupFrame", "ignore", 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_NetworkMgmt(), this.getFlexRayHeaderFlagSelection(), "networkMgmt", "ignore", 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_FlexrayMessageId(), ecorePackage.getEString(), "flexrayMessageId",
                "<Channel>.<SlotID>.<Offset>.<Repetition>", 0, 1, FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_PayloadPreambleTmplParam(), ecorePackage.getEString(), "payloadPreambleTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_ZeroFrameTmplParam(), ecorePackage.getEString(), "zeroFrameTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_SyncFrameTmplParam(), ecorePackage.getEString(), "syncFrameTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_StartupFrameTmplParam(), ecorePackage.getEString(), "startupFrameTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_NetworkMgmtTmplParam(), ecorePackage.getEString(), "networkMgmtTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_BusIdTmplParam(), ecorePackage.getEString(), "busIdTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_Type(), this.getCheckType(), "type", "ANY", 1, 1, FlexRayMessageCheckExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayMessageCheckExpression_TypeTmplParam(), ecorePackage.getEString(), "typeTmplParam", null, 0, 1,
                FlexRayMessageCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidBusId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidCheckType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidStartupFrame", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidSyncFrame", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidZeroFrame", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidPayloadPreamble", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidNetworkMgmt", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayMessageCheckExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(stopExpressionEClass, StopExpression.class, "StopExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStopExpression_AnalyseArt(), this.getAnalysisTypeOrTemplatePlaceholderEnum(), "analyseArt", "ALL", 1, 1, StopExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStopExpression_AnalyseArtTmplParam(), ecorePackage.getEString(), "analyseArtTmplParam", null, 0, 1, StopExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(comparatorSignalEClass, ComparatorSignal.class, "ComparatorSignal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getComparatorSignal_Description(), ecorePackage.getEString(), "description", null, 0, 1, ComparatorSignal.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(constantComparatorValueEClass, ConstantComparatorValue.class, "ConstantComparatorValue", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getConstantComparatorValue_Value(), this.getDoubleOrHexOrTemplatePlaceholder(), "value", "0", 0, 1, ConstantComparatorValue.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConstantComparatorValue_InterpretedValue(), this.getBooleanOrTemplatePlaceholderEnum(), "interpretedValue", "true", 1, 1,
                ConstantComparatorValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getConstantComparatorValue_InterpretedValueTmplParam(), ecorePackage.getEString(), "interpretedValueTmplParam", null, 0, 1,
                ConstantComparatorValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(constantComparatorValueEClass, ecorePackage.getEBoolean(), "isValid_hasValidInterpretedValue", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(constantComparatorValueEClass, ecorePackage.getEBoolean(), "isValid_hasValidValue", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(calculationExpressionEClass, CalculationExpression.class, "CalculationExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCalculationExpression_Expression(), ecorePackage.getEString(), "Expression", null, 0, 1, CalculationExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getCalculationExpression_Operands(), this.getINumericOperand(), null, "operands", null, 0, -1, CalculationExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(calculationExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(calculationExpressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidOperands", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(expressionEClass, Expression.class, "Expression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getExpression_Description(), ecorePackage.getEString(), "description", null, 0, 1, Expression.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(expressionEClass, ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(variableReferenceEClass, VariableReference.class, "VariableReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getVariableReference_Variable(), this.getVariable(), null, "variable", null, 1, 1, VariableReference.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(conditionsDocumentEClass, ConditionsDocument.class, "ConditionsDocument", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getConditionsDocument_SignalReferencesSet(), this.getSignalReferenceSet(), null, "signalReferencesSet", null, 0, 1,
                ConditionsDocument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);
        initEReference(getConditionsDocument_VariableSet(), this.getVariableSet(), null, "variableSet", null, 0, 1, ConditionsDocument.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getConditionsDocument_ConditionSet(), this.getConditionSet(), null, "conditionSet", null, 0, 1, ConditionsDocument.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(variableSetEClass, VariableSet.class, "VariableSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getVariableSet_Uuid(), theXMLTypePackage.getID(), "uuid", null, 0, 1, VariableSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getVariableSet_Variables(), this.getAbstractVariable(), null, "variables", null, 0, -1, VariableSet.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(variableSetEClass, ecorePackage.getEBoolean(), "isValid_hasValidValueRanges", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(variableEClass, Variable.class, "Variable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getVariable_DataType(), this.getDataType(), "dataType", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVariable_Unit(), ecorePackage.getEString(), "unit", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getVariable_VariableFormat(), this.getVariableFormat(), null, "variableFormat", null, 0, 1, Variable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVariable_VariableFormatTmplParam(), ecorePackage.getEString(), "variableFormatTmplParam", null, 0, 1, Variable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(variableEClass, ecorePackage.getEBoolean(), "isValid_hasValidInterpretedValue", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(variableEClass, ecorePackage.getEBoolean(), "isValid_hasValidType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(variableEClass, ecorePackage.getEBoolean(), "isValid_hasValidUnit", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(variableEClass, ecorePackage.getEBoolean(), "isValid_hasValidVariableFormatTmplParam", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(variableEClass, null, "getObservers", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEEList());
        g2 = createEGenericType(this.getAbstractObserver());
        g1.getETypeArguments().add(g2);
        initEOperation(op, g1);

        initEClass(signalVariableEClass, SignalVariable.class, "SignalVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getSignalVariable_InterpretedValue(), this.getBooleanOrTemplatePlaceholderEnum(), "interpretedValue", "false", 0, 1,
                SignalVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSignalVariable_Signal(), this.getISignalOrReference(), null, "signal", null, 1, 1, SignalVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSignalVariable_InterpretedValueTmplParam(), ecorePackage.getEString(), "interpretedValueTmplParam", null, 0, 1, SignalVariable.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getSignalVariable_SignalObservers(), this.getSignalObserver(), null, "signalObservers", null, 0, -1, SignalVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSignalVariable_Lag(), this.getSignalVariableLagEnum(), "lag", "CURRENT", 1, 1, SignalVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(valueVariableEClass, ValueVariable.class, "ValueVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getValueVariable_InitialValue(), this.getValVarInitialValueDoubleOrTemplatePlaceholderOrString(), "initialValue", "0", 0, 1,
                ValueVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getValueVariable_ValueVariableObservers(), this.getValueVariableObserver(), null, "valueVariableObservers", null, 0, -1,
                ValueVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);

        initEClass(variableFormatEClass, VariableFormat.class, "VariableFormat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getVariableFormat_Digits(), ecorePackage.getEInt(), "digits", "0", 0, 1, VariableFormat.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVariableFormat_BaseDataType(), this.getFormatDataType(), "baseDataType", "Float", 1, 1, VariableFormat.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVariableFormat_UpperCase(), ecorePackage.getEBoolean(), "upperCase", "false", 1, 1, VariableFormat.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(variableFormatEClass, ecorePackage.getEBoolean(), "isValid_hasValidDigits", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(abstractObserverEClass, AbstractObserver.class, "AbstractObserver", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractObserver_Name(), ecorePackage.getEString(), "name", null, 0, 1, AbstractObserver.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractObserver_Description(), ecorePackage.getEString(), "description", null, 0, 1, AbstractObserver.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractObserver_LogLevel(), this.getIntOrTemplatePlaceholder(), "logLevel", "0", 0, 1, AbstractObserver.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractObserver_ValueRanges(), this.getObserverValueRange(), null, "valueRanges", null, 0, -1, AbstractObserver.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractObserver_ValueRangesTmplParam(), ecorePackage.getEString(), "valueRangesTmplParam", null, 0, 1, AbstractObserver.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractObserver_ActiveAtStart(), ecorePackage.getEBoolean(), "activeAtStart", "true", 1, 1, AbstractObserver.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(abstractObserverEClass, ecorePackage.getEBoolean(), "isValid_hasValidName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractObserverEClass, ecorePackage.getEBoolean(), "isValid_hasValidLogLevel", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractObserverEClass, ecorePackage.getEBoolean(), "isValid_hasValidValueRanges", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEEList());
        g2 = createEGenericType(ecorePackage.getEString());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "errorMessages", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEEList());
        g2 = createEGenericType(ecorePackage.getEString());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "warningMessages", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractObserverEClass, ecorePackage.getEBoolean(), "isValid_hasValidValueRangesTmplParam", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(observerValueRangeEClass, ObserverValueRange.class, "ObserverValueRange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getObserverValueRange_Value(), ecorePackage.getEString(), "value", null, 0, 1, ObserverValueRange.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getObserverValueRange_Description(), ecorePackage.getEString(), "description", null, 0, 1, ObserverValueRange.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getObserverValueRange_ValueType(), this.getObserverValueType(), "valueType", "INFO", 0, 1, ObserverValueRange.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(observerValueRangeEClass, ecorePackage.getEBoolean(), "isValid_hasValidValue", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(observerValueRangeEClass, ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(signalObserverEClass, SignalObserver.class, "SignalObserver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(signalReferenceSetEClass, SignalReferenceSet.class, "SignalReferenceSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSignalReferenceSet_Messages(), this.getAbstractMessage(), null, "messages", null, 0, -1, SignalReferenceSet.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(signalReferenceEClass, SignalReference.class, "SignalReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSignalReference_Signal(), this.getISignalOrReference(), null, "signal", null, 1, 1, SignalReference.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(signalReferenceEClass, ecorePackage.getEBoolean(), "isValid_hasValidSignal", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(iSignalOrReferenceEClass, ISignalOrReference.class, "ISignalOrReference", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(bitPatternComparatorValueEClass, BitPatternComparatorValue.class, "BitPatternComparatorValue", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getBitPatternComparatorValue_Value(), ecorePackage.getEString(), "value", "", 0, 1, BitPatternComparatorValue.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getBitPatternComparatorValue_Startbit(), this.getIntOrTemplatePlaceholder(), "startbit", "0", 0, 1, BitPatternComparatorValue.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(bitPatternComparatorValueEClass, ecorePackage.getEBoolean(), "isValid_hasValidBitPattern", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(bitPatternComparatorValueEClass, ecorePackage.getEBoolean(), "isValid_hasValidStartbit", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(notExpressionEClass, NotExpression.class, "NotExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getNotExpression_Expression(), this.getExpression(), null, "expression", null, 1, 1, NotExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getNotExpression_Operator(), this.getLogicalOperatorType(), "operator", "NOT", 1, 1, NotExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
                !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(iOperandEClass, IOperand.class, "IOperand", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        addEOperation(iOperandEClass, this.getDataType(), "get_EvaluationDataType", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(iComputeVariableActionOperandEClass, IComputeVariableActionOperand.class, "IComputeVariableActionOperand", IS_ABSTRACT, IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);

        initEClass(iStringOperandEClass, IStringOperand.class, "IStringOperand", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(iSignalComparisonExpressionOperandEClass, ISignalComparisonExpressionOperand.class, "ISignalComparisonExpressionOperand", IS_ABSTRACT,
                IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(iOperationEClass, IOperation.class, "IOperation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        addEOperation(iOperationEClass, this.getDataType(), "get_OperandDataType", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(iOperationEClass, null, "get_Operands", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEEList());
        g2 = createEGenericType(this.getIOperand());
        g1.getETypeArguments().add(g2);
        initEOperation(op, g1);

        initEClass(iStringOperationEClass, IStringOperation.class, "IStringOperation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(iNumericOperandEClass, INumericOperand.class, "INumericOperand", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(iNumericOperationEClass, INumericOperation.class, "INumericOperation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(valueVariableObserverEClass, ValueVariableObserver.class, "ValueVariableObserver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(parseStringToDoubleEClass, ParseStringToDouble.class, "ParseStringToDouble", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getParseStringToDouble_StringToParse(), this.getIStringOperand(), null, "stringToParse", null, 1, 1, ParseStringToDouble.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(stringExpressionEClass, StringExpression.class, "StringExpression", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(matchesEClass, Matches.class, "Matches", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMatches_StringToCheck(), this.getIStringOperand(), null, "stringToCheck", null, 0, 1, Matches.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(matchesEClass, ecorePackage.getEBoolean(), "isValid_hasValidStringToCheck", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(extractEClass, Extract.class, "Extract", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getExtract_StringToExtract(), this.getIStringOperand(), null, "stringToExtract", null, 1, 1, Extract.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getExtract_GroupIndex(), ecorePackage.getEInt(), "groupIndex", "0", 0, 1, Extract.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(substringEClass, Substring.class, "Substring", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSubstring_StringToExtract(), this.getIStringOperand(), null, "stringToExtract", null, 1, 1, Substring.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSubstring_StartIndex(), ecorePackage.getEInt(), "startIndex", "0", 0, 1, Substring.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSubstring_EndIndex(), ecorePackage.getEInt(), "endIndex", "-1", 0, 1, Substring.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(parseNumericToStringEClass, ParseNumericToString.class, "ParseNumericToString", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getParseNumericToString_NumericOperandToBeParsed(), this.getINumericOperand(), null, "numericOperandToBeParsed", null, 1, 1,
                ParseNumericToString.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(stringLengthEClass, StringLength.class, "StringLength", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getStringLength_StringOperand(), this.getIStringOperand(), null, "stringOperand", null, 1, 1, StringLength.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(abstractMessageEClass, AbstractMessage.class, "AbstractMessage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getAbstractMessage_Signals(), this.getAbstractSignal(), null, "signals", null, 0, -1, AbstractMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractMessage_Name(), ecorePackage.getEString(), "name", null, 0, 1, AbstractMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        addEOperation(abstractMessageEClass, this.getAbstractFilter(), "getPrimaryFilter", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractMessageEClass, null, "getFilters", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEBoolean(), "includingPrimaryFilter", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEEList());
        g2 = createEGenericType(this.getAbstractFilter());
        g1.getETypeArguments().add(g2);
        initEOperation(op, g1);

        initEClass(someIPMessageEClass, SomeIPMessage.class, "SomeIPMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSomeIPMessage_SomeIPFilter(), this.getSomeIPFilter(), null, "someIPFilter", null, 1, 1, SomeIPMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(abstractSignalEClass, AbstractSignal.class, "AbstractSignal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractSignal_Name(), ecorePackage.getEString(), "name", null, 0, 1, AbstractSignal.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractSignal_DecodeStrategy(), this.getDecodeStrategy(), null, "decodeStrategy", null, 1, 1, AbstractSignal.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractSignal_ExtractStrategy(), this.getExtractStrategy(), this.getExtractStrategy_AbstractSignal(), "extractStrategy", null, 1, 1,
                AbstractSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);

        addEOperation(abstractSignalEClass, this.getAbstractMessage(), "getMessage", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(containerSignalEClass, ContainerSignal.class, "ContainerSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getContainerSignal_ContainedSignals(), this.getAbstractSignal(), null, "containedSignals", null, 0, -1, ContainerSignal.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(abstractFilterEClass, AbstractFilter.class, "AbstractFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractFilter_PayloadLength(), this.getLongOrTemplatePlaceholder(), "payloadLength", null, 0, 1, AbstractFilter.class, IS_TRANSIENT,
                !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        addEOperation(abstractFilterEClass, ecorePackage.getEBoolean(), "isNeedsErrorFrames", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(ethernetFilterEClass, EthernetFilter.class, "EthernetFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getEthernetFilter_SourceMAC(), this.getMACPatternOrTemplatePlaceholder(), "sourceMAC", null, 0, 1, EthernetFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_DestMAC(), this.getMACPatternOrTemplatePlaceholder(), "destMAC", null, 0, 1, EthernetFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_EtherType(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "etherType", "0x0800", 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_InnerVlanId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "innerVlanId", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_OuterVlanId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "outerVlanId", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_InnerVlanCFI(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "innerVlanCFI", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_OuterVlanCFI(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "outerVlanCFI", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_InnerVlanVID(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "innerVlanVID", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_OuterVlanVID(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "outerVlanVID", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_CRC(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "CRC", null, 0, 1, EthernetFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_InnerVlanTPID(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "innerVlanTPID", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getEthernetFilter_OuterVlanTPID(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "outerVlanTPID", null, 0, 1, EthernetFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSourceMAC", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidDestinationMAC", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidCRC", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidEtherType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInnerVlanVid", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidOuterVlanVid", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInnerVlanCFI", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidOuterVlanCFI", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInnerVlanPCP", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidOuterVlanPCP", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInnerVlanTPID", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(ethernetFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidOuterVlanTPID", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(udpFilterEClass, UDPFilter.class, "UDPFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getUDPFilter_Checksum(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "checksum", null, 0, 1, UDPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(udpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSourcePort", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(udpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidDestinationPort", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(udpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidChecksum", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(tcpFilterEClass, TCPFilter.class, "TCPFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTCPFilter_SequenceNumber(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "sequenceNumber", null, 0, 1, TCPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTCPFilter_AcknowledgementNumber(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "acknowledgementNumber", null, 0, 1,
                TCPFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTCPFilter_TcpFlags(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "tcpFlags", null, 0, 1, TCPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTCPFilter_StreamAnalysisFlags(), this.getStreamAnalysisFlagsOrTemplatePlaceholderEnum(), "streamAnalysisFlags", "ALL", 0, 1,
                TCPFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTCPFilter_StreamAnalysisFlagsTmplParam(), ecorePackage.getEString(), "streamAnalysisFlagsTmplParam", null, 0, 1, TCPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTCPFilter_StreamValidPayloadOffset(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "streamValidPayloadOffset", null, 0, 1,
                TCPFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSourcePort", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidDestinationPort", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSequenceNumber", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidAcknowledgementNumber", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidTcpFlag", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidStreamAnalysisFlag", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(tcpFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidStreamValidPayloadOffset", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(iPv4FilterEClass, IPv4Filter.class, "IPv4Filter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getIPv4Filter_ProtocolType(), this.getProtocolTypeOrTemplatePlaceholderEnum(), "protocolType", "ALL", 0, 1, IPv4Filter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIPv4Filter_ProtocolTypeTmplParam(), ecorePackage.getEString(), "protocolTypeTmplParam", null, 0, 1, IPv4Filter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIPv4Filter_SourceIP(), this.getIPPatternOrTemplatePlaceholder(), "sourceIP", null, 0, 1, IPv4Filter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIPv4Filter_DestIP(), this.getIPPatternOrTemplatePlaceholder(), "destIP", null, 0, 1, IPv4Filter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getIPv4Filter_TimeToLive(), this.getIntOrTemplatePlaceholder(), "timeToLive", null, 0, 1, IPv4Filter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(iPv4FilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSourceIpAddress", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(iPv4FilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidDestinationIpAddress", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(iPv4FilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidTimeToLive", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(someIPFilterEClass, SomeIPFilter.class, "SomeIPFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getSomeIPFilter_ServiceId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "serviceId", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_MethodType(), this.getSomeIPMethodTypeOrTemplatePlaceholderEnum(), "methodType", "ALL", 0, 1, SomeIPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_MethodTypeTmplParam(), ecorePackage.getEString(), "methodTypeTmplParam", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_MethodId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "methodId", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_ClientId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "clientId", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_SessionId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "sessionId", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_ProtocolVersion(), this.getIntOrTemplatePlaceholder(), "protocolVersion", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_InterfaceVersion(), this.getIntOrTemplatePlaceholder(), "interfaceVersion", null, 0, 1, SomeIPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_MessageType(), this.getSomeIPMessageTypeOrTemplatePlaceholderEnum(), "messageType", "ALL", 0, 1, SomeIPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_MessageTypeTmplParam(), ecorePackage.getEString(), "messageTypeTmplParam", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_ReturnCode(), this.getSomeIPReturnCodeOrTemplatePlaceholderEnum(), "returnCode", "ALL", 0, 1, SomeIPFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_ReturnCodeTmplParam(), ecorePackage.getEString(), "returnCodeTmplParam", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPFilter_SomeIPLength(), this.getIntOrTemplatePlaceholder(), "someIPLength", null, 0, 1, SomeIPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidServiceId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMethodId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSessionId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidClientId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidLength", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidProtocolVersion", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInterfaceVersion", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidReturnCode", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMethodType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(forEachExpressionEClass, ForEachExpression.class, "ForEachExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getForEachExpression_FilterExpressions(), this.getExpression(), null, "filterExpressions", null, 1, -1, ForEachExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getForEachExpression_ContainerSignal(), this.getContainerSignal(), null, "containerSignal", null, 0, 1, ForEachExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(messageCheckExpressionEClass, MessageCheckExpression.class, "MessageCheckExpression", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEReference(getMessageCheckExpression_Message(), this.getAbstractMessage(), null, "message", null, 1, 1, MessageCheckExpression.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(someIPSDFilterEClass, SomeIPSDFilter.class, "SomeIPSDFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getSomeIPSDFilter_Flags(), this.getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum(), "flags", "ALL", 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_FlagsTmplParam(), ecorePackage.getEString(), "flagsTmplParam", null, 0, 1, SomeIPSDFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_SdType(), this.getSomeIPSDTypeOrTemplatePlaceholderEnum(), "sdType", "ALL", 0, 1, SomeIPSDFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_SdTypeTmplParam(), ecorePackage.getEString(), "sdTypeTmplParam", null, 0, 1, SomeIPSDFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_InstanceId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "instanceId", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_Ttl(), this.getTTLOrTemplatePlaceHolderEnum(), "ttl", null, 0, 1, SomeIPSDFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_MajorVersion(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "majorVersion", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_MinorVersion(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "minorVersion", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_EventGroupId(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "eventGroupId", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_IndexFirstOption(), this.getIntOrTemplatePlaceholder(), "indexFirstOption", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_IndexSecondOption(), this.getIntOrTemplatePlaceholder(), "indexSecondOption", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_NumberFirstOption(), this.getIntOrTemplatePlaceholder(), "numberFirstOption", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_NumberSecondOption(), this.getIntOrTemplatePlaceholder(), "numberSecondOption", null, 0, 1, SomeIPSDFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getSomeIPSDFilter_ServiceId_SomeIPSD(), this.getHexOrIntOrNullOrTemplatePlaceholder(), "serviceId_SomeIPSD", null, 0, 1,
                SomeIPSDFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidInstanceId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidEventGroupId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidFlags", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSdType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMajorVersion", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMinorVersion", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidIndexFirstOption", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidIndexSecondOption", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidNumberFirstOption", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(someIPSDFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidNumberSecondOption", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(someIPSDMessageEClass, SomeIPSDMessage.class, "SomeIPSDMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getSomeIPSDMessage_SomeIPSDFilter(), this.getSomeIPSDFilter(), null, "someIPSDFilter", null, 1, 1, SomeIPSDMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(doubleSignalEClass, DoubleSignal.class, "DoubleSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDoubleSignal_IgnoreInvalidValues(), this.getBooleanOrTemplatePlaceholderEnum(), "ignoreInvalidValues", "false", 1, 1,
                DoubleSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDoubleSignal_IgnoreInvalidValuesTmplParam(), ecorePackage.getEString(), "ignoreInvalidValuesTmplParam", null, 0, 1,
                DoubleSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(stringSignalEClass, StringSignal.class, "StringSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(decodeStrategyEClass, DecodeStrategy.class, "DecodeStrategy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(doubleDecodeStrategyEClass, DoubleDecodeStrategy.class, "DoubleDecodeStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDoubleDecodeStrategy_Factor(), this.getDoubleOrHexOrTemplatePlaceholder(), "factor", "1.0", 0, 1, DoubleDecodeStrategy.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDoubleDecodeStrategy_Offset(), this.getDoubleOrHexOrTemplatePlaceholder(), "offset", "0.0", 0, 1, DoubleDecodeStrategy.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(doubleDecodeStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidFactor", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(doubleDecodeStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidOffset", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(extractStrategyEClass, ExtractStrategy.class, "ExtractStrategy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getExtractStrategy_AbstractSignal(), this.getAbstractSignal(), this.getAbstractSignal_ExtractStrategy(), "abstractSignal", null, 1, 1,
                ExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);

        initEClass(universalPayloadExtractStrategyEClass, UniversalPayloadExtractStrategy.class, "UniversalPayloadExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getUniversalPayloadExtractStrategy_ExtractionRule(), ecorePackage.getEString(), "extractionRule", "", 0, 1,
                UniversalPayloadExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUniversalPayloadExtractStrategy_ByteOrder(), this.getByteOrderType(), "byteOrder", "INTEL", 0, 1,
                UniversalPayloadExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam(), ecorePackage.getEString(), "extractionRuleTmplParam", null, 0, 1,
                UniversalPayloadExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUniversalPayloadExtractStrategy_SignalDataType(), this.getAUTOSARDataType(), "signalDataType", null, 0, 1,
                UniversalPayloadExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam(), ecorePackage.getEString(), "signalDataTypeTmplParam", null, 0, 1,
                UniversalPayloadExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(universalPayloadExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidExtractString", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(universalPayloadExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidSignalDataType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(emptyExtractStrategyEClass, EmptyExtractStrategy.class, "EmptyExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(canFilterEClass, CANFilter.class, "CANFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCANFilter_MessageIdRange(), ecorePackage.getEString(), "messageIdRange", null, 0, 1, CANFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_FrameId(), this.getHexOrIntOrTemplatePlaceholder(), "frameId", "0x0", 0, 1, CANFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_RxtxFlag(), this.getRxTxFlagTypeOrTemplatePlaceholderEnum(), "rxtxFlag", "ALL", 0, 1, CANFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_RxtxFlagTmplParam(), ecorePackage.getEString(), "rxtxFlagTmplParam", null, 0, 1, CANFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_ExtIdentifier(), this.getCanExtIdentifierOrTemplatePlaceholderEnum(), "extIdentifier", "ALL", 0, 1, CANFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_ExtIdentifierTmplParam(), ecorePackage.getEString(), "extIdentifierTmplParam", null, 0, 1, CANFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_FrameType(), this.getCANFrameTypeOrTemplatePlaceholderEnum(), "frameType", "Standard", 0, 1, CANFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCANFilter_FrameTypeTmplParam(), ecorePackage.getEString(), "frameTypeTmplParam", null, 0, 1, CANFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(canFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidFrameIdOrFrameIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidRxTxFlag", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(canFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidExtIdentifier", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(linFilterEClass, LINFilter.class, "LINFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLINFilter_MessageIdRange(), ecorePackage.getEString(), "messageIdRange", null, 0, 1, LINFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLINFilter_FrameId(), this.getHexOrIntOrTemplatePlaceholder(), "frameId", "0x0", 0, 1, LINFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(linFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidFrameIdOrFrameIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(flexRayFilterEClass, FlexRayFilter.class, "FlexRayFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getFlexRayFilter_MessageIdRange(), ecorePackage.getEString(), "messageIdRange", null, 0, 1, FlexRayFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayFilter_SlotId(), this.getIntOrTemplatePlaceholder(), "slotId", "0", 0, 1, FlexRayFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayFilter_CycleOffset(), this.getIntOrTemplatePlaceholder(), "cycleOffset", "0", 0, 1, FlexRayFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayFilter_CycleRepetition(), this.getIntOrTemplatePlaceholder(), "cycleRepetition", "1", 0, 1, FlexRayFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayFilter_Channel(), this.getFlexChannelType(), "channel", "A", 0, 1, FlexRayFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getFlexRayFilter_ChannelTmplParam(), ecorePackage.getEString(), "channelTmplParam", null, 0, 1, FlexRayFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(flexRayFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidFrameIdOrFrameIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidChannelType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidCycleOffset", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(flexRayFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidCycleRepeatInterval", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(dltFilterEClass, DLTFilter.class, "DLTFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDLTFilter_EcuID_ECU(), ecorePackage.getEString(), "ecuID_ECU", null, 0, 1, DLTFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_SessionID_SEID(), this.getLongOrTemplatePlaceholder(), "sessionID_SEID", null, 0, 1, DLTFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_ApplicationID_APID(), ecorePackage.getEString(), "applicationID_APID", null, 0, 1, DLTFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_ContextID_CTID(), ecorePackage.getEString(), "contextID_CTID", null, 0, 1, DLTFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageType_MSTP(), this.getDLT_MessageType(), "messageType_MSTP", "NOT_SPECIFIED", 1, 1, DLTFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageLogInfo_MSLI(), this.getDLT_MessageLogInfo(), "messageLogInfo_MSLI", "NOT_SPECIFIED", 1, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageTraceInfo_MSTI(), this.getDLT_MessageTraceInfo(), "messageTraceInfo_MSTI", "NOT_SPECIFIED", 1, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageBusInfo_MSBI(), this.getDLT_MessageBusInfo(), "messageBusInfo_MSBI", "NOT_SPECIFIED", 1, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageControlInfo_MSCI(), this.getDLT_MessageControlInfo(), "messageControlInfo_MSCI", "NOT_SPECIFIED", 1, 1,
                DLTFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageLogInfo_MSLITmplParam(), ecorePackage.getEString(), "messageLogInfo_MSLITmplParam", null, 0, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageTraceInfo_MSTITmplParam(), ecorePackage.getEString(), "messageTraceInfo_MSTITmplParam", null, 0, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageBusInfo_MSBITmplParam(), ecorePackage.getEString(), "messageBusInfo_MSBITmplParam", null, 0, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageControlInfo_MSCITmplParam(), ecorePackage.getEString(), "messageControlInfo_MSCITmplParam", null, 0, 1,
                DLTFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getDLTFilter_MessageType_MSTPTmplParam(), ecorePackage.getEString(), "messageType_MSTPTmplParam", null, 0, 1, DLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageBusInfo", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageControlInfo", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageLogInfo", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageTraceInfo", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(dltFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidContextIDOrApplicationID", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(udpnmFilterEClass, UDPNMFilter.class, "UDPNMFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getUDPNMFilter_SourceNodeIdentifier(), this.getIntOrTemplatePlaceholder(), "sourceNodeIdentifier", "", 0, 1, UDPNMFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUDPNMFilter_ControlBitVector(), this.getIntOrTemplatePlaceholder(), "controlBitVector", "", 0, 1, UDPNMFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUDPNMFilter_PwfStatus(), this.getIntOrTemplatePlaceholder(), "pwfStatus", "", 0, 1, UDPNMFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUDPNMFilter_TeilnetzStatus(), this.getIntOrTemplatePlaceholder(), "teilnetzStatus", "", 0, 1, UDPNMFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(udpnmFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidSourceNodeIdentifier", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(udpnmFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidControlBitVector", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(udpnmFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidPwfStatus", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(udpnmFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidTeilnetzStatus", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(canMessageEClass, CANMessage.class, "CANMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getCANMessage_CanFilter(), this.getCANFilter(), null, "canFilter", null, 1, 1, CANMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(linMessageEClass, LINMessage.class, "LINMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getLINMessage_LinFilter(), this.getLINFilter(), null, "linFilter", null, 1, 1, LINMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(flexRayMessageEClass, FlexRayMessage.class, "FlexRayMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getFlexRayMessage_FlexRayFilter(), this.getFlexRayFilter(), null, "flexRayFilter", null, 1, 1, FlexRayMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(dltMessageEClass, DLTMessage.class, "DLTMessage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(udpMessageEClass, UDPMessage.class, "UDPMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getUDPMessage_UdpFilter(), this.getUDPFilter(), null, "udpFilter", null, 1, 1, UDPMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(tcpMessageEClass, TCPMessage.class, "TCPMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getTCPMessage_TcpFilter(), this.getTCPFilter(), null, "tcpFilter", null, 1, 1, TCPMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(udpnmMessageEClass, UDPNMMessage.class, "UDPNMMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getUDPNMMessage_UdpnmFilter(), this.getUDPNMFilter(), null, "udpnmFilter", null, 1, 1, UDPNMMessage.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(udpnmMessageEClass, ecorePackage.getEBoolean(), "isValid_hasValidUDPFilter", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(verboseDLTMessageEClass, VerboseDLTMessage.class, "VerboseDLTMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getVerboseDLTMessage_DltFilter(), this.getDLTFilter(), null, "dltFilter", null, 1, 1, VerboseDLTMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(universalPayloadWithLegacyExtractStrategyEClass, UniversalPayloadWithLegacyExtractStrategy.class,
                "UniversalPayloadWithLegacyExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getUniversalPayloadWithLegacyExtractStrategy_StartBit(), this.getIntOrTemplatePlaceholder(), "startBit", "0", 0, 1,
                UniversalPayloadWithLegacyExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);
        initEAttribute(getUniversalPayloadWithLegacyExtractStrategy_Length(), this.getIntOrTemplatePlaceholder(), "length", "1", 0, 1,
                UniversalPayloadWithLegacyExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);

        op = addEOperation(universalPayloadWithLegacyExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidStartbit", 0, 1, IS_UNIQUE,
                IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(universalPayloadWithLegacyExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidDataLength", 0, 1, IS_UNIQUE,
                IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(abstractBusMessageEClass, AbstractBusMessage.class, "AbstractBusMessage", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractBusMessage_BusId(), this.getIntOrTemplatePlaceholder(), "busId", null, 0, 1, AbstractBusMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractBusMessage_BusIdTmplParam(), ecorePackage.getEString(), "busIdTmplParam", null, 0, 1, AbstractBusMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractBusMessage_BusIdRange(), ecorePackage.getEString(), "busIdRange", null, 0, 1, AbstractBusMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractBusMessage_Filters(), this.getAbstractFilter(), null, "filters", null, 0, -1, AbstractBusMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(abstractBusMessageEClass, ecorePackage.getEBoolean(), "isValid_hasValidBusId", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractBusMessageEClass, ecorePackage.getEBoolean(), "isValid_isOsiLayerConform", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        addEOperation(abstractBusMessageEClass, ecorePackage.getEBoolean(), "isNeedsEthernet", 0, 1, IS_UNIQUE, IS_ORDERED);

        addEOperation(abstractBusMessageEClass, ecorePackage.getEBoolean(), "isNeedsErrorFrames", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(pluginFilterEClass, PluginFilter.class, "PluginFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPluginFilter_PluginName(), ecorePackage.getEString(), "pluginName", null, 0, 1, PluginFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPluginFilter_PluginVersion(), ecorePackage.getEString(), "pluginVersion", null, 0, 1, PluginFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pluginMessageEClass, PluginMessage.class, "PluginMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getPluginMessage_PluginFilter(), this.getPluginFilter(), null, "pluginFilter", null, 1, 1, PluginMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(pluginSignalEClass, PluginSignal.class, "PluginSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(pluginStateExtractStrategyEClass, PluginStateExtractStrategy.class, "PluginStateExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPluginStateExtractStrategy_States(), ecorePackage.getEString(), "states", "ALL", 0, 1, PluginStateExtractStrategy.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPluginStateExtractStrategy_StatesTmplParam(), ecorePackage.getEString(), "statesTmplParam", null, 0, 1,
                PluginStateExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPluginStateExtractStrategy_StatesActive(), this.getBooleanOrTemplatePlaceholderEnum(), "statesActive", "true", 1, 1,
                PluginStateExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPluginStateExtractStrategy_StatesActiveTmplParam(), ecorePackage.getEString(), "statesActiveTmplParam", null, 0, 1,
                PluginStateExtractStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(pluginStateExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidStatesActive", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(pluginStateExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidState", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(pluginResultExtractStrategyEClass, PluginResultExtractStrategy.class, "PluginResultExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPluginResultExtractStrategy_ResultRange(), ecorePackage.getEString(), "resultRange", "", 1, 1, PluginResultExtractStrategy.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(pluginResultExtractStrategyEClass, ecorePackage.getEBoolean(), "isValid_hasValidResultRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(emptyDecodeStrategyEClass, EmptyDecodeStrategy.class, "EmptyDecodeStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        initEClass(pluginCheckExpressionEClass, PluginCheckExpression.class, "PluginCheckExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPluginCheckExpression_EvaluationBehaviour(), this.getEvaluationBehaviourOrTemplateDefinedEnum(), "evaluationBehaviour",
                "PULL_FROM_PLUGIN", 0, 1, PluginCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEReference(getPluginCheckExpression_SignalToCheck(), this.getPluginSignal(), null, "signalToCheck", null, 1, 1, PluginCheckExpression.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPluginCheckExpression_EvaluationBehaviourTmplParam(), ecorePackage.getEString(), "evaluationBehaviourTmplParam", null, 0, 1,
                PluginCheckExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(baseClassWithIDEClass, BaseClassWithID.class, "BaseClassWithID", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getBaseClassWithID_Id(), ecorePackage.getEString(), "id", null, 1, 1, BaseClassWithID.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(headerSignalEClass, HeaderSignal.class, "HeaderSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getHeaderSignal_Attribute(), ecorePackage.getEString(), "attribute", null, 1, 1, HeaderSignal.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getHeaderSignal_AttributeTmplParam(), ecorePackage.getEString(), "attributeTmplParam", null, 0, 1, HeaderSignal.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(headerSignalEClass, ecorePackage.getEBoolean(), "isValid_hasValidAttribute", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(iVariableReaderWriterEClass, IVariableReaderWriter.class, "IVariableReaderWriter", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        addEOperation(iVariableReaderWriterEClass, theXMLTypePackage.getString(), "getReadVariables", 0, -1, IS_UNIQUE, IS_ORDERED);

        addEOperation(iVariableReaderWriterEClass, theXMLTypePackage.getString(), "getWriteVariables", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(iStateTransitionReferenceEClass, IStateTransitionReference.class, "IStateTransitionReference", IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);

        addEOperation(iStateTransitionReferenceEClass, theStatemachinePackage.getAbstractState(), "getStateDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

        addEOperation(iStateTransitionReferenceEClass, theStatemachinePackage.getTransition(), "getTransitionDependencies", 0, -1, IS_UNIQUE, IS_ORDERED);

        initEClass(tpFilterEClass, TPFilter.class, "TPFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTPFilter_SourcePort(), this.getPortPatternOrTemplatePlaceholder(), "sourcePort", null, 0, 1, TPFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTPFilter_DestPort(), this.getPortPatternOrTemplatePlaceholder(), "destPort", null, 0, 1, TPFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(baseClassWithSourceReferenceEClass, BaseClassWithSourceReference.class, "BaseClassWithSourceReference", IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEReference(getBaseClassWithSourceReference_SourceReference(), this.getSourceReference(), null, "sourceReference", null, 0, 1,
                BaseClassWithSourceReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(sourceReferenceEClass, SourceReference.class, "SourceReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getSourceReference_SerializedReference(), ecorePackage.getEString(), "serializedReference", "", 1, 1, SourceReference.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(ethernetMessageEClass, EthernetMessage.class, "EthernetMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getEthernetMessage_EthernetFilter(), this.getEthernetFilter(), null, "ethernetFilter", null, 1, 1, EthernetMessage.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(iPv4MessageEClass, IPv4Message.class, "IPv4Message", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getIPv4Message_IPv4Filter(), this.getIPv4Filter(), null, "iPv4Filter", null, 1, 1, IPv4Message.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(nonVerboseDLTMessageEClass, NonVerboseDLTMessage.class, "NonVerboseDLTMessage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getNonVerboseDLTMessage_NonVerboseDltFilter(), this.getNonVerboseDLTFilter(), null, "nonVerboseDltFilter", null, 1, 1,
                NonVerboseDLTMessage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(stringDecodeStrategyEClass, StringDecodeStrategy.class, "StringDecodeStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStringDecodeStrategy_StringTermination(), this.getBooleanOrTemplatePlaceholderEnum(), "stringTermination", "true", 0, 1,
                StringDecodeStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStringDecodeStrategy_StringTerminationTmplParam(), ecorePackage.getEString(), "stringTerminationTmplParam", null, 0, 1,
                StringDecodeStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(nonVerboseDLTFilterEClass, NonVerboseDLTFilter.class, "NonVerboseDLTFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getNonVerboseDLTFilter_MessageId(), this.getHexOrIntOrTemplatePlaceholder(), "messageId", null, 0, 1, NonVerboseDLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getNonVerboseDLTFilter_MessageIdRange(), ecorePackage.getEString(), "messageIdRange", null, 0, 1, NonVerboseDLTFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(nonVerboseDLTFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMessageIdOrMessageIdRange", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(verboseDLTExtractStrategyEClass, VerboseDLTExtractStrategy.class, "VerboseDLTExtractStrategy", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);

        initEClass(regexOperationEClass, RegexOperation.class, "RegexOperation", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getRegexOperation_Regex(), ecorePackage.getEString(), "regex", null, 0, 1, RegexOperation.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getRegexOperation_DynamicRegex(), this.getIStringOperand(), null, "dynamicRegex", null, 0, 1, RegexOperation.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(regexOperationEClass, ecorePackage.getEBoolean(), "isValid_hasValidRegex", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(payloadFilterEClass, PayloadFilter.class, "PayloadFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getPayloadFilter_Index(), this.getIntOrTemplatePlaceholder(), "index", "0", 1, 1, PayloadFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPayloadFilter_Mask(), this.getByteOrTemplatePlaceholder(), "mask", "0xFF", 1, 1, PayloadFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getPayloadFilter_Value(), this.getByteOrTemplatePlaceholder(), "value", "0", 1, 1, PayloadFilter.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(payloadFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidIndex", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(payloadFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidMask", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(payloadFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidValue", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(verboseDLTPayloadFilterEClass, VerboseDLTPayloadFilter.class, "VerboseDLTPayloadFilter", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getVerboseDLTPayloadFilter_Regex(), ecorePackage.getEString(), "regex", "", 1, 1, VerboseDLTPayloadFilter.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVerboseDLTPayloadFilter_ContainsAny(), ecorePackage.getEString(), "containsAny", null, 0, -1, VerboseDLTPayloadFilter.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(verboseDLTPayloadFilterEClass, ecorePackage.getEBoolean(), "isValid_hasValidRegex", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(abstractVariableEClass, AbstractVariable.class, "AbstractVariable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, AbstractVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractVariable_DisplayName(), ecorePackage.getEString(), "displayName", null, 0, 1, AbstractVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractVariable_Description(), ecorePackage.getEString(), "description", "", 0, 1, AbstractVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(abstractVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidDisplayName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(computedVariableEClass, ComputedVariable.class, "ComputedVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getComputedVariable_Operands(), this.getIComputeVariableActionOperand(), null, "operands", null, 0, -1, ComputedVariable.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getComputedVariable_Expression(), ecorePackage.getEString(), "expression", null, 0, 1, ComputedVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(computedVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(computedVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidOperands", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(structureVariableEClass, StructureVariable.class, "StructureVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getStructureVariable_Variables(), this.getAbstractVariable(), null, "variables", null, 0, -1, StructureVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStructureVariable_VariableReferences(), this.getVariableReference(), null, "variableReferences", null, 0, -1, StructureVariable.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(checkTypeEEnum, CheckType.class, "CheckType");
        addEEnumLiteral(checkTypeEEnum, CheckType.TEMPLATE_DEFINED);
        addEEnumLiteral(checkTypeEEnum, CheckType.ALL);
        addEEnumLiteral(checkTypeEEnum, CheckType.ANY);

        initEEnum(comparatorEEnum, Comparator.class, "Comparator");
        addEEnumLiteral(comparatorEEnum, Comparator.TEMPLATE_DEFINED);
        addEEnumLiteral(comparatorEEnum, Comparator.LT);
        addEEnumLiteral(comparatorEEnum, Comparator.LE);
        addEEnumLiteral(comparatorEEnum, Comparator.EQ);
        addEEnumLiteral(comparatorEEnum, Comparator.NE);
        addEEnumLiteral(comparatorEEnum, Comparator.GE);
        addEEnumLiteral(comparatorEEnum, Comparator.GT);

        initEEnum(logicalOperatorTypeEEnum, LogicalOperatorType.class, "LogicalOperatorType");
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.TEMPLATE_DEFINED);
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.OR);
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.AND);
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.NOR);
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.NAND);
        addEEnumLiteral(logicalOperatorTypeEEnum, LogicalOperatorType.NOT);

        initEEnum(signalDataTypeEEnum, SignalDataType.class, "SignalDataType");
        addEEnumLiteral(signalDataTypeEEnum, SignalDataType.TEMPLATE_DEFINED);
        addEEnumLiteral(signalDataTypeEEnum, SignalDataType.INT);
        addEEnumLiteral(signalDataTypeEEnum, SignalDataType.UINT);

        initEEnum(flexChannelTypeEEnum, FlexChannelType.class, "FlexChannelType");
        addEEnumLiteral(flexChannelTypeEEnum, FlexChannelType.TEMPLATE_DEFINED);
        addEEnumLiteral(flexChannelTypeEEnum, FlexChannelType.A);
        addEEnumLiteral(flexChannelTypeEEnum, FlexChannelType.B);
        addEEnumLiteral(flexChannelTypeEEnum, FlexChannelType.BOTH);

        initEEnum(byteOrderTypeEEnum, ByteOrderType.class, "ByteOrderType");
        addEEnumLiteral(byteOrderTypeEEnum, ByteOrderType.INTEL);
        addEEnumLiteral(byteOrderTypeEEnum, ByteOrderType.MOTOROLA);

        initEEnum(formatDataTypeEEnum, FormatDataType.class, "FormatDataType");
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.TEMPLATEDEFINED);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.FLOAT);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.DEC);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.HEX);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.BOOL);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.BIN);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.OCT);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.ROUND);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.CEIL);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.HHMMSS);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.MS);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.IP);
        addEEnumLiteral(formatDataTypeEEnum, FormatDataType.MAC);

        initEEnum(flexRayHeaderFlagSelectionEEnum, FlexRayHeaderFlagSelection.class, "FlexRayHeaderFlagSelection");
        addEEnumLiteral(flexRayHeaderFlagSelectionEEnum, FlexRayHeaderFlagSelection.TEMPLATE_DEFINED);
        addEEnumLiteral(flexRayHeaderFlagSelectionEEnum, FlexRayHeaderFlagSelection.IGNORE);
        addEEnumLiteral(flexRayHeaderFlagSelectionEEnum, FlexRayHeaderFlagSelection.NO);
        addEEnumLiteral(flexRayHeaderFlagSelectionEEnum, FlexRayHeaderFlagSelection.YES);

        initEEnum(booleanOrTemplatePlaceholderEnumEEnum, BooleanOrTemplatePlaceholderEnum.class, "BooleanOrTemplatePlaceholderEnum");
        addEEnumLiteral(booleanOrTemplatePlaceholderEnumEEnum, BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(booleanOrTemplatePlaceholderEnumEEnum, BooleanOrTemplatePlaceholderEnum.FALSE);
        addEEnumLiteral(booleanOrTemplatePlaceholderEnumEEnum, BooleanOrTemplatePlaceholderEnum.TRUE);

        initEEnum(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.class, "ProtocolTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.UDP);
        addEEnumLiteral(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.TCP);
        addEEnumLiteral(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.ICMP);
        addEEnumLiteral(protocolTypeOrTemplatePlaceholderEnumEEnum, ProtocolTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(ethernetProtocolTypeSelectionEEnum, EthernetProtocolTypeSelection.class, "EthernetProtocolTypeSelection");
        addEEnumLiteral(ethernetProtocolTypeSelectionEEnum, EthernetProtocolTypeSelection.TEMPLATE_DEFINED);
        addEEnumLiteral(ethernetProtocolTypeSelectionEEnum, EthernetProtocolTypeSelection.UDP);
        addEEnumLiteral(ethernetProtocolTypeSelectionEEnum, EthernetProtocolTypeSelection.TCP);

        initEEnum(rxTxFlagTypeOrTemplatePlaceholderEnumEEnum, RxTxFlagTypeOrTemplatePlaceholderEnum.class, "RxTxFlagTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(rxTxFlagTypeOrTemplatePlaceholderEnumEEnum, RxTxFlagTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(rxTxFlagTypeOrTemplatePlaceholderEnumEEnum, RxTxFlagTypeOrTemplatePlaceholderEnum.RX);
        addEEnumLiteral(rxTxFlagTypeOrTemplatePlaceholderEnumEEnum, RxTxFlagTypeOrTemplatePlaceholderEnum.TX);
        addEEnumLiteral(rxTxFlagTypeOrTemplatePlaceholderEnumEEnum, RxTxFlagTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(observerValueTypeEEnum, ObserverValueType.class, "ObserverValueType");
        addEEnumLiteral(observerValueTypeEEnum, ObserverValueType.INFO);
        addEEnumLiteral(observerValueTypeEEnum, ObserverValueType.OK);
        addEEnumLiteral(observerValueTypeEEnum, ObserverValueType.WARN);
        addEEnumLiteral(observerValueTypeEEnum, ObserverValueType.DEFECT);

        initEEnum(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.class,
                "PluginStateValueTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.CREATED);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.INITIALIZED);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.STARTED);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.STOPPED);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.RESET);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.READY);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.ERROR);
        addEEnumLiteral(pluginStateValueTypeOrTemplatePlaceholderEnumEEnum, PluginStateValueTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(analysisTypeOrTemplatePlaceholderEnumEEnum, AnalysisTypeOrTemplatePlaceholderEnum.class, "AnalysisTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(analysisTypeOrTemplatePlaceholderEnumEEnum, AnalysisTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(analysisTypeOrTemplatePlaceholderEnumEEnum, AnalysisTypeOrTemplatePlaceholderEnum.OFFLINE);
        addEEnumLiteral(analysisTypeOrTemplatePlaceholderEnumEEnum, AnalysisTypeOrTemplatePlaceholderEnum.ONLINE);
        addEEnumLiteral(analysisTypeOrTemplatePlaceholderEnumEEnum, AnalysisTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.class,
                "SomeIPMessageTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.REQUEST);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.REQUEST_NO_RETURN);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.NOTIFICATION);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.REQUEST_ACK);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.REQUEST_NO_RETURN_ACK);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.NOTIFICATION_ACK);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.RESPONSE);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.ERROR);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.RESPONSE_ACK);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.ERROR_ACK);
        addEEnumLiteral(someIPMessageTypeOrTemplatePlaceholderEnumEEnum, SomeIPMessageTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPMethodTypeOrTemplatePlaceholderEnumEEnum, SomeIPMethodTypeOrTemplatePlaceholderEnum.class, "SomeIPMethodTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPMethodTypeOrTemplatePlaceholderEnumEEnum, SomeIPMethodTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPMethodTypeOrTemplatePlaceholderEnumEEnum, SomeIPMethodTypeOrTemplatePlaceholderEnum.METHOD);
        addEEnumLiteral(someIPMethodTypeOrTemplatePlaceholderEnumEEnum, SomeIPMethodTypeOrTemplatePlaceholderEnum.EVENT);
        addEEnumLiteral(someIPMethodTypeOrTemplatePlaceholderEnumEEnum, SomeIPMethodTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.class, "SomeIPReturnCodeOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.OK);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.NOT_OK);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.UNKNOWN_SVC);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.UNKNOWN_METHOD);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.NOT_READY);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.NOT_REACHABLE);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.TIMEOUT);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.WRONG_PROTOCOL_VER);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.WRONG_INTERFACE_VER);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.MALFORMED_MESSAGE);
        addEEnumLiteral(someIPReturnCodeOrTemplatePlaceholderEnumEEnum, SomeIPReturnCodeOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPSDFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDFlagsOrTemplatePlaceholderEnum.class, "SomeIPSDFlagsOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPSDFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDFlagsOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPSDFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDFlagsOrTemplatePlaceholderEnum.UNICAST);
        addEEnumLiteral(someIPSDFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDFlagsOrTemplatePlaceholderEnum.REBOOT);
        addEEnumLiteral(someIPSDFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDFlagsOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.class, "SomeIPSDTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.FIND_SVC);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.OFFER_SVC);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.REQUEST_SVC);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.REQUEST_SVC_ACK);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.FIND_EVENTGROUP);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.PUBLISH_EVENTGROUP);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.SUBSCRIBE_EVENTGROUP);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.SUBSCRIBE_EVENTGROUP_ACK);
        addEEnumLiteral(someIPSDTypeOrTemplatePlaceholderEnumEEnum, SomeIPSDTypeOrTemplatePlaceholderEnum.ALL);

        initEEnum(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.class,
                "StreamAnalysisFlagsOrTemplatePlaceholderEnum");
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.SAF_RETRANSMISSION);
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.SAF_LOST_SEGMENT);
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.SAF_DUP_ACK);
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.SAF_PARTIAL_RETRANSMIT);
        addEEnumLiteral(streamAnalysisFlagsOrTemplatePlaceholderEnumEEnum, StreamAnalysisFlagsOrTemplatePlaceholderEnum.ALL);

        initEEnum(someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.class,
                "SomeIPSDEntryFlagsOrTemplatePlaceholderEnum");
        addEEnumLiteral(someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.SDF_UNICAST);
        addEEnumLiteral(someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.SDF_REBOOT);
        addEEnumLiteral(someIPSDEntryFlagsOrTemplatePlaceholderEnumEEnum, SomeIPSDEntryFlagsOrTemplatePlaceholderEnum.ALL);

        initEEnum(canExtIdentifierOrTemplatePlaceholderEnumEEnum, CanExtIdentifierOrTemplatePlaceholderEnum.class, "CanExtIdentifierOrTemplatePlaceholderEnum");
        addEEnumLiteral(canExtIdentifierOrTemplatePlaceholderEnumEEnum, CanExtIdentifierOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(canExtIdentifierOrTemplatePlaceholderEnumEEnum, CanExtIdentifierOrTemplatePlaceholderEnum.FALSE);
        addEEnumLiteral(canExtIdentifierOrTemplatePlaceholderEnumEEnum, CanExtIdentifierOrTemplatePlaceholderEnum.TRUE);
        addEEnumLiteral(canExtIdentifierOrTemplatePlaceholderEnumEEnum, CanExtIdentifierOrTemplatePlaceholderEnum.ALL);

        initEEnum(dataTypeEEnum, DataType.class, "DataType");
        addEEnumLiteral(dataTypeEEnum, DataType.DOUBLE);
        addEEnumLiteral(dataTypeEEnum, DataType.STRING);

        initEEnum(dlT_MessageTypeEEnum, DLT_MessageType.class, "DLT_MessageType");
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.TEMPLATE_DEFINED);
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.DLT_TYPE_LOG);
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.DLT_TYPE_APP_TRACE);
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.DLT_TYPE_NW_TRACE);
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.DLT_TYPE_CONTROL);
        addEEnumLiteral(dlT_MessageTypeEEnum, DLT_MessageType.NOT_SPECIFIED);

        initEEnum(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.class, "DLT_MessageTraceInfo");
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.NOT_SPECIFIED);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.DLT_TRACE_VARIABLE);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.DLT_TRACE_FUNCTION_IN);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.DLT_TRACE_FUNCTION_OUT);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.DLT_TRACE_STATE);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.DLT_TRACE_VFB);
        addEEnumLiteral(dlT_MessageTraceInfoEEnum, DLT_MessageTraceInfo.TEMPLATE_DEFINED);

        initEEnum(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.class, "DLT_MessageLogInfo");
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.NOT_SPECIFIED);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_FATAL);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_ERROR);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_WARN);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_INFO);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_DEBUG);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_VERBOSE);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.TEMPLATE_DEFINED);
        addEEnumLiteral(dlT_MessageLogInfoEEnum, DLT_MessageLogInfo.DLT_LOG_OFF);

        initEEnum(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.class, "DLT_MessageControlInfo");
        addEEnumLiteral(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.NOT_SPECIFIED);
        addEEnumLiteral(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.DLT_CONTROL_REQUEST);
        addEEnumLiteral(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.DLT_CONTROL_RESPONSE);
        addEEnumLiteral(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.DLT_CONTROL_TIME);
        addEEnumLiteral(dlT_MessageControlInfoEEnum, DLT_MessageControlInfo.TEMPLATE_DEFINED);

        initEEnum(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.class, "DLT_MessageBusInfo");
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.NOT_SPECIFIED);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_IPC);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_CAN);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_FLEXRAY);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_MOST);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_RESERVED1);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_RESERVED2);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_RESERVED3);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED1);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED2);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED3);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED4);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED5);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED6);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED7);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED8);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED9);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED10);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED11);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED12);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED13);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.DLT_NW_TRACE_USER_DEFINED14);
        addEEnumLiteral(dlT_MessageBusInfoEEnum, DLT_MessageBusInfo.TEMPLATE_DEFINED);

        initEEnum(forEachExpressionModifierTemplateEEnum, ForEachExpressionModifierTemplate.class, "ForEachExpressionModifierTemplate");
        addEEnumLiteral(forEachExpressionModifierTemplateEEnum, ForEachExpressionModifierTemplate.ALL);
        addEEnumLiteral(forEachExpressionModifierTemplateEEnum, ForEachExpressionModifierTemplate.ANY);

        initEEnum(autosarDataTypeEEnum, AUTOSARDataType.class, "AUTOSARDataType");
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUNICODE2STRING);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AASCIISTRING);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AINT8);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AINT16);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AINT32);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUINT8);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUINT16);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUINT32);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AFLOAT32);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AFLOAT64);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUTF8STRING);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.TEMPLATE_DEFINED);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AINT64);
        addEEnumLiteral(autosarDataTypeEEnum, AUTOSARDataType.AUINT64);

        initEEnum(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.class, "CANFrameTypeOrTemplatePlaceholderEnum");
        addEEnumLiteral(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED);
        addEEnumLiteral(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.UNDEFINED);
        addEEnumLiteral(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.STANDARD);
        addEEnumLiteral(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.ERROR_FRAME);
        addEEnumLiteral(canFrameTypeOrTemplatePlaceholderEnumEEnum, CANFrameTypeOrTemplatePlaceholderEnum.REMOTE_FRAME);

        initEEnum(evaluationBehaviourOrTemplateDefinedEnumEEnum, EvaluationBehaviourOrTemplateDefinedEnum.class, "EvaluationBehaviourOrTemplateDefinedEnum");
        addEEnumLiteral(evaluationBehaviourOrTemplateDefinedEnumEEnum, EvaluationBehaviourOrTemplateDefinedEnum.PULL_FROM_PLUGIN);
        addEEnumLiteral(evaluationBehaviourOrTemplateDefinedEnumEEnum, EvaluationBehaviourOrTemplateDefinedEnum.PUSHED_BY_PLUGIN);
        addEEnumLiteral(evaluationBehaviourOrTemplateDefinedEnumEEnum, EvaluationBehaviourOrTemplateDefinedEnum.TEMPLATE_DEFINED);

        initEEnum(signalVariableLagEnumEEnum, SignalVariableLagEnum.class, "SignalVariableLagEnum");
        addEEnumLiteral(signalVariableLagEnumEEnum, SignalVariableLagEnum.CURRENT);
        addEEnumLiteral(signalVariableLagEnumEEnum, SignalVariableLagEnum.PREVIOUS);

        initEEnum(ttlOrTemplatePlaceHolderEnumEEnum, TTLOrTemplatePlaceHolderEnum.class, "TTLOrTemplatePlaceHolderEnum");
        addEEnumLiteral(ttlOrTemplatePlaceHolderEnumEEnum, TTLOrTemplatePlaceHolderEnum.ZERO);
        addEEnumLiteral(ttlOrTemplatePlaceHolderEnumEEnum, TTLOrTemplatePlaceHolderEnum.GREATER_ZERO);
        addEEnumLiteral(ttlOrTemplatePlaceHolderEnumEEnum, TTLOrTemplatePlaceHolderEnum.ALL);
        addEEnumLiteral(ttlOrTemplatePlaceHolderEnumEEnum, TTLOrTemplatePlaceHolderEnum.TEMPLATE_DEFINED);

        // Initialize data types
        initEDataType(checkTypeObjectEDataType, CheckType.class, "CheckTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
        initEDataType(comparatorObjectEDataType, Comparator.class, "ComparatorObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
        initEDataType(logicalOperatorTypeObjectEDataType, LogicalOperatorType.class, "LogicalOperatorTypeObject", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
        initEDataType(hexOrIntOrTemplatePlaceholderEDataType, String.class, "HexOrIntOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(intOrTemplatePlaceholderEDataType, String.class, "IntOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(longOrTemplatePlaceholderEDataType, String.class, "LongOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(doubleOrTemplatePlaceholderEDataType, String.class, "DoubleOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(doubleOrHexOrTemplatePlaceholderEDataType, String.class, "DoubleOrHexOrTemplatePlaceholder", IS_SERIALIZABLE,
                !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(ipPatternOrTemplatePlaceholderEDataType, String.class, "IPPatternOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(macPatternOrTemplatePlaceholderEDataType, String.class, "MACPatternOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(portPatternOrTemplatePlaceholderEDataType, String.class, "PortPatternOrTemplatePlaceholder", IS_SERIALIZABLE,
                !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(vlanIdPatternOrTemplatePlaceholderEDataType, String.class, "VlanIdPatternOrTemplatePlaceholder", IS_SERIALIZABLE,
                !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(ipOrTemplatePlaceholderEDataType, String.class, "IPOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(portOrTemplatePlaceholderEDataType, String.class, "PortOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(vlanIdOrTemplatePlaceholderEDataType, String.class, "VlanIdOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(macOrTemplatePlaceholderEDataType, String.class, "MACOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(bitmaskOrTemplatePlaceholderEDataType, String.class, "BitmaskOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(hexOrIntOrNullOrTemplatePlaceholderEDataType, String.class, "HexOrIntOrNullOrTemplatePlaceholder", IS_SERIALIZABLE,
                !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(nonZeroDoubleOrTemplatePlaceholderEDataType, String.class, "NonZeroDoubleOrTemplatePlaceholder", IS_SERIALIZABLE,
                !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(valVarInitialValueDoubleOrTemplatePlaceholderOrStringEDataType, String.class, "ValVarInitialValueDoubleOrTemplatePlaceholderOrString",
                IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
        initEDataType(byteOrTemplatePlaceholderEDataType, String.class, "ByteOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations();
        // http://www.eclipse.org/edapt
        createEdaptAnnotations();
        // http://www.eclipse.org/OCL/Import
        createImportAnnotations();
        // http:///de/bmw/smard/modeller/TemplateMetaData
        createTemplateMetaDataAnnotations();
        // http:///de/bmw/smard/modeller/HeaderSignalAttribute
        createHeaderSignalAttributeAnnotations();
    }

    /**
     * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createExtendedMetaDataAnnotations() {
        String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
        addAnnotation(this,
                source,
                new String[] {
                        "qualified", "true"
                });
        addAnnotation(documentRootEClass,
                source,
                new String[] {
                        "name", "",
                        "kind", "mixed"
                });
        addAnnotation(getDocumentRoot_Mixed(),
                source,
                new String[] {
                        "kind", "elementWildcard",
                        "name", ":mixed"
                });
        addAnnotation(getDocumentRoot_XMLNSPrefixMap(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "xmlns:prefix"
                });
        addAnnotation(getDocumentRoot_XSISchemaLocation(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "xsi:schemaLocation"
                });
        addAnnotation(getDocumentRoot_ConditionSet(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "conditionSet",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getDocumentRoot_ConditionsDocument(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "conditionsDocument",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(conditionSetEClass,
                source,
                new String[] {
                        "name", "conditionSet",
                        "kind", "elementOnly"
                });
        addAnnotation(getConditionSet_Group(),
                source,
                new String[] {
                        "kind", "group",
                        "name", "group:0"
                });
        addAnnotation(getConditionSet_Baureihe(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "baureihe",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Description(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "description",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Istufe(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "istufe",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Schemaversion(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "schemaversion",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Uuid(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "uuid",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Version(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "version",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getConditionSet_Conditions(),
                source,
                new String[] {
                        "kind", "element"
                });
        addAnnotation(checkTypeEEnum,
                source,
                new String[] {
                        "name", "checkType"
                });
        addAnnotation(checkTypeObjectEDataType,
                source,
                new String[] {
                        "name", "checkType:Object",
                        "baseType", "checkType"
                });
        addAnnotation(comparatorEEnum,
                source,
                new String[] {
                        "name", "comparator"
                });
        addAnnotation(comparatorObjectEDataType,
                source,
                new String[] {
                        "name", "comparator:Object",
                        "baseType", "comparator"
                });
        addAnnotation(conditionEClass,
                source,
                new String[] {
                        "name", "condition",
                        "kind", "element"
                });
        addAnnotation(getCondition_Name(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "name",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getCondition_Description(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "description",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getCondition_Expression(),
                source,
                new String[] {
                        "name", "expression",
                        "kind", "element"
                });
        addAnnotation(signalComparisonExpressionEClass,
                source,
                new String[] {
                        "name", "elementaryCondition",
                        "kind", "elementOnly"
                });
        addAnnotation(getSignalComparisonExpression_ComparatorSignal(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "canBusMessage",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getSignalComparisonExpression_Operator(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "operator",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(canMessageCheckExpressionEClass,
                source,
                new String[] {
                        "name", "messageCheckCondition",
                        "kind", "empty"
                });
        addAnnotation(getCanMessageCheckExpression_Busid(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "busid",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getCanMessageCheckExpression_Type(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "type",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(linMessageCheckExpressionEClass,
                source,
                new String[] {
                        "kind", "empty"
                });
        addAnnotation(getLinMessageCheckExpression_Busid(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "busid",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getLinMessageCheckExpression_Type(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "type",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(stateCheckExpressionEClass,
                source,
                new String[] {
                        "name", "stateCheckCondition",
                        "kind", "empty"
                });
        addAnnotation(getStateCheckExpression_CheckStateActive(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "checkStateActive",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(timingExpressionEClass,
                source,
                new String[] {
                        "name", "timingCondition",
                        "kind", "empty"
                });
        addAnnotation(getTimingExpression_Maxtime(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "maxtime",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getTimingExpression_Mintime(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "mintime",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(trueExpressionEClass,
                source,
                new String[] {
                        "name", "trueCondition",
                        "kind", "empty"
                });
        addAnnotation(logicalExpressionEClass,
                source,
                new String[] {
                        "name", "logicalOperator",
                        "kind", "elementOnly"
                });
        addAnnotation(getLogicalExpression_Expressions(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "logicaloperations",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getLogicalExpression_Operator(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "operator",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getFlexRayMessageCheckExpression_Type(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "type",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(stopExpressionEClass,
                source,
                new String[] {
                        "name", "stopCondition",
                        "kind", "empty"
                });
        addAnnotation(getStopExpression_AnalyseArt(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "analyseArt",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(logicalOperatorTypeEEnum,
                source,
                new String[] {
                        "name", "logicalOperatorType"
                });
        addAnnotation(logicalOperatorTypeObjectEDataType,
                source,
                new String[] {
                        "name", "logicalOperatorType:Object",
                        "baseType", "logicalOperatorType"
                });
        addAnnotation(conditionsDocumentEClass,
                source,
                new String[] {
                        "name", "conditionsDocument"
                });
        addAnnotation(getVariableSet_Uuid(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "uuid",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getObserverValueRange_ValueType(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "valueType",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getPluginFilter_PluginVersion(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "pluginVersion",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getPluginStateExtractStrategy_StatesActive(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "checkStateActive",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getPluginResultExtractStrategy_ResultRange(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "result",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(getBaseClassWithID_Id(),
                source,
                new String[] {
                        "kind", "element"
                });
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/edapt</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createEdaptAnnotations() {
        String source = "http://www.eclipse.org/edapt";
        addAnnotation(this,
                source,
                new String[] {
                        "historyURI", "modeller.history"
                });
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/OCL/Import</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createImportAnnotations() {
        String source = "http://www.eclipse.org/OCL/Import";
        addAnnotation(this,
                source,
                new String[] {
                        "ecore", "http://www.eclipse.org/emf/2002/Ecore",
                        "ecore.xml.type", "http://www.eclipse.org/emf/2003/XMLType"
                });
    }

    /**
     * Initializes the annotations for <b>http:///de/bmw/smard/modeller/TemplateMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createTemplateMetaDataAnnotations() {
        String source = "http:///de/bmw/smard/modeller/TemplateMetaData";
        addAnnotation(getConditionSet_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getConditionSet_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getCondition_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getCondition_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getSignalComparisonExpression_Operator(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCanMessageCheckExpression_Busid(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getCanMessageCheckExpression_Type(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCanMessageCheckExpression_MessageIDs(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getCanMessageCheckExpression_RxtxFlag(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCanMessageCheckExpression_ExtIdentifier(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getLinMessageCheckExpression_Busid(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getLinMessageCheckExpression_Type(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getLinMessageCheckExpression_MessageIDs(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getStateCheckExpression_CheckStateActive(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getTimingExpression_Maxtime(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTimingExpression_Mintime(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getLogicalExpression_Operator(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getTransitionCheckExpression_StayActive(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_BusId(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getFlexRayMessageCheckExpression_PayloadPreamble(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_ZeroFrame(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_SyncFrame(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_StartupFrame(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_NetworkMgmt(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getFlexRayMessageCheckExpression_FlexrayMessageId(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getFlexRayMessageCheckExpression_Type(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getStopExpression_AnalyseArt(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getComparatorSignal_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getConstantComparatorValue_Value(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getConstantComparatorValue_InterpretedValue(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCalculationExpression_Expression(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getExpression_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getVariable_Unit(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getVariable_VariableFormatTmplParam(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getSignalVariable_InterpretedValue(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getValueVariable_InitialValue(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getAbstractObserver_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractObserver_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractObserver_LogLevel(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getAbstractObserver_ValueRangesTmplParam(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getObserverValueRange_Value(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getObserverValueRange_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getObserverValueRange_ValueType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getBitPatternComparatorValue_Value(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getBitPatternComparatorValue_Startbit(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getAbstractMessage_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractSignal_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_SourceMAC(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_DestMAC(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_EtherType(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_InnerVlanId(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_OuterVlanId(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_InnerVlanCFI(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_OuterVlanCFI(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_InnerVlanVID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_OuterVlanVID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_CRC(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getEthernetFilter_InnerVlanTPID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getEthernetFilter_OuterVlanTPID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getUDPFilter_Checksum(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTCPFilter_SequenceNumber(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTCPFilter_AcknowledgementNumber(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTCPFilter_TcpFlags(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTCPFilter_StreamAnalysisFlags(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getTCPFilter_StreamValidPayloadOffset(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getIPv4Filter_ProtocolType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getIPv4Filter_SourceIP(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getIPv4Filter_DestIP(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getIPv4Filter_TimeToLive(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_ServiceId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_MethodType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getSomeIPFilter_MethodId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_ClientId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_SessionId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_ProtocolVersion(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_InterfaceVersion(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPFilter_MessageType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getSomeIPFilter_ReturnCode(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getSomeIPFilter_SomeIPLength(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_Flags(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getSomeIPSDFilter_SdType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getSomeIPSDFilter_InstanceId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_Ttl(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_MajorVersion(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_MinorVersion(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_EventGroupId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_IndexFirstOption(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_IndexSecondOption(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_NumberFirstOption(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_NumberSecondOption(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getSomeIPSDFilter_ServiceId_SomeIPSD(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getDoubleSignal_IgnoreInvalidValues(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDoubleSignal_IgnoreInvalidValuesTmplParam(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getDoubleDecodeStrategy_Factor(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getDoubleDecodeStrategy_Offset(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUniversalPayloadExtractStrategy_ExtractionRule(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getUniversalPayloadExtractStrategy_SignalDataType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCANFilter_MessageIdRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getCANFilter_FrameId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getCANFilter_RxtxFlag(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCANFilter_ExtIdentifier(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getCANFilter_FrameType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getLINFilter_MessageIdRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getLINFilter_FrameId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getFlexRayFilter_MessageIdRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getFlexRayFilter_SlotId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getFlexRayFilter_CycleOffset(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getFlexRayFilter_CycleRepetition(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getFlexRayFilter_Channel(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDLTFilter_EcuID_ECU(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getDLTFilter_SessionID_SEID(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getDLTFilter_ApplicationID_APID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getDLTFilter_ContextID_CTID(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getDLTFilter_MessageType_MSTP(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDLTFilter_MessageLogInfo_MSLI(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDLTFilter_MessageTraceInfo_MSTI(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDLTFilter_MessageBusInfo_MSBI(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getDLTFilter_MessageControlInfo_MSCI(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getUDPNMFilter_SourceNodeIdentifier(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUDPNMFilter_ControlBitVector(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUDPNMFilter_PwfStatus(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUDPNMFilter_TeilnetzStatus(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUniversalPayloadWithLegacyExtractStrategy_StartBit(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getUniversalPayloadWithLegacyExtractStrategy_Length(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getAbstractBusMessage_BusId(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getAbstractBusMessage_BusIdRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPluginFilter_PluginName(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPluginFilter_PluginVersion(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPluginStateExtractStrategy_States(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPluginStateExtractStrategy_StatesActive(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getPluginResultExtractStrategy_ResultRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPluginCheckExpression_EvaluationBehaviour(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getHeaderSignal_Attribute(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getTPFilter_SourcePort(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getTPFilter_DestPort(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getStringDecodeStrategy_StringTermination(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getStringDecodeStrategy_StringTerminationTmplParam(),
                source,
                new String[] {
                        "attrType", "special"
                });
        addAnnotation(getNonVerboseDLTFilter_MessageId(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getNonVerboseDLTFilter_MessageIdRange(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getRegexOperation_Regex(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getPayloadFilter_Index(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getPayloadFilter_Mask(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getPayloadFilter_Value(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getVerboseDLTPayloadFilter_Regex(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractVariable_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractVariable_DisplayName(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractVariable_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getComputedVariable_Expression(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
    }

    /**
     * Initializes the annotations for <b>http:///de/bmw/smard/modeller/HeaderSignalAttribute</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createHeaderSignalAttributeAnnotations() {
        String source = "http:///de/bmw/smard/modeller/HeaderSignalAttribute";
        addAnnotation(getAbstractFilter_PayloadLength(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_SourceMAC(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_DestMAC(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_EtherType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_InnerVlanId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_OuterVlanId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_InnerVlanCFI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_OuterVlanCFI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_InnerVlanVID(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_OuterVlanVID(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_CRC(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_InnerVlanTPID(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getEthernetFilter_OuterVlanTPID(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getUDPFilter_Checksum(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTCPFilter_SequenceNumber(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTCPFilter_AcknowledgementNumber(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTCPFilter_TcpFlags(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTCPFilter_StreamAnalysisFlags(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTCPFilter_StreamValidPayloadOffset(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getIPv4Filter_ProtocolType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getIPv4Filter_SourceIP(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getIPv4Filter_DestIP(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getIPv4Filter_TimeToLive(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_ServiceId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_MethodType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_MethodId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_ClientId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_SessionId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_ProtocolVersion(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_InterfaceVersion(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_MessageType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_ReturnCode(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPFilter_SomeIPLength(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_Flags(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_SdType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_InstanceId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_Ttl(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_MajorVersion(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_MinorVersion(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_EventGroupId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_IndexFirstOption(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_IndexSecondOption(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_NumberFirstOption(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_NumberSecondOption(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getSomeIPSDFilter_ServiceId_SomeIPSD(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getCANFilter_FrameId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getCANFilter_RxtxFlag(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getCANFilter_ExtIdentifier(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getCANFilter_FrameType(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getLINFilter_FrameId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getFlexRayFilter_SlotId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getFlexRayFilter_Channel(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getDLTFilter_EcuID_ECU(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getDLTFilter_SessionID_SEID(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getDLTFilter_ApplicationID_APID(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getDLTFilter_ContextID_CTID(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getDLTFilter_MessageType_MSTP(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getDLTFilter_MessageLogInfo_MSLI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getDLTFilter_MessageTraceInfo_MSTI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getDLTFilter_MessageBusInfo_MSBI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getDLTFilter_MessageControlInfo_MSCI(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getUDPNMFilter_SourceNodeIdentifier(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getUDPNMFilter_ControlBitVector(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getUDPNMFilter_PwfStatus(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getUDPNMFilter_TeilnetzStatus(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getPluginFilter_PluginName(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getPluginFilter_PluginVersion(),
                source,
                new String[] {
                        "attrType", "String"
                });
        addAnnotation(getTPFilter_SourcePort(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getTPFilter_DestPort(),
                source,
                new String[] {
                        "attrType", "Double"
                });
        addAnnotation(getNonVerboseDLTFilter_MessageId(),
                source,
                new String[] {
                        "attrType", "Double"
                });
    }


} //ConditionsPackageImpl
