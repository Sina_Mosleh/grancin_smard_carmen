package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Verbose DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNonVerboseDLTFilter()
 * @model
 * @generated
 */
public interface NonVerboseDLTFilter extends AbstractFilter {

    /**
     * Returns the value of the '<em><b>Message Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message Id</em>' attribute.
     * @see #setMessageId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNonVerboseDLTFilter_MessageId()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getMessageId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message Id</em>' attribute.
     * @see #getMessageId()
     * @generated
     */
    void setMessageId(String value);

    /**
     * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message Id Range</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message Id Range</em>' attribute.
     * @see #setMessageIdRange(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNonVerboseDLTFilter_MessageIdRange()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIdRange();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message Id Range</em>' attribute.
     * @see #getMessageIdRange()
     * @generated
     */
    void setMessageIdRange(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMessageIdOrMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

} // NonVerboseDLTFilter
