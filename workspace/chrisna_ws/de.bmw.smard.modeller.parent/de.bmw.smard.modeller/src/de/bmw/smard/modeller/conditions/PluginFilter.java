package de.bmw.smard.modeller.conditions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginName <em>Plugin Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginVersion <em>Plugin Version</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginFilter()
 * @model
 * @generated
 */
public interface PluginFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Plugin Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Plugin Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Plugin Name</em>' attribute.
     * @see #setPluginName(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginFilter_PluginName()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
     * @generated
     */
    String getPluginName();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginName <em>Plugin Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Plugin Name</em>' attribute.
     * @see #getPluginName()
     * @generated
     */
    void setPluginName(String value);

    /**
     * Returns the value of the '<em><b>Plugin Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Plugin Version</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Plugin Version</em>' attribute.
     * @see #setPluginVersion(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginFilter_PluginVersion()
     * @model extendedMetaData="kind='attribute' name='pluginVersion' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='String'"
     * @generated
     */
    String getPluginVersion();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginVersion <em>Plugin Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Plugin Version</em>' attribute.
     * @see #getPluginVersion()
     * @generated
     */
    void setPluginVersion(String value);

} // PluginFilter
