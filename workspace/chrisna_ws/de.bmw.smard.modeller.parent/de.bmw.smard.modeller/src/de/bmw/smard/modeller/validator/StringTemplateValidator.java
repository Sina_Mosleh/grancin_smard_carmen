package de.bmw.smard.modeller.validator;

public class StringTemplateValidator {

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> builder(String value) {
        return new StringTemplateValidator.Builder(value);
    }

    public static class Builder extends ValueTemplateValidator.Builder {
        public Builder(String value) {
            super(value);
        }
    }
}
