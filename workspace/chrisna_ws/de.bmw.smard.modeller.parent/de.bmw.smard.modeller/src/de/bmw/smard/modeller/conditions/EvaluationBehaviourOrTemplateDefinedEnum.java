package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEvaluationBehaviourOrTemplateDefinedEnum()
 * @model
 * @generated
 */
public enum EvaluationBehaviourOrTemplateDefinedEnum implements Enumerator {
    /**
     * The '<em><b>PULL FROM PLUGIN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #PULL_FROM_PLUGIN_VALUE
     * @generated
     * @ordered
     */
    PULL_FROM_PLUGIN(0, "PULL_FROM_PLUGIN", "PULL_FROM_PLUGIN"),

    /**
     * The '<em><b>PUSHED BY PLUGIN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #PUSHED_BY_PLUGIN_VALUE
     * @generated
     * @ordered
     */
    PUSHED_BY_PLUGIN(1, "PUSHED_BY_PLUGIN", "PUSHED_BY_PLUGIN"),

    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined");

    /**
     * The '<em><b>PULL FROM PLUGIN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>PULL FROM PLUGIN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #PULL_FROM_PLUGIN
     * @model
     * @generated
     * @ordered
     */
    public static final int PULL_FROM_PLUGIN_VALUE = 0;

    /**
     * The '<em><b>PUSHED BY PLUGIN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>PUSHED BY PLUGIN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #PUSHED_BY_PLUGIN
     * @model
     * @generated
     * @ordered
     */
    public static final int PUSHED_BY_PLUGIN_VALUE = 1;

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * An array of all the '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final EvaluationBehaviourOrTemplateDefinedEnum[] VALUES_ARRAY =
            new EvaluationBehaviourOrTemplateDefinedEnum[] {
                    PULL_FROM_PLUGIN,
                    PUSHED_BY_PLUGIN,
                    TEMPLATE_DEFINED,
            };

    /**
     * A public read-only list of all the '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<EvaluationBehaviourOrTemplateDefinedEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static EvaluationBehaviourOrTemplateDefinedEnum get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            EvaluationBehaviourOrTemplateDefinedEnum result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static EvaluationBehaviourOrTemplateDefinedEnum getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            EvaluationBehaviourOrTemplateDefinedEnum result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Evaluation Behaviour Or Template Defined Enum</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static EvaluationBehaviourOrTemplateDefinedEnum get(int value) {
        switch (value) {
            case PULL_FROM_PLUGIN_VALUE:
                return PULL_FROM_PLUGIN;
            case PUSHED_BY_PLUGIN_VALUE:
                return PUSHED_BY_PLUGIN;
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EvaluationBehaviourOrTemplateDefinedEnum(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //EvaluationBehaviourOrTemplateDefinedEnum
