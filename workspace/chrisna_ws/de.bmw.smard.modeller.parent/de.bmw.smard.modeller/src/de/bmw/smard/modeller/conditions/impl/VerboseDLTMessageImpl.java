package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DLTFilter;
import de.bmw.smard.modeller.conditions.VerboseDLTMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTMessageImpl#getDltFilter <em>Dlt Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VerboseDLTMessageImpl extends DLTMessageImpl implements VerboseDLTMessage {
    /**
     * The cached value of the '{@link #getDltFilter() <em>Dlt Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDltFilter()
     * @generated
     * @ordered
     */
    protected DLTFilter dltFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected VerboseDLTMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VERBOSE_DLT_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public DLTFilter getDltFilter() {
        return dltFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetDltFilter(DLTFilter newDltFilter, NotificationChain msgs) {
        DLTFilter oldDltFilter = dltFilter;
        dltFilter = newDltFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER, oldDltFilter, newDltFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDltFilter(DLTFilter newDltFilter) {
        if (newDltFilter != dltFilter) {
            NotificationChain msgs = null;
            if (dltFilter != null)
                msgs = ((InternalEObject) dltFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER, null,
                        msgs);
            if (newDltFilter != null)
                msgs = ((InternalEObject) newDltFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER, null,
                        msgs);
            msgs = basicSetDltFilter(newDltFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER, newDltFilter, newDltFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER:
                return basicSetDltFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER:
                return getDltFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER:
                setDltFilter((DLTFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER:
                setDltFilter((DLTFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_MESSAGE__DLT_FILTER:
                return dltFilter != null;
        }
        return super.eIsSet(featureID);
    }

    @Override
    public AbstractFilter getPrimaryFilter() {
        return dltFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //VerboseDLTMessageImpl
