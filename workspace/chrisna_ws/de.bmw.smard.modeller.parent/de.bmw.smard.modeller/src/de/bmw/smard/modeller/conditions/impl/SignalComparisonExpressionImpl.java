package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.BitPatternComparatorValue;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.CalculationExpression;
import de.bmw.smard.modeller.conditions.Comparator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ConstantComparatorValue;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand;
import de.bmw.smard.modeller.conditions.SignalComparisonExpression;
import de.bmw.smard.modeller.conditions.SignalReference;
import de.bmw.smard.modeller.conditions.SignalVariable;
import de.bmw.smard.modeller.conditions.ValueVariable;
import de.bmw.smard.modeller.conditions.VariableReference;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Comparison Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl#getComparatorSignal <em>Comparator Signal</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl#getOperator <em>Operator</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalComparisonExpressionImpl extends ExpressionImpl implements SignalComparisonExpression {
    /**
     * The cached value of the '{@link #getComparatorSignal() <em>Comparator Signal</em>}' containment reference list.
     * <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #getComparatorSignal()
     * @generated
     * @ordered
     */
    protected EList<ISignalComparisonExpressionOperand> comparatorSignal;

    /**
     * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected static final Comparator OPERATOR_EDEFAULT = Comparator.EQ;

    /**
     * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected Comparator operator = OPERATOR_EDEFAULT;

    /**
     * The default value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperatorTmplParam()
     * @generated
     * @ordered
     */
    protected static final String OPERATOR_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperatorTmplParam()
     * @generated
     * @ordered
     */
    protected String operatorTmplParam = OPERATOR_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SignalComparisonExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SIGNAL_COMPARISON_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<ISignalComparisonExpressionOperand> getComparatorSignal() {
        if (comparatorSignal == null) {
            comparatorSignal =
                    new EObjectContainmentEList<ISignalComparisonExpressionOperand>(ISignalComparisonExpressionOperand.class, this, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL);
        }
        return comparatorSignal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Comparator getOperator() {
        return operator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOperator(Comparator newOperator) {
        Comparator oldOperator = operator;
        operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR, oldOperator, operator));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOperatorTmplParam() {
        return operatorTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOperatorTmplParam(String newOperatorTmplParam) {
        String oldOperatorTmplParam = operatorTmplParam;
        operatorTmplParam = newOperatorTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM, oldOperatorTmplParam, operatorTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidComparator(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_COMPARATOR)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(operator)
                        .error("_Validation_Conditions_SignalComparisonExpression_Comparator_IsNull")
                        .build())

                .with(Validators.checkTemplate(operator)
                        .templateType(Comparator.TEMPLATE_DEFINED)
                        .tmplParam(operatorTmplParam)
                        .containsParameterError("_Validation_Conditions_SignalComparisonExpression_Comparator_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_SignalComparisonExpression_ComparatorTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_SignalComparisonExpression_ComparatorTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
        assert operator != null;
        String errMsgID = "";
        boolean hasValidOperands = true;

        EList<ISignalComparisonExpressionOperand> operands = this.getComparatorSignal();
        if (operands == null || operands.size() < 2) {
            // why? this constrain is emf-modelled and thus not this function's
            // business
            return true;
        }

        ISignalComparisonExpressionOperand operand1 = operands.get(0);
        ISignalComparisonExpressionOperand operand2 = operands.get(1);

        // comparison of constants only not allowed
        if (areConstantsOnly(operand1, operand2)) {
            errMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_NotOnlyConstants";
            hasValidOperands = false;
        }

        // types must match
        if (!haveCompatibleDatatypes(operand1, operand2)) {
            errMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_TypeMisMatch";
            hasValidOperands = false;
        }

        // setting if raw or interpreted value is used should match
        if (Boolean.FALSE == checkIfBothOperandsTreatRawOrSignalFlagIsSetEqually(operand1, operand2, true)) {
            String warnMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_InterpreteValueFlag";
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS)
                        .messageId(PluginActivator.INSTANCE, warnMsgID)
                        .data(this)
                        .build());
            }
        }

        boolean mustAssertConstantComparatorValuesInterpreteFlagIsFalse = mustAssertConstantComparatorValuesInterpreteFlagIsFalse(
                operand1)
            || mustAssertConstantComparatorValuesInterpreteFlagIsFalse(operand2);

        for (ISignalComparisonExpressionOperand operand : operands) {

            // some operands only work with EQ / NE
            if (operator != Comparator.EQ && operator != Comparator.NE) {

                if (DataType.STRING.equals(operand.get_EvaluationDataType())) {
                    errMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_StringComparisonOperator";
                    hasValidOperands = false;
                    break;
                }

                if (operand instanceof BitPatternComparatorValue) {
                    errMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_BitPatternComparison";
                    hasValidOperands = false;
                    break;
                }
            }

            if (mustAssertConstantComparatorValuesInterpreteFlagIsFalse && operand instanceof ConstantComparatorValue) {
                if (((ConstantComparatorValue) operand)
                        .getInterpretedValue() != BooleanOrTemplatePlaceholderEnum.FALSE) {
                    errMsgID = "_Validation_Conditions_SignalComparisonExpression_Operands_ConstantFALSE";
                    hasValidOperands = false;
                    break;
                }
            }
        }

        if (!hasValidOperands) {
            assert !StringUtils.nullOrEmpty(errMsgID);
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.SIGNAL_COMPARISON_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS)
                        .messageId(PluginActivator.INSTANCE, errMsgID)
                        .data(this)
                        .build());
            }
        }
        return hasValidOperands;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        for (ISignalComparisonExpressionOperand op : getComparatorSignal()) {
            messages.addAll(op.getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * @param operands
     * @return
     * @generated NOT
     */
    private boolean haveCompatibleDatatypes(ISignalComparisonExpressionOperand... operands) {
        if (operands == null || operands.length < 1)
            return false;

        DataType type = operands[0].get_EvaluationDataType();
        if (type != null) {
            for (ISignalComparisonExpressionOperand op : operands) {
                if (op.get_EvaluationDataType() != null && type != op.get_EvaluationDataType()) {
                    return false;
                }
            }
        }
        // cases where get_EvaluationDataType() returns null is caught in other validation methods
        // so return true
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * 
     * @generated NOT
     */
    private boolean areConstantsOnly(ISignalComparisonExpressionOperand operand1,
            ISignalComparisonExpressionOperand operand2) {
        return isInstanceOfConstantComparatorValue_Or_BitPatternComparatorValue(operand1)
            && isInstanceOfConstantComparatorValue_Or_BitPatternComparatorValue(operand2);
    }

    /**
     * <!-- begin-user-doc -->
     * 
     * @generated NOT
     */
    private boolean isInstanceOfConstantComparatorValue_Or_BitPatternComparatorValue(
            ISignalComparisonExpressionOperand operand) {
        return operand instanceof ConstantComparatorValue || operand instanceof BitPatternComparatorValue;
    }

    /**
     * <!-- begin-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        for (IOperand op : getComparatorSignal()) {
            if (!(op instanceof SignalReference)) {
                if (op.get_EvaluationDataType() != null)
                    return op.get_EvaluationDataType();
            }
        }

        return null;
    }

    /** @generated NOT */
    private Boolean checkIfBothOperandsTreatRawOrSignalFlagIsSetEqually(ISignalComparisonExpressionOperand op1,
            ISignalComparisonExpressionOperand op2, boolean swap) {
        Boolean result = null;
        if (op1 instanceof ConstantComparatorValue && ((op2 instanceof VariableReference)
            && (((VariableReference) op2).getVariable() instanceof SignalVariable))) {
            BooleanOrTemplatePlaceholderEnum interpretedValueFlag = ((ConstantComparatorValue) op1)
                    .getInterpretedValue();

            if (interpretedValueFlag != null
                && interpretedValueFlag != BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED) {
                result = interpretedValueFlag == ((SignalVariable) ((VariableReference) op2).getVariable())
                        .getInterpretedValue();
            }
        }
        if (result == null && swap) {
            return checkIfBothOperandsTreatRawOrSignalFlagIsSetEqually(op2, op1, false);
        }
        return result;
    }

    /** @generated NOT */
    private boolean mustAssertConstantComparatorValuesInterpreteFlagIsFalse(
            ISignalComparisonExpressionOperand operand) {
        return ((operand instanceof CalculationExpression) || (operand instanceof VariableReference)
            && (((VariableReference) operand).getVariable() instanceof ValueVariable));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<ISignalComparisonExpressionOperand> comparatorSignals = getComparatorSignal();
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.addAll(comparatorSignals);
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        // since it is an Expression it should return BOOLEAN here...
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
                return ((InternalEList<?>) getComparatorSignal()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
                return getComparatorSignal();
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
                return getOperator();
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
                return getOperatorTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
                getComparatorSignal().clear();
                getComparatorSignal().addAll((Collection<? extends ISignalComparisonExpressionOperand>) newValue);
                return;
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
                setOperator((Comparator) newValue);
                return;
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
                setOperatorTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
                getComparatorSignal().clear();
                return;
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
                setOperator(OPERATOR_EDEFAULT);
                return;
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
                setOperatorTmplParam(OPERATOR_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL:
                return comparatorSignal != null && !comparatorSignal.isEmpty();
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR:
                return operator != OPERATOR_EDEFAULT;
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM:
                return OPERATOR_TMPL_PARAM_EDEFAULT == null ? operatorTmplParam != null : !OPERATOR_TMPL_PARAM_EDEFAULT.equals(operatorTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (operator: ");
        result.append(operator);
        result.append(", operatorTmplParam: ");
        result.append(operatorTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>();
        for (IOperand operand : this.get_Operands()) {
            result.addAll(operand.getReadVariables());
        }
        return result;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        EList<String> result = new BasicEList<String>();
        for (IOperand operand : this.get_Operands()) {
            result.addAll(operand.getWriteVariables());
        }
        return result;
    }


} // SignalComparisonExpressionImpl
