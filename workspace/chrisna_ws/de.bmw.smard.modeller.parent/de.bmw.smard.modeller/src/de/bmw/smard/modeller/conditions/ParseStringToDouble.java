package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parse String To Double</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ParseStringToDouble#getStringToParse <em>String To Parse</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getParseStringToDouble()
 * @model
 * @generated
 */
public interface ParseStringToDouble extends INumericOperation {
    /**
     * Returns the value of the '<em><b>String To Parse</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>String To Parse</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>String To Parse</em>' containment reference.
     * @see #setStringToParse(IStringOperand)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getParseStringToDouble_StringToParse()
     * @model containment="true" required="true"
     * @generated
     */
    IStringOperand getStringToParse();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ParseStringToDouble#getStringToParse <em>String To Parse</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>String To Parse</em>' containment reference.
     * @see #getStringToParse()
     * @generated
     */
    void setStringToParse(IStringOperand value);

} // ParseStringToDouble
