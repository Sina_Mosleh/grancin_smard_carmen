package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.IStringOperand;
import de.bmw.smard.modeller.conditions.ParseStringToDouble;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parse String To Double</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ParseStringToDoubleImpl#getStringToParse <em>String To Parse</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParseStringToDoubleImpl extends EObjectImpl implements ParseStringToDouble {
    /**
     * The cached value of the '{@link #getStringToParse() <em>String To Parse</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStringToParse()
     * @generated
     * @ordered
     */
    protected IStringOperand stringToParse;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ParseStringToDoubleImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PARSE_STRING_TO_DOUBLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getStringToParse() {
        return stringToParse;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetStringToParse(IStringOperand newStringToParse, NotificationChain msgs) {
        IStringOperand oldStringToParse = stringToParse;
        stringToParse = newStringToParse;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, oldStringToParse, newStringToParse);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStringToParse(IStringOperand newStringToParse) {
        if (newStringToParse != stringToParse) {
            NotificationChain msgs = null;
            if (stringToParse != null)
                msgs = ((InternalEObject) stringToParse).eInverseRemove(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, null, msgs);
            if (newStringToParse != null)
                msgs = ((InternalEObject) newStringToParse).eInverseAdd(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, null, msgs);
            msgs = basicSetStringToParse(newStringToParse, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE, newStringToParse, newStringToParse));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        return DataType.STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.add(getStringToParse());
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        if (getStringToParse() == null) {
            return ECollections.emptyEList();
        }
        return getStringToParse().getReadVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        if (getStringToParse() == null) {
            return ECollections.emptyEList();
        }
        return getStringToParse().getWriteVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
                return basicSetStringToParse(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
                return getStringToParse();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
                setStringToParse((IStringOperand) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
                setStringToParse((IStringOperand) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE:
                return stringToParse != null;
        }
        return super.eIsSet(featureID);
    }

} //ParseStringToDoubleImpl
