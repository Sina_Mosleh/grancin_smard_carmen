package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Transition;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl#getInitialState <em>Initial State</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl#getStates <em>States</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl#getTransitions <em>Transitions</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl#isActiveAtStart <em>Active At Start</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateMachineImpl extends BaseClassWithIDImpl implements StateMachine {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = "StateMachine";

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getInitialState() <em>Initial State</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInitialState()
     * @generated
     * @ordered
     */
    protected InitialState initialState;

    /**
     * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStates()
     * @generated
     * @ordered
     */
    protected EList<State> states;

    /**
     * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTransitions()
     * @generated
     * @ordered
     */
    protected EList<Transition> transitions;

    /**
     * The default value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isActiveAtStart()
     * @generated
     * @ordered
     */
    protected static final boolean ACTIVE_AT_START_EDEFAULT = true;

    /**
     * The cached value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isActiveAtStart()
     * @generated
     * @ordered
     */
    protected boolean activeAtStart = ACTIVE_AT_START_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * modified to have unique id
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StateMachineImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.STATE_MACHINE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        for (AbstractState abstractState : getStates()) {
            messages.addAll(abstractState.getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.STATE_MACHINE__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public InitialState getInitialState() {
        return initialState;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetInitialState(InitialState newInitialState, NotificationChain msgs) {
        InitialState oldInitialState = initialState;
        initialState = newInitialState;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, StatemachinePackage.STATE_MACHINE__INITIAL_STATE, oldInitialState, newInitialState);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInitialState(InitialState newInitialState) {
        if (newInitialState != initialState) {
            NotificationChain msgs = null;
            if (initialState != null)
                msgs = ((InternalEObject) initialState).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachinePackage.STATE_MACHINE__INITIAL_STATE, null,
                        msgs);
            if (newInitialState != null)
                msgs = ((InternalEObject) newInitialState).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachinePackage.STATE_MACHINE__INITIAL_STATE, null,
                        msgs);
            msgs = basicSetInitialState(newInitialState, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.STATE_MACHINE__INITIAL_STATE, newInitialState, newInitialState));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<State> getStates() {
        if (states == null) {
            states = new EObjectContainmentEList<State>(State.class, this, StatemachinePackage.STATE_MACHINE__STATES);
        }
        return states;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Transition> getTransitions() {
        if (transitions == null) {
            transitions = new EObjectContainmentEList<Transition>(Transition.class, this, StatemachinePackage.STATE_MACHINE__TRANSITIONS);
        }
        return transitions;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isActiveAtStart() {
        return activeAtStart;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActiveAtStart(boolean newActiveAtStart) {
        boolean oldActiveAtStart = activeAtStart;
        activeAtStart = newActiveAtStart;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.STATE_MACHINE__ACTIVE_AT_START, oldActiveAtStart, activeAtStart));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>();
        EList<Transition> proxyTransitions = getTransitions();
        if (proxyTransitions != null && !proxyTransitions.isEmpty()) {
            for (Transition transition : proxyTransitions) {
                result.addAll(transition.getReadVariables());
            }
        }
        EList<State> proxyStates = getStates();
        if (proxyStates != null && !proxyStates.isEmpty()) {
            for (State state : proxyStates) {
                result.addAll(state.getReadVariables());
            }
        }
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        EList<String> result = new BasicEList<String>();

        EList<Transition> proxyTransitions = getTransitions();
        if (proxyTransitions != null) {
            for (Transition transition : proxyTransitions) {
                result.addAll(transition.getWriteVariables());
            }
        }

        EList<State> proxyStates = getStates();
        if (proxyStates != null && !proxyStates.isEmpty()) {
            for (State state : proxyStates) {
                result.addAll(state.getWriteVariables());
            }
        }

        EList<String> uniqueResults = new BasicEList<String>();
        for (String varName : result) {
            if (!uniqueResults.contains(varName)) {
                uniqueResults.add(varName);
            }
        }
        return uniqueResults;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case StatemachinePackage.STATE_MACHINE__INITIAL_STATE:
                return basicSetInitialState(null, msgs);
            case StatemachinePackage.STATE_MACHINE__STATES:
                return ((InternalEList<?>) getStates()).basicRemove(otherEnd, msgs);
            case StatemachinePackage.STATE_MACHINE__TRANSITIONS:
                return ((InternalEList<?>) getTransitions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.STATE_MACHINE__NAME:
                return getName();
            case StatemachinePackage.STATE_MACHINE__INITIAL_STATE:
                return getInitialState();
            case StatemachinePackage.STATE_MACHINE__STATES:
                return getStates();
            case StatemachinePackage.STATE_MACHINE__TRANSITIONS:
                return getTransitions();
            case StatemachinePackage.STATE_MACHINE__ACTIVE_AT_START:
                return isActiveAtStart();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.STATE_MACHINE__NAME:
                setName((String) newValue);
                return;
            case StatemachinePackage.STATE_MACHINE__INITIAL_STATE:
                setInitialState((InitialState) newValue);
                return;
            case StatemachinePackage.STATE_MACHINE__STATES:
                getStates().clear();
                getStates().addAll((Collection<? extends State>) newValue);
                return;
            case StatemachinePackage.STATE_MACHINE__TRANSITIONS:
                getTransitions().clear();
                getTransitions().addAll((Collection<? extends Transition>) newValue);
                return;
            case StatemachinePackage.STATE_MACHINE__ACTIVE_AT_START:
                setActiveAtStart((Boolean) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.STATE_MACHINE__NAME:
                setName(NAME_EDEFAULT);
                return;
            case StatemachinePackage.STATE_MACHINE__INITIAL_STATE:
                setInitialState((InitialState) null);
                return;
            case StatemachinePackage.STATE_MACHINE__STATES:
                getStates().clear();
                return;
            case StatemachinePackage.STATE_MACHINE__TRANSITIONS:
                getTransitions().clear();
                return;
            case StatemachinePackage.STATE_MACHINE__ACTIVE_AT_START:
                setActiveAtStart(ACTIVE_AT_START_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.STATE_MACHINE__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case StatemachinePackage.STATE_MACHINE__INITIAL_STATE:
                return initialState != null;
            case StatemachinePackage.STATE_MACHINE__STATES:
                return states != null && !states.isEmpty();
            case StatemachinePackage.STATE_MACHINE__TRANSITIONS:
                return transitions != null && !transitions.isEmpty();
            case StatemachinePackage.STATE_MACHINE__ACTIVE_AT_START:
                return activeAtStart != ACTIVE_AT_START_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", activeAtStart: ");
        result.append(activeAtStart);
        result.append(')');
        return result.toString();
    }

} //StateMachineImpl
