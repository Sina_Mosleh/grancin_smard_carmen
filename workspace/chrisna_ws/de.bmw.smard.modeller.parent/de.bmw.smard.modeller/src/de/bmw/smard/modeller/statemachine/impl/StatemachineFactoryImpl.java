package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.statemachine.Action;
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty;
import de.bmw.smard.modeller.statemachine.ComputeVariable;
import de.bmw.smard.modeller.statemachine.ControlAction;
import de.bmw.smard.modeller.statemachine.ControlType;
import de.bmw.smard.modeller.statemachine.DocumentRoot;
import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.ShowVariable;
import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StateType;
import de.bmw.smard.modeller.statemachine.StatemachineFactory;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.statemachine.Trigger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatemachineFactoryImpl extends EFactoryImpl implements StatemachineFactory {
    /**
     * Creates the default factory implementation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static StatemachineFactory init() {
        try {
            StatemachineFactory theStatemachineFactory = (StatemachineFactory) EPackage.Registry.INSTANCE.getEFactory(StatemachinePackage.eNS_URI);
            if (theStatemachineFactory != null) {
                return theStatemachineFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new StatemachineFactoryImpl();
    }

    /**
     * Creates an instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachineFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
            case StatemachinePackage.DOCUMENT_ROOT:
                return createDocumentRoot();
            case StatemachinePackage.TRANSITION:
                return createTransition();
            case StatemachinePackage.STATE_MACHINE:
                return createStateMachine();
            case StatemachinePackage.ACTION:
                return createAction();
            case StatemachinePackage.INITIAL_STATE:
                return createInitialState();
            case StatemachinePackage.STATE:
                return createState();
            case StatemachinePackage.COMPUTE_VARIABLE:
                return createComputeVariable();
            case StatemachinePackage.SHOW_VARIABLE:
                return createShowVariable();
            case StatemachinePackage.CONTROL_ACTION:
                return createControlAction();
            default:
                throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
            case StatemachinePackage.TRIGGER:
                return createTriggerFromString(eDataType, initialValue);
            case StatemachinePackage.STATE_TYPE:
                return createStateTypeFromString(eDataType, initialValue);
            case StatemachinePackage.ACTION_TYPE_NOT_EMPTY:
                return createActionTypeNotEmptyFromString(eDataType, initialValue);
            case StatemachinePackage.CONTROL_TYPE:
                return createControlTypeFromString(eDataType, initialValue);
            case StatemachinePackage.INT_OR_TEMPLATE_PLACEHOLDER:
                return createIntOrTemplatePlaceholderFromString(eDataType, initialValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
            case StatemachinePackage.TRIGGER:
                return convertTriggerToString(eDataType, instanceValue);
            case StatemachinePackage.STATE_TYPE:
                return convertStateTypeToString(eDataType, instanceValue);
            case StatemachinePackage.ACTION_TYPE_NOT_EMPTY:
                return convertActionTypeNotEmptyToString(eDataType, instanceValue);
            case StatemachinePackage.CONTROL_TYPE:
                return convertControlTypeToString(eDataType, instanceValue);
            case StatemachinePackage.INT_OR_TEMPLATE_PLACEHOLDER:
                return convertIntOrTemplatePlaceholderToString(eDataType, instanceValue);
            default:
                throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public DocumentRoot createDocumentRoot() {
        DocumentRootImpl documentRoot = new DocumentRootImpl();
        return documentRoot;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Transition createTransition() {
        TransitionImpl transition = new TransitionImpl();
        return transition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public StateMachine createStateMachine() {
        StateMachineImpl stateMachine = new StateMachineImpl();
        return stateMachine;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Action createAction() {
        ActionImpl action = new ActionImpl();
        return action;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public InitialState createInitialState() {
        InitialStateImpl initialState = new InitialStateImpl();
        return initialState;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public State createState() {
        StateImpl state = new StateImpl();
        return state;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ComputeVariable createComputeVariable() {
        ComputeVariableImpl computeVariable = new ComputeVariableImpl();
        return computeVariable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public ShowVariable createShowVariable() {
        return new ShowVariableImpl();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ControlAction createControlAction() {
        ControlActionImpl controlAction = new ControlActionImpl();
        return controlAction;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Trigger createTriggerFromString(EDataType eDataType, String initialValue) {
        Trigger result = Trigger.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertTriggerToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StateType createStateTypeFromString(EDataType eDataType, String initialValue) {
        StateType result = StateType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertStateTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ActionTypeNotEmpty createActionTypeNotEmptyFromString(EDataType eDataType, String initialValue) {
        ActionTypeNotEmpty result = ActionTypeNotEmpty.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertActionTypeNotEmptyToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ControlType createControlTypeFromString(EDataType eDataType, String initialValue) {
        ControlType result = ControlType.get(initialValue);
        if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertControlTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String createIntOrTemplatePlaceholderFromString(EDataType eDataType, String initialValue) {
        return (String) super.createFromString(eDataType, initialValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertIntOrTemplatePlaceholderToString(EDataType eDataType, Object instanceValue) {
        return super.convertToString(eDataType, instanceValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public StatemachinePackage getStatemachinePackage() {
        return (StatemachinePackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static StatemachinePackage getPackage() {
        return StatemachinePackage.eINSTANCE;
    }

} //StatemachineFactoryImpl
