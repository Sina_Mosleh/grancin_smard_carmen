package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;

public class IntTemplateValidator {

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> builder(String value) {
        return new Builder(value);
    }

    public static class Builder extends ValueTemplateValidator.Builder {

        Builder(String value) {
            super(value);
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            if(!super.validateValue(baseValidator, exportable)) {
                return false;
            }

            String substitution = TemplateUtils.checkIntNotPlaceholder(getValue(), true);
            if (!StringUtils.nullOrEmpty(substitution)) {
                attachError(baseValidator, getIllegalValueErrorId(), substitution);
                return false;
            }

            return true;
        }
    }
}
