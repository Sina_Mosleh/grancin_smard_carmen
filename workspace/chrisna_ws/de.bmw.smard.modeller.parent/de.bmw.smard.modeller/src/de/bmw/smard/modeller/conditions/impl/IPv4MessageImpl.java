package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.IPv4Filter;
import de.bmw.smard.modeller.conditions.IPv4Message;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPv4 Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4MessageImpl#getIPv4Filter <em>IPv4 Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IPv4MessageImpl extends AbstractBusMessageImpl implements IPv4Message {
    /**
     * The cached value of the '{@link #getIPv4Filter() <em>IPv4 Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIPv4Filter()
     * @generated
     * @ordered
     */
    protected IPv4Filter iPv4Filter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IPv4MessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.IPV4_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IPv4Filter getIPv4Filter() {
        return iPv4Filter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetIPv4Filter(IPv4Filter newIPv4Filter, NotificationChain msgs) {
        IPv4Filter oldIPv4Filter = iPv4Filter;
        iPv4Filter = newIPv4Filter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER, oldIPv4Filter, newIPv4Filter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setIPv4Filter(IPv4Filter newIPv4Filter) {
        if (newIPv4Filter != iPv4Filter) {
            NotificationChain msgs = null;
            if (iPv4Filter != null)
                msgs = ((InternalEObject) iPv4Filter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER, null, msgs);
            if (newIPv4Filter != null)
                msgs = ((InternalEObject) newIPv4Filter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER, null, msgs);
            msgs = basicSetIPv4Filter(newIPv4Filter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER, newIPv4Filter, newIPv4Filter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER:
                return basicSetIPv4Filter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER:
                return getIPv4Filter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER:
                setIPv4Filter((IPv4Filter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER:
                setIPv4Filter((IPv4Filter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.IPV4_MESSAGE__IPV4_FILTER:
                return iPv4Filter != null;
        }
        return super.eIsSet(featureID);
    }

    @Override
    public AbstractFilter getPrimaryFilter() {
        return iPv4Filter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //IPv4MessageImpl
