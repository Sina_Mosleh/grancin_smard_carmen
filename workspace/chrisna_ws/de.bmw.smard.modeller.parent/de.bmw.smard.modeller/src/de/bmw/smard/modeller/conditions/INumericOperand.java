package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INumeric Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getINumericOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface INumericOperand extends IOperand, IComputeVariableActionOperand {
} // INumericOperand
