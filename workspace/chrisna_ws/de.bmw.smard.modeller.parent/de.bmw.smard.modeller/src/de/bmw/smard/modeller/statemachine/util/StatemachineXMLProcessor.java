package de.bmw.smard.modeller.statemachine.util;

import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import java.util.Map;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatemachineXMLProcessor extends XMLProcessor {

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachineXMLProcessor() {
        super((EPackage.Registry.INSTANCE));
        StatemachinePackage.eINSTANCE.eClass();
    }

    /**
     * Register for "*" and "xml" file extensions the StatemachineResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            super.getRegistrations();
            registrations.put(XML_EXTENSION, new StatemachineResourceFactoryImpl());
            registrations.put(STAR_EXTENSION, new StatemachineResourceFactoryImpl());
        }
        return registrations;
    }

} //StatemachineXMLProcessor
