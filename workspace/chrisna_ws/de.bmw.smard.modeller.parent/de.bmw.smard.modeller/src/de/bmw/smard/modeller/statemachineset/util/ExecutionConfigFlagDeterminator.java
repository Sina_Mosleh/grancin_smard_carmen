package de.bmw.smard.modeller.statemachineset.util;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.statemachineset.ExecutionConfig;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import java.util.*;

public class ExecutionConfigFlagDeterminator {

    public static void determineEthernetAndErrorFrames(ExecutionConfig executionConfig) {
        EObject parent = executionConfig.eContainer();
        if (!(parent instanceof StateMachineSet)) {
            return;
        }
        determineEthernetAndErrorFrames(executionConfig, (StateMachineSet) parent);
    }

    public static void determineEthernetAndErrorFrames(ExecutionConfig executionConfig, StateMachineSet stateMachineSet) {
        EList<AbstractBusMessage> messages = stateMachineSet.getAllReferencedBusMessages();
        setFlags(new HashSet<AbstractBusMessage>(messages), executionConfig);
    }

    private static void setFlags(Set<AbstractBusMessage> messages, ExecutionConfig executionConfig) {
        boolean needsEthernet = false;
        boolean needsErrorFrames = false;
        for (AbstractBusMessage message : messages) {
            if (!needsEthernet) {
                needsEthernet = message.isNeedsEthernet();
            }
            if (!needsErrorFrames) {
                needsErrorFrames = message.isNeedsErrorFrames();
            }

            if (needsEthernet && needsErrorFrames) {
                break;
            }
        }
        executionConfig.setNeedsEthernet(needsEthernet);
        executionConfig.setNeedsErrorFrames(needsErrorFrames);
    }

}
