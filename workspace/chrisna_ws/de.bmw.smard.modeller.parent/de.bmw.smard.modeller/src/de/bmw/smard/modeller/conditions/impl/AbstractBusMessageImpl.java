package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.config.OsiLayerConfig;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.BaseValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Abstract Bus Message</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl#getBusId <em>Bus Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl#getBusIdRange <em>Bus Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractBusMessageImpl extends AbstractMessageImpl implements AbstractBusMessage {
    /**
     * The default value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusId()
     * @generated
     * @ordered
     */
    protected static final String BUS_ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusId()
     * @generated
     * @ordered
     */
    protected String busId = BUS_ID_EDEFAULT;

    /**
     * The default value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdTmplParam()
     * @generated
     * @ordered
     */
    protected static final String BUS_ID_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdTmplParam()
     * @generated
     * @ordered
     */
    protected String busIdTmplParam = BUS_ID_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getBusIdRange() <em>Bus Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdRange()
     * @generated
     * @ordered
     */
    protected static final String BUS_ID_RANGE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getBusIdRange() <em>Bus Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdRange()
     * @generated
     * @ordered
     */
    protected String busIdRange = BUS_ID_RANGE_EDEFAULT;

    /**
     * The cached value of the '{@link #getFilters() <em>Filters</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFilters()
     * @generated
     * @ordered
     */
    protected EList<AbstractFilter> filters;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractBusMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.ABSTRACT_BUS_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusId() {
        return busId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusId(String newBusId) {
        String oldBusId = busId;
        busId = newBusId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID, oldBusId, busId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusIdTmplParam() {
        return busIdTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusIdTmplParam(String newBusIdTmplParam) {
        String oldBusIdTmplParam = busIdTmplParam;
        busIdTmplParam = newBusIdTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM, oldBusIdTmplParam, busIdTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusIdRange() {
        return busIdRange;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusIdRange(String newBusIdRange) {
        String oldBusIdRange = busIdRange;
        busIdRange = newBusIdRange;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE, oldBusIdRange, busIdRange));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractFilter> getFilters() {
        if (filters == null) {
            filters = new EObjectContainmentEList<AbstractFilter>(AbstractFilter.class, this, ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS);
        }
        return filters;
    }

    /**
     * @generated NOT
     */
    public boolean addFilter(AbstractFilter filter) {
        if (filters == null) {
            filters = new EObjectContainmentEList<AbstractFilter>(AbstractFilter.class, this, ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS);
        }
        if (filter != null && !filters.contains(filter)) {
            filters.add(filter);
            return true;
        }

        return false;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        BaseValidator.WithStep validator = Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_HAS_VALID_BUS_ID)
                .object(this)
                .diagnostic(diagnostics);

        if (StringUtils.isBlank(busIdRange)) {
            validator
                    .with(Validators.notNull(busId)
                            .error("_Validation_Conditions_AbstractBusMessage_BusId_IsNullOrZero")
                            .build())

                    .with(Validators.checkTemplate(busId)
                            .tmplParam(busIdTmplParam)
                            .containsParameterError("_Validation_Conditions_AbstractBusMessage_BusId_ContainsPlaceholders")
                            .tmplParamIsNullError("_Validation_Conditions_AbstractBusMessage_BusIdTmplParam_IsNull")
                            .noPlaceholderError("_Validation_Conditions_AbstractBusMessage_BusIdTmplParam_NotAValidPlaceholderName")
                            .illegalValueError(ValueValidators.busId(AbstractBusMessage.class.getSimpleName()))
                            .build());

        } else {
            validator.with(Validators.stringTemplate(busIdRange)
                    .containsParameterError("_Validation_Conditions_AbstractBusMessage_BusIdRange_ContainsPlaceholders")
                    .invalidPlaceholderError("_Validation_Conditions_AbstractBusMessage_BusIdTmplParam_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.busIdRange(AbstractBusMessage.class.getSimpleName()))
                    .build());
        }
        return validator.validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_isOsiLayerConform(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean onlyOneFilterOfAType = true;
        boolean notUDPandTCP = true;
        boolean filtersCompatibleWithMessage = true;

        String errorMessageIdentifier = "_Validation_Conditions_AbstractBusMessage_isOsiLayerConform_Error";

        // TODO fix validation
        //		// check if only one of each filter type
        //		//FIXME mj
        //		onlyOneFilterOfAType = hasOnlyOneFilterOfAType(this);
        //		if(!onlyOneFilterOfAType) {
        //			errorMessageIdentifier = "_Validation_Conditions_AbstractBusMessage_isOsiLayerConform_TwoFilters";
        //
        //			if (diagnostics != null) {
        //				diagnostics.add
        //					(new BasicDiagnostic
        //						(Diagnostic.ERROR,
        //						 ConditionsValidator.DIAGNOSTIC_SOURCE,
        //						 ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_IS_OSI_LAYER_CONFORM,
        //						 SMARDModelValidator.isEngineContext(context) ?
        //									errorMessageIdentifier :
        //										(
        //						 PluginActivator.getInstance().getString(errorMessageIdentifier)),
        //						 new Object [] { this }));
        //			}
        //		}
        //
        //		// check if not UDP- and TCPFilter
        //		notUDPandTCP = hasOnlyOneFilterOfTCPorUDP(this);
        //		if(!notUDPandTCP) {
        //			errorMessageIdentifier = "_Validation_Conditions_AbstractBusMessage_isOsiLayerConform_UDPandTCP";
        //
        //			if (diagnostics != null) {
        //				diagnostics.add
        //					(new BasicDiagnostic
        //						(Diagnostic.ERROR,
        //						 ConditionsValidator.DIAGNOSTIC_SOURCE,
        //						 ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_IS_OSI_LAYER_CONFORM,
        //						 SMARDModelValidator.isEngineContext(context) ?
        //									errorMessageIdentifier :
        //										(
        //						 PluginActivator.getInstance().getString(errorMessageIdentifier)),
        //						 new Object [] { this }));
        //			}
        //		}

        // check if filter are compatible with type
        for (AbstractFilter filter : this.getFilters()) {
            filtersCompatibleWithMessage = OsiLayerConfig.isCompatible(filter, this)
                || this.getPrimaryFilter().equals(filter);
            if (!filtersCompatibleWithMessage) {
                errorMessageIdentifier = "_Validation_Conditions_AbstractBusMessage_isOsiLayerConform_FilterNotApplicable";

                if (diagnostics != null) {
                    diagnostics.add(DiagnosticBuilder.error()
                            .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                            .code(ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_IS_OSI_LAYER_CONFORM)
                            .messageId(PluginActivator.INSTANCE, errorMessageIdentifier)
                            .data(this)
                            .build());
                }
            }
        }

        return onlyOneFilterOfAType && filtersCompatibleWithMessage;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return false;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsErrorFrames() {
        return false;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
                return ((InternalEList<?>) getFilters()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
                return getBusId();
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
                return getBusIdTmplParam();
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
                return getBusIdRange();
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
                return getFilters();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
                setBusId((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
                setBusIdTmplParam((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
                setBusIdRange((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
                getFilters().clear();
                getFilters().addAll((Collection<? extends AbstractFilter>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
                setBusId(BUS_ID_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
                setBusIdTmplParam(BUS_ID_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
                setBusIdRange(BUS_ID_RANGE_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
                getFilters().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
                return BUS_ID_EDEFAULT == null ? busId != null : !BUS_ID_EDEFAULT.equals(busId);
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
                return BUS_ID_TMPL_PARAM_EDEFAULT == null ? busIdTmplParam != null : !BUS_ID_TMPL_PARAM_EDEFAULT.equals(busIdTmplParam);
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
                return BUS_ID_RANGE_EDEFAULT == null ? busIdRange != null : !BUS_ID_RANGE_EDEFAULT.equals(busIdRange);
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
                return filters != null && !filters.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (busId: ");
        result.append(busId);
        result.append(", busIdTmplParam: ");
        result.append(busIdTmplParam);
        result.append(", busIdRange: ");
        result.append(busIdRange);
        result.append(')');
        return result.toString();
    }

    /** @generated NOT */
    private boolean hasOnlyOneFilterOfAType(AbstractBusMessage message) {
        boolean result = true;

        List<AbstractFilter> filters = message.getFilters();
        for (AbstractFilter filter : filters) {
            for (AbstractFilter compareFilter : filters) {
                // check if the classes of two filters are the same
                // but it's not the same filter ;-)
                if (compareFilter.eClass().equals(filter.eClass()) && !compareFilter.equals(filter)) {
                    result = false;
                    return result;
                }
            }
        }

        return result;
    }

    /** @generated NOT */
    private boolean hasOnlyOneFilterOfTCPorUDP(AbstractBusMessage message) {
        boolean tcpFilterFound = false;
        boolean udpFilterFound = false;

        List<AbstractFilter> filters = message.getFilters();
        for (AbstractFilter filter : filters) {
            if (filter instanceof TCPFilterImpl) {
                tcpFilterFound = true;
            }
            if (filter instanceof UDPFilterImpl) {
                udpFilterFound = true;
            }
        }

        return !(tcpFilterFound && udpFilterFound);
    }

} // AbstractBusMessageImpl
