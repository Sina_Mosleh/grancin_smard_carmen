package de.bmw.smard.modeller.util.validation;

import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

public interface IModelValidator {
	
	boolean validate(Set<EObject> objects2Validate, 
			DiagnosticChain diagChain,
			Map<Object, Object> context);
	
	boolean validate(EObject object2Validate, 
			DiagnosticChain diagChain,
			Map<Object, Object> context);
	
	boolean accept4Validation(Object objToValidate);
	
	<T> boolean accept4Validation(Class<T> classToValidate);
	
	<T> Set<Class<T>> getRelevantTypes();
	
	boolean isApplicableIn(SMARDModelValidator.ValidationContext context);
	
}
