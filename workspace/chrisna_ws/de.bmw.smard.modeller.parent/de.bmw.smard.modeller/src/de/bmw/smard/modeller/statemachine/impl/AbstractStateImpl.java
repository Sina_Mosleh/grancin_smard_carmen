/**
 */
package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl;
import de.bmw.smard.modeller.statemachine.AbstractAction;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.StateType;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Abstract State</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getOut <em>Out</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getIn <em>In</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getActions <em>Actions</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl#getStateType <em>State Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractStateImpl extends BaseClassWithIDImpl implements AbstractState {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getOut() <em>Out</em>}' reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getOut()
     * @generated
     * @ordered
     */
    protected EList<Transition> out;

    /**
     * The cached value of the '{@link #getIn() <em>In</em>}' reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getIn()
     * @generated
     * @ordered
     */
    protected EList<Transition> in;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getActions()
     * @generated
     * @ordered
     */
    protected EList<AbstractAction> actions;

    /**
     * The default value of the '{@link #getStateType() <em>State Type</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getStateType()
     * @generated
     * @ordered
     */
    protected static final StateType STATE_TYPE_EDEFAULT = StateType.INFO;

    /**
     * The cached value of the '{@link #getStateType() <em>State Type</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getStateType()
     * @generated
     * @ordered
     */
    protected StateType stateType = STATE_TYPE_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractStateImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.ABSTRACT_STATE;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Transition> getOut() {
        if (out == null) {
            out = new EObjectWithInverseEList<Transition>(Transition.class, this, StatemachinePackage.ABSTRACT_STATE__OUT, StatemachinePackage.TRANSITION__FROM);
        }
        return out;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Transition> getIn() {
        if (in == null) {
            in = new EObjectWithInverseEList<Transition>(Transition.class, this, StatemachinePackage.ABSTRACT_STATE__IN, StatemachinePackage.TRANSITION__TO);
        }
        return in;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractAction> getActions() {
        if (actions == null) {
            actions = new EObjectContainmentEList<AbstractAction>(AbstractAction.class, this, StatemachinePackage.ABSTRACT_STATE__ACTIONS);
        }
        return actions;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public StateType getStateType() {
        return stateType;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStateType(StateType newStateType) {
        StateType oldStateType = stateType;
        stateType = newStateType == null ? STATE_TYPE_EDEFAULT : newStateType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__STATE_TYPE, oldStateType, stateType));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.ABSTRACT_STATE__IS_VALID_HAS_VALID_NAME)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.stringTemplate(name)
                        .containsParameterError("_Validation_Statemachine_State_Name_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Statemachine_State_Name_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        for (AbstractAction action : getActions()) {
            messages.addAll(action.getAllReferencedBusMessages());
        }
        for(Transition transition : getIn()){
            messages.addAll(transition.getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.ABSTRACT_STATE__IS_VALID_HAS_VALID_DESCRIPTION)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.stringTemplate(description)
                        .containsParameterError("_Validation_Statemachine_State_Description_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Statemachine_State_Description_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>();
        EList<AbstractAction> proxyActions = getActions();
        if (proxyActions != null && proxyActions.size() > 0) {
            for (AbstractAction action : proxyActions) {
                result.addAll(action.getReadVariables());
            }
        }
        EList<Transition> proxyTransitions = getOut();
        if (proxyTransitions != null && proxyTransitions.size() > 0) {
            for (Transition out : proxyTransitions) {
                result.addAll(out.getReadVariables());
            }
        }
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        EList<String> result = new BasicEList<String>();
        if (actions != null && actions.size() > 0) {
            for (AbstractAction action : actions) {
                result.addAll(action.getWriteVariables());
            }
        }
        if (getOut() != null && getOut().size() > 0) {
            for (Transition out : getOut()) {
                result.addAll(out.getWriteVariables());
            }
        }
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getOut()).basicAdd(otherEnd, msgs);
            case StatemachinePackage.ABSTRACT_STATE__IN:
                return ((InternalEList<InternalEObject>) (InternalEList<?>) getIn()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                return ((InternalEList<?>) getOut()).basicRemove(otherEnd, msgs);
            case StatemachinePackage.ABSTRACT_STATE__IN:
                return ((InternalEList<?>) getIn()).basicRemove(otherEnd, msgs);
            case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
                return ((InternalEList<?>) getActions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__NAME:
                return getName();
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                return getOut();
            case StatemachinePackage.ABSTRACT_STATE__IN:
                return getIn();
            case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
                return getDescription();
            case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
                return getActions();
            case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
                return getStateType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__NAME:
                setName((String) newValue);
                return;
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                getOut().clear();
                getOut().addAll((Collection<? extends Transition>) newValue);
                return;
            case StatemachinePackage.ABSTRACT_STATE__IN:
                getIn().clear();
                getIn().addAll((Collection<? extends Transition>) newValue);
                return;
            case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
                setDescription((String) newValue);
                return;
            case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
                getActions().clear();
                getActions().addAll((Collection<? extends AbstractAction>) newValue);
                return;
            case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
                setStateType((StateType) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__NAME:
                setName(NAME_EDEFAULT);
                return;
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                getOut().clear();
                return;
            case StatemachinePackage.ABSTRACT_STATE__IN:
                getIn().clear();
                return;
            case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
                getActions().clear();
                return;
            case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
                setStateType(STATE_TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_STATE__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case StatemachinePackage.ABSTRACT_STATE__OUT:
                return out != null && !out.isEmpty();
            case StatemachinePackage.ABSTRACT_STATE__IN:
                return in != null && !in.isEmpty();
            case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
                return actions != null && !actions.isEmpty();
            case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
                return stateType != STATE_TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", description: ");
        result.append(description);
        result.append(", stateType: ");
        result.append(stateType);
        result.append(')');
        return result.toString();
    }

} // AbstractStateImpl
