package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.UDPFilter;
import de.bmw.smard.modeller.conditions.UDPMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UDP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UDPMessageImpl#getUdpFilter <em>Udp Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UDPMessageImpl extends AbstractBusMessageImpl implements UDPMessage {
    /**
     * The cached value of the '{@link #getUdpFilter() <em>Udp Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getUdpFilter()
     * @generated
     * @ordered
     */
    protected UDPFilter udpFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UDPMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.UDP_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public UDPFilter getUdpFilter() {
        return udpFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetUdpFilter(UDPFilter newUdpFilter, NotificationChain msgs) {
        UDPFilter oldUdpFilter = udpFilter;
        udpFilter = newUdpFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDP_MESSAGE__UDP_FILTER, oldUdpFilter, newUdpFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUdpFilter(UDPFilter newUdpFilter) {
        if (newUdpFilter != udpFilter) {
            NotificationChain msgs = null;
            if (udpFilter != null)
                msgs = ((InternalEObject) udpFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDP_MESSAGE__UDP_FILTER, null, msgs);
            if (newUdpFilter != null)
                msgs = ((InternalEObject) newUdpFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDP_MESSAGE__UDP_FILTER, null, msgs);
            msgs = basicSetUdpFilter(newUdpFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDP_MESSAGE__UDP_FILTER, newUdpFilter, newUdpFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.UDP_MESSAGE__UDP_FILTER:
                return basicSetUdpFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.UDP_MESSAGE__UDP_FILTER:
                return getUdpFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.UDP_MESSAGE__UDP_FILTER:
                setUdpFilter((UDPFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDP_MESSAGE__UDP_FILTER:
                setUdpFilter((UDPFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDP_MESSAGE__UDP_FILTER:
                return udpFilter != null;
        }
        return super.eIsSet(featureID);
    }

    @Override
    public AbstractFilter getPrimaryFilter() {
        return udpFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //UDPMessageImpl
