package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.util.TemplateUtils;

public class BooleanTemplateAttributeValidator {

    public static TemplateAttributeValidator.TemplateTypeStep<IllegalValueErrorStep> builder(BooleanOrTemplatePlaceholderEnum value) {
        return new BooleanTemplateAttributeValidator.Builder(value);
    }

    public interface IllegalValueErrorStep extends Build {
       Build illegalValueError(String errorId);
    }

    public static class Builder extends TemplateAttributeValidator.Builder<BooleanOrTemplatePlaceholderEnum, IllegalValueErrorStep> implements
            IllegalValueErrorStep {

        private String illegalValueErrorId;

        Builder(BooleanOrTemplatePlaceholderEnum value) {
            super(value);
        }

        @Override
        public Build illegalValueError(String errorId) {
            this.illegalValueErrorId = errorId;
            return this;
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            String detail = TemplateUtils.checkBooleanNotPlaceholder(getValue(), false);

            if (detail != null) {
                attachError(baseValidator, illegalValueErrorId, detail);
            }

            return detail == null;
        }
    }
}
