package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractSignal;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ExtractStrategy;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ExtractStrategyImpl#getAbstractSignal <em>Abstract Signal</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ExtractStrategyImpl extends EObjectImpl implements ExtractStrategy {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.EXTRACT_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AbstractSignal getAbstractSignal() {
        if (eContainerFeatureID() != ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL) return null;
        return (AbstractSignal) eInternalContainer();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetAbstractSignal(AbstractSignal newAbstractSignal, NotificationChain msgs) {
        msgs = eBasicSetContainer((InternalEObject) newAbstractSignal, ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL, msgs);
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAbstractSignal(AbstractSignal newAbstractSignal) {
        if (newAbstractSignal != eInternalContainer()
            || (eContainerFeatureID() != ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL && newAbstractSignal != null)) {
            if (EcoreUtil.isAncestor(this, newAbstractSignal))
                throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
            NotificationChain msgs = null;
            if (eInternalContainer() != null)
                msgs = eBasicRemoveFromContainer(msgs);
            if (newAbstractSignal != null)
                msgs = ((InternalEObject) newAbstractSignal).eInverseAdd(this, ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY, AbstractSignal.class, msgs);
            msgs = basicSetAbstractSignal(newAbstractSignal, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL, newAbstractSignal, newAbstractSignal));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                if (eInternalContainer() != null)
                    msgs = eBasicRemoveFromContainer(msgs);
                return basicSetAbstractSignal((AbstractSignal) otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                return basicSetAbstractSignal(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
        switch (eContainerFeatureID()) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                return eInternalContainer().eInverseRemove(this, ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY, AbstractSignal.class, msgs);
        }
        return super.eBasicRemoveFromContainerFeature(msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                return getAbstractSignal();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                setAbstractSignal((AbstractSignal) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                setAbstractSignal((AbstractSignal) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL:
                return getAbstractSignal() != null;
        }
        return super.eIsSet(featureID);
    }

} //ExtractStrategyImpl
