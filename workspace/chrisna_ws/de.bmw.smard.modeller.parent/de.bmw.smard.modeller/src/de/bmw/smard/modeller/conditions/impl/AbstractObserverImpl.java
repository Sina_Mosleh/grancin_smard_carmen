package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ObserverValueRange;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.SmardTraceElementIdentifierHelper;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Abstract Observer</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#getLogLevel <em>Log Level</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#getValueRanges <em>Value Ranges</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#getValueRangesTmplParam <em>Value Ranges Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl#isActiveAtStart <em>Active At Start</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractObserverImpl extends BaseClassWithSourceReferenceImpl implements AbstractObserver {
    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLogLevel()
     * @generated
     * @ordered
     */
    protected static final String LOG_LEVEL_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLogLevel()
     * @generated
     * @ordered
     */
    protected String logLevel = LOG_LEVEL_EDEFAULT;

    /**
     * The cached value of the '{@link #getValueRanges() <em>Value Ranges</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueRanges()
     * @generated
     * @ordered
     */
    protected EList<ObserverValueRange> valueRanges;

    /**
     * The default value of the '{@link #getValueRangesTmplParam() <em>Value Ranges Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueRangesTmplParam()
     * @generated
     * @ordered
     */
    protected static final String VALUE_RANGES_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getValueRangesTmplParam() <em>Value Ranges Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueRangesTmplParam()
     * @generated
     * @ordered
     */
    protected String valueRangesTmplParam = VALUE_RANGES_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isActiveAtStart()
     * @generated
     * @ordered
     */
    protected static final boolean ACTIVE_AT_START_EDEFAULT = true;

    /**
     * The cached value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isActiveAtStart()
     * @generated
     * @ordered
     */
    protected boolean activeAtStart = ACTIVE_AT_START_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractObserverImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.ABSTRACT_OBSERVER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public String getIdentifier() {
        return SmardTraceElementIdentifierHelper.getIdentifier(this);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public String getIdentifier(String projectName) {
        return SmardTraceElementIdentifierHelper.getIdentifier(this, projectName);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLogLevel() {
        return logLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setLogLevel(String newLogLevel) {
        String oldLogLevel = logLevel;
        logLevel = newLogLevel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL, oldLogLevel, logLevel));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<ObserverValueRange> getValueRanges() {
        if (valueRanges == null) {
            valueRanges = new EObjectContainmentEList<ObserverValueRange>(ObserverValueRange.class, this, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES);
        }
        return valueRanges;
    }

    /**
     * @generated NOT
     */
    public boolean addValueRange(ObserverValueRange valueRange) {
        if (valueRanges == null) {
            valueRanges = new EObjectContainmentEList<ObserverValueRange>(ObserverValueRange.class, this, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES);
        }
        if (!valueRanges.contains(valueRange)) {
            valueRanges.add(valueRange);
            return true;
        }
        return false;
    }

    /**
     * @generated NOT
     */
    public boolean removeValueRanges() {
        valueRanges = new EObjectContainmentEList<ObserverValueRange>(ObserverValueRange.class, this, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES);
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getValueRangesTmplParam() {
        return valueRangesTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValueRangesTmplParam(String newValueRangesTmplParam) {
        String oldValueRangesTmplParam = valueRangesTmplParam;
        valueRangesTmplParam = newValueRangesTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM, oldValueRangesTmplParam, valueRangesTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isActiveAtStart() {
        return activeAtStart;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActiveAtStart(boolean newActiveAtStart) {
        boolean oldActiveAtStart = activeAtStart;
        activeAtStart = newActiveAtStart;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START, oldActiveAtStart, activeAtStart));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_NAME)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(name)
                        .error("_Validation_Conditions_AbstractObserver_Name_IsNull")
                        .build())

                .with(Validators.stringTemplate(name)
                        .containsParameterError("_Validation_Conditions_AbstractObserver_Name_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_AbstractObserver_Name_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_LOG_LEVEL)
                .object(this)
                .diagnostic(diagnostics)
                .with(Validators.stringTemplate(logLevel)
                        .containsParameterError("_Validation_Conditions_AbstractObserver_LogLevel_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_AbstractObserver_LogLevel_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public abstract boolean isValid_hasValidValueRanges(EList<String> errorMessages, EList<String> warningMessages);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidValueRangesTmplParam(DiagnosticChain diagnostics, Map<Object, Object> context) {
        // is evaluated in isValid_hasValidValueRanges of children
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
                return ((InternalEList<?>) getValueRanges()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
                return getName();
            case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
                return getDescription();
            case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
                return getLogLevel();
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
                return getValueRanges();
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
                return getValueRangesTmplParam();
            case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
                return isActiveAtStart();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
                setName((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
                setDescription((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
                setLogLevel((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
                getValueRanges().clear();
                getValueRanges().addAll((Collection<? extends ObserverValueRange>) newValue);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
                setValueRangesTmplParam((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
                setActiveAtStart((Boolean) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
                setName(NAME_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
                setLogLevel(LOG_LEVEL_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
                getValueRanges().clear();
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
                setValueRangesTmplParam(VALUE_RANGES_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
                setActiveAtStart(ACTIVE_AT_START_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
                return LOG_LEVEL_EDEFAULT == null ? logLevel != null : !LOG_LEVEL_EDEFAULT.equals(logLevel);
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
                return valueRanges != null && !valueRanges.isEmpty();
            case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
                return VALUE_RANGES_TMPL_PARAM_EDEFAULT == null ? valueRangesTmplParam != null : !VALUE_RANGES_TMPL_PARAM_EDEFAULT.equals(valueRangesTmplParam);
            case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
                return activeAtStart != ACTIVE_AT_START_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(", description: ");
        result.append(description);
        result.append(", logLevel: ");
        result.append(logLevel);
        result.append(", valueRangesTmplParam: ");
        result.append(valueRangesTmplParam);
        result.append(", activeAtStart: ");
        result.append(activeAtStart);
        result.append(')');
        return result.toString();
    }

} // AbstractObserverImpl
