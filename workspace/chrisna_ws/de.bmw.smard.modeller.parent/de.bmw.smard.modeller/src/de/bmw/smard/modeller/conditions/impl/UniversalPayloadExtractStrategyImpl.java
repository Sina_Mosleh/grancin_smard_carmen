package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AUTOSARDataType;
import de.bmw.smard.modeller.conditions.AbstractSignal;
import de.bmw.smard.modeller.conditions.ByteOrderType;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy;
import de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.upe.load.UpeLoaderException;
import de.bmw.upe.load.UpeModelLoader;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Universal Payload Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl#getExtractionRule <em>Extraction Rule</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl#getByteOrder <em>Byte Order</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl#getExtractionRuleTmplParam <em>Extraction Rule Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl#getSignalDataType <em>Signal Data Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl#getSignalDataTypeTmplParam <em>Signal Data Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversalPayloadExtractStrategyImpl extends ExtractStrategyImpl implements UniversalPayloadExtractStrategy {
    /**
     * The default value of the '{@link #getExtractionRule() <em>Extraction Rule</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtractionRule()
     * @generated
     * @ordered
     */
    protected static final String EXTRACTION_RULE_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getExtractionRule() <em>Extraction Rule</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtractionRule()
     * @generated
     * @ordered
     */
    protected String extractionRule = EXTRACTION_RULE_EDEFAULT;

    /**
     * The default value of the '{@link #getByteOrder() <em>Byte Order</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getByteOrder()
     * @generated
     * @ordered
     */
    protected static final ByteOrderType BYTE_ORDER_EDEFAULT = ByteOrderType.INTEL;

    /**
     * The cached value of the '{@link #getByteOrder() <em>Byte Order</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getByteOrder()
     * @generated
     * @ordered
     */
    protected ByteOrderType byteOrder = BYTE_ORDER_EDEFAULT;

    /**
     * The default value of the '{@link #getExtractionRuleTmplParam() <em>Extraction Rule Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtractionRuleTmplParam()
     * @generated
     * @ordered
     */
    protected static final String EXTRACTION_RULE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getExtractionRuleTmplParam() <em>Extraction Rule Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtractionRuleTmplParam()
     * @generated
     * @ordered
     */
    protected String extractionRuleTmplParam = EXTRACTION_RULE_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getSignalDataType() <em>Signal Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalDataType()
     * @generated
     * @ordered
     */
    protected static final AUTOSARDataType SIGNAL_DATA_TYPE_EDEFAULT = AUTOSARDataType.AUNICODE2STRING;

    /**
     * The cached value of the '{@link #getSignalDataType() <em>Signal Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalDataType()
     * @generated
     * @ordered
     */
    protected AUTOSARDataType signalDataType = SIGNAL_DATA_TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getSignalDataTypeTmplParam() <em>Signal Data Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalDataTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSignalDataTypeTmplParam() <em>Signal Data Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalDataTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String signalDataTypeTmplParam = SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UniversalPayloadExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getExtractionRule() {
        return extractionRule;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExtractionRule(String newExtractionRule) {
        String oldExtractionRule = extractionRule;
        extractionRule = newExtractionRule;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE, oldExtractionRule, extractionRule));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ByteOrderType getByteOrder() {
        return byteOrder;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setByteOrder(ByteOrderType newByteOrder) {
        ByteOrderType oldByteOrder = byteOrder;
        byteOrder = newByteOrder == null ? BYTE_ORDER_EDEFAULT : newByteOrder;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER, oldByteOrder, byteOrder));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getExtractionRuleTmplParam() {
        return extractionRuleTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExtractionRuleTmplParam(String newExtractionRuleTmplParam) {
        String oldExtractionRuleTmplParam = extractionRuleTmplParam;
        extractionRuleTmplParam = newExtractionRuleTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM, oldExtractionRuleTmplParam, extractionRuleTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AUTOSARDataType getSignalDataType() {
        return signalDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSignalDataType(AUTOSARDataType newSignalDataType) {
        AUTOSARDataType oldSignalDataType = signalDataType;
        signalDataType = newSignalDataType == null ? SIGNAL_DATA_TYPE_EDEFAULT : newSignalDataType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE, oldSignalDataType, signalDataType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSignalDataTypeTmplParam() {
        return signalDataTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSignalDataTypeTmplParam(String newSignalDataTypeTmplParam) {
        String oldSignalDataTypeTmplParam = signalDataTypeTmplParam;
        signalDataTypeTmplParam = newSignalDataTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM, oldSignalDataTypeTmplParam, signalDataTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidExtractString(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValidHasValidExtractString = true;
        String errorMessageIdentifier = "";
        Set<String> errorMessages = new HashSet<String>();
        String extractionRule = this.extractionRule;
        String extractionRuleTmplParam = this.extractionRuleTmplParam;

        if ((extractionRuleTmplParam == null || extractionRuleTmplParam.length() == 0) ||
            TemplateUtils.getTemplateCategorization(this).isExportable()) {
            if (extractionRule == null || extractionRule.length() == 0) {
                if (this instanceof UniversalPayloadWithLegacyExtractStrategy) {
                    UniversalPayloadWithLegacyExtractStrategy upwe = (UniversalPayloadWithLegacyExtractStrategy) this;
                    isValidHasValidExtractString = upwe.isValid_hasValidStartbit(diagnostics, context) && upwe.isValid_hasValidDataLength(diagnostics, context);
                } else {
                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_Empty";
                    errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));
                    isValidHasValidExtractString = false;
                }
            } else {
                UpeModelLoader modelLoader = new UpeModelLoader();
                try {
                    modelLoader.load(extractionRule);
                    if (modelLoader.isUpeLoaded()) {
                        // exactly one extract is allowed in an Upe
                        if (modelLoader.getNumberOfExtractsInLoadedUpe() == 1) {
                            // for (U)Int maximum 54 bit allowed 
                            if (AUTOSARDataType.getIntDataTypes(true).contains(this.signalDataType)) {
                                // get fixed length contained by extract
                                int fixedLengthInExtract = modelLoader.getFixedLengthOfExtract();
                                if (fixedLengthInExtract > 54) {
                                    // Error
                                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_NotValid";
                                    errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));

                                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_LessThan54";
                                    errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));
                                    isValidHasValidExtractString = false;
                                }
                            }
                        } else {
                            // too many or not enough extracts -> Error
                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_NotValid";
                            errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));

                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_WrongNumberOfExtracts";
                            errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));
                            isValidHasValidExtractString = false;
                        }
                    }
                    // if this is successful, the validation is successful
                } catch (UpeLoaderException e) {
                    // if the UpeModelLoader cannot load the extractString -> error in validation
                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadStrategy_ExtractString_NotValid";
                    errorMessages.add(PluginActivator.getInstance().getString(errorMessageIdentifier));
                    errorMessages.addAll(e.getErrorMessages());
                    isValidHasValidExtractString = false;
                }
            }
        } else {
            isValidHasValidExtractString = true;
        }

        if (!isValidHasValidExtractString) {
            for (String errorMessage : errorMessages) {
                if (diagnostics != null) {
                    diagnostics.add(DiagnosticBuilder.error()
                            .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                            .code(ConditionsValidator.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_EXTRACT_STRING)
                            .message(this.getExtractionRule() + ": " + errorMessage)
                            .data(getAbstractSignal())
                            .build());
                }
            }

        }

        return isValidHasValidExtractString;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSignalDataType(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidSignalDataType = true;
        String errorMessage = "";

        AbstractSignal signal = (AbstractSignal) this.eContainer();
        DataType dataType = signal.get_EvaluationDataType();

        AUTOSARDataType signalDataType = this.signalDataType;
        if (signalDataType.equals(AUTOSARDataType.TEMPLATE_DEFINED)) {
            if (this.signalDataTypeTmplParam == null || this.signalDataTypeTmplParam.length() > 0) {
                errorMessage = TemplateUtils.checkValidPlaceholderName(this.signalDataTypeTmplParam);
                if (errorMessage.length() > 0) {
                    hasValidSignalDataType = false;
                }
            }
        }

        if (dataType.equals(DataType.DOUBLE)) {
            if (!AUTOSARDataType.getNumericDataTypes(true).contains(signalDataType)) {
                hasValidSignalDataType = false;
                errorMessage = "Signal is of type double, cannot match with Signal Data Type String";
            }
        } else if (dataType.equals(DataType.STRING)) {
            if (!AUTOSARDataType.getStringDataTypes(true).contains(signalDataType)) {
                hasValidSignalDataType = false;
                errorMessage = "Signal is of type String, cannot match with Signal Data Type double";
            }
        }

        if (!hasValidSignalDataType) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_EXTRACT_STRING)
                        .data(this)
                        .message("Signal Data Type: " + errorMessage)
                        .build());

            }
        }

        return hasValidSignalDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
                return getExtractionRule();
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
                return getByteOrder();
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
                return getExtractionRuleTmplParam();
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
                return getSignalDataType();
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
                return getSignalDataTypeTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
                setExtractionRule((String) newValue);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
                setByteOrder((ByteOrderType) newValue);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
                setExtractionRuleTmplParam((String) newValue);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
                setSignalDataType((AUTOSARDataType) newValue);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
                setSignalDataTypeTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
                setExtractionRule(EXTRACTION_RULE_EDEFAULT);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
                setByteOrder(BYTE_ORDER_EDEFAULT);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
                setExtractionRuleTmplParam(EXTRACTION_RULE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
                setSignalDataType(SIGNAL_DATA_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
                setSignalDataTypeTmplParam(SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
                return EXTRACTION_RULE_EDEFAULT == null ? extractionRule != null : !EXTRACTION_RULE_EDEFAULT.equals(extractionRule);
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
                return byteOrder != BYTE_ORDER_EDEFAULT;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
                return EXTRACTION_RULE_TMPL_PARAM_EDEFAULT == null ? extractionRuleTmplParam != null : !EXTRACTION_RULE_TMPL_PARAM_EDEFAULT
                        .equals(extractionRuleTmplParam);
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
                return signalDataType != SIGNAL_DATA_TYPE_EDEFAULT;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
                return SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT == null ? signalDataTypeTmplParam != null : !SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT
                        .equals(signalDataTypeTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (extractionRule: ");
        result.append(extractionRule);
        result.append(", byteOrder: ");
        result.append(byteOrder);
        result.append(", extractionRuleTmplParam: ");
        result.append(extractionRuleTmplParam);
        result.append(", signalDataType: ");
        result.append(signalDataType);
        result.append(", signalDataTypeTmplParam: ");
        result.append(signalDataTypeTmplParam);
        result.append(')');
        return result.toString();
    }

} //UniversalPayloadExtractStrategyImpl
