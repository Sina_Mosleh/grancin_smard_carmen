package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.modeller.validation.ValidationMessage;
import org.eclipse.emf.common.util.Diagnostic;

import java.util.List;
import java.util.stream.Collectors;

public class DiagnosticConverter {

    private DiagnosticConverter() { }

    public static List<ValidationMessage> convert(List<Diagnostic> diagnosticList) {
        return diagnosticList.stream()
                .map(DiagnosticConverter::convert)
                .collect(Collectors.toList());
    }

    public static ValidationMessage convert(Diagnostic diagnostic) {

        return new ValidationMessage(SeverityConverter.convert(diagnostic.getSeverity()),
                diagnostic.getSource(),
                diagnostic.getCode(),
                diagnostic.getMessage(),
                diagnostic.getData().stream()
                        .map(d -> (Object)d)
                        .collect(Collectors.toList()));
    }
}
