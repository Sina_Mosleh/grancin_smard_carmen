package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IString Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIStringOperation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IStringOperation extends IOperation, IStringOperand {
} // IStringOperation
