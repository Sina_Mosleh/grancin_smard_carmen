/**
 */
package de.bmw.smard.modeller.statemachineset;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bus Message Referable</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getBusMessageReferable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface BusMessageReferable extends EObject {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    EList<AbstractBusMessage> getAllReferencedBusMessages();

} // BusMessageReferable
