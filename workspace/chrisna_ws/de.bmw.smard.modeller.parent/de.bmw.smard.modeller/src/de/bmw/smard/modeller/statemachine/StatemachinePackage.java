package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachineFactory
 * @model kind="package"
 *        extendedMetaData="qualified='true'"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 * @generated
 */
public interface StatemachinePackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "statemachine";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/statemachine";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "statemachine";

    /**
     * The package content type ID.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eCONTENT_TYPE = "de.bmw.smard.modeller.statemachine";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    StatemachinePackage eINSTANCE = de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl.init();

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.DocumentRootImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getDocumentRoot()
     * @generated
     */
    int DOCUMENT_ROOT = 0;

    /**
     * The feature id for the '<em><b>Mixed</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__MIXED = 0;

    /**
     * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

    /**
     * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

    /**
     * The feature id for the '<em><b>State Machine</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__STATE_MACHINE = 3;

    /**
     * The number of structural features of the '<em>Document Root</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT_FEATURE_COUNT = 4;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl <em>Abstract State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAbstractState()
     * @generated
     */
    int ABSTRACT_STATE = 1;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Out</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__OUT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>In</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__IN = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__ACTIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>State Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE__STATE_TYPE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

    /**
     * The number of structural features of the '<em>Abstract State</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_STATE_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 6;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.TransitionImpl <em>Transition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.TransitionImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getTransition()
     * @generated
     */
    int TRANSITION = 2;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>From</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__FROM = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>To</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__TO = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Conditions</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__CONDITIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__LOG_LEVEL = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Rootcause</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__ROOTCAUSE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 6;

    /**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__ACTIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 7;

    /**
     * The feature id for the '<em><b>Environment</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION__ENVIRONMENT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 8;

    /**
     * The number of structural features of the '<em>Transition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 9;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl <em>State Machine</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.StateMachineImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getStateMachine()
     * @generated
     */
    int STATE_MACHINE = 3;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Initial State</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__INITIAL_STATE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>States</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__STATES = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__TRANSITIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Active At Start</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE__ACTIVE_AT_START = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>State Machine</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl <em>Abstract Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAbstractAction()
     * @generated
     */
    int ABSTRACT_ACTION = 7;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_ACTION__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_ACTION__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Trigger</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_ACTION__TRIGGER = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_ACTION__TRIGGER_TMPL_PARAM = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Abstract Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_ACTION_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl <em>Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.ActionImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAction()
     * @generated
     */
    int ACTION = 4;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__ID = ABSTRACT_ACTION__ID;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Trigger</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__TRIGGER = ABSTRACT_ACTION__TRIGGER;

    /**
     * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

    /**
     * The feature id for the '<em><b>Action Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__ACTION_TYPE = ABSTRACT_ACTION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Action Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__ACTION_EXPRESSION = ABSTRACT_ACTION_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Action Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__ACTION_TYPE_TMPL_PARAM = ABSTRACT_ACTION_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Environment</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION__ENVIRONMENT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ACTION_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 4;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.InitialStateImpl <em>Initial State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.InitialStateImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getInitialState()
     * @generated
     */
    int INITIAL_STATE = 5;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__ID = ABSTRACT_STATE__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__NAME = ABSTRACT_STATE__NAME;

    /**
     * The feature id for the '<em><b>Out</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__OUT = ABSTRACT_STATE__OUT;

    /**
     * The feature id for the '<em><b>In</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__IN = ABSTRACT_STATE__IN;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__DESCRIPTION = ABSTRACT_STATE__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__ACTIONS = ABSTRACT_STATE__ACTIONS;

    /**
     * The feature id for the '<em><b>State Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE__STATE_TYPE = ABSTRACT_STATE__STATE_TYPE;

    /**
     * The number of structural features of the '<em>Initial State</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INITIAL_STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 0;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.StateImpl <em>State</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.StateImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getState()
     * @generated
     */
    int STATE = 6;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__ID = ABSTRACT_STATE__ID;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__NAME = ABSTRACT_STATE__NAME;

    /**
     * The feature id for the '<em><b>Out</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__OUT = ABSTRACT_STATE__OUT;

    /**
     * The feature id for the '<em><b>In</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__IN = ABSTRACT_STATE__IN;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__DESCRIPTION = ABSTRACT_STATE__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Actions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__ACTIONS = ABSTRACT_STATE__ACTIONS;

    /**
     * The feature id for the '<em><b>State Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__STATE_TYPE = ABSTRACT_STATE__STATE_TYPE;

    /**
     * The feature id for the '<em><b>State Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE__STATE_TYPE_TMPL_PARAM = ABSTRACT_STATE_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>State</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 1;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl <em>Compute Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getComputeVariable()
     * @generated
     */
    int COMPUTE_VARIABLE = 8;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__ID = ABSTRACT_ACTION__ID;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Trigger</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__TRIGGER = ABSTRACT_ACTION__TRIGGER;

    /**
     * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

    /**
     * The feature id for the '<em><b>Action Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__ACTION_EXPRESSION = ABSTRACT_ACTION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Target</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__TARGET = ABSTRACT_ACTION_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Operands</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE__OPERANDS = ABSTRACT_ACTION_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Compute Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTE_VARIABLE_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl <em>Show Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getShowVariable()
     * @generated
     */
    int SHOW_VARIABLE = 9;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__ID = ABSTRACT_ACTION__ID;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Trigger</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__TRIGGER = ABSTRACT_ACTION__TRIGGER;

    /**
     * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

    /**
     * The feature id for the '<em><b>Format</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__FORMAT = ABSTRACT_ACTION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Prefix</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__PREFIX = ABSTRACT_ACTION_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Postfix</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__POSTFIX = ABSTRACT_ACTION_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Root Cause</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__ROOT_CAUSE = ABSTRACT_ACTION_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__LOG_LEVEL = ABSTRACT_ACTION_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Variable</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__VARIABLE = ABSTRACT_ACTION_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Environment</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE__ENVIRONMENT = ABSTRACT_ACTION_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>Show Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SHOW_VARIABLE_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 7;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.SmardTraceElement <em>Smard Trace Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.SmardTraceElement
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getSmardTraceElement()
     * @generated
     */
    int SMARD_TRACE_ELEMENT = 10;

    /**
     * The number of structural features of the '<em>Smard Trace Element</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SMARD_TRACE_ELEMENT_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.impl.ControlActionImpl <em>Control Action</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.impl.ControlActionImpl
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getControlAction()
     * @generated
     */
    int CONTROL_ACTION = 11;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__ID = ABSTRACT_ACTION__ID;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

    /**
     * The feature id for the '<em><b>Trigger</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__TRIGGER = ABSTRACT_ACTION__TRIGGER;

    /**
     * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

    /**
     * The feature id for the '<em><b>Control Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__CONTROL_TYPE = ABSTRACT_ACTION_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>State Machines</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__STATE_MACHINES = ABSTRACT_ACTION_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Observers</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION__OBSERVERS = ABSTRACT_ACTION_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Control Action</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTROL_ACTION_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.Trigger <em>Trigger</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.Trigger
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getTrigger()
     * @generated
     */
    int TRIGGER = 12;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.StateType <em>State Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.StateType
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getStateType()
     * @generated
     */
    int STATE_TYPE = 13;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getActionTypeNotEmpty()
     * @generated
     */
    int ACTION_TYPE_NOT_EMPTY = 14;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachine.ControlType <em>Control Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachine.ControlType
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getControlType()
     * @generated
     */
    int CONTROL_TYPE = 15;

    /**
     * The meta object id for the '<em>Int Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getIntOrTemplatePlaceholder()
     * @generated
     */
    int INT_OR_TEMPLATE_PLACEHOLDER = 16;


    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.DocumentRoot <em>Document Root</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Document Root</em>'.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot
     * @generated
     */
    EClass getDocumentRoot();

    /**
     * Returns the meta object for the attribute list '{@link de.bmw.smard.modeller.statemachine.DocumentRoot#getMixed <em>Mixed</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Mixed</em>'.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot#getMixed()
     * @see #getDocumentRoot()
     * @generated
     */
    EAttribute getDocumentRoot_Mixed();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.statemachine.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot#getXMLNSPrefixMap()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XMLNSPrefixMap();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.statemachine.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XSI Schema Location</em>'.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot#getXSISchemaLocation()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XSISchemaLocation();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachine.DocumentRoot#getStateMachine <em>State Machine</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>State Machine</em>'.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot#getStateMachine()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_StateMachine();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.AbstractState <em>Abstract State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract State</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState
     * @generated
     */
    EClass getAbstractState();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractState#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getName()
     * @see #getAbstractState()
     * @generated
     */
    EAttribute getAbstractState_Name();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachine.AbstractState#getOut <em>Out</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Out</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getOut()
     * @see #getAbstractState()
     * @generated
     */
    EReference getAbstractState_Out();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachine.AbstractState#getIn <em>In</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>In</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getIn()
     * @see #getAbstractState()
     * @generated
     */
    EReference getAbstractState_In();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractState#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getDescription()
     * @see #getAbstractState()
     * @generated
     */
    EAttribute getAbstractState_Description();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachine.AbstractState#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Actions</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getActions()
     * @see #getAbstractState()
     * @generated
     */
    EReference getAbstractState_Actions();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractState#getStateType <em>State Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>State Type</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractState#getStateType()
     * @see #getAbstractState()
     * @generated
     */
    EAttribute getAbstractState_StateType();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.Transition <em>Transition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Transition</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition
     * @generated
     */
    EClass getTransition();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Transition#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getName()
     * @see #getTransition()
     * @generated
     */
    EAttribute getTransition_Name();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.Transition#getFrom <em>From</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>From</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getFrom()
     * @see #getTransition()
     * @generated
     */
    EReference getTransition_From();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.Transition#getTo <em>To</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>To</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getTo()
     * @see #getTransition()
     * @generated
     */
    EReference getTransition_To();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachine.Transition#getConditions <em>Conditions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Conditions</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getConditions()
     * @see #getTransition()
     * @generated
     */
    EReference getTransition_Conditions();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Transition#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getDescription()
     * @see #getTransition()
     * @generated
     */
    EAttribute getTransition_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Transition#getLogLevel <em>Log Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Log Level</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getLogLevel()
     * @see #getTransition()
     * @generated
     */
    EAttribute getTransition_LogLevel();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Transition#getRootcause <em>Rootcause</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Rootcause</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getRootcause()
     * @see #getTransition()
     * @generated
     */
    EAttribute getTransition_Rootcause();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachine.Transition#getActions <em>Actions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Actions</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getActions()
     * @see #getTransition()
     * @generated
     */
    EReference getTransition_Actions();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.Transition#getEnvironment <em>Environment</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Environment</em>'.
     * @see de.bmw.smard.modeller.statemachine.Transition#getEnvironment()
     * @see #getTransition()
     * @generated
     */
    EReference getTransition_Environment();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.StateMachine <em>State Machine</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>State Machine</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine
     * @generated
     */
    EClass getStateMachine();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.StateMachine#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine#getName()
     * @see #getStateMachine()
     * @generated
     */
    EAttribute getStateMachine_Name();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachine.StateMachine#getInitialState <em>Initial State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Initial State</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine#getInitialState()
     * @see #getStateMachine()
     * @generated
     */
    EReference getStateMachine_InitialState();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachine.StateMachine#getStates <em>States</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>States</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine#getStates()
     * @see #getStateMachine()
     * @generated
     */
    EReference getStateMachine_States();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachine.StateMachine#getTransitions <em>Transitions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Transitions</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine#getTransitions()
     * @see #getStateMachine()
     * @generated
     */
    EReference getStateMachine_Transitions();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.StateMachine#isActiveAtStart <em>Active At Start</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Active At Start</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateMachine#isActiveAtStart()
     * @see #getStateMachine()
     * @generated
     */
    EAttribute getStateMachine_ActiveAtStart();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.Action <em>Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Action</em>'.
     * @see de.bmw.smard.modeller.statemachine.Action
     * @generated
     */
    EClass getAction();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Action#getActionType <em>Action Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Action Type</em>'.
     * @see de.bmw.smard.modeller.statemachine.Action#getActionType()
     * @see #getAction()
     * @generated
     */
    EAttribute getAction_ActionType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Action#getActionExpression <em>Action Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Action Expression</em>'.
     * @see de.bmw.smard.modeller.statemachine.Action#getActionExpression()
     * @see #getAction()
     * @generated
     */
    EAttribute getAction_ActionExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.Action#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Action Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.statemachine.Action#getActionTypeTmplParam()
     * @see #getAction()
     * @generated
     */
    EAttribute getAction_ActionTypeTmplParam();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.Action#getEnvironment <em>Environment</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Environment</em>'.
     * @see de.bmw.smard.modeller.statemachine.Action#getEnvironment()
     * @see #getAction()
     * @generated
     */
    EReference getAction_Environment();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.InitialState <em>Initial State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Initial State</em>'.
     * @see de.bmw.smard.modeller.statemachine.InitialState
     * @generated
     */
    EClass getInitialState();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.State <em>State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>State</em>'.
     * @see de.bmw.smard.modeller.statemachine.State
     * @generated
     */
    EClass getState();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.State#getStateTypeTmplParam <em>State Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>State Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.statemachine.State#getStateTypeTmplParam()
     * @see #getState()
     * @generated
     */
    EAttribute getState_StateTypeTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.AbstractAction <em>Abstract Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Action</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractAction
     * @generated
     */
    EClass getAbstractAction();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractAction#getDescription()
     * @see #getAbstractAction()
     * @generated
     */
    EAttribute getAbstractAction_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTrigger <em>Trigger</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Trigger</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractAction#getTrigger()
     * @see #getAbstractAction()
     * @generated
     */
    EAttribute getAbstractAction_Trigger();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Trigger Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.statemachine.AbstractAction#getTriggerTmplParam()
     * @see #getAbstractAction()
     * @generated
     */
    EAttribute getAbstractAction_TriggerTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.ComputeVariable <em>Compute Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Compute Variable</em>'.
     * @see de.bmw.smard.modeller.statemachine.ComputeVariable
     * @generated
     */
    EClass getComputeVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Action Expression</em>'.
     * @see de.bmw.smard.modeller.statemachine.ComputeVariable#getActionExpression()
     * @see #getComputeVariable()
     * @generated
     */
    EAttribute getComputeVariable_ActionExpression();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getTarget <em>Target</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Target</em>'.
     * @see de.bmw.smard.modeller.statemachine.ComputeVariable#getTarget()
     * @see #getComputeVariable()
     * @generated
     */
    EReference getComputeVariable_Target();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getOperands <em>Operands</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Operands</em>'.
     * @see de.bmw.smard.modeller.statemachine.ComputeVariable#getOperands()
     * @see #getComputeVariable()
     * @generated
     */
    EReference getComputeVariable_Operands();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.ShowVariable <em>Show Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Show Variable</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable
     * @generated
     */
    EClass getShowVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getFormat <em>Format</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Format</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getFormat()
     * @see #getShowVariable()
     * @generated
     */
    EAttribute getShowVariable_Format();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getPrefix <em>Prefix</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Prefix</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getPrefix()
     * @see #getShowVariable()
     * @generated
     */
    EAttribute getShowVariable_Prefix();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getPostfix <em>Postfix</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Postfix</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getPostfix()
     * @see #getShowVariable()
     * @generated
     */
    EAttribute getShowVariable_Postfix();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getRootCause <em>Root Cause</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Root Cause</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getRootCause()
     * @see #getShowVariable()
     * @generated
     */
    EAttribute getShowVariable_RootCause();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getLogLevel <em>Log Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Log Level</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getLogLevel()
     * @see #getShowVariable()
     * @generated
     */
    EAttribute getShowVariable_LogLevel();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getVariable <em>Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Variable</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getVariable()
     * @see #getShowVariable()
     * @generated
     */
    EReference getShowVariable_Variable();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.statemachine.ShowVariable#getEnvironment <em>Environment</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Environment</em>'.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable#getEnvironment()
     * @see #getShowVariable()
     * @generated
     */
    EReference getShowVariable_Environment();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.SmardTraceElement <em>Smard Trace Element</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Smard Trace Element</em>'.
     * @see de.bmw.smard.modeller.statemachine.SmardTraceElement
     * @generated
     */
    EClass getSmardTraceElement();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachine.ControlAction <em>Control Action</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Control Action</em>'.
     * @see de.bmw.smard.modeller.statemachine.ControlAction
     * @generated
     */
    EClass getControlAction();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachine.ControlAction#getControlType <em>Control Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Control Type</em>'.
     * @see de.bmw.smard.modeller.statemachine.ControlAction#getControlType()
     * @see #getControlAction()
     * @generated
     */
    EAttribute getControlAction_ControlType();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachine.ControlAction#getStateMachines <em>State Machines</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>State Machines</em>'.
     * @see de.bmw.smard.modeller.statemachine.ControlAction#getStateMachines()
     * @see #getControlAction()
     * @generated
     */
    EReference getControlAction_StateMachines();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachine.ControlAction#getObservers <em>Observers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Observers</em>'.
     * @see de.bmw.smard.modeller.statemachine.ControlAction#getObservers()
     * @see #getControlAction()
     * @generated
     */
    EReference getControlAction_Observers();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.statemachine.Trigger <em>Trigger</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Trigger</em>'.
     * @see de.bmw.smard.modeller.statemachine.Trigger
     * @generated
     */
    EEnum getTrigger();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.statemachine.StateType <em>State Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>State Type</em>'.
     * @see de.bmw.smard.modeller.statemachine.StateType
     * @generated
     */
    EEnum getStateType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Action Type Not Empty</em>'.
     * @see de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
     * @generated
     */
    EEnum getActionTypeNotEmpty();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.statemachine.ControlType <em>Control Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Control Type</em>'.
     * @see de.bmw.smard.modeller.statemachine.ControlType
     * @generated
     */
    EEnum getControlType();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Int Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Int Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getIntOrTemplatePlaceholder();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    StatemachineFactory getStatemachineFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.DocumentRootImpl <em>Document Root</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.DocumentRootImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getDocumentRoot()
         * @generated
         */
        EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

        /**
         * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

        /**
         * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

        /**
         * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

        /**
         * The meta object literal for the '<em><b>State Machine</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__STATE_MACHINE = eINSTANCE.getDocumentRoot_StateMachine();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl <em>Abstract State</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.AbstractStateImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAbstractState()
         * @generated
         */
        EClass ABSTRACT_STATE = eINSTANCE.getAbstractState();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_STATE__NAME = eINSTANCE.getAbstractState_Name();

        /**
         * The meta object literal for the '<em><b>Out</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_STATE__OUT = eINSTANCE.getAbstractState_Out();

        /**
         * The meta object literal for the '<em><b>In</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_STATE__IN = eINSTANCE.getAbstractState_In();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_STATE__DESCRIPTION = eINSTANCE.getAbstractState_Description();

        /**
         * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_STATE__ACTIONS = eINSTANCE.getAbstractState_Actions();

        /**
         * The meta object literal for the '<em><b>State Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_STATE__STATE_TYPE = eINSTANCE.getAbstractState_StateType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.TransitionImpl <em>Transition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.TransitionImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getTransition()
         * @generated
         */
        EClass TRANSITION = eINSTANCE.getTransition();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION__NAME = eINSTANCE.getTransition_Name();

        /**
         * The meta object literal for the '<em><b>From</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION__FROM = eINSTANCE.getTransition_From();

        /**
         * The meta object literal for the '<em><b>To</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION__TO = eINSTANCE.getTransition_To();

        /**
         * The meta object literal for the '<em><b>Conditions</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION__CONDITIONS = eINSTANCE.getTransition_Conditions();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION__DESCRIPTION = eINSTANCE.getTransition_Description();

        /**
         * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION__LOG_LEVEL = eINSTANCE.getTransition_LogLevel();

        /**
         * The meta object literal for the '<em><b>Rootcause</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION__ROOTCAUSE = eINSTANCE.getTransition_Rootcause();

        /**
         * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION__ACTIONS = eINSTANCE.getTransition_Actions();

        /**
         * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION__ENVIRONMENT = eINSTANCE.getTransition_Environment();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.StateMachineImpl <em>State Machine</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.StateMachineImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getStateMachine()
         * @generated
         */
        EClass STATE_MACHINE = eINSTANCE.getStateMachine();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_MACHINE__NAME = eINSTANCE.getStateMachine_Name();

        /**
         * The meta object literal for the '<em><b>Initial State</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE__INITIAL_STATE = eINSTANCE.getStateMachine_InitialState();

        /**
         * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE__STATES = eINSTANCE.getStateMachine_States();

        /**
         * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE__TRANSITIONS = eINSTANCE.getStateMachine_Transitions();

        /**
         * The meta object literal for the '<em><b>Active At Start</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_MACHINE__ACTIVE_AT_START = eINSTANCE.getStateMachine_ActiveAtStart();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl <em>Action</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.ActionImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAction()
         * @generated
         */
        EClass ACTION = eINSTANCE.getAction();

        /**
         * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ACTION__ACTION_TYPE = eINSTANCE.getAction_ActionType();

        /**
         * The meta object literal for the '<em><b>Action Expression</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ACTION__ACTION_EXPRESSION = eINSTANCE.getAction_ActionExpression();

        /**
         * The meta object literal for the '<em><b>Action Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ACTION__ACTION_TYPE_TMPL_PARAM = eINSTANCE.getAction_ActionTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ACTION__ENVIRONMENT = eINSTANCE.getAction_Environment();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.InitialStateImpl <em>Initial State</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.InitialStateImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getInitialState()
         * @generated
         */
        EClass INITIAL_STATE = eINSTANCE.getInitialState();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.StateImpl <em>State</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.StateImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getState()
         * @generated
         */
        EClass STATE = eINSTANCE.getState();

        /**
         * The meta object literal for the '<em><b>State Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE__STATE_TYPE_TMPL_PARAM = eINSTANCE.getState_StateTypeTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl <em>Abstract Action</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getAbstractAction()
         * @generated
         */
        EClass ABSTRACT_ACTION = eINSTANCE.getAbstractAction();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_ACTION__DESCRIPTION = eINSTANCE.getAbstractAction_Description();

        /**
         * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_ACTION__TRIGGER = eINSTANCE.getAbstractAction_Trigger();

        /**
         * The meta object literal for the '<em><b>Trigger Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_ACTION__TRIGGER_TMPL_PARAM = eINSTANCE.getAbstractAction_TriggerTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl <em>Compute Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getComputeVariable()
         * @generated
         */
        EClass COMPUTE_VARIABLE = eINSTANCE.getComputeVariable();

        /**
         * The meta object literal for the '<em><b>Action Expression</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COMPUTE_VARIABLE__ACTION_EXPRESSION = eINSTANCE.getComputeVariable_ActionExpression();

        /**
         * The meta object literal for the '<em><b>Target</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMPUTE_VARIABLE__TARGET = eINSTANCE.getComputeVariable_Target();

        /**
         * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMPUTE_VARIABLE__OPERANDS = eINSTANCE.getComputeVariable_Operands();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl <em>Show Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getShowVariable()
         * @generated
         */
        EClass SHOW_VARIABLE = eINSTANCE.getShowVariable();

        /**
         * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SHOW_VARIABLE__FORMAT = eINSTANCE.getShowVariable_Format();

        /**
         * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SHOW_VARIABLE__PREFIX = eINSTANCE.getShowVariable_Prefix();

        /**
         * The meta object literal for the '<em><b>Postfix</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SHOW_VARIABLE__POSTFIX = eINSTANCE.getShowVariable_Postfix();

        /**
         * The meta object literal for the '<em><b>Root Cause</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SHOW_VARIABLE__ROOT_CAUSE = eINSTANCE.getShowVariable_RootCause();

        /**
         * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SHOW_VARIABLE__LOG_LEVEL = eINSTANCE.getShowVariable_LogLevel();

        /**
         * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SHOW_VARIABLE__VARIABLE = eINSTANCE.getShowVariable_Variable();

        /**
         * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SHOW_VARIABLE__ENVIRONMENT = eINSTANCE.getShowVariable_Environment();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.SmardTraceElement <em>Smard Trace Element</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.SmardTraceElement
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getSmardTraceElement()
         * @generated
         */
        EClass SMARD_TRACE_ELEMENT = eINSTANCE.getSmardTraceElement();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.impl.ControlActionImpl <em>Control Action</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.impl.ControlActionImpl
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getControlAction()
         * @generated
         */
        EClass CONTROL_ACTION = eINSTANCE.getControlAction();

        /**
         * The meta object literal for the '<em><b>Control Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTROL_ACTION__CONTROL_TYPE = eINSTANCE.getControlAction_ControlType();

        /**
         * The meta object literal for the '<em><b>State Machines</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONTROL_ACTION__STATE_MACHINES = eINSTANCE.getControlAction_StateMachines();

        /**
         * The meta object literal for the '<em><b>Observers</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONTROL_ACTION__OBSERVERS = eINSTANCE.getControlAction_Observers();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.Trigger <em>Trigger</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.Trigger
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getTrigger()
         * @generated
         */
        EEnum TRIGGER = eINSTANCE.getTrigger();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.StateType <em>State Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.StateType
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getStateType()
         * @generated
         */
        EEnum STATE_TYPE = eINSTANCE.getStateType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getActionTypeNotEmpty()
         * @generated
         */
        EEnum ACTION_TYPE_NOT_EMPTY = eINSTANCE.getActionTypeNotEmpty();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachine.ControlType <em>Control Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachine.ControlType
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getControlType()
         * @generated
         */
        EEnum CONTROL_TYPE = eINSTANCE.getControlType();

        /**
         * The meta object literal for the '<em>Int Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.statemachine.impl.StatemachinePackageImpl#getIntOrTemplatePlaceholder()
         * @generated
         */
        EDataType INT_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getIntOrTemplatePlaceholder();

    }

} //StatemachinePackage
