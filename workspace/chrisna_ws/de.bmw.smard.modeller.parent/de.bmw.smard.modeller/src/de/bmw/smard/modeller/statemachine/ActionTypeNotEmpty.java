package de.bmw.smard.modeller.statemachine;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Action Type Not Empty</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getActionTypeNotEmpty()
 * @model
 * @generated
 */
public enum ActionTypeNotEmpty implements Enumerator {
    /**
     * The '<em><b>TEMPLATEDEFINED</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATEDEFINED(-1, "TEMPLATEDEFINED", "TEMPLATEDEFINED"),
    /**
     * The '<em><b>COMPUTE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #COMPUTE_VALUE
     * @generated
     * @ordered
     */
    COMPUTE(0, "COMPUTE", "COMPUTE"),

    /**
     * The '<em><b>SHOWVARIABLE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #SHOWVARIABLE_VALUE
     * @generated
     * @ordered
     */
    SHOWVARIABLE(1, "SHOWVARIABLE", "SHOWVARIABLE"),

    /**
     * The '<em><b>FLEXRAY</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #FLEXRAY_VALUE
     * @generated
     * @ordered
     */
    FLEXRAY(2, "FLEXRAY", "FLEXRAY"),

    /**
     * The '<em><b>CAN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #CAN_VALUE
     * @generated
     * @ordered
     */
    CAN(3, "CAN", "CAN"),

    /**
     * The '<em><b>LIN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #LIN_VALUE
     * @generated
     * @ordered
     */
    LIN(4, "LIN", "LIN"),

    /**
     * The '<em><b>PLUGIN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #PLUGIN_VALUE
     * @generated
     * @ordered
     */
    PLUGIN(6, "PLUGIN", "PLUGIN");

    /**
     * The '<em><b>TEMPLATEDEFINED</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED
     * @model
     * @generated
     * @ordered
     */
    public static final int TEMPLATEDEFINED_VALUE = -1;

    /**
     * The '<em><b>COMPUTE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>COMPUTE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #COMPUTE
     * @model
     * @generated
     * @ordered
     */
    public static final int COMPUTE_VALUE = 0;

    /**
     * The '<em><b>SHOWVARIABLE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>SHOWVARIABLE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #SHOWVARIABLE
     * @model
     * @generated
     * @ordered
     */
    public static final int SHOWVARIABLE_VALUE = 1;

    /**
     * The '<em><b>FLEXRAY</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>FLEXRAY</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #FLEXRAY
     * @model
     * @generated
     * @ordered
     */
    public static final int FLEXRAY_VALUE = 2;

    /**
     * The '<em><b>CAN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>CAN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #CAN
     * @model
     * @generated
     * @ordered
     */
    public static final int CAN_VALUE = 3;

    /**
     * The '<em><b>LIN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>LIN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #LIN
     * @model
     * @generated
     * @ordered
     */
    public static final int LIN_VALUE = 4;

    /**
     * The '<em><b>PLUGIN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>PLUGIN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #PLUGIN
     * @model
     * @generated
     * @ordered
     */
    public static final int PLUGIN_VALUE = 6;

    /**
     * An array of all the '<em><b>Action Type Not Empty</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final ActionTypeNotEmpty[] VALUES_ARRAY =
            new ActionTypeNotEmpty[] {
                    TEMPLATEDEFINED,
                    COMPUTE,
                    SHOWVARIABLE,
                    FLEXRAY,
                    CAN,
                    LIN,
                    PLUGIN,
            };

    /**
     * A public read-only list of all the '<em><b>Action Type Not Empty</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<ActionTypeNotEmpty> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Action Type Not Empty</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * �nderungen am generierten Code um die R�ckw�rtskompatibilit�t zu gew�hrleisten
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated NOT
     */
    public static ActionTypeNotEmpty get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ActionTypeNotEmpty result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
            // eingef�gt, um die R�ckw�rtskompatibilit�t zu gew�hrleisten
            else if (result.toString().toUpperCase().equals(literal.toUpperCase())) {
                return result;
            }
        }
        return ActionTypeNotEmpty.CAN; // wegen der R�ckw�rtskompatibilit�t hier nicht null zur�ckgeben!
    }

    /**
     * Returns the '<em><b>Action Type Not Empty</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ActionTypeNotEmpty getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ActionTypeNotEmpty result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Action Type Not Empty</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ActionTypeNotEmpty get(int value) {
        switch (value) {
            case TEMPLATEDEFINED_VALUE:
                return TEMPLATEDEFINED;
            case COMPUTE_VALUE:
                return COMPUTE;
            case SHOWVARIABLE_VALUE:
                return SHOWVARIABLE;
            case FLEXRAY_VALUE:
                return FLEXRAY;
            case CAN_VALUE:
                return CAN;
            case LIN_VALUE:
                return LIN;
            case PLUGIN_VALUE:
                return PLUGIN;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private ActionTypeNotEmpty(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //ActionTypeNotEmpty
