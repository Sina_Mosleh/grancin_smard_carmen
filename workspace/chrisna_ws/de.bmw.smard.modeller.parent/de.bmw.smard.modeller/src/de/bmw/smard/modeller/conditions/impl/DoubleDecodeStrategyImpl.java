package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DoubleDecodeStrategy;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Double Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl#getFactor <em>Factor</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DoubleDecodeStrategyImpl extends DecodeStrategyImpl implements DoubleDecodeStrategy {
    /**
     * The default value of the '{@link #getFactor() <em>Factor</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFactor()
     * @generated
     * @ordered
     */
    protected static final String FACTOR_EDEFAULT = "1.0";

    /**
     * The cached value of the '{@link #getFactor() <em>Factor</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFactor()
     * @generated
     * @ordered
     */
    protected String factor = FACTOR_EDEFAULT;

    /**
     * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOffset()
     * @generated
     * @ordered
     */
    protected static final String OFFSET_EDEFAULT = "0.0";

    /**
     * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOffset()
     * @generated
     * @ordered
     */
    protected String offset = OFFSET_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected DoubleDecodeStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.DOUBLE_DECODE_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFactor() {
        return factor;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFactor(String newFactor) {
        String oldFactor = factor;
        factor = newFactor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DOUBLE_DECODE_STRATEGY__FACTOR, oldFactor, factor));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOffset() {
        return offset;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOffset(String newOffset) {
        String oldOffset = offset;
        offset = newOffset;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DOUBLE_DECODE_STRATEGY__OFFSET, oldOffset, offset));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidFactor(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.DOUBLE_DECODE_STRATEGY__IS_VALID_HAS_VALID_FACTOR)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.doubleTemplate(factor)
                        .containsParameterError("_Validation_Conditions_DoubleDecodeStrategy_Factor_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_DoubleDecodeStrategy_Factor_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_DoubleDecodeStrategy_Factor_IllegalFactor")
                        .build())

                .with(Validators.notNull(factor)
                        .error("_Validation_Conditions_DoubleDecodeStrategy_Factor_IsNullOrZero")
                        .build())

                .with(Validators.when(isRepresentationOfDoubleZero(factor))
                        .error("_Validation_Conditions_DoubleDecodeStrategy_Factor_IsNullOrZero")
                        .build())

                .validate();
    }

    /**
     * @param string2Check
     * @return
     * @generated NOT
     */
    private boolean isRepresentationOfDoubleZero(String string2Check) {
        double value = 1;
        try {
            value = Double.parseDouble(string2Check);
        } catch (NumberFormatException e) {
            return false;
        }
        return Double.toString(value).equals("0.0");
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOffset(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.DOUBLE_DECODE_STRATEGY__IS_VALID_HAS_VALID_OFFSET)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.doubleTemplate(factor)
                        .containsParameterError("_Validation_Conditions_DoubleDecodeStrategy_Offset_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_DoubleDecodeStrategy_Offset_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_DoubleDecodeStrategy_Offset_IllegalOffset")
                        .build())

                .with(Validators.notNull(offset)
                        .error("_Validation_Conditions_DoubleDecodeStrategy_Offset_IsNull")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__FACTOR:
                return getFactor();
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__OFFSET:
                return getOffset();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__FACTOR:
                setFactor((String) newValue);
                return;
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__OFFSET:
                setOffset((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__FACTOR:
                setFactor(FACTOR_EDEFAULT);
                return;
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__OFFSET:
                setOffset(OFFSET_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__FACTOR:
                return FACTOR_EDEFAULT == null ? factor != null : !FACTOR_EDEFAULT.equals(factor);
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY__OFFSET:
                return OFFSET_EDEFAULT == null ? offset != null : !OFFSET_EDEFAULT.equals(offset);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (factor: ");
        result.append(factor);
        result.append(", offset: ");
        result.append(offset);
        result.append(')');
        return result.toString();
    }

} //DoubleDecodeStrategyImpl
