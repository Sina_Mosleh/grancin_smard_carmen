package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

import java.util.ArrayList;
import java.util.List;

public class BaseValidator {

    private final String source;
    private final int code;
    private final EObject object;
    private final DiagnosticChain diagnostics;

    public EObject getObject() {
        return object;
    }

    private BaseValidator(String source, int code, EObject object, DiagnosticChain diagnostics) {
        this.source = source;
        this.code = code;
        this.object = object;
        this.diagnostics = diagnostics;
    }


    public void attach(Severity severity, String messageId, String substitions) {

        if (diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.builder(severity)
                    .code(code)
                    .source(source)
                    .messageId(PluginActivator.INSTANCE, messageId, substitions)
                    .data(object)
                    .build());
        }
    }

    public boolean isTemplateAllowed() {
        return TemplateUtils.getTemplateCategorization(object).allowsTemplateParameters();
    }

    public boolean isExportable() {
        return TemplateUtils.getTemplateCategorization(object).isExportable();
    }

    public static SourceStep builder() {
        return new BaseValidator.Builder();
    }

    public interface SourceStep {
        CodeStep source(String source);
    }

    public interface CodeStep {
        ObjectStep code(int code);
    }

    public interface ObjectStep {
        DiagnosticChainStep object(EObject object);
    }

    public interface DiagnosticChainStep {
        WithStep diagnostic(DiagnosticChain diagnostics);
    }

    public interface WithStep {
        WithStep with(Validator validator);

        boolean validate();
    }

    public static class Builder implements
            SourceStep,
            CodeStep,
            ObjectStep,
            DiagnosticChainStep,
            WithStep {

        private String source;
        private int code;
        private EObject object;
        private DiagnosticChain diagnostics;
        private List<Validator> validators = new ArrayList<>();

        protected EObject getObject() {
            return object;
        }

        protected int getCode() {
            return code;
        }

        protected DiagnosticChain getDiagnostics() {
            return diagnostics;
        }

        protected String getSource() {
            return source;
        }

        @Override
        public CodeStep source(String source) {
            this.source = source;
            return this;
        }

        @Override
        public ObjectStep code(int code) {
            this.code = code;
            return this;
        }

        @Override
        public DiagnosticChainStep object(EObject object) {
            this.object = object;
            return this;
        }

        @Override
        public WithStep diagnostic(DiagnosticChain diagnostics) {
            this.diagnostics = diagnostics;
            return this;
        }

        @Override
        public WithStep with(Validator validator) {
            validators.add(validator);
            return this;
        }

        @Override
        public boolean validate() {

            BaseValidator baseValidator = new BaseValidator(source, code, object, diagnostics);
            for (Validator validator : validators) {
                if (!validator.validate(baseValidator)) {
                    return false;
                }
            }
            return true;
        }
    }







}
