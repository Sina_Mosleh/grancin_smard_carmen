package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DecodeStrategy;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DecodeStrategyImpl extends EObjectImpl implements DecodeStrategy {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected DecodeStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.DECODE_STRATEGY;
    }

} //DecodeStrategyImpl
