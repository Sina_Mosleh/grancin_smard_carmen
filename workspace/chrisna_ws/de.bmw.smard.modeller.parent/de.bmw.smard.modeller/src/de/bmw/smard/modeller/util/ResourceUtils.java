package de.bmw.smard.modeller.util;
/**
 * 
 */


import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.util.TemplateUtils.TemplateCategorization;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Allgemeine Funktionen
 *
 */
public class ResourceUtils {
	public static final String CONDITIONS_FILEEXTENSION = "conditions";
	public static final String STATEMACHINESET_FILEEXTENSION = "statemachine_set";
	public static final String STATEMACHINE_FILEEXTENSION = "statemachine";
	public static final String STATEMACHINE_DIAGRAM_FILE_EXTENSION = "statemachine_diagram";
	
	/**
	 * Durchl�uft Ordnerhierarchie und gibt eine Liste aller Dateien zur�ck.
	 * @param members
	 * @return Liste aller Dateien ohne Verzeichnishierarchie
	 */
	public static List<IFile> getFlatFileList(IResource[] members) {
		List<IFile> files =  new ArrayList<IFile>();
		for (IResource res : members){
			if (res instanceof IFolder) {
				try {
					files.addAll(getFlatFileList(((IContainer) res).members()));
				} catch (CoreException e) {
					e.printStackTrace();
				}
			} else if (res instanceof IFile) {
				files.add((IFile)res);
			}
		}
		return files;
	}
	
	/**
	 * loads and returns the resources with the given file extension within the same project as the resource of the given {@link EObject}.
	 * 
	 * @param eObject the object
	 * @param fileExtension the desired file extension
	 * @return the matching resources, loaded in the context of the resourceSet of the eObject, never null but may be empty
	 */
	public static List<Resource> collectResources(final EObject eObject, String fileExtension, boolean considerObjectsTempleCat)
    {
		final ResourceSet resourceSet = eObject.eResource().getResourceSet();
		IResource eclipseResource = EMFUtils.getEclipseResource(eObject.eResource());
		final IProject eclipseProject = eclipseResource.getProject();
		List<Resource> resources = new ArrayList<Resource>();
		
		if ( considerObjectsTempleCat ) {
			TemplateCategorization objectsTemplCat = TemplateUtils.getTemplateCategorization(eObject);
			for ( TemplateCategorization referencableTempleCat : objectsTemplCat.getReferencableTempleCats() ) {
				resources.addAll( collectResources(resourceSet, eclipseProject, fileExtension,referencableTempleCat) );
			}			
		} else {
			resources.addAll( collectResources(resourceSet, eclipseProject, fileExtension,null) );
		}
		return resources;
    }
		
	/**
	 * loads and returns the resources with the given file extension within the given project.
	 * 
	 * @param resourceSet the resource set to use when loading the resources
	 * @param eclipseProject the eclipse project to search
	 * @param fileExtension the desired file extension
	 * @return the matching resources, loaded in the context of the resourceSet of the eObject, never null but may be empty
	 */
	public static List<Resource> collectResources(final ResourceSet resourceSet, final IProject eclipseProject, String fileExtension, TemplateCategorization templeCat)
	{
		final List<Resource> result = new ArrayList<Resource>();
		try
		{
			List<IFile> eclipseFiles = collectFiles(eclipseProject, fileExtension,templeCat);
			for (IFile file : eclipseFiles)
			{
				String fileString = file.getFullPath().toString();
				URI emfUri = URI.createPlatformResourceURI(fileString,true);
				Resource emfResouce = resourceSet.getResource(emfUri,true);
				result.add(emfResouce);
			}
		} catch (CoreException e)
		{
			PluginActivator.logException("Could not load project resources: "+e,e);
		}
		return result;
    }
	
	/**
	 * loads and returns the files with the given file extension within the given project.
	 * 
	 * @param eclipseProject the eclipse project to search
	 * @param fileExtension the desired file extension
	 * @return the matching Files, loaded in the context of the resourceSet of the eObject, never null but may be empty
	 * @throws CoreException if an error occurs
	 */
	public static List<IFile> collectFiles(final IProject eclipseProject, String fileExtension) throws CoreException
	{
		return collectFiles(eclipseProject,fileExtension,null);
    }
	
	public static List<IFile> collectFiles(final IProject eclipseProject, String fileExtension, final TemplateCategorization templeCat) throws CoreException
	{
		final String fileExtensionLc = fileExtension.toLowerCase();
		
		final List<IFile> result = new ArrayList<IFile>();
		eclipseProject.accept( new IResourceVisitor()
		{
			@Override
			public boolean visit(IResource eclipseResourceInProject) throws CoreException
			{
				if (eclipseResourceInProject instanceof IFile)
				{
					String extension = eclipseResourceInProject.getFileExtension();
					String path = eclipseResourceInProject.getProjectRelativePath().toString();

					if (extension != null && extension.toLowerCase().equals(fileExtensionLc)) {
						if (templeCat != TemplateUtils.getTemplateCategorizationFor(path) && templeCat != null) {
							return false;
						}

						// include this one
						result.add((IFile) eclipseResourceInProject);
					}
				}
				return true;
			}
		});
		return result;
    }
	
	
	
	
	/**
	 * @param emfResource an emf resource
	 * @return the base file name (without dot and extension) of the file of the emf resource,
	 * may return null if that name cannot be determined 
	 */
	public static String getBaseFilename(Resource emfResource) {
		if (emfResource==null)
			return null;
		IResource eclipseResource = EMFUtils.getEclipseResource(emfResource);
		if (eclipseResource==null)
			return null;
		if (!eclipseResource.exists())
			return null;
		int dotExtensionStart = eclipseResource.getName().length()-eclipseResource.getFileExtension().length()-1;
		if (dotExtensionStart<=0) {
			return null;
		}
		return eclipseResource.getName().substring(0,dotExtensionStart);
	}
	
}
