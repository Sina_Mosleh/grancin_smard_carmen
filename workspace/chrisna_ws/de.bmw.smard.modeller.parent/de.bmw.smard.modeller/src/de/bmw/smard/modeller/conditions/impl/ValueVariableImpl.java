package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.ValueVariable;
import de.bmw.smard.modeller.conditions.ValueVariableObserver;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ValueVariableImpl#getInitialValue <em>Initial Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ValueVariableImpl#getValueVariableObservers <em>Value Variable Observers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ValueVariableImpl extends VariableImpl implements ValueVariable {
    /**
     * The default value of the '{@link #getInitialValue() <em>Initial Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInitialValue()
     * @generated
     * @ordered
     */
    protected static final String INITIAL_VALUE_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getInitialValue() <em>Initial Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInitialValue()
     * @generated
     * @ordered
     */
    protected String initialValue = INITIAL_VALUE_EDEFAULT;

    /**
     * This is true if the Initial Value attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean initialValueESet;

    /**
     * The cached value of the '{@link #getValueVariableObservers() <em>Value Variable Observers</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueVariableObservers()
     * @generated
     * @ordered
     */
    protected EList<ValueVariableObserver> valueVariableObservers;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ValueVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VALUE_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInitialValue() {
        return initialValue;
    }

    /**
     * <!-- begin-user-doc -->
     * check for validity dependent on currentEvalDataType
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public void setInitialValue(String newInitialValue) {
        if (isValidInitialValue(newInitialValue)) {
            String oldInitialValue = initialValue;
            initialValue = newInitialValue;
            boolean oldInitialValueESet = initialValueESet;
            initialValueESet = true;
            if (eNotificationRequired())
                eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE, oldInitialValue, initialValue, !oldInitialValueESet));
        }
    }

    /** generated NOT */
    public boolean isValidInitialValue(String newInitialValue) {
        if (getDataType() == DataType.DOUBLE
            && !(newInitialValue.trim().startsWith("#")
                && newInitialValue.trim().endsWith("#"))) {
            boolean isNumber = false;
            try {
                Double.valueOf(newInitialValue);
                isNumber = true;
            } catch (NumberFormatException e) {
            }
            return isNumber;
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetInitialValue() {
        String oldInitialValue = initialValue;
        boolean oldInitialValueESet = initialValueESet;
        initialValue = INITIAL_VALUE_EDEFAULT;
        initialValueESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE, oldInitialValue, INITIAL_VALUE_EDEFAULT, oldInitialValueESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetInitialValue() {
        return initialValueESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<ValueVariableObserver> getValueVariableObservers() {
        if (valueVariableObservers == null) {
            valueVariableObservers =
                    new EObjectContainmentEList<ValueVariableObserver>(ValueVariableObserver.class, this, ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS);
        }
        return valueVariableObservers;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS:
                return ((InternalEList<?>) getValueVariableObservers()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE:
                return getInitialValue();
            case ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS:
                return getValueVariableObservers();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE:
                setInitialValue((String) newValue);
                return;
            case ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS:
                getValueVariableObservers().clear();
                getValueVariableObservers().addAll((Collection<? extends ValueVariableObserver>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE:
                unsetInitialValue();
                return;
            case ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS:
                getValueVariableObservers().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VALUE_VARIABLE__INITIAL_VALUE:
                return isSetInitialValue();
            case ConditionsPackage.VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS:
                return valueVariableObservers != null && !valueVariableObservers.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (initialValue: ");
        if (initialValueESet) result.append(initialValue);
        else result.append("<unset>");
        result.append(')');
        return result.toString();
    }

    /** @generated NOT */
    @Override
    public EList<AbstractObserver> getObservers() {
        if (valueVariableObservers == null || valueVariableObservers.isEmpty())
            return (EList<AbstractObserver>) ECollections.EMPTY_ELIST;

        EList<AbstractObserver> observers = new BasicEList<AbstractObserver>(valueVariableObservers.size());
        observers.addAll(valueVariableObservers);
        return observers;
    }

} //ValueVariableImpl
