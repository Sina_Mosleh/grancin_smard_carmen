package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.statemachine.StateType;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.StateImpl#getStateTypeTmplParam <em>State Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateImpl extends AbstractStateImpl implements State {
    /**
     * The default value of the '{@link #getStateTypeTmplParam() <em>State Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStateTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STATE_TYPE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStateTypeTmplParam() <em>State Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStateTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String stateTypeTmplParam = STATE_TYPE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StateImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.STATE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStateTypeTmplParam() {
        return stateTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStateTypeTmplParam(String newStateTypeTmplParam) {
        String oldStateTypeTmplParam = stateTypeTmplParam;
        stateTypeTmplParam = newStateTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.STATE__STATE_TYPE_TMPL_PARAM, oldStateTypeTmplParam, stateTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        List<StateType> legalStateTypes = Arrays.asList(
                StateType.INFO,
                StateType.DEFECT,
                StateType.OK,
                StateType.WARN);

        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.STATE__IS_VALID_HAS_VALID_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(stateType)
                        .templateType(StateType.TEMPLATE_DEFINED)
                        .tmplParam(stateTypeTmplParam)
                        .containsParameterError("_Validation_Statemachine_State_StateType_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Statemachine_State_StateTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Statemachine_State_StateTypeTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(legalStateTypes, "_Validation_Statemachine_State_StateType_IllegalStateType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.STATE__STATE_TYPE_TMPL_PARAM:
                return getStateTypeTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.STATE__STATE_TYPE_TMPL_PARAM:
                setStateTypeTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.STATE__STATE_TYPE_TMPL_PARAM:
                setStateTypeTmplParam(STATE_TYPE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.STATE__STATE_TYPE_TMPL_PARAM:
                return STATE_TYPE_TMPL_PARAM_EDEFAULT == null ? stateTypeTmplParam != null : !STATE_TYPE_TMPL_PARAM_EDEFAULT.equals(stateTypeTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (stateTypeTmplParam: ");
        result.append(stateTypeTmplParam);
        result.append(')');
        return result.toString();
    }

} // StateImpl
