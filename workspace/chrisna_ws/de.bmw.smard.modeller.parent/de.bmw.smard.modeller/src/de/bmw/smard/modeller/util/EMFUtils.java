package de.bmw.smard.modeller.util;

import de.bmw.smard.modeller.conditions.BaseClassWithID;
import de.bmw.smard.modeller.util.TemplateUtils.TemplateCategorization;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * various utility methods for dealing with EMF classes 
 * (on a technical basis, not on a business basis as in de.bmw.smard.modeller.utils.data.DataUtils.)
 *
 */
public final class EMFUtils
{
	/**
	 * No instantiation
	 */
	private EMFUtils() {}

	/**
	 * Finds and returns the Eclipse-Resource for the given EMF-Resource
	 * or null if the resource could not be mapped
	 * @param emfResource the EMF-Resource
	 * @return the Eclipse-resource or null if it could not be mapped
	 */
	public static IResource getEclipseResource( Resource emfResource)
	{
	    if (emfResource == null)
	        return null;
		return WorkspaceSynchronizer.getFile(emfResource);
	}
	
	/**
	 * Finds and returns the Eclipse Project for the given EMF-Resource
	 * or null if the resource could not be mapped
	 * @param emfResource the EMF-Resource
	 * @return the Eclipse project or null if it could not be mapped
	 */
	public static IProject getEclipseProject( Resource emfResource)
	{
		IResource eclipseResource = getEclipseResource(emfResource);
		if (eclipseResource==null)
			return null;
		return eclipseResource.getProject();
	}


    /**
	 * Checks if two objects are from the same project (in the eclipse workspace sense).
	 * Note: the implementation currently only works with platform resources. If an object's resource's URI is not platform,
	 * the method will return false.
	 * 
	 * @param obj1 the first object
	 * @param obj2 the second object
	 * @return true if both objects are from the same project in the workspace
	 */
	public static boolean sameProject(EObject obj1, EObject obj2)
	{
		if (obj1.eResource()==null)
		{
			return false;
		}
		if (obj2.eResource()==null)
		{
			return false;
		}
		return sameProject(obj1.eResource(),obj2.eResource());
	}


	public static boolean notInSameProject(EObject obj1, EObject obj2) {
		if (obj1.eResource() == null || obj2.eResource() == null) {
			return false;
		}

		return !sameProject(obj1.eResource(),obj2.eResource());
	}

	/**
     * Takes a resource and returns the file encoding 
     * contained in the XML-Header of the file as a String.
     *   
     * @param resource the resource to get the encoding from
     * @return the encoding of the resource
	 * @throws IOException 
     */
	public static String getXMLEncodingForResource(Resource resource) throws IOException {
	    ExtensibleURIConverterImpl converter = new ExtensibleURIConverterImpl();
        URI uri = resource.getURI();
        Map<String, Set<String>> options = new HashMap<>();
        Set<String> properties = new HashSet<>();
        properties.add(ContentHandler.CHARSET_PROPERTY);
        options.put(ContentHandler.OPTION_REQUESTED_PROPERTIES, properties);
        Map<String, ?> contentDescription = converter.contentDescription(uri, options);
        return contentDescription.get(ContentHandler.CHARSET_PROPERTY).toString();
	}
	
	/**
	 * appends (in brackets) the filename of the object's resource to the given label and returns the result.
	 * If the label is null, it is treated as if it were an empty string.
	 * If the filename is not available, the label is returned unchanged.
	 *   
	 * @param label the label to add the filename to
	 * @param object the object whose file's name should be added
	 * @return the label with the added filename
	 */
	public static String appendFilename(String label, EObject object)
	{
		if (label==null)
		{
			label = "";
		}
		if (object==null)
		{
			return label;
		}
		Resource res = object.eResource();
		if (res == null)
		{
			return label;
		}
		return label + " [" + URI.decode (res.getURI().lastSegment()) + "]";
	}
	
	public static <T> List<T> getObjectsInProject(Class<T> requiredType,
			String fileextension, EObject objectInProject, boolean includeNull,
			boolean considerTemplateCategorization) {
		return getObjectsInProject(requiredType, fileextension, objectInProject, includeNull, considerTemplateCategorization, false);
	}

	/**
	 * Goes through all files in a project that have a specific extension and returns all objects in those files that
	 * have a specific type.
	 * 
	 * @param <T> the type
	 * @param requiredType the class whose instances should be found within the project
	 * @param fileextension the extension of the files in the project that should be searched for the objects
	 * @param objectInProject an object in the project (to get a resource and to determine the availability if considerTemplateCategorization is true)
	 * @param includeNull if true, the first value in the list will be a null
	 * @param considerTemplateCategorization if true, only objects that can be referenced from the given object will be included
	 * @return a list of all objects of the given type that are present in the files with the given extension inside the project identified by the object
	 */
	public static <T> List<T> getObjectsInProject(Class<T> requiredType,
			String fileextension, EObject objectInProject, boolean includeNull,
			boolean considerTemplateCategorization, boolean onlyFilesFromSameTemplateProject) {
		List<T> result = new ArrayList<T>();
		if (includeNull) {
			result.add(null);
		}
		TemplateCategorization sourceObjectTemplateCategorization = TemplateUtils.getTemplateCategorization(objectInProject);

		List<Resource> relevantResources = ResourceUtils.collectResources(
				objectInProject, fileextension, considerTemplateCategorization);

		for (Resource nrs : relevantResources) {
			if (!considerTemplateCategorization
					|| sourceObjectTemplateCategorization.allowsReferenceTo(TemplateUtils.getTemplateCategorization(nrs))) {
				if(onlyFilesFromSameTemplateProject) {
					if(TemplateUtils.resourcesInTemplateContext(objectInProject.eResource(), nrs)) {
						if(TemplateUtils.resourcesInSameTemplateFolder(objectInProject.eResource(), nrs)) {
							result.addAll(getAllContentsFromResource(nrs, requiredType));
						}
					} else {
						result.addAll(getAllContentsFromResource(nrs, requiredType));
					}
				} else {
					result.addAll(getAllContentsFromResource(nrs, requiredType));
				}
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private static <T> List<T> getAllContentsFromResource(Resource resource, Class<T> requiredType) {
		List<T> result = new ArrayList<T>();
		TreeIterator<EObject> ittree = resource.getAllContents();
		while (ittree.hasNext()) {
			EObject eobj = ittree.next();
			if (requiredType.isAssignableFrom(eobj.getClass())) {
				result.add((T) eobj);
			}
		}
		return result;
	}
	

	/**
	 * checks if the two emf resources are in the same eclipse project
	 * @param resource1 the first resource
	 * @param resource2 the second resource
	 * @return true if both resources are in the same eclipse project 
	 */
	public static boolean sameProject(Resource resource1,
			Resource resource2)
	{
		IResource workspaceResource1 = getEclipseResource(resource1);
		if (workspaceResource1==null)
		{
			return false;
		}
		IResource workspaceResource2 = getEclipseResource(resource2);
		if (workspaceResource2==null)
		{
			return false;
		}
		return workspaceResource1.getProject()==workspaceResource2.getProject();
	}

	/**
	 * If the given object is one of ours that requires a unique id (e.g. Transition) it will be assigned a new one.
	 * @param obj the object
	 */
	public static void assignNewUniqueObjectId(Object obj)
	{
		if(obj instanceof BaseClassWithID) {
			((BaseClassWithID)obj).setId(createNewUniqueObjectId((EObject)obj));
		}

//		else
//		{
//			// js - unless there is a common interface for "id" and "oid" it can not be determined which types must be handled
//			// extending the model ends with high possibility in forgetting an entry here...
//			sLog.warn("unknown object for setting new ID " + obj);
//		}
	}
	
	public static String createNewUniqueObjectId(EObject obj) {
		String newUniqueObjectId = UUID.randomUUID().toString();
		return newUniqueObjectId;
	}

	/**
	 * As above, but done properly, i.e. traverses recursively.
	 * @param obj
	 */
	@SuppressWarnings("unchecked")
	public static void deepAssignNewUniqueObjectId(EObject pObj)
	{
		EClass meta = pObj.eClass();		
		
		for (EReference ref : meta.getEAllReferences())
		{
			if (ref.isContainment())
			{
				Object obj = pObj.eGet(ref);
				
				if (obj instanceof EObject)
				{
					deepAssignNewUniqueObjectId((EObject)obj);
				}
				else if (obj instanceof EList)
				{
					List<EObject> objList = (List<EObject>) obj;
					for (EObject eObject : objList)
					{
						deepAssignNewUniqueObjectId(eObject);
					}
				}
			}
		}
		
		assignNewUniqueObjectId(pObj);
	}
	
	// TODO MJ comment
	public static boolean isInSubtreeOfTypes(EObject childObject, Class<? extends EObject>... typesToFindInTreeAbove ) {
		
		EObject container = childObject.eContainer();
		
		while (container != null) {
			if (isOfTypeOrSubType(container, typesToFindInTreeAbove))
				return true;
			container = container.eContainer();
		}		
		return false;
	}
	
	
	// TODO MJ comment
	public static boolean isOfTypeOrSubType(Object obj, Class... types) {
		if ( obj != null && types != null ) {
			for (Class clazz : types) {
				if (clazz.isAssignableFrom(obj.getClass())) {
					return true;
				}
			}			
		}
		return false;
	}
}
