package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.statemachine.State;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActive <em>Check State Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckState <em>Check State</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActiveTmplParam <em>Check State Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStateCheckExpression()
 * @model extendedMetaData="name='stateCheckCondition' kind='empty'"
 * @generated
 */
public interface StateCheckExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Check State Active</b></em>' attribute.
     * The default value is <code>"true"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Check State Active</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Check State Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #setCheckStateActive(BooleanOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStateCheckExpression_CheckStateActive()
     * @model default="true" required="true"
     *        extendedMetaData="kind='attribute' name='checkStateActive' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    BooleanOrTemplatePlaceholderEnum getCheckStateActive();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActive <em>Check State Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Check State Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #getCheckStateActive()
     * @generated
     */
    void setCheckStateActive(BooleanOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Check State</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Check State</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Check State</em>' reference.
     * @see #setCheckState(State)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStateCheckExpression_CheckState()
     * @model required="true"
     * @generated
     */
    State getCheckState();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckState <em>Check State</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Check State</em>' reference.
     * @see #getCheckState()
     * @generated
     */
    void setCheckState(State value);

    /**
     * Returns the value of the '<em><b>Check State Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Check State Active Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Check State Active Tmpl Param</em>' attribute.
     * @see #setCheckStateActiveTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStateCheckExpression_CheckStateActiveTmplParam()
     * @model
     * @generated
     */
    String getCheckStateActiveTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActiveTmplParam <em>Check State Active Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Check State Active Tmpl Param</em>' attribute.
     * @see #getCheckStateActiveTmplParam()
     * @generated
     */
    void setCheckStateActiveTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context);

} // StateCheckExpression
