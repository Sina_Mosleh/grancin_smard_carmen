package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue <em>Initial Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.ValueVariable#getValueVariableObservers <em>Value Variable Observers</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getValueVariable()
 * @model
 * @generated
 */
public interface ValueVariable extends Variable {
    /**
     * Returns the value of the '<em><b>Initial Value</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Initial Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Initial Value</em>' attribute.
     * @see #isSetInitialValue()
     * @see #unsetInitialValue()
     * @see #setInitialValue(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getValueVariable_InitialValue()
     * @model default="0" unsettable="true" dataType="de.bmw.smard.modeller.conditions.ValVarInitialValueDoubleOrTemplatePlaceholderOrString"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getInitialValue();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue <em>Initial Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Initial Value</em>' attribute.
     * @see #isSetInitialValue()
     * @see #unsetInitialValue()
     * @see #getInitialValue()
     * @generated
     */
    void setInitialValue(String value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue <em>Initial Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetInitialValue()
     * @see #getInitialValue()
     * @see #setInitialValue(String)
     * @generated
     */
    void unsetInitialValue();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue <em>Initial Value</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Initial Value</em>' attribute is set.
     * @see #unsetInitialValue()
     * @see #getInitialValue()
     * @see #setInitialValue(String)
     * @generated
     */
    boolean isSetInitialValue();

    /**
     * Returns the value of the '<em><b>Value Variable Observers</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.ValueVariableObserver}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value Variable Observers</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value Variable Observers</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getValueVariable_ValueVariableObservers()
     * @model containment="true"
     * @generated
     */
    EList<ValueVariableObserver> getValueVariableObservers();

} // ValueVariable
