package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class GreaterZeroValidator implements ValueValidator {
	
	private final String errorMessageIdentifier;
	
	public GreaterZeroValidator(String errorMessageIdentifier) {
		this.errorMessageIdentifier = errorMessageIdentifier;
	}

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		if(TemplateUtils.getDoubleOrHexNotPlaceholder(value, true) < 0) {
			if(baseValidator != null) {
				baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
			}
			return false;
		}
		return true;
	}

}
