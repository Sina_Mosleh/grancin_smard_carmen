package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class BusIdValidator implements ValueValidator {

    private String interfaceName;

    BusIdValidator(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        String errorMessageIdentifier = null;

        Integer busId = null;
        try {
            busId = TemplateUtils.getIntNotPlaceholder(value, true);
        } catch (NumberFormatException e) {
        }

        if(busId != null && busId == 0) {
            errorMessageIdentifier = "_Validation_Conditions_" + interfaceName + "_BusId_IsZero";
        } else if (value != null && value.trim().length() != 0) {
            try {
                int busIdInt = TemplateUtils.getIntNotPlaceholder(value, false);
                if (busIdInt < 0) {
                    errorMessageIdentifier = "_Validation_Conditions_" + interfaceName + "_BusId_LowerZero";
                }
            } catch (NumberFormatException e) {
                errorMessageIdentifier = "_Validation_Conditions_" + interfaceName + "_BusId_NotANumber";
            }
        } else {
            errorMessageIdentifier = "_Validation_Conditions_" + interfaceName + "_BusId_IsNull";
        }

        if (!StringUtils.nullOrEmpty(errorMessageIdentifier)) {
            if (baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
            }
            return false;
        }
        return true;
    }
}
