package de.bmw.smard.modeller.util;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.Variables;
import de.bmw.smard.common.math.Assignment;
import de.bmw.smard.common.math.BinaryOperation;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.common.math.VariableReference;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractVariable;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.Variable;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import java.util.List;
import java.util.Map;

public class ActionExpressionUtils {

	
    public static EList<String> extractVariableNames(String expression) {
        if (StringUtils.isBlank(expression)) {
            return null;
        }
        Term term = MathParser.parse(expression, null);
        EList<String> variableNames = new BasicEList<>(term.getReferencedVariableNames());
        variableNames.addAll(term.getAssignedVariableNames());
        return variableNames;
    }
	
	public static EList<String> extractReadVariableNames(String expressionString, EList<? extends IOperand> operands,
														 boolean parseStringLiterals) {

		if (StringUtils.nullOrEmpty(expressionString))
			return ECollections.emptyEList();

		Term term = null;
		try {
			term = MathParser.parse(expressionString, operands, parseStringLiterals);
		} catch (ActionExpressionParseException e) {
			PluginActivator.logException("math parsing expression failed", e);
			return ECollections.emptyEList();
		}

		BasicEList<String> result = new BasicEList<>();

		if (term != null && term.getReferencedVariableNames() != null
				&& term.getReferencedVariableNames().size() >= 1) {
			List<String> referencedVariableNames = term.getReferencedVariableNames();
			result.addAll(ListEListConverter.convertToEList(referencedVariableNames));
			
			if (operands != null && !operands.isEmpty()) {
				for (IOperand operand : operands) {
					for (String name: operand.getReadVariables())
						if (result.contains(name)==false)
							result.add(name);
				}
			}
		}		

		return result;
	}
	
	public static void checkAndWarnForDeprecatedSystemVariableUsage(List<String> variableNames, 
			String expression, EObject caller, 
			DiagnosticChain diagnostics, Map<Object, Object> context) {

		// SMARD-2478 no more warning for usage of system variable "Timestamp"
		if(variableNames.contains("Timestamp")) {
			return;
		}
		
		if (variableNames != null && Variables.containsPredefinedName(variableNames)) {			

			if (diagnostics != null && context != null && !context.isEmpty()) {

				String warningMessageIdentifier = "_Validation_DeprecatedSystemVariableUsage";
				String actionExpression = expression != null ? expression : "";
				
				diagnostics.add(DiagnosticBuilder.warn()
						.source(StatemachineValidator.DIAGNOSTIC_SOURCE)
						.code(StatemachineValidator.ACTION__IS_VALID_HAS_VALID_ACTION_EXPRESSION)
						.messageId(PluginActivator.INSTANCE, warningMessageIdentifier, actionExpression)
						.data(caller == null ? "NO_CALLER" : caller)
						.build());
			}				
		}
	}
	
	public static boolean isValidTermWithResolvedVariableReferences(Term term, Map<String, AbstractVariable> mapNameToVariable) {
		boolean result = true;
		if(term instanceof BinaryOperation) {
			boolean isValidLeft = isValidTermWithResolvedVariableReferences(((BinaryOperation) term).getLeft(), mapNameToVariable);
			boolean isValidRight = isValidTermWithResolvedVariableReferences(((BinaryOperation) term).getRight(), mapNameToVariable);
			result = isValidLeft && isValidRight;

			if(result) {
				DataType dataTypeLeft = getDataTypeForTerm(((BinaryOperation) term).getLeft(), mapNameToVariable);
				DataType dataTypeRight = getDataTypeForTerm(((BinaryOperation) term).getRight(), mapNameToVariable);
				// if one or both dataTypes are null, this does not concern *this* validation, but has to be validated separately
				
				if(DataType.STRING.equals(dataTypeLeft) || DataType.STRING.equals(dataTypeRight)) {
					char operator = ((BinaryOperation) term).getOperator();
					if(operator != '+') {
						result = false;
					}
				}
			}
		} else if(term instanceof Assignment) {
			result = isValidTermWithResolvedVariableReferences(((Assignment) term).getValue(), mapNameToVariable);
		}
		
		return result;
	}
	
	private static DataType getDataTypeForTerm(Term term, Map<String, AbstractVariable> mapNameToVariable) {
		DataType dataTypeOfTerm = null;
		if(term instanceof VariableReference) {
			if(mapNameToVariable != null && !mapNameToVariable.isEmpty()) {
				String referencedVariableName = ((VariableReference) term).getVariable();
				AbstractVariable referencedVariable = mapNameToVariable.get(referencedVariableName);
				if(referencedVariable != null && referencedVariable instanceof Variable) {
					dataTypeOfTerm = ((Variable)referencedVariable).getDataType();
				} else {
					dataTypeOfTerm = null;
				}
			}
		} else if(term instanceof Operation) {
			dataTypeOfTerm = getDataTypeForOperand((IOperand) ((Operation) term).getOperand(), mapNameToVariable);
		} else if(term instanceof BinaryOperation) {
			DataType dataTypeLeft = getDataTypeForTerm(((BinaryOperation) term).getLeft(), mapNameToVariable);
			DataType dataTypeRight = getDataTypeForTerm(((BinaryOperation) term).getRight(), mapNameToVariable);
			if(dataTypeLeft == null || dataTypeRight == null) {
				dataTypeOfTerm = null;
			}
			if(DataType.STRING.equals(dataTypeLeft) || DataType.STRING.equals(dataTypeRight)) {
				dataTypeOfTerm = DataType.STRING;
			} else {
				dataTypeOfTerm = DataType.DOUBLE;
			}
		} else {
			dataTypeOfTerm = DataType.getByName(term.getEvaluationDataType().name());
		}
		return dataTypeOfTerm;
	}

	private static DataType getDataTypeForOperand(IOperand operand, Map<String, AbstractVariable> mapNameToVariable) {
		if(operand instanceof de.bmw.smard.modeller.conditions.VariableReference) {
			return ((de.bmw.smard.modeller.conditions.VariableReference) operand).getVariable().getDataType();
		} else {
			return operand.get_EvaluationDataType();
		}
	}
	
}
