package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.TCPFilter;
import de.bmw.smard.modeller.conditions.TCPMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPMessageImpl#getTcpFilter <em>Tcp Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCPMessageImpl extends AbstractBusMessageImpl implements TCPMessage {
    /**
     * The cached value of the '{@link #getTcpFilter() <em>Tcp Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTcpFilter()
     * @generated
     * @ordered
     */
    protected TCPFilter tcpFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected TCPMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.TCP_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public TCPFilter getTcpFilter() {
        return tcpFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetTcpFilter(TCPFilter newTcpFilter, NotificationChain msgs) {
        TCPFilter oldTcpFilter = tcpFilter;
        tcpFilter = newTcpFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_MESSAGE__TCP_FILTER, oldTcpFilter, newTcpFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTcpFilter(TCPFilter newTcpFilter) {
        if (newTcpFilter != tcpFilter) {
            NotificationChain msgs = null;
            if (tcpFilter != null)
                msgs = ((InternalEObject) tcpFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.TCP_MESSAGE__TCP_FILTER, null, msgs);
            if (newTcpFilter != null)
                msgs = ((InternalEObject) newTcpFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.TCP_MESSAGE__TCP_FILTER, null, msgs);
            msgs = basicSetTcpFilter(newTcpFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_MESSAGE__TCP_FILTER, newTcpFilter, newTcpFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.TCP_MESSAGE__TCP_FILTER:
                return basicSetTcpFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.TCP_MESSAGE__TCP_FILTER:
                return getTcpFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.TCP_MESSAGE__TCP_FILTER:
                setTcpFilter((TCPFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TCP_MESSAGE__TCP_FILTER:
                setTcpFilter((TCPFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TCP_MESSAGE__TCP_FILTER:
                return tcpFilter != null;
        }
        return super.eIsSet(featureID);
    }

    @Override
    public AbstractFilter getPrimaryFilter() {
        return tcpFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //TCPMessageImpl
