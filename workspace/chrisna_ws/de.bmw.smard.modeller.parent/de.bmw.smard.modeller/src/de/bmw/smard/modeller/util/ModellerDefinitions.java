package de.bmw.smard.modeller.util;

import de.bmw.smard.modeller.PluginActivator;

/**
 * some constants for use in the modeller plugins
 */
public final class ModellerDefinitions
{
	
	/**
	 * No instantiation
	 */
	private ModellerDefinitions() {}
	
	/** the id of the validation marker type that EMF uses (and where we're children of) */
	public static final String ID_EMF_VALIDATION_MARKER = "org.eclipse.emf.validation.problem";
	
    /** id of the unresolved reference marker, must match the value in the plugin.xml */
    public static final String ID_UNRESOLVED_REFERENCE_MARKER = PluginActivator.PLUGIN_ID+".unresolvedReference";
	
	

}
