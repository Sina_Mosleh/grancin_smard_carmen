package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DLT Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDLTMessage()
 * @model abstract="true"
 * @generated
 */
public interface DLTMessage extends AbstractBusMessage {

} // DLTMessage
