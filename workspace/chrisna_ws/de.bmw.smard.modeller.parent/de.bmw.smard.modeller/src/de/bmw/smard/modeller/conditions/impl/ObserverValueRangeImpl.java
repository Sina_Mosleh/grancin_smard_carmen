package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ObserverValueRange;
import de.bmw.smard.modeller.conditions.ObserverValueType;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Observer Value Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl#getValue <em>Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl#getValueType <em>Value Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObserverValueRangeImpl extends EObjectImpl implements ObserverValueRange {
    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final String VALUE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected String value = VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getValueType() <em>Value Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueType()
     * @generated
     * @ordered
     */
    protected static final ObserverValueType VALUE_TYPE_EDEFAULT = ObserverValueType.INFO;

    /**
     * The cached value of the '{@link #getValueType() <em>Value Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValueType()
     * @generated
     * @ordered
     */
    protected ObserverValueType valueType = VALUE_TYPE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ObserverValueRangeImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.OBSERVER_VALUE_RANGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValue(String newValue) {
        String oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.OBSERVER_VALUE_RANGE__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ObserverValueType getValueType() {
        return valueType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValueType(ObserverValueType newValueType) {
        ObserverValueType oldValueType = valueType;
        valueType = newValueType == null ? VALUE_TYPE_EDEFAULT : newValueType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE_TYPE, oldValueType, valueType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.OBSERVER_VALUE_RANGE__IS_VALID_HAS_VALID_VALUE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.stringTemplate(getValue())
                        .containsParameterError("_Validation_Conditions_ObserverValueRange_Value_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_ObserverValueRange_Value_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context) {


        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.OBSERVER_VALUE_RANGE__IS_VALID_HAS_VALID_DESCRIPTION)
                .object(this)
                .diagnostic(diagnostics)

                // must not be null see SMC
                .with(Validators.notNull(description)
                        .error("_Validation_Conditions_ObserverValueRange_Description_IsNull")
                        .build())

                .with(Validators.stringTemplate(description)
                        .containsParameterError("_Validation_Conditions_ObserverValueRange_Description_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_ObserverValueRange_Description_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE:
                return getValue();
            case ConditionsPackage.OBSERVER_VALUE_RANGE__DESCRIPTION:
                return getDescription();
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE_TYPE:
                return getValueType();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE:
                setValue((String) newValue);
                return;
            case ConditionsPackage.OBSERVER_VALUE_RANGE__DESCRIPTION:
                setDescription((String) newValue);
                return;
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE_TYPE:
                setValueType((ObserverValueType) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE:
                setValue(VALUE_EDEFAULT);
                return;
            case ConditionsPackage.OBSERVER_VALUE_RANGE__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE_TYPE:
                setValueType(VALUE_TYPE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE:
                return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
            case ConditionsPackage.OBSERVER_VALUE_RANGE__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case ConditionsPackage.OBSERVER_VALUE_RANGE__VALUE_TYPE:
                return valueType != VALUE_TYPE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (value: ");
        result.append(value);
        result.append(", description: ");
        result.append(description);
        result.append(", valueType: ");
        result.append(valueType);
        result.append(')');
        return result.toString();
    }

} // ObserverValueRangeImpl
