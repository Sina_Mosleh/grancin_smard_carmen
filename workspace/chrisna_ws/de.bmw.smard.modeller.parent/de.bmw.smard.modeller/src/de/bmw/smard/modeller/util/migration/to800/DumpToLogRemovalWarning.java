package de.bmw.smard.modeller.util.migration.to800;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import de.bmw.smard.modeller.util.migration.MigrationMarkerUtil;

public class DumpToLogRemovalWarning extends CustomMigration {
	
	private static final String ACTION = "statemachine.Action";
	private static final String ACTION_TYPE = "actionType";
	private static final String DUMP_TO_LOG_LITERAL = "DumptoLog";
	
	private Set<Instance> actionsWithDumpToLog = new HashSet<>();
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		// collect Actions with ActionType.DumpToLog
		for(Instance action : model.getAllInstances(ACTION)) {
			EEnumLiteral actionType = action.get(ACTION_TYPE);
			if(DUMP_TO_LOG_LITERAL.equalsIgnoreCase(actionType.getLiteral())) {
				actionsWithDumpToLog.add(action);
			}
		}
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
		// warn user that action with ActionType.DumpToLog was deleted and delete it from model
		for(Instance action : actionsWithDumpToLog) {
			String nameOfParent = "";
			
			Instance parentOfAction = action.getContainer();
			if(parentOfAction.instanceOf("statemachine.State") || parentOfAction.instanceOf("statemachine.Transition")) {
				nameOfParent = parentOfAction.get("name");
			}
			
			MigrationMarkerUtil.createWarning(
					String.format("Action with ActionType DumpToLog was removed: %1$s", nameOfParent));
			model.delete(action);
		}
	}
}
