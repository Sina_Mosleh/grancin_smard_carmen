package de.bmw.smard.modeller.statemachine.util;

import de.bmw.smard.modeller.conditions.BaseClassWithID;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.IOperation;
import de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand;
import de.bmw.smard.modeller.conditions.IVariableReaderWriter;
import de.bmw.smard.modeller.statemachine.AbstractAction;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.Action;
import de.bmw.smard.modeller.statemachine.ComputeVariable;
import de.bmw.smard.modeller.statemachine.ControlAction;
import de.bmw.smard.modeller.statemachine.DocumentRoot;
import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.ShowVariable;
import de.bmw.smard.modeller.statemachine.SmardTraceElement;
import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage
 * @generated
 */
public class StatemachineAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static StatemachinePackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachineAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = StatemachinePackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StatemachineSwitch<Adapter> modelSwitch =
            new StatemachineSwitch<Adapter>() {
                @Override
                public Adapter caseDocumentRoot(DocumentRoot object) {
                    return createDocumentRootAdapter();
                }

                @Override
                public Adapter caseAbstractState(AbstractState object) {
                    return createAbstractStateAdapter();
                }

                @Override
                public Adapter caseTransition(Transition object) {
                    return createTransitionAdapter();
                }

                @Override
                public Adapter caseStateMachine(StateMachine object) {
                    return createStateMachineAdapter();
                }

                @Override
                public Adapter caseAction(Action object) {
                    return createActionAdapter();
                }

                @Override
                public Adapter caseInitialState(InitialState object) {
                    return createInitialStateAdapter();
                }

                @Override
                public Adapter caseState(State object) {
                    return createStateAdapter();
                }

                @Override
                public Adapter caseAbstractAction(AbstractAction object) {
                    return createAbstractActionAdapter();
                }

                @Override
                public Adapter caseComputeVariable(ComputeVariable object) {
                    return createComputeVariableAdapter();
                }

                @Override
                public Adapter caseShowVariable(ShowVariable object) {
                    return createShowVariableAdapter();
                }

                @Override
                public Adapter caseSmardTraceElement(SmardTraceElement object) {
                    return createSmardTraceElementAdapter();
                }

                @Override
                public Adapter caseControlAction(ControlAction object) {
                    return createControlActionAdapter();
                }

                @Override
                public Adapter caseBaseClassWithID(BaseClassWithID object) {
                    return createBaseClassWithIDAdapter();
                }

                @Override
                public Adapter caseIVariableReaderWriter(IVariableReaderWriter object) {
                    return createIVariableReaderWriterAdapter();
                }

                @Override
                public Adapter caseBusMessageReferable(BusMessageReferable object) {
                    return createBusMessageReferableAdapter();
                }

                @Override
                public Adapter caseIOperand(IOperand object) {
                    return createIOperandAdapter();
                }

                @Override
                public Adapter caseISignalComparisonExpressionOperand(ISignalComparisonExpressionOperand object) {
                    return createISignalComparisonExpressionOperandAdapter();
                }

                @Override
                public Adapter caseIOperation(IOperation object) {
                    return createIOperationAdapter();
                }

                @Override
                public Adapter defaultCase(EObject object) {
                    return createEObjectAdapter();
                }
            };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }


    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.DocumentRoot <em>Document Root</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.DocumentRoot
     * @generated
     */
    public Adapter createDocumentRootAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.AbstractState <em>Abstract State</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.AbstractState
     * @generated
     */
    public Adapter createAbstractStateAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.Transition <em>Transition</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.Transition
     * @generated
     */
    public Adapter createTransitionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.StateMachine <em>State Machine</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.StateMachine
     * @generated
     */
    public Adapter createStateMachineAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.Action <em>Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.Action
     * @generated
     */
    public Adapter createActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.InitialState <em>Initial State</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.InitialState
     * @generated
     */
    public Adapter createInitialStateAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.State <em>State</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.State
     * @generated
     */
    public Adapter createStateAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.AbstractAction <em>Abstract Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.AbstractAction
     * @generated
     */
    public Adapter createAbstractActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.ComputeVariable <em>Compute Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.ComputeVariable
     * @generated
     */
    public Adapter createComputeVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.ShowVariable <em>Show Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.ShowVariable
     * @generated
     */
    public Adapter createShowVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.SmardTraceElement <em>Smard Trace Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.SmardTraceElement
     * @generated
     */
    public Adapter createSmardTraceElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.ControlAction <em>Control Action</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.ControlAction
     * @generated
     */
    public Adapter createControlActionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.BaseClassWithID <em>Base Class With ID</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithID
     * @generated
     */
    public Adapter createBaseClassWithIDAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IVariableReaderWriter
     * @generated
     */
    public Adapter createIVariableReaderWriterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachineset.BusMessageReferable <em>Bus Message Referable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachineset.BusMessageReferable
     * @generated
     */
    public Adapter createBusMessageReferableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IOperand <em>IOperand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IOperand
     * @generated
     */
    public Adapter createIOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison
     * Expression Operand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand
     * @generated
     */
    public Adapter createISignalComparisonExpressionOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IOperation <em>IOperation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IOperation
     * @generated
     */
    public Adapter createIOperationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //StatemachineAdapterFactory
