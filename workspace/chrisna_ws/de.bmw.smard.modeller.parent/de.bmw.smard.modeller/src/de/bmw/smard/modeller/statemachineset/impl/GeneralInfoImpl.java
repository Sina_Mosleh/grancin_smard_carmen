package de.bmw.smard.modeller.statemachineset.impl;

import de.bmw.smard.modeller.statemachineset.GeneralInfo;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>General Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl#getAuthor <em>Author</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl#getTopic <em>Topic</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GeneralInfoImpl extends EObjectImpl implements GeneralInfo {
    /**
     * The default value of the '{@link #getAuthor() <em>Author</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAuthor()
     * @generated
     * @ordered
     */
    protected static final String AUTHOR_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getAuthor() <em>Author</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAuthor()
     * @generated
     * @ordered
     */
    protected String author = AUTHOR_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getTopic() <em>Topic</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTopic()
     * @generated
     * @ordered
     */
    protected static final String TOPIC_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getTopic() <em>Topic</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTopic()
     * @generated
     * @ordered
     */
    protected String topic = TOPIC_EDEFAULT;

    /**
     * The default value of the '{@link #getDepartment() <em>Department</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDepartment()
     * @generated
     * @ordered
     */
    protected static final String DEPARTMENT_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getDepartment() <em>Department</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDepartment()
     * @generated
     * @ordered
     */
    protected String department = DEPARTMENT_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected GeneralInfoImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinesetPackage.Literals.GENERAL_INFO;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAuthor() {
        return author;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAuthor(String newAuthor) {
        String oldAuthor = author;
        author = newAuthor;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.GENERAL_INFO__AUTHOR, oldAuthor, author));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.GENERAL_INFO__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTopic() {
        return topic;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTopic(String newTopic) {
        String oldTopic = topic;
        topic = newTopic;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.GENERAL_INFO__TOPIC, oldTopic, topic));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDepartment() {
        return department;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDepartment(String newDepartment) {
        String oldDepartment = department;
        department = newDepartment;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.GENERAL_INFO__DEPARTMENT, oldDepartment, department));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinesetPackage.GENERAL_INFO__AUTHOR:
                return getAuthor();
            case StatemachinesetPackage.GENERAL_INFO__DESCRIPTION:
                return getDescription();
            case StatemachinesetPackage.GENERAL_INFO__TOPIC:
                return getTopic();
            case StatemachinesetPackage.GENERAL_INFO__DEPARTMENT:
                return getDepartment();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinesetPackage.GENERAL_INFO__AUTHOR:
                setAuthor((String) newValue);
                return;
            case StatemachinesetPackage.GENERAL_INFO__DESCRIPTION:
                setDescription((String) newValue);
                return;
            case StatemachinesetPackage.GENERAL_INFO__TOPIC:
                setTopic((String) newValue);
                return;
            case StatemachinesetPackage.GENERAL_INFO__DEPARTMENT:
                setDepartment((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinesetPackage.GENERAL_INFO__AUTHOR:
                setAuthor(AUTHOR_EDEFAULT);
                return;
            case StatemachinesetPackage.GENERAL_INFO__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case StatemachinesetPackage.GENERAL_INFO__TOPIC:
                setTopic(TOPIC_EDEFAULT);
                return;
            case StatemachinesetPackage.GENERAL_INFO__DEPARTMENT:
                setDepartment(DEPARTMENT_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinesetPackage.GENERAL_INFO__AUTHOR:
                return AUTHOR_EDEFAULT == null ? author != null : !AUTHOR_EDEFAULT.equals(author);
            case StatemachinesetPackage.GENERAL_INFO__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case StatemachinesetPackage.GENERAL_INFO__TOPIC:
                return TOPIC_EDEFAULT == null ? topic != null : !TOPIC_EDEFAULT.equals(topic);
            case StatemachinesetPackage.GENERAL_INFO__DEPARTMENT:
                return DEPARTMENT_EDEFAULT == null ? department != null : !DEPARTMENT_EDEFAULT.equals(department);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (author: ");
        result.append(author);
        result.append(", description: ");
        result.append(description);
        result.append(", topic: ");
        result.append(topic);
        result.append(", department: ");
        result.append(department);
        result.append(')');
        return result.toString();
    }

} //GeneralInfoImpl
