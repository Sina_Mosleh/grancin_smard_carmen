package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.NonVerboseDLTFilter;
import de.bmw.smard.modeller.conditions.NonVerboseDLTMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTMessageImpl#getNonVerboseDltFilter <em>Non Verbose Dlt Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NonVerboseDLTMessageImpl extends DLTMessageImpl implements NonVerboseDLTMessage {
    /**
     * The cached value of the '{@link #getNonVerboseDltFilter() <em>Non Verbose Dlt Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNonVerboseDltFilter()
     * @generated
     * @ordered
     */
    protected NonVerboseDLTFilter nonVerboseDltFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected NonVerboseDLTMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.NON_VERBOSE_DLT_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NonVerboseDLTFilter getNonVerboseDltFilter() {
        return nonVerboseDltFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetNonVerboseDltFilter(NonVerboseDLTFilter newNonVerboseDltFilter, NotificationChain msgs) {
        NonVerboseDLTFilter oldNonVerboseDltFilter = nonVerboseDltFilter;
        nonVerboseDltFilter = newNonVerboseDltFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER, oldNonVerboseDltFilter, newNonVerboseDltFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNonVerboseDltFilter(NonVerboseDLTFilter newNonVerboseDltFilter) {
        if (newNonVerboseDltFilter != nonVerboseDltFilter) {
            NotificationChain msgs = null;
            if (nonVerboseDltFilter != null)
                msgs = ((InternalEObject) nonVerboseDltFilter).eInverseRemove(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER, null, msgs);
            if (newNonVerboseDltFilter != null)
                msgs = ((InternalEObject) newNonVerboseDltFilter).eInverseAdd(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER, null, msgs);
            msgs = basicSetNonVerboseDltFilter(newNonVerboseDltFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER, newNonVerboseDltFilter, newNonVerboseDltFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER:
                return basicSetNonVerboseDltFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER:
                return getNonVerboseDltFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER:
                setNonVerboseDltFilter((NonVerboseDLTFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER:
                setNonVerboseDltFilter((NonVerboseDLTFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER:
                return nonVerboseDltFilter != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * @generated NOT
     */
    @Override
    public AbstractFilter getPrimaryFilter() {
        return nonVerboseDltFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //NonVerboseDLTMessageImpl
