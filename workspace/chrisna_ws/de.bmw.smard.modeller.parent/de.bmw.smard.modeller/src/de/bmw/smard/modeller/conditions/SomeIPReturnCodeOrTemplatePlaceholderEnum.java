package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPReturnCodeOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum SomeIPReturnCodeOrTemplatePlaceholderEnum implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>OK</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #OK_VALUE
     * @generated
     * @ordered
     */
    OK(0, "OK", "OK"),

    /**
     * The '<em><b>NOT OK</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #NOT_OK_VALUE
     * @generated
     * @ordered
     */
    NOT_OK(1, "NOT_OK", "NOT_OK"),

    /**
     * The '<em><b>UNKNOWN SVC</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #UNKNOWN_SVC_VALUE
     * @generated
     * @ordered
     */
    UNKNOWN_SVC(2, "UNKNOWN_SVC", "UNKNOWN_SVC"),

    /**
     * The '<em><b>UNKNOWN METHOD</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #UNKNOWN_METHOD_VALUE
     * @generated
     * @ordered
     */
    UNKNOWN_METHOD(3, "UNKNOWN_METHOD", "UNKNOWN_METHOD"),

    /**
     * The '<em><b>NOT READY</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #NOT_READY_VALUE
     * @generated
     * @ordered
     */
    NOT_READY(4, "NOT_READY", "NOT_READY"),

    /**
     * The '<em><b>NOT REACHABLE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #NOT_REACHABLE_VALUE
     * @generated
     * @ordered
     */
    NOT_REACHABLE(5, "NOT_REACHABLE", "NOT_REACHABLE"),

    /**
     * The '<em><b>TIMEOUT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TIMEOUT_VALUE
     * @generated
     * @ordered
     */
    TIMEOUT(6, "TIMEOUT", "TIMEOUT"),

    /**
     * The '<em><b>WRONG PROTOCOL VER</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #WRONG_PROTOCOL_VER_VALUE
     * @generated
     * @ordered
     */
    WRONG_PROTOCOL_VER(7, "WRONG_PROTOCOL_VER", "WRONG_PROTOCOL_VER"),

    /**
     * The '<em><b>WRONG INTERFACE VER</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #WRONG_INTERFACE_VER_VALUE
     * @generated
     * @ordered
     */
    WRONG_INTERFACE_VER(8, "WRONG_INTERFACE_VER", "WRONG_INTERFACE_VER"),

    /**
     * The '<em><b>MALFORMED MESSAGE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #MALFORMED_MESSAGE_VALUE
     * @generated
     * @ordered
     */
    MALFORMED_MESSAGE(9, "MALFORMED_MESSAGE", "MALFORMED_MESSAGE"),

    /**
     * The '<em><b>ALL</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ALL_VALUE
     * @generated
     * @ordered
     */
    ALL(10, "ALL", "ALL");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * The '<em><b>OK</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>OK</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #OK
     * @model
     * @generated
     * @ordered
     */
    public static final int OK_VALUE = 0;

    /**
     * The '<em><b>NOT OK</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>NOT OK</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #NOT_OK
     * @model
     * @generated
     * @ordered
     */
    public static final int NOT_OK_VALUE = 1;

    /**
     * The '<em><b>UNKNOWN SVC</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>UNKNOWN SVC</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #UNKNOWN_SVC
     * @model
     * @generated
     * @ordered
     */
    public static final int UNKNOWN_SVC_VALUE = 2;

    /**
     * The '<em><b>UNKNOWN METHOD</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>UNKNOWN METHOD</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #UNKNOWN_METHOD
     * @model
     * @generated
     * @ordered
     */
    public static final int UNKNOWN_METHOD_VALUE = 3;

    /**
     * The '<em><b>NOT READY</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>NOT READY</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #NOT_READY
     * @model
     * @generated
     * @ordered
     */
    public static final int NOT_READY_VALUE = 4;

    /**
     * The '<em><b>NOT REACHABLE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>NOT REACHABLE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #NOT_REACHABLE
     * @model
     * @generated
     * @ordered
     */
    public static final int NOT_REACHABLE_VALUE = 5;

    /**
     * The '<em><b>TIMEOUT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>TIMEOUT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TIMEOUT
     * @model
     * @generated
     * @ordered
     */
    public static final int TIMEOUT_VALUE = 6;

    /**
     * The '<em><b>WRONG PROTOCOL VER</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>WRONG PROTOCOL VER</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #WRONG_PROTOCOL_VER
     * @model
     * @generated
     * @ordered
     */
    public static final int WRONG_PROTOCOL_VER_VALUE = 7;

    /**
     * The '<em><b>WRONG INTERFACE VER</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>WRONG INTERFACE VER</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #WRONG_INTERFACE_VER
     * @model
     * @generated
     * @ordered
     */
    public static final int WRONG_INTERFACE_VER_VALUE = 8;

    /**
     * The '<em><b>MALFORMED MESSAGE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>MALFORMED MESSAGE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #MALFORMED_MESSAGE
     * @model
     * @generated
     * @ordered
     */
    public static final int MALFORMED_MESSAGE_VALUE = 9;

    /**
     * The '<em><b>ALL</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ALL</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ALL
     * @model
     * @generated
     * @ordered
     */
    public static final int ALL_VALUE = 10;

    /**
     * An array of all the '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final SomeIPReturnCodeOrTemplatePlaceholderEnum[] VALUES_ARRAY =
            new SomeIPReturnCodeOrTemplatePlaceholderEnum[] {
                    TEMPLATE_DEFINED,
                    OK,
                    NOT_OK,
                    UNKNOWN_SVC,
                    UNKNOWN_METHOD,
                    NOT_READY,
                    NOT_REACHABLE,
                    TIMEOUT,
                    WRONG_PROTOCOL_VER,
                    WRONG_INTERFACE_VER,
                    MALFORMED_MESSAGE,
                    ALL,
            };

    /**
     * A public read-only list of all the '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<SomeIPReturnCodeOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPReturnCodeOrTemplatePlaceholderEnum get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SomeIPReturnCodeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPReturnCodeOrTemplatePlaceholderEnum getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SomeIPReturnCodeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Some IP Return Code Or Template Placeholder Enum</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPReturnCodeOrTemplatePlaceholderEnum get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case OK_VALUE:
                return OK;
            case NOT_OK_VALUE:
                return NOT_OK;
            case UNKNOWN_SVC_VALUE:
                return UNKNOWN_SVC;
            case UNKNOWN_METHOD_VALUE:
                return UNKNOWN_METHOD;
            case NOT_READY_VALUE:
                return NOT_READY;
            case NOT_REACHABLE_VALUE:
                return NOT_REACHABLE;
            case TIMEOUT_VALUE:
                return TIMEOUT;
            case WRONG_PROTOCOL_VER_VALUE:
                return WRONG_PROTOCOL_VER;
            case WRONG_INTERFACE_VER_VALUE:
                return WRONG_INTERFACE_VER;
            case MALFORMED_MESSAGE_VALUE:
                return MALFORMED_MESSAGE;
            case ALL_VALUE:
                return ALL;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private SomeIPReturnCodeOrTemplatePlaceholderEnum(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //SomeIPReturnCodeOrTemplatePlaceholderEnum
