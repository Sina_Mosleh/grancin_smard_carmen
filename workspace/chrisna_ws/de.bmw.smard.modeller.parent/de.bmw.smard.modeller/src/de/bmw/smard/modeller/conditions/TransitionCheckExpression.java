package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.statemachine.Transition;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActive <em>Stay Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getCheckTransition <em>Check Transition</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActiveTmplParam <em>Stay Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTransitionCheckExpression()
 * @model
 * @generated
 */
public interface TransitionCheckExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Stay Active</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stay Active</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Stay Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #setStayActive(BooleanOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTransitionCheckExpression_StayActive()
     * @model default="false" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    BooleanOrTemplatePlaceholderEnum getStayActive();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActive <em>Stay Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Stay Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #getStayActive()
     * @generated
     */
    void setStayActive(BooleanOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Check Transition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Check Transition</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Check Transition</em>' reference.
     * @see #setCheckTransition(Transition)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTransitionCheckExpression_CheckTransition()
     * @model required="true"
     * @generated
     */
    Transition getCheckTransition();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getCheckTransition <em>Check Transition</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Check Transition</em>' reference.
     * @see #getCheckTransition()
     * @generated
     */
    void setCheckTransition(Transition value);

    /**
     * Returns the value of the '<em><b>Stay Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stay Active Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Stay Active Tmpl Param</em>' attribute.
     * @see #setStayActiveTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTransitionCheckExpression_StayActiveTmplParam()
     * @model
     * @generated
     */
    String getStayActiveTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActiveTmplParam <em>Stay Active Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Stay Active Tmpl Param</em>' attribute.
     * @see #getStayActiveTmplParam()
     * @generated
     */
    void setStayActiveTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStayActive(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidTransition(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TransitionCheckExpression
