package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.TimingExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timing Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl#getMaxtime <em>Maxtime</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl#getMintime <em>Mintime</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimingExpressionImpl extends ExpressionImpl implements TimingExpression {
    /**
     * The default value of the '{@link #getMaxtime() <em>Maxtime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMaxtime()
     * @generated
     * @ordered
     */
    protected static final String MAXTIME_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getMaxtime() <em>Maxtime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMaxtime()
     * @generated
     * @ordered
     */
    protected String maxtime = MAXTIME_EDEFAULT;

    /**
     * This is true if the Maxtime attribute has been set.
     * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean maxtimeESet;

    /**
     * The default value of the '{@link #getMintime() <em>Mintime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMintime()
     * @generated
     * @ordered
     */
    protected static final String MINTIME_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getMintime() <em>Mintime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMintime()
     * @generated
     * @ordered
     */
    protected String mintime = MINTIME_EDEFAULT;

    /**
     * This is true if the Mintime attribute has been set.
     * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean mintimeESet;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected TimingExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.TIMING_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMaxtime() {
        return maxtime;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMaxtime(String newMaxtime) {
        String oldMaxtime = maxtime;
        maxtime = newMaxtime;
        boolean oldMaxtimeESet = maxtimeESet;
        maxtimeESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TIMING_EXPRESSION__MAXTIME, oldMaxtime, maxtime, !oldMaxtimeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetMaxtime() {
        String oldMaxtime = maxtime;
        boolean oldMaxtimeESet = maxtimeESet;
        maxtime = MAXTIME_EDEFAULT;
        maxtimeESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.TIMING_EXPRESSION__MAXTIME, oldMaxtime, MAXTIME_EDEFAULT, oldMaxtimeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetMaxtime() {
        return maxtimeESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMintime() {
        return mintime;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMintime(String newMintime) {
        String oldMintime = mintime;
        mintime = newMintime;
        boolean oldMintimeESet = mintimeESet;
        mintimeESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TIMING_EXPRESSION__MINTIME, oldMintime, mintime, !oldMintimeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetMintime() {
        String oldMintime = mintime;
        boolean oldMintimeESet = mintimeESet;
        mintime = MINTIME_EDEFAULT;
        mintimeESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.TIMING_EXPRESSION__MINTIME, oldMintime, MINTIME_EDEFAULT, oldMintimeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetMintime() {
        return mintimeESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidMinMaxTimes(DiagnosticChain diagnostics, Map<Object, Object> context) {

        boolean hasValidMinMaxTimes = true;
        String messageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_Conditions_TimingExpression_MinMaxTime_IllegalMinMaxTime";
        List<String> errorMessages = new ArrayList<String>();
        String minTime = this.mintime;
        String maxTime = this.maxtime;
        if (minTime != null && minTime.length() > 0) {
            if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
                String errorMsg = TemplateUtils.checkLongNotPlaceholder(minTime, false);
                if (errorMsg != null) {
                    errorMessages.add("Min time: " + errorMsg + ".");
                    hasValidMinMaxTimes = false;
                } else {
                    Long t = TemplateUtils.getLongNotPlaceholder(minTime, false);
                    if (t < 0) {
                        errorMessages.add("Illegal min time lower zero");
                        hasValidMinMaxTimes = false;
                    }
                }
            } else {
                if (TemplateUtils.containsParameters(minTime)) {
                    String errorMessage = TemplateUtils.checkValidPlaceholderNameWithHashtags(minTime);
                    if (errorMessage != null) {
                        errorMessages.add(errorMessage);
                        hasValidMinMaxTimes = false;
                    }
                } else {
                    Long t = TemplateUtils.getLongNotPlaceholder(minTime, false);
                    if (t < 0) {
                        errorMessages.add("Illegal min time lower zero");
                        hasValidMinMaxTimes = false;
                    }
                }
            }
        } else { // minTime empty string -> leads to NPE in engine
            errorMessages.add("Min time may not be empty");
            hasValidMinMaxTimes = false;
        }

        if (maxTime != null && maxTime.length() > 0) {
            if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
                String errorMsg = TemplateUtils.checkLongNotPlaceholder(maxTime, false);
                if (errorMsg != null) {
                    errorMessages.add("Min time: " + errorMsg + ".");
                    hasValidMinMaxTimes = false;
                } else {
                    Long t = TemplateUtils.getLongNotPlaceholder(maxTime, false);
                    if (t < 0) {
                        errorMessages.add("Max time: " + errorMsg + ".");
                        hasValidMinMaxTimes = false;
                    }
                }
            } else {
                if (TemplateUtils.containsParameters(maxTime)) {
                    String errorMessage = TemplateUtils.checkValidPlaceholderNameWithHashtags(minTime);
                    if (errorMessage != null) {
                        errorMessages.add(errorMessage);
                        hasValidMinMaxTimes = false;
                    }
                } else {
                    Long t = TemplateUtils.getLongNotPlaceholder(maxTime, false);
                    if (t < 0) {
                        errorMessages.add("Illegal max time lower zero");
                        hasValidMinMaxTimes = false;
                    }
                }
            }
        } else { // maxTime empty string -> leads to NPE in engine
            errorMessages.add("Max time may not be empty");
            hasValidMinMaxTimes = false;
        }

        Long minTimeLong;
        Long maxTimeLong;
        try {
            minTimeLong = TemplateUtils.getLongNotPlaceholder(minTime, false);
        } catch (NumberFormatException e) {
            minTimeLong = null;
        }
        try {
            maxTimeLong = TemplateUtils.getLongNotPlaceholder(maxTime, false);
        } catch (NumberFormatException e) {
            maxTimeLong = null;
        }
        if ((minTimeLong == null || minTimeLong == 0) && (maxTimeLong == null || maxTimeLong == 0)) {
            if (!(TemplateUtils.containsParameters(minTime) || TemplateUtils.containsParameters(maxTime))) {
                hasValidMinMaxTimes = false;
                errorMessages.add("At least min time or max time must be > 0");
            }
        }
        if ((minTimeLong != null && minTimeLong != 0) && (maxTimeLong != null && maxTimeLong != 0)
            && minTimeLong > maxTimeLong) {
            hasValidMinMaxTimes = false;
            errorMessages.add("Min time must not be after max time in");
        }

        if (errorMessages != null && errorMessages.size() > 0) {
            messageToSubstitute = StringUtils.join(errorMessages, "; ");
        }

        if (!hasValidMinMaxTimes) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.TIMING_EXPRESSION__IS_VALID_HAS_VALID_MIN_MAX_TIMES)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                        .data(this)
                        .build());
            }
            return false;
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
                return getMaxtime();
            case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
                return getMintime();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
                setMaxtime((String) newValue);
                return;
            case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
                setMintime((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
                unsetMaxtime();
                return;
            case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
                unsetMintime();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TIMING_EXPRESSION__MAXTIME:
                return isSetMaxtime();
            case ConditionsPackage.TIMING_EXPRESSION__MINTIME:
                return isSetMintime();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (maxtime: ");
        if (maxtimeESet) result.append(maxtime);
        else result.append("<unset>");
        result.append(", mintime: ");
        if (mintimeESet) result.append(mintime);
        else result.append("<unset>");
        result.append(')');
        return result.toString();
    }

} // TimingExpressionImpl
