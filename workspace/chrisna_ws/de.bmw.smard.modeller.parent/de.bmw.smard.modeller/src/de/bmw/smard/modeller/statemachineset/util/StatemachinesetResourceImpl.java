/**
 * <copyright>
 * </copyright>
 * $Id$
 */
package de.bmw.smard.modeller.statemachineset.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachineset.util.StatemachinesetResourceFactoryImpl
 * @generated
 */
public class StatemachinesetResourceImpl extends XMIResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param uri the URI of the new resource.
     * @generated
     */
    public StatemachinesetResourceImpl(URI uri) {
        super(uri);
    }

} //StatemachinesetResourceImpl
