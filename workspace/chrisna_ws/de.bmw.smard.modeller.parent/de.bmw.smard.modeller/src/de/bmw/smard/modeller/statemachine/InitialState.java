package de.bmw.smard.modeller.statemachine;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getInitialState()
 * @model
 * @generated
 */
public interface InitialState extends AbstractState {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidNoInTransition(DiagnosticChain diagnostics, Map<Object, Object> context);
} // InitialState
