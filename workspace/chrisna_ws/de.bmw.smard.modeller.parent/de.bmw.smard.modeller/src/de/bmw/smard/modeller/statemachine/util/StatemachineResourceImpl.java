package de.bmw.smard.modeller.statemachine.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import java.util.Iterator;

/**
 * <!-- begin-user-doc --> The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.util.StatemachineResourceFactoryImpl
 * @generated
 */
public class StatemachineResourceImpl extends XMLResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param uri the URI of the new resource.
     * @generated
     */
    public StatemachineResourceImpl(URI uri) {
        super(uri);
    }

    // @generated NOT
    @Override
    public void attached(EObject eObject) {
        if (isAttachedDetachedHelperRequired()) {
            attachedHelper(eObject);
            for (Iterator tree = eObject.eAllContents(); tree.hasNext();) {
                attachedHelper((EObject) tree.next());
            }
        }
    }

    // @generated NOT
    @Override
    protected XMLHelper createXMLHelper() {
        return new StatemachineXMLHelperImpl(this);
    }

} // StatemachineResourceImpl
