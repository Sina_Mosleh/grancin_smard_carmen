package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ConstantComparatorValue;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl#getValue <em>Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl#getInterpretedValue <em>Interpreted Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantComparatorValueImpl extends ComparatorSignalImpl implements ConstantComparatorValue {
    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final String VALUE_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected String value = VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValue()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum INTERPRETED_VALUE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

    /**
     * The cached value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValue()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum interpretedValue = INTERPRETED_VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValueTmplParam()
     * @generated
     * @ordered
     */
    protected static final String INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValueTmplParam()
     * @generated
     * @ordered
     */
    protected String interpretedValueTmplParam = INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ConstantComparatorValueImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.CONSTANT_COMPARATOR_VALUE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValue(String newValue) {
        String oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getInterpretedValue() {
        return interpretedValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInterpretedValue(BooleanOrTemplatePlaceholderEnum newInterpretedValue) {
        BooleanOrTemplatePlaceholderEnum oldInterpretedValue = interpretedValue;
        interpretedValue = newInterpretedValue == null ? INTERPRETED_VALUE_EDEFAULT : newInterpretedValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE, oldInterpretedValue, interpretedValue));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInterpretedValueTmplParam() {
        return interpretedValueTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInterpretedValueTmplParam(String newInterpretedValueTmplParam) {
        String oldInterpretedValueTmplParam = interpretedValueTmplParam;
        interpretedValueTmplParam = newInterpretedValueTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM, oldInterpretedValueTmplParam, interpretedValueTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidInterpretedValue(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_INTERPRETED_VALUE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(interpretedValue)
                        .templateType(BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(interpretedValueTmplParam)
                        .containsParameterError("_Validation_Conditions_ConstantComparatorValue_InterpretedValue_ContainsPlaceholders")
                        .tmplParamIsNullError("_Validation_Conditions_ConstantComparatorValue_InterpretedValueTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_ConstantComparatorValue_InterpretedValueTmplParam_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_ConstantComparatorValue_InterpretedValue_IllegalInterpretedValue")
                        .build())

                .with(Validators.notNull(interpretedValue)
                        .error("_Validation_Conditions_ConstantComparatorValue_InterpretedValue_IsNull_")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.CONSTANT_COMPARATOR_VALUE__IS_VALID_HAS_VALID_VALUE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.numberTemplate(value)
                        .containsParameterError("_Validation_Conditions_ConstantComparatorValue_Value_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_ConstantComparatorValue_Value_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_ConstantComparatorValue_Value_IllegalValue")
                        .build())

                .with(Validators.notNull(value)
                        .error("_Validation_Conditions_ConstantComparatorValue_Value_IsNull")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
                return getValue();
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
                return getInterpretedValue();
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
                return getInterpretedValueTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
                setValue((String) newValue);
                return;
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
                setInterpretedValue((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
                setInterpretedValueTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
                setValue(VALUE_EDEFAULT);
                return;
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
                setInterpretedValue(INTERPRETED_VALUE_EDEFAULT);
                return;
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
                setInterpretedValueTmplParam(INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__VALUE:
                return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE:
                return interpretedValue != INTERPRETED_VALUE_EDEFAULT;
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM:
                return INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT == null ? interpretedValueTmplParam != null : !INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT
                        .equals(interpretedValueTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (value: ");
        result.append(value);
        result.append(", interpretedValue: ");
        result.append(interpretedValue);
        result.append(", interpretedValueTmplParam: ");
        result.append(interpretedValueTmplParam);
        result.append(')');
        return result.toString();
    }

    /** generated NOT */
    @Override
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

} //ConstantComparatorValueImpl
