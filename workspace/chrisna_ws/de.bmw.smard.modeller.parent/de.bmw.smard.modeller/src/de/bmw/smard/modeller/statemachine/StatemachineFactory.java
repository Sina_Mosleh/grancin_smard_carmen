package de.bmw.smard.modeller.statemachine;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage
 * @generated
 */
public interface StatemachineFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    StatemachineFactory eINSTANCE = de.bmw.smard.modeller.statemachine.impl.StatemachineFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Document Root</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Document Root</em>'.
     * @generated
     */
    DocumentRoot createDocumentRoot();

    /**
     * Returns a new object of class '<em>Transition</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Transition</em>'.
     * @generated
     */
    Transition createTransition();

    /**
     * Returns a new object of class '<em>State Machine</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>State Machine</em>'.
     * @generated
     */
    StateMachine createStateMachine();

    /**
     * Returns a new object of class '<em>Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Action</em>'.
     * @generated
     */
    Action createAction();

    /**
     * Returns a new object of class '<em>Initial State</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Initial State</em>'.
     * @generated
     */
    InitialState createInitialState();

    /**
     * Returns a new object of class '<em>State</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>State</em>'.
     * @generated
     */
    State createState();

    /**
     * Returns a new object of class '<em>Compute Variable</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Compute Variable</em>'.
     * @generated
     */
    ComputeVariable createComputeVariable();

    /**
     * Returns a new object of class '<em>Show Variable</em>'.
     * <!-- begin-user-doc -->
     * for Modeller functionality use custom_createShowVariable
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Show Variable</em>'.
     * @generated
     */
    ShowVariable createShowVariable();

    /**
     * Returns a new object of class '<em>Control Action</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Control Action</em>'.
     * @generated
     */
    ControlAction createControlAction();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    StatemachinePackage getStatemachinePackage();

} //StatemachineFactory
