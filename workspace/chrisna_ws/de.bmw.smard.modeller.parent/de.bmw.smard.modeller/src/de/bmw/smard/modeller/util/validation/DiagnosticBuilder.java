package de.bmw.smard.modeller.util.validation;

import com.google.common.base.Strings;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.ResourceLocator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagnosticBuilder {

    private Severity severity;
    private String source;
    private int code;
    private String messageId;
    private List<Object> data = new ArrayList<>();
    private String message;
    private List<Object> substititions = new ArrayList<>();
    private ResourceLocator resourceLocator;

    private DiagnosticBuilder() {
    }

    public static DiagnosticBuilder builder(Severity severity) {
        DiagnosticBuilder builder = new DiagnosticBuilder();
        builder.severity = severity;
        return builder;
    }

    public static DiagnosticBuilder info() {
        return builder(Severity.INFO);
    }

    public static DiagnosticBuilder error() {
        return builder(Severity.ERROR);
    }

    public static DiagnosticBuilder warn() {
        return builder(Severity.WARNING);
    }

    public DiagnosticBuilder source(String source) {
        this.source = source;
        return this;
    }

    public DiagnosticBuilder code(int code) {
        this.code = code;
        return this;
    }

    public DiagnosticBuilder messageId(ResourceLocator resourceLocator, String messageId, Object[] substitutions) {
        this.resourceLocator = resourceLocator;
        this.messageId = messageId;
        this.substititions.addAll(Arrays.asList(substitutions));
        return this;
    }

    public DiagnosticBuilder messageId(ResourceLocator resourceLocator, String messageId) {
        if(messageId != null) {
            this.resourceLocator = resourceLocator;
            this.messageId = messageId;
        }
        return this;
    }

    public DiagnosticBuilder messageId(ResourceLocator resourceLocator, String messageId, Object substitution) {
        this.resourceLocator = resourceLocator;
        this.messageId = messageId;
        if(substitution != null)
            this.substititions.add(substitution);
        return this;
    }

    public DiagnosticBuilder message(String message) {
        this.message = message;
        return this;
    }

    public DiagnosticBuilder data(Object[] data) {
        this.data.addAll(Arrays.asList(data));
        return this;
    }

    public DiagnosticBuilder data(Object data) {
        this.data.add(data);
        return this;
    }

    public BasicDiagnostic build() {

        if (!Strings.isNullOrEmpty(messageId)) {
            return new BasicDiagnostic(severity.value(), source, code, resourceLocator.getString(messageId, substititions.toArray()), data.toArray());
        } else {
            return new BasicDiagnostic(severity.value(), source, code, message, data.toArray());
        }
    }
}
