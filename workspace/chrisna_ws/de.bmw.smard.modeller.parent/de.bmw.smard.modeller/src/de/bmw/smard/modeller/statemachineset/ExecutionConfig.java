package de.bmw.smard.modeller.statemachineset;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execution Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsEthernet <em>Needs Ethernet</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsErrorFrames <em>Needs Error Frames</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getKeyValuePairs <em>Key Value Pairs</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getLogLevel <em>Log Level</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getSorterValue <em>Sorter Value</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig()
 * @model
 * @generated
 */
public interface ExecutionConfig extends EObject {
    /**
     * Returns the value of the '<em><b>Log Level</b></em>' attribute.
     * The default value is <code>"40"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Log Level</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Log Level</em>' attribute.
     * @see #setLogLevel(int)
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig_LogLevel()
     * @model default="40" required="true"
     * @generated
     */
    int getLogLevel();

    /**
     * @generated NOT
     */
    String getLogLevelAsString();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getLogLevel <em>Log Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Log Level</em>' attribute.
     * @see #getLogLevel()
     * @generated
     */
    void setLogLevel(int value);

    /**
     * Returns the value of the '<em><b>Sorter Value</b></em>' attribute.
     * The default value is <code>"10000"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sorter Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Sorter Value</em>' attribute.
     * @see #setSorterValue(int)
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig_SorterValue()
     * @model default="10000" required="true"
     * @generated
     */
    int getSorterValue();

    /**
     * @generated NOT
     */
    String getSorterValueAsString();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getSorterValue <em>Sorter Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Sorter Value</em>' attribute.
     * @see #getSorterValue()
     * @generated
     */
    void setSorterValue(int value);

    /**
     * Returns the value of the '<em><b>Key Value Pairs</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Key Value Pairs</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Key Value Pairs</em>' containment reference list.
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig_KeyValuePairs()
     * @model containment="true"
     * @generated
     */
    EList<KeyValuePairUserConfig> getKeyValuePairs();

    /**
     * @generated NOT
     * @param key
     * @return
     */
    KeyValuePairUserConfig getKeyValuePair(String key);

    /**
     * @generated NOT
     */
    boolean isKeyInKeyValuePairs(String key);

    /**
     * @generated NOT
     */
    void addKeyValuePair(String key, String value);

    /**
     * @generated NOT
     */
    void removeKeyValuePair(String key);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSorterValue(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * Returns the value of the '<em><b>Needs Ethernet</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Needs Ethernet</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Needs Ethernet</em>' attribute.
     * @see #setNeedsEthernet(boolean)
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig_NeedsEthernet()
     * @model default="false"
     * @generated
     */
    boolean isNeedsEthernet();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsEthernet <em>Needs Ethernet</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Needs Ethernet</em>' attribute.
     * @see #isNeedsEthernet()
     * @generated
     */
    void setNeedsEthernet(boolean value);

    /**
     * Returns the value of the '<em><b>Needs Error Frames</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Needs Error Frames</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Needs Error Frames</em>' attribute.
     * @see #setNeedsErrorFrames(boolean)
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getExecutionConfig_NeedsErrorFrames()
     * @model default="false"
     * @generated
     */
    boolean isNeedsErrorFrames();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsErrorFrames <em>Needs Error Frames</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Needs Error Frames</em>' attribute.
     * @see #isNeedsErrorFrames()
     * @generated
     */
    void setNeedsErrorFrames(boolean value);

} // ExecutionConfig
