package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DLT Message Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDLT_MessageType()
 * @model
 * @generated
 */
public enum DLT_MessageType implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>DLT TYPE LOG</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_LOG_VALUE
     * @generated
     * @ordered
     */
    DLT_TYPE_LOG(1, "DLT_TYPE_LOG", "DLT_TYPE_LOG"),

    /**
     * The '<em><b>DLT TYPE APP TRACE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_APP_TRACE_VALUE
     * @generated
     * @ordered
     */
    DLT_TYPE_APP_TRACE(2, "DLT_TYPE_APP_TRACE", "DLT_TYPE_APP_TRACE"),

    /**
     * The '<em><b>DLT TYPE NW TRACE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_NW_TRACE_VALUE
     * @generated
     * @ordered
     */
    DLT_TYPE_NW_TRACE(3, "DLT_TYPE_NW_TRACE", "DLT_TYPE_NW_TRACE"),

    /**
     * The '<em><b>DLT TYPE CONTROL</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_CONTROL_VALUE
     * @generated
     * @ordered
     */
    DLT_TYPE_CONTROL(4, "DLT_TYPE_CONTROL", "DLT_TYPE_CONTROL"),

    /**
     * The '<em><b>NOT SPECIFIED</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #NOT_SPECIFIED_VALUE
     * @generated
     * @ordered
     */
    NOT_SPECIFIED(0, "NOT_SPECIFIED", "NOT_SPECIFIED");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * The '<em><b>DLT TYPE LOG</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT TYPE LOG</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_LOG
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_TYPE_LOG_VALUE = 1;

    /**
     * The '<em><b>DLT TYPE APP TRACE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT TYPE APP TRACE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_APP_TRACE
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_TYPE_APP_TRACE_VALUE = 2;

    /**
     * The '<em><b>DLT TYPE NW TRACE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT TYPE NW TRACE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_NW_TRACE
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_TYPE_NW_TRACE_VALUE = 3;

    /**
     * The '<em><b>DLT TYPE CONTROL</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT TYPE CONTROL</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_TYPE_CONTROL
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_TYPE_CONTROL_VALUE = 4;

    /**
     * The '<em><b>NOT SPECIFIED</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>NOT SPECIFIED</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #NOT_SPECIFIED
     * @model
     * @generated
     * @ordered
     */
    public static final int NOT_SPECIFIED_VALUE = 0;

    /**
     * An array of all the '<em><b>DLT Message Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final DLT_MessageType[] VALUES_ARRAY =
            new DLT_MessageType[] {
                    TEMPLATE_DEFINED,
                    DLT_TYPE_LOG,
                    DLT_TYPE_APP_TRACE,
                    DLT_TYPE_NW_TRACE,
                    DLT_TYPE_CONTROL,
                    NOT_SPECIFIED,
            };

    /**
     * A public read-only list of all the '<em><b>DLT Message Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<DLT_MessageType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>DLT Message Type</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DLT_MessageType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>DLT Message Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DLT_MessageType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>DLT Message Type</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageType get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case DLT_TYPE_LOG_VALUE:
                return DLT_TYPE_LOG;
            case DLT_TYPE_APP_TRACE_VALUE:
                return DLT_TYPE_APP_TRACE;
            case DLT_TYPE_NW_TRACE_VALUE:
                return DLT_TYPE_NW_TRACE;
            case DLT_TYPE_CONTROL_VALUE:
                return DLT_TYPE_CONTROL;
            case NOT_SPECIFIED_VALUE:
                return NOT_SPECIFIED;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private DLT_MessageType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //DLT_MessageType
