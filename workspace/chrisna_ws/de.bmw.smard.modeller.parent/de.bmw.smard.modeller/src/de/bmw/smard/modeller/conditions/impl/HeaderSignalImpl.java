package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.HeaderSignal;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.conditions.util.HeaderSignalAttributeUtil;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Header Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl#getAttribute <em>Attribute</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl#getAttributeTmplParam <em>Attribute Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HeaderSignalImpl extends AbstractSignalImpl implements HeaderSignal {
    /**
     * The default value of the '{@link #getAttribute() <em>Attribute</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAttribute()
     * @generated
     * @ordered
     */
    protected static final String ATTRIBUTE_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getAttribute() <em>Attribute</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAttribute()
     * @generated
     * @ordered
     */
    protected String attribute = ATTRIBUTE_EDEFAULT;

    /**
     * The default value of the '{@link #getAttributeTmplParam() <em>Attribute Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAttributeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String ATTRIBUTE_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getAttributeTmplParam() <em>Attribute Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAttributeTmplParam()
     * @generated
     * @ordered
     */
    protected String attributeTmplParam = ATTRIBUTE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected HeaderSignalImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.HEADER_SIGNAL;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAttribute() {
        return attribute;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAttribute(String newAttribute) {
        String oldAttribute = attribute;
        attribute = newAttribute;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE, oldAttribute, attribute));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAttributeTmplParam() {
        return attributeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAttributeTmplParam(String newAttributeTmplParam) {
        String oldAttributeTmplParam = attributeTmplParam;
        attributeTmplParam = newAttributeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM, oldAttributeTmplParam, attributeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidAttribute(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValid_hasValidAttribute = true;
        String errorMessageIdentifier = "_Validation_Condition_HeaderSignal_IllegalAttribute";
        String errorMessageToSubstitute = null;

        AbstractMessage parentMessage = (AbstractMessage) this.eContainer();
        List<String> possibleAttributes = HeaderSignalAttributeUtil.getAttributesForMessage(parentMessage);
        if (!possibleAttributes.contains(attribute)) {
            isValid_hasValidAttribute = false;
            errorMessageToSubstitute = attribute + " is not a legal attribute for this HeaderSignal";
        }

        if (!TemplateUtils.getTemplateCategorization(this).isExportable()) { // if we are in template conditions file
            if (this.attribute.equals("TemplateDefined")) {
                if (this.attributeTmplParam == null) {
                    errorMessageIdentifier = "_Validation_Condition_HeaderSignal_AttributeTmplParam_IsNull";
                    isValid_hasValidAttribute = false;
                } else {
                    errorMessageToSubstitute = TemplateUtils.checkValidPlaceholderName(this.attributeTmplParam);
                    if (errorMessageToSubstitute != null && errorMessageToSubstitute.length() > 0) {
                        errorMessageIdentifier = "_Validation_Condition_HeaderSignal_AttributeTmplParam_NotAValidPlaceholderName";
                        isValid_hasValidAttribute = false;
                    }
                }
            }
        }

        if (!isValid_hasValidAttribute) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.HEADER_SIGNAL__IS_VALID_HAS_VALID_ATTRIBUTE)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, errorMessageToSubstitute)
                        .data(this)
                        .build());
            }
        }

        return isValid_hasValidAttribute;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        return HeaderSignalAttributeUtil.getDataType(getMessage(), getAttribute());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE:
                return getAttribute();
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM:
                return getAttributeTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE:
                setAttribute((String) newValue);
                return;
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM:
                setAttributeTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE:
                setAttribute(ATTRIBUTE_EDEFAULT);
                return;
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM:
                setAttributeTmplParam(ATTRIBUTE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE:
                return ATTRIBUTE_EDEFAULT == null ? attribute != null : !ATTRIBUTE_EDEFAULT.equals(attribute);
            case ConditionsPackage.HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM:
                return ATTRIBUTE_TMPL_PARAM_EDEFAULT == null ? attributeTmplParam != null : !ATTRIBUTE_TMPL_PARAM_EDEFAULT.equals(attributeTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (attribute: ");
        result.append(attribute);
        result.append(", attributeTmplParam: ");
        result.append(attributeTmplParam);
        result.append(')');
        return result.toString();
    }

} //HeaderSignalImpl
