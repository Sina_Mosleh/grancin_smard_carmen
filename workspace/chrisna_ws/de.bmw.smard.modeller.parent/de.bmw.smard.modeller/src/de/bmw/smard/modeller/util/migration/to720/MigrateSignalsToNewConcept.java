package de.bmw.smard.modeller.util.migration.to720;

import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.migration.Utils;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import java.text.MessageFormat;
import java.util.*;

public class MigrateSignalsToNewConcept extends CustomMigration {
	
	private static final Logger LOGGER = Logger.getLogger(MigrateSignalsToNewConcept.class);
	private static final String UDPNM_SOURCE_PORT = "30500";
	
	List<Instance> signalsToMigrate = new ArrayList<Instance>();
	HashMap<Instance, Instance> signalVariablesToUpdate = new HashMap<Instance, Instance>();
	HashMap<Instance, Instance> signalChecksToMigate = new HashMap<Instance, Instance>();
	
	List<Instance> messageChecksToMigrate = new ArrayList<Instance>();
	List<Instance> pluginChecksToMigrate = new ArrayList<Instance>();
	
	List<String> objectsWithIdToMigrate = Arrays.asList(
            "conditions.Condition", 
            "conditions.Expression", 
            "conditions.Variable", 
            "conditions.AbstractObserver", 
            "conditions.AbstractMessage",
            "conditions.AbstractSignal", 
            "conditions.AbstractFilter", 
            "statemachine.AbstractState", 
            "statemachine.Transition", 
            "statemachine.StateMachine",
            "statemachine.AbstractAction");
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		
		// make sure, there is an instance of SignalReferenceSet
		Instance conditionsDocument = model.getAllInstances("conditions.ConditionsDocument").get(0);
		if (null == getSignalReferenceSet(conditionsDocument)) {
				conditionsDocument.add("signalReferencesSet", model.newInstance("conditions.SignalReferenceSet") );		
		}
		
		// Signal - child of SignalReferenceSet
		signalsToMigrate = model.getAllInstances("conditions.Signal");
		
		// SignalVariables to migrate
		for(Instance signalVariable : model.getAllInstances("conditions.SignalVariable")) {
			Instance referencedSignal = signalVariable.get("signal");
			if(referencedSignal != null) {
				if(referencedSignal.instanceOf("conditions.Signal")) {
					signalVariablesToUpdate.put(signalVariable, referencedSignal);
				}
			}
		}
		
		// SignalReferences to migrate (handles like SignalVariables)
		for(Instance signalReference : model.getAllInstances("conditions.SignalReferenceUse")) {
			Instance referencedSignal = signalReference.get("signal");
			if(referencedSignal != null) {
				if(referencedSignal.instanceOf("conditions.Signal")) {
					signalVariablesToUpdate.put(signalReference, referencedSignal);
				}
			}
		}
		
		// SignalCheck
		for(Instance signalCheck : model.getAllInstances("conditions.SignalCheck")) {
			Instance referencedSignal = signalCheck.get("signal");
			if(referencedSignal != null) {
				if(referencedSignal.instanceOf("conditions.Signal")) {
					signalChecksToMigate.put(signalCheck, referencedSignal);
				}
			}
		}
		
		// migrate all MessageChecks derived from EthernetMessageCheckExpression
		messageChecksToMigrate.addAll(model.getAllInstances("conditions.EthernetMessageCheckExpression"));
		
		// PluginState-, PluginResultCheckExpression, PluginState-, PluginResultMessageCheckExpression 
		pluginChecksToMigrate.addAll(model.getAllInstances("conditions.PluginStateCheckExpression"));
		pluginChecksToMigrate.addAll(model.getAllInstances("conditions.PluginResultCheckExpression"));
		pluginChecksToMigrate.addAll(model.getAllInstances("conditions.PluginStateMessageCheckExpression"));
		pluginChecksToMigrate.addAll(model.getAllInstances("conditions.PluginResultMessageCheckExpression"));
		
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
		
		for(String object : objectsWithIdToMigrate) {
            migrateClassWithId(object, model);
        }
		
		// migrate Signals to AbstractMessage with appropriate AbstractFilters
		for(Instance signal : signalsToMigrate) {
			Instance abstractMessage = migrateSignalToAbstractMessage(signal, model, metamodel);	
			if(abstractMessage != null) {
				Instance signalReferenceSet = getSignalReferenceSet(signal);
				signalReferenceSet.add("messages", abstractMessage);
			}
			
			model.delete(signal);
		}
		
		// migrate xxxMessageCheckExpression to
		// MessageCheckExpression referencing AbstractMessage (with Filter(s) and potential Signal)
		for(Instance messageCheckExpression : messageChecksToMigrate) {
			migrateMessageCheckExpression(messageCheckExpression, model, metamodel);
		}
		
		for(Instance pluginCheckExpression : pluginChecksToMigrate) {
			Instance signalReferenceSet = getSignalReferenceSet(pluginCheckExpression);
			Instance pluginMessage = migratePluginCheckToPluginMessage(pluginCheckExpression, model, metamodel);
			if(pluginMessage != null) {
				signalReferenceSet.add("messages", pluginMessage);
			}
			
			model.delete(pluginCheckExpression);
		}
		
	}

	/**
	 * takes a Signal and converts it to the matching AbstractMessage implementation
	 * adds filters (especially primary filters) and potential signals 
	 * @param signal
	 * @param model
	 * @param metamodel
	 * @return
	 */
	private Instance migrateSignalToAbstractMessage(Instance signal, Model model, Metamodel metamodel) {
		
		Instance abstractMessage = null;
		Instance primaryFilter = null;
		Instance abstractSignal = null;
		
		String signalId = signal.get("id");
		String signalName = signal.get("name");
		
		String abstractMessageBusId = signal.get("busId");
		String abstractMessageBusIdTmplParam = signal.get("busIdTmplParam");
		String abstractMessageBusIdRange = signal.get("busIdRange");
		
		if(signal.instanceOf("conditions.CanSignal")) {
			
			abstractMessage = model.newInstance("conditions.CANMessage");
			
			abstractSignal = model.newInstance("conditions.DoubleSignal");
			abstractSignal = setBasicAttributesOfCanLinFlexRaySignal(abstractSignal, signal, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.CANFilter");
			primaryFilter.set("messageIdRange", signal.get("messageIdRange"));
			primaryFilter.set("frameId", signal.get("frameId"));
			primaryFilter.set("extIdentifier", signal.get("extIdentifier"));
			primaryFilter.set("extIdentifierTmplParam", signal.get("extIdentifierTmplParam"));
			primaryFilter.set("rxtxFlag", signal.get("rxtxFlag"));
			primaryFilter.set("rxtxFlagTmplParam", signal.get("rxtxFlagTmplParam"));
			
			abstractMessage.set("canFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
			
		} else if(signal.instanceOf("conditions.LinSignal")) {
			
			abstractMessage = model.newInstance("conditions.LINMessage");

			abstractSignal = model.newInstance("conditions.DoubleSignal");
			abstractSignal = setBasicAttributesOfCanLinFlexRaySignal(abstractSignal, signal, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.LINFilter");
			primaryFilter.set("messageIdRange", signal.get("messageIdRange"));
			primaryFilter.set("frameId", signal.get("frameId"));

			abstractMessage.set("linFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
			
		} else if(signal.instanceOf("conditions.FlexSignal")) {
			
			abstractMessage = model.newInstance("conditions.FlexRayMessage");
			
			abstractSignal = model.newInstance("conditions.DoubleSignal");
			abstractSignal = setBasicAttributesOfCanLinFlexRaySignal(abstractSignal, signal, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.FlexRayFilter");
			primaryFilter.set("messageIdRange", signal.get("messageIdRange"));
			primaryFilter.set("slotId", signal.get("slotId"));
			primaryFilter.set("cycleOffset", signal.get("cycleOffset"));
			primaryFilter.set("cycleRepetition", signal.get("cycleRepetition"));
			primaryFilter.set("channel", signal.get("channel"));
			primaryFilter.set("channelTmplParam", signal.get("channelTmplParam"));

			abstractMessage.set("flexRayFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
			
		} else if(signal.instanceOf("conditions.EthernetSignal")) {
			
			// SMARD-714 -> EthernetSignal not needed any more (was never used anyway)
			LOGGER.warn(MessageFormat.format("EthernetSignal will not be migrated.", MigrateSignalsToNewConcept.class));
			
		} else if(signal.instanceOf("conditions.DltSignal")) {
			
			abstractMessage = model.newInstance("conditions.VerboseDLTMessage");
			
			abstractSignal = model.newInstance("conditions.StringSignal");
			abstractSignal.set("decodeStrategy", model.newInstance("conditions.EmptyDecodeStrategy"));
			abstractSignal.set("extractStrategy", model.newInstance("conditions.DLTExtractStrategy"));
			
			primaryFilter = model.newInstance("conditions.DLTFilter");
			primaryFilter.set("ecuID_ECU", signal.get("ecuID_ECU"));
			primaryFilter.set("sessionID_SEID", signal.get("sessionID_SEID"));
			primaryFilter.set("applicationID_APID", signal.get("applicationID_APID"));
			primaryFilter.set("contextID_CTID", signal.get("contextID_CTID"));
			primaryFilter.set("messageType_MSTP", signal.get("messageType_MSTP"));
			primaryFilter.set("messageType_MSTPTmplParam", signal.get("messageType_MSTPTmplParam"));
			primaryFilter.set("messageLogInfo_MSLI", signal.get("messageLogInfo_MSLI"));
			primaryFilter.set("messageLogInfo_MSLITmplParam", signal.get("messageLogInfo_MSLITmplParam"));
			primaryFilter.set("messageTraceInfo_MSTI", signal.get("messageTraceInfo_MSTI"));
			primaryFilter.set("messageTraceInfo_MSTITmplParam", signal.get("messageTraceInfo_MSTITmplParam"));
			primaryFilter.set("messageBusInfo_MSBI", signal.get("messageBusInfo_MSBI"));
			primaryFilter.set("messageBusInfo_MSBITmplParam", signal.get("messageBusInfo_MSBITmplParam"));
			primaryFilter.set("messageControlInfo_MSCI", signal.get("messageControlInfo_MSCI"));
			primaryFilter.set("messageControlInfo_MSCITmplParam", signal.get("messageControlInfo_MSCITmplParam"));

			abstractMessage.set("dltFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
			
			// migrate SignalChecks (VerboseDLTSignals might be referenced in a SignalCheck -> will be
			// migrated to a MessageCheck of the DLTMessage)
			for(Instance signalCheck : signalChecksToMigate.keySet()) {
				if(signalChecksToMigate.get(signalCheck).equals(signal)) {			
					signalCheck.remove("signal", signal);
					
					Instance condition = signalCheck.getContainer();
					String expressionAttributeName = "expression";
					
					if(!condition.instanceOf("conditions.Condition")) {
						expressionAttributeName = "expressions";
					}
					condition.remove(expressionAttributeName, signalCheck);
					model.delete(signalCheck);
					
					Instance messageCheckExpression = model.newInstance("conditions.MessageCheckExpression");
					messageCheckExpression.set("message", abstractMessage);
					
					condition.add(expressionAttributeName, messageCheckExpression);
				}
			}
			
		} else if(signal.instanceOf("conditions.PluginResultSignal")) {
			abstractMessage = model.newInstance("conditions.PluginMessage");
			
			primaryFilter = model.newInstance("conditions.PluginFilter");
			primaryFilter.set("pluginName", signal.get("pluginName"));
			primaryFilter.set("pluginVersion", signal.get("pluginVersion"));
			
			abstractSignal = model.newInstance("conditions.PluginSignal");
			
			Instance decodeStrategy = model.newInstance("conditions.EmptyDecodeStrategy");
			Instance extractStrategy = model.newInstance("conditions.PluginResultExtractStrategy");
			
			extractStrategy.set("resultRange", signal.get("resultRange"));
			
			abstractSignal.set("decodeStrategy", decodeStrategy);
			abstractSignal.set("extractStrategy", extractStrategy);
			
			abstractMessage.set("pluginFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
			
		} else if(signal.instanceOf("conditions.PluginStateSignal")) {
			abstractMessage = model.newInstance("conditions.PluginMessage");
			
			primaryFilter = model.newInstance("conditions.PluginFilter");
			primaryFilter.set("pluginName", signal.get("pluginName"));
			primaryFilter.set("pluginVersion", signal.get("pluginVersion"));

			abstractSignal = model.newInstance("conditions.PluginSignal");
			
			Instance decodeStrategy = model.newInstance("conditions.EmptyDecodeStrategy");
			Instance extractStrategy = model.newInstance("conditions.PluginStateExtractStrategy");
			
			extractStrategy.set("states", signal.get("states"));
			extractStrategy.set("statesTmplParam", signal.get("statesTmplParam"));
			
			abstractSignal.set("decodeStrategy", decodeStrategy);
			abstractSignal.set("extractStrategy", extractStrategy);
			
			abstractMessage.set("pluginFilter", primaryFilter);
			abstractMessage.add("signals", abstractSignal);
		}
		
		if(abstractMessage != null && abstractSignal != null) {
			if(!abstractMessage.instanceOf("conditions.PluginMessage")) {
				abstractMessage.set("busId", abstractMessageBusId);
				abstractMessage.set("busIdTmplParam", abstractMessageBusIdTmplParam);
				abstractMessage.set("busIdRange", abstractMessageBusIdRange);
			}
			
			abstractSignal.set("id", signalId);
			abstractSignal.set("name", signalName);
		}
		

		// update references to the Signal
		for(Instance signalVariable : signalVariablesToUpdate.keySet()) {
			if(signalVariablesToUpdate.get(signalVariable).equals(signal)) {
				signalVariable.remove("signal", signal);
				
				if(abstractSignal != null) {
					signalVariable.set("signal", abstractSignal);
				} else {
					model.delete(signalVariable);
				}
			}
		}
		
		return abstractMessage;
	}
	
	
	private void migrateMessageCheckExpression(Instance messageCheckExpression, Model model, Metamodel metamodel) {
		Instance abstractMessage = null;
		Instance primaryFilter = null;
		
		Instance signalReferenceSet = getSignalReferenceSet(messageCheckExpression);
		
		Instance condition = messageCheckExpression.getContainer();

		if(messageCheckExpression.instanceOf("conditions.UDPMessageCheckExpression") &&
				!(messageCheckExpression.instanceOf("conditions.UdpNMMessageCheckExpression"))) {
			
			abstractMessage = model.newInstance("conditions.UDPMessage");
			abstractMessage = setBasicAttributesOfEthernetMessageCheckExpression(abstractMessage, messageCheckExpression, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.UDPFilter");
			primaryFilter.set("sourcePort", messageCheckExpression.get("sourcePort"));
			primaryFilter.set("destPort", messageCheckExpression.get("destPort"));
			primaryFilter.set("checksum", messageCheckExpression.get("checksum"));
			
			abstractMessage.set("udpFilter", primaryFilter);
			
			if(messageCheckExpression.get("typeL4").toString().equalsIgnoreCase("ALL")) {
				LOGGER.warn(MessageFormat.format("CheckType ALL no longer supported", MigrateSignalsToNewConcept.class));
			}
			
		} else if(messageCheckExpression.instanceOf("conditions.TCPMessageCheckExpression")) {
			abstractMessage = model.newInstance("conditions.TCPMessage");
			abstractMessage = setBasicAttributesOfEthernetMessageCheckExpression(abstractMessage, messageCheckExpression, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.TCPFilter");
			primaryFilter.set("sourcePort", messageCheckExpression.get("sourcePort"));
			primaryFilter.set("destPort", messageCheckExpression.get("destPort"));
			primaryFilter.set("sequenceNumber", messageCheckExpression.get("sequenceNumber"));
			primaryFilter.set("acknowledgementNumber", messageCheckExpression.get("acknowledgementNumber"));
			primaryFilter.set("tcpFlags", messageCheckExpression.get("tcpFlags"));
			primaryFilter.set("streamAnalysisFlags", messageCheckExpression.get("streamAnalysisFlags"));
			primaryFilter.set("streamAnalysisFlagsTmplParam", messageCheckExpression.get("streamAnalysisFlagsTmplParam"));
			primaryFilter.set("streamValidPayloadOffset", messageCheckExpression.get("streamValidPayloadOffset"));
			
			abstractMessage.set("tcpFilter", primaryFilter);
			
			if(messageCheckExpression.get("typeL4").toString().equalsIgnoreCase("ALL")) {
				LOGGER.warn(MessageFormat.format("CheckType ALL no longer supported", MigrateSignalsToNewConcept.class));
			}
			
		} else if(messageCheckExpression.instanceOf("conditions.UdpNMMessageCheckExpression")) {
			abstractMessage = model.newInstance("conditions.UDPNMMessage");
			abstractMessage = setBasicAttributesOfEthernetMessageCheckExpression(abstractMessage, messageCheckExpression, model, metamodel);
			
			primaryFilter = model.newInstance("conditions.UDPNMFilter");
			primaryFilter.set("sourceNodeIdentifier", messageCheckExpression.get("sourceNodeIdentifier"));
			primaryFilter.set("controlBitVector", messageCheckExpression.get("controlBitVector"));
			primaryFilter.set("pwfStatus", messageCheckExpression.get("pwfStatus"));
			primaryFilter.set("teilnetzStatus", messageCheckExpression.get("teilnetzStatus"));
			
			Instance udpFilter = model.newInstance("conditions.UDPFilter");

			Object sourcePort = messageCheckExpression.get("sourcePort");
			if(sourcePort instanceof String && !((String)sourcePort).isEmpty() ) {
				if ( !((String)sourcePort).equals( UDPNM_SOURCE_PORT ) ) {
					LOGGER.warn(MessageFormat.format("UDPNM Source Port different to "+UDPNM_SOURCE_PORT+" - overwritten", MigrateSignalsToNewConcept.class));
				}
			} else {
				LOGGER.info(MessageFormat.format("UDPNM Source Port empty - using "+UDPNM_SOURCE_PORT+" as default", MigrateSignalsToNewConcept.class));
			}

			udpFilter.set("sourcePort", UDPNM_SOURCE_PORT);
			udpFilter.set("destPort", messageCheckExpression.get("destPort"));
			udpFilter.set("checksum", messageCheckExpression.get("checksum"));
			
			abstractMessage.set("udpnmFilter", primaryFilter);
			abstractMessage.add("filters", udpFilter);
			
			if(messageCheckExpression.get("typeL5").toString().equalsIgnoreCase("ALL")) {
				LOGGER.warn(MessageFormat.format("CheckType ALL no longer supported", MigrateSignalsToNewConcept.class));
			}
		}
		
		if(abstractMessage != null) {
			signalReferenceSet.add("messages", abstractMessage);
			
			Instance genericMessageCheckExpression = model.newInstance("conditions.MessageCheckExpression");
			genericMessageCheckExpression.set("message", abstractMessage);
			condition.remove("expression", messageCheckExpression);
			condition.set("expression", genericMessageCheckExpression);
			
			model.delete(messageCheckExpression);
		}
		
	}
	
	/**
	 * 
	 * @param pluginCheckExpression
	 * @param model
	 * @param metamodel
	 * @return
	 */
	private Instance migratePluginCheckToPluginMessage(Instance pluginCheckExpression, Model model,
			Metamodel metamodel) {
		Instance pluginMessage = model.newInstance("conditions.PluginMessage");
		
		Instance pluginFilter = model.newInstance("conditions.PluginFilter");
		pluginFilter.set("pluginName", pluginCheckExpression.get("pluginName"));
		pluginFilter.set("pluginVersion", pluginCheckExpression.get("pluginVersion"));
		
		Instance pluginSignal = model.newInstance("conditions.PluginSignal");
		
		Instance decodeStrategy = model.newInstance("conditions.EmptyDecodeStrategy");
		pluginSignal.set("decodeStrategy", decodeStrategy);
		
		Instance extractStrategy = null;
		
		Instance newPluginCheckExpression = model.newInstance("conditions.PluginCheckExpression");
		
		EEnum evaluationBehaviourEnum = metamodel.getEEnum("conditions.EvaluationBehaviour");
		EEnumLiteral evaluationBehaviour = evaluationBehaviourEnum.getEEnumLiteral("passive");
		if(pluginCheckExpression.instanceOf("conditions.PluginStateCheckExpression")) {
			
			extractStrategy = model.newInstance("conditions.PluginStateExtractStrategy");
			extractStrategy.set("states", pluginCheckExpression.get("states"));
			extractStrategy.set("statesTmplParam", pluginCheckExpression.get("statesTmplParam"));
			extractStrategy.set("statesActive", pluginCheckExpression.get("statesActive"));
			extractStrategy.set("statesActiveTmplParam", pluginCheckExpression.get("statesActiveTmplParam"));
			
			evaluationBehaviour = evaluationBehaviourEnum.getEEnumLiteral("active");
			
		} else if(pluginCheckExpression.instanceOf("conditions.PluginResultCheckExpression")) {
			
			extractStrategy = model.newInstance("conditions.PluginResultExtractStrategy");
			evaluationBehaviour = evaluationBehaviourEnum.getEEnumLiteral("active");
			
			extractStrategy.set("resultRange", pluginCheckExpression.get("resultRange"));
			
		} else if(pluginCheckExpression.instanceOf("conditions.PluginStateMessageCheckExpression")) {
			
			extractStrategy = model.newInstance("conditions.PluginStateExtractStrategy");
			extractStrategy.set("states", pluginCheckExpression.get("states"));
			extractStrategy.set("statesTmplParam", pluginCheckExpression.get("statesTmplParam"));
			extractStrategy.set("statesActive", pluginCheckExpression.get("statesActive"));
			extractStrategy.set("statesActiveTmplParam", pluginCheckExpression.get("statesActiveTmplParam"));
			
		} else if(pluginCheckExpression.instanceOf("conditions.PluginResultMessageCheckExpression")) {
			
			extractStrategy = model.newInstance("conditions.PluginResultExtractStrategy");
			extractStrategy.set("resultRange", pluginCheckExpression.get("resultRange"));
			
		}
		pluginSignal.set("extractStrategy", extractStrategy);
		
		pluginMessage.set("pluginFilter", pluginFilter);
		pluginMessage.add("signals", pluginSignal);
		
		newPluginCheckExpression.set("evaluationBehaviour", evaluationBehaviour);
		newPluginCheckExpression.set("signalToCheck", pluginSignal);
		
		Instance condition = pluginCheckExpression.getContainer();
		condition.remove("expression", pluginCheckExpression);
		try {
			if(condition.instanceOf("conditions.Condition") || condition.instanceOf("conditions.NotExpression")) {
				condition.set("expression", newPluginCheckExpression);
			} else {
				condition.add("expressions", newPluginCheckExpression);
			}
		} catch(Exception e) {
			// parent of expression is not a condition
			LOGGER.error(new MessageFormat("parent of PluginCheckExpression is not a condition"), e);
		}
		return pluginMessage;
	}

	/**
	 * since Can-, Lin- and FlexRaySignals extend NumericSignal, they share
	 * basic attributes, that can be migrated just the same for all of them
	 * @param abstractSignal
	 * @param signal
	 * @param model
	 * @param metamodel
	 * @return
	 */
	private Instance setBasicAttributesOfCanLinFlexRaySignal(Instance abstractSignal, Instance signal, 
			Model model, Metamodel metamodel) {
		
		Instance decodeStrategy = model.newInstance("conditions.DoubleDecodeStrategy");
		decodeStrategy.set("factor", signal.get("signalFactor"));
		decodeStrategy.set("offset", signal.get("signalOffset"));
		
		Instance extractStrategy = model.newInstance("conditions.UniversalPayloadWithLegacyExtractStrategy");
		extractStrategy.set("byteOrder", signal.get("byteOrder"));
		extractStrategy.set("startBit", signal.get("startbit"));
		extractStrategy.set("length", signal.get("length"));
		extractStrategy.set("extractionRule", createExtractionRule(signal));
		
		// migrate DataType
		extractStrategy.set("signalDataType", convertSignalDataTypeToAutosar(signal, metamodel));
		
		abstractSignal.set("decodeStrategy", decodeStrategy);
		abstractSignal.set("extractStrategy", extractStrategy);
		
		return abstractSignal;
	}
	
	/**
	 * converts dataType of Signal to autosarDataType of DoubleSignal 
	 * taking the length of Signal into consideration (the AUTOSARDataType
	 * must be "big" enough to contain the length, that should be extracted)
	 * @param signal
	 * @param metamodel
	 * @return
	 */
	private EEnumLiteral convertSignalDataTypeToAutosar(Instance signal, Metamodel metamodel) {
		EEnum signalDataType = metamodel.getEEnum("conditions.SignalDataType");
		EEnum autosarDataType = metamodel.getEEnum("conditions.AUTOSARDataType");
		
		EEnumLiteral newDataType = null;
		
		// the AUTOSARDataType must be able to contain the data, so
		// the length determines, if we need (U)Int8, -16, -32 or -64
		String lengthString = signal.get("length");
		int length = 0; 
		if(TemplateUtils.isPlaceholder(lengthString)) {
			newDataType = autosarDataType.getEEnumLiteral("TemplateDefined");
			return newDataType;
		} else {
			length = Integer.parseInt(lengthString); // else parse length to int
		}
		
		EEnumLiteral oldDataType = signal.get("signalDataType");
		
		String autosarString = "";
		
		// if the SignalDataType was unsigned, the AUTOSARDataType starts with A_UINT
		// and signed results in A_INT accordingly
		if(oldDataType.equals(signalDataType.getEEnumLiteral("UInt"))) {
			autosarString = "A_UINT";
		} else if(oldDataType.equals(signalDataType.getEEnumLiteral("Int"))) {
			autosarString = "A_INT";
		} else {
			// if SignalDataType was neither Int nor UInt, it was TemplateDefined
			newDataType = autosarDataType.getEEnumLiteral("TemplateDefined");
			return newDataType;
		}
		
		if(length <= 8) {
			autosarString = autosarString.concat("8");
		} else if(length > 8 && length <= 16) {
			autosarString = autosarString.concat("16");
		} else if(length > 16 && length <= 32) {
			autosarString = autosarString.concat("32");
		} else {
			autosarString = autosarString.concat("64");
		}
		
		newDataType = autosarDataType.getEEnumLiteral(autosarString);
		return newDataType;
	}
	
	/**
	 * takes an AbstractMessage and migrates the basic attributes from a EthernetMessageCheckExpression
	 * @param abstractMessage
	 * @param messageCheckExpression
	 * @param model
	 * @param metamodel
	 * @return
	 */
	private Instance setBasicAttributesOfEthernetMessageCheckExpression(Instance abstractMessage, Instance messageCheckExpression,
			Model model, Metamodel metamodel) {
		abstractMessage.set("busId", messageCheckExpression.get("busId"));
		abstractMessage.set("busIdTmplParam", messageCheckExpression.get("busIdTmplParam"));
		
		// EthernetFilter
		Instance ethernetFilter = model.newInstance("conditions.EthernetFilter");
		Utils.setIfNotNull(messageCheckExpression,"destMAC",ethernetFilter,"destMAC");
		Utils.setIfNotNull(messageCheckExpression,"sourceMAC",ethernetFilter,"sourceMAC");
		Utils.setIfNotNull(messageCheckExpression,"etherType",ethernetFilter,"etherType");
		Utils.setIfNotNull(messageCheckExpression,"InnerVlanId",ethernetFilter,"innerVlanId");
		Utils.setIfNotNull(messageCheckExpression,"OuterVlanId",ethernetFilter,"outerVlanId");
		Utils.setIfNotNull(messageCheckExpression,"InnerVlanCFI",ethernetFilter,"innerVlanCFI");
		Utils.setIfNotNull(messageCheckExpression,"OuterVlanCFI",ethernetFilter,"outerVlanCFI");
		Utils.setIfNotNull(messageCheckExpression,"InnerVlanVID",ethernetFilter,"innerVlanVID");
		Utils.setIfNotNull(messageCheckExpression,"OuterVlanVID",ethernetFilter,"outerVlanVID");
		Utils.setIfNotNull(messageCheckExpression,"CRC",ethernetFilter,"CRC");
		Utils.setIfNotNull(messageCheckExpression,"InnerVlanTPID",ethernetFilter,"innerVlanTPID");
		Utils.setIfNotNull(messageCheckExpression,"OuterVlanTPID",ethernetFilter,"outerVlanTPID");

		if(messageCheckExpression.get("typeL2").toString().equalsIgnoreCase("ALL")) {
			LOGGER.warn(MessageFormat.format("CheckType ALL no longer supported", MigrateSignalsToNewConcept.class));
		}
		
		// IPv4Filter
		Instance ipv4Filter = model.newInstance("conditions.IPv4Filter");
		Utils.setIfNotNull(messageCheckExpression,"sourceIP",ipv4Filter,"sourceIP");
		Utils.setIfNotNull(messageCheckExpression,"destIP",ipv4Filter,"destIP");
		Utils.setIfNotNull(messageCheckExpression,"timeToLive",ipv4Filter,"timeToLive");
		
		EEnum protocolType = metamodel.getEEnum("conditions.ProtocolTypeOrTemplatePlaceholderEnum");
		
		if(messageCheckExpression.instanceOf("conditions.TCPMessageCheckExpression")) {
			ipv4Filter.set("protocolType", protocolType.getEEnumLiteral("TCP"));
		} else {
			ipv4Filter.set("protocolType", protocolType.getEEnumLiteral("UDP"));
		}
		
		if(messageCheckExpression.get("typeL2").toString().equalsIgnoreCase("ALL")) {
			LOGGER.warn(MessageFormat.format("CheckType ALL no longer supported", MigrateSignalsToNewConcept.class));
		}
		
		// attaching filters to message
		abstractMessage.add("filters", ethernetFilter);
		abstractMessage.add("filters", ipv4Filter);
		return abstractMessage;
	}
	
	/**
	 * created the extractionRule from startBit and length:
	 * extractionRule := startBit, extract(length)
	 * @param signal
	 * @return
	 */
	private String createExtractionRule(Instance signal) {
		String startBit = signal.get("startbit");
		String length = signal.get("length");
		
		if(TemplateUtils.isPlaceholder(startBit) || TemplateUtils.isPlaceholder(length)) {
			return "";
		}
		
		return startBit + ",extract(" + length + ")";
	}
	
	/**
	 * algorithm to find the root of the document "ConditionDocument"
	 * from which you can find the SignalReferenceSet
	 * @return
	 */
	private Instance getSignalReferenceSet(Instance instance) {
		Instance documentRoot = instance;
		documentRoot = instance.getResource().getRootInstances().get(0);	
		Instance signalReferenceSet = documentRoot.get("signalReferencesSet");
		
		return signalReferenceSet;
	}
	
	/**
	 * classes with id as attribute will be migrated to have a super class
	 * BaseClassWithId 
	 * the original UUID has a prefix (c for condition, f for filter, etc)
	 * get rid of the prefix, only keep UUID 
	 * @param className
	 * @param model
	 */
	private void migrateClassWithId(String className, Model model) {
		List<Instance> objectsToMigrate = model.getAllInstances(className);
		
		for(Instance instance : objectsToMigrate) {
			String uuid = instance.get("id");
			String newUuid = "";
			
			if(uuid != null && uuid.contains(".")) {
				String[] splitUuid = uuid.split("\\.");
				if(splitUuid.length <= 0) {
					newUuid = UUID.randomUUID().toString();
				} else if(splitUuid.length > 0) {
					newUuid = splitUuid[1];
				}
			} else {
				LOGGER.warn(MessageFormat.format("uuid is " + uuid + " for " + instance.toString(), MigrateSignalsToNewConcept.class));
				newUuid = UUID.randomUUID().toString();
			}
			instance.set("id", newUuid);
		}
	}
}
