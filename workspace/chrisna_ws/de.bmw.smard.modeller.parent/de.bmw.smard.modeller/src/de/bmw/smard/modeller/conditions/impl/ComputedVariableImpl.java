package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.math.Assignment;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.ComputedVariable;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IComputeVariableActionOperand;
import de.bmw.smard.modeller.conditions.VariableReference;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.util.ActionExpressionUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Computed Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl#getOperands <em>Operands</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputedVariableImpl extends VariableImpl implements ComputedVariable {
    /**
     * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperands()
     * @generated
     * @ordered
     */
    protected EList<IComputeVariableActionOperand> operands;

    /**
     * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpression()
     * @generated
     * @ordered
     */
    protected static final String EXPRESSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpression()
     * @generated
     * @ordered
     */
    protected String expression = EXPRESSION_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ComputedVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.COMPUTED_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<IComputeVariableActionOperand> getOperands() {
        if (operands == null) {
            operands =
                    new EObjectContainmentEList<IComputeVariableActionOperand>(IComputeVariableActionOperand.class, this, ConditionsPackage.COMPUTED_VARIABLE__OPERANDS);
        }
        return operands;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getExpression() {
        return expression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExpression(String newExpression) {
        String oldExpression = expression;
        expression = newExpression;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.COMPUTED_VARIABLE__EXPRESSION, oldExpression, expression));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isValid_hasValidExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidExpression = true;
        String errorMessageIdentifier = "_Validation_Conditions_ComputedVariable_Expression";
        String messageToSubstitute = null;

        if (!StringUtils.isBlank(expression)) {

            // if TemplateContext, assume everything is valid -> generated Statemachines will be validated then
            if (!TemplateUtils.getTemplateCategorization(this).isExportable()) {
                messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(expression);
                if (StringUtils.isBlank(messageToSubstitute)) {
                    return true;
                }
            }

            Term term = null;
            try {
                // parse StringLiterals for ComputedVariable if target is STRING
                term = MathParser.parse(expression, operands, dataType == DataType.STRING);
            } catch (ActionExpressionParseException parseExc) {
                messageToSubstitute = parseExc.getMessage();
                hasValidExpression = false;
            }

            if (hasValidExpression && term == null) {
                messageToSubstitute = "missing action expression. (Use \"OP1\",\"OP2\", ... to reference child operators.)";
                hasValidExpression = false;
            } else if (term != null) {
                if (term instanceof Assignment) {
                    errorMessageIdentifier = "_Validation_Conditions_ComputedVariable_ExpressionAssignmentNotAllowed";
                    messageToSubstitute = expression;
                    hasValidExpression = false;
                } else if (!ActionExpressionUtils.isValidTermWithResolvedVariableReferences(term, Collections.emptyMap())) {
                    errorMessageIdentifier = "_Validation_global_Action_ActionExpression_ComputeVariable_StringConcat_NoPlusOperator";
                    messageToSubstitute = expression;
                    hasValidExpression = false;
                }
            }
        } else {
            errorMessageIdentifier = "_Validation_Conditions_ComputedVariable_ExpressionNullOrEmpty";
            hasValidExpression = false;
        }

        if (!hasValidExpression && diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.error()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.COMPUTED_VARIABLE__IS_VALID_HAS_VALID_EXPRESSION)
                    .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                    .data(this)
                    .build());
        }
        return hasValidExpression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.COMPUTED_VARIABLE__IS_VALID_HAS_VALID_OPERANDS)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(invalidOperands())
                        .error("_Validation_Conditions_ComputedVariable_operands",
                                "One or more operands have different DataType than Variable.")
                        .build())

                .with(Validators.when(hasCyclicReference(this, new BasicEList<String>()))
                        .error("_Validation_Conditions_ComputedVariable_operands",
                                "There are cyclic references in Operands.")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>(super.getReadVariables());
        result.addAll(ActionExpressionUtils.extractReadVariableNames(expression, operands, true));
        return result;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        if (StringUtils.isNotBlank(getName())) {
            return ECollections.singletonEList(getName());
        }
        return ECollections.emptyEList();
    }

    /**
     * @return
     * @generated NOT but added
     */
    private boolean invalidOperands() {
        // Computed Double Variables can only have Double children
        if (dataType == DataType.DOUBLE) {
            if (operands != null) {
                for (IComputeVariableActionOperand op : operands) {
                    if (op.get_EvaluationDataType() != dataType) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param computedVariable
     * @return
     * @generated NOT but added
     */
    private boolean hasCyclicReference(ComputedVariable computedVariable, EList<String> referencedVarNames) {
        referencedVarNames.addAll(computedVariable.getReadVariables());
        if (referencedVarNames.contains(computedVariable.getName())
            || referencedVarNames.contains(getName())) {
            return true;
        }

        for (IComputeVariableActionOperand operand : getOperands()) {
            if (operand instanceof VariableReference) {
                VariableReference child = (VariableReference) operand;
                if (child.getVariable() instanceof ComputedVariable) {
                    if (hasCyclicReference((ComputedVariable) child.getVariable(), referencedVarNames)) {
                        return true;
                    }
                }
            } else {
                for (String referencedVariable : operand.getReadVariables()) {
                    if (referencedVarNames.contains(referencedVariable)) {
                        return true;
                    } else {
                        referencedVarNames.add(referencedVariable);
                    }
                }
            }
        }
        return false;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.COMPUTED_VARIABLE__OPERANDS:
                return ((InternalEList<?>) getOperands()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.COMPUTED_VARIABLE__OPERANDS:
                return getOperands();
            case ConditionsPackage.COMPUTED_VARIABLE__EXPRESSION:
                return getExpression();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.COMPUTED_VARIABLE__OPERANDS:
                getOperands().clear();
                getOperands().addAll((Collection<? extends IComputeVariableActionOperand>) newValue);
                return;
            case ConditionsPackage.COMPUTED_VARIABLE__EXPRESSION:
                setExpression((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.COMPUTED_VARIABLE__OPERANDS:
                getOperands().clear();
                return;
            case ConditionsPackage.COMPUTED_VARIABLE__EXPRESSION:
                setExpression(EXPRESSION_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.COMPUTED_VARIABLE__OPERANDS:
                return operands != null && !operands.isEmpty();
            case ConditionsPackage.COMPUTED_VARIABLE__EXPRESSION:
                return EXPRESSION_EDEFAULT == null ? expression != null : !EXPRESSION_EDEFAULT.equals(expression);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (expression: ");
        result.append(expression);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT but added
     */
    @Override
    public EList<AbstractObserver> getObservers() {
        return ECollections.emptyEList();
    }

} //ComputedVariableImpl
