package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.FormatDataType;
import de.bmw.smard.modeller.conditions.VariableFormat;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Format</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VariableFormatImpl#getDigits <em>Digits</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VariableFormatImpl#getBaseDataType <em>Base Data Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VariableFormatImpl#isUpperCase <em>Upper Case</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableFormatImpl extends EObjectImpl implements VariableFormat {
    /**
     * The default value of the '{@link #getDigits() <em>Digits</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated
     * @ordered
     */
    protected static final int DIGITS_EDEFAULT = 0;

    /**
     * The max value of the '{@link #getDigits() <em>Digits</em>}' attribute if the DataType is Float/Round/Ceil.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated NOT
     * @ordered
     */
    public static final int MAX_DOUBLE_DIGITS = 16;

    /**
     * The max value of the '{@link #getDigits() <em>Digits</em>}' attribute if the DataType is BIN.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated NOT
     * @ordered
     */
    public static final int MAX_BIN_DIGITS = 54;

    /**
     * The max value of the '{@link #getDigits() <em>Digits</em>}' attribute if the DataType is OCT.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated NOT
     * @ordered
     */
    public static final int MAX_OCT_DIGITS = 52;

    /**
     * The max value of the '{@link #getDigits() <em>Digits</em>}' attribute if the DataType is DEC.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated NOT
     * @ordered
     */
    public static final int MAX_DEC_DIGITS = 18;

    /**
     * The max value of the '{@link #getDigits() <em>Digits</em>}' attribute if the DataType is HEX.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated NOT
     * @ordered
     */
    public static final int MAX_HEX_DIGITS = 53;

    /**
     * The cached value of the '{@link #getDigits() <em>Digits</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDigits()
     * @generated
     * @ordered
     */
    protected int digits = DIGITS_EDEFAULT;

    /**
     * The default value of the '{@link #getBaseDataType() <em>Base Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBaseDataType()
     * @generated
     * @ordered
     */
    protected static final FormatDataType BASE_DATA_TYPE_EDEFAULT = FormatDataType.FLOAT;

    /**
     * The cached value of the '{@link #getBaseDataType() <em>Base Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBaseDataType()
     * @generated
     * @ordered
     */
    protected FormatDataType baseDataType = BASE_DATA_TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #isUpperCase() <em>Upper Case</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isUpperCase()
     * @generated
     * @ordered
     */
    protected static final boolean UPPER_CASE_EDEFAULT = false;

    /**
     * The cached value of the '{@link #isUpperCase() <em>Upper Case</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isUpperCase()
     * @generated
     * @ordered
     */
    protected boolean upperCase = UPPER_CASE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public VariableFormatImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VARIABLE_FORMAT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getDigits() {
        return digits;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDigits(int newDigits) {
        int oldDigits = digits;
        digits = newDigits;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__DIGITS, oldDigits, digits));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FormatDataType getBaseDataType() {
        return baseDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBaseDataType(FormatDataType newBaseDataType) {
        FormatDataType oldBaseDataType = baseDataType;
        baseDataType = newBaseDataType == null ? BASE_DATA_TYPE_EDEFAULT : newBaseDataType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE, oldBaseDataType, baseDataType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isUpperCase() {
        return upperCase;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUpperCase(boolean newUpperCase) {
        boolean oldUpperCase = upperCase;
        upperCase = newUpperCase;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE, oldUpperCase, upperCase));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isValid_hasValidDigits(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.VARIABLE_FORMAT__IS_VALID_HAS_VALID_DIGITS)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(!validateDigits())
                        .error("_Validation_Conditions_Variable_Format_Digits", "VariableFormat: Digits not valid " + digits)
                        .build())
                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    private boolean validateDigits() {
        if (digits < 0)
            return false;
        if (baseDataType == FormatDataType.HEX)
            if (digits > MAX_HEX_DIGITS)
                return false;

        if (baseDataType == FormatDataType.BIN)
            if (digits > MAX_BIN_DIGITS)
                return false;

        if (baseDataType == FormatDataType.OCT)
            if (digits > MAX_OCT_DIGITS)
                return false;

        if (baseDataType == FormatDataType.DEC)
            if (digits > MAX_DEC_DIGITS)
                return false;

        if (baseDataType == FormatDataType.FLOAT || baseDataType == FormatDataType.ROUND || baseDataType == FormatDataType.CEIL)
            if (digits > MAX_DOUBLE_DIGITS)
                return false;

        if (baseDataType == FormatDataType.BOOL || baseDataType == FormatDataType.HHMMSS
            || baseDataType == FormatDataType.MS || baseDataType == FormatDataType.IP) {
            if (digits > 0)
                return false;
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
                return getDigits();
            case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
                return getBaseDataType();
            case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
                return isUpperCase();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
                setDigits((Integer) newValue);
                return;
            case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
                setBaseDataType((FormatDataType) newValue);
                return;
            case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
                setUpperCase((Boolean) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
                setDigits(DIGITS_EDEFAULT);
                return;
            case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
                setBaseDataType(BASE_DATA_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
                setUpperCase(UPPER_CASE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_FORMAT__DIGITS:
                return digits != DIGITS_EDEFAULT;
            case ConditionsPackage.VARIABLE_FORMAT__BASE_DATA_TYPE:
                return baseDataType != BASE_DATA_TYPE_EDEFAULT;
            case ConditionsPackage.VARIABLE_FORMAT__UPPER_CASE:
                return upperCase != UPPER_CASE_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (digits: ");
        result.append(digits);
        result.append(", baseDataType: ");
        result.append(baseDataType);
        result.append(", upperCase: ");
        result.append(upperCase);
        result.append(')');
        return result.toString();
    }

} //VariableFormatImpl
