package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Matches</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.Matches#getStringToCheck <em>String To Check</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getMatches()
 * @model
 * @generated
 */
public interface Matches extends StringExpression, RegexOperation {
    /**
     * Returns the value of the '<em><b>String To Check</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>String To Check</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>String To Check</em>' containment reference.
     * @see #setStringToCheck(IStringOperand)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getMatches_StringToCheck()
     * @model containment="true"
     * @generated
     */
    IStringOperand getStringToCheck();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.Matches#getStringToCheck <em>String To Check</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>String To Check</em>' containment reference.
     * @see #getStringToCheck()
     * @generated
     */
    void setStringToCheck(IStringOperand value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStringToCheck(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Matches
