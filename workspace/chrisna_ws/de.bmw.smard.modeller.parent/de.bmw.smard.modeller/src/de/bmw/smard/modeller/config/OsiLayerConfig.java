package de.bmw.smard.modeller.config;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.bmw.smard.common.interfaces.bus.BusType;
import de.bmw.smard.common.interfaces.bus.MessageType;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.CANFilter;
import de.bmw.smard.modeller.conditions.CANMessage;
import de.bmw.smard.modeller.conditions.DLTFilter;
import de.bmw.smard.modeller.conditions.DLTMessage;
import de.bmw.smard.modeller.conditions.EthernetFilter;
import de.bmw.smard.modeller.conditions.EthernetMessage;
import de.bmw.smard.modeller.conditions.FlexRayFilter;
import de.bmw.smard.modeller.conditions.FlexRayMessage;
import de.bmw.smard.modeller.conditions.IPv4Filter;
import de.bmw.smard.modeller.conditions.IPv4Message;
import de.bmw.smard.modeller.conditions.LINFilter;
import de.bmw.smard.modeller.conditions.LINMessage;
import de.bmw.smard.modeller.conditions.NonVerboseDLTFilter;
import de.bmw.smard.modeller.conditions.NonVerboseDLTMessage;
import de.bmw.smard.modeller.conditions.PayloadFilter;
import de.bmw.smard.modeller.conditions.PluginFilter;
import de.bmw.smard.modeller.conditions.PluginMessage;
import de.bmw.smard.modeller.conditions.SomeIPFilter;
import de.bmw.smard.modeller.conditions.SomeIPMessage;
import de.bmw.smard.modeller.conditions.SomeIPSDFilter;
import de.bmw.smard.modeller.conditions.SomeIPSDMessage;
import de.bmw.smard.modeller.conditions.TCPFilter;
import de.bmw.smard.modeller.conditions.TCPMessage;
import de.bmw.smard.modeller.conditions.UDPFilter;
import de.bmw.smard.modeller.conditions.UDPMessage;
import de.bmw.smard.modeller.conditions.UDPNMFilter;
import de.bmw.smard.modeller.conditions.UDPNMMessage;
import de.bmw.smard.modeller.conditions.VerboseDLTMessage;
import de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter;


public class OsiLayerConfig {
	
	//todo maybe these layers should reside in MessageType?
	private static final List<Set<MessageType>> layers;

	static {
		List<Set<MessageType>> intermediateLayerList = new ArrayList<>();
		
		Set<MessageType> aLayer = null;
		
		//TODO plugin
		
		// lowest
		aLayer = new HashSet<>();
		aLayer.add(MessageType.L2_ETHERNET);
		aLayer.add(MessageType.CAN);
		aLayer.add(MessageType.LIN);
		aLayer.add(MessageType.FLEXRAY);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );
		
		aLayer = new HashSet<>();
		aLayer.add(MessageType.L3_IPV4);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );

		aLayer = new HashSet<>();
		aLayer.add(MessageType.L4_TCP);
		aLayer.add(MessageType.L4_UDP);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );
		
		aLayer = new HashSet<>();
		aLayer.add(MessageType.L7_DLT);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );

		aLayer = new HashSet<>();
		aLayer.add(MessageType.L5_SOME_IP);
		aLayer.add(MessageType.L5_UDPNM);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );
		
		aLayer = new HashSet<>();
		aLayer.add(MessageType.L6_SOME_IP_SD_ENTRY);
		intermediateLayerList.add( Collections.unmodifiableSet( aLayer ) );
		
		layers = Collections.unmodifiableList( intermediateLayerList );
	}

	private OsiLayerConfig() {
		// avoid instantiation
	}
	
	private static int compareTo(MessageType thiz, MessageType other) {
		int thizIdx = getLayer(thiz);
		int otherIdx = getLayer(other);
		assert thizIdx >= 0 && otherIdx >= 0; //FIXME mj assertions
		return thizIdx - otherIdx;		
	}
	
	private static int getLayer(MessageType messageType){
		int idx = -1;
		for (Set<MessageType> set : layers) {
			++idx;
			if (set.contains(messageType))
				break;			
		}
		return idx;
	}
	
	private static List<BusType> getBusTypesFor(Class<? extends AbstractBusMessage> messageType) {
		List<BusType> busTypes = new ArrayList<BusType>();
		
		if (messageType != null) {
			
			//TODO if there is a message that can be transmitted via several bus types
			//someday (e.g. Dlt->Ethernet,Serial) there is more todo here...
			
			if ( SomeIPMessage.class.isAssignableFrom(messageType)  )
				busTypes.add(BusType.ETHERNET);
			
			else if ( SomeIPSDMessage.class.isAssignableFrom(messageType)  )
				busTypes.add(BusType.ETHERNET);
			
			else if ( UDPMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			else if ( TCPMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			else if ( IPv4Message.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			else if ( EthernetMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			else if ( UDPNMMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			else if ( CANMessage.class.isAssignableFrom(messageType) )
				busTypes.add(BusType.CAN);			
			
			else if ( LINMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.LIN);	
			
			else if ( FlexRayMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.FLEXRAY);	
			
			else if ( VerboseDLTMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.DLT);	
			
			// SMARD-2459 NonVerboseDLTMessage is of BusType Ethernet
			else if ( NonVerboseDLTMessage.class.isAssignableFrom(messageType))
				busTypes.add(BusType.ETHERNET);	
			
			if (busTypes.isEmpty())
				throw new RuntimeException("unhandled type: "+messageType.getName());
		}
		
		return busTypes;
	}
	
	public static List<BusType> getBusTypesFor(AbstractBusMessage message) {
		
		//todo if there is a message that can be transmitted via several bus types someday
		//we have to look at the possibly set bus filter to answer the question for a message object
		
		// as there is currently no such message the answer is the same as for the types...
		// TODO to determine correct bus for PluginMes
		return getBusTypesFor(message.getClass());
	}
	
	private static MessageType getMessageType(AbstractMessage message) {		
		if (message != null) {
			return getMessageType(message.getPrimaryFilter());
		}
		throw new RuntimeException("message is null");
	}
	
	public static MessageType getMessageType(AbstractFilter filter) {
		if (filter != null) {
			if (filter instanceof CANFilter)
				return MessageType.CAN;
			
			if (filter instanceof LINFilter)
				return MessageType.LIN;
			
			if (filter instanceof FlexRayFilter)
				return MessageType.FLEXRAY;
			
			if (filter instanceof EthernetFilter)
				return MessageType.L2_ETHERNET;
			
			if (filter instanceof IPv4Filter)
				return MessageType.L3_IPV4;
			
			if (filter instanceof TCPFilter)
				return MessageType.L4_TCP;
			
			if (filter instanceof UDPFilter)
				return MessageType.L4_UDP;
			
			if (filter instanceof UDPNMFilter)
				return MessageType.L5_UDPNM;
			
			if (filter instanceof SomeIPFilter)
				return MessageType.L5_SOME_IP;
			
			if (filter instanceof SomeIPSDFilter)
				return MessageType.L6_SOME_IP_SD_ENTRY;
			
			if (filter instanceof DLTFilter || filter instanceof NonVerboseDLTFilter || filter instanceof VerboseDLTPayloadFilter)
				return MessageType.L7_DLT;
			
			if (filter instanceof PluginFilter) 
				return MessageType.PLUGIN;
			
			if (filter instanceof PayloadFilter)
				return getMessageType(((PayloadFilter) filter).getMessage());

			throw new RuntimeException("unhandled type: "+filter.eClass());
		}
		throw new RuntimeException("filter is null");
	}
	
	/**
	 * 
	 * @param filter
	 * @param message
	 * @return
	 */
	public static boolean isCompatible(AbstractFilter filter, AbstractMessage message) {
		if(filter instanceof PayloadFilter) {
			return !(message instanceof PluginMessage);
		} else if(filter instanceof VerboseDLTPayloadFilter) {
			return message instanceof VerboseDLTMessage;
		}

		boolean result = 0 > compareTo( getMessageType(filter),getMessageType(message) );

		if (result) {	// filter
			// handle special cases here like SOMEIP is Ethernet only
			
			if (filter instanceof CANFilter) {
				result = message instanceof CANMessage;
			}

			else if (filter instanceof LINFilter) {
				result = message instanceof LINMessage;
			}
			
			else if (filter instanceof FlexRayFilter) {
				result = message instanceof FlexRayMessage;
			}	
			
			else if (filter instanceof PluginFilter) {
				result = message instanceof PluginMessage;
			}
			
			else if(filter instanceof DLTFilter) {
				result = message instanceof DLTMessage;
			}
			
			else if (filter instanceof VerboseDLTPayloadFilter) {
				result = message instanceof VerboseDLTMessage;
			}

			else if(filter instanceof NonVerboseDLTFilter) {
				result = message instanceof NonVerboseDLTMessage;
			}
		}

		if (result) {	// message
			if(message instanceof PluginMessage) {
				result = filter instanceof PluginFilter;
			}

			if(message instanceof UDPNMMessage) {
				if(filter instanceof TCPFilter) {
					return false;
				}
			}
		}
					
		return result;		
	}

	private static boolean hasFreeSlot(AbstractBusMessage message, MessageType slot) {
		List<AbstractFilter> filters = new ArrayList<>(message.getFilters());
		filters.add(message.getPrimaryFilter());
		
		for (AbstractFilter filter : filters) {
			if (0==compareTo(slot,getMessageType(filter))) {
				return false;
			}			
		}
		return true;
	}
	
	public static boolean canAddFilterToMessage(AbstractFilter filter, AbstractMessage message) {
		if(filter instanceof PayloadFilter
                || (filter instanceof VerboseDLTPayloadFilter && message instanceof VerboseDLTMessage)) {
			return true;
		}

		boolean result = isCompatible(filter, message);
		if (result && message instanceof AbstractBusMessage) {
				result = hasFreeSlot((AbstractBusMessage) message, getMessageType(filter));
			}
		return result;
	}
}
