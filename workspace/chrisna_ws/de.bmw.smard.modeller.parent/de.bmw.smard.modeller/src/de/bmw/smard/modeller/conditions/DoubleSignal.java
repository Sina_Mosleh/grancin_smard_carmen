package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDoubleSignal()
 * @model
 * @generated
 */
public interface DoubleSignal extends AbstractSignal {

    /**
     * Returns the value of the '<em><b>Ignore Invalid Values</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ignore Invalid Values</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ignore Invalid Values</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #setIgnoreInvalidValues(BooleanOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDoubleSignal_IgnoreInvalidValues()
     * @model default="false" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    BooleanOrTemplatePlaceholderEnum getIgnoreInvalidValues();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ignore Invalid Values</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #getIgnoreInvalidValues()
     * @generated
     */
    void setIgnoreInvalidValues(BooleanOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ignore Invalid Values Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ignore Invalid Values Tmpl Param</em>' attribute.
     * @see #setIgnoreInvalidValuesTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDoubleSignal_IgnoreInvalidValuesTmplParam()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getIgnoreInvalidValuesTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ignore Invalid Values Tmpl Param</em>' attribute.
     * @see #getIgnoreInvalidValuesTmplParam()
     * @generated
     */
    void setIgnoreInvalidValuesTmplParam(String value);
} // DoubleSignal
