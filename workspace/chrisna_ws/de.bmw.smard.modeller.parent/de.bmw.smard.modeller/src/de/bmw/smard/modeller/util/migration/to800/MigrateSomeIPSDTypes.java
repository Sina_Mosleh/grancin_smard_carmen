package de.bmw.smard.modeller.util.migration.to800;

import de.bmw.smard.base.util.Pair;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum;
import de.bmw.smard.modeller.util.ModellerDataConversions;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import java.util.HashMap;
import java.util.Map;

public class MigrateSomeIPSDTypes extends CustomMigration {

	private static final String STOP_PREFIX = "STOP_";
	public static final String ALL_LITERAL = "ALL";

	private static final String SOMEIPSD_FILTER = "conditions.SomeIPSDFilter";
	private static final String SOMEIPSD_TYPE = "sdType";
	private static final String TTL = "ttl";

	private Map<Instance, Pair<SomeIPSDTypeOrTemplatePlaceholderEnum, TTLOrTemplatePlaceHolderEnum>> mapSomeIPSDFilterToTypeAndTTl =
			new HashMap<>();

	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		for(Instance someIpSdFilter : model.getAllInstances(SOMEIPSD_FILTER)) {
			Object someIPSDType = someIpSdFilter.get(SOMEIPSD_TYPE);
			Object ttl = someIpSdFilter.get(TTL);
			if(someIPSDType instanceof EEnumLiteral) {
				SomeIPSDTypeOrTemplatePlaceholderEnum newSomeIpSdType = null;
				TTLOrTemplatePlaceHolderEnum newTtl = TTLOrTemplatePlaceHolderEnum.ALL;
				
				String sdTypeName = ((EEnumLiteral) someIPSDType).getName();
				// SomeIPSDType starts with "_STOP" -> use non-Stop SomeIPSDType and TTL = 0
				if(sdTypeName.startsWith(STOP_PREFIX)) {
					newSomeIpSdType = 
							SomeIPSDTypeOrTemplatePlaceholderEnum.get(sdTypeName.replace(STOP_PREFIX, ""));	
					newTtl = TTLOrTemplatePlaceHolderEnum.ZERO;
				// SomeIPSDType does not start with "_STOP"
				} else {
					// SomeIPSDType is not "ALL" -> leave SomeIPSDType, set TTL > 0
					if(!(sdTypeName.equals(ALL_LITERAL))) {
						newTtl = TTLOrTemplatePlaceHolderEnum.GREATER_ZERO;
					// SomeIPSDType is "ALL" -> leave someIPSDType
					} else {
						if(ttl instanceof String && !StringUtils.nullOrEmpty((String) ttl)) {
							int ttlNumericValue = getTtlNumericValue((String) ttl);
							// TTL is zero -> set TTL = zero
							if(ttlNumericValue == 0) {
								newTtl = TTLOrTemplatePlaceHolderEnum.ZERO;
							// TTL is not zero set TTL > 0
							} else {
								newTtl = TTLOrTemplatePlaceHolderEnum.GREATER_ZERO;
							}
						}
					}
				}
				mapSomeIPSDFilterToTypeAndTTl.put(someIpSdFilter, 
						new Pair<>(newSomeIpSdType, newTtl));
			}
		}
	}

	private int getTtlNumericValue(String ttl) throws MigrationException {
		Integer intFromString = null;
		try {
			intFromString = ModellerDataConversions.getIntFromHexOrNumber(ttl);
		} catch (NumberFormatException e) {
			// not a valid numeric format
			throw new MigrationException("TTL - " + ttl + " - is not a valid format", e);
		}
		return intFromString;
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
		// set attributes of SomeIPSDFilter to new values
		mapSomeIPSDFilterToTypeAndTTl.forEach((filter, attributes) -> {
			if(attributes.elem1 != null) {
				filter.set(SOMEIPSD_TYPE, attributes.elem1);
			}
			if(attributes.elem2 != null) {
				filter.set(TTL, attributes.elem2);
			}
		});
	}

}
