package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.ISignalOrReference;

import java.util.function.Supplier;

public class DataTypeSupplierUtil {

    public static Supplier<DataType> getDataTypeSupplier(ISignalOrReference signal) {
        return signal::get_EvaluationDataType;
    }
}
