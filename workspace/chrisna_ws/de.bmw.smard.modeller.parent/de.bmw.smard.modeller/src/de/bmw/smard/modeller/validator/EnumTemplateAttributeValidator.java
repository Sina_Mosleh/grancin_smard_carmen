package de.bmw.smard.modeller.validator;

import org.eclipse.emf.common.util.Enumerator;

import java.util.List;

public class EnumTemplateAttributeValidator {

    public static TemplateAttributeValidator.TemplateTypeStep<IllegalValueErrorStep> builder(Enumerator value) {
        return new EnumTemplateAttributeValidator.Builder(value);
    }

    public interface IllegalValueErrorStep extends Build {
        Build illegalValueError(List<? extends Enumerator> legalValues, String errorId);
    }

    public static class Builder extends TemplateAttributeValidator.Builder<Enumerator, IllegalValueErrorStep> implements
            IllegalValueErrorStep {

        private String illegalValueErrorId;
        private List<? extends Enumerator> legalValues;

        Builder(Enumerator value) {
            super(value);
        }

        @Override
        public Build illegalValueError(List<? extends Enumerator> legalValues, String errorId) {
            this.legalValues = legalValues;
            this.illegalValueErrorId = errorId;
            return this;
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            if(legalValues != null && !legalValues.isEmpty()) {
                if(!legalValues.contains(getValue())) {
                    attachError(baseValidator, illegalValueErrorId);
                    return false;
                }
            }
            return true;
        }
    }
}
