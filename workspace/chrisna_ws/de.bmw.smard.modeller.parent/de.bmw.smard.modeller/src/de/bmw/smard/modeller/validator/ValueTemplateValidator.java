package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.validator.value.ValueValidator;

public abstract class ValueTemplateValidator {

    public interface IllegalValueErrorStep extends Build {
        IllegalValueErrorStep illegalValueError(String errorId);
        IllegalValueErrorStep illegalValueError(ValueValidator valueValidator);
    }

    public static abstract class Builder extends TemplateValidator.Builder<ValueTemplateValidator.IllegalValueErrorStep> implements
            ValueTemplateValidator.IllegalValueErrorStep {

        private String illegalValueErrorId;
        private ValueValidator valueValidator;

        protected String getIllegalValueErrorId() {
            return illegalValueErrorId;
        }

        Builder(String value) {
            super(value);
        }

        @Override
        public IllegalValueErrorStep illegalValueError(String errorId) {
            this.illegalValueErrorId = errorId;
            return this;
        }

        @Override
        public IllegalValueErrorStep illegalValueError(ValueValidator valueValidator) {
            this.valueValidator = valueValidator;
            return this;
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            if (valueValidator != null) {
                return valueValidator.validate(getValue(), baseValidator);
            }
            return true;
        }
    }
}
