/**
 *
 */
package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.SomeIPFilter;
import de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Some IP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getServiceId <em>Service Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getMethodType <em>Method Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getMethodTypeTmplParam <em>Method Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getMethodId <em>Method Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getClientId <em>Client Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getSessionId <em>Session Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getProtocolVersion <em>Protocol Version</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getInterfaceVersion <em>Interface Version</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getMessageType <em>Message Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getMessageTypeTmplParam <em>Message Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getReturnCode <em>Return Code</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getReturnCodeTmplParam <em>Return Code Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl#getSomeIPLength <em>Some IP Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SomeIPFilterImpl extends AbstractFilterImpl implements SomeIPFilter {
    /**
     * The default value of the '{@link #getServiceId() <em>Service Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getServiceId()
     * @generated
     * @ordered
     */
    protected static final String SERVICE_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getServiceId() <em>Service Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getServiceId()
     * @generated
     * @ordered
     */
    protected String serviceId = SERVICE_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getMethodType() <em>Method Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodType()
     * @generated
     * @ordered
     */
    protected static final SomeIPMethodTypeOrTemplatePlaceholderEnum METHOD_TYPE_EDEFAULT = SomeIPMethodTypeOrTemplatePlaceholderEnum.ALL;
    /**
     * The cached value of the '{@link #getMethodType() <em>Method Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodType()
     * @generated
     * @ordered
     */
    protected SomeIPMethodTypeOrTemplatePlaceholderEnum methodType = METHOD_TYPE_EDEFAULT;
    /**
     * The default value of the '{@link #getMethodTypeTmplParam() <em>Method Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String METHOD_TYPE_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getMethodTypeTmplParam() <em>Method Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String methodTypeTmplParam = METHOD_TYPE_TMPL_PARAM_EDEFAULT;
    /**
     * The default value of the '{@link #getMethodId() <em>Method Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodId()
     * @generated
     * @ordered
     */
    protected static final String METHOD_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getMethodId() <em>Method Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMethodId()
     * @generated
     * @ordered
     */
    protected String methodId = METHOD_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getClientId() <em>Client Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getClientId()
     * @generated
     * @ordered
     */
    protected static final String CLIENT_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getClientId() <em>Client Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getClientId()
     * @generated
     * @ordered
     */
    protected String clientId = CLIENT_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getSessionId() <em>Session Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSessionId()
     * @generated
     * @ordered
     */
    protected static final String SESSION_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getSessionId() <em>Session Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSessionId()
     * @generated
     * @ordered
     */
    protected String sessionId = SESSION_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getProtocolVersion() <em>Protocol Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolVersion()
     * @generated
     * @ordered
     */
    protected static final String PROTOCOL_VERSION_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getProtocolVersion() <em>Protocol Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolVersion()
     * @generated
     * @ordered
     */
    protected String protocolVersion = PROTOCOL_VERSION_EDEFAULT;
    /**
     * The default value of the '{@link #getInterfaceVersion() <em>Interface Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterfaceVersion()
     * @generated
     * @ordered
     */
    protected static final String INTERFACE_VERSION_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getInterfaceVersion() <em>Interface Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterfaceVersion()
     * @generated
     * @ordered
     */
    protected String interfaceVersion = INTERFACE_VERSION_EDEFAULT;
    /**
     * The default value of the '{@link #getMessageType() <em>Message Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageType()
     * @generated
     * @ordered
     */
    protected static final SomeIPMessageTypeOrTemplatePlaceholderEnum MESSAGE_TYPE_EDEFAULT = SomeIPMessageTypeOrTemplatePlaceholderEnum.ALL;
    /**
     * The cached value of the '{@link #getMessageType() <em>Message Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageType()
     * @generated
     * @ordered
     */
    protected SomeIPMessageTypeOrTemplatePlaceholderEnum messageType = MESSAGE_TYPE_EDEFAULT;
    /**
     * The default value of the '{@link #getMessageTypeTmplParam() <em>Message Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_TYPE_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getMessageTypeTmplParam() <em>Message Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String messageTypeTmplParam = MESSAGE_TYPE_TMPL_PARAM_EDEFAULT;
    /**
     * The default value of the '{@link #getReturnCode() <em>Return Code</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getReturnCode()
     * @generated
     * @ordered
     */
    protected static final SomeIPReturnCodeOrTemplatePlaceholderEnum RETURN_CODE_EDEFAULT = SomeIPReturnCodeOrTemplatePlaceholderEnum.ALL;
    /**
     * The cached value of the '{@link #getReturnCode() <em>Return Code</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getReturnCode()
     * @generated
     * @ordered
     */
    protected SomeIPReturnCodeOrTemplatePlaceholderEnum returnCode = RETURN_CODE_EDEFAULT;
    /**
     * The default value of the '{@link #getReturnCodeTmplParam() <em>Return Code Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getReturnCodeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String RETURN_CODE_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getReturnCodeTmplParam() <em>Return Code Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getReturnCodeTmplParam()
     * @generated
     * @ordered
     */
    protected String returnCodeTmplParam = RETURN_CODE_TMPL_PARAM_EDEFAULT;
    /**
     * The default value of the '{@link #getSomeIPLength() <em>Some IP Length</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSomeIPLength()
     * @generated
     * @ordered
     */
    protected static final String SOME_IP_LENGTH_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getSomeIPLength() <em>Some IP Length</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSomeIPLength()
     * @generated
     * @ordered
     */
    protected String someIPLength = SOME_IP_LENGTH_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SomeIPFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SOME_IP_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getServiceId() {
        return serviceId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setServiceId(String newServiceId) {
        String oldServiceId = serviceId;
        serviceId = newServiceId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__SERVICE_ID, oldServiceId, serviceId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public SomeIPMethodTypeOrTemplatePlaceholderEnum getMethodType() {
        return methodType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMethodType(SomeIPMethodTypeOrTemplatePlaceholderEnum newMethodType) {
        SomeIPMethodTypeOrTemplatePlaceholderEnum oldMethodType = methodType;
        methodType = newMethodType == null ? METHOD_TYPE_EDEFAULT : newMethodType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE, oldMethodType, methodType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMethodTypeTmplParam() {
        return methodTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMethodTypeTmplParam(String newMethodTypeTmplParam) {
        String oldMethodTypeTmplParam = methodTypeTmplParam;
        methodTypeTmplParam = newMethodTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM, oldMethodTypeTmplParam, methodTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMethodId() {
        return methodId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMethodId(String newMethodId) {
        String oldMethodId = methodId;
        methodId = newMethodId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__METHOD_ID, oldMethodId, methodId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getClientId() {
        return clientId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setClientId(String newClientId) {
        String oldClientId = clientId;
        clientId = newClientId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__CLIENT_ID, oldClientId, clientId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSessionId() {
        return sessionId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSessionId(String newSessionId) {
        String oldSessionId = sessionId;
        sessionId = newSessionId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__SESSION_ID, oldSessionId, sessionId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getProtocolVersion() {
        return protocolVersion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setProtocolVersion(String newProtocolVersion) {
        String oldProtocolVersion = protocolVersion;
        protocolVersion = newProtocolVersion;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION, oldProtocolVersion, protocolVersion));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInterfaceVersion() {
        return interfaceVersion;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInterfaceVersion(String newInterfaceVersion) {
        String oldInterfaceVersion = interfaceVersion;
        interfaceVersion = newInterfaceVersion;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION, oldInterfaceVersion, interfaceVersion));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public SomeIPMessageTypeOrTemplatePlaceholderEnum getMessageType() {
        return messageType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageType(SomeIPMessageTypeOrTemplatePlaceholderEnum newMessageType) {
        SomeIPMessageTypeOrTemplatePlaceholderEnum oldMessageType = messageType;
        messageType = newMessageType == null ? MESSAGE_TYPE_EDEFAULT : newMessageType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE, oldMessageType, messageType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageTypeTmplParam() {
        return messageTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageTypeTmplParam(String newMessageTypeTmplParam) {
        String oldMessageTypeTmplParam = messageTypeTmplParam;
        messageTypeTmplParam = newMessageTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM, oldMessageTypeTmplParam, messageTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public SomeIPReturnCodeOrTemplatePlaceholderEnum getReturnCode() {
        return returnCode;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setReturnCode(SomeIPReturnCodeOrTemplatePlaceholderEnum newReturnCode) {
        SomeIPReturnCodeOrTemplatePlaceholderEnum oldReturnCode = returnCode;
        returnCode = newReturnCode == null ? RETURN_CODE_EDEFAULT : newReturnCode;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__RETURN_CODE, oldReturnCode, returnCode));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getReturnCodeTmplParam() {
        return returnCodeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setReturnCodeTmplParam(String newReturnCodeTmplParam) {
        String oldReturnCodeTmplParam = returnCodeTmplParam;
        returnCodeTmplParam = newReturnCodeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM, oldReturnCodeTmplParam, returnCodeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSomeIPLength() {
        return someIPLength;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSomeIPLength(String newSomeIPLength) {
        String oldSomeIPLength = someIPLength;
        someIPLength = newSomeIPLength;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH, oldSomeIPLength, someIPLength));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidServiceId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_SERVICE_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(serviceId)
                        .name(ConditionsPackage.Literals.SOME_IP_FILTER__SERVICE_ID.getName())
                        .warning("_Validation_Conditions_SomeIPFilter_ServiceId_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_ServiceId_IllegalServiceId")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidMethodId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_METHOD_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(methodId)
                        .name(ConditionsPackage.Literals.SOME_IP_FILTER__METHOD_ID.getName())
                        .warning("_Validation_Conditions_SomeIPFilter_MethodId_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_MethodId_IllegalMethodId")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidSessionId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_METHOD_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(sessionId)
                        .name("SessionID")
                        .warning("_Validation_Conditions_SomeIPFilter_SessionId_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_SessionId_IllegalSessionId")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidClientId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_CLIENT_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(clientId)
                        .name("ClientID")
                        .warning("_Validation_Conditions_SomeIPFilter_ClientId_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_ClientId_IllegalClientId")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidLength(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_LENGTH)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(someIPLength)
                        .name(ConditionsPackage.Literals.SOME_IP_FILTER__SOME_IP_LENGTH.getName())
                        .warning("_Validation_Conditions_SomeIPFilter_Length_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_Length_IllegalLength")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidProtocolVersion(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_PROTOCOL_VERSION)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(protocolVersion)
                        .name(ConditionsPackage.Literals.SOME_IP_FILTER__PROTOCOL_VERSION.getName())
                        .warning("_Validation_Conditions_SomeIPFilter_ProtocolVersion_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_ProtocolVersion_IllegalProtocolVersion")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidInterfaceVersion(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_INTERFACE_VERSION)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(interfaceVersion)
                        .name(ConditionsPackage.Literals.SOME_IP_FILTER__INTERFACE_VERSION.getName())
                        .warning("_Validation_Conditions_SomeIPFilter_InterfaceVersion_WARNING")
                        .error("_Validation_Conditions_SomeIPFilter_InterfaceVersion_IllegalInterfaceVersion")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidMessageType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_MESSAGE_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(messageType)
                        .templateType(SomeIPMessageTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(messageTypeTmplParam)
                        .containsParameterError("_Validation_Conditions_SomeIPFilter_MessageType_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_SomeIPFilter_MessageTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_SomeIPFilter_MessageTypeTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidReturnCode(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_RETURN_CODE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(returnCode)
                        .templateType(SomeIPReturnCodeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(returnCodeTmplParam)
                        .containsParameterError("_Validation_Conditions_SomeIPFilter_ReturnCode_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_SomeIPFilter_ReturnCodeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_SomeIPFilter_ReturnCodeTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidMethodType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.SOME_IP_FILTER__IS_VALID_HAS_VALID_METHOD_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(methodType)
                        .templateType(SomeIPMethodTypeOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(methodTypeTmplParam)
                        .containsParameterError("_Validation_Conditions_SomeIPFilter_MethodType_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_SomeIPFilter_MethodTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_SomeIPFilter_MethodTypeTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.SOME_IP_FILTER__SERVICE_ID:
                return getServiceId();
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE:
                return getMethodType();
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM:
                return getMethodTypeTmplParam();
            case ConditionsPackage.SOME_IP_FILTER__METHOD_ID:
                return getMethodId();
            case ConditionsPackage.SOME_IP_FILTER__CLIENT_ID:
                return getClientId();
            case ConditionsPackage.SOME_IP_FILTER__SESSION_ID:
                return getSessionId();
            case ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION:
                return getProtocolVersion();
            case ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION:
                return getInterfaceVersion();
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE:
                return getMessageType();
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM:
                return getMessageTypeTmplParam();
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE:
                return getReturnCode();
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM:
                return getReturnCodeTmplParam();
            case ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH:
                return getSomeIPLength();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.SOME_IP_FILTER__SERVICE_ID:
                setServiceId((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE:
                setMethodType((SomeIPMethodTypeOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM:
                setMethodTypeTmplParam((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_ID:
                setMethodId((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__CLIENT_ID:
                setClientId((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__SESSION_ID:
                setSessionId((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION:
                setProtocolVersion((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION:
                setInterfaceVersion((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE:
                setMessageType((SomeIPMessageTypeOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM:
                setMessageTypeTmplParam((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE:
                setReturnCode((SomeIPReturnCodeOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM:
                setReturnCodeTmplParam((String) newValue);
                return;
            case ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH:
                setSomeIPLength((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SOME_IP_FILTER__SERVICE_ID:
                setServiceId(SERVICE_ID_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE:
                setMethodType(METHOD_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM:
                setMethodTypeTmplParam(METHOD_TYPE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_ID:
                setMethodId(METHOD_ID_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__CLIENT_ID:
                setClientId(CLIENT_ID_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__SESSION_ID:
                setSessionId(SESSION_ID_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION:
                setProtocolVersion(PROTOCOL_VERSION_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION:
                setInterfaceVersion(INTERFACE_VERSION_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE:
                setMessageType(MESSAGE_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM:
                setMessageTypeTmplParam(MESSAGE_TYPE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE:
                setReturnCode(RETURN_CODE_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM:
                setReturnCodeTmplParam(RETURN_CODE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH:
                setSomeIPLength(SOME_IP_LENGTH_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SOME_IP_FILTER__SERVICE_ID:
                return SERVICE_ID_EDEFAULT == null ? serviceId != null : !SERVICE_ID_EDEFAULT.equals(serviceId);
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE:
                return methodType != METHOD_TYPE_EDEFAULT;
            case ConditionsPackage.SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM:
                return METHOD_TYPE_TMPL_PARAM_EDEFAULT == null ? methodTypeTmplParam != null : !METHOD_TYPE_TMPL_PARAM_EDEFAULT.equals(methodTypeTmplParam);
            case ConditionsPackage.SOME_IP_FILTER__METHOD_ID:
                return METHOD_ID_EDEFAULT == null ? methodId != null : !METHOD_ID_EDEFAULT.equals(methodId);
            case ConditionsPackage.SOME_IP_FILTER__CLIENT_ID:
                return CLIENT_ID_EDEFAULT == null ? clientId != null : !CLIENT_ID_EDEFAULT.equals(clientId);
            case ConditionsPackage.SOME_IP_FILTER__SESSION_ID:
                return SESSION_ID_EDEFAULT == null ? sessionId != null : !SESSION_ID_EDEFAULT.equals(sessionId);
            case ConditionsPackage.SOME_IP_FILTER__PROTOCOL_VERSION:
                return PROTOCOL_VERSION_EDEFAULT == null ? protocolVersion != null : !PROTOCOL_VERSION_EDEFAULT.equals(protocolVersion);
            case ConditionsPackage.SOME_IP_FILTER__INTERFACE_VERSION:
                return INTERFACE_VERSION_EDEFAULT == null ? interfaceVersion != null : !INTERFACE_VERSION_EDEFAULT.equals(interfaceVersion);
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE:
                return messageType != MESSAGE_TYPE_EDEFAULT;
            case ConditionsPackage.SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM:
                return MESSAGE_TYPE_TMPL_PARAM_EDEFAULT == null ? messageTypeTmplParam != null : !MESSAGE_TYPE_TMPL_PARAM_EDEFAULT.equals(messageTypeTmplParam);
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE:
                return returnCode != RETURN_CODE_EDEFAULT;
            case ConditionsPackage.SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM:
                return RETURN_CODE_TMPL_PARAM_EDEFAULT == null ? returnCodeTmplParam != null : !RETURN_CODE_TMPL_PARAM_EDEFAULT.equals(returnCodeTmplParam);
            case ConditionsPackage.SOME_IP_FILTER__SOME_IP_LENGTH:
                return SOME_IP_LENGTH_EDEFAULT == null ? someIPLength != null : !SOME_IP_LENGTH_EDEFAULT.equals(someIPLength);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (serviceId: ");
        result.append(serviceId);
        result.append(", methodType: ");
        result.append(methodType);
        result.append(", methodTypeTmplParam: ");
        result.append(methodTypeTmplParam);
        result.append(", methodId: ");
        result.append(methodId);
        result.append(", clientId: ");
        result.append(clientId);
        result.append(", sessionId: ");
        result.append(sessionId);
        result.append(", protocolVersion: ");
        result.append(protocolVersion);
        result.append(", interfaceVersion: ");
        result.append(interfaceVersion);
        result.append(", messageType: ");
        result.append(messageType);
        result.append(", messageTypeTmplParam: ");
        result.append(messageTypeTmplParam);
        result.append(", returnCode: ");
        result.append(returnCode);
        result.append(", returnCodeTmplParam: ");
        result.append(returnCodeTmplParam);
        result.append(", someIPLength: ");
        result.append(someIPLength);
        result.append(')');
        return result.toString();
    }

} //SomeIPFilterImpl
