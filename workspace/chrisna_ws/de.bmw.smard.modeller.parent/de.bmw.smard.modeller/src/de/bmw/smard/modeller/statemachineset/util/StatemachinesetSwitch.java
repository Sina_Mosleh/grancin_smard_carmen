package de.bmw.smard.modeller.statemachineset.util;

import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import de.bmw.smard.modeller.statemachineset.DocumentRoot;
import de.bmw.smard.modeller.statemachineset.ExecutionConfig;
import de.bmw.smard.modeller.statemachineset.GeneralInfo;
import de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig;
import de.bmw.smard.modeller.statemachineset.Output;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage
 * @generated
 */
public class StatemachinesetSwitch<T> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static StatemachinesetPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachinesetSwitch() {
        if (modelPackage == null) {
            modelPackage = StatemachinesetPackage.eINSTANCE;
        }
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    public T doSwitch(EObject theEObject) {
        return doSwitch(theEObject.eClass(), theEObject);
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    protected T doSwitch(EClass theEClass, EObject theEObject) {
        if (theEClass.eContainer() == modelPackage) {
            return doSwitch(theEClass.getClassifierID(), theEObject);
        } else {
            List<EClass> eSuperTypes = theEClass.getESuperTypes();
            return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(eSuperTypes.get(0), theEObject);
        }
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case StatemachinesetPackage.DOCUMENT_ROOT: {
                DocumentRoot documentRoot = (DocumentRoot) theEObject;
                T result = caseDocumentRoot(documentRoot);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.GENERAL_INFO: {
                GeneralInfo generalInfo = (GeneralInfo) theEObject;
                T result = caseGeneralInfo(generalInfo);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.OUTPUT: {
                Output output = (Output) theEObject;
                T result = caseOutput(output);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.STATE_MACHINE_SET: {
                StateMachineSet stateMachineSet = (StateMachineSet) theEObject;
                T result = caseStateMachineSet(stateMachineSet);
                if (result == null) result = caseBusMessageReferable(stateMachineSet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.KEY_VALUE_PAIR_USER_CONFIG: {
                KeyValuePairUserConfig keyValuePairUserConfig = (KeyValuePairUserConfig) theEObject;
                T result = caseKeyValuePairUserConfig(keyValuePairUserConfig);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.EXECUTION_CONFIG: {
                ExecutionConfig executionConfig = (ExecutionConfig) theEObject;
                T result = caseExecutionConfig(executionConfig);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case StatemachinesetPackage.BUS_MESSAGE_REFERABLE: {
                BusMessageReferable busMessageReferable = (BusMessageReferable) theEObject;
                T result = caseBusMessageReferable(busMessageReferable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDocumentRoot(DocumentRoot object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>General Info</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>General Info</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseGeneralInfo(GeneralInfo object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Output</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Output</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseOutput(Output object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>State Machine Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>State Machine Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStateMachineSet(StateMachineSet object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Key Value Pair User Config</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Key Value Pair User Config</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseKeyValuePairUserConfig(KeyValuePairUserConfig object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Execution Config</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Execution Config</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExecutionConfig(ExecutionConfig object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bus Message Referable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bus Message Referable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusMessageReferable(BusMessageReferable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    public T defaultCase(EObject object) {
        return null;
    }

} //StatemachinesetSwitch
