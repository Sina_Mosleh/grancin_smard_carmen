package de.bmw.smard.modeller.conditions.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;

public class ConditionsXMLHelperImpl extends XMLHelperImpl {

    public ConditionsXMLHelperImpl(XMLResource resource) {
        super(resource);
    }

    @Override
    public String getHREF(EObject obj) {
        InternalEObject o = (InternalEObject) obj;

        URI objectURI = o.eProxyURI();
        if (objectURI == null) {
            Resource otherResource = obj.eResource();
            if (otherResource == null) {
                objectURI = handleDanglingHREF(obj);
                if (objectURI == null) {
                    return null;
                }
            } else {
                objectURI = getHREF(otherResource, obj);
            }
        }

        objectURI = deresolve(objectURI);


        return objectURI.toString();
    }

    @Override
    public URI resolve(URI relative, URI base) {
        if (relative.toString().equals("de.bmw.smard.modeller.conditions")) {
            return relative;
        }
        return super.resolve(relative, base);
    }


}
