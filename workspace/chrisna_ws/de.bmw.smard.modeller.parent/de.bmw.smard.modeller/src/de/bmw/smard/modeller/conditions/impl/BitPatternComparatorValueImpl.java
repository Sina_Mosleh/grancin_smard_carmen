package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.BitPatternComparatorValue;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bit Pattern Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl#getValue <em>Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl#getStartbit <em>Startbit</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BitPatternComparatorValueImpl extends ComparatorSignalImpl implements BitPatternComparatorValue {
    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final String VALUE_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected String value = VALUE_EDEFAULT;

    /**
     * The default value of the '{@link #getStartbit() <em>Startbit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartbit()
     * @generated
     * @ordered
     */
    protected static final String STARTBIT_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getStartbit() <em>Startbit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartbit()
     * @generated
     * @ordered
     */
    protected String startbit = STARTBIT_EDEFAULT;

    /**
     * This is true if the Startbit attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean startbitESet;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected BitPatternComparatorValueImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.BIT_PATTERN_COMPARATOR_VALUE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValue(String newValue) {
        String oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStartbit() {
        return startbit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStartbit(String newStartbit) {
        String oldStartbit = startbit;
        startbit = newStartbit;
        boolean oldStartbitESet = startbitESet;
        startbitESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT, oldStartbit, startbit, !oldStartbitESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetStartbit() {
        String oldStartbit = startbit;
        boolean oldStartbitESet = startbitESet;
        startbit = STARTBIT_EDEFAULT;
        startbitESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT, oldStartbit, STARTBIT_EDEFAULT, oldStartbitESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetStartbit() {
        return startbitESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidBitPattern(DiagnosticChain diagnostics, Map<Object, Object> context) {

        String bitPattern = value;
        if (bitPattern != null) {
            bitPattern = bitPattern.trim().toUpperCase().replace(" ", "");
        }

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.BIT_PATTERN_COMPARATOR_VALUE__IS_VALID_HAS_VALID_BIT_PATTERN)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(StringUtils.isBlank(value))
                        .error("_Validation_Conditions_BitPatternComparatorValue_BitPattern_isEmpty")
                        .build())

                .with(Validators.stringTemplate(bitPattern)
                        .containsParameterError("_Validation_Conditions_BitPatternComparatorValue_BitPattern_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_BitPatternComparatorValue_BitPattern_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.validBitPattern())
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStartbit(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.BIT_PATTERN_COMPARATOR_VALUE__IS_VALID_HAS_VALID_STARTBIT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intTemplate(startbit)
                        .containsParameterError("_Validation_Conditions_BitPatternComparatorValue_StartBit_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_BitPatternComparatorValue_StartBit_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.validBitPatternStartBit())
                        .illegalValueError("_Validation_Conditions_BitPatternComparatorValue_StartBit_IllegalStartbit")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__VALUE:
                return getValue();
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT:
                return getStartbit();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__VALUE:
                setValue((String) newValue);
                return;
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT:
                setStartbit((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__VALUE:
                setValue(VALUE_EDEFAULT);
                return;
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT:
                unsetStartbit();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__VALUE:
                return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE__STARTBIT:
                return isSetStartbit();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (value: ");
        result.append(value);
        result.append(", startbit: ");
        if (startbitESet) result.append(startbit);
        else result.append("<unset>");
        result.append(')');
        return result.toString();
    }

    /** generated NOT */
    @Override
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

} //BitPatternComparatorValueImpl
