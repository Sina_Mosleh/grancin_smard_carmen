package de.bmw.smard.modeller.conditions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.NotExpression#getExpression <em>Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.NotExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNotExpression()
 * @model
 * @generated
 */
public interface NotExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Expression</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Expression</em>' containment reference.
     * @see #setExpression(Expression)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNotExpression_Expression()
     * @model containment="true" required="true"
     * @generated
     */
    Expression getExpression();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.NotExpression#getExpression <em>Expression</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Expression</em>' containment reference.
     * @see #getExpression()
     * @generated
     */
    void setExpression(Expression value);

    /**
     * Returns the value of the '<em><b>Operator</b></em>' attribute.
     * The default value is <code>"NOT"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.LogicalOperatorType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operator</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Operator</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getNotExpression_Operator()
     * @model default="NOT" required="true" changeable="false"
     * @generated
     */
    LogicalOperatorType getOperator();

} // NotExpression
