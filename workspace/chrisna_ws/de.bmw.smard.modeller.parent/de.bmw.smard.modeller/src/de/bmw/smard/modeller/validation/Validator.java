package de.bmw.smard.modeller.validation;

public interface Validator {

    ValidationResult validate();
}
