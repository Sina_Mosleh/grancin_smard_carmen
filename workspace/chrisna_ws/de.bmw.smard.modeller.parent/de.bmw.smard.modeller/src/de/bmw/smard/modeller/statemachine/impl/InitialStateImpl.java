package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.util.EMFUtils;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InitialStateImpl extends AbstractStateImpl implements InitialState {
    /**
     * <!-- begin-user-doc -->
     * modified to have unique id and fixed name
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    protected InitialStateImpl() {
        super();
        EMFUtils.assignNewUniqueObjectId(this);
        name = "InitialState";
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.INITIAL_STATE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidNoInTransition(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.INITIAL_STATE__IS_VALID_HAS_VALID_NO_IN_TRANSITION)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(in != null && !in.isEmpty())
                        .error("_Validation_Statemachine_InitialState_NoInTransition", "Initial State must not have an incoming Transition")
                        .build())

                .validate();
    }


} //InitialStateImpl
