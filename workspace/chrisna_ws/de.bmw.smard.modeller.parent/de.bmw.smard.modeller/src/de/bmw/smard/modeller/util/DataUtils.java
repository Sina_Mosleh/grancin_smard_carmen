package de.bmw.smard.modeller.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.bmw.smard.modeller.statemachine.DocumentRoot;
import de.bmw.smard.modeller.statemachine.StateMachine;

/**
 * Helper methods centered around EMF/GMF data handling (business scope, not technical as in {@link EMFUtils})
 */
public final class DataUtils {

	/**
	 * no instantiation
	 */
	private DataUtils() {}
	
    /**
     * Zugriff auf den Zustandsautomaten, der durch eine Resource repräsentiert wird.
     * @param resource auszulesende Resource
     * @return Zustandsautomat oder {@code null} im Fehlerfall
     */
    public static StateMachine getStateMachine(Resource resource)
    {
    	EList<EObject> contents = resource.getContents();
        if (contents != null)
        {
        	EObject firstContent = contents.get(0);
        	if (firstContent instanceof DocumentRoot) {
        		return ((DocumentRoot) firstContent).getStateMachine();
        	}
        	else if (firstContent instanceof StateMachine) {
        		return (StateMachine) firstContent; 
        	}
        }
        return null;
    }

    /**
     * Methode zum Ermitteln aller Zustandsautomaten-Modelle des Projekts (rekursiv durch die Resourcen).
     * 
     * @param pSrc ist ein Array von Ressourcen, die rekursiv durchsucht werden sollen.
     * @return eine Liste von {@link IResource} Objekte, die alle auf ein .statemachine File
     *         verweisen.
     * @throws CoreException
     */
    private static List<IResource> getStateMachines(IResource[] pSrc) throws CoreException
    {
        List<IResource> result = new ArrayList<IResource>();
        for (IResource res : pSrc)
        {
            if (res instanceof IFile)
            {
                IFile file = (IFile) res;
                if (file.getFileExtension() == null)
                    continue;
                String fileExtension = file.getFileExtension();
                if (fileExtension.equalsIgnoreCase(ResourceUtils.STATEMACHINE_FILEEXTENSION))
                {
                    result.add(res);
                }
            }
            else if (res instanceof IFolder)
            {
                result.addAll(getStateMachines(((IFolder) res).members()));
            }
        }
        return result;
    }

    /**
     * Finds and returns all state machine files in the project.
     * A state machine file is a file that has the correct file extension.
     * 
     * @param pProject the project
     * @return a list of all state machine files in the project
     * @throws CoreException
     */
    public static List<IResource> getStateMachinesInProject(IProject pProject) throws CoreException
    {
        IResource[] res = pProject.members();
        return getStateMachines(res);
    }
    
    /**
     * Returns the state machine file for the given diagram file
     * @param pDiagramFile the statemachine_diagram file, must not be null and must be of correct type
     * @return the statemachine file or null if it doesn't exist in the workspace
     */
    public static IResource getStateMachineForDiagram(IResource pDiagramFile)
    {
    	if (!ResourceUtils.STATEMACHINE_DIAGRAM_FILE_EXTENSION.equals(pDiagramFile.getFileExtension()))
    	{
			throw new IllegalArgumentException("File does not have the correct extension: "+pDiagramFile);
    	}
    	String name = pDiagramFile.getName();
    	int endIndex = name.length() - ResourceUtils.STATEMACHINE_DIAGRAM_FILE_EXTENSION.length();
    	name = name.substring(0,endIndex)+ResourceUtils.STATEMACHINE_FILEEXTENSION;
		IResource statemachineFile = pDiagramFile.getParent().findMember(name);
		return statemachineFile;
    }


}
