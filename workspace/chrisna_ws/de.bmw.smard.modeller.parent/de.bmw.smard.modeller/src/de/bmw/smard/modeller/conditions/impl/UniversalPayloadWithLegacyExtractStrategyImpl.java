package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Universal Payload With Legacy Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl#getStartBit <em>Start Bit</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl#getLength <em>Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversalPayloadWithLegacyExtractStrategyImpl extends UniversalPayloadExtractStrategyImpl implements UniversalPayloadWithLegacyExtractStrategy {
    /**
     * The default value of the '{@link #getStartBit() <em>Start Bit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartBit()
     * @generated
     * @ordered
     */
    protected static final String START_BIT_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getStartBit() <em>Start Bit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartBit()
     * @generated
     * @ordered
     */
    protected String startBit = START_BIT_EDEFAULT;

    /**
     * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLength()
     * @generated
     * @ordered
     */
    protected static final String LENGTH_EDEFAULT = "1";

    /**
     * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLength()
     * @generated
     * @ordered
     */
    protected String length = LENGTH_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UniversalPayloadWithLegacyExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStartBit() {
        return startBit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStartBit(String newStartBit) {
        String oldStartBit = startBit;
        startBit = newStartBit;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT, oldStartBit, startBit));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLength() {
        return length;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setLength(String newLength) {
        String oldLength = length;
        length = newLength;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH, oldLength, length));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStartbit(DiagnosticChain diagnostics, Map<Object, Object> context) {
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        boolean hasValidStartbit = true;
        boolean isMessageToSubstitute = false;
        String messageToSubstitute = null;
        String startbit = this.startBit;
        if (startbit != null && startbit.length() > 0) {
            if (extractionRule == null || extractionRule.length() == 0) {
                if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
                    messageToSubstitute = TemplateUtils.checkIntNotPlaceholder(startbit, false);
                    if (messageToSubstitute == null) {
                        try {
                            int startbitInt = TemplateUtils.getIntNotPlaceholder(startbit, false);
                            if (startbitInt < 0) {
                                errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IsLowerEqualsZero";
                                hasValidStartbit = false;
                            }
                        } catch (NumberFormatException e) {
                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IsNull";
                            hasValidStartbit = false;
                        }
                    } else {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IllegalStartbit";
                        isMessageToSubstitute = true;
                        hasValidStartbit = false;
                    }
                }
            } else {
                if (TemplateUtils.containsParameters(startbit)) {
                    messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(startbit);
                    if (messageToSubstitute != null && messageToSubstitute.length() > 0) {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_NotAValidPlaceholderName";
                        isMessageToSubstitute = true;
                        hasValidStartbit = false;
                    }
                } else {
                    try {
                        int startbitInt = TemplateUtils.getIntNotPlaceholder(startbit, false);
                        if (startbitInt < 0) {
                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IsLowerEqualsZero";
                            hasValidStartbit = false;
                        }
                    } catch (NumberFormatException e) {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IsNull";
                        hasValidStartbit = false;
                    }
                }
            }
        } else {
            if (extractionRule == "" && extractionRuleTmplParam == "") {
                errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_StartBit_IsNull";
                hasValidStartbit = false;
            }
        }
        if (!hasValidStartbit) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.builder(Severity.ERROR)
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STARTBIT)
                        .messageId(PluginActivator.getInstance(), errorMessageIdentifier, messageToSubstitute)
                        .data(this)
                        .build());
            }
            return hasValidStartbit;
        }
        return hasValidStartbit;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDataLength(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidDataLength = true;
        boolean isMessageToSubstitute = false;
        String messageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        String length = this.length;
        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            if (extractionRule == null || extractionRule.length() == 0) {
                messageToSubstitute = TemplateUtils.checkIntNotPlaceholder(length, false);
                if (messageToSubstitute != null && messageToSubstitute.length() > 0) {
                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IllegalDataLength";
                    hasValidDataLength = false;
                    isMessageToSubstitute = true;
                } else {
                    try {
                        int lengthInt = TemplateUtils.getIntNotPlaceholder(length, false);
                        if (lengthInt <= 0) {
                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IsLowerEqualsZero";
                            hasValidDataLength = false;
                        } else if (lengthInt > 54) {
                            errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IsGreaterFiftyFour";
                            hasValidDataLength = false;
                        }
                    } catch (NumberFormatException e) {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_NotANumber";
                        hasValidDataLength = false;
                    }
                }
            }
        } else {
            if (TemplateUtils.containsParameters(length)) {
                messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(length);
                if (messageToSubstitute != null && messageToSubstitute.length() > 0) {
                    errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IllegalDataLength";
                    hasValidDataLength = false;
                    isMessageToSubstitute = true;
                }
            } else {
                try {
                    int lengthInt = TemplateUtils.getIntNotPlaceholder(length, false);
                    if (lengthInt <= 0) {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IsLowerEqualsZero";
                        hasValidDataLength = false;
                    } else if (lengthInt > 54) {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_IsGreaterFiftyFour";
                        hasValidDataLength = false;
                    }
                } catch (NumberFormatException e) {
                    if (extractionRule == "" && extractionRuleTmplParam == "") {
                        errorMessageIdentifier = "_Validation_Conditions_UniversalPayloadWithLegacyExtractStrategy_Length_NotANumber";
                        hasValidDataLength = false;
                    }
                }
            }
        }
        if (!hasValidDataLength) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.builder(Severity.ERROR)
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_DATA_LENGTH)
                        .messageId(PluginActivator.getInstance(), errorMessageIdentifier, messageToSubstitute)
                        .data(this)
                        .build());
            }
            return hasValidDataLength;
        }
        return hasValidDataLength;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
                return getStartBit();
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
                return getLength();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
                setStartBit((String) newValue);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
                setLength((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
                setStartBit(START_BIT_EDEFAULT);
                return;
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
                setLength(LENGTH_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT:
                return START_BIT_EDEFAULT == null ? startBit != null : !START_BIT_EDEFAULT.equals(startBit);
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH:
                return LENGTH_EDEFAULT == null ? length != null : !LENGTH_EDEFAULT.equals(length);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (startBit: ");
        result.append(startBit);
        result.append(", length: ");
        result.append(length);
        result.append(')');
        return result.toString();
    }


    /** @generated NOT */
    @Override
    public String getExtractionRule() {
        String result = super.getExtractionRule();
        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            if (StringUtils.nullOrEmpty(result)) {
                result = (StringUtils.nullOrEmpty(startBit) ? "0" : startBit) + ",extract(" + length + ")";
            }
        }
        return result;
    }

} //UniversalPayloadWithLegacyExtractStrategyImpl
