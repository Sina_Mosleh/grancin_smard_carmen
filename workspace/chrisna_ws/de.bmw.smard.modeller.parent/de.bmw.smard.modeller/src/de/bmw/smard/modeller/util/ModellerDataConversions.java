package de.bmw.smard.modeller.util;

import de.bmw.smard.common.data.DataConversions;

public final class ModellerDataConversions extends DataConversions
{

	/** no instance */
	private ModellerDataConversions() {}

	/**
	 * converts a string into a string while replacing an integer value by the corresponding hex value allowing a template parameter
	 * string like #name# to pass through.
	 * Null and empty strings will become 0x0 and strings not matching any of the mentioned values will be passed through unmodified.
	 * 
	 * 
	 * @param instanceValue the string with a hex (0x...) or integer number or template parameter like #name#
	 * @return the number as hex string or the template parameter like #name#
	 */
	public static String getHexOrTemplatePlaceholderExternalStringFromObject( Object instanceValue, boolean nullable)
	{
		if (instanceValue==null || ((String)instanceValue).trim().length()==0)
		{
		    if (!nullable)
			{
		        return "0x0";
			}
		    else
		    {
		        return "";
		    }
		}
		String stringValue = (String) instanceValue;
		if (TemplateUtils.isPlaceholder(stringValue))
		{
			// Note: here, we're more strict and use isPlaceholder instead of containsPlaceholders
			return stringValue;
		}
		try
		{
			int intValue = getIntFromHexOrNumber(stringValue);
			return "0x"+Integer.toHexString(intValue).toUpperCase();
		}
		catch (NumberFormatException ex)
		{
			// not a valid value - pass through unmodified (so the user can correct it)
			return stringValue;
		}
	}



}
