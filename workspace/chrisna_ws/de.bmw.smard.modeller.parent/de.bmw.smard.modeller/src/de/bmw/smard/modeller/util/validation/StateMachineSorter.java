package de.bmw.smard.modeller.util.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.bmw.smard.base.exception.SmardRuntimeException;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.bmw.smard.modeller.conditions.Condition;
import de.bmw.smard.modeller.statemachine.AbstractAction;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.Transition;

/**
 * adopted from de.bmw.smard.stateengine.core.data.StateMachineSorter needed for
 * validation of statemachines before export
 * 
 * @author S.Riethig
 *
 */
public class StateMachineSorter {

    private static final Logger log = Logger.getLogger(StateMachineSorter.class);
	private final boolean suppressExceptions;
	private Set<StateMachine> missingStateMachines = new HashSet<>();

	/**
     * Constructor of the StateMachine Sorter
     * @param suppressExceptions if true the Exceptions won't be thrown (instead of throwing the Exception will be logged)
     */
	public StateMachineSorter(boolean suppressExceptions) {
		this.suppressExceptions = suppressExceptions;
	}

    public Set<StateMachine> getMissingStateMachines() {
		return missingStateMachines;
	}

	/**
	 * sorts a list of StateMachines in the resulting list there are independent
	 * StateMachine and the StateMachines that are depending on one of the
	 * independent StateMachines already in the list
	 *
	 * reasons for dependencies currently are:
	 * <ul>
	 * <li>Transition- and StateCheckConditions on Transitions</li>
	 * <li>Variables referenced in Action- and ConditionExpressions</li>
	 * </ul>
	 *
	 * @param allStateMachines
	 *            list of all StateMachines that should be sorted
	 * @return sorted list of allStateMachines
	 */
	public Map<StateMachine, Set<StateMachine>> sort(final List<StateMachine> allStateMachines) {
		EList<StateMachine> allStateMachinesEList = new BasicEList<>(allStateMachines);
		return sort(allStateMachinesEList);
	}
	
	private Map<StateMachine, Set<StateMachine>> sort(final EList<StateMachine> allStateMachines) {
		this.missingStateMachines = new HashSet<>();
		
		Map<AbstractState, StateMachine> state2Statemachine = new HashMap<>();
		Map<Transition, StateMachine> transition2Statemachine = new HashMap<>();
		Map<StateMachine, List<String>> writeVariables2Statemachine = new HashMap<>();

		// find out which state / transition belongs to which statemachine
		for (StateMachine stateMachine : allStateMachines) {
			if(stateMachine.eIsProxy()) {
				// if StateMachine is a proxy, we can't work with it from here
				except(new IllegalStateException("Unresolved proxy of StateMachine " + stateMachine.getName()));
			}

			try {
				for (AbstractState state : getAllStatesOfStateMachine(stateMachine)) {
					state2Statemachine.put(state, stateMachine);
					for (Transition trans : state.getOut()) {
						transition2Statemachine.put(trans, stateMachine);
					}
				}
			} catch (NullPointerException e) {
				// NPE, when filename of a resource or xml of resource contains a special character and cannot be parsed
				except(new SmardRuntimeException("Unable to load resource from file for " + stateMachine.getName()));
			}

			// store names of variables that are written by a statemachine
			List<String> writeVariables = stateMachine.getWriteVariables();
			if(writeVariables != null) {
				writeVariables2Statemachine.put(stateMachine, writeVariables);
			} else {
				writeVariables2Statemachine.put(stateMachine, Collections.emptyList());
			}
		}

		// stores which statemachine depends on which other statemachines
		// the latter are therefore prior to the dependant statemachine
		Map<StateMachine, Set<StateMachine>> dependentStateMachine2PriorStateMachines = new HashMap<>();
		Set<StateMachine> priorStateMachines;

		for (StateMachine currentSM : allStateMachines) {
			// find the statemachines that the currentStateMachine depends on
			priorStateMachines = new HashSet<>();

			EList<AbstractState> allStates = getAllStatesOfStateMachine(currentSM);
			for (AbstractState state : allStates) {
				for (Transition t : state.getOut()) {
					for (Condition cond : t.getConditions()) {
						addConditionsDependencies(cond, priorStateMachines, state2Statemachine,
								transition2Statemachine);

						// TODO SMARD-896 - to be specified
						addStatemachinesDependentDueToReferencedVariables(cond.getReadVariables(), currentSM,
								allStateMachines, priorStateMachines, writeVariables2Statemachine);
					}

					// check transition's (single) action for referenced variables
					for(AbstractAction action : t.getActions()) {
						addStatemachinesDependentDueToReferencedVariables(action, currentSM, allStateMachines,
								priorStateMachines, writeVariables2Statemachine);
					}
				}

				// check state's actions for referenced variables
				for (AbstractAction act : state.getActions()) {
					addStatemachinesDependentDueToReferencedVariables(act.getReadVariables(), currentSM,
							allStateMachines, priorStateMachines, writeVariables2Statemachine);
				}
			}
			dependentStateMachine2PriorStateMachines.put(currentSM, priorStateMachines);
		}
		
		Map<StateMachine, Set<StateMachine>> cachedDependentStateMachines =
				new HashMap<>(dependentStateMachine2PriorStateMachines);

		int retryCount = 0;
		List<StateMachine> orderedStateMachines = new ArrayList<>();
		Set<StateMachine> stateMachines;
		do {
			int oldDuMapSize = dependentStateMachine2PriorStateMachines.size();
			for (Map.Entry<StateMachine, Set<StateMachine>> entry : dependentStateMachine2PriorStateMachines
					.entrySet()) {
				stateMachines = entry.getValue();
				if (stateMachines.isEmpty()) {
					orderedStateMachines.add(entry.getKey());
				} else {
					if (orderedStateMachines.containsAll(stateMachines)) {
						orderedStateMachines.add(entry.getKey());
					}
				}
			}
			for (StateMachine rt : orderedStateMachines) {
				dependentStateMachine2PriorStateMachines.remove(rt);
			}
			
			if (dependentStateMachine2PriorStateMachines.size() == 0) {
				break;
			}
			
			if (oldDuMapSize == dependentStateMachine2PriorStateMachines.size()) {
				retryCount++;
			}
		} while (retryCount < 2);
		
		if (dependentStateMachine2PriorStateMachines.size() > 0) {
			except(new CyclicDependencyException(dependentStateMachine2PriorStateMachines));
		}

		Map<StateMachine, Set<StateMachine>> result = new LinkedHashMap<>();
		for(StateMachine stateMachine : orderedStateMachines) {
			result.put(stateMachine, cachedDependentStateMachines.get(stateMachine));
		}
		return result;
	}

	private EList<AbstractState> getAllStatesOfStateMachine(StateMachine currentSM) {
		EList<AbstractState> result = new BasicEList<>();
		result.addAll(currentSM.getStates());
		result.add(currentSM.getInitialState());
		return result;
	}

	private void addStatemachinesDependentDueToReferencedVariables(AbstractAction abstractAction,
			StateMachine currentStateMachineNotToBeChecked, List<StateMachine> stateMachinesToBeChecked,
			Set<StateMachine> dependentStatemachinesToAddTo, Map<StateMachine, List<String>> writeVariables2Statemachine) {
		if (abstractAction == null)
			return;

		// find all variables that are referenced by this action
		List<String> namesOfReferencedVariables = abstractAction.getReadVariables();

		addStatemachinesDependentDueToReferencedVariables(namesOfReferencedVariables, currentStateMachineNotToBeChecked,
				stateMachinesToBeChecked, dependentStatemachinesToAddTo, writeVariables2Statemachine);
	}

	private void addStatemachinesDependentDueToReferencedVariables(List<String> namesOfReferencedVariables,
			StateMachine currentStateMachineNotToBeChecked, List<StateMachine> stateMachinesToBeChecked,
			Set<StateMachine> dependentStatemachinesToAddTo, Map<StateMachine, List<String>> writeVariables2Statemachine) {

		// check for every var name and every StateMachine
		for (String varName : namesOfReferencedVariables) {
			for (StateMachine stateMachine : stateMachinesToBeChecked) {
				// if the var name is contained in StateMachine
				if (stateMachine != currentStateMachineNotToBeChecked
						&& writeVariables2Statemachine.get(stateMachine).contains(varName)
						&& !dependentStatemachinesToAddTo.contains(stateMachine)) {
					dependentStatemachinesToAddTo.add(stateMachine);
				}
			}
		}
	}

	private void addConditionsDependencies(final Condition conditionToBeChecked,
			final Set<StateMachine> referencedStatemachines, final Map<AbstractState, StateMachine> state2Statemachine,
			final Map<Transition, StateMachine> transition2Statemachine) {

		List<AbstractState> depStates = conditionToBeChecked.getStateDependencies();
		for (AbstractState state : depStates) {
			StateMachine smrt = state2Statemachine.get(state);
			if (smrt == null) {
				this.missingStateMachines.add(getParentStateMachine(state));
			}
			else if(!referencedStatemachines.contains(smrt)) {
				referencedStatemachines.add(smrt);
			}
		}

		List<Transition> depTransitions = conditionToBeChecked.getTransitionDependencies();
		for (Transition transition : depTransitions) {
			StateMachine smrt = transition2Statemachine.get(transition);
			if (smrt == null) {
				this.missingStateMachines.add(getParentStateMachine(transition));
			}
			else if(!referencedStatemachines.contains(smrt)) {
				referencedStatemachines.add(smrt);
			}
		}
	}

	private StateMachine getParentStateMachine(EObject stateOrTransition) {
		EObject parent = stateOrTransition.eContainer();
		if(parent instanceof StateMachine) {
			return (StateMachine) parent;
		}
		return null;
	}

	private <T extends Exception> void except(T ex) throws T {
	    if(suppressExceptions)
	        log.warn("Suppressed Exception: "+ex.getClass().getSimpleName(), ex);
		else
			throw ex;
	}

}
