package de.bmw.smard.modeller.util;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

public class ListEListConverter {
	
	public static <T> EList<T> convertToEList(List<T> input) {
		EList<T> result = new BasicEList<T>();
		for(T obj : input) {
			result.add(obj);
		}
		return result;
	}

}
