package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;

import java.util.function.BiFunction;

public class RangeIdTemplateValidator {

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> builder(String value, BiFunction<String, Boolean, String> valueValidator) {
        return new Builder(value, valueValidator);
    }

    public static class Builder extends ValueTemplateValidator.Builder {

        private  BiFunction<String, Boolean, String> valueValidator;

        Builder(String value, BiFunction<String, Boolean, String> valueValidator) {
            super(value);
            this.valueValidator = valueValidator;
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            if(!super.validateValue(baseValidator, exportable)) {
                return false;
            }

            String substitution =  valueValidator.apply(getValue(), true);
            if (!StringUtils.nullOrEmpty(substitution)) {
                attachError(baseValidator, getIllegalValueErrorId(), substitution);
                return false;
            }

            return true;
        }
    }
}
