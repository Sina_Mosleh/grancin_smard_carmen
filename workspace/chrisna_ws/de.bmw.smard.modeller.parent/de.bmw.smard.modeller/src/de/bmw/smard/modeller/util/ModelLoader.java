package de.bmw.smard.modeller.util;

import com.bmw.swll6.business.smard.common.ArtefaktArchivHandler.ArtefaktException;
import com.bmw.swll6.business.smard.common.ArtefaktArchivSteckbriefHandler.SteckbriefException;
import com.bmw.swll6.business.smard.common.AutomatArtefaktArchivHandler;
import de.bmw.smard.base.util.Precondition;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.util.ConditionsResourceFactoryImpl;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.util.StatemachineResourceFactoryImpl;
import de.bmw.smard.modeller.statemachineset.DocumentRoot;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import de.bmw.smard.modeller.statemachineset.util.StatemachinesetResourceFactoryImpl;
import de.bmw.smard.modeller.util.cipher.EncryptedResourceFactoryImpl;
import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.PackageNotFoundException;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.edapt.common.IResourceSetFactory;
import org.eclipse.emf.edapt.internal.migration.execution.ValidationLevel;
import org.eclipse.emf.edapt.internal.migration.execution.internal.ClassLoaderFacade;
import org.eclipse.emf.edapt.migration.ReleaseUtils;
import org.eclipse.emf.edapt.migration.execution.Migrator;
import org.eclipse.emf.edapt.spi.history.History;
import org.eclipse.emf.edapt.spi.history.HistoryPackage;
import org.eclipse.emf.edapt.spi.history.Release;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class ModelLoader {
	
	private static final Logger LOGGER = Logger.getLogger(ModelLoader.class);

	protected static final String MODEL_LOAD_ERROR = "Model cannot be loaded";
	protected static final String MODEL_IO_ERROR = "Model cannot be found";
	protected static final String MODEL_MIGRATION_ERROR = "Encrypted model cannot be migrated and loaded";


	private ResourceSet _resourceSet;
	private ResourceSet _migResourceSet;
	private Migrator _mig;
	private boolean _isCallFromEngine = true;

	public ModelLoader() {
		this(true);
	}
	
	public ModelLoader(boolean isCallFromEngine) {
		_isCallFromEngine = isCallFromEngine;
		// Register the appropriate resource factory to handle file extensions.
		_migResourceSet = new ResourceSetImpl();
		_resourceSet = new ResourceSetImpl();

		registerDefaultExtensions();
		registerDefaultFactories();
		if (isCallFromEngine) {
			try {
				registerHistory();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	private void registerDefaultExtensions() {

		registerExtension("conditions", new ConditionsResourceFactoryImpl());
		registerExtension("statemachine", new StatemachineResourceFactoryImpl());
		registerExtension("statemachine_set", new StatemachinesetResourceFactoryImpl());
		registerExtension("history", new XMIResourceFactoryImpl());
		registerExtension("ecore", new XMIResourceFactoryImpl());
	}

	private void registerDefaultFactories() {
		_resourceSet.getPackageRegistry().put(StatemachinePackage.eNS_URI, StatemachinePackage.eINSTANCE);
		_resourceSet.getPackageRegistry().put(ConditionsPackage.eNS_URI, ConditionsPackage.eINSTANCE);
		_resourceSet.getPackageRegistry().put(StatemachinesetPackage.eNS_URI, StatemachinesetPackage.eINSTANCE);
	}

	private static URI createArchiveURI(String archive, String relativePath) throws IOException {
		if (archive == null) {
			throw new IOException("archive can not be null");
		} else {
			archive = archive.replace('\\', '/');
		}
		String prefixslash = "";
		if (!archive.startsWith("/")) {
			prefixslash = "/";
		}
		return URI.createURI("archive:file:" + prefixslash + archive + "!/" + relativePath);
	}

	public StateMachineSet load(File file) throws ModelLoaderException {

		LOGGER.info("Loading state machine");
		if (file == null || !file.exists() ) {
			throw new ModelLoaderException(MODEL_LOAD_ERROR, file == null ?
					"file is null" : "file does not exist");
		}
		
		String filePath = file.getAbsolutePath();
		URI stateMachineSetURI = null;
		Set<URI> projectFileURIs = new HashSet<URI>();
		
		// a statemachine file is used to determine source release of a migration
		// reason: 	conditions' namespace was empty in 64x, 
		//			statemachine_set's namespace did not change from 64x to 660
		URI statemachineURI = null;
		
		AutomatArtefaktArchivHandler artefaktHandler = new AutomatArtefaktArchivHandler(file);
		boolean isZipArchiveArtefakt = isArchive(file, artefaktHandler);
		
		
		if ( isZipArchiveArtefakt ) {
			String relativePath = "";
			try {
				relativePath = artefaktHandler.getStateMachineSetFile();
				stateMachineSetURI = createArchiveURI(filePath, relativePath);
			} catch (ArtefaktException ae) {
				throw new ModelLoaderException(MODEL_LOAD_ERROR, relativePath, ae);
			} catch (IOException e) {
				throw new ModelLoaderException(MODEL_LOAD_ERROR, relativePath + " could not be extracted", e);
			}
			
		} else {
			stateMachineSetURI = URI.createFileURI(filePath);
		}
		projectFileURIs.add(stateMachineSetURI);
		
		URI currentURI = null;
		try {
			for (String projectFile : 
				findProjectFiles( isZipArchiveArtefakt ? filePath : file.getParent(), true, false) ) {
				
				currentURI = isZipArchiveArtefakt ? 	createArchiveURI(filePath, projectFile) :
													URI.createFileURI(projectFile);
				if ( statemachineURI == null &&
						projectFile.endsWith( AutomatArtefaktArchivHandler.EXTENSION_STATEMACHINE) ) {
					statemachineURI = currentURI;
				}
				projectFileURIs.add( currentURI );
			}
		} catch (IOException e) {
			throw new ModelLoaderException(MODEL_IO_ERROR + " error searching artefact for model files", stateMachineSetURI.toFileString(), e);
		}

		boolean isEncrypted = isProbablyEncrypted(stateMachineSetURI);
		if (isEncrypted) {
			decrypt(projectFileURIs);
		} else {
			Release sourceRelease = getSourceRelease(statemachineURI);
			if (!sourceRelease.isLatestRelease()) {
				migrateModel(stateMachineSetURI, projectFileURIs, sourceRelease);
			}
		}

		Resource tmpResource = getResource(stateMachineSetURI, projectFileURIs);

		String errors = getLoadErrors(_resourceSet);
        if(tmpResource == null ||
        		tmpResource.getContents() == null ||
        		tmpResource.getContents().isEmpty() ||
        		!(tmpResource.getContents().get(0) instanceof DocumentRoot) ||
				!(((DocumentRoot)tmpResource.getContents().get(0)).getStateMachineSet() instanceof
						StateMachineSet)

				) {
            throw new ModelLoaderException(!StringUtils.nullOrEmpty(errors) ? 
            		errors : MODEL_LOAD_ERROR, stateMachineSetURI.toFileString() );
        }

        return (StateMachineSet)((DocumentRoot)tmpResource.getContents().get(0)).getStateMachineSet();
    }

	/**
	 * load StateMachine EObject from *.statemachine file
	 * @param file of contentType *.statemachine
	 * @return StateMachine resolved from file
	 * @throws ModelLoaderException
	 */
	public StateMachine loadStatemachineFile(File file) throws ModelLoaderException {
		LOGGER.info("Loading state machine file");
		if (file == null || !file.exists() ) {
			throw new ModelLoaderException(MODEL_LOAD_ERROR, file == null ?
					"file is null" : "file does not exist");
		}

		String statemachinePath = file.getAbsolutePath();
		URI statemachineURI = URI.createFileURI(statemachinePath);
		Set<URI> projectFileURIs = new HashSet<URI>();
		projectFileURIs.add(statemachineURI);

		Release sourceRelease = getSourceRelease(statemachineURI);
		if (!sourceRelease.isLatestRelease()) {
			migrateModel(statemachineURI, projectFileURIs, sourceRelease);
		}

		Resource tmpResource = getResource(statemachineURI, projectFileURIs);
		if(tmpResource == null ||
				tmpResource.getContents() == null ||
				tmpResource.getContents().isEmpty() ||
				!(tmpResource.getContents().get(0) instanceof StateMachine)) {
			throw new ModelLoaderException(MODEL_LOAD_ERROR, statemachineURI.toFileString() );
		}

		return (StateMachine) tmpResource.getContents().get(0);
	}

	private Resource getResource(URI stateMachineSetURI, Set<URI> projectFileURIs) throws ModelLoaderException {
		Resource tmpResource = _resourceSet.getResource(stateMachineSetURI, true);

		// SMARD-1195 js: there are variable references from expressions in conditions files which are never used by a real reference from a statemachine
		// we have to load them all to be able to iterate through our variables in WrapperFactory
		// yes - this means more load time!
		for (URI dep : projectFileURIs) {
			_resourceSet.getResource(dep, true);
		}

		EcoreUtil.resolveAll(_resourceSet);

		String errors = getLoadErrors(_resourceSet);
		if(!StringUtils.nullOrEmpty(errors)) {
			throw new ModelLoaderException(!StringUtils.nullOrEmpty(errors) ?
					errors : MODEL_LOAD_ERROR, stateMachineSetURI.toFileString() );
		}
		return tmpResource;
	}

	private void migrateModel(URI stateMachineSetURI, Set<URI> projectFileURIs, Release sourceRelease) throws ModelLoaderException {
		try {
			_resourceSet = _mig.migrateAndLoad(new ArrayList<URI>(projectFileURIs), sourceRelease, null, new Log4jProgressMonitor(LOGGER));
		} catch (Exception e) {
			String migrationErrors = getLoadErrors(_resourceSet);
			throw new ModelLoaderException("Migration failed for ", stateMachineSetURI.toFileString() + "\n" + migrationErrors, e);
		}
	}


	private Release getSourceRelease(URI stateMachineResourceURI ) throws ModelLoaderException {

		assert stateMachineResourceURI.fileExtension().equals(
				AutomatArtefaktArchivHandler.EXTENSION_STATEMACHINE.substring(1));

		String nsURI = ReleaseUtils.getNamespaceURI(stateMachineResourceURI);
		Map<String, Set<Release>> releaseMap = _mig.getReleaseMap();

		Set<Release> releases = releaseMap.get(nsURI);
		if(releases == null || releases.isEmpty()) {
			throw new ModelLoaderException("unable to load release map for ",
					stateMachineResourceURI.toFileString());
		}

		Release sourceRelease = releases.iterator().next();
		if (sourceRelease == null) {
			throw new ModelLoaderException("unknown source release for migration in ",
					stateMachineResourceURI.toFileString());
		}

		return sourceRelease;
	}
	
	private  boolean isArchive(File file, AutomatArtefaktArchivHandler aaaHandler) {
		try {
			return aaaHandler.isPotentialArchive() && aaaHandler.isAutomatArtefakt();
		} catch (SteckbriefException e) {
			//todo mj
			e.printStackTrace();
			return false;
		}
	}

	protected String getLoadErrors(ResourceSet resourceSet) {
		String error = "";
		for (Resource resource : resourceSet.getResources()) {
			for (Resource.Diagnostic diag : resource.getErrors()) {
				error += resource.getURI() + ": " + diag.getMessage() + "\n";
			}
		}
		return error;
	}


	private boolean isProbablyEncrypted(URI uri) {
		String nsURI = ReleaseUtils.getNamespaceURI(uri);
		return nsURI == null;
	}
	
	private void decrypt(Set<URI> modelfileURIs) throws ModelLoaderException {
		// set up decrypting resource set
		ResourceSet decryptingResourceSet = new ResourceSetImpl();
		decryptingResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("conditions",
				new EncryptedResourceFactoryImpl());
		decryptingResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("statemachine",
				new EncryptedResourceFactoryImpl());
		decryptingResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("statemachine_set",
				new EncryptedResourceFactoryImpl());
		
		// decrypt each of them
		Resource decryptedR = null;
		Resource encryptedR = null;
		for (URI aModelFileURI : modelfileURIs) {
			try {
				encryptedR = decryptingResourceSet.getResource(aModelFileURI, true);
			} catch (Exception e) {
				if (e.getCause() instanceof PackageNotFoundException) {
					throw new ModelLoaderException(MODEL_MIGRATION_ERROR +": "+ e.getMessage(), aModelFileURI.toFileString(), e);
				}
				throw new ModelLoaderException(e.getMessage(),aModelFileURI.toFileString(), e);

			}

			if (null != encryptedR) {
				decryptedR = _resourceSet.createResource(aModelFileURI);
				decryptedR.getContents().addAll(encryptedR.getContents());
				_resourceSet.getResources().add(decryptedR);
				//todo mj really needed?
				decryptedR = _resourceSet.getResource(aModelFileURI, true);				
				
				try {
					//todo mj really needed?
					decryptedR.load(null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			} else {
				System.out.println("Error: " + aModelFileURI);
			}

			if (decryptedR == null) {
				System.out.println(aModelFileURI);
			}
		}
		
		
	}


	protected void registerExtension(String extension, ResourceFactoryImpl resourceFactory) {

		_resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(extension, resourceFactory);
		_migResourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(extension, resourceFactory);

		// this is needed - otherwise a NullPointerException will be thrown on
		// Ecore Forward Constructor
		// it can not resolve test/xyz.ecore
		org.eclipse.emf.ecore.resource.Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(extension,
				resourceFactory);
	}

	private void registerHistory() throws IOException {

		// Register the package to ensure it is available during loading
		_migResourceSet.getPackageRegistry().put(HistoryPackage.eNS_URI, HistoryPackage.eINSTANCE);

		//
		// Resource historyResource =
		// migResourceSet.getResource(URI.createFileURI(historyFilePath), true);
		// needed for carmen/osgi context
		String historyFilePath = null;
		if (_isCallFromEngine) {
			URL historyFile =  getClass().getResource("/modeller.history");
			if(historyFile == null) {
				// with gradle build we have similar setup in engine and modeller - files are in /model/
				historyFile = getClass().getResource("/model/modeller.history");
			}
			historyFilePath = historyFile.toExternalForm();
		}

		Resource historyResource = _migResourceSet.getResource(URI.createURI(historyFilePath), true);
		historyResource.load(null);
		History history = (History) historyResource.getContents().get(0);

		// JS: getClass().getClassLoader() reflects the BundleClassLoader which knows Custom Migration Steps
		//     Thread....getContextClassLoader() reflects the de.bmw.smard.carmen.starter default Classloader which
		//                                       knows nothing about bundles and quits with ClassNotFoundException
		_mig = new Migrator(history, new ClassLoaderFacade(getClass().getClassLoader()));
		_mig.setLevel(ValidationLevel.NONE);

		_mig.setResourceSetFactory(new IResourceSetFactory() {
			// https://www.eclipse.org/forums/index.php?t=msg&th=1027924&goto=1651378&#msg_1651378
			@Override
			public ResourceSet createResourceSet() {
				ResourceSet resourceSet = new ResourceSetImpl();
				return resourceSet;
			}
		});
	}

	private List<String> findProjectFiles(String path, boolean filterTemplates, boolean includeStatemachineset)
			throws IOException {

		Set<String> dep = new HashSet<String>();
		List<String> files = AutomatArtefaktArchivHandler.getPathsOfFiles(path,
				AutomatArtefaktArchivHandler.EXTENSION_CONDITION);
		if (files != null) {
			for (String c : files) {
				if (filterTemplates && c.contains("/Templates/"))
					continue;
				dep.add(c);
			}
		}
		files = AutomatArtefaktArchivHandler.getPathsOfFiles(path, AutomatArtefaktArchivHandler.EXTENSION_STATEMACHINE);
		if (files != null) {
			for (String sm : files) {
				if (filterTemplates && sm.contains("/Templates/"))
					continue;
				dep.add(sm);
			}
		}

		if (includeStatemachineset)
			dep.addAll(AutomatArtefaktArchivHandler.getPathsOfFiles(path,
					AutomatArtefaktArchivHandler.EXTENSION_STATEMACHINESET));

		return new ArrayList<String>(dep);
	}
}
