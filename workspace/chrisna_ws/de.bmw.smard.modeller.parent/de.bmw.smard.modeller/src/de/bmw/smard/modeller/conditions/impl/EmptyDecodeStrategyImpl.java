package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.EmptyDecodeStrategy;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Empty Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmptyDecodeStrategyImpl extends DecodeStrategyImpl implements EmptyDecodeStrategy {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected EmptyDecodeStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.EMPTY_DECODE_STRATEGY;
    }

} //EmptyDecodeStrategyImpl
