package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.TransitionCheckExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl#getStayActive <em>Stay Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl#getCheckTransition <em>Check Transition</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl#getStayActiveTmplParam <em>Stay Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionCheckExpressionImpl extends ExpressionImpl implements TransitionCheckExpression {
    /**
     * The default value of the '{@link #getStayActive() <em>Stay Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStayActive()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum STAY_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.FALSE;

    /**
     * The cached value of the '{@link #getStayActive() <em>Stay Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStayActive()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum stayActive = STAY_ACTIVE_EDEFAULT;

    /**
     * The cached value of the '{@link #getCheckTransition() <em>Check Transition</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCheckTransition()
     * @generated
     * @ordered
     */
    protected Transition checkTransition;

    /**
     * The default value of the '{@link #getStayActiveTmplParam() <em>Stay Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #getStayActiveTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STAY_ACTIVE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStayActiveTmplParam() <em>Stay Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStayActiveTmplParam()
     * @generated
     * @ordered
     */
    protected String stayActiveTmplParam = STAY_ACTIVE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected TransitionCheckExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getCheckTransition() != null) {
            return getCheckTransition().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.TRANSITION_CHECK_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getStayActive() {
        return stayActive;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStayActive(BooleanOrTemplatePlaceholderEnum newStayActive) {
        BooleanOrTemplatePlaceholderEnum oldStayActive = stayActive;
        stayActive = newStayActive == null ? STAY_ACTIVE_EDEFAULT : newStayActive;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE, oldStayActive, stayActive));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Transition getCheckTransition() {
        if (checkTransition != null && checkTransition.eIsProxy()) {
            InternalEObject oldCheckTransition = (InternalEObject) checkTransition;
            checkTransition = (Transition) eResolveProxy(oldCheckTransition);
            if (checkTransition != oldCheckTransition) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION, oldCheckTransition, checkTransition));
            }
        }
        return checkTransition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Transition basicGetCheckTransition() {
        return checkTransition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCheckTransition(Transition newCheckTransition) {
        Transition oldCheckTransition = checkTransition;
        checkTransition = newCheckTransition;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION, oldCheckTransition, checkTransition));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStayActiveTmplParam() {
        return stayActiveTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStayActiveTmplParam(String newStayActiveTmplParam) {
        String oldStayActiveTmplParam = stayActiveTmplParam;
        stayActiveTmplParam = newStayActiveTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM, oldStayActiveTmplParam, stayActiveTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStayActive(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STAY_ACTIVE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(stayActive)
                        .error("_Validation_Conditions_TransitionCheckExpression_StayActive_IsNull")
                        .build())

                .with(Validators.checkTemplate(stayActive)
                        .templateType(BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(stayActiveTmplParam)
                        .containsParameterError("_Validation_Conditions_TransitionCheckExpression_StayActive_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_TransitionCheckExpression_StayActiveTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_TransitionCheckExpression_StayActiveTmplParam_NotAValidPlaceholder")
                        .illegalValueError("_Validation_Conditions_TransitionCheckExpression_StayActive_IllegalStayActive")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidTransition(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TRANSITION_CHECK_EXPRESSION__IS_VALID_HAS_VALID_TRANSITION)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(TemplateUtils.getTemplateCategorization(this).isExportable())
                        .and(!TemplateUtils.getTemplateCategorization(checkTransition).isExportable())
                        .error("_Validation_Conditions_TransitionCheckExpression_CheckTransition_IsTemplateTransition")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
                return getStayActive();
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
                if (resolve) return getCheckTransition();
                return basicGetCheckTransition();
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
                return getStayActiveTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
                setStayActive((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
                setCheckTransition((Transition) newValue);
                return;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
                setStayActiveTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
                setStayActive(STAY_ACTIVE_EDEFAULT);
                return;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
                setCheckTransition((Transition) null);
                return;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
                setStayActiveTmplParam(STAY_ACTIVE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE:
                return stayActive != STAY_ACTIVE_EDEFAULT;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION:
                return checkTransition != null;
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM:
                return STAY_ACTIVE_TMPL_PARAM_EDEFAULT == null ? stayActiveTmplParam != null : !STAY_ACTIVE_TMPL_PARAM_EDEFAULT.equals(stayActiveTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (stayActive: ");
        result.append(stayActive);
        result.append(", stayActiveTmplParam: ");
        result.append(stayActiveTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<Transition> getTransitionDependencies() {
        return ECollections.singletonEList(getCheckTransition());
    }

} // TransitionCheckExpressionImpl
