/**
 * <copyright>
 * </copyright>
 * $Id$
 */
package de.bmw.smard.modeller.statemachineset;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage
 * @generated
 */
public interface StatemachinesetFactory extends EFactory {
    /**
     * The singleton instance of the factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    StatemachinesetFactory eINSTANCE = de.bmw.smard.modeller.statemachineset.impl.StatemachinesetFactoryImpl.init();

    /**
     * Returns a new object of class '<em>Document Root</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Document Root</em>'.
     * @generated
     */
    DocumentRoot createDocumentRoot();

    /**
     * Returns a new object of class '<em>General Info</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>General Info</em>'.
     * @generated
     */
    GeneralInfo createGeneralInfo();

    /**
     * Returns a new object of class '<em>Output</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Output</em>'.
     * @generated
     */
    Output createOutput();

    /**
     * Returns a new object of class '<em>State Machine Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>State Machine Set</em>'.
     * @generated
     */
    StateMachineSet createStateMachineSet();

    /**
     * Returns a new object of class '<em>Key Value Pair User Config</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Key Value Pair User Config</em>'.
     * @generated
     */
    KeyValuePairUserConfig createKeyValuePairUserConfig();

    /**
     * Returns a new object of class '<em>Execution Config</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Execution Config</em>'.
     * @generated
     */
    ExecutionConfig createExecutionConfig();

    /**
     * Returns the package supported by this factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    StatemachinesetPackage getStatemachinesetPackage();

} //StatemachinesetFactory
