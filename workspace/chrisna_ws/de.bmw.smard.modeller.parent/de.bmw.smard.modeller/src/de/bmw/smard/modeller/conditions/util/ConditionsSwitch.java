package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.statemachine.SmardTraceElement;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage
 * @generated
 */
public class ConditionsSwitch<T> {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ConditionsPackage modelPackage;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ConditionsSwitch() {
        if (modelPackage == null) {
            modelPackage = ConditionsPackage.eINSTANCE;
        }
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    public T doSwitch(EObject theEObject) {
        return doSwitch(theEObject.eClass(), theEObject);
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    protected T doSwitch(EClass theEClass, EObject theEObject) {
        if (theEClass.eContainer() == modelPackage) {
            return doSwitch(theEClass.getClassifierID(), theEObject);
        } else {
            List<EClass> eSuperTypes = theEClass.getESuperTypes();
            return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(eSuperTypes.get(0), theEObject);
        }
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
            case ConditionsPackage.DOCUMENT_ROOT: {
                DocumentRoot documentRoot = (DocumentRoot) theEObject;
                T result = caseDocumentRoot(documentRoot);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CONDITION_SET: {
                ConditionSet conditionSet = (ConditionSet) theEObject;
                T result = caseConditionSet(conditionSet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CONDITION: {
                Condition condition = (Condition) theEObject;
                T result = caseCondition(condition);
                if (result == null) result = caseBaseClassWithID(condition);
                if (result == null) result = caseIVariableReaderWriter(condition);
                if (result == null) result = caseIStateTransitionReference(condition);
                if (result == null) result = caseBusMessageReferable(condition);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SIGNAL_COMPARISON_EXPRESSION: {
                SignalComparisonExpression signalComparisonExpression = (SignalComparisonExpression) theEObject;
                T result = caseSignalComparisonExpression(signalComparisonExpression);
                if (result == null) result = caseExpression(signalComparisonExpression);
                if (result == null) result = caseIOperation(signalComparisonExpression);
                if (result == null) result = caseBaseClassWithID(signalComparisonExpression);
                if (result == null) result = caseIStateTransitionReference(signalComparisonExpression);
                if (result == null) result = caseISignalComparisonExpressionOperand(signalComparisonExpression);
                if (result == null) result = caseIVariableReaderWriter(signalComparisonExpression);
                if (result == null) result = caseBusMessageReferable(signalComparisonExpression);
                if (result == null) result = caseIOperand(signalComparisonExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CAN_MESSAGE_CHECK_EXPRESSION: {
                CanMessageCheckExpression canMessageCheckExpression = (CanMessageCheckExpression) theEObject;
                T result = caseCanMessageCheckExpression(canMessageCheckExpression);
                if (result == null) result = caseExpression(canMessageCheckExpression);
                if (result == null) result = caseBaseClassWithID(canMessageCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(canMessageCheckExpression);
                if (result == null) result = caseIStateTransitionReference(canMessageCheckExpression);
                if (result == null) result = caseBusMessageReferable(canMessageCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION: {
                LinMessageCheckExpression linMessageCheckExpression = (LinMessageCheckExpression) theEObject;
                T result = caseLinMessageCheckExpression(linMessageCheckExpression);
                if (result == null) result = caseExpression(linMessageCheckExpression);
                if (result == null) result = caseBaseClassWithID(linMessageCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(linMessageCheckExpression);
                if (result == null) result = caseIStateTransitionReference(linMessageCheckExpression);
                if (result == null) result = caseBusMessageReferable(linMessageCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STATE_CHECK_EXPRESSION: {
                StateCheckExpression stateCheckExpression = (StateCheckExpression) theEObject;
                T result = caseStateCheckExpression(stateCheckExpression);
                if (result == null) result = caseExpression(stateCheckExpression);
                if (result == null) result = caseBaseClassWithID(stateCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(stateCheckExpression);
                if (result == null) result = caseIStateTransitionReference(stateCheckExpression);
                if (result == null) result = caseBusMessageReferable(stateCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TIMING_EXPRESSION: {
                TimingExpression timingExpression = (TimingExpression) theEObject;
                T result = caseTimingExpression(timingExpression);
                if (result == null) result = caseExpression(timingExpression);
                if (result == null) result = caseBaseClassWithID(timingExpression);
                if (result == null) result = caseIVariableReaderWriter(timingExpression);
                if (result == null) result = caseIStateTransitionReference(timingExpression);
                if (result == null) result = caseBusMessageReferable(timingExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TRUE_EXPRESSION: {
                TrueExpression trueExpression = (TrueExpression) theEObject;
                T result = caseTrueExpression(trueExpression);
                if (result == null) result = caseExpression(trueExpression);
                if (result == null) result = caseBaseClassWithID(trueExpression);
                if (result == null) result = caseIVariableReaderWriter(trueExpression);
                if (result == null) result = caseIStateTransitionReference(trueExpression);
                if (result == null) result = caseBusMessageReferable(trueExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.LOGICAL_EXPRESSION: {
                LogicalExpression logicalExpression = (LogicalExpression) theEObject;
                T result = caseLogicalExpression(logicalExpression);
                if (result == null) result = caseExpression(logicalExpression);
                if (result == null) result = caseBaseClassWithID(logicalExpression);
                if (result == null) result = caseIVariableReaderWriter(logicalExpression);
                if (result == null) result = caseIStateTransitionReference(logicalExpression);
                if (result == null) result = caseBusMessageReferable(logicalExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION: {
                ReferenceConditionExpression referenceConditionExpression = (ReferenceConditionExpression) theEObject;
                T result = caseReferenceConditionExpression(referenceConditionExpression);
                if (result == null) result = caseExpression(referenceConditionExpression);
                if (result == null) result = caseBaseClassWithID(referenceConditionExpression);
                if (result == null) result = caseIVariableReaderWriter(referenceConditionExpression);
                if (result == null) result = caseIStateTransitionReference(referenceConditionExpression);
                if (result == null) result = caseBusMessageReferable(referenceConditionExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TRANSITION_CHECK_EXPRESSION: {
                TransitionCheckExpression transitionCheckExpression = (TransitionCheckExpression) theEObject;
                T result = caseTransitionCheckExpression(transitionCheckExpression);
                if (result == null) result = caseExpression(transitionCheckExpression);
                if (result == null) result = caseBaseClassWithID(transitionCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(transitionCheckExpression);
                if (result == null) result = caseIStateTransitionReference(transitionCheckExpression);
                if (result == null) result = caseBusMessageReferable(transitionCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION: {
                FlexRayMessageCheckExpression flexRayMessageCheckExpression = (FlexRayMessageCheckExpression) theEObject;
                T result = caseFlexRayMessageCheckExpression(flexRayMessageCheckExpression);
                if (result == null) result = caseExpression(flexRayMessageCheckExpression);
                if (result == null) result = caseBaseClassWithID(flexRayMessageCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(flexRayMessageCheckExpression);
                if (result == null) result = caseIStateTransitionReference(flexRayMessageCheckExpression);
                if (result == null) result = caseBusMessageReferable(flexRayMessageCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STOP_EXPRESSION: {
                StopExpression stopExpression = (StopExpression) theEObject;
                T result = caseStopExpression(stopExpression);
                if (result == null) result = caseExpression(stopExpression);
                if (result == null) result = caseBaseClassWithID(stopExpression);
                if (result == null) result = caseIVariableReaderWriter(stopExpression);
                if (result == null) result = caseIStateTransitionReference(stopExpression);
                if (result == null) result = caseBusMessageReferable(stopExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.COMPARATOR_SIGNAL: {
                ComparatorSignal comparatorSignal = (ComparatorSignal) theEObject;
                T result = caseComparatorSignal(comparatorSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(comparatorSignal);
                if (result == null) result = caseIOperand(comparatorSignal);
                if (result == null) result = caseBusMessageReferable(comparatorSignal);
                if (result == null) result = caseIVariableReaderWriter(comparatorSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CONSTANT_COMPARATOR_VALUE: {
                ConstantComparatorValue constantComparatorValue = (ConstantComparatorValue) theEObject;
                T result = caseConstantComparatorValue(constantComparatorValue);
                if (result == null) result = caseComparatorSignal(constantComparatorValue);
                if (result == null) result = caseISignalComparisonExpressionOperand(constantComparatorValue);
                if (result == null) result = caseIOperand(constantComparatorValue);
                if (result == null) result = caseBusMessageReferable(constantComparatorValue);
                if (result == null) result = caseIVariableReaderWriter(constantComparatorValue);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CALCULATION_EXPRESSION: {
                CalculationExpression calculationExpression = (CalculationExpression) theEObject;
                T result = caseCalculationExpression(calculationExpression);
                if (result == null) result = caseComparatorSignal(calculationExpression);
                if (result == null) result = caseIOperation(calculationExpression);
                if (result == null) result = caseISignalComparisonExpressionOperand(calculationExpression);
                if (result == null) result = caseIOperand(calculationExpression);
                if (result == null) result = caseBusMessageReferable(calculationExpression);
                if (result == null) result = caseIVariableReaderWriter(calculationExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.EXPRESSION: {
                Expression expression = (Expression) theEObject;
                T result = caseExpression(expression);
                if (result == null) result = caseBaseClassWithID(expression);
                if (result == null) result = caseIVariableReaderWriter(expression);
                if (result == null) result = caseIStateTransitionReference(expression);
                if (result == null) result = caseBusMessageReferable(expression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VARIABLE_REFERENCE: {
                VariableReference variableReference = (VariableReference) theEObject;
                T result = caseVariableReference(variableReference);
                if (result == null) result = caseComparatorSignal(variableReference);
                if (result == null) result = caseINumericOperand(variableReference);
                if (result == null) result = caseIStringOperand(variableReference);
                if (result == null) result = caseISignalComparisonExpressionOperand(variableReference);
                if (result == null) result = caseIComputeVariableActionOperand(variableReference);
                if (result == null) result = caseIOperand(variableReference);
                if (result == null) result = caseBusMessageReferable(variableReference);
                if (result == null) result = caseIVariableReaderWriter(variableReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CONDITIONS_DOCUMENT: {
                ConditionsDocument conditionsDocument = (ConditionsDocument) theEObject;
                T result = caseConditionsDocument(conditionsDocument);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VARIABLE_SET: {
                VariableSet variableSet = (VariableSet) theEObject;
                T result = caseVariableSet(variableSet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VARIABLE: {
                Variable variable = (Variable) theEObject;
                T result = caseVariable(variable);
                if (result == null) result = caseAbstractVariable(variable);
                if (result == null) result = caseBaseClassWithID(variable);
                if (result == null) result = caseIVariableReaderWriter(variable);
                if (result == null) result = caseBusMessageReferable(variable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SIGNAL_VARIABLE: {
                SignalVariable signalVariable = (SignalVariable) theEObject;
                T result = caseSignalVariable(signalVariable);
                if (result == null) result = caseVariable(signalVariable);
                if (result == null) result = caseAbstractVariable(signalVariable);
                if (result == null) result = caseBaseClassWithID(signalVariable);
                if (result == null) result = caseIVariableReaderWriter(signalVariable);
                if (result == null) result = caseBusMessageReferable(signalVariable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VALUE_VARIABLE: {
                ValueVariable valueVariable = (ValueVariable) theEObject;
                T result = caseValueVariable(valueVariable);
                if (result == null) result = caseVariable(valueVariable);
                if (result == null) result = caseAbstractVariable(valueVariable);
                if (result == null) result = caseBaseClassWithID(valueVariable);
                if (result == null) result = caseIVariableReaderWriter(valueVariable);
                if (result == null) result = caseBusMessageReferable(valueVariable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VARIABLE_FORMAT: {
                VariableFormat variableFormat = (VariableFormat) theEObject;
                T result = caseVariableFormat(variableFormat);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_OBSERVER: {
                AbstractObserver abstractObserver = (AbstractObserver) theEObject;
                T result = caseAbstractObserver(abstractObserver);
                if (result == null) result = caseBaseClassWithSourceReference(abstractObserver);
                if (result == null) result = caseSmardTraceElement(abstractObserver);
                if (result == null) result = caseBusMessageReferable(abstractObserver);
                if (result == null) result = caseBaseClassWithID(abstractObserver);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.OBSERVER_VALUE_RANGE: {
                ObserverValueRange observerValueRange = (ObserverValueRange) theEObject;
                T result = caseObserverValueRange(observerValueRange);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SIGNAL_OBSERVER: {
                SignalObserver signalObserver = (SignalObserver) theEObject;
                T result = caseSignalObserver(signalObserver);
                if (result == null) result = caseAbstractObserver(signalObserver);
                if (result == null) result = caseBaseClassWithSourceReference(signalObserver);
                if (result == null) result = caseSmardTraceElement(signalObserver);
                if (result == null) result = caseBusMessageReferable(signalObserver);
                if (result == null) result = caseBaseClassWithID(signalObserver);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SIGNAL_REFERENCE_SET: {
                SignalReferenceSet signalReferenceSet = (SignalReferenceSet) theEObject;
                T result = caseSignalReferenceSet(signalReferenceSet);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SIGNAL_REFERENCE: {
                SignalReference signalReference = (SignalReference) theEObject;
                T result = caseSignalReference(signalReference);
                if (result == null) result = caseComparatorSignal(signalReference);
                if (result == null) result = caseISignalOrReference(signalReference);
                if (result == null) result = caseINumericOperand(signalReference);
                if (result == null) result = caseIStringOperand(signalReference);
                if (result == null) result = caseISignalComparisonExpressionOperand(signalReference);
                if (result == null) result = caseIComputeVariableActionOperand(signalReference);
                if (result == null) result = caseIOperand(signalReference);
                if (result == null) result = caseBusMessageReferable(signalReference);
                if (result == null) result = caseIVariableReaderWriter(signalReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ISIGNAL_OR_REFERENCE: {
                ISignalOrReference iSignalOrReference = (ISignalOrReference) theEObject;
                T result = caseISignalOrReference(iSignalOrReference);
                if (result == null) result = caseISignalComparisonExpressionOperand(iSignalOrReference);
                if (result == null) result = caseIOperand(iSignalOrReference);
                if (result == null) result = caseBusMessageReferable(iSignalOrReference);
                if (result == null) result = caseIVariableReaderWriter(iSignalOrReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.BIT_PATTERN_COMPARATOR_VALUE: {
                BitPatternComparatorValue bitPatternComparatorValue = (BitPatternComparatorValue) theEObject;
                T result = caseBitPatternComparatorValue(bitPatternComparatorValue);
                if (result == null) result = caseComparatorSignal(bitPatternComparatorValue);
                if (result == null) result = caseISignalComparisonExpressionOperand(bitPatternComparatorValue);
                if (result == null) result = caseIOperand(bitPatternComparatorValue);
                if (result == null) result = caseBusMessageReferable(bitPatternComparatorValue);
                if (result == null) result = caseIVariableReaderWriter(bitPatternComparatorValue);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.NOT_EXPRESSION: {
                NotExpression notExpression = (NotExpression) theEObject;
                T result = caseNotExpression(notExpression);
                if (result == null) result = caseExpression(notExpression);
                if (result == null) result = caseBaseClassWithID(notExpression);
                if (result == null) result = caseIVariableReaderWriter(notExpression);
                if (result == null) result = caseIStateTransitionReference(notExpression);
                if (result == null) result = caseBusMessageReferable(notExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.IOPERAND: {
                IOperand iOperand = (IOperand) theEObject;
                T result = caseIOperand(iOperand);
                if (result == null) result = caseIVariableReaderWriter(iOperand);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ICOMPUTE_VARIABLE_ACTION_OPERAND: {
                IComputeVariableActionOperand iComputeVariableActionOperand = (IComputeVariableActionOperand) theEObject;
                T result = caseIComputeVariableActionOperand(iComputeVariableActionOperand);
                if (result == null) result = caseIOperand(iComputeVariableActionOperand);
                if (result == null) result = caseIVariableReaderWriter(iComputeVariableActionOperand);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ISTRING_OPERAND: {
                IStringOperand iStringOperand = (IStringOperand) theEObject;
                T result = caseIStringOperand(iStringOperand);
                if (result == null) result = caseIComputeVariableActionOperand(iStringOperand);
                if (result == null) result = caseIOperand(iStringOperand);
                if (result == null) result = caseIVariableReaderWriter(iStringOperand);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ISIGNAL_COMPARISON_EXPRESSION_OPERAND: {
                ISignalComparisonExpressionOperand iSignalComparisonExpressionOperand = (ISignalComparisonExpressionOperand) theEObject;
                T result = caseISignalComparisonExpressionOperand(iSignalComparisonExpressionOperand);
                if (result == null) result = caseIOperand(iSignalComparisonExpressionOperand);
                if (result == null) result = caseBusMessageReferable(iSignalComparisonExpressionOperand);
                if (result == null) result = caseIVariableReaderWriter(iSignalComparisonExpressionOperand);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.IOPERATION: {
                IOperation iOperation = (IOperation) theEObject;
                T result = caseIOperation(iOperation);
                if (result == null) result = caseISignalComparisonExpressionOperand(iOperation);
                if (result == null) result = caseIOperand(iOperation);
                if (result == null) result = caseIVariableReaderWriter(iOperation);
                if (result == null) result = caseBusMessageReferable(iOperation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ISTRING_OPERATION: {
                IStringOperation iStringOperation = (IStringOperation) theEObject;
                T result = caseIStringOperation(iStringOperation);
                if (result == null) result = caseIOperation(iStringOperation);
                if (result == null) result = caseIStringOperand(iStringOperation);
                if (result == null) result = caseISignalComparisonExpressionOperand(iStringOperation);
                if (result == null) result = caseIComputeVariableActionOperand(iStringOperation);
                if (result == null) result = caseIOperand(iStringOperation);
                if (result == null) result = caseIVariableReaderWriter(iStringOperation);
                if (result == null) result = caseBusMessageReferable(iStringOperation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.INUMERIC_OPERAND: {
                INumericOperand iNumericOperand = (INumericOperand) theEObject;
                T result = caseINumericOperand(iNumericOperand);
                if (result == null) result = caseIComputeVariableActionOperand(iNumericOperand);
                if (result == null) result = caseIOperand(iNumericOperand);
                if (result == null) result = caseIVariableReaderWriter(iNumericOperand);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.INUMERIC_OPERATION: {
                INumericOperation iNumericOperation = (INumericOperation) theEObject;
                T result = caseINumericOperation(iNumericOperation);
                if (result == null) result = caseINumericOperand(iNumericOperation);
                if (result == null) result = caseIOperation(iNumericOperation);
                if (result == null) result = caseIComputeVariableActionOperand(iNumericOperation);
                if (result == null) result = caseISignalComparisonExpressionOperand(iNumericOperation);
                if (result == null) result = caseIOperand(iNumericOperation);
                if (result == null) result = caseIVariableReaderWriter(iNumericOperation);
                if (result == null) result = caseBusMessageReferable(iNumericOperation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VALUE_VARIABLE_OBSERVER: {
                ValueVariableObserver valueVariableObserver = (ValueVariableObserver) theEObject;
                T result = caseValueVariableObserver(valueVariableObserver);
                if (result == null) result = caseAbstractObserver(valueVariableObserver);
                if (result == null) result = caseBaseClassWithSourceReference(valueVariableObserver);
                if (result == null) result = caseSmardTraceElement(valueVariableObserver);
                if (result == null) result = caseBusMessageReferable(valueVariableObserver);
                if (result == null) result = caseBaseClassWithID(valueVariableObserver);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PARSE_STRING_TO_DOUBLE: {
                ParseStringToDouble parseStringToDouble = (ParseStringToDouble) theEObject;
                T result = caseParseStringToDouble(parseStringToDouble);
                if (result == null) result = caseINumericOperation(parseStringToDouble);
                if (result == null) result = caseINumericOperand(parseStringToDouble);
                if (result == null) result = caseIOperation(parseStringToDouble);
                if (result == null) result = caseIComputeVariableActionOperand(parseStringToDouble);
                if (result == null) result = caseISignalComparisonExpressionOperand(parseStringToDouble);
                if (result == null) result = caseIOperand(parseStringToDouble);
                if (result == null) result = caseIVariableReaderWriter(parseStringToDouble);
                if (result == null) result = caseBusMessageReferable(parseStringToDouble);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STRING_EXPRESSION: {
                StringExpression stringExpression = (StringExpression) theEObject;
                T result = caseStringExpression(stringExpression);
                if (result == null) result = caseExpression(stringExpression);
                if (result == null) result = caseBaseClassWithID(stringExpression);
                if (result == null) result = caseIVariableReaderWriter(stringExpression);
                if (result == null) result = caseIStateTransitionReference(stringExpression);
                if (result == null) result = caseBusMessageReferable(stringExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.MATCHES: {
                Matches matches = (Matches) theEObject;
                T result = caseMatches(matches);
                if (result == null) result = caseStringExpression(matches);
                if (result == null) result = caseRegexOperation(matches);
                if (result == null) result = caseExpression(matches);
                if (result == null) result = caseBaseClassWithID(matches);
                if (result == null) result = caseIVariableReaderWriter(matches);
                if (result == null) result = caseIStateTransitionReference(matches);
                if (result == null) result = caseBusMessageReferable(matches);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.EXTRACT: {
                Extract extract = (Extract) theEObject;
                T result = caseExtract(extract);
                if (result == null) result = caseIStringOperation(extract);
                if (result == null) result = caseRegexOperation(extract);
                if (result == null) result = caseIOperation(extract);
                if (result == null) result = caseIStringOperand(extract);
                if (result == null) result = caseISignalComparisonExpressionOperand(extract);
                if (result == null) result = caseIComputeVariableActionOperand(extract);
                if (result == null) result = caseIOperand(extract);
                if (result == null) result = caseIVariableReaderWriter(extract);
                if (result == null) result = caseBusMessageReferable(extract);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SUBSTRING: {
                Substring substring = (Substring) theEObject;
                T result = caseSubstring(substring);
                if (result == null) result = caseIStringOperation(substring);
                if (result == null) result = caseIOperation(substring);
                if (result == null) result = caseIStringOperand(substring);
                if (result == null) result = caseISignalComparisonExpressionOperand(substring);
                if (result == null) result = caseIComputeVariableActionOperand(substring);
                if (result == null) result = caseIOperand(substring);
                if (result == null) result = caseIVariableReaderWriter(substring);
                if (result == null) result = caseBusMessageReferable(substring);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING: {
                ParseNumericToString parseNumericToString = (ParseNumericToString) theEObject;
                T result = caseParseNumericToString(parseNumericToString);
                if (result == null) result = caseIStringOperation(parseNumericToString);
                if (result == null) result = caseIOperation(parseNumericToString);
                if (result == null) result = caseIStringOperand(parseNumericToString);
                if (result == null) result = caseISignalComparisonExpressionOperand(parseNumericToString);
                if (result == null) result = caseIComputeVariableActionOperand(parseNumericToString);
                if (result == null) result = caseIOperand(parseNumericToString);
                if (result == null) result = caseIVariableReaderWriter(parseNumericToString);
                if (result == null) result = caseBusMessageReferable(parseNumericToString);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STRING_LENGTH: {
                StringLength stringLength = (StringLength) theEObject;
                T result = caseStringLength(stringLength);
                if (result == null) result = caseINumericOperation(stringLength);
                if (result == null) result = caseINumericOperand(stringLength);
                if (result == null) result = caseIOperation(stringLength);
                if (result == null) result = caseIComputeVariableActionOperand(stringLength);
                if (result == null) result = caseISignalComparisonExpressionOperand(stringLength);
                if (result == null) result = caseIOperand(stringLength);
                if (result == null) result = caseIVariableReaderWriter(stringLength);
                if (result == null) result = caseBusMessageReferable(stringLength);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_MESSAGE: {
                AbstractMessage abstractMessage = (AbstractMessage) theEObject;
                T result = caseAbstractMessage(abstractMessage);
                if (result == null) result = caseBaseClassWithSourceReference(abstractMessage);
                if (result == null) result = caseBaseClassWithID(abstractMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SOME_IP_MESSAGE: {
                SomeIPMessage someIPMessage = (SomeIPMessage) theEObject;
                T result = caseSomeIPMessage(someIPMessage);
                if (result == null) result = caseAbstractBusMessage(someIPMessage);
                if (result == null) result = caseAbstractMessage(someIPMessage);
                if (result == null) result = caseBaseClassWithSourceReference(someIPMessage);
                if (result == null) result = caseBaseClassWithID(someIPMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_SIGNAL: {
                AbstractSignal abstractSignal = (AbstractSignal) theEObject;
                T result = caseAbstractSignal(abstractSignal);
                if (result == null) result = caseBaseClassWithSourceReference(abstractSignal);
                if (result == null) result = caseISignalOrReference(abstractSignal);
                if (result == null) result = caseBaseClassWithID(abstractSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(abstractSignal);
                if (result == null) result = caseIOperand(abstractSignal);
                if (result == null) result = caseBusMessageReferable(abstractSignal);
                if (result == null) result = caseIVariableReaderWriter(abstractSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CONTAINER_SIGNAL: {
                ContainerSignal containerSignal = (ContainerSignal) theEObject;
                T result = caseContainerSignal(containerSignal);
                if (result == null) result = caseAbstractSignal(containerSignal);
                if (result == null) result = caseBaseClassWithSourceReference(containerSignal);
                if (result == null) result = caseISignalOrReference(containerSignal);
                if (result == null) result = caseBaseClassWithID(containerSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(containerSignal);
                if (result == null) result = caseIOperand(containerSignal);
                if (result == null) result = caseBusMessageReferable(containerSignal);
                if (result == null) result = caseIVariableReaderWriter(containerSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_FILTER: {
                AbstractFilter abstractFilter = (AbstractFilter) theEObject;
                T result = caseAbstractFilter(abstractFilter);
                if (result == null) result = caseBaseClassWithSourceReference(abstractFilter);
                if (result == null) result = caseBaseClassWithID(abstractFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ETHERNET_FILTER: {
                EthernetFilter ethernetFilter = (EthernetFilter) theEObject;
                T result = caseEthernetFilter(ethernetFilter);
                if (result == null) result = caseAbstractFilter(ethernetFilter);
                if (result == null) result = caseBaseClassWithSourceReference(ethernetFilter);
                if (result == null) result = caseBaseClassWithID(ethernetFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UDP_FILTER: {
                UDPFilter udpFilter = (UDPFilter) theEObject;
                T result = caseUDPFilter(udpFilter);
                if (result == null) result = caseTPFilter(udpFilter);
                if (result == null) result = caseAbstractFilter(udpFilter);
                if (result == null) result = caseBaseClassWithSourceReference(udpFilter);
                if (result == null) result = caseBaseClassWithID(udpFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TCP_FILTER: {
                TCPFilter tcpFilter = (TCPFilter) theEObject;
                T result = caseTCPFilter(tcpFilter);
                if (result == null) result = caseTPFilter(tcpFilter);
                if (result == null) result = caseAbstractFilter(tcpFilter);
                if (result == null) result = caseBaseClassWithSourceReference(tcpFilter);
                if (result == null) result = caseBaseClassWithID(tcpFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.IPV4_FILTER: {
                IPv4Filter iPv4Filter = (IPv4Filter) theEObject;
                T result = caseIPv4Filter(iPv4Filter);
                if (result == null) result = caseAbstractFilter(iPv4Filter);
                if (result == null) result = caseBaseClassWithSourceReference(iPv4Filter);
                if (result == null) result = caseBaseClassWithID(iPv4Filter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SOME_IP_FILTER: {
                SomeIPFilter someIPFilter = (SomeIPFilter) theEObject;
                T result = caseSomeIPFilter(someIPFilter);
                if (result == null) result = caseAbstractFilter(someIPFilter);
                if (result == null) result = caseBaseClassWithSourceReference(someIPFilter);
                if (result == null) result = caseBaseClassWithID(someIPFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.FOR_EACH_EXPRESSION: {
                ForEachExpression forEachExpression = (ForEachExpression) theEObject;
                T result = caseForEachExpression(forEachExpression);
                if (result == null) result = caseExpression(forEachExpression);
                if (result == null) result = caseBaseClassWithID(forEachExpression);
                if (result == null) result = caseIVariableReaderWriter(forEachExpression);
                if (result == null) result = caseIStateTransitionReference(forEachExpression);
                if (result == null) result = caseBusMessageReferable(forEachExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.MESSAGE_CHECK_EXPRESSION: {
                MessageCheckExpression messageCheckExpression = (MessageCheckExpression) theEObject;
                T result = caseMessageCheckExpression(messageCheckExpression);
                if (result == null) result = caseExpression(messageCheckExpression);
                if (result == null) result = caseBaseClassWithID(messageCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(messageCheckExpression);
                if (result == null) result = caseIStateTransitionReference(messageCheckExpression);
                if (result == null) result = caseBusMessageReferable(messageCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SOME_IPSD_FILTER: {
                SomeIPSDFilter someIPSDFilter = (SomeIPSDFilter) theEObject;
                T result = caseSomeIPSDFilter(someIPSDFilter);
                if (result == null) result = caseAbstractFilter(someIPSDFilter);
                if (result == null) result = caseBaseClassWithSourceReference(someIPSDFilter);
                if (result == null) result = caseBaseClassWithID(someIPSDFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SOME_IPSD_MESSAGE: {
                SomeIPSDMessage someIPSDMessage = (SomeIPSDMessage) theEObject;
                T result = caseSomeIPSDMessage(someIPSDMessage);
                if (result == null) result = caseAbstractBusMessage(someIPSDMessage);
                if (result == null) result = caseAbstractMessage(someIPSDMessage);
                if (result == null) result = caseBaseClassWithSourceReference(someIPSDMessage);
                if (result == null) result = caseBaseClassWithID(someIPSDMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.DOUBLE_SIGNAL: {
                DoubleSignal doubleSignal = (DoubleSignal) theEObject;
                T result = caseDoubleSignal(doubleSignal);
                if (result == null) result = caseAbstractSignal(doubleSignal);
                if (result == null) result = caseBaseClassWithSourceReference(doubleSignal);
                if (result == null) result = caseISignalOrReference(doubleSignal);
                if (result == null) result = caseBaseClassWithID(doubleSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(doubleSignal);
                if (result == null) result = caseIOperand(doubleSignal);
                if (result == null) result = caseBusMessageReferable(doubleSignal);
                if (result == null) result = caseIVariableReaderWriter(doubleSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STRING_SIGNAL: {
                StringSignal stringSignal = (StringSignal) theEObject;
                T result = caseStringSignal(stringSignal);
                if (result == null) result = caseAbstractSignal(stringSignal);
                if (result == null) result = caseBaseClassWithSourceReference(stringSignal);
                if (result == null) result = caseISignalOrReference(stringSignal);
                if (result == null) result = caseBaseClassWithID(stringSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(stringSignal);
                if (result == null) result = caseIOperand(stringSignal);
                if (result == null) result = caseBusMessageReferable(stringSignal);
                if (result == null) result = caseIVariableReaderWriter(stringSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.DECODE_STRATEGY: {
                DecodeStrategy decodeStrategy = (DecodeStrategy) theEObject;
                T result = caseDecodeStrategy(decodeStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.DOUBLE_DECODE_STRATEGY: {
                DoubleDecodeStrategy doubleDecodeStrategy = (DoubleDecodeStrategy) theEObject;
                T result = caseDoubleDecodeStrategy(doubleDecodeStrategy);
                if (result == null) result = caseDecodeStrategy(doubleDecodeStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.EXTRACT_STRATEGY: {
                ExtractStrategy extractStrategy = (ExtractStrategy) theEObject;
                T result = caseExtractStrategy(extractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY: {
                UniversalPayloadExtractStrategy universalPayloadExtractStrategy = (UniversalPayloadExtractStrategy) theEObject;
                T result = caseUniversalPayloadExtractStrategy(universalPayloadExtractStrategy);
                if (result == null) result = caseExtractStrategy(universalPayloadExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.EMPTY_EXTRACT_STRATEGY: {
                EmptyExtractStrategy emptyExtractStrategy = (EmptyExtractStrategy) theEObject;
                T result = caseEmptyExtractStrategy(emptyExtractStrategy);
                if (result == null) result = caseExtractStrategy(emptyExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CAN_FILTER: {
                CANFilter canFilter = (CANFilter) theEObject;
                T result = caseCANFilter(canFilter);
                if (result == null) result = caseAbstractFilter(canFilter);
                if (result == null) result = caseBaseClassWithSourceReference(canFilter);
                if (result == null) result = caseBaseClassWithID(canFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.LIN_FILTER: {
                LINFilter linFilter = (LINFilter) theEObject;
                T result = caseLINFilter(linFilter);
                if (result == null) result = caseAbstractFilter(linFilter);
                if (result == null) result = caseBaseClassWithSourceReference(linFilter);
                if (result == null) result = caseBaseClassWithID(linFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.FLEX_RAY_FILTER: {
                FlexRayFilter flexRayFilter = (FlexRayFilter) theEObject;
                T result = caseFlexRayFilter(flexRayFilter);
                if (result == null) result = caseAbstractFilter(flexRayFilter);
                if (result == null) result = caseBaseClassWithSourceReference(flexRayFilter);
                if (result == null) result = caseBaseClassWithID(flexRayFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.DLT_FILTER: {
                DLTFilter dltFilter = (DLTFilter) theEObject;
                T result = caseDLTFilter(dltFilter);
                if (result == null) result = caseAbstractFilter(dltFilter);
                if (result == null) result = caseBaseClassWithSourceReference(dltFilter);
                if (result == null) result = caseBaseClassWithID(dltFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UDPNM_FILTER: {
                UDPNMFilter udpnmFilter = (UDPNMFilter) theEObject;
                T result = caseUDPNMFilter(udpnmFilter);
                if (result == null) result = caseAbstractFilter(udpnmFilter);
                if (result == null) result = caseBaseClassWithSourceReference(udpnmFilter);
                if (result == null) result = caseBaseClassWithID(udpnmFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.CAN_MESSAGE: {
                CANMessage canMessage = (CANMessage) theEObject;
                T result = caseCANMessage(canMessage);
                if (result == null) result = caseAbstractBusMessage(canMessage);
                if (result == null) result = caseAbstractMessage(canMessage);
                if (result == null) result = caseBaseClassWithSourceReference(canMessage);
                if (result == null) result = caseBaseClassWithID(canMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.LIN_MESSAGE: {
                LINMessage linMessage = (LINMessage) theEObject;
                T result = caseLINMessage(linMessage);
                if (result == null) result = caseAbstractBusMessage(linMessage);
                if (result == null) result = caseAbstractMessage(linMessage);
                if (result == null) result = caseBaseClassWithSourceReference(linMessage);
                if (result == null) result = caseBaseClassWithID(linMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.FLEX_RAY_MESSAGE: {
                FlexRayMessage flexRayMessage = (FlexRayMessage) theEObject;
                T result = caseFlexRayMessage(flexRayMessage);
                if (result == null) result = caseAbstractBusMessage(flexRayMessage);
                if (result == null) result = caseAbstractMessage(flexRayMessage);
                if (result == null) result = caseBaseClassWithSourceReference(flexRayMessage);
                if (result == null) result = caseBaseClassWithID(flexRayMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.DLT_MESSAGE: {
                DLTMessage dltMessage = (DLTMessage) theEObject;
                T result = caseDLTMessage(dltMessage);
                if (result == null) result = caseAbstractBusMessage(dltMessage);
                if (result == null) result = caseAbstractMessage(dltMessage);
                if (result == null) result = caseBaseClassWithSourceReference(dltMessage);
                if (result == null) result = caseBaseClassWithID(dltMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UDP_MESSAGE: {
                UDPMessage udpMessage = (UDPMessage) theEObject;
                T result = caseUDPMessage(udpMessage);
                if (result == null) result = caseAbstractBusMessage(udpMessage);
                if (result == null) result = caseAbstractMessage(udpMessage);
                if (result == null) result = caseBaseClassWithSourceReference(udpMessage);
                if (result == null) result = caseBaseClassWithID(udpMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TCP_MESSAGE: {
                TCPMessage tcpMessage = (TCPMessage) theEObject;
                T result = caseTCPMessage(tcpMessage);
                if (result == null) result = caseAbstractBusMessage(tcpMessage);
                if (result == null) result = caseAbstractMessage(tcpMessage);
                if (result == null) result = caseBaseClassWithSourceReference(tcpMessage);
                if (result == null) result = caseBaseClassWithID(tcpMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UDPNM_MESSAGE: {
                UDPNMMessage udpnmMessage = (UDPNMMessage) theEObject;
                T result = caseUDPNMMessage(udpnmMessage);
                if (result == null) result = caseAbstractBusMessage(udpnmMessage);
                if (result == null) result = caseAbstractMessage(udpnmMessage);
                if (result == null) result = caseBaseClassWithSourceReference(udpnmMessage);
                if (result == null) result = caseBaseClassWithID(udpnmMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VERBOSE_DLT_MESSAGE: {
                VerboseDLTMessage verboseDLTMessage = (VerboseDLTMessage) theEObject;
                T result = caseVerboseDLTMessage(verboseDLTMessage);
                if (result == null) result = caseDLTMessage(verboseDLTMessage);
                if (result == null) result = caseAbstractBusMessage(verboseDLTMessage);
                if (result == null) result = caseAbstractMessage(verboseDLTMessage);
                if (result == null) result = caseBaseClassWithSourceReference(verboseDLTMessage);
                if (result == null) result = caseBaseClassWithID(verboseDLTMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY: {
                UniversalPayloadWithLegacyExtractStrategy universalPayloadWithLegacyExtractStrategy = (UniversalPayloadWithLegacyExtractStrategy) theEObject;
                T result = caseUniversalPayloadWithLegacyExtractStrategy(universalPayloadWithLegacyExtractStrategy);
                if (result == null) result = caseUniversalPayloadExtractStrategy(universalPayloadWithLegacyExtractStrategy);
                if (result == null) result = caseExtractStrategy(universalPayloadWithLegacyExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_BUS_MESSAGE: {
                AbstractBusMessage abstractBusMessage = (AbstractBusMessage) theEObject;
                T result = caseAbstractBusMessage(abstractBusMessage);
                if (result == null) result = caseAbstractMessage(abstractBusMessage);
                if (result == null) result = caseBaseClassWithSourceReference(abstractBusMessage);
                if (result == null) result = caseBaseClassWithID(abstractBusMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_FILTER: {
                PluginFilter pluginFilter = (PluginFilter) theEObject;
                T result = casePluginFilter(pluginFilter);
                if (result == null) result = caseAbstractFilter(pluginFilter);
                if (result == null) result = caseBaseClassWithSourceReference(pluginFilter);
                if (result == null) result = caseBaseClassWithID(pluginFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_MESSAGE: {
                PluginMessage pluginMessage = (PluginMessage) theEObject;
                T result = casePluginMessage(pluginMessage);
                if (result == null) result = caseAbstractMessage(pluginMessage);
                if (result == null) result = caseBaseClassWithSourceReference(pluginMessage);
                if (result == null) result = caseBaseClassWithID(pluginMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_SIGNAL: {
                PluginSignal pluginSignal = (PluginSignal) theEObject;
                T result = casePluginSignal(pluginSignal);
                if (result == null) result = caseDoubleSignal(pluginSignal);
                if (result == null) result = caseAbstractSignal(pluginSignal);
                if (result == null) result = caseBaseClassWithSourceReference(pluginSignal);
                if (result == null) result = caseISignalOrReference(pluginSignal);
                if (result == null) result = caseBaseClassWithID(pluginSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(pluginSignal);
                if (result == null) result = caseIOperand(pluginSignal);
                if (result == null) result = caseBusMessageReferable(pluginSignal);
                if (result == null) result = caseIVariableReaderWriter(pluginSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY: {
                PluginStateExtractStrategy pluginStateExtractStrategy = (PluginStateExtractStrategy) theEObject;
                T result = casePluginStateExtractStrategy(pluginStateExtractStrategy);
                if (result == null) result = caseExtractStrategy(pluginStateExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY: {
                PluginResultExtractStrategy pluginResultExtractStrategy = (PluginResultExtractStrategy) theEObject;
                T result = casePluginResultExtractStrategy(pluginResultExtractStrategy);
                if (result == null) result = caseExtractStrategy(pluginResultExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.EMPTY_DECODE_STRATEGY: {
                EmptyDecodeStrategy emptyDecodeStrategy = (EmptyDecodeStrategy) theEObject;
                T result = caseEmptyDecodeStrategy(emptyDecodeStrategy);
                if (result == null) result = caseDecodeStrategy(emptyDecodeStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION: {
                PluginCheckExpression pluginCheckExpression = (PluginCheckExpression) theEObject;
                T result = casePluginCheckExpression(pluginCheckExpression);
                if (result == null) result = caseExpression(pluginCheckExpression);
                if (result == null) result = caseBaseClassWithID(pluginCheckExpression);
                if (result == null) result = caseIVariableReaderWriter(pluginCheckExpression);
                if (result == null) result = caseIStateTransitionReference(pluginCheckExpression);
                if (result == null) result = caseBusMessageReferable(pluginCheckExpression);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.BASE_CLASS_WITH_ID: {
                BaseClassWithID baseClassWithID = (BaseClassWithID) theEObject;
                T result = caseBaseClassWithID(baseClassWithID);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.HEADER_SIGNAL: {
                HeaderSignal headerSignal = (HeaderSignal) theEObject;
                T result = caseHeaderSignal(headerSignal);
                if (result == null) result = caseAbstractSignal(headerSignal);
                if (result == null) result = caseBaseClassWithSourceReference(headerSignal);
                if (result == null) result = caseISignalOrReference(headerSignal);
                if (result == null) result = caseBaseClassWithID(headerSignal);
                if (result == null) result = caseISignalComparisonExpressionOperand(headerSignal);
                if (result == null) result = caseIOperand(headerSignal);
                if (result == null) result = caseBusMessageReferable(headerSignal);
                if (result == null) result = caseIVariableReaderWriter(headerSignal);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.IVARIABLE_READER_WRITER: {
                IVariableReaderWriter iVariableReaderWriter = (IVariableReaderWriter) theEObject;
                T result = caseIVariableReaderWriter(iVariableReaderWriter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ISTATE_TRANSITION_REFERENCE: {
                IStateTransitionReference iStateTransitionReference = (IStateTransitionReference) theEObject;
                T result = caseIStateTransitionReference(iStateTransitionReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.TP_FILTER: {
                TPFilter tpFilter = (TPFilter) theEObject;
                T result = caseTPFilter(tpFilter);
                if (result == null) result = caseAbstractFilter(tpFilter);
                if (result == null) result = caseBaseClassWithSourceReference(tpFilter);
                if (result == null) result = caseBaseClassWithID(tpFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE: {
                BaseClassWithSourceReference baseClassWithSourceReference = (BaseClassWithSourceReference) theEObject;
                T result = caseBaseClassWithSourceReference(baseClassWithSourceReference);
                if (result == null) result = caseBaseClassWithID(baseClassWithSourceReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.SOURCE_REFERENCE: {
                SourceReference sourceReference = (SourceReference) theEObject;
                T result = caseSourceReference(sourceReference);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ETHERNET_MESSAGE: {
                EthernetMessage ethernetMessage = (EthernetMessage) theEObject;
                T result = caseEthernetMessage(ethernetMessage);
                if (result == null) result = caseAbstractBusMessage(ethernetMessage);
                if (result == null) result = caseAbstractMessage(ethernetMessage);
                if (result == null) result = caseBaseClassWithSourceReference(ethernetMessage);
                if (result == null) result = caseBaseClassWithID(ethernetMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.IPV4_MESSAGE: {
                IPv4Message iPv4Message = (IPv4Message) theEObject;
                T result = caseIPv4Message(iPv4Message);
                if (result == null) result = caseAbstractBusMessage(iPv4Message);
                if (result == null) result = caseAbstractMessage(iPv4Message);
                if (result == null) result = caseBaseClassWithSourceReference(iPv4Message);
                if (result == null) result = caseBaseClassWithID(iPv4Message);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.NON_VERBOSE_DLT_MESSAGE: {
                NonVerboseDLTMessage nonVerboseDLTMessage = (NonVerboseDLTMessage) theEObject;
                T result = caseNonVerboseDLTMessage(nonVerboseDLTMessage);
                if (result == null) result = caseDLTMessage(nonVerboseDLTMessage);
                if (result == null) result = caseAbstractBusMessage(nonVerboseDLTMessage);
                if (result == null) result = caseAbstractMessage(nonVerboseDLTMessage);
                if (result == null) result = caseBaseClassWithSourceReference(nonVerboseDLTMessage);
                if (result == null) result = caseBaseClassWithID(nonVerboseDLTMessage);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STRING_DECODE_STRATEGY: {
                StringDecodeStrategy stringDecodeStrategy = (StringDecodeStrategy) theEObject;
                T result = caseStringDecodeStrategy(stringDecodeStrategy);
                if (result == null) result = caseDecodeStrategy(stringDecodeStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER: {
                NonVerboseDLTFilter nonVerboseDLTFilter = (NonVerboseDLTFilter) theEObject;
                T result = caseNonVerboseDLTFilter(nonVerboseDLTFilter);
                if (result == null) result = caseAbstractFilter(nonVerboseDLTFilter);
                if (result == null) result = caseBaseClassWithSourceReference(nonVerboseDLTFilter);
                if (result == null) result = caseBaseClassWithID(nonVerboseDLTFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VERBOSE_DLT_EXTRACT_STRATEGY: {
                VerboseDLTExtractStrategy verboseDLTExtractStrategy = (VerboseDLTExtractStrategy) theEObject;
                T result = caseVerboseDLTExtractStrategy(verboseDLTExtractStrategy);
                if (result == null) result = caseExtractStrategy(verboseDLTExtractStrategy);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.REGEX_OPERATION: {
                RegexOperation regexOperation = (RegexOperation) theEObject;
                T result = caseRegexOperation(regexOperation);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.PAYLOAD_FILTER: {
                PayloadFilter payloadFilter = (PayloadFilter) theEObject;
                T result = casePayloadFilter(payloadFilter);
                if (result == null) result = caseAbstractFilter(payloadFilter);
                if (result == null) result = caseBaseClassWithSourceReference(payloadFilter);
                if (result == null) result = caseBaseClassWithID(payloadFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER: {
                VerboseDLTPayloadFilter verboseDLTPayloadFilter = (VerboseDLTPayloadFilter) theEObject;
                T result = caseVerboseDLTPayloadFilter(verboseDLTPayloadFilter);
                if (result == null) result = caseAbstractFilter(verboseDLTPayloadFilter);
                if (result == null) result = caseBaseClassWithSourceReference(verboseDLTPayloadFilter);
                if (result == null) result = caseBaseClassWithID(verboseDLTPayloadFilter);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.ABSTRACT_VARIABLE: {
                AbstractVariable abstractVariable = (AbstractVariable) theEObject;
                T result = caseAbstractVariable(abstractVariable);
                if (result == null) result = caseBaseClassWithID(abstractVariable);
                if (result == null) result = caseIVariableReaderWriter(abstractVariable);
                if (result == null) result = caseBusMessageReferable(abstractVariable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.COMPUTED_VARIABLE: {
                ComputedVariable computedVariable = (ComputedVariable) theEObject;
                T result = caseComputedVariable(computedVariable);
                if (result == null) result = caseVariable(computedVariable);
                if (result == null) result = caseAbstractVariable(computedVariable);
                if (result == null) result = caseBaseClassWithID(computedVariable);
                if (result == null) result = caseIVariableReaderWriter(computedVariable);
                if (result == null) result = caseBusMessageReferable(computedVariable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            case ConditionsPackage.STRUCTURE_VARIABLE: {
                StructureVariable structureVariable = (StructureVariable) theEObject;
                T result = caseStructureVariable(structureVariable);
                if (result == null) result = caseAbstractVariable(structureVariable);
                if (result == null) result = caseBaseClassWithID(structureVariable);
                if (result == null) result = caseIVariableReaderWriter(structureVariable);
                if (result == null) result = caseBusMessageReferable(structureVariable);
                if (result == null) result = defaultCase(theEObject);
                return result;
            }
            default:
                return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDocumentRoot(DocumentRoot object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Condition Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Condition Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConditionSet(ConditionSet object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCondition(Condition object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Signal Comparison Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Signal Comparison Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSignalComparisonExpression(SignalComparisonExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Can Message Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Can Message Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCanMessageCheckExpression(CanMessageCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Lin Message Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Lin Message Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLinMessageCheckExpression(LinMessageCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>State Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>State Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStateCheckExpression(StateCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Timing Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Timing Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTimingExpression(TimingExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>True Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>True Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTrueExpression(TrueExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Logical Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Logical Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLogicalExpression(LogicalExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Reference Condition Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Reference Condition Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseReferenceConditionExpression(ReferenceConditionExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Transition Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Transition Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTransitionCheckExpression(TransitionCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Flex Ray Message Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Flex Ray Message Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFlexRayMessageCheckExpression(FlexRayMessageCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Stop Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Stop Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStopExpression(StopExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Comparator Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Comparator Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseComparatorSignal(ComparatorSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Constant Comparator Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Constant Comparator Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConstantComparatorValue(ConstantComparatorValue object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Calculation Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Calculation Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCalculationExpression(CalculationExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExpression(Expression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Variable Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Variable Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVariableReference(VariableReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Document</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Document</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseConditionsDocument(ConditionsDocument object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Variable Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Variable Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVariableSet(VariableSet object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVariable(Variable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Signal Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Signal Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSignalVariable(SignalVariable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValueVariable(ValueVariable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Observer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Observer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractObserver(AbstractObserver object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Observer Value Range</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Observer Value Range</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseObserverValueRange(ObserverValueRange object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Signal Observer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Signal Observer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSignalObserver(SignalObserver object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Signal Reference Set</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Signal Reference Set</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSignalReferenceSet(SignalReferenceSet object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Signal Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Signal Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSignalReference(SignalReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ISignal Or Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ISignal Or Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseISignalOrReference(ISignalOrReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bit Pattern Comparator Value</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bit Pattern Comparator Value</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBitPatternComparatorValue(BitPatternComparatorValue object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Not Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Not Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNotExpression(NotExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IOperand</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IOperand</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIOperand(IOperand object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ICompute Variable Action Operand</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ICompute Variable Action Operand</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIComputeVariableActionOperand(IComputeVariableActionOperand object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IString Operand</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IString Operand</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIStringOperand(IStringOperand object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>ISignal Comparison Expression Operand</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>ISignal Comparison Expression Operand</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseISignalComparisonExpressionOperand(ISignalComparisonExpressionOperand object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IOperation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IOperation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIOperation(IOperation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IString Operation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IString Operation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIStringOperation(IStringOperation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>INumeric Operand</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>INumeric Operand</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseINumericOperand(INumericOperand object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>INumeric Operation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>INumeric Operation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseINumericOperation(INumericOperation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Value Variable Observer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Value Variable Observer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseValueVariableObserver(ValueVariableObserver object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Parse String To Double</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Parse String To Double</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseParseStringToDouble(ParseStringToDouble object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringExpression(StringExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Matches</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Matches</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMatches(Matches object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Extract</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Extract</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExtract(Extract object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Substring</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Substring</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSubstring(Substring object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Parse Numeric To String</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Parse Numeric To String</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseParseNumericToString(ParseNumericToString object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Length</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Length</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringLength(StringLength object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractMessage(AbstractMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Some IP Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Some IP Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSomeIPMessage(SomeIPMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractSignal(AbstractSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Container Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Container Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContainerSignal(ContainerSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractFilter(AbstractFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ethernet Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ethernet Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEthernetFilter(EthernetFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>UDP Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>UDP Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUDPFilter(UDPFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>TCP Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>TCP Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTCPFilter(TCPFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IPv4 Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IPv4 Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIPv4Filter(IPv4Filter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Some IP Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Some IP Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSomeIPFilter(SomeIPFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>For Each Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>For Each Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseForEachExpression(ForEachExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Message Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Message Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMessageCheckExpression(MessageCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Some IPSD Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Some IPSD Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSomeIPSDFilter(SomeIPSDFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Some IPSD Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Some IPSD Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSomeIPSDMessage(SomeIPSDMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Double Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Double Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDoubleSignal(DoubleSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringSignal(StringSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Decode Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Decode Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDecodeStrategy(DecodeStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Double Decode Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Double Decode Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDoubleDecodeStrategy(DoubleDecodeStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseExtractStrategy(ExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Universal Payload Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Universal Payload Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUniversalPayloadExtractStrategy(UniversalPayloadExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Empty Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Empty Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmptyExtractStrategy(EmptyExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CAN Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CAN Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCANFilter(CANFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>LIN Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>LIN Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLINFilter(LINFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Flex Ray Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Flex Ray Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFlexRayFilter(FlexRayFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DLT Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DLT Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDLTFilter(DLTFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>UDPNM Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>UDPNM Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUDPNMFilter(UDPNMFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>CAN Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>CAN Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCANMessage(CANMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>LIN Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>LIN Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLINMessage(LINMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Flex Ray Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Flex Ray Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseFlexRayMessage(FlexRayMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>DLT Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>DLT Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseDLTMessage(DLTMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>UDP Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>UDP Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUDPMessage(UDPMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Verbose DLT Payload Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Verbose DLT Payload Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVerboseDLTPayloadFilter(VerboseDLTPayloadFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractVariable(AbstractVariable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Structure Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Structure Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStructureVariable(StructureVariable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Bus Message Referable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Bus Message Referable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBusMessageReferable(BusMessageReferable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Computed Variable</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Computed Variable</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseComputedVariable(ComputedVariable object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Variable Format</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Variable Format</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVariableFormat(VariableFormat object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Payload Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Payload Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePayloadFilter(PayloadFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>TCP Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>TCP Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTCPMessage(TCPMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>UDPNM Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>UDPNM Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUDPNMMessage(UDPNMMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Verbose DLT Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Verbose DLT Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVerboseDLTMessage(VerboseDLTMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Universal Payload With Legacy Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Universal Payload With Legacy Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUniversalPayloadWithLegacyExtractStrategy(UniversalPayloadWithLegacyExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Abstract Bus Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Abstract Bus Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAbstractBusMessage(AbstractBusMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginFilter(PluginFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginMessage(PluginMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginSignal(PluginSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin State Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin State Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginStateExtractStrategy(PluginStateExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin Result Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin Result Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginResultExtractStrategy(PluginResultExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Empty Decode Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Empty Decode Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEmptyDecodeStrategy(EmptyDecodeStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Plugin Check Expression</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Plugin Check Expression</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T casePluginCheckExpression(PluginCheckExpression object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Base Class With ID</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Base Class With ID</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBaseClassWithID(BaseClassWithID object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Header Signal</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Header Signal</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseHeaderSignal(HeaderSignal object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IVariable Reader Writer</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IVariable Reader Writer</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIVariableReaderWriter(IVariableReaderWriter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IState Transition Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IState Transition Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIStateTransitionReference(IStateTransitionReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>TP Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>TP Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseTPFilter(TPFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Base Class With Source Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Base Class With Source Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseBaseClassWithSourceReference(BaseClassWithSourceReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Source Reference</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Source Reference</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSourceReference(SourceReference object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Ethernet Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Ethernet Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEthernetMessage(EthernetMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>IPv4 Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>IPv4 Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseIPv4Message(IPv4Message object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Non Verbose DLT Message</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Non Verbose DLT Message</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonVerboseDLTMessage(NonVerboseDLTMessage object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>String Decode Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>String Decode Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseStringDecodeStrategy(StringDecodeStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Non Verbose DLT Filter</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Non Verbose DLT Filter</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseNonVerboseDLTFilter(NonVerboseDLTFilter object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Verbose DLT Extract Strategy</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Verbose DLT Extract Strategy</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVerboseDLTExtractStrategy(VerboseDLTExtractStrategy object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Regex Operation</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Regex Operation</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseRegexOperation(RegexOperation object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Smard Trace Element</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Smard Trace Element</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseSmardTraceElement(SmardTraceElement object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
     * <!-- begin-user-doc -->
     * This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last case anyway.
     * <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    public T defaultCase(EObject object) {
        return null;
    }

} //ConditionsSwitch
