package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Verbose DLT Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getContainsAny <em>Contains Any</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVerboseDLTPayloadFilter()
 * @model
 * @generated
 */
public interface VerboseDLTPayloadFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Regex</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Regex</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Regex</em>' attribute.
     * @see #setRegex(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVerboseDLTPayloadFilter_Regex()
     * @model default="" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getRegex();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Regex</em>' attribute.
     * @see #getRegex()
     * @generated
     */
    void setRegex(String value);

    /**
     * Returns the value of the '<em><b>Contains Any</b></em>' attribute list.
     * The list contents are of type {@link java.lang.String}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Contains Any</em>' attribute list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVerboseDLTPayloadFilter_ContainsAny()
     * @model ordered="false"
     * @generated
     */
    EList<String> getContainsAny();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context);

} // VerboseDLTPayloadFilter
