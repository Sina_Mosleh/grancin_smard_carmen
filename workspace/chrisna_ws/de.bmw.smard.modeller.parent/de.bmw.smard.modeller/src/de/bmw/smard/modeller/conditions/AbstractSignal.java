/**
 */
package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.statemachineset.BusMessageReferable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractSignal#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractSignal#getDecodeStrategy <em>Decode Strategy</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractSignal()
 * @model abstract="true"
 * @generated
 */
public interface AbstractSignal extends BaseClassWithSourceReference, ISignalOrReference, BusMessageReferable {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractSignal_Name()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Decode Strategy</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Decode Strategy</em>' containment reference.
     * @see #setDecodeStrategy(DecodeStrategy)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractSignal_DecodeStrategy()
     * @model containment="true" required="true"
     * @generated
     */
    DecodeStrategy getDecodeStrategy();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getDecodeStrategy <em>Decode Strategy</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Decode Strategy</em>' containment reference.
     * @see #getDecodeStrategy()
     * @generated
     */
    void setDecodeStrategy(DecodeStrategy value);

    /**
     * Returns the value of the '<em><b>Extract Strategy</b></em>' containment reference.
     * It is bidirectional and its opposite is '{@link de.bmw.smard.modeller.conditions.ExtractStrategy#getAbstractSignal <em>Abstract Signal</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Extract Strategy</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Extract Strategy</em>' containment reference.
     * @see #setExtractStrategy(ExtractStrategy)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractSignal_ExtractStrategy()
     * @see de.bmw.smard.modeller.conditions.ExtractStrategy#getAbstractSignal
     * @model opposite="abstractSignal" containment="true" required="true"
     * @generated
     */
    ExtractStrategy getExtractStrategy();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Extract Strategy</em>' containment reference.
     * @see #getExtractStrategy()
     * @generated
     */
    void setExtractStrategy(ExtractStrategy value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    AbstractMessage getMessage();

} // AbstractSignal
