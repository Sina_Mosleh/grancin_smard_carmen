package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.Expression;
import de.bmw.smard.modeller.conditions.LogicalOperatorType;
import de.bmw.smard.modeller.conditions.NotExpression;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.Transition;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Not Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.NotExpressionImpl#getExpression <em>Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.NotExpressionImpl#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NotExpressionImpl extends ExpressionImpl implements NotExpression {
    /**
     * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpression()
     * @generated
     * @ordered
     */
    protected Expression expression;

    /**
     * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected static final LogicalOperatorType OPERATOR_EDEFAULT = LogicalOperatorType.NOT;

    /**
     * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected LogicalOperatorType operator = OPERATOR_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected NotExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.NOT_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Expression getExpression() {
        return expression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetExpression(Expression newExpression, NotificationChain msgs) {
        Expression oldExpression = expression;
        expression = newExpression;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.NOT_EXPRESSION__EXPRESSION, oldExpression, newExpression);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getExpression() != null) {
            return getExpression().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExpression(Expression newExpression) {
        if (newExpression != expression) {
            NotificationChain msgs = null;
            if (expression != null)
                msgs = ((InternalEObject) expression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.NOT_EXPRESSION__EXPRESSION, null, msgs);
            if (newExpression != null)
                msgs = ((InternalEObject) newExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.NOT_EXPRESSION__EXPRESSION, null, msgs);
            msgs = basicSetExpression(newExpression, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.NOT_EXPRESSION__EXPRESSION, newExpression, newExpression));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public LogicalOperatorType getOperator() {
        return operator;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.NOT_EXPRESSION__EXPRESSION:
                return basicSetExpression(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.NOT_EXPRESSION__EXPRESSION:
                return getExpression();
            case ConditionsPackage.NOT_EXPRESSION__OPERATOR:
                return getOperator();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.NOT_EXPRESSION__EXPRESSION:
                setExpression((Expression) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NOT_EXPRESSION__EXPRESSION:
                setExpression((Expression) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NOT_EXPRESSION__EXPRESSION:
                return expression != null;
            case ConditionsPackage.NOT_EXPRESSION__OPERATOR:
                return operator != OPERATOR_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (operator: ");
        result.append(operator);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        if (getExpression() == null) {
            return ECollections.emptyEList();
        }
        return getExpression().getReadVariables();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        return getExpression().getWriteVariables();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<AbstractState> getStateDependencies() {
        return expression.getStateDependencies();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<Transition> getTransitionDependencies() {
        return expression.getTransitionDependencies();
    }

} //NotExpressionImpl
