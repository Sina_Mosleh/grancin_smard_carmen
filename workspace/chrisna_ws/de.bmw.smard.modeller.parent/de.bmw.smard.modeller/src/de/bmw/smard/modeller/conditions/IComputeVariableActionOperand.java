package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ICompute Variable Action Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIComputeVariableActionOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IComputeVariableActionOperand extends IOperand {
} // IComputeVariableActionOperand
