package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;

public class NotNullValidator {

    private NotNullValidator() {
    }

    static ErrorStep builder(Object value) {
        return new Builder(value);
    }

    public interface ErrorStep {
        Build error(String errorId);
    }

    public static class Builder extends Validator implements ErrorStep {

        private final Object value;
        private String errorId;

        Builder(Object value) {
            this.value = value;
        }

        @Override
        public Build error(String errorId) {
            this.errorId = errorId;
            return this;
        }

        @Override
        protected boolean validate(BaseValidator baseValidator) {
            if (value == null
            		|| (value instanceof String && StringUtils.isBlank((String) value))) {
                attachError(baseValidator, errorId);
                return false;
            } 
            return true;
        }
    }
}
