package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observer Value Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValue <em>Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValueType <em>Value Type</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getObserverValueRange()
 * @model
 * @generated
 */
public interface ObserverValueRange extends EObject {
    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getObserverValueRange_Value()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getValue();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getObserverValueRange_Description()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Value Type</b></em>' attribute.
     * The default value is <code>"INFO"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.ObserverValueType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ObserverValueType
     * @see #setValueType(ObserverValueType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getObserverValueRange_ValueType()
     * @model default="INFO"
     *        extendedMetaData="kind='attribute' name='valueType' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    ObserverValueType getValueType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValueType <em>Value Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Value Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ObserverValueType
     * @see #getValueType()
     * @generated
     */
    void setValueType(ObserverValueType value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ObserverValueRange
