package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DLT Message Log Info</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDLT_MessageLogInfo()
 * @model
 * @generated
 */
public enum DLT_MessageLogInfo implements Enumerator {
    /**
     * The '<em><b>NOT SPECIFIED</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #NOT_SPECIFIED_VALUE
     * @generated
     * @ordered
     */
    NOT_SPECIFIED(0, "NOT_SPECIFIED", "NOT_SPECIFIED"),

    /**
     * The '<em><b>DLT LOG FATAL</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_FATAL_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_FATAL(1, "DLT_LOG_FATAL", "DLT_LOG_FATAL"),

    /**
     * The '<em><b>DLT LOG ERROR</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_ERROR_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_ERROR(2, "DLT_LOG_ERROR", "DLT_LOG_ERROR"),

    /**
     * The '<em><b>DLT LOG WARN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_WARN_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_WARN(3, "DLT_LOG_WARN", "DLT_LOG_WARN"),

    /**
     * The '<em><b>DLT LOG INFO</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_INFO_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_INFO(4, "DLT_LOG_INFO", "DLT_LOG_INFO"),

    /**
     * The '<em><b>DLT LOG DEBUG</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_DEBUG_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_DEBUG(5, "DLT_LOG_DEBUG", "DLT_LOG_DEBUG"),

    /**
     * The '<em><b>DLT LOG VERBOSE</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_VERBOSE_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_VERBOSE(6, "DLT_LOG_VERBOSE", "DLT_LOG_VERBOSE"),

    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),
    /**
     * The '<em><b>DLT LOG OFF</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_OFF_VALUE
     * @generated
     * @ordered
     */
    DLT_LOG_OFF(7, "DLT_LOG_OFF", "DLT_LOG_OFF");

    /**
     * The '<em><b>NOT SPECIFIED</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>NOT SPECIFIED</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #NOT_SPECIFIED
     * @model
     * @generated
     * @ordered
     */
    public static final int NOT_SPECIFIED_VALUE = 0;

    /**
     * The '<em><b>DLT LOG FATAL</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG FATAL</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_FATAL
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_FATAL_VALUE = 1;

    /**
     * The '<em><b>DLT LOG ERROR</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG ERROR</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_ERROR
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_ERROR_VALUE = 2;

    /**
     * The '<em><b>DLT LOG WARN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG WARN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_WARN
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_WARN_VALUE = 3;

    /**
     * The '<em><b>DLT LOG INFO</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG INFO</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_INFO
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_INFO_VALUE = 4;

    /**
     * The '<em><b>DLT LOG DEBUG</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG DEBUG</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_DEBUG
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_DEBUG_VALUE = 5;

    /**
     * The '<em><b>DLT LOG VERBOSE</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG VERBOSE</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_VERBOSE
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_VERBOSE_VALUE = 6;

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * The '<em><b>DLT LOG OFF</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DLT LOG OFF</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DLT_LOG_OFF
     * @model
     * @generated
     * @ordered
     */
    public static final int DLT_LOG_OFF_VALUE = 7;

    /**
     * An array of all the '<em><b>DLT Message Log Info</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final DLT_MessageLogInfo[] VALUES_ARRAY =
            new DLT_MessageLogInfo[] {
                    NOT_SPECIFIED,
                    DLT_LOG_FATAL,
                    DLT_LOG_ERROR,
                    DLT_LOG_WARN,
                    DLT_LOG_INFO,
                    DLT_LOG_DEBUG,
                    DLT_LOG_VERBOSE,
                    TEMPLATE_DEFINED,
                    DLT_LOG_OFF,
            };

    /**
     * A public read-only list of all the '<em><b>DLT Message Log Info</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<DLT_MessageLogInfo> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>DLT Message Log Info</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageLogInfo get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DLT_MessageLogInfo result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>DLT Message Log Info</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageLogInfo getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            DLT_MessageLogInfo result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>DLT Message Log Info</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static DLT_MessageLogInfo get(int value) {
        switch (value) {
            case NOT_SPECIFIED_VALUE:
                return NOT_SPECIFIED;
            case DLT_LOG_FATAL_VALUE:
                return DLT_LOG_FATAL;
            case DLT_LOG_ERROR_VALUE:
                return DLT_LOG_ERROR;
            case DLT_LOG_WARN_VALUE:
                return DLT_LOG_WARN;
            case DLT_LOG_INFO_VALUE:
                return DLT_LOG_INFO;
            case DLT_LOG_DEBUG_VALUE:
                return DLT_LOG_DEBUG;
            case DLT_LOG_VERBOSE_VALUE:
                return DLT_LOG_VERBOSE;
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case DLT_LOG_OFF_VALUE:
                return DLT_LOG_OFF;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private DLT_MessageLogInfo(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //DLT_MessageLogInfo
