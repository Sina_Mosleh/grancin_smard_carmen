package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IVariable Reader Writer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIVariableReaderWriter()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IVariableReaderWriter extends EObject {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation" dataType="org.eclipse.emf.ecore.xml.type.String"
     * @generated
     */
    EList<String> getReadVariables();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation" dataType="org.eclipse.emf.ecore.xml.type.String"
     * @generated
     */
    EList<String> getWriteVariables();

} // IVariableReaderWriter
