package de.bmw.smard.modeller.util.validation;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.bmw.smard.modeller.statemachine.StateMachine;

public class VariableUsageChecker {

	public Map<String, Set<StateMachine>> checkMissingStateMachines(final Collection<StateMachine> allStateMachines, final Collection<StateMachine> stateMachinesInSet) {
		Map<String, Set<StateMachine>> missingWriterStateMachinesForVariable = new HashMap<>();
		
		// read-/writeVariables := variables read or written by one (or more) StateMachine(s) contained in set
		Set<String> readVariables = new HashSet<>();
		Set<String> writeVariables = new HashSet<>();
		stateMachinesInSet.forEach(sm -> {
			readVariables.addAll(sm.getReadVariables());
			writeVariables.addAll(sm.getWriteVariables());
		});

		// if there are no variables read by StateMachine, there can't be a problem with another StateMachine writing 
		if(readVariables.isEmpty()) {
			return missingWriterStateMachinesForVariable;
		}

		readVariables.removeAll(writeVariables);
		// -> readVariables := variables read by any StateMachine contained in set but not
		// written by any StateMachine
		
		// get StateMachines not contained in set
		Collection<StateMachine> stateMachinesNotInSet = getAllStateMachinesNotInSet(allStateMachines, stateMachinesInSet);

        // which variables get written by which StateMachine not contained in set
        Map<String, Set<StateMachine>> writeVariables2StateMachine = new HashMap<>();
        
        stateMachinesNotInSet.forEach(sm ->
            sm.getWriteVariables().forEach(v ->
					writeVariables2StateMachine.computeIfAbsent(v, k -> new HashSet<>()).add(sm)
            )
        );

        // readVariables that are read by a StateMachine contained in set
        readVariables.forEach(v -> {
            if (writeVariables2StateMachine.get(v) != null) {
                // variable is read by a StateMachine contained in the StateMachineSet but
                // written by a StateMachine not contained in the StateMachineSet
				missingWriterStateMachinesForVariable.put(v, writeVariables2StateMachine.get(v));
            }
        });
        
        return missingWriterStateMachinesForVariable;
	}
	
	private Collection<StateMachine> getAllStateMachinesNotInSet(Collection<StateMachine> allStateMachines, Collection<StateMachine> stateMachinesInSet) {
		Collection<StateMachine> stateMachinesNotInSet = allStateMachines;
        stateMachinesNotInSet.removeAll(stateMachinesInSet);

        return stateMachinesNotInSet;
    }
}