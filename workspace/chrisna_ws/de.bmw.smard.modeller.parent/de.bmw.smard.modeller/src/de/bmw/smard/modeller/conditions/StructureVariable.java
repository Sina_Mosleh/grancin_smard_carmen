/**
 */
package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structure Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.StructureVariable#getVariables <em>Variables</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.StructureVariable#getVariableReferences <em>Variable References</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStructureVariable()
 * @model
 * @generated
 */
public interface StructureVariable extends AbstractVariable {
    /**
     * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractVariable}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variables</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStructureVariable_Variables()
     * @model containment="true"
     * @generated
     */
    EList<AbstractVariable> getVariables();

    /**
     * Returns the value of the '<em><b>Variable References</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.VariableReference}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variable References</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStructureVariable_VariableReferences()
     * @model containment="true"
     * @generated
     */
    EList<VariableReference> getVariableReferences();

    /**
     * @generated NOT
     */
    Collection<AbstractVariable> getAllInheritedVariables(boolean includeReferences);

} // StructureVariable
