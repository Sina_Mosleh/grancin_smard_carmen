package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Universal Payload Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRule <em>Extraction Rule</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getByteOrder <em>Byte Order</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam <em>Extraction Rule Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataType <em>Signal Data Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam <em>Signal Data Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy()
 * @model
 * @generated
 */
public interface UniversalPayloadExtractStrategy extends ExtractStrategy {
    /**
     * Returns the value of the '<em><b>Extraction Rule</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Extraction Rule</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Extraction Rule</em>' attribute.
     * @see #setExtractionRule(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy_ExtractionRule()
     * @model default=""
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getExtractionRule();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRule <em>Extraction Rule</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Extraction Rule</em>' attribute.
     * @see #getExtractionRule()
     * @generated
     */
    void setExtractionRule(String value);

    /**
     * Returns the value of the '<em><b>Byte Order</b></em>' attribute.
     * The default value is <code>"INTEL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.ByteOrderType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Byte Order</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Byte Order</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ByteOrderType
     * @see #setByteOrder(ByteOrderType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy_ByteOrder()
     * @model default="INTEL"
     * @generated
     */
    ByteOrderType getByteOrder();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getByteOrder <em>Byte Order</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Byte Order</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ByteOrderType
     * @see #getByteOrder()
     * @generated
     */
    void setByteOrder(ByteOrderType value);

    /**
     * Returns the value of the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Extraction Rule Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Extraction Rule Tmpl Param</em>' attribute.
     * @see #setExtractionRuleTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam()
     * @model
     * @generated
     */
    String getExtractionRuleTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam <em>Extraction Rule Tmpl
     * Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Extraction Rule Tmpl Param</em>' attribute.
     * @see #getExtractionRuleTmplParam()
     * @generated
     */
    void setExtractionRuleTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Signal Data Type</b></em>' attribute.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.AUTOSARDataType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.AUTOSARDataType
     * @see #setSignalDataType(AUTOSARDataType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy_SignalDataType()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    AUTOSARDataType getSignalDataType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataType <em>Signal Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.AUTOSARDataType
     * @see #getSignalDataType()
     * @generated
     */
    void setSignalDataType(AUTOSARDataType value);

    /**
     * Returns the value of the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal Data Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal Data Type Tmpl Param</em>' attribute.
     * @see #setSignalDataTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam()
     * @model
     * @generated
     */
    String getSignalDataTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam <em>Signal Data Type Tmpl
     * Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal Data Type Tmpl Param</em>' attribute.
     * @see #getSignalDataTypeTmplParam()
     * @generated
     */
    void setSignalDataTypeTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidExtractString(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSignalDataType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // UniversalPayloadExtractStrategy
