package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.util.Diagnostic;

public class SeverityConverter {

    private SeverityConverter() {
    }

    public static Severity convert(int diagnosticSeverity) {

        switch (diagnosticSeverity) {
            case Diagnostic.OK:
                return Severity.OK;
            case Diagnostic.INFO:
                return Severity.INFO;
            case Diagnostic.WARNING:
                return Severity.WARNING;
            case Diagnostic.ERROR:
                return Severity.ERROR;
            case Diagnostic.CANCEL:
                return Severity.CANCEL;
            default:
                return Severity.ERROR;
        }
    }
}
