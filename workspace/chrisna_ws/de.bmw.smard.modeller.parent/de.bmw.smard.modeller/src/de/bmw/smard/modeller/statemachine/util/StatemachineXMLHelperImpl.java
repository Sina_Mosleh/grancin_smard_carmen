package de.bmw.smard.modeller.statemachine.util;

import de.bmw.smard.modeller.statemachine.StateType;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLHelperImpl;

import java.util.HashMap;
import java.util.Map;

public class StatemachineXMLHelperImpl extends XMLHelperImpl {

    /**
     * contains the value mappings for state types from old to new values
     */
    private static final Map<String, String> STATE_TYPE_VALUE_MAPPING;

    static {
        // version 1.0.5 did support the following states: DEFAULT, ERROR_STATE, WARNING_STATE
        // version 1.0.x does support the following states: INIT, INFO, OK, WARN, DEFECT
        // The previous state DEFAULT is mapped to INFO, ERROR_STATE is mapped to DEFECT, and WARNING_STATE is mapped to WARN
        // The initial state is set to INIT in the post load processing. That value is not allowed for any of the other states, only for the initial state.
        STATE_TYPE_VALUE_MAPPING = new HashMap<String, String>();
        STATE_TYPE_VALUE_MAPPING.put("ERROR_STATE", "DEFECT");
        STATE_TYPE_VALUE_MAPPING.put("WARNING_STATE", "WARN");
        STATE_TYPE_VALUE_MAPPING.put("DEFAULT", "INFO");
    }


    public StatemachineXMLHelperImpl(XMLResource resource) {
        super(resource);
    }

    @Override
    public String getHREF(EObject obj) {
        InternalEObject o = (InternalEObject) obj;

        URI objectURI = o.eProxyURI();
        if (objectURI == null) {
            Resource otherResource = obj.eResource();
            if (otherResource == null) {
                objectURI = handleDanglingHREF(obj);
                if (objectURI == null) {
                    return null;
                }
            } else {
                objectURI = getHREF(otherResource, obj);
            }
        }

        objectURI = deresolve(objectURI);

        return objectURI.toString();
    }


    /**
     * overridden to translate older enum values which are mapped to newer values
     */
    @Override
    protected Object createFromString(EFactory eFactory, EDataType eDataType,
            String value) {
        if (StateType.class.equals(eDataType.getInstanceClass())) {
            return createValueForStateType(eFactory, eDataType, value);
        } else {
            return super.createFromString(eFactory, eDataType, value);
        }
    }

    /**
     * Creates the correct value for the StateType, even if an older obsolete string value is given in the xml.
     * 
     * @param eFactory  the factory to create the entities
     * @param eDataType the data type for {@link StateType}
     * @param value     the value from the xml
     * @return the object representation of the value
     */
    private Object createValueForStateType(EFactory eFactory,
            EDataType eDataType, String value) {
        if (STATE_TYPE_VALUE_MAPPING.containsKey(value)) {
            value = STATE_TYPE_VALUE_MAPPING.get(value);
        }
        return super.createFromString(eFactory, eDataType, value);
    }

}
