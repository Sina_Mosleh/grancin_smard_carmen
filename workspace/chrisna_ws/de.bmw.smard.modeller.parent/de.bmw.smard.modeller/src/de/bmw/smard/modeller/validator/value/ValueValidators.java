package de.bmw.smard.modeller.validator.value;

public final class ValueValidators {

    private ValueValidators() {
    }

    public static ValueValidator validCharacters(String errorId) {
        return new CharacterValidator(errorId);
    }
    
    public static ValueValidator greaterZero(String errorId) {
    	return new GreaterZeroValidator(errorId);
    }
    
    public static ValueValidator validFormat(String errorId) {
    	return new FormatValidator(errorId);
    }
    
    public static ValueValidator validBitPatternStartBit() {
    	return new BitPatternComparatorStartBitValidator();
    }
    
    public static ValueValidator validBitPattern() {
    	return new BitPatternComparatorPatternValidator();
    }
    
    public static ValueValidator validRegexValue(String errorId) {
    	return new RegexValueValidator(errorId);
    }

    public static ValueValidator validJavaName(String errorId) {
        return new NamingConventionValidator(errorId);
    }

    public static ValueValidator flexRayCycle(String repetition) {
        return new FlexRayCycleValidator(repetition);
    }

    public static ValueValidator flexRayRepetition() {
        return new FlexRayRepetitionValidator();
    }

    public static ValueValidator flexRayFrameIdRange() {
        return new FlexRayFrameIdRangeValidator();
    }

    public static ValueValidator canFrameIdRange() {
        return FrameIdRangeValidator.can();
    }

    public static ValueValidator linFrameIdRange() {
        return FrameIdRangeValidator.lin();
    }

    public static ValueValidator flexRaySlotId() {
        return new FlexRaySlotIdValidator();
    }

    public static ValueValidator canFrameId(boolean isErrorFrame) {
        return FrameIdValidator.can(isErrorFrame);
    }

    public static ValueValidator linFrameId() {
        return FrameIdValidator.lin();
    }

    /**
     * Creates a new Bus ID Validator
     * @param interfaceName there must exist properties for
     *                      "_Validation_Conditions_{interfaceName}_BusId_LowerZero" and
     *                      "_Validation_Conditions_{interfaceName}_BusId_NotANumber" and
     *                      "_Validation_Conditions_{interfaceName}_BusId_IsNull" and
     *                      "_Validation_Conditions_{interfaceName}_BusId_IsZero"
     */
    public static ValueValidator busId(String interfaceName) { return new BusIdValidator(interfaceName); }

    public static ValueValidator busIdRange(String interfaceName) { return new BusIdRangeValidator(interfaceName); }
}
