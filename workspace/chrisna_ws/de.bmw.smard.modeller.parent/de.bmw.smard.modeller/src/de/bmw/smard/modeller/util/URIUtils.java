package de.bmw.smard.modeller.util;

import de.bmw.smard.base.util.StringUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

public class URIUtils {
	
	/**
	 * @param eObj - EObject for which the caller needs the project relative path
	 * @return relative path without the platform:/resource prefix
	 */
	private static String uriWithoutPlatformPrefix(EObject eObj) {
		String uriString = "";
		if(eObj == null) {
			return uriString;
		}
		
		// remove platform prefix
		Resource resource = eObj.eResource();
		if(resource == null) {
			return uriString;
		}
		
		URI smURI = resource.getURI();
		if(smURI != null) {
			if(smURI.isFile()) {
				uriString = makeFileURIProjectRelative(smURI, eObj);
			} else if(smURI.isPlatform()) {
				// remove the /platform:/resource/ prefix
				uriString = smURI.toPlatformString(true);
				if(uriString.startsWith("/")) {
					// remove leading slash from platform String
					uriString = uriString.substring(1);
				}
			} else {
				uriString = smURI.toString();
			}
		}
		
		return uriString;
	}
	
	private static String makeFileURIProjectRelative(URI smURI, EObject eObj) {
		IProject project = getProject(eObj);
		if(project != null) {
			IPath pathOfSM = new Path(smURI.toFileString());
			IPath relativePath = pathOfSM.makeRelativeTo(project.getLocation());
			if(relativePath != null) {
				return relativePath.toString();
			}
		}
		return "";
	}

	/**
	 * uriString now looks like this:
	 * [projectName]/StateMachines/default.statemachine
	 * projectName must be removed
	 * @param uriString - relative path with project name folder as prefix
	 * @param eObj
	 * @return relative path without project folder prefix
	 */
	private static String removeProjectNameFromUriString(String uriString, EObject eObj) {
		if(eObj == null) {
			return uriString;
		}

		IProject project = getProject(eObj);
		if(project != null) {
			String projectName = project.getName();
			if (!StringUtils.nullOrEmpty(projectName) && !StringUtils.nullOrEmpty(uriString) &&
					uriString.startsWith(projectName)) {
				// remove name of project and its trailing slash from uri string
				uriString = uriString.substring(projectName.length() + 1);
			}
		}
		return uriString;
	}
	
	private static IProject getProject(EObject eObj) {
		if(eObj == null) {
			return null;
		}
		
		Resource resource = eObj.eResource();
		if(resource == null) {
			return null;
		}
		
		IProject project = EMFUtils.getEclipseProject(resource);
		if(project == null) {
			return null;
		}
		
		return project;
	}

	/**
	 * SMARD-2540 displaying paths of StateMachines in StatemachineSetFormEditor
	 * input:
	 * /platform:/resource/[projectName]/StateMachines/default.statemachine
	 * output:
	 * StateMachines/default.statemachine
	 * @param eObj - EObject for which the caller wants the relative path
	 * @return relative path without platform:resource prefix and without project folder
	 */
	public static String createRelativePathForPlatformResource(EObject eObj) {
		if (eObj != null) {
			String uriString = URIUtils.uriWithoutPlatformPrefix(eObj);
			
			// SMARD-2540 remove project name prefix
			return URIUtils.removeProjectNameFromUriString(uriString, eObj);
		}
		return "";
	}

}
