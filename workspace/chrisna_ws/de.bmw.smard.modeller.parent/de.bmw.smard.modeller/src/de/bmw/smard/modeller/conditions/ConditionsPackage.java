package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;


/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsFactory
 * @model kind="package"
 *        extendedMetaData="qualified='true'"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 *        annotation="http://www.eclipse.org/OCL/Import ecore='http://www.eclipse.org/emf/2002/Ecore' ecore.xml.type='http://www.eclipse.org/emf/2003/XMLType'"
 * @generated
 */
public interface ConditionsPackage extends EPackage {

    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "conditions";
    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/conditions";
    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "conditions";
    /**
     * The package content type ID.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eCONTENT_TYPE = "de.bmw.smard.modeller.conditions";
    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    ConditionsPackage eINSTANCE = de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl.init();
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DocumentRootImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDocumentRoot()
     * @generated
     */
    int DOCUMENT_ROOT = 0;
    /**
     * The feature id for the '<em><b>Mixed</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__MIXED = 0;
    /**
     * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;
    /**
     * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;
    /**
     * The feature id for the '<em><b>Condition Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__CONDITION_SET = 3;
    /**
     * The feature id for the '<em><b>Conditions Document</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__CONDITIONS_DOCUMENT = 4;
    /**
     * The number of structural features of the '<em>Document Root</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT_FEATURE_COUNT = 5;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionSetImpl <em>Condition Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ConditionSetImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConditionSet()
     * @generated
     */
    int CONDITION_SET = 1;
    /**
     * The feature id for the '<em><b>Group</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__GROUP = 0;
    /**
     * The feature id for the '<em><b>Baureihe</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__BAUREIHE = 1;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__DESCRIPTION = 2;
    /**
     * The feature id for the '<em><b>Istufe</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__ISTUFE = 3;
    /**
     * The feature id for the '<em><b>Schemaversion</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__SCHEMAVERSION = 4;
    /**
     * The feature id for the '<em><b>Uuid</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__UUID = 5;
    /**
     * The feature id for the '<em><b>Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__VERSION = 6;
    /**
     * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__CONDITIONS = 7;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET__NAME = 8;
    /**
     * The number of structural features of the '<em>Condition Set</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_SET_FEATURE_COUNT = 9;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl <em>Base Class With ID</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBaseClassWithID()
     * @generated
     */
    int BASE_CLASS_WITH_ID = 92;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BASE_CLASS_WITH_ID__ID = 0;
    /**
     * The number of structural features of the '<em>Base Class With ID</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BASE_CLASS_WITH_ID_FEATURE_COUNT = 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionImpl <em>Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ConditionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCondition()
     * @generated
     */
    int CONDITION = 2;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION__ID = BASE_CLASS_WITH_ID__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION__NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Expression</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION__EXPRESSION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Condition</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITION_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ExpressionImpl <em>Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExpression()
     * @generated
     */
    int EXPRESSION = 17;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXPRESSION__ID = BASE_CLASS_WITH_ID__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXPRESSION__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXPRESSION_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl <em>Signal Comparison Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalComparisonExpression()
     * @generated
     */
    int SIGNAL_COMPARISON_EXPRESSION = 3;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Comparator Signal</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Operator</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Operator Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Signal Comparison Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_COMPARISON_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.CanMessageCheckExpressionImpl <em>Can Message Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.CanMessageCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCanMessageCheckExpression()
     * @generated
     */
    int CAN_MESSAGE_CHECK_EXPRESSION = 4;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Busid</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__BUSID = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Message IDs</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Busid Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Rxtx Flag</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG = EXPRESSION_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Rxtx Flag Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Ext Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER = EXPRESSION_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 8;
    /**
     * The number of structural features of the '<em>Can Message Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 9;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl <em>Lin Message Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLinMessageCheckExpression()
     * @generated
     */
    int LIN_MESSAGE_CHECK_EXPRESSION = 5;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Busid</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__BUSID = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Message IDs</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Busid Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 4;
    /**
     * The number of structural features of the '<em>Lin Message Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 5;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl <em>State Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStateCheckExpression()
     * @generated
     */
    int STATE_CHECK_EXPRESSION = 6;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Check State Active</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Check State</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION__CHECK_STATE = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Check State Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>State Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl <em>Timing Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTimingExpression()
     * @generated
     */
    int TIMING_EXPRESSION = 7;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TIMING_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TIMING_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Maxtime</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TIMING_EXPRESSION__MAXTIME = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Mintime</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TIMING_EXPRESSION__MINTIME = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Timing Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TIMING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TrueExpressionImpl <em>True Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TrueExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTrueExpression()
     * @generated
     */
    int TRUE_EXPRESSION = 8;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRUE_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRUE_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The number of structural features of the '<em>True Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRUE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalExpression()
     * @generated
     */
    int LOGICAL_EXPRESSION = 9;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Expressions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION__EXPRESSIONS = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Operator</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Operator Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Logical Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LOGICAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ReferenceConditionExpressionImpl <em>Reference Condition Expression</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ReferenceConditionExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getReferenceConditionExpression()
     * @generated
     */
    int REFERENCE_CONDITION_EXPRESSION = 10;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_CONDITION_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_CONDITION_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_CONDITION_EXPRESSION__CONDITION = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Reference Condition Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REFERENCE_CONDITION_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl <em>Transition Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTransitionCheckExpression()
     * @generated
     */
    int TRANSITION_CHECK_EXPRESSION = 11;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Stay Active</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Check Transition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Stay Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Transition Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TRANSITION_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl <em>Flex Ray Message Check Expression</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayMessageCheckExpression()
     * @generated
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION = 12;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Payload Preamble</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Zero Frame</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Sync Frame</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Startup Frame</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME = EXPRESSION_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Network Mgmt</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT = EXPRESSION_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Flexray Message Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID = EXPRESSION_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Payload Preamble Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Zero Frame Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 8;
    /**
     * The feature id for the '<em><b>Sync Frame Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 9;
    /**
     * The feature id for the '<em><b>Startup Frame Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 10;
    /**
     * The feature id for the '<em><b>Network Mgmt Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 11;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 12;
    /**
     * The feature id for the '<em><b>Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE = EXPRESSION_FEATURE_COUNT + 13;
    /**
     * The feature id for the '<em><b>Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 14;
    /**
     * The number of structural features of the '<em>Flex Ray Message Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 15;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StopExpressionImpl <em>Stop Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StopExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStopExpression()
     * @generated
     */
    int STOP_EXPRESSION = 13;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STOP_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STOP_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Analyse Art</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STOP_EXPRESSION__ANALYSE_ART = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Analyse Art Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Stop Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STOP_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IVariableReaderWriter
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIVariableReaderWriter()
     * @generated
     */
    int IVARIABLE_READER_WRITER = 94;
    /**
     * The number of structural features of the '<em>IVariable Reader Writer</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IVARIABLE_READER_WRITER_FEATURE_COUNT = 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IOperand <em>IOperand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IOperand
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIOperand()
     * @generated
     */
    int IOPERAND = 33;
    /**
     * The number of structural features of the '<em>IOperand</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IOPERAND_FEATURE_COUNT = IVARIABLE_READER_WRITER_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison Expression Operand</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getISignalComparisonExpressionOperand()
     * @generated
     */
    int ISIGNAL_COMPARISON_EXPRESSION_OPERAND = 36;
    /**
     * The number of structural features of the '<em>ISignal Comparison Expression Operand</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ComparatorSignalImpl <em>Comparator Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ComparatorSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparatorSignal()
     * @generated
     */
    int COMPARATOR_SIGNAL = 14;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPARATOR_SIGNAL__DESCRIPTION = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Comparator Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPARATOR_SIGNAL_FEATURE_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl <em>Constant Comparator Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConstantComparatorValue()
     * @generated
     */
    int CONSTANT_COMPARATOR_VALUE = 15;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONSTANT_COMPARATOR_VALUE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONSTANT_COMPARATOR_VALUE__VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Interpreted Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Constant Comparator Value</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONSTANT_COMPARATOR_VALUE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl <em>Calculation Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCalculationExpression()
     * @generated
     */
    int CALCULATION_EXPRESSION = 16;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CALCULATION_EXPRESSION__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CALCULATION_EXPRESSION__EXPRESSION = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Operands</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CALCULATION_EXPRESSION__OPERANDS = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Calculation Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CALCULATION_EXPRESSION_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VariableReferenceImpl <em>Variable Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VariableReferenceImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableReference()
     * @generated
     */
    int VARIABLE_REFERENCE = 18;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_REFERENCE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Variable</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_REFERENCE__VARIABLE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Variable Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_REFERENCE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionsDocumentImpl <em>Document</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsDocumentImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConditionsDocument()
     * @generated
     */
    int CONDITIONS_DOCUMENT = 19;
    /**
     * The feature id for the '<em><b>Signal References Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET = 0;
    /**
     * The feature id for the '<em><b>Variable Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITIONS_DOCUMENT__VARIABLE_SET = 1;
    /**
     * The feature id for the '<em><b>Condition Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITIONS_DOCUMENT__CONDITION_SET = 2;
    /**
     * The number of structural features of the '<em>Document</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONDITIONS_DOCUMENT_FEATURE_COUNT = 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VariableSetImpl <em>Variable Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VariableSetImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableSet()
     * @generated
     */
    int VARIABLE_SET = 20;
    /**
     * The feature id for the '<em><b>Uuid</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_SET__UUID = 0;
    /**
     * The feature id for the '<em><b>Variables</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_SET__VARIABLES = 1;
    /**
     * The number of structural features of the '<em>Variable Set</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_SET_FEATURE_COUNT = 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractVariableImpl <em>Abstract Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractVariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractVariable()
     * @generated
     */
    int ABSTRACT_VARIABLE = 108;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_VARIABLE__ID = BASE_CLASS_WITH_ID__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_VARIABLE__NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_VARIABLE__DISPLAY_NAME = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_VARIABLE__DESCRIPTION = BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Abstract Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_VARIABLE_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariable()
     * @generated
     */
    int VARIABLE = 21;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__ID = ABSTRACT_VARIABLE__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__NAME = ABSTRACT_VARIABLE__NAME;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__DISPLAY_NAME = ABSTRACT_VARIABLE__DISPLAY_NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__DESCRIPTION = ABSTRACT_VARIABLE__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__DATA_TYPE = ABSTRACT_VARIABLE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__UNIT = ABSTRACT_VARIABLE_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__VARIABLE_FORMAT = ABSTRACT_VARIABLE_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = ABSTRACT_VARIABLE_FEATURE_COUNT + 3;
    /**
     * The number of structural features of the '<em>Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_FEATURE_COUNT = ABSTRACT_VARIABLE_FEATURE_COUNT + 4;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl <em>Signal Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SignalVariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalVariable()
     * @generated
     */
    int SIGNAL_VARIABLE = 22;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__ID = VARIABLE__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__NAME = VARIABLE__NAME;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;
    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__UNIT = VARIABLE__UNIT;
    /**
     * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;
    /**
     * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Interpreted Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__INTERPRETED_VALUE = VARIABLE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Signal</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__SIGNAL = VARIABLE_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM = VARIABLE_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Signal Observers</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__SIGNAL_OBSERVERS = VARIABLE_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Lag</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE__LAG = VARIABLE_FEATURE_COUNT + 4;
    /**
     * The number of structural features of the '<em>Signal Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 5;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ValueVariableImpl <em>Value Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ValueVariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValueVariable()
     * @generated
     */
    int VALUE_VARIABLE = 23;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__ID = VARIABLE__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__NAME = VARIABLE__NAME;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;
    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__UNIT = VARIABLE__UNIT;
    /**
     * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;
    /**
     * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Initial Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__INITIAL_VALUE = VARIABLE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Value Variable Observers</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS = VARIABLE_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Value Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VariableFormatImpl <em>Variable Format</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VariableFormatImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableFormat()
     * @generated
     */
    int VARIABLE_FORMAT = 24;
    /**
     * The feature id for the '<em><b>Digits</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_FORMAT__DIGITS = 0;
    /**
     * The feature id for the '<em><b>Base Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_FORMAT__BASE_DATA_TYPE = 1;
    /**
     * The feature id for the '<em><b>Upper Case</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_FORMAT__UPPER_CASE = 2;
    /**
     * The number of structural features of the '<em>Variable Format</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VARIABLE_FORMAT_FEATURE_COUNT = 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.BaseClassWithSourceReferenceImpl <em>Base Class With Source Reference</em>}'
     * class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.BaseClassWithSourceReferenceImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBaseClassWithSourceReference()
     * @generated
     */
    int BASE_CLASS_WITH_SOURCE_REFERENCE = 97;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BASE_CLASS_WITH_SOURCE_REFERENCE__ID = BASE_CLASS_WITH_ID__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE = BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Base Class With Source Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT = BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl <em>Abstract Observer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractObserver()
     * @generated
     */
    int ABSTRACT_OBSERVER = 25;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__DESCRIPTION = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__LOG_LEVEL = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__VALUE_RANGES = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Active At Start</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER__ACTIVE_AT_START = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 5;
    /**
     * The number of structural features of the '<em>Abstract Observer</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_OBSERVER_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 6;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl <em>Observer Value Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getObserverValueRange()
     * @generated
     */
    int OBSERVER_VALUE_RANGE = 26;
    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OBSERVER_VALUE_RANGE__VALUE = 0;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OBSERVER_VALUE_RANGE__DESCRIPTION = 1;
    /**
     * The feature id for the '<em><b>Value Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OBSERVER_VALUE_RANGE__VALUE_TYPE = 2;
    /**
     * The number of structural features of the '<em>Observer Value Range</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OBSERVER_VALUE_RANGE_FEATURE_COUNT = 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SignalObserverImpl <em>Signal Observer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SignalObserverImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalObserver()
     * @generated
     */
    int SIGNAL_OBSERVER = 27;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__ID = ABSTRACT_OBSERVER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__SOURCE_REFERENCE = ABSTRACT_OBSERVER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__NAME = ABSTRACT_OBSERVER__NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__DESCRIPTION = ABSTRACT_OBSERVER__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__LOG_LEVEL = ABSTRACT_OBSERVER__LOG_LEVEL;
    /**
     * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__VALUE_RANGES = ABSTRACT_OBSERVER__VALUE_RANGES;
    /**
     * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__VALUE_RANGES_TMPL_PARAM = ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Active At Start</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER__ACTIVE_AT_START = ABSTRACT_OBSERVER__ACTIVE_AT_START;
    /**
     * The number of structural features of the '<em>Signal Observer</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_OBSERVER_FEATURE_COUNT = ABSTRACT_OBSERVER_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceSetImpl <em>Signal Reference Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SignalReferenceSetImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalReferenceSet()
     * @generated
     */
    int SIGNAL_REFERENCE_SET = 28;
    /**
     * The feature id for the '<em><b>Messages</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_REFERENCE_SET__MESSAGES = 0;
    /**
     * The number of structural features of the '<em>Signal Reference Set</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_REFERENCE_SET_FEATURE_COUNT = 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceImpl <em>Signal Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SignalReferenceImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalReference()
     * @generated
     */
    int SIGNAL_REFERENCE = 29;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_REFERENCE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Signal</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_REFERENCE__SIGNAL = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Signal Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SIGNAL_REFERENCE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ISignalOrReference <em>ISignal Or Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ISignalOrReference
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getISignalOrReference()
     * @generated
     */
    int ISIGNAL_OR_REFERENCE = 30;
    /**
     * The number of structural features of the '<em>ISignal Or Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ISIGNAL_OR_REFERENCE_FEATURE_COUNT = ISIGNAL_COMPARISON_EXPRESSION_OPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl <em>Bit Pattern Comparator Value</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBitPatternComparatorValue()
     * @generated
     */
    int BIT_PATTERN_COMPARATOR_VALUE = 31;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BIT_PATTERN_COMPARATOR_VALUE__DESCRIPTION = COMPARATOR_SIGNAL__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BIT_PATTERN_COMPARATOR_VALUE__VALUE = COMPARATOR_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Startbit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BIT_PATTERN_COMPARATOR_VALUE__STARTBIT = COMPARATOR_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Bit Pattern Comparator Value</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BIT_PATTERN_COMPARATOR_VALUE_FEATURE_COUNT = COMPARATOR_SIGNAL_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.NotExpressionImpl <em>Not Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.NotExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNotExpression()
     * @generated
     */
    int NOT_EXPRESSION = 32;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NOT_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NOT_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Expression</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NOT_EXPRESSION__EXPRESSION = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Operator</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NOT_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Not Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NOT_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IComputeVariableActionOperand <em>ICompute Variable Action Operand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IComputeVariableActionOperand
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIComputeVariableActionOperand()
     * @generated
     */
    int ICOMPUTE_VARIABLE_ACTION_OPERAND = 34;
    /**
     * The number of structural features of the '<em>ICompute Variable Action Operand</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ICOMPUTE_VARIABLE_ACTION_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IStringOperand <em>IString Operand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IStringOperand
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStringOperand()
     * @generated
     */
    int ISTRING_OPERAND = 35;
    /**
     * The number of structural features of the '<em>IString Operand</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ISTRING_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IOperation <em>IOperation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IOperation
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIOperation()
     * @generated
     */
    int IOPERATION = 37;
    /**
     * The number of structural features of the '<em>IOperation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IOPERATION_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.IStringOperation <em>IString Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.IStringOperation
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStringOperation()
     * @generated
     */
    int ISTRING_OPERATION = 38;
    /**
     * The number of structural features of the '<em>IString Operation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ISTRING_OPERATION_FEATURE_COUNT = IOPERATION_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.INumericOperand <em>INumeric Operand</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.INumericOperand
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getINumericOperand()
     * @generated
     */
    int INUMERIC_OPERAND = 39;
    /**
     * The number of structural features of the '<em>INumeric Operand</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INUMERIC_OPERAND_FEATURE_COUNT = IOPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.INumericOperation <em>INumeric Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.INumericOperation
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getINumericOperation()
     * @generated
     */
    int INUMERIC_OPERATION = 40;
    /**
     * The number of structural features of the '<em>INumeric Operation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int INUMERIC_OPERATION_FEATURE_COUNT = INUMERIC_OPERAND_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ValueVariableObserverImpl <em>Value Variable Observer</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ValueVariableObserverImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValueVariableObserver()
     * @generated
     */
    int VALUE_VARIABLE_OBSERVER = 41;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__ID = ABSTRACT_OBSERVER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__SOURCE_REFERENCE = ABSTRACT_OBSERVER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__NAME = ABSTRACT_OBSERVER__NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__DESCRIPTION = ABSTRACT_OBSERVER__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__LOG_LEVEL = ABSTRACT_OBSERVER__LOG_LEVEL;
    /**
     * The feature id for the '<em><b>Value Ranges</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__VALUE_RANGES = ABSTRACT_OBSERVER__VALUE_RANGES;
    /**
     * The feature id for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__VALUE_RANGES_TMPL_PARAM = ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Active At Start</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER__ACTIVE_AT_START = ABSTRACT_OBSERVER__ACTIVE_AT_START;
    /**
     * The number of structural features of the '<em>Value Variable Observer</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VALUE_VARIABLE_OBSERVER_FEATURE_COUNT = ABSTRACT_OBSERVER_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ParseStringToDoubleImpl <em>Parse String To Double</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ParseStringToDoubleImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getParseStringToDouble()
     * @generated
     */
    int PARSE_STRING_TO_DOUBLE = 42;
    /**
     * The feature id for the '<em><b>String To Parse</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE = INUMERIC_OPERATION_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Parse String To Double</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PARSE_STRING_TO_DOUBLE_FEATURE_COUNT = INUMERIC_OPERATION_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StringExpressionImpl <em>String Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StringExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringExpression()
     * @generated
     */
    int STRING_EXPRESSION = 43;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The number of structural features of the '<em>String Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.MatchesImpl <em>Matches</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.MatchesImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMatches()
     * @generated
     */
    int MATCHES = 44;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES__ID = STRING_EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES__DESCRIPTION = STRING_EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES__REGEX = STRING_EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES__DYNAMIC_REGEX = STRING_EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>String To Check</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES__STRING_TO_CHECK = STRING_EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Matches</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MATCHES_FEATURE_COUNT = STRING_EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl <em>Extract</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ExtractImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExtract()
     * @generated
     */
    int EXTRACT = 45;
    /**
     * The feature id for the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT__REGEX = ISTRING_OPERATION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT__DYNAMIC_REGEX = ISTRING_OPERATION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>String To Extract</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT__STRING_TO_EXTRACT = ISTRING_OPERATION_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Group Index</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT__GROUP_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 3;
    /**
     * The number of structural features of the '<em>Extract</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 4;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SubstringImpl <em>Substring</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SubstringImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSubstring()
     * @generated
     */
    int SUBSTRING = 46;
    /**
     * The feature id for the '<em><b>String To Extract</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SUBSTRING__STRING_TO_EXTRACT = ISTRING_OPERATION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Start Index</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SUBSTRING__START_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>End Index</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SUBSTRING__END_INDEX = ISTRING_OPERATION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Substring</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SUBSTRING_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ParseNumericToStringImpl <em>Parse Numeric To String</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ParseNumericToStringImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getParseNumericToString()
     * @generated
     */
    int PARSE_NUMERIC_TO_STRING = 47;
    /**
     * The feature id for the '<em><b>Numeric Operand To Be Parsed</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED = ISTRING_OPERATION_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Parse Numeric To String</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PARSE_NUMERIC_TO_STRING_FEATURE_COUNT = ISTRING_OPERATION_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StringLengthImpl <em>String Length</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StringLengthImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringLength()
     * @generated
     */
    int STRING_LENGTH = 48;
    /**
     * The feature id for the '<em><b>String Operand</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_LENGTH__STRING_OPERAND = INUMERIC_OPERATION_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>String Length</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_LENGTH_FEATURE_COUNT = INUMERIC_OPERATION_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractMessageImpl <em>Abstract Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractMessage()
     * @generated
     */
    int ABSTRACT_MESSAGE = 49;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_MESSAGE__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_MESSAGE__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_MESSAGE__SIGNALS = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_MESSAGE__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Abstract Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_MESSAGE_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl <em>Abstract Bus Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractBusMessage()
     * @generated
     */
    int ABSTRACT_BUS_MESSAGE = 84;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__ID = ABSTRACT_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE = ABSTRACT_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__SIGNALS = ABSTRACT_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__NAME = ABSTRACT_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__BUS_ID = ABSTRACT_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE = ABSTRACT_MESSAGE_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE__FILTERS = ABSTRACT_MESSAGE_FEATURE_COUNT + 3;
    /**
     * The number of structural features of the '<em>Abstract Bus Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_BUS_MESSAGE_FEATURE_COUNT = ABSTRACT_MESSAGE_FEATURE_COUNT + 4;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPMessageImpl <em>Some IP Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SomeIPMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMessage()
     * @generated
     */
    int SOME_IP_MESSAGE = 50;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Some IP Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE__SOME_IP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Some IP Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl <em>Abstract Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractSignal()
     * @generated
     */
    int ABSTRACT_SIGNAL = 51;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL__NAME = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL__DECODE_STRATEGY = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL__EXTRACT_STRATEGY = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Abstract Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_SIGNAL_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ContainerSignalImpl <em>Container Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ContainerSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getContainerSignal()
     * @generated
     */
    int CONTAINER_SIGNAL = 52;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__ID = ABSTRACT_SIGNAL__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;
    /**
     * The feature id for the '<em><b>Contained Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL__CONTAINED_SIGNALS = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Container Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTAINER_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractFilterImpl <em>Abstract Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.AbstractFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractFilter()
     * @generated
     */
    int ABSTRACT_FILTER = 53;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_FILTER__ID = BASE_CLASS_WITH_SOURCE_REFERENCE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_FILTER__SOURCE_REFERENCE = BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_FILTER__PAYLOAD_LENGTH = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Abstract Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ABSTRACT_FILTER_FEATURE_COUNT = BASE_CLASS_WITH_SOURCE_REFERENCE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl <em>Ethernet Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetFilter()
     * @generated
     */
    int ETHERNET_FILTER = 54;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Source MAC</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__SOURCE_MAC = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Dest MAC</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__DEST_MAC = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Ether Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__ETHER_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Inner Vlan Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__INNER_VLAN_ID = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Outer Vlan Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__OUTER_VLAN_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Inner Vlan CFI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__INNER_VLAN_CFI = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Outer Vlan CFI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__OUTER_VLAN_CFI = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Inner Vlan VID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__INNER_VLAN_VID = ABSTRACT_FILTER_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Outer Vlan VID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__OUTER_VLAN_VID = ABSTRACT_FILTER_FEATURE_COUNT + 8;
    /**
     * The feature id for the '<em><b>CRC</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__CRC = ABSTRACT_FILTER_FEATURE_COUNT + 9;
    /**
     * The feature id for the '<em><b>Inner Vlan TPID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__INNER_VLAN_TPID = ABSTRACT_FILTER_FEATURE_COUNT + 10;
    /**
     * The feature id for the '<em><b>Outer Vlan TPID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER__OUTER_VLAN_TPID = ABSTRACT_FILTER_FEATURE_COUNT + 11;
    /**
     * The number of structural features of the '<em>Ethernet Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 12;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TPFilterImpl <em>TP Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TPFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTPFilter()
     * @generated
     */
    int TP_FILTER = 96;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Source Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER__SOURCE_PORT = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Dest Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER__DEST_PORT = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>TP Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TP_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UDPFilterImpl <em>UDP Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UDPFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPFilter()
     * @generated
     */
    int UDP_FILTER = 55;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__ID = TP_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__SOURCE_REFERENCE = TP_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__PAYLOAD_LENGTH = TP_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Source Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__SOURCE_PORT = TP_FILTER__SOURCE_PORT;
    /**
     * The feature id for the '<em><b>Dest Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__DEST_PORT = TP_FILTER__DEST_PORT;
    /**
     * The feature id for the '<em><b>Checksum</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER__CHECKSUM = TP_FILTER_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>UDP Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_FILTER_FEATURE_COUNT = TP_FILTER_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl <em>TCP Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TCPFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTCPFilter()
     * @generated
     */
    int TCP_FILTER = 56;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__ID = TP_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__SOURCE_REFERENCE = TP_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__PAYLOAD_LENGTH = TP_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Source Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__SOURCE_PORT = TP_FILTER__SOURCE_PORT;
    /**
     * The feature id for the '<em><b>Dest Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__DEST_PORT = TP_FILTER__DEST_PORT;
    /**
     * The feature id for the '<em><b>Sequence Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__SEQUENCE_NUMBER = TP_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Acknowledgement Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__ACKNOWLEDGEMENT_NUMBER = TP_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Tcp Flags</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__TCP_FLAGS = TP_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Stream Analysis Flags</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__STREAM_ANALYSIS_FLAGS = TP_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Stream Analysis Flags Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM = TP_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Stream Valid Payload Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET = TP_FILTER_FEATURE_COUNT + 5;
    /**
     * The number of structural features of the '<em>TCP Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_FILTER_FEATURE_COUNT = TP_FILTER_FEATURE_COUNT + 6;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl <em>IPv4 Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPv4Filter()
     * @generated
     */
    int IPV4_FILTER = 57;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Protocol Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__PROTOCOL_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Protocol Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Source IP</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__SOURCE_IP = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Dest IP</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__DEST_IP = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Time To Live</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER__TIME_TO_LIVE = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The number of structural features of the '<em>IPv4 Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl <em>Some IP Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPFilter()
     * @generated
     */
    int SOME_IP_FILTER = 58;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Service Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__SERVICE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Method Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__METHOD_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Method Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Method Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__METHOD_ID = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Client Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__CLIENT_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Session Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__SESSION_ID = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Protocol Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__PROTOCOL_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Interface Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__INTERFACE_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Message Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__MESSAGE_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 8;
    /**
     * The feature id for the '<em><b>Message Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 9;
    /**
     * The feature id for the '<em><b>Return Code</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__RETURN_CODE = ABSTRACT_FILTER_FEATURE_COUNT + 10;
    /**
     * The feature id for the '<em><b>Return Code Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 11;
    /**
     * The feature id for the '<em><b>Some IP Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER__SOME_IP_LENGTH = ABSTRACT_FILTER_FEATURE_COUNT + 12;
    /**
     * The number of structural features of the '<em>Some IP Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IP_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 13;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ForEachExpressionImpl <em>For Each Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ForEachExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getForEachExpression()
     * @generated
     */
    int FOR_EACH_EXPRESSION = 59;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FOR_EACH_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FOR_EACH_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Filter Expressions</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Container Signal</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FOR_EACH_EXPRESSION__CONTAINER_SIGNAL = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>For Each Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FOR_EACH_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.MessageCheckExpressionImpl <em>Message Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.MessageCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMessageCheckExpression()
     * @generated
     */
    int MESSAGE_CHECK_EXPRESSION = 60;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MESSAGE_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MESSAGE_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Message</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MESSAGE_CHECK_EXPRESSION__MESSAGE = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Message Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MESSAGE_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPSDFilterImpl <em>Some IPSD Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SomeIPSDFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDFilter()
     * @generated
     */
    int SOME_IPSD_FILTER = 61;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Flags</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__FLAGS = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Flags Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__FLAGS_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Sd Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__SD_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Sd Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Instance Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__INSTANCE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Ttl</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__TTL = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Major Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__MAJOR_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Minor Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__MINOR_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Event Group Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__EVENT_GROUP_ID = ABSTRACT_FILTER_FEATURE_COUNT + 8;
    /**
     * The feature id for the '<em><b>Index First Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__INDEX_FIRST_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 9;
    /**
     * The feature id for the '<em><b>Index Second Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__INDEX_SECOND_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 10;
    /**
     * The feature id for the '<em><b>Number First Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__NUMBER_FIRST_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 11;
    /**
     * The feature id for the '<em><b>Number Second Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__NUMBER_SECOND_OPTION = ABSTRACT_FILTER_FEATURE_COUNT + 12;
    /**
     * The feature id for the '<em><b>Service Id Some IPSD</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD = ABSTRACT_FILTER_FEATURE_COUNT + 13;
    /**
     * The number of structural features of the '<em>Some IPSD Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 14;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPSDMessageImpl <em>Some IPSD Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SomeIPSDMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDMessage()
     * @generated
     */
    int SOME_IPSD_MESSAGE = 62;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Some IPSD Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE__SOME_IPSD_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Some IPSD Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOME_IPSD_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl <em>Double Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleSignal()
     * @generated
     */
    int DOUBLE_SIGNAL = 63;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__ID = ABSTRACT_SIGNAL__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;
    /**
     * The feature id for the '<em><b>Ignore Invalid Values</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__IGNORE_INVALID_VALUES = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Double Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StringSignalImpl <em>String Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StringSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringSignal()
     * @generated
     */
    int STRING_SIGNAL = 64;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL__ID = ABSTRACT_SIGNAL__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;
    /**
     * The number of structural features of the '<em>String Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DecodeStrategyImpl <em>Decode Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DecodeStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDecodeStrategy()
     * @generated
     */
    int DECODE_STRATEGY = 65;
    /**
     * The number of structural features of the '<em>Decode Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DECODE_STRATEGY_FEATURE_COUNT = 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl <em>Double Decode Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleDecodeStrategy()
     * @generated
     */
    int DOUBLE_DECODE_STRATEGY = 66;
    /**
     * The feature id for the '<em><b>Factor</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_DECODE_STRATEGY__FACTOR = DECODE_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_DECODE_STRATEGY__OFFSET = DECODE_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Double Decode Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOUBLE_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ExtractStrategyImpl <em>Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExtractStrategy()
     * @generated
     */
    int EXTRACT_STRATEGY = 67;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT_STRATEGY__ABSTRACT_SIGNAL = 0;
    /**
     * The number of structural features of the '<em>Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXTRACT_STRATEGY_FEATURE_COUNT = 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl <em>Universal Payload Extract
     * Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUniversalPayloadExtractStrategy()
     * @generated
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY = 68;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The feature id for the '<em><b>Extraction Rule</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE = EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Byte Order</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER = EXTRACT_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Signal Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE = EXTRACT_STRATEGY_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 4;
    /**
     * The number of structural features of the '<em>Universal Payload Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 5;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.EmptyExtractStrategyImpl <em>Empty Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.EmptyExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEmptyExtractStrategy()
     * @generated
     */
    int EMPTY_EXTRACT_STRATEGY = 69;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPTY_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The number of structural features of the '<em>Empty Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPTY_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.CANFilterImpl <em>CAN Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.CANFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANFilter()
     * @generated
     */
    int CAN_FILTER = 70;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Frame Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__FRAME_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Rxtx Flag</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__RXTX_FLAG = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Rxtx Flag Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__RXTX_FLAG_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Ext Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__EXT_IDENTIFIER = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Frame Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__FRAME_TYPE = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Frame Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER__FRAME_TYPE_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 7;
    /**
     * The number of structural features of the '<em>CAN Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 8;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.LINFilterImpl <em>LIN Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.LINFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLINFilter()
     * @generated
     */
    int LIN_FILTER = 71;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Frame Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER__FRAME_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>LIN Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl <em>Flex Ray Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayFilter()
     * @generated
     */
    int FLEX_RAY_FILTER = 72;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Slot Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__SLOT_ID = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Cycle Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__CYCLE_OFFSET = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Cycle Repetition</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__CYCLE_REPETITION = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Channel</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__CHANNEL = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Channel Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The number of structural features of the '<em>Flex Ray Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DLTFilterImpl <em>DLT Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DLTFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLTFilter()
     * @generated
     */
    int DLT_FILTER = 73;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Ecu ID ECU</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__ECU_ID_ECU = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Session ID SEID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__SESSION_ID_SEID = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Application ID APID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__APPLICATION_ID_APID = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Context ID CTID</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__CONTEXT_ID_CTID = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The feature id for the '<em><b>Message Type MSTP</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_TYPE_MSTP = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The feature id for the '<em><b>Message Log Info MSLI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_LOG_INFO_MSLI = ABSTRACT_FILTER_FEATURE_COUNT + 5;
    /**
     * The feature id for the '<em><b>Message Trace Info MSTI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_TRACE_INFO_MSTI = ABSTRACT_FILTER_FEATURE_COUNT + 6;
    /**
     * The feature id for the '<em><b>Message Bus Info MSBI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_BUS_INFO_MSBI = ABSTRACT_FILTER_FEATURE_COUNT + 7;
    /**
     * The feature id for the '<em><b>Message Control Info MSCI</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI = ABSTRACT_FILTER_FEATURE_COUNT + 8;
    /**
     * The feature id for the '<em><b>Message Log Info MSLI Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 9;
    /**
     * The feature id for the '<em><b>Message Trace Info MSTI Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 10;
    /**
     * The feature id for the '<em><b>Message Bus Info MSBI Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 11;
    /**
     * The feature id for the '<em><b>Message Control Info MSCI Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 12;
    /**
     * The feature id for the '<em><b>Message Type MSTP Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM = ABSTRACT_FILTER_FEATURE_COUNT + 13;
    /**
     * The number of structural features of the '<em>DLT Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 14;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UDPNMFilterImpl <em>UDPNM Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UDPNMFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPNMFilter()
     * @generated
     */
    int UDPNM_FILTER = 74;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Source Node Identifier</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__SOURCE_NODE_IDENTIFIER = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Control Bit Vector</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__CONTROL_BIT_VECTOR = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Pwf Status</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__PWF_STATUS = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Teilnetz Status</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER__TEILNETZ_STATUS = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The number of structural features of the '<em>UDPNM Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 4;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.CANMessageImpl <em>CAN Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.CANMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANMessage()
     * @generated
     */
    int CAN_MESSAGE = 75;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Can Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE__CAN_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>CAN Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CAN_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.LINMessageImpl <em>LIN Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.LINMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLINMessage()
     * @generated
     */
    int LIN_MESSAGE = 76;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Lin Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE__LIN_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>LIN Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIN_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageImpl <em>Flex Ray Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.FlexRayMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayMessage()
     * @generated
     */
    int FLEX_RAY_MESSAGE = 77;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Flex Ray Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE__FLEX_RAY_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Flex Ray Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int FLEX_RAY_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.DLTMessageImpl <em>DLT Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.DLTMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLTMessage()
     * @generated
     */
    int DLT_MESSAGE = 78;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The number of structural features of the '<em>DLT Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DLT_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UDPMessageImpl <em>UDP Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UDPMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPMessage()
     * @generated
     */
    int UDP_MESSAGE = 79;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Udp Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE__UDP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>UDP Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.TCPMessageImpl <em>TCP Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.TCPMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTCPMessage()
     * @generated
     */
    int TCP_MESSAGE = 80;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Tcp Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE__TCP_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>TCP Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int TCP_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UDPNMMessageImpl <em>UDPNM Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UDPNMMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPNMMessage()
     * @generated
     */
    int UDPNM_MESSAGE = 81;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Udpnm Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE__UDPNM_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>UDPNM Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UDPNM_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTMessageImpl <em>Verbose DLT Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTMessage()
     * @generated
     */
    int VERBOSE_DLT_MESSAGE = 82;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__ID = DLT_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__SOURCE_REFERENCE = DLT_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__SIGNALS = DLT_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__NAME = DLT_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__BUS_ID = DLT_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__BUS_ID_TMPL_PARAM = DLT_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__BUS_ID_RANGE = DLT_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__FILTERS = DLT_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Dlt Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE__DLT_FILTER = DLT_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Verbose DLT Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_MESSAGE_FEATURE_COUNT = DLT_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl <em>Universal Payload With Legacy
     * Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUniversalPayloadWithLegacyExtractStrategy()
     * @generated
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY = 83;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The feature id for the '<em><b>Extraction Rule</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__EXTRACTION_RULE = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE;
    /**
     * The feature id for the '<em><b>Byte Order</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__BYTE_ORDER = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER;
    /**
     * The feature id for the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Signal Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE;
    /**
     * The feature id for the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Start Bit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Universal Payload With Legacy Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY_FEATURE_COUNT = UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginFilterImpl <em>Plugin Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginFilter()
     * @generated
     */
    int PLUGIN_FILTER = 85;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Plugin Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER__PLUGIN_NAME = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Plugin Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER__PLUGIN_VERSION = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Plugin Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginMessageImpl <em>Plugin Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginMessage()
     * @generated
     */
    int PLUGIN_MESSAGE = 86;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE__ID = ABSTRACT_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE__SOURCE_REFERENCE = ABSTRACT_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE__SIGNALS = ABSTRACT_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE__NAME = ABSTRACT_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Plugin Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE__PLUGIN_FILTER = ABSTRACT_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Plugin Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_MESSAGE_FEATURE_COUNT = ABSTRACT_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginSignalImpl <em>Plugin Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginSignal()
     * @generated
     */
    int PLUGIN_SIGNAL = 87;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__ID = DOUBLE_SIGNAL__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__SOURCE_REFERENCE = DOUBLE_SIGNAL__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__NAME = DOUBLE_SIGNAL__NAME;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__DECODE_STRATEGY = DOUBLE_SIGNAL__DECODE_STRATEGY;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__EXTRACT_STRATEGY = DOUBLE_SIGNAL__EXTRACT_STRATEGY;
    /**
     * The feature id for the '<em><b>Ignore Invalid Values</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__IGNORE_INVALID_VALUES = DOUBLE_SIGNAL__IGNORE_INVALID_VALUES;
    /**
     * The feature id for the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM = DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM;
    /**
     * The number of structural features of the '<em>Plugin Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_SIGNAL_FEATURE_COUNT = DOUBLE_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl <em>Plugin State Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginStateExtractStrategy()
     * @generated
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY = 88;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The feature id for the '<em><b>States</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY__STATES = EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>States Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>States Active</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE = EXTRACT_STRATEGY_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>States Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM = EXTRACT_STRATEGY_FEATURE_COUNT + 3;
    /**
     * The number of structural features of the '<em>Plugin State Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_STATE_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 4;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginResultExtractStrategyImpl <em>Plugin Result Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginResultExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginResultExtractStrategy()
     * @generated
     */
    int PLUGIN_RESULT_EXTRACT_STRATEGY = 89;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_RESULT_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The feature id for the '<em><b>Result Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE = EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Plugin Result Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_RESULT_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.EmptyDecodeStrategyImpl <em>Empty Decode Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.EmptyDecodeStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEmptyDecodeStrategy()
     * @generated
     */
    int EMPTY_DECODE_STRATEGY = 90;
    /**
     * The number of structural features of the '<em>Empty Decode Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EMPTY_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl <em>Plugin Check Expression</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginCheckExpression()
     * @generated
     */
    int PLUGIN_CHECK_EXPRESSION = 91;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION__ID = EXPRESSION__ID;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION__DESCRIPTION = EXPRESSION__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Evaluation Behaviour</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR = EXPRESSION_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Signal To Check</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK = EXPRESSION_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Evaluation Behaviour Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM = EXPRESSION_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Plugin Check Expression</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PLUGIN_CHECK_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl <em>Header Signal</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHeaderSignal()
     * @generated
     */
    int HEADER_SIGNAL = 93;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__ID = ABSTRACT_SIGNAL__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__SOURCE_REFERENCE = ABSTRACT_SIGNAL__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__NAME = ABSTRACT_SIGNAL__NAME;
    /**
     * The feature id for the '<em><b>Decode Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__DECODE_STRATEGY = ABSTRACT_SIGNAL__DECODE_STRATEGY;
    /**
     * The feature id for the '<em><b>Extract Strategy</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__EXTRACT_STRATEGY = ABSTRACT_SIGNAL__EXTRACT_STRATEGY;
    /**
     * The feature id for the '<em><b>Attribute</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__ATTRIBUTE = ABSTRACT_SIGNAL_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Attribute Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM = ABSTRACT_SIGNAL_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Header Signal</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int HEADER_SIGNAL_FEATURE_COUNT = ABSTRACT_SIGNAL_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.IStateTransitionReferenceImpl <em>IState Transition Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.IStateTransitionReferenceImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStateTransitionReference()
     * @generated
     */
    int ISTATE_TRANSITION_REFERENCE = 95;
    /**
     * The number of structural features of the '<em>IState Transition Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ISTATE_TRANSITION_REFERENCE_FEATURE_COUNT = 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.SourceReferenceImpl <em>Source Reference</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.SourceReferenceImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSourceReference()
     * @generated
     */
    int SOURCE_REFERENCE = 98;
    /**
     * The feature id for the '<em><b>Serialized Reference</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOURCE_REFERENCE__SERIALIZED_REFERENCE = 0;
    /**
     * The number of structural features of the '<em>Source Reference</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int SOURCE_REFERENCE_FEATURE_COUNT = 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.EthernetMessageImpl <em>Ethernet Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.EthernetMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetMessage()
     * @generated
     */
    int ETHERNET_MESSAGE = 99;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Ethernet Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE__ETHERNET_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Ethernet Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ETHERNET_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.IPv4MessageImpl <em>IPv4 Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.IPv4MessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPv4Message()
     * @generated
     */
    int IPV4_MESSAGE = 100;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__ID = ABSTRACT_BUS_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__SOURCE_REFERENCE = ABSTRACT_BUS_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__SIGNALS = ABSTRACT_BUS_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__NAME = ABSTRACT_BUS_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__BUS_ID = ABSTRACT_BUS_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__BUS_ID_TMPL_PARAM = ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__BUS_ID_RANGE = ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__FILTERS = ABSTRACT_BUS_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>IPv4 Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE__IPV4_FILTER = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>IPv4 Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IPV4_MESSAGE_FEATURE_COUNT = ABSTRACT_BUS_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTMessageImpl <em>Non Verbose DLT Message</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.NonVerboseDLTMessageImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonVerboseDLTMessage()
     * @generated
     */
    int NON_VERBOSE_DLT_MESSAGE = 101;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__ID = DLT_MESSAGE__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__SOURCE_REFERENCE = DLT_MESSAGE__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Signals</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__SIGNALS = DLT_MESSAGE__SIGNALS;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__NAME = DLT_MESSAGE__NAME;
    /**
     * The feature id for the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__BUS_ID = DLT_MESSAGE__BUS_ID;
    /**
     * The feature id for the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__BUS_ID_TMPL_PARAM = DLT_MESSAGE__BUS_ID_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__BUS_ID_RANGE = DLT_MESSAGE__BUS_ID_RANGE;
    /**
     * The feature id for the '<em><b>Filters</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__FILTERS = DLT_MESSAGE__FILTERS;
    /**
     * The feature id for the '<em><b>Non Verbose Dlt Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER = DLT_MESSAGE_FEATURE_COUNT + 0;
    /**
     * The number of structural features of the '<em>Non Verbose DLT Message</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_MESSAGE_FEATURE_COUNT = DLT_MESSAGE_FEATURE_COUNT + 1;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StringDecodeStrategyImpl <em>String Decode Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StringDecodeStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringDecodeStrategy()
     * @generated
     */
    int STRING_DECODE_STRATEGY = 102;
    /**
     * The feature id for the '<em><b>String Termination</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_DECODE_STRATEGY__STRING_TERMINATION = DECODE_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>String Termination Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM = DECODE_STRATEGY_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>String Decode Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRING_DECODE_STRATEGY_FEATURE_COUNT = DECODE_STRATEGY_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl <em>Non Verbose DLT Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonVerboseDLTFilter()
     * @generated
     */
    int NON_VERBOSE_DLT_FILTER = 103;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Message Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER__MESSAGE_ID = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Non Verbose DLT Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int NON_VERBOSE_DLT_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTExtractStrategyImpl <em>Verbose DLT Extract Strategy</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTExtractStrategyImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTExtractStrategy()
     * @generated
     */
    int VERBOSE_DLT_EXTRACT_STRATEGY = 104;
    /**
     * The feature id for the '<em><b>Abstract Signal</b></em>' container reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_EXTRACT_STRATEGY__ABSTRACT_SIGNAL = EXTRACT_STRATEGY__ABSTRACT_SIGNAL;
    /**
     * The number of structural features of the '<em>Verbose DLT Extract Strategy</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_EXTRACT_STRATEGY_FEATURE_COUNT = EXTRACT_STRATEGY_FEATURE_COUNT + 0;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.RegexOperation <em>Regex Operation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.RegexOperation
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getRegexOperation()
     * @generated
     */
    int REGEX_OPERATION = 105;
    /**
     * The feature id for the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REGEX_OPERATION__REGEX = 0;
    /**
     * The feature id for the '<em><b>Dynamic Regex</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REGEX_OPERATION__DYNAMIC_REGEX = 1;
    /**
     * The number of structural features of the '<em>Regex Operation</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int REGEX_OPERATION_FEATURE_COUNT = 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl <em>Payload Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPayloadFilter()
     * @generated
     */
    int PAYLOAD_FILTER = 106;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Index</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__INDEX = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Mask</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__MASK = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER__VALUE = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The number of structural features of the '<em>Payload Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int PAYLOAD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 3;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl <em>Verbose DLT Payload Filter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTPayloadFilter()
     * @generated
     */
    int VERBOSE_DLT_PAYLOAD_FILTER = 107;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER__ID = ABSTRACT_FILTER__ID;
    /**
     * The feature id for the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER__SOURCE_REFERENCE = ABSTRACT_FILTER__SOURCE_REFERENCE;
    /**
     * The feature id for the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER__PAYLOAD_LENGTH = ABSTRACT_FILTER__PAYLOAD_LENGTH;
    /**
     * The feature id for the '<em><b>Regex</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER__REGEX = ABSTRACT_FILTER_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Contains Any</b></em>' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY = ABSTRACT_FILTER_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Verbose DLT Payload Filter</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VERBOSE_DLT_PAYLOAD_FILTER_FEATURE_COUNT = ABSTRACT_FILTER_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.StructureVariableImpl <em>Structure Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.StructureVariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStructureVariable()
     * @generated
     */
    int STRUCTURE_VARIABLE = 110;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl <em>Computed Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComputedVariable()
     * @generated
     */
    int COMPUTED_VARIABLE = 109;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__ID = VARIABLE__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__NAME = VARIABLE__NAME;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__DISPLAY_NAME = VARIABLE__DISPLAY_NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__DESCRIPTION = VARIABLE__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Data Type</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__DATA_TYPE = VARIABLE__DATA_TYPE;
    /**
     * The feature id for the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__UNIT = VARIABLE__UNIT;
    /**
     * The feature id for the '<em><b>Variable Format</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__VARIABLE_FORMAT = VARIABLE__VARIABLE_FORMAT;
    /**
     * The feature id for the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = VARIABLE__VARIABLE_FORMAT_TMPL_PARAM;
    /**
     * The feature id for the '<em><b>Operands</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__OPERANDS = VARIABLE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE__EXPRESSION = VARIABLE_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Computed Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMPUTED_VARIABLE_FEATURE_COUNT = VARIABLE_FEATURE_COUNT + 2;
    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__ID = ABSTRACT_VARIABLE__ID;
    /**
     * The feature id for the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__NAME = ABSTRACT_VARIABLE__NAME;
    /**
     * The feature id for the '<em><b>Display Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__DISPLAY_NAME = ABSTRACT_VARIABLE__DISPLAY_NAME;
    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__DESCRIPTION = ABSTRACT_VARIABLE__DESCRIPTION;
    /**
     * The feature id for the '<em><b>Variables</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__VARIABLES = ABSTRACT_VARIABLE_FEATURE_COUNT + 0;
    /**
     * The feature id for the '<em><b>Variable References</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE__VARIABLE_REFERENCES = ABSTRACT_VARIABLE_FEATURE_COUNT + 1;
    /**
     * The number of structural features of the '<em>Structure Variable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STRUCTURE_VARIABLE_FEATURE_COUNT = ABSTRACT_VARIABLE_FEATURE_COUNT + 2;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.CheckType <em>Check Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCheckType()
     * @generated
     */
    int CHECK_TYPE = 111;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.Comparator <em>Comparator</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.Comparator
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparator()
     * @generated
     */
    int COMPARATOR = 112;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.LogicalOperatorType <em>Logical Operator Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalOperatorType()
     * @generated
     */
    int LOGICAL_OPERATOR_TYPE = 113;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SignalDataType <em>Signal Data Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SignalDataType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalDataType()
     * @generated
     */
    int SIGNAL_DATA_TYPE = 114;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.FlexChannelType <em>Flex Channel Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.FlexChannelType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexChannelType()
     * @generated
     */
    int FLEX_CHANNEL_TYPE = 115;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ByteOrderType <em>Byte Order Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ByteOrderType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getByteOrderType()
     * @generated
     */
    int BYTE_ORDER_TYPE = 116;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.FormatDataType <em>Format Data Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.FormatDataType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFormatDataType()
     * @generated
     */
    int FORMAT_DATA_TYPE = 117;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection <em>Flex Ray Header Flag Selection</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayHeaderFlagSelection()
     * @generated
     */
    int FLEX_RAY_HEADER_FLAG_SELECTION = 118;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum <em>Boolean Or Template Placeholder Enum</em>}'
     * enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBooleanOrTemplatePlaceholderEnum()
     * @generated
     */
    int BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM = 119;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum <em>Protocol Type Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getProtocolTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 120;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection <em>Ethernet Protocol Type Selection</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetProtocolTypeSelection()
     * @generated
     */
    int ETHERNET_PROTOCOL_TYPE_SELECTION = 121;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum <em>Rx Tx Flag Type Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getRxTxFlagTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 122;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ObserverValueType <em>Observer Value Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ObserverValueType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getObserverValueType()
     * @generated
     */
    int OBSERVER_VALUE_TYPE = 123;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum <em>Plugin State Value Type Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginStateValueTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 124;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum <em>Analysis Type Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAnalysisTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 125;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum <em>Some IP Message Type Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMessageTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 126;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum <em>Some IP Method Type Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMethodTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 127;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum <em>Some IP Return Code Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPReturnCodeOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM = 128;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum <em>Some IPSD Flags Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDFlagsOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 129;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum <em>Some IPSD Type Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 130;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum <em>Stream Analysis Flags Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStreamAnalysisFlagsOrTemplatePlaceholderEnum()
     * @generated
     */
    int STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 131;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum <em>Some IPSD Entry Flags Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum()
     * @generated
     */
    int SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = 132;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum <em>Can Ext Identifier Or Template
     * Placeholder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCanExtIdentifierOrTemplatePlaceholderEnum()
     * @generated
     */
    int CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM = 133;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DataType <em>Data Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DataType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDataType()
     * @generated
     */
    int DATA_TYPE = 134;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageType <em>DLT Message Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DLT_MessageType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageType()
     * @generated
     */
    int DLT_MESSAGE_TYPE = 135;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo <em>DLT Message Trace Info</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageTraceInfo()
     * @generated
     */
    int DLT_MESSAGE_TRACE_INFO = 136;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageLogInfo <em>DLT Message Log Info</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DLT_MessageLogInfo
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageLogInfo()
     * @generated
     */
    int DLT_MESSAGE_LOG_INFO = 137;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageControlInfo <em>DLT Message Control Info</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DLT_MessageControlInfo
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageControlInfo()
     * @generated
     */
    int DLT_MESSAGE_CONTROL_INFO = 138;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageBusInfo <em>DLT Message Bus Info</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.DLT_MessageBusInfo
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageBusInfo()
     * @generated
     */
    int DLT_MESSAGE_BUS_INFO = 139;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate <em>For Each Expression Modifier Template</em>}'
     * enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getForEachExpressionModifierTemplate()
     * @generated
     */
    int FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE = 140;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.AUTOSARDataType <em>AUTOSAR Data Type</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.AUTOSARDataType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAUTOSARDataType()
     * @generated
     */
    int AUTOSAR_DATA_TYPE = 141;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum <em>CAN Frame Type Or Template Placeholder
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANFrameTypeOrTemplatePlaceholderEnum()
     * @generated
     */
    int CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = 142;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum <em>Evaluation Behaviour Or Template Defined
     * Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEvaluationBehaviourOrTemplateDefinedEnum()
     * @generated
     */
    int EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM = 143;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.SignalVariableLagEnum <em>Signal Variable Lag Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.SignalVariableLagEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalVariableLagEnum()
     * @generated
     */
    int SIGNAL_VARIABLE_LAG_ENUM = 144;
    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum <em>TTL Or Template Place Holder Enum</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTTLOrTemplatePlaceHolderEnum()
     * @generated
     */
    int TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM = 145;
    /**
     * The meta object id for the '<em>Check Type Object</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCheckTypeObject()
     * @generated
     */
    int CHECK_TYPE_OBJECT = 146;
    /**
     * The meta object id for the '<em>Comparator Object</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.Comparator
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparatorObject()
     * @generated
     */
    int COMPARATOR_OBJECT = 147;
    /**
     * The meta object id for the '<em>Logical Operator Type Object</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalOperatorTypeObject()
     * @generated
     */
    int LOGICAL_OPERATOR_TYPE_OBJECT = 148;
    /**
     * The meta object id for the '<em>Hex Or Int Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHexOrIntOrTemplatePlaceholder()
     * @generated
     */
    int HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER = 149;
    /**
     * The meta object id for the '<em>Int Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIntOrTemplatePlaceholder()
     * @generated
     */
    int INT_OR_TEMPLATE_PLACEHOLDER = 150;
    /**
     * The meta object id for the '<em>Long Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLongOrTemplatePlaceholder()
     * @generated
     */
    int LONG_OR_TEMPLATE_PLACEHOLDER = 151;
    /**
     * The meta object id for the '<em>Double Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleOrTemplatePlaceholder()
     * @generated
     */
    int DOUBLE_OR_TEMPLATE_PLACEHOLDER = 152;
    /**
     * The meta object id for the '<em>Double Or Hex Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleOrHexOrTemplatePlaceholder()
     * @generated
     */
    int DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER = 153;
    /**
     * The meta object id for the '<em>IP Pattern Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPPatternOrTemplatePlaceholder()
     * @generated
     */
    int IP_PATTERN_OR_TEMPLATE_PLACEHOLDER = 154;
    /**
     * The meta object id for the '<em>MAC Pattern Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMACPatternOrTemplatePlaceholder()
     * @generated
     */
    int MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER = 155;
    /**
     * The meta object id for the '<em>Port Pattern Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPortPatternOrTemplatePlaceholder()
     * @generated
     */
    int PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER = 156;
    /**
     * The meta object id for the '<em>Vlan Id Pattern Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVlanIdPatternOrTemplatePlaceholder()
     * @generated
     */
    int VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER = 157;
    /**
     * The meta object id for the '<em>IP Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPOrTemplatePlaceholder()
     * @generated
     */
    int IP_OR_TEMPLATE_PLACEHOLDER = 158;
    /**
     * The meta object id for the '<em>Port Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPortOrTemplatePlaceholder()
     * @generated
     */
    int PORT_OR_TEMPLATE_PLACEHOLDER = 159;
    /**
     * The meta object id for the '<em>Vlan Id Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVlanIdOrTemplatePlaceholder()
     * @generated
     */
    int VLAN_ID_OR_TEMPLATE_PLACEHOLDER = 160;
    /**
     * The meta object id for the '<em>MAC Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMACOrTemplatePlaceholder()
     * @generated
     */
    int MAC_OR_TEMPLATE_PLACEHOLDER = 161;
    /**
     * The meta object id for the '<em>Bitmask Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBitmaskOrTemplatePlaceholder()
     * @generated
     */
    int BITMASK_OR_TEMPLATE_PLACEHOLDER = 162;
    /**
     * The meta object id for the '<em>Hex Or Int Or Null Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHexOrIntOrNullOrTemplatePlaceholder()
     * @generated
     */
    int HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER = 163;
    /**
     * The meta object id for the '<em>Non Zero Double Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonZeroDoubleOrTemplatePlaceholder()
     * @generated
     */
    int NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER = 164;
    /**
     * The meta object id for the '<em>Val Var Initial Value Double Or Template Placeholder Or String</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValVarInitialValueDoubleOrTemplatePlaceholderOrString()
     * @generated
     */
    int VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING = 165;
    /**
     * The meta object id for the '<em>Byte Or Template Placeholder</em>' data type.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see java.lang.String
     * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getByteOrTemplatePlaceholder()
     * @generated
     */
    int BYTE_OR_TEMPLATE_PLACEHOLDER = 166;

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DocumentRoot <em>Document Root</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Document Root</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot
     * @generated
     */
    EClass getDocumentRoot();

    /**
     * Returns the meta object for the attribute list '{@link de.bmw.smard.modeller.conditions.DocumentRoot#getMixed <em>Mixed</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Mixed</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot#getMixed()
     * @see #getDocumentRoot()
     * @generated
     */
    EAttribute getDocumentRoot_Mixed();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.conditions.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot#getXMLNSPrefixMap()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XMLNSPrefixMap();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.conditions.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XSI Schema Location</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot#getXSISchemaLocation()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XSISchemaLocation();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.DocumentRoot#getConditionSet <em>Condition Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Condition Set</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot#getConditionSet()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_ConditionSet();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.DocumentRoot#getConditionsDocument <em>Conditions
     * Document</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Conditions Document</em>'.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot#getConditionsDocument()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_ConditionsDocument();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ConditionSet <em>Condition Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Condition Set</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet
     * @generated
     */
    EClass getConditionSet();

    /**
     * Returns the meta object for the attribute list '{@link de.bmw.smard.modeller.conditions.ConditionSet#getGroup <em>Group</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Group</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getGroup()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Group();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getBaureihe <em>Baureihe</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Baureihe</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getBaureihe()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Baureihe();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getDescription()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getIstufe <em>Istufe</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Istufe</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getIstufe()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Istufe();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Schemaversion</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getSchemaversion()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Schemaversion();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getUuid <em>Uuid</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Uuid</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getUuid()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Uuid();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getVersion <em>Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Version</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getVersion()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Version();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.ConditionSet#getConditions <em>Conditions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Conditions</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getConditions()
     * @see #getConditionSet()
     * @generated
     */
    EReference getConditionSet_Conditions();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConditionSet#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionSet#getName()
     * @see #getConditionSet()
     * @generated
     */
    EAttribute getConditionSet_Name();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Condition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Condition</em>'.
     * @see de.bmw.smard.modeller.conditions.Condition
     * @generated
     */
    EClass getCondition();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Condition#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.Condition#getName()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Name();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Condition#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.Condition#getDescription()
     * @see #getCondition()
     * @generated
     */
    EAttribute getCondition_Description();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.Condition#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.Condition#getExpression()
     * @see #getCondition()
     * @generated
     */
    EReference getCondition_Expression();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SignalComparisonExpression <em>Signal Comparison Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Signal Comparison Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalComparisonExpression
     * @generated
     */
    EClass getSignalComparisonExpression();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.SignalComparisonExpression#getComparatorSignal
     * <em>Comparator Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Comparator Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalComparisonExpression#getComparatorSignal()
     * @see #getSignalComparisonExpression()
     * @generated
     */
    EReference getSignalComparisonExpression_ComparatorSignal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SignalComparisonExpression#getOperator <em>Operator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Operator</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalComparisonExpression#getOperator()
     * @see #getSignalComparisonExpression()
     * @generated
     */
    EAttribute getSignalComparisonExpression_Operator();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SignalComparisonExpression#getOperatorTmplParam <em>Operator Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Operator Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalComparisonExpression#getOperatorTmplParam()
     * @see #getSignalComparisonExpression()
     * @generated
     */
    EAttribute getSignalComparisonExpression_OperatorTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression <em>Can Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Can Message Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression
     * @generated
     */
    EClass getCanMessageCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Busid</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_Busid();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getType()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_Type();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getMessageIDs <em>Message IDs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message IDs</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getMessageIDs()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_MessageIDs();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getTypeTmplParam()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_TypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Busid Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusidTmplParam()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_BusidTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlag <em>Rxtx Flag</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Rxtx Flag</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlag()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_RxtxFlag();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam <em>Rxtx Flag Type
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Rxtx Flag Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_RxtxFlagTypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifier <em>Ext Identifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ext Identifier</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifier()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_ExtIdentifier();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifierTmplParam <em>Ext Identifier
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ext Identifier Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifierTmplParam()
     * @see #getCanMessageCheckExpression()
     * @generated
     */
    EAttribute getCanMessageCheckExpression_ExtIdentifierTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression <em>Lin Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Lin Message Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression
     * @generated
     */
    EClass getLinMessageCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Busid</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid()
     * @see #getLinMessageCheckExpression()
     * @generated
     */
    EAttribute getLinMessageCheckExpression_Busid();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getType()
     * @see #getLinMessageCheckExpression()
     * @generated
     */
    EAttribute getLinMessageCheckExpression_Type();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getMessageIDs <em>Message IDs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message IDs</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getMessageIDs()
     * @see #getLinMessageCheckExpression()
     * @generated
     */
    EAttribute getLinMessageCheckExpression_MessageIDs();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getTypeTmplParam()
     * @see #getLinMessageCheckExpression()
     * @generated
     */
    EAttribute getLinMessageCheckExpression_TypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Busid Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusidTmplParam()
     * @see #getLinMessageCheckExpression()
     * @generated
     */
    EAttribute getLinMessageCheckExpression_BusidTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StateCheckExpression <em>State Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>State Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.StateCheckExpression
     * @generated
     */
    EClass getStateCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActive <em>Check State
     * Active</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Check State Active</em>'.
     * @see de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActive()
     * @see #getStateCheckExpression()
     * @generated
     */
    EAttribute getStateCheckExpression_CheckStateActive();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckState <em>Check State</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Check State</em>'.
     * @see de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckState()
     * @see #getStateCheckExpression()
     * @generated
     */
    EReference getStateCheckExpression_CheckState();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActiveTmplParam <em>Check State
     * Active Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Check State Active Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.StateCheckExpression#getCheckStateActiveTmplParam()
     * @see #getStateCheckExpression()
     * @generated
     */
    EAttribute getStateCheckExpression_CheckStateActiveTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TimingExpression <em>Timing Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Timing Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.TimingExpression
     * @generated
     */
    EClass getTimingExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime <em>Maxtime</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Maxtime</em>'.
     * @see de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime()
     * @see #getTimingExpression()
     * @generated
     */
    EAttribute getTimingExpression_Maxtime();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMintime <em>Mintime</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Mintime</em>'.
     * @see de.bmw.smard.modeller.conditions.TimingExpression#getMintime()
     * @see #getTimingExpression()
     * @generated
     */
    EAttribute getTimingExpression_Mintime();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TrueExpression <em>True Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>True Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.TrueExpression
     * @generated
     */
    EClass getTrueExpression();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.LogicalExpression <em>Logical Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Logical Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalExpression
     * @generated
     */
    EClass getLogicalExpression();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.LogicalExpression#getExpressions
     * <em>Expressions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Expressions</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalExpression#getExpressions()
     * @see #getLogicalExpression()
     * @generated
     */
    EReference getLogicalExpression_Expressions();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperator <em>Operator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Operator</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalExpression#getOperator()
     * @see #getLogicalExpression()
     * @generated
     */
    EAttribute getLogicalExpression_Operator();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Operator Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalExpression#getOperatorTmplParam()
     * @see #getLogicalExpression()
     * @generated
     */
    EAttribute getLogicalExpression_OperatorTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ReferenceConditionExpression <em>Reference Condition Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Reference Condition Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.ReferenceConditionExpression
     * @generated
     */
    EClass getReferenceConditionExpression();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.ReferenceConditionExpression#getCondition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Condition</em>'.
     * @see de.bmw.smard.modeller.conditions.ReferenceConditionExpression#getCondition()
     * @see #getReferenceConditionExpression()
     * @generated
     */
    EReference getReferenceConditionExpression_Condition();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression <em>Transition Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Transition Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.TransitionCheckExpression
     * @generated
     */
    EClass getTransitionCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActive <em>Stay Active</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Stay Active</em>'.
     * @see de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActive()
     * @see #getTransitionCheckExpression()
     * @generated
     */
    EAttribute getTransitionCheckExpression_StayActive();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getCheckTransition <em>Check
     * Transition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Check Transition</em>'.
     * @see de.bmw.smard.modeller.conditions.TransitionCheckExpression#getCheckTransition()
     * @see #getTransitionCheckExpression()
     * @generated
     */
    EReference getTransitionCheckExpression_CheckTransition();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActiveTmplParam <em>Stay Active Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Stay Active Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.TransitionCheckExpression#getStayActiveTmplParam()
     * @see #getTransitionCheckExpression()
     * @generated
     */
    EAttribute getTransitionCheckExpression_StayActiveTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression <em>Flex Ray Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Flex Ray Message Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression
     * @generated
     */
    EClass getFlexRayMessageCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getBusId <em>Bus Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Bus Id</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getBusId()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_BusId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getPayloadPreamble <em>Payload
     * Preamble</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Payload Preamble</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getPayloadPreamble()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_PayloadPreamble();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getZeroFrame <em>Zero Frame</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Zero Frame</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getZeroFrame()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_ZeroFrame();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getSyncFrame <em>Sync Frame</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sync Frame</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getSyncFrame()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_SyncFrame();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getStartupFrame <em>Startup
     * Frame</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Startup Frame</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getStartupFrame()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_StartupFrame();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getNetworkMgmt <em>Network Mgmt</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Network Mgmt</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getNetworkMgmt()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_NetworkMgmt();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getFlexrayMessageId <em>Flexray Message
     * Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Flexray Message Id</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getFlexrayMessageId()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_FlexrayMessageId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam <em>Payload
     * Preamble Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Payload Preamble Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_PayloadPreambleTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam <em>Zero Frame
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Zero Frame Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_ZeroFrameTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam <em>Sync Frame
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sync Frame Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_SyncFrameTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam <em>Startup
     * Frame Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Startup Frame Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_StartupFrameTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam <em>Network Mgmt
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Network Mgmt Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_NetworkMgmtTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Bus Id Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getBusIdTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_BusIdTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getType <em>Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getType()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_Type();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getTypeTmplParam <em>Type Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression#getTypeTmplParam()
     * @see #getFlexRayMessageCheckExpression()
     * @generated
     */
    EAttribute getFlexRayMessageCheckExpression_TypeTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StopExpression <em>Stop Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Stop Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.StopExpression
     * @generated
     */
    EClass getStopExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Analyse Art</em>'.
     * @see de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArt()
     * @see #getStopExpression()
     * @generated
     */
    EAttribute getStopExpression_AnalyseArt();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Analyse Art Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArtTmplParam()
     * @see #getStopExpression()
     * @generated
     */
    EAttribute getStopExpression_AnalyseArtTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ComparatorSignal <em>Comparator Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Comparator Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.ComparatorSignal
     * @generated
     */
    EClass getComparatorSignal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ComparatorSignal#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.ComparatorSignal#getDescription()
     * @see #getComparatorSignal()
     * @generated
     */
    EAttribute getComparatorSignal_Description();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ConstantComparatorValue <em>Constant Comparator Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Constant Comparator Value</em>'.
     * @see de.bmw.smard.modeller.conditions.ConstantComparatorValue
     * @generated
     */
    EClass getConstantComparatorValue();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConstantComparatorValue#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see de.bmw.smard.modeller.conditions.ConstantComparatorValue#getValue()
     * @see #getConstantComparatorValue()
     * @generated
     */
    EAttribute getConstantComparatorValue_Value();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConstantComparatorValue#getInterpretedValue <em>Interpreted
     * Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Interpreted Value</em>'.
     * @see de.bmw.smard.modeller.conditions.ConstantComparatorValue#getInterpretedValue()
     * @see #getConstantComparatorValue()
     * @generated
     */
    EAttribute getConstantComparatorValue_InterpretedValue();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ConstantComparatorValue#getInterpretedValueTmplParam <em>Interpreted
     * Value Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Interpreted Value Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.ConstantComparatorValue#getInterpretedValueTmplParam()
     * @see #getConstantComparatorValue()
     * @generated
     */
    EAttribute getConstantComparatorValue_InterpretedValueTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.CalculationExpression <em>Calculation Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Calculation Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.CalculationExpression
     * @generated
     */
    EClass getCalculationExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CalculationExpression#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.CalculationExpression#getExpression()
     * @see #getCalculationExpression()
     * @generated
     */
    EAttribute getCalculationExpression_Expression();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.CalculationExpression#getOperands
     * <em>Operands</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Operands</em>'.
     * @see de.bmw.smard.modeller.conditions.CalculationExpression#getOperands()
     * @see #getCalculationExpression()
     * @generated
     */
    EReference getCalculationExpression_Operands();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Expression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.Expression
     * @generated
     */
    EClass getExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Expression#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.Expression#getDescription()
     * @see #getExpression()
     * @generated
     */
    EAttribute getExpression_Description();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VariableReference <em>Variable Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Variable Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableReference
     * @generated
     */
    EClass getVariableReference();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.VariableReference#getVariable <em>Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableReference#getVariable()
     * @see #getVariableReference()
     * @generated
     */
    EReference getVariableReference_Variable();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ConditionsDocument <em>Document</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Document</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionsDocument
     * @generated
     */
    EClass getConditionsDocument();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getSignalReferencesSet <em>Signal
     * References Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Signal References Set</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionsDocument#getSignalReferencesSet()
     * @see #getConditionsDocument()
     * @generated
     */
    EReference getConditionsDocument_SignalReferencesSet();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getVariableSet <em>Variable Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Variable Set</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionsDocument#getVariableSet()
     * @see #getConditionsDocument()
     * @generated
     */
    EReference getConditionsDocument_VariableSet();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getConditionSet <em>Condition
     * Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Condition Set</em>'.
     * @see de.bmw.smard.modeller.conditions.ConditionsDocument#getConditionSet()
     * @see #getConditionsDocument()
     * @generated
     */
    EReference getConditionsDocument_ConditionSet();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VariableSet <em>Variable Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Variable Set</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableSet
     * @generated
     */
    EClass getVariableSet();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.VariableSet#getUuid <em>Uuid</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Uuid</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableSet#getUuid()
     * @see #getVariableSet()
     * @generated
     */
    EAttribute getVariableSet_Uuid();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.VariableSet#getVariables <em>Variables</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Variables</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableSet#getVariables()
     * @see #getVariableSet()
     * @generated
     */
    EReference getVariableSet_Variables();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Variable <em>Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.Variable
     * @generated
     */
    EClass getVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Variable#getDataType <em>Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.Variable#getDataType()
     * @see #getVariable()
     * @generated
     */
    EAttribute getVariable_DataType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Variable#getUnit <em>Unit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Unit</em>'.
     * @see de.bmw.smard.modeller.conditions.Variable#getUnit()
     * @see #getVariable()
     * @generated
     */
    EAttribute getVariable_Unit();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormat <em>Variable Format</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Variable Format</em>'.
     * @see de.bmw.smard.modeller.conditions.Variable#getVariableFormat()
     * @see #getVariable()
     * @generated
     */
    EReference getVariable_VariableFormat();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormatTmplParam <em>Variable Format Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Variable Format Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.Variable#getVariableFormatTmplParam()
     * @see #getVariable()
     * @generated
     */
    EAttribute getVariable_VariableFormatTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SignalVariable <em>Signal Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Signal Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable
     * @generated
     */
    EClass getSignalVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Interpreted Value</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue()
     * @see #getSignalVariable()
     * @generated
     */
    EAttribute getSignalVariable_InterpretedValue();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.SignalVariable#getSignal <em>Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable#getSignal()
     * @see #getSignalVariable()
     * @generated
     */
    EReference getSignalVariable_Signal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValueTmplParam <em>Interpreted Value Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Interpreted Value Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValueTmplParam()
     * @see #getSignalVariable()
     * @generated
     */
    EAttribute getSignalVariable_InterpretedValueTmplParam();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.SignalVariable#getSignalObservers <em>Signal
     * Observers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Signal Observers</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable#getSignalObservers()
     * @see #getSignalVariable()
     * @generated
     */
    EReference getSignalVariable_SignalObservers();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SignalVariable#getLag <em>Lag</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Lag</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariable#getLag()
     * @see #getSignalVariable()
     * @generated
     */
    EAttribute getSignalVariable_Lag();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ValueVariable <em>Value Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Value Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.ValueVariable
     * @generated
     */
    EClass getValueVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue <em>Initial Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Initial Value</em>'.
     * @see de.bmw.smard.modeller.conditions.ValueVariable#getInitialValue()
     * @see #getValueVariable()
     * @generated
     */
    EAttribute getValueVariable_InitialValue();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.ValueVariable#getValueVariableObservers <em>Value
     * Variable Observers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Value Variable Observers</em>'.
     * @see de.bmw.smard.modeller.conditions.ValueVariable#getValueVariableObservers()
     * @see #getValueVariable()
     * @generated
     */
    EReference getValueVariable_ValueVariableObservers();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VariableFormat <em>Variable Format</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Variable Format</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableFormat
     * @generated
     */
    EClass getVariableFormat();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.VariableFormat#getDigits <em>Digits</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Digits</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableFormat#getDigits()
     * @see #getVariableFormat()
     * @generated
     */
    EAttribute getVariableFormat_Digits();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.VariableFormat#getBaseDataType <em>Base Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Base Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableFormat#getBaseDataType()
     * @see #getVariableFormat()
     * @generated
     */
    EAttribute getVariableFormat_BaseDataType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.VariableFormat#isUpperCase <em>Upper Case</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Upper Case</em>'.
     * @see de.bmw.smard.modeller.conditions.VariableFormat#isUpperCase()
     * @see #getVariableFormat()
     * @generated
     */
    EAttribute getVariableFormat_UpperCase();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractObserver <em>Abstract Observer</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Observer</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver
     * @generated
     */
    EClass getAbstractObserver();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractObserver#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#getName()
     * @see #getAbstractObserver()
     * @generated
     */
    EAttribute getAbstractObserver_Name();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractObserver#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#getDescription()
     * @see #getAbstractObserver()
     * @generated
     */
    EAttribute getAbstractObserver_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractObserver#getLogLevel <em>Log Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Log Level</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#getLogLevel()
     * @see #getAbstractObserver()
     * @generated
     */
    EAttribute getAbstractObserver_LogLevel();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.AbstractObserver#getValueRanges <em>Value
     * Ranges</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Value Ranges</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#getValueRanges()
     * @see #getAbstractObserver()
     * @generated
     */
    EReference getAbstractObserver_ValueRanges();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractObserver#getValueRangesTmplParam <em>Value Ranges Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value Ranges Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#getValueRangesTmplParam()
     * @see #getAbstractObserver()
     * @generated
     */
    EAttribute getAbstractObserver_ValueRangesTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractObserver#isActiveAtStart <em>Active At Start</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Active At Start</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver#isActiveAtStart()
     * @see #getAbstractObserver()
     * @generated
     */
    EAttribute getAbstractObserver_ActiveAtStart();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ObserverValueRange <em>Observer Value Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Observer Value Range</em>'.
     * @see de.bmw.smard.modeller.conditions.ObserverValueRange
     * @generated
     */
    EClass getObserverValueRange();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see de.bmw.smard.modeller.conditions.ObserverValueRange#getValue()
     * @see #getObserverValueRange()
     * @generated
     */
    EAttribute getObserverValueRange_Value();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.ObserverValueRange#getDescription()
     * @see #getObserverValueRange()
     * @generated
     */
    EAttribute getObserverValueRange_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ObserverValueRange#getValueType <em>Value Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value Type</em>'.
     * @see de.bmw.smard.modeller.conditions.ObserverValueRange#getValueType()
     * @see #getObserverValueRange()
     * @generated
     */
    EAttribute getObserverValueRange_ValueType();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SignalObserver <em>Signal Observer</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Signal Observer</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalObserver
     * @generated
     */
    EClass getSignalObserver();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SignalReferenceSet <em>Signal Reference Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Signal Reference Set</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalReferenceSet
     * @generated
     */
    EClass getSignalReferenceSet();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.SignalReferenceSet#getMessages <em>Messages</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Messages</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalReferenceSet#getMessages()
     * @see #getSignalReferenceSet()
     * @generated
     */
    EReference getSignalReferenceSet_Messages();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SignalReference <em>Signal Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Signal Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalReference
     * @generated
     */
    EClass getSignalReference();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.SignalReference#getSignal <em>Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalReference#getSignal()
     * @see #getSignalReference()
     * @generated
     */
    EReference getSignalReference_Signal();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ISignalOrReference <em>ISignal Or Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>ISignal Or Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.ISignalOrReference
     * @generated
     */
    EClass getISignalOrReference();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.BitPatternComparatorValue <em>Bit Pattern Comparator Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Bit Pattern Comparator Value</em>'.
     * @see de.bmw.smard.modeller.conditions.BitPatternComparatorValue
     * @generated
     */
    EClass getBitPatternComparatorValue();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.BitPatternComparatorValue#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see de.bmw.smard.modeller.conditions.BitPatternComparatorValue#getValue()
     * @see #getBitPatternComparatorValue()
     * @generated
     */
    EAttribute getBitPatternComparatorValue_Value();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Startbit</em>'.
     * @see de.bmw.smard.modeller.conditions.BitPatternComparatorValue#getStartbit()
     * @see #getBitPatternComparatorValue()
     * @generated
     */
    EAttribute getBitPatternComparatorValue_Startbit();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.NotExpression <em>Not Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Not Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.NotExpression
     * @generated
     */
    EClass getNotExpression();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.NotExpression#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.NotExpression#getExpression()
     * @see #getNotExpression()
     * @generated
     */
    EReference getNotExpression_Expression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.NotExpression#getOperator <em>Operator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Operator</em>'.
     * @see de.bmw.smard.modeller.conditions.NotExpression#getOperator()
     * @see #getNotExpression()
     * @generated
     */
    EAttribute getNotExpression_Operator();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IOperand <em>IOperand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IOperand</em>'.
     * @see de.bmw.smard.modeller.conditions.IOperand
     * @generated
     */
    EClass getIOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IComputeVariableActionOperand <em>ICompute Variable Action Operand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>ICompute Variable Action Operand</em>'.
     * @see de.bmw.smard.modeller.conditions.IComputeVariableActionOperand
     * @generated
     */
    EClass getIComputeVariableActionOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IStringOperand <em>IString Operand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IString Operand</em>'.
     * @see de.bmw.smard.modeller.conditions.IStringOperand
     * @generated
     */
    EClass getIStringOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison Expression
     * Operand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>ISignal Comparison Expression Operand</em>'.
     * @see de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand
     * @generated
     */
    EClass getISignalComparisonExpressionOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IOperation <em>IOperation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IOperation</em>'.
     * @see de.bmw.smard.modeller.conditions.IOperation
     * @generated
     */
    EClass getIOperation();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IStringOperation <em>IString Operation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IString Operation</em>'.
     * @see de.bmw.smard.modeller.conditions.IStringOperation
     * @generated
     */
    EClass getIStringOperation();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.INumericOperand <em>INumeric Operand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>INumeric Operand</em>'.
     * @see de.bmw.smard.modeller.conditions.INumericOperand
     * @generated
     */
    EClass getINumericOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.INumericOperation <em>INumeric Operation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>INumeric Operation</em>'.
     * @see de.bmw.smard.modeller.conditions.INumericOperation
     * @generated
     */
    EClass getINumericOperation();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ValueVariableObserver <em>Value Variable Observer</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Value Variable Observer</em>'.
     * @see de.bmw.smard.modeller.conditions.ValueVariableObserver
     * @generated
     */
    EClass getValueVariableObserver();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ParseStringToDouble <em>Parse String To Double</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Parse String To Double</em>'.
     * @see de.bmw.smard.modeller.conditions.ParseStringToDouble
     * @generated
     */
    EClass getParseStringToDouble();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.ParseStringToDouble#getStringToParse <em>String To
     * Parse</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>String To Parse</em>'.
     * @see de.bmw.smard.modeller.conditions.ParseStringToDouble#getStringToParse()
     * @see #getParseStringToDouble()
     * @generated
     */
    EReference getParseStringToDouble_StringToParse();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StringExpression <em>String Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>String Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.StringExpression
     * @generated
     */
    EClass getStringExpression();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Matches <em>Matches</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Matches</em>'.
     * @see de.bmw.smard.modeller.conditions.Matches
     * @generated
     */
    EClass getMatches();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.Matches#getStringToCheck <em>String To Check</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>String To Check</em>'.
     * @see de.bmw.smard.modeller.conditions.Matches#getStringToCheck()
     * @see #getMatches()
     * @generated
     */
    EReference getMatches_StringToCheck();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Extract <em>Extract</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Extract</em>'.
     * @see de.bmw.smard.modeller.conditions.Extract
     * @generated
     */
    EClass getExtract();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.Extract#getStringToExtract <em>String To Extract</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>String To Extract</em>'.
     * @see de.bmw.smard.modeller.conditions.Extract#getStringToExtract()
     * @see #getExtract()
     * @generated
     */
    EReference getExtract_StringToExtract();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Extract#getGroupIndex <em>Group Index</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Group Index</em>'.
     * @see de.bmw.smard.modeller.conditions.Extract#getGroupIndex()
     * @see #getExtract()
     * @generated
     */
    EAttribute getExtract_GroupIndex();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.Substring <em>Substring</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Substring</em>'.
     * @see de.bmw.smard.modeller.conditions.Substring
     * @generated
     */
    EClass getSubstring();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.Substring#getStringToExtract <em>String To Extract</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>String To Extract</em>'.
     * @see de.bmw.smard.modeller.conditions.Substring#getStringToExtract()
     * @see #getSubstring()
     * @generated
     */
    EReference getSubstring_StringToExtract();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Substring#getStartIndex <em>Start Index</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Start Index</em>'.
     * @see de.bmw.smard.modeller.conditions.Substring#getStartIndex()
     * @see #getSubstring()
     * @generated
     */
    EAttribute getSubstring_StartIndex();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.Substring#getEndIndex <em>End Index</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>End Index</em>'.
     * @see de.bmw.smard.modeller.conditions.Substring#getEndIndex()
     * @see #getSubstring()
     * @generated
     */
    EAttribute getSubstring_EndIndex();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ParseNumericToString <em>Parse Numeric To String</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Parse Numeric To String</em>'.
     * @see de.bmw.smard.modeller.conditions.ParseNumericToString
     * @generated
     */
    EClass getParseNumericToString();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.ParseNumericToString#getNumericOperandToBeParsed
     * <em>Numeric Operand To Be Parsed</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Numeric Operand To Be Parsed</em>'.
     * @see de.bmw.smard.modeller.conditions.ParseNumericToString#getNumericOperandToBeParsed()
     * @see #getParseNumericToString()
     * @generated
     */
    EReference getParseNumericToString_NumericOperandToBeParsed();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StringLength <em>String Length</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>String Length</em>'.
     * @see de.bmw.smard.modeller.conditions.StringLength
     * @generated
     */
    EClass getStringLength();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.StringLength#getStringOperand <em>String Operand</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>String Operand</em>'.
     * @see de.bmw.smard.modeller.conditions.StringLength#getStringOperand()
     * @see #getStringLength()
     * @generated
     */
    EReference getStringLength_StringOperand();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractMessage <em>Abstract Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Message</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractMessage
     * @generated
     */
    EClass getAbstractMessage();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.AbstractMessage#getSignals <em>Signals</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Signals</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractMessage#getSignals()
     * @see #getAbstractMessage()
     * @generated
     */
    EReference getAbstractMessage_Signals();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractMessage#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractMessage#getName()
     * @see #getAbstractMessage()
     * @generated
     */
    EAttribute getAbstractMessage_Name();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SomeIPMessage <em>Some IP Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Some IP Message</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPMessage
     * @generated
     */
    EClass getSomeIPMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Some IP Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPMessage#getSomeIPFilter()
     * @see #getSomeIPMessage()
     * @generated
     */
    EReference getSomeIPMessage_SomeIPFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractSignal <em>Abstract Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractSignal
     * @generated
     */
    EClass getAbstractSignal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractSignal#getName()
     * @see #getAbstractSignal()
     * @generated
     */
    EAttribute getAbstractSignal_Name();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getDecodeStrategy <em>Decode
     * Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Decode Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractSignal#getDecodeStrategy()
     * @see #getAbstractSignal()
     * @generated
     */
    EReference getAbstractSignal_DecodeStrategy();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.AbstractSignal#getExtractStrategy <em>Extract
     * Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractSignal#getExtractStrategy()
     * @see #getAbstractSignal()
     * @generated
     */
    EReference getAbstractSignal_ExtractStrategy();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ContainerSignal <em>Container Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Container Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.ContainerSignal
     * @generated
     */
    EClass getContainerSignal();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.ContainerSignal#getContainedSignals <em>Contained
     * Signals</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Contained Signals</em>'.
     * @see de.bmw.smard.modeller.conditions.ContainerSignal#getContainedSignals()
     * @see #getContainerSignal()
     * @generated
     */
    EReference getContainerSignal_ContainedSignals();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractFilter <em>Abstract Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractFilter
     * @generated
     */
    EClass getAbstractFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractFilter#getPayloadLength <em>Payload Length</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Payload Length</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractFilter#getPayloadLength()
     * @see #getAbstractFilter()
     * @generated
     */
    EAttribute getAbstractFilter_PayloadLength();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.EthernetFilter <em>Ethernet Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Ethernet Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter
     * @generated
     */
    EClass getEthernetFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getSourceMAC <em>Source MAC</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Source MAC</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getSourceMAC()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_SourceMAC();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getDestMAC <em>Dest MAC</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Dest MAC</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getDestMAC()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_DestMAC();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getEtherType <em>Ether Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ether Type</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getEtherType()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_EtherType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanId <em>Inner Vlan Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Inner Vlan Id</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanId()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_InnerVlanId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanId <em>Outer Vlan Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Outer Vlan Id</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanId()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_OuterVlanId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanCFI <em>Inner Vlan CFI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Inner Vlan CFI</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanCFI()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_InnerVlanCFI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanCFI <em>Outer Vlan CFI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Outer Vlan CFI</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanCFI()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_OuterVlanCFI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanVID <em>Inner Vlan VID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Inner Vlan VID</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanVID()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_InnerVlanVID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanVID <em>Outer Vlan VID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Outer Vlan VID</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanVID()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_OuterVlanVID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getCRC <em>CRC</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>CRC</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getCRC()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_CRC();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanTPID <em>Inner Vlan TPID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Inner Vlan TPID</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getInnerVlanTPID()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_InnerVlanTPID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanTPID <em>Outer Vlan TPID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Outer Vlan TPID</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter#getOuterVlanTPID()
     * @see #getEthernetFilter()
     * @generated
     */
    EAttribute getEthernetFilter_OuterVlanTPID();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UDPFilter <em>UDP Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>UDP Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPFilter
     * @generated
     */
    EClass getUDPFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UDPFilter#getChecksum <em>Checksum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Checksum</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPFilter#getChecksum()
     * @see #getUDPFilter()
     * @generated
     */
    EAttribute getUDPFilter_Checksum();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TCPFilter <em>TCP Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>TCP Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter
     * @generated
     */
    EClass getTCPFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getSequenceNumber <em>Sequence Number</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sequence Number</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getSequenceNumber()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_SequenceNumber();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getAcknowledgementNumber <em>Acknowledgement Number</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Acknowledgement Number</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getAcknowledgementNumber()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_AcknowledgementNumber();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getTcpFlags <em>Tcp Flags</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Tcp Flags</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getTcpFlags()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_TcpFlags();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Stream Analysis Flags</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlags()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_StreamAnalysisFlags();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Stream Analysis Flags Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlagsTmplParam()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_StreamAnalysisFlagsTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamValidPayloadOffset <em>Stream Valid Payload
     * Offset</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Stream Valid Payload Offset</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPFilter#getStreamValidPayloadOffset()
     * @see #getTCPFilter()
     * @generated
     */
    EAttribute getTCPFilter_StreamValidPayloadOffset();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IPv4Filter <em>IPv4 Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IPv4 Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter
     * @generated
     */
    EClass getIPv4Filter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.IPv4Filter#getProtocolType <em>Protocol Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Protocol Type</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter#getProtocolType()
     * @see #getIPv4Filter()
     * @generated
     */
    EAttribute getIPv4Filter_ProtocolType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.IPv4Filter#getProtocolTypeTmplParam <em>Protocol Type Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Protocol Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter#getProtocolTypeTmplParam()
     * @see #getIPv4Filter()
     * @generated
     */
    EAttribute getIPv4Filter_ProtocolTypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.IPv4Filter#getSourceIP <em>Source IP</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Source IP</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter#getSourceIP()
     * @see #getIPv4Filter()
     * @generated
     */
    EAttribute getIPv4Filter_SourceIP();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.IPv4Filter#getDestIP <em>Dest IP</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Dest IP</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter#getDestIP()
     * @see #getIPv4Filter()
     * @generated
     */
    EAttribute getIPv4Filter_DestIP();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.IPv4Filter#getTimeToLive <em>Time To Live</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Time To Live</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter#getTimeToLive()
     * @see #getIPv4Filter()
     * @generated
     */
    EAttribute getIPv4Filter_TimeToLive();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SomeIPFilter <em>Some IP Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Some IP Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter
     * @generated
     */
    EClass getSomeIPFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getServiceId <em>Service Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getServiceId()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_ServiceId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodType <em>Method Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Method Type</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodType()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_MethodType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodTypeTmplParam <em>Method Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Method Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodTypeTmplParam()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_MethodTypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodId <em>Method Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Method Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getMethodId()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_MethodId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getClientId <em>Client Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Client Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getClientId()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_ClientId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getSessionId <em>Session Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Session Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getSessionId()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_SessionId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getProtocolVersion <em>Protocol Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Protocol Version</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getProtocolVersion()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_ProtocolVersion();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getInterfaceVersion <em>Interface Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Interface Version</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getInterfaceVersion()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_InterfaceVersion();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getMessageType <em>Message Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Type</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getMessageType()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_MessageType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getMessageTypeTmplParam <em>Message Type Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getMessageTypeTmplParam()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_MessageTypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getReturnCode <em>Return Code</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Return Code</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getReturnCode()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_ReturnCode();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getReturnCodeTmplParam <em>Return Code Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Return Code Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getReturnCodeTmplParam()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_ReturnCodeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPFilter#getSomeIPLength <em>Some IP Length</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Some IP Length</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter#getSomeIPLength()
     * @see #getSomeIPFilter()
     * @generated
     */
    EAttribute getSomeIPFilter_SomeIPLength();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ForEachExpression <em>For Each Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>For Each Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.ForEachExpression
     * @generated
     */
    EClass getForEachExpression();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.ForEachExpression#getFilterExpressions <em>Filter
     * Expressions</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Filter Expressions</em>'.
     * @see de.bmw.smard.modeller.conditions.ForEachExpression#getFilterExpressions()
     * @see #getForEachExpression()
     * @generated
     */
    EReference getForEachExpression_FilterExpressions();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.ForEachExpression#getContainerSignal <em>Container Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Container Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.ForEachExpression#getContainerSignal()
     * @see #getForEachExpression()
     * @generated
     */
    EReference getForEachExpression_ContainerSignal();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.MessageCheckExpression <em>Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Message Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.MessageCheckExpression
     * @generated
     */
    EClass getMessageCheckExpression();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.MessageCheckExpression#getMessage <em>Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Message</em>'.
     * @see de.bmw.smard.modeller.conditions.MessageCheckExpression#getMessage()
     * @see #getMessageCheckExpression()
     * @generated
     */
    EReference getMessageCheckExpression_Message();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter <em>Some IPSD Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Some IPSD Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter
     * @generated
     */
    EClass getSomeIPSDFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlags <em>Flags</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Flags</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlags()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_Flags();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlagsTmplParam <em>Flags Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Flags Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlagsTmplParam()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_FlagsTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdType <em>Sd Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sd Type</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdType()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_SdType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdTypeTmplParam <em>Sd Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sd Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdTypeTmplParam()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_SdTypeTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getInstanceId <em>Instance Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Instance Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getInstanceId()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_InstanceId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getTtl <em>Ttl</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ttl</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getTtl()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_Ttl();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMajorVersion <em>Major Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Major Version</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMajorVersion()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_MajorVersion();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMinorVersion <em>Minor Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Minor Version</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMinorVersion()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_MinorVersion();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getEventGroupId <em>Event Group Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Event Group Id</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getEventGroupId()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_EventGroupId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexFirstOption <em>Index First Option</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Index First Option</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexFirstOption()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_IndexFirstOption();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexSecondOption <em>Index Second Option</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Index Second Option</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexSecondOption()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_IndexSecondOption();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberFirstOption <em>Number First Option</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Number First Option</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberFirstOption()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_NumberFirstOption();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberSecondOption <em>Number Second Option</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Number Second Option</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberSecondOption()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_NumberSecondOption();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getServiceId_SomeIPSD <em>Service Id Some IPSD</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Service Id Some IPSD</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter#getServiceId_SomeIPSD()
     * @see #getSomeIPSDFilter()
     * @generated
     */
    EAttribute getSomeIPSDFilter_ServiceId_SomeIPSD();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SomeIPSDMessage <em>Some IPSD Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Some IPSD Message</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDMessage
     * @generated
     */
    EClass getSomeIPSDMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD
     * Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Some IPSD Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDMessage#getSomeIPSDFilter()
     * @see #getSomeIPSDMessage()
     * @generated
     */
    EReference getSomeIPSDMessage_SomeIPSDFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DoubleSignal <em>Double Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Double Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleSignal
     * @generated
     */
    EClass getDoubleSignal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ignore Invalid Values</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValues()
     * @see #getDoubleSignal()
     * @generated
     */
    EAttribute getDoubleSignal_IgnoreInvalidValues();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ignore Invalid Values Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam()
     * @see #getDoubleSignal()
     * @generated
     */
    EAttribute getDoubleSignal_IgnoreInvalidValuesTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StringSignal <em>String Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>String Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.StringSignal
     * @generated
     */
    EClass getStringSignal();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DecodeStrategy <em>Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Decode Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.DecodeStrategy
     * @generated
     */
    EClass getDecodeStrategy();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DoubleDecodeStrategy <em>Double Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Double Decode Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleDecodeStrategy
     * @generated
     */
    EClass getDoubleDecodeStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DoubleDecodeStrategy#getFactor <em>Factor</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Factor</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleDecodeStrategy#getFactor()
     * @see #getDoubleDecodeStrategy()
     * @generated
     */
    EAttribute getDoubleDecodeStrategy_Factor();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DoubleDecodeStrategy#getOffset <em>Offset</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Offset</em>'.
     * @see de.bmw.smard.modeller.conditions.DoubleDecodeStrategy#getOffset()
     * @see #getDoubleDecodeStrategy()
     * @generated
     */
    EAttribute getDoubleDecodeStrategy_Offset();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ExtractStrategy <em>Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.ExtractStrategy
     * @generated
     */
    EClass getExtractStrategy();

    /**
     * Returns the meta object for the container reference '{@link de.bmw.smard.modeller.conditions.ExtractStrategy#getAbstractSignal <em>Abstract
     * Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the container reference '<em>Abstract Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.ExtractStrategy#getAbstractSignal()
     * @see #getExtractStrategy()
     * @generated
     */
    EReference getExtractStrategy_AbstractSignal();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy <em>Universal Payload Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Universal Payload Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy
     * @generated
     */
    EClass getUniversalPayloadExtractStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRule <em>Extraction
     * Rule</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Extraction Rule</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRule()
     * @see #getUniversalPayloadExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadExtractStrategy_ExtractionRule();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getByteOrder <em>Byte Order</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Byte Order</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getByteOrder()
     * @see #getUniversalPayloadExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadExtractStrategy_ByteOrder();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam
     * <em>Extraction Rule Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Extraction Rule Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getExtractionRuleTmplParam()
     * @see #getUniversalPayloadExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataType <em>Signal Data
     * Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Signal Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataType()
     * @see #getUniversalPayloadExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadExtractStrategy_SignalDataType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam <em>Signal
     * Data Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Signal Data Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy#getSignalDataTypeTmplParam()
     * @see #getUniversalPayloadExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.EmptyExtractStrategy <em>Empty Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Empty Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.EmptyExtractStrategy
     * @generated
     */
    EClass getEmptyExtractStrategy();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.CANFilter <em>CAN Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>CAN Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter
     * @generated
     */
    EClass getCANFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getMessageIdRange <em>Message Id Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Id Range</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getMessageIdRange()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_MessageIdRange();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameId <em>Frame Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Frame Id</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getFrameId()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_FrameId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlag <em>Rxtx Flag</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Rxtx Flag</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlag()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_RxtxFlag();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlagTmplParam <em>Rxtx Flag Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Rxtx Flag Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlagTmplParam()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_RxtxFlagTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifier <em>Ext Identifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ext Identifier</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifier()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_ExtIdentifier();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifierTmplParam <em>Ext Identifier Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ext Identifier Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifierTmplParam()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_ExtIdentifierTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameType <em>Frame Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Frame Type</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getFrameType()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_FrameType();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameTypeTmplParam <em>Frame Type Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Frame Type Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFilter#getFrameTypeTmplParam()
     * @see #getCANFilter()
     * @generated
     */
    EAttribute getCANFilter_FrameTypeTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.LINFilter <em>LIN Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>LIN Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.LINFilter
     * @generated
     */
    EClass getLINFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LINFilter#getMessageIdRange <em>Message Id Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Id Range</em>'.
     * @see de.bmw.smard.modeller.conditions.LINFilter#getMessageIdRange()
     * @see #getLINFilter()
     * @generated
     */
    EAttribute getLINFilter_MessageIdRange();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.LINFilter#getFrameId <em>Frame Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Frame Id</em>'.
     * @see de.bmw.smard.modeller.conditions.LINFilter#getFrameId()
     * @see #getLINFilter()
     * @generated
     */
    EAttribute getLINFilter_FrameId();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.FlexRayFilter <em>Flex Ray Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Flex Ray Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter
     * @generated
     */
    EClass getFlexRayFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Id Range</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getMessageIdRange()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_MessageIdRange();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Slot Id</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getSlotId()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_SlotId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Cycle Offset</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleOffset()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_CycleOffset();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Cycle Repetition</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleRepetition()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_CycleRepetition();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannel <em>Channel</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Channel</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getChannel()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_Channel();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Channel Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter#getChannelTmplParam()
     * @see #getFlexRayFilter()
     * @generated
     */
    EAttribute getFlexRayFilter_ChannelTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DLTFilter <em>DLT Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>DLT Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter
     * @generated
     */
    EClass getDLTFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getEcuID_ECU <em>Ecu ID ECU</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Ecu ID ECU</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getEcuID_ECU()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_EcuID_ECU();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getSessionID_SEID <em>Session ID SEID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Session ID SEID</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getSessionID_SEID()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_SessionID_SEID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getApplicationID_APID <em>Application ID APID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Application ID APID</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getApplicationID_APID()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_ApplicationID_APID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getContextID_CTID <em>Context ID CTID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Context ID CTID</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getContextID_CTID()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_ContextID_CTID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageType_MSTP <em>Message Type MSTP</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Type MSTP</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageType_MSTP()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageType_MSTP();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageLogInfo_MSLI <em>Message Log Info MSLI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Log Info MSLI</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageLogInfo_MSLI()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageLogInfo_MSLI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageTraceInfo_MSTI <em>Message Trace Info MSTI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Trace Info MSTI</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageTraceInfo_MSTI()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageTraceInfo_MSTI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageBusInfo_MSBI <em>Message Bus Info MSBI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Bus Info MSBI</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageBusInfo_MSBI()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageBusInfo_MSBI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageControlInfo_MSCI <em>Message Control Info
     * MSCI</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Control Info MSCI</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageControlInfo_MSCI()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageControlInfo_MSCI();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageLogInfo_MSLITmplParam <em>Message Log Info MSLI
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Log Info MSLI Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageLogInfo_MSLITmplParam()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageLogInfo_MSLITmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam <em>Message Trace Info
     * MSTI Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Trace Info MSTI Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageTraceInfo_MSTITmplParam()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageTraceInfo_MSTITmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageBusInfo_MSBITmplParam <em>Message Bus Info MSBI
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Bus Info MSBI Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageBusInfo_MSBITmplParam()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageBusInfo_MSBITmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageControlInfo_MSCITmplParam <em>Message Control Info
     * MSCI Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Control Info MSCI Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageControlInfo_MSCITmplParam()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageControlInfo_MSCITmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.DLTFilter#getMessageType_MSTPTmplParam <em>Message Type MSTP Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Type MSTP Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTFilter#getMessageType_MSTPTmplParam()
     * @see #getDLTFilter()
     * @generated
     */
    EAttribute getDLTFilter_MessageType_MSTPTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UDPNMFilter <em>UDPNM Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>UDPNM Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter
     * @generated
     */
    EClass getUDPNMFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UDPNMFilter#getSourceNodeIdentifier <em>Source Node Identifier</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Source Node Identifier</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter#getSourceNodeIdentifier()
     * @see #getUDPNMFilter()
     * @generated
     */
    EAttribute getUDPNMFilter_SourceNodeIdentifier();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UDPNMFilter#getControlBitVector <em>Control Bit Vector</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Control Bit Vector</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter#getControlBitVector()
     * @see #getUDPNMFilter()
     * @generated
     */
    EAttribute getUDPNMFilter_ControlBitVector();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UDPNMFilter#getPwfStatus <em>Pwf Status</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Pwf Status</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter#getPwfStatus()
     * @see #getUDPNMFilter()
     * @generated
     */
    EAttribute getUDPNMFilter_PwfStatus();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UDPNMFilter#getTeilnetzStatus <em>Teilnetz Status</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Teilnetz Status</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter#getTeilnetzStatus()
     * @see #getUDPNMFilter()
     * @generated
     */
    EAttribute getUDPNMFilter_TeilnetzStatus();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.CANMessage <em>CAN Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>CAN Message</em>'.
     * @see de.bmw.smard.modeller.conditions.CANMessage
     * @generated
     */
    EClass getCANMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.CANMessage#getCanFilter <em>Can Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Can Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.CANMessage#getCanFilter()
     * @see #getCANMessage()
     * @generated
     */
    EReference getCANMessage_CanFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.LINMessage <em>LIN Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>LIN Message</em>'.
     * @see de.bmw.smard.modeller.conditions.LINMessage
     * @generated
     */
    EClass getLINMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.LINMessage#getLinFilter <em>Lin Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Lin Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.LINMessage#getLinFilter()
     * @see #getLINMessage()
     * @generated
     */
    EReference getLINMessage_LinFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.FlexRayMessage <em>Flex Ray Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Flex Ray Message</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessage
     * @generated
     */
    EClass getFlexRayMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray
     * Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Flex Ray Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessage#getFlexRayFilter()
     * @see #getFlexRayMessage()
     * @generated
     */
    EReference getFlexRayMessage_FlexRayFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.DLTMessage <em>DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>DLT Message</em>'.
     * @see de.bmw.smard.modeller.conditions.DLTMessage
     * @generated
     */
    EClass getDLTMessage();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UDPMessage <em>UDP Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>UDP Message</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPMessage
     * @generated
     */
    EClass getUDPMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.UDPMessage#getUdpFilter <em>Udp Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Udp Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPMessage#getUdpFilter()
     * @see #getUDPMessage()
     * @generated
     */
    EReference getUDPMessage_UdpFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TCPMessage <em>TCP Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>TCP Message</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPMessage
     * @generated
     */
    EClass getTCPMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.TCPMessage#getTcpFilter <em>Tcp Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Tcp Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.TCPMessage#getTcpFilter()
     * @see #getTCPMessage()
     * @generated
     */
    EReference getTCPMessage_TcpFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UDPNMMessage <em>UDPNM Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>UDPNM Message</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMMessage
     * @generated
     */
    EClass getUDPNMMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.UDPNMMessage#getUdpnmFilter <em>Udpnm Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Udpnm Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.UDPNMMessage#getUdpnmFilter()
     * @see #getUDPNMMessage()
     * @generated
     */
    EReference getUDPNMMessage_UdpnmFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VerboseDLTMessage <em>Verbose DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Verbose DLT Message</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTMessage
     * @generated
     */
    EClass getVerboseDLTMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.VerboseDLTMessage#getDltFilter <em>Dlt Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Dlt Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTMessage#getDltFilter()
     * @see #getVerboseDLTMessage()
     * @generated
     */
    EReference getVerboseDLTMessage_DltFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy <em>Universal Payload With Legacy
     * Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Universal Payload With Legacy Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy
     * @generated
     */
    EClass getUniversalPayloadWithLegacyExtractStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit <em>Start
     * Bit</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Start Bit</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy#getStartBit()
     * @see #getUniversalPayloadWithLegacyExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadWithLegacyExtractStrategy_StartBit();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy#getLength <em>Length</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Length</em>'.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy#getLength()
     * @see #getUniversalPayloadWithLegacyExtractStrategy()
     * @generated
     */
    EAttribute getUniversalPayloadWithLegacyExtractStrategy_Length();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage <em>Abstract Bus Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Bus Message</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage
     * @generated
     */
    EClass getAbstractBusMessage();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Bus Id</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusId()
     * @see #getAbstractBusMessage()
     * @generated
     */
    EAttribute getAbstractBusMessage_BusId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Bus Id Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdTmplParam()
     * @see #getAbstractBusMessage()
     * @generated
     */
    EAttribute getAbstractBusMessage_BusIdTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Bus Id Range</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdRange()
     * @see #getAbstractBusMessage()
     * @generated
     */
    EAttribute getAbstractBusMessage_BusIdRange();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getFilters <em>Filters</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Filters</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage#getFilters()
     * @see #getAbstractBusMessage()
     * @generated
     */
    EReference getAbstractBusMessage_Filters();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginFilter <em>Plugin Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginFilter
     * @generated
     */
    EClass getPluginFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginName <em>Plugin Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Plugin Name</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginFilter#getPluginName()
     * @see #getPluginFilter()
     * @generated
     */
    EAttribute getPluginFilter_PluginName();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginFilter#getPluginVersion <em>Plugin Version</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Plugin Version</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginFilter#getPluginVersion()
     * @see #getPluginFilter()
     * @generated
     */
    EAttribute getPluginFilter_PluginVersion();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginMessage <em>Plugin Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin Message</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginMessage
     * @generated
     */
    EClass getPluginMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.PluginMessage#getPluginFilter <em>Plugin Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Plugin Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginMessage#getPluginFilter()
     * @see #getPluginMessage()
     * @generated
     */
    EReference getPluginMessage_PluginFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginSignal <em>Plugin Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginSignal
     * @generated
     */
    EClass getPluginSignal();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy <em>Plugin State Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin State Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy
     * @generated
     */
    EClass getPluginStateExtractStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStates <em>States</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>States</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStates()
     * @see #getPluginStateExtractStrategy()
     * @generated
     */
    EAttribute getPluginStateExtractStrategy_States();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesTmplParam <em>States Tmpl
     * Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>States Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesTmplParam()
     * @see #getPluginStateExtractStrategy()
     * @generated
     */
    EAttribute getPluginStateExtractStrategy_StatesTmplParam();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActive <em>States Active</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>States Active</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActive()
     * @see #getPluginStateExtractStrategy()
     * @generated
     */
    EAttribute getPluginStateExtractStrategy_StatesActive();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActiveTmplParam <em>States Active
     * Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>States Active Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActiveTmplParam()
     * @see #getPluginStateExtractStrategy()
     * @generated
     */
    EAttribute getPluginStateExtractStrategy_StatesActiveTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginResultExtractStrategy <em>Plugin Result Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin Result Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginResultExtractStrategy
     * @generated
     */
    EClass getPluginResultExtractStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginResultExtractStrategy#getResultRange <em>Result Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Result Range</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginResultExtractStrategy#getResultRange()
     * @see #getPluginResultExtractStrategy()
     * @generated
     */
    EAttribute getPluginResultExtractStrategy_ResultRange();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.EmptyDecodeStrategy <em>Empty Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Empty Decode Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.EmptyDecodeStrategy
     * @generated
     */
    EClass getEmptyDecodeStrategy();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PluginCheckExpression <em>Plugin Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Plugin Check Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginCheckExpression
     * @generated
     */
    EClass getPluginCheckExpression();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginCheckExpression#getEvaluationBehaviour <em>Evaluation
     * Behaviour</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Evaluation Behaviour</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginCheckExpression#getEvaluationBehaviour()
     * @see #getPluginCheckExpression()
     * @generated
     */
    EAttribute getPluginCheckExpression_EvaluationBehaviour();

    /**
     * Returns the meta object for the reference '{@link de.bmw.smard.modeller.conditions.PluginCheckExpression#getSignalToCheck <em>Signal To Check</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Signal To Check</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginCheckExpression#getSignalToCheck()
     * @see #getPluginCheckExpression()
     * @generated
     */
    EReference getPluginCheckExpression_SignalToCheck();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam <em>Evaluation
     * Behaviour Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Evaluation Behaviour Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginCheckExpression#getEvaluationBehaviourTmplParam()
     * @see #getPluginCheckExpression()
     * @generated
     */
    EAttribute getPluginCheckExpression_EvaluationBehaviourTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.BaseClassWithID <em>Base Class With ID</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Base Class With ID</em>'.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithID
     * @generated
     */
    EClass getBaseClassWithID();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.BaseClassWithID#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithID#getId()
     * @see #getBaseClassWithID()
     * @generated
     */
    EAttribute getBaseClassWithID_Id();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.HeaderSignal <em>Header Signal</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Header Signal</em>'.
     * @see de.bmw.smard.modeller.conditions.HeaderSignal
     * @generated
     */
    EClass getHeaderSignal();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.HeaderSignal#getAttribute <em>Attribute</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Attribute</em>'.
     * @see de.bmw.smard.modeller.conditions.HeaderSignal#getAttribute()
     * @see #getHeaderSignal()
     * @generated
     */
    EAttribute getHeaderSignal_Attribute();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.HeaderSignal#getAttributeTmplParam <em>Attribute Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Attribute Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.HeaderSignal#getAttributeTmplParam()
     * @see #getHeaderSignal()
     * @generated
     */
    EAttribute getHeaderSignal_AttributeTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IVariable Reader Writer</em>'.
     * @see de.bmw.smard.modeller.conditions.IVariableReaderWriter
     * @generated
     */
    EClass getIVariableReaderWriter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IStateTransitionReference <em>IState Transition Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IState Transition Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.IStateTransitionReference
     * @generated
     */
    EClass getIStateTransitionReference();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.TPFilter <em>TP Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>TP Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.TPFilter
     * @generated
     */
    EClass getTPFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TPFilter#getSourcePort <em>Source Port</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Source Port</em>'.
     * @see de.bmw.smard.modeller.conditions.TPFilter#getSourcePort()
     * @see #getTPFilter()
     * @generated
     */
    EAttribute getTPFilter_SourcePort();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.TPFilter#getDestPort <em>Dest Port</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Dest Port</em>'.
     * @see de.bmw.smard.modeller.conditions.TPFilter#getDestPort()
     * @see #getTPFilter()
     * @generated
     */
    EAttribute getTPFilter_DestPort();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference <em>Base Class With Source Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Base Class With Source Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithSourceReference
     * @generated
     */
    EClass getBaseClassWithSourceReference();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference <em>Source
     * Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Source Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference()
     * @see #getBaseClassWithSourceReference()
     * @generated
     */
    EReference getBaseClassWithSourceReference_SourceReference();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.SourceReference <em>Source Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Source Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.SourceReference
     * @generated
     */
    EClass getSourceReference();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.SourceReference#getSerializedReference <em>Serialized
     * Reference</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Serialized Reference</em>'.
     * @see de.bmw.smard.modeller.conditions.SourceReference#getSerializedReference()
     * @see #getSourceReference()
     * @generated
     */
    EAttribute getSourceReference_SerializedReference();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.EthernetMessage <em>Ethernet Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Ethernet Message</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetMessage
     * @generated
     */
    EClass getEthernetMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.EthernetMessage#getEthernetFilter <em>Ethernet
     * Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Ethernet Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetMessage#getEthernetFilter()
     * @see #getEthernetMessage()
     * @generated
     */
    EReference getEthernetMessage_EthernetFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.IPv4Message <em>IPv4 Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>IPv4 Message</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Message
     * @generated
     */
    EClass getIPv4Message();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.IPv4Message#getIPv4Filter <em>IPv4 Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>IPv4 Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.IPv4Message#getIPv4Filter()
     * @see #getIPv4Message()
     * @generated
     */
    EReference getIPv4Message_IPv4Filter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTMessage <em>Non Verbose DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Non Verbose DLT Message</em>'.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTMessage
     * @generated
     */
    EClass getNonVerboseDLTMessage();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTMessage#getNonVerboseDltFilter <em>Non
     * Verbose Dlt Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Non Verbose Dlt Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTMessage#getNonVerboseDltFilter()
     * @see #getNonVerboseDLTMessage()
     * @generated
     */
    EReference getNonVerboseDLTMessage_NonVerboseDltFilter();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StringDecodeStrategy <em>String Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>String Decode Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.StringDecodeStrategy
     * @generated
     */
    EClass getStringDecodeStrategy();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StringDecodeStrategy#getStringTermination <em>String
     * Termination</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>String Termination</em>'.
     * @see de.bmw.smard.modeller.conditions.StringDecodeStrategy#getStringTermination()
     * @see #getStringDecodeStrategy()
     * @generated
     */
    EAttribute getStringDecodeStrategy_StringTermination();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.StringDecodeStrategy#getStringTerminationTmplParam <em>String
     * Termination Tmpl Param</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>String Termination Tmpl Param</em>'.
     * @see de.bmw.smard.modeller.conditions.StringDecodeStrategy#getStringTerminationTmplParam()
     * @see #getStringDecodeStrategy()
     * @generated
     */
    EAttribute getStringDecodeStrategy_StringTerminationTmplParam();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter <em>Non Verbose DLT Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Non Verbose DLT Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTFilter
     * @generated
     */
    EClass getNonVerboseDLTFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageId <em>Message Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Id</em>'.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageId()
     * @see #getNonVerboseDLTFilter()
     * @generated
     */
    EAttribute getNonVerboseDLTFilter_MessageId();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageIdRange <em>Message Id Range</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Message Id Range</em>'.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTFilter#getMessageIdRange()
     * @see #getNonVerboseDLTFilter()
     * @generated
     */
    EAttribute getNonVerboseDLTFilter_MessageIdRange();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy <em>Verbose DLT Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Verbose DLT Extract Strategy</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy
     * @generated
     */
    EClass getVerboseDLTExtractStrategy();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.RegexOperation <em>Regex Operation</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Regex Operation</em>'.
     * @see de.bmw.smard.modeller.conditions.RegexOperation
     * @generated
     */
    EClass getRegexOperation();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.RegexOperation#getRegex <em>Regex</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Regex</em>'.
     * @see de.bmw.smard.modeller.conditions.RegexOperation#getRegex()
     * @see #getRegexOperation()
     * @generated
     */
    EAttribute getRegexOperation_Regex();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.conditions.RegexOperation#getDynamicRegex <em>Dynamic Regex</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Dynamic Regex</em>'.
     * @see de.bmw.smard.modeller.conditions.RegexOperation#getDynamicRegex()
     * @see #getRegexOperation()
     * @generated
     */
    EReference getRegexOperation_DynamicRegex();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.PayloadFilter <em>Payload Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Payload Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.PayloadFilter
     * @generated
     */
    EClass getPayloadFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getIndex <em>Index</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Index</em>'.
     * @see de.bmw.smard.modeller.conditions.PayloadFilter#getIndex()
     * @see #getPayloadFilter()
     * @generated
     */
    EAttribute getPayloadFilter_Index();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getMask <em>Mask</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Mask</em>'.
     * @see de.bmw.smard.modeller.conditions.PayloadFilter#getMask()
     * @see #getPayloadFilter()
     * @generated
     */
    EAttribute getPayloadFilter_Mask();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see de.bmw.smard.modeller.conditions.PayloadFilter#getValue()
     * @see #getPayloadFilter()
     * @generated
     */
    EAttribute getPayloadFilter_Value();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter <em>Verbose DLT Payload Filter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Verbose DLT Payload Filter</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter
     * @generated
     */
    EClass getVerboseDLTPayloadFilter();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getRegex <em>Regex</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Regex</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getRegex()
     * @see #getVerboseDLTPayloadFilter()
     * @generated
     */
    EAttribute getVerboseDLTPayloadFilter_Regex();

    /**
     * Returns the meta object for the attribute list '{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getContainsAny <em>Contains Any</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute list '<em>Contains Any</em>'.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter#getContainsAny()
     * @see #getVerboseDLTPayloadFilter()
     * @generated
     */
    EAttribute getVerboseDLTPayloadFilter_ContainsAny();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.AbstractVariable <em>Abstract Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Abstract Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractVariable
     * @generated
     */
    EClass getAbstractVariable();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractVariable#getName <em>Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractVariable#getName()
     * @see #getAbstractVariable()
     * @generated
     */
    EAttribute getAbstractVariable_Name();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractVariable#getDisplayName <em>Display Name</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Display Name</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractVariable#getDisplayName()
     * @see #getAbstractVariable()
     * @generated
     */
    EAttribute getAbstractVariable_DisplayName();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.AbstractVariable#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.conditions.AbstractVariable#getDescription()
     * @see #getAbstractVariable()
     * @generated
     */
    EAttribute getAbstractVariable_Description();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.StructureVariable <em>Structure Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Structure Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.StructureVariable
     * @generated
     */
    EClass getStructureVariable();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.StructureVariable#getVariables <em>Variables</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Variables</em>'.
     * @see de.bmw.smard.modeller.conditions.StructureVariable#getVariables()
     * @see #getStructureVariable()
     * @generated
     */
    EReference getStructureVariable_Variables();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.StructureVariable#getVariableReferences <em>Variable
     * References</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Variable References</em>'.
     * @see de.bmw.smard.modeller.conditions.StructureVariable#getVariableReferences()
     * @see #getStructureVariable()
     * @generated
     */
    EReference getStructureVariable_VariableReferences();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.conditions.ComputedVariable <em>Computed Variable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Computed Variable</em>'.
     * @see de.bmw.smard.modeller.conditions.ComputedVariable
     * @generated
     */
    EClass getComputedVariable();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.conditions.ComputedVariable#getOperands <em>Operands</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Operands</em>'.
     * @see de.bmw.smard.modeller.conditions.ComputedVariable#getOperands()
     * @see #getComputedVariable()
     * @generated
     */
    EReference getComputedVariable_Operands();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.conditions.ComputedVariable#getExpression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Expression</em>'.
     * @see de.bmw.smard.modeller.conditions.ComputedVariable#getExpression()
     * @see #getComputedVariable()
     * @generated
     */
    EAttribute getComputedVariable_Expression();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.CheckType <em>Check Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Check Type</em>'.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @generated
     */
    EEnum getCheckType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.Comparator <em>Comparator</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Comparator</em>'.
     * @see de.bmw.smard.modeller.conditions.Comparator
     * @generated
     */
    EEnum getComparator();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.LogicalOperatorType <em>Logical Operator Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Logical Operator Type</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @generated
     */
    EEnum getLogicalOperatorType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SignalDataType <em>Signal Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Signal Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalDataType
     * @generated
     */
    EEnum getSignalDataType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.FlexChannelType <em>Flex Channel Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Flex Channel Type</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexChannelType
     * @generated
     */
    EEnum getFlexChannelType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.ByteOrderType <em>Byte Order Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Byte Order Type</em>'.
     * @see de.bmw.smard.modeller.conditions.ByteOrderType
     * @generated
     */
    EEnum getByteOrderType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.FormatDataType <em>Format Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Format Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.FormatDataType
     * @generated
     */
    EEnum getFormatDataType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection <em>Flex Ray Header Flag Selection</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Flex Ray Header Flag Selection</em>'.
     * @see de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection
     * @generated
     */
    EEnum getFlexRayHeaderFlagSelection();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum <em>Boolean Or Template Placeholder
     * Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Boolean Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getBooleanOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum <em>Protocol Type Or Template Placeholder
     * Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Protocol Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getProtocolTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection <em>Ethernet Protocol Type Selection</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Ethernet Protocol Type Selection</em>'.
     * @see de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection
     * @generated
     */
    EEnum getEthernetProtocolTypeSelection();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum <em>Rx Tx Flag Type Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Rx Tx Flag Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getRxTxFlagTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.ObserverValueType <em>Observer Value Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Observer Value Type</em>'.
     * @see de.bmw.smard.modeller.conditions.ObserverValueType
     * @generated
     */
    EEnum getObserverValueType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum <em>Plugin State Value Type Or
     * Template Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Plugin State Value Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getPluginStateValueTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum <em>Analysis Type Or Template Placeholder
     * Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Analysis Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getAnalysisTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum <em>Some IP Message Type Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IP Message Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPMessageTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum <em>Some IP Method Type Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IP Method Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPMethodTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum <em>Some IP Return Code Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IP Return Code Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPReturnCodeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum <em>Some IPSD Flags Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IPSD Flags Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPSDFlagsOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum <em>Some IPSD Type Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IPSD Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPSDTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum <em>Stream Analysis Flags Or
     * Template Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Stream Analysis Flags Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getStreamAnalysisFlagsOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum <em>Some IPSD Entry Flags Or
     * Template Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Some IPSD Entry Flags Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum <em>Can Ext Identifier Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Can Ext Identifier Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getCanExtIdentifierOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DataType <em>Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.DataType
     * @generated
     */
    EEnum getDataType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DLT_MessageType <em>DLT Message Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>DLT Message Type</em>'.
     * @see de.bmw.smard.modeller.conditions.DLT_MessageType
     * @generated
     */
    EEnum getDLT_MessageType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo <em>DLT Message Trace Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>DLT Message Trace Info</em>'.
     * @see de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo
     * @generated
     */
    EEnum getDLT_MessageTraceInfo();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DLT_MessageLogInfo <em>DLT Message Log Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>DLT Message Log Info</em>'.
     * @see de.bmw.smard.modeller.conditions.DLT_MessageLogInfo
     * @generated
     */
    EEnum getDLT_MessageLogInfo();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DLT_MessageControlInfo <em>DLT Message Control Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>DLT Message Control Info</em>'.
     * @see de.bmw.smard.modeller.conditions.DLT_MessageControlInfo
     * @generated
     */
    EEnum getDLT_MessageControlInfo();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.DLT_MessageBusInfo <em>DLT Message Bus Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>DLT Message Bus Info</em>'.
     * @see de.bmw.smard.modeller.conditions.DLT_MessageBusInfo
     * @generated
     */
    EEnum getDLT_MessageBusInfo();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate <em>For Each Expression Modifier
     * Template</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>For Each Expression Modifier Template</em>'.
     * @see de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate
     * @generated
     */
    EEnum getForEachExpressionModifierTemplate();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.AUTOSARDataType <em>AUTOSAR Data Type</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>AUTOSAR Data Type</em>'.
     * @see de.bmw.smard.modeller.conditions.AUTOSARDataType
     * @generated
     */
    EEnum getAUTOSARDataType();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum <em>CAN Frame Type Or Template
     * Placeholder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>CAN Frame Type Or Template Placeholder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum
     * @generated
     */
    EEnum getCANFrameTypeOrTemplatePlaceholderEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum <em>Evaluation Behaviour Or Template
     * Defined Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Evaluation Behaviour Or Template Defined Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum
     * @generated
     */
    EEnum getEvaluationBehaviourOrTemplateDefinedEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.SignalVariableLagEnum <em>Signal Variable Lag Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Signal Variable Lag Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.SignalVariableLagEnum
     * @generated
     */
    EEnum getSignalVariableLagEnum();

    /**
     * Returns the meta object for enum '{@link de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum <em>TTL Or Template Place Holder Enum</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>TTL Or Template Place Holder Enum</em>'.
     * @see de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum
     * @generated
     */
    EEnum getTTLOrTemplatePlaceHolderEnum();

    /**
     * Returns the meta object for data type '{@link de.bmw.smard.modeller.conditions.CheckType <em>Check Type Object</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Check Type Object</em>'.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @model instanceClass="de.bmw.smard.modeller.conditions.CheckType"
     *        extendedMetaData="name='checkType:Object' baseType='checkType'"
     * @generated
     */
    EDataType getCheckTypeObject();

    /**
     * Returns the meta object for data type '{@link de.bmw.smard.modeller.conditions.Comparator <em>Comparator Object</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Comparator Object</em>'.
     * @see de.bmw.smard.modeller.conditions.Comparator
     * @model instanceClass="de.bmw.smard.modeller.conditions.Comparator"
     *        extendedMetaData="name='comparator:Object' baseType='comparator'"
     * @generated
     */
    EDataType getComparatorObject();

    /**
     * Returns the meta object for data type '{@link de.bmw.smard.modeller.conditions.LogicalOperatorType <em>Logical Operator Type Object</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Logical Operator Type Object</em>'.
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @model instanceClass="de.bmw.smard.modeller.conditions.LogicalOperatorType"
     *        extendedMetaData="name='logicalOperatorType:Object' baseType='logicalOperatorType'"
     * @generated
     */
    EDataType getLogicalOperatorTypeObject();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Hex Or Int Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Hex Or Int Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getHexOrIntOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Int Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Int Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getIntOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Long Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Long Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getLongOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Double Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Double Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getDoubleOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Double Or Hex Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Double Or Hex Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getDoubleOrHexOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>IP Pattern Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>IP Pattern Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getIPPatternOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>MAC Pattern Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>MAC Pattern Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getMACPatternOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Port Pattern Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Port Pattern Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getPortPatternOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Vlan Id Pattern Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Vlan Id Pattern Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getVlanIdPatternOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>IP Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>IP Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getIPOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Port Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Port Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getPortOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Vlan Id Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Vlan Id Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getVlanIdOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>MAC Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>MAC Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getMACOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Bitmask Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Bitmask Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getBitmaskOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Hex Or Int Or Null Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Hex Or Int Or Null Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getHexOrIntOrNullOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Non Zero Double Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Non Zero Double Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getNonZeroDoubleOrTemplatePlaceholder();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Val Var Initial Value Double Or Template Placeholder Or String</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Val Var Initial Value Double Or Template Placeholder Or String</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getValVarInitialValueDoubleOrTemplatePlaceholderOrString();

    /**
     * Returns the meta object for data type '{@link java.lang.String <em>Byte Or Template Placeholder</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for data type '<em>Byte Or Template Placeholder</em>'.
     * @see java.lang.String
     * @model instanceClass="java.lang.String"
     * @generated
     */
    EDataType getByteOrTemplatePlaceholder();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ConditionsFactory getConditionsFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DocumentRootImpl <em>Document Root</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DocumentRootImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDocumentRoot()
         * @generated
         */
        EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

        /**
         * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

        /**
         * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

        /**
         * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

        /**
         * The meta object literal for the '<em><b>Condition Set</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__CONDITION_SET = eINSTANCE.getDocumentRoot_ConditionSet();

        /**
         * The meta object literal for the '<em><b>Conditions Document</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__CONDITIONS_DOCUMENT = eINSTANCE.getDocumentRoot_ConditionsDocument();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionSetImpl <em>Condition Set</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ConditionSetImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConditionSet()
         * @generated
         */
        EClass CONDITION_SET = eINSTANCE.getConditionSet();

        /**
         * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__GROUP = eINSTANCE.getConditionSet_Group();

        /**
         * The meta object literal for the '<em><b>Baureihe</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__BAUREIHE = eINSTANCE.getConditionSet_Baureihe();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__DESCRIPTION = eINSTANCE.getConditionSet_Description();

        /**
         * The meta object literal for the '<em><b>Istufe</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__ISTUFE = eINSTANCE.getConditionSet_Istufe();

        /**
         * The meta object literal for the '<em><b>Schemaversion</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__SCHEMAVERSION = eINSTANCE.getConditionSet_Schemaversion();

        /**
         * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__UUID = eINSTANCE.getConditionSet_Uuid();

        /**
         * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__VERSION = eINSTANCE.getConditionSet_Version();

        /**
         * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONDITION_SET__CONDITIONS = eINSTANCE.getConditionSet_Conditions();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION_SET__NAME = eINSTANCE.getConditionSet_Name();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionImpl <em>Condition</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ConditionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCondition()
         * @generated
         */
        EClass CONDITION = eINSTANCE.getCondition();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION__NAME = eINSTANCE.getCondition_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONDITION__DESCRIPTION = eINSTANCE.getCondition_Description();

        /**
         * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONDITION__EXPRESSION = eINSTANCE.getCondition_Expression();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl <em>Signal Comparison Expression</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SignalComparisonExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalComparisonExpression()
         * @generated
         */
        EClass SIGNAL_COMPARISON_EXPRESSION = eINSTANCE.getSignalComparisonExpression();

        /**
         * The meta object literal for the '<em><b>Comparator Signal</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL = eINSTANCE.getSignalComparisonExpression_ComparatorSignal();

        /**
         * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SIGNAL_COMPARISON_EXPRESSION__OPERATOR = eINSTANCE.getSignalComparisonExpression_Operator();

        /**
         * The meta object literal for the '<em><b>Operator Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SIGNAL_COMPARISON_EXPRESSION__OPERATOR_TMPL_PARAM = eINSTANCE.getSignalComparisonExpression_OperatorTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.CanMessageCheckExpressionImpl <em>Can Message Check Expression</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.CanMessageCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCanMessageCheckExpression()
         * @generated
         */
        EClass CAN_MESSAGE_CHECK_EXPRESSION = eINSTANCE.getCanMessageCheckExpression();

        /**
         * The meta object literal for the '<em><b>Busid</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__BUSID = eINSTANCE.getCanMessageCheckExpression_Busid();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__TYPE = eINSTANCE.getCanMessageCheckExpression_Type();

        /**
         * The meta object literal for the '<em><b>Message IDs</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = eINSTANCE.getCanMessageCheckExpression_MessageIDs();

        /**
         * The meta object literal for the '<em><b>Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = eINSTANCE.getCanMessageCheckExpression_TypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Busid Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = eINSTANCE.getCanMessageCheckExpression_BusidTmplParam();

        /**
         * The meta object literal for the '<em><b>Rxtx Flag</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG = eINSTANCE.getCanMessageCheckExpression_RxtxFlag();

        /**
         * The meta object literal for the '<em><b>Rxtx Flag Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__RXTX_FLAG_TYPE_TMPL_PARAM = eINSTANCE.getCanMessageCheckExpression_RxtxFlagTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Ext Identifier</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER = eINSTANCE.getCanMessageCheckExpression_ExtIdentifier();

        /**
         * The meta object literal for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_MESSAGE_CHECK_EXPRESSION__EXT_IDENTIFIER_TMPL_PARAM = eINSTANCE.getCanMessageCheckExpression_ExtIdentifierTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl <em>Lin Message Check Expression</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLinMessageCheckExpression()
         * @generated
         */
        EClass LIN_MESSAGE_CHECK_EXPRESSION = eINSTANCE.getLinMessageCheckExpression();

        /**
         * The meta object literal for the '<em><b>Busid</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_MESSAGE_CHECK_EXPRESSION__BUSID = eINSTANCE.getLinMessageCheckExpression_Busid();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_MESSAGE_CHECK_EXPRESSION__TYPE = eINSTANCE.getLinMessageCheckExpression_Type();

        /**
         * The meta object literal for the '<em><b>Message IDs</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS = eINSTANCE.getLinMessageCheckExpression_MessageIDs();

        /**
         * The meta object literal for the '<em><b>Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = eINSTANCE.getLinMessageCheckExpression_TypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Busid Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM = eINSTANCE.getLinMessageCheckExpression_BusidTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl <em>State Check Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStateCheckExpression()
         * @generated
         */
        EClass STATE_CHECK_EXPRESSION = eINSTANCE.getStateCheckExpression();

        /**
         * The meta object literal for the '<em><b>Check State Active</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE = eINSTANCE.getStateCheckExpression_CheckStateActive();

        /**
         * The meta object literal for the '<em><b>Check State</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_CHECK_EXPRESSION__CHECK_STATE = eINSTANCE.getStateCheckExpression_CheckState();

        /**
         * The meta object literal for the '<em><b>Check State Active Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM = eINSTANCE.getStateCheckExpression_CheckStateActiveTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl <em>Timing Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TimingExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTimingExpression()
         * @generated
         */
        EClass TIMING_EXPRESSION = eINSTANCE.getTimingExpression();

        /**
         * The meta object literal for the '<em><b>Maxtime</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TIMING_EXPRESSION__MAXTIME = eINSTANCE.getTimingExpression_Maxtime();

        /**
         * The meta object literal for the '<em><b>Mintime</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TIMING_EXPRESSION__MINTIME = eINSTANCE.getTimingExpression_Mintime();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TrueExpressionImpl <em>True Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TrueExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTrueExpression()
         * @generated
         */
        EClass TRUE_EXPRESSION = eINSTANCE.getTrueExpression();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalExpression()
         * @generated
         */
        EClass LOGICAL_EXPRESSION = eINSTANCE.getLogicalExpression();

        /**
         * The meta object literal for the '<em><b>Expressions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LOGICAL_EXPRESSION__EXPRESSIONS = eINSTANCE.getLogicalExpression_Expressions();

        /**
         * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LOGICAL_EXPRESSION__OPERATOR = eINSTANCE.getLogicalExpression_Operator();

        /**
         * The meta object literal for the '<em><b>Operator Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM = eINSTANCE.getLogicalExpression_OperatorTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ReferenceConditionExpressionImpl <em>Reference Condition
         * Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ReferenceConditionExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getReferenceConditionExpression()
         * @generated
         */
        EClass REFERENCE_CONDITION_EXPRESSION = eINSTANCE.getReferenceConditionExpression();

        /**
         * The meta object literal for the '<em><b>Condition</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference REFERENCE_CONDITION_EXPRESSION__CONDITION = eINSTANCE.getReferenceConditionExpression_Condition();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl <em>Transition Check Expression</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TransitionCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTransitionCheckExpression()
         * @generated
         */
        EClass TRANSITION_CHECK_EXPRESSION = eINSTANCE.getTransitionCheckExpression();

        /**
         * The meta object literal for the '<em><b>Stay Active</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE = eINSTANCE.getTransitionCheckExpression_StayActive();

        /**
         * The meta object literal for the '<em><b>Check Transition</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TRANSITION_CHECK_EXPRESSION__CHECK_TRANSITION = eINSTANCE.getTransitionCheckExpression_CheckTransition();

        /**
         * The meta object literal for the '<em><b>Stay Active Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TRANSITION_CHECK_EXPRESSION__STAY_ACTIVE_TMPL_PARAM = eINSTANCE.getTransitionCheckExpression_StayActiveTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl <em>Flex Ray Message Check
         * Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayMessageCheckExpression()
         * @generated
         */
        EClass FLEX_RAY_MESSAGE_CHECK_EXPRESSION = eINSTANCE.getFlexRayMessageCheckExpression();

        /**
         * The meta object literal for the '<em><b>Bus Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID = eINSTANCE.getFlexRayMessageCheckExpression_BusId();

        /**
         * The meta object literal for the '<em><b>Payload Preamble</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE = eINSTANCE.getFlexRayMessageCheckExpression_PayloadPreamble();

        /**
         * The meta object literal for the '<em><b>Zero Frame</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME = eINSTANCE.getFlexRayMessageCheckExpression_ZeroFrame();

        /**
         * The meta object literal for the '<em><b>Sync Frame</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME = eINSTANCE.getFlexRayMessageCheckExpression_SyncFrame();

        /**
         * The meta object literal for the '<em><b>Startup Frame</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME = eINSTANCE.getFlexRayMessageCheckExpression_StartupFrame();

        /**
         * The meta object literal for the '<em><b>Network Mgmt</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT = eINSTANCE.getFlexRayMessageCheckExpression_NetworkMgmt();

        /**
         * The meta object literal for the '<em><b>Flexray Message Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID = eINSTANCE.getFlexRayMessageCheckExpression_FlexrayMessageId();

        /**
         * The meta object literal for the '<em><b>Payload Preamble Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_PayloadPreambleTmplParam();

        /**
         * The meta object literal for the '<em><b>Zero Frame Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_ZeroFrameTmplParam();

        /**
         * The meta object literal for the '<em><b>Sync Frame Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_SyncFrameTmplParam();

        /**
         * The meta object literal for the '<em><b>Startup Frame Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_StartupFrameTmplParam();

        /**
         * The meta object literal for the '<em><b>Network Mgmt Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_NetworkMgmtTmplParam();

        /**
         * The meta object literal for the '<em><b>Bus Id Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_BusIdTmplParam();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE = eINSTANCE.getFlexRayMessageCheckExpression_Type();

        /**
         * The meta object literal for the '<em><b>Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM = eINSTANCE.getFlexRayMessageCheckExpression_TypeTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StopExpressionImpl <em>Stop Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StopExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStopExpression()
         * @generated
         */
        EClass STOP_EXPRESSION = eINSTANCE.getStopExpression();

        /**
         * The meta object literal for the '<em><b>Analyse Art</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STOP_EXPRESSION__ANALYSE_ART = eINSTANCE.getStopExpression_AnalyseArt();

        /**
         * The meta object literal for the '<em><b>Analyse Art Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM = eINSTANCE.getStopExpression_AnalyseArtTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ComparatorSignalImpl <em>Comparator Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ComparatorSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparatorSignal()
         * @generated
         */
        EClass COMPARATOR_SIGNAL = eINSTANCE.getComparatorSignal();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COMPARATOR_SIGNAL__DESCRIPTION = eINSTANCE.getComparatorSignal_Description();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl <em>Constant Comparator Value</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ConstantComparatorValueImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConstantComparatorValue()
         * @generated
         */
        EClass CONSTANT_COMPARATOR_VALUE = eINSTANCE.getConstantComparatorValue();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONSTANT_COMPARATOR_VALUE__VALUE = eINSTANCE.getConstantComparatorValue_Value();

        /**
         * The meta object literal for the '<em><b>Interpreted Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE = eINSTANCE.getConstantComparatorValue_InterpretedValue();

        /**
         * The meta object literal for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONSTANT_COMPARATOR_VALUE__INTERPRETED_VALUE_TMPL_PARAM = eINSTANCE.getConstantComparatorValue_InterpretedValueTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl <em>Calculation Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCalculationExpression()
         * @generated
         */
        EClass CALCULATION_EXPRESSION = eINSTANCE.getCalculationExpression();

        /**
         * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CALCULATION_EXPRESSION__EXPRESSION = eINSTANCE.getCalculationExpression_Expression();

        /**
         * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CALCULATION_EXPRESSION__OPERANDS = eINSTANCE.getCalculationExpression_Operands();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ExpressionImpl <em>Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExpression()
         * @generated
         */
        EClass EXPRESSION = eINSTANCE.getExpression();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXPRESSION__DESCRIPTION = eINSTANCE.getExpression_Description();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VariableReferenceImpl <em>Variable Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VariableReferenceImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableReference()
         * @generated
         */
        EClass VARIABLE_REFERENCE = eINSTANCE.getVariableReference();

        /**
         * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference VARIABLE_REFERENCE__VARIABLE = eINSTANCE.getVariableReference_Variable();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ConditionsDocumentImpl <em>Document</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsDocumentImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getConditionsDocument()
         * @generated
         */
        EClass CONDITIONS_DOCUMENT = eINSTANCE.getConditionsDocument();

        /**
         * The meta object literal for the '<em><b>Signal References Set</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONDITIONS_DOCUMENT__SIGNAL_REFERENCES_SET = eINSTANCE.getConditionsDocument_SignalReferencesSet();

        /**
         * The meta object literal for the '<em><b>Variable Set</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONDITIONS_DOCUMENT__VARIABLE_SET = eINSTANCE.getConditionsDocument_VariableSet();

        /**
         * The meta object literal for the '<em><b>Condition Set</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONDITIONS_DOCUMENT__CONDITION_SET = eINSTANCE.getConditionsDocument_ConditionSet();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VariableSetImpl <em>Variable Set</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VariableSetImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableSet()
         * @generated
         */
        EClass VARIABLE_SET = eINSTANCE.getVariableSet();

        /**
         * The meta object literal for the '<em><b>Uuid</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE_SET__UUID = eINSTANCE.getVariableSet_Uuid();

        /**
         * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference VARIABLE_SET__VARIABLES = eINSTANCE.getVariableSet_Variables();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VariableImpl <em>Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariable()
         * @generated
         */
        EClass VARIABLE = eINSTANCE.getVariable();

        /**
         * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE__DATA_TYPE = eINSTANCE.getVariable_DataType();

        /**
         * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE__UNIT = eINSTANCE.getVariable_Unit();

        /**
         * The meta object literal for the '<em><b>Variable Format</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference VARIABLE__VARIABLE_FORMAT = eINSTANCE.getVariable_VariableFormat();

        /**
         * The meta object literal for the '<em><b>Variable Format Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE__VARIABLE_FORMAT_TMPL_PARAM = eINSTANCE.getVariable_VariableFormatTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl <em>Signal Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SignalVariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalVariable()
         * @generated
         */
        EClass SIGNAL_VARIABLE = eINSTANCE.getSignalVariable();

        /**
         * The meta object literal for the '<em><b>Interpreted Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SIGNAL_VARIABLE__INTERPRETED_VALUE = eINSTANCE.getSignalVariable_InterpretedValue();

        /**
         * The meta object literal for the '<em><b>Signal</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SIGNAL_VARIABLE__SIGNAL = eINSTANCE.getSignalVariable_Signal();

        /**
         * The meta object literal for the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM = eINSTANCE.getSignalVariable_InterpretedValueTmplParam();

        /**
         * The meta object literal for the '<em><b>Signal Observers</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SIGNAL_VARIABLE__SIGNAL_OBSERVERS = eINSTANCE.getSignalVariable_SignalObservers();

        /**
         * The meta object literal for the '<em><b>Lag</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SIGNAL_VARIABLE__LAG = eINSTANCE.getSignalVariable_Lag();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ValueVariableImpl <em>Value Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ValueVariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValueVariable()
         * @generated
         */
        EClass VALUE_VARIABLE = eINSTANCE.getValueVariable();

        /**
         * The meta object literal for the '<em><b>Initial Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VALUE_VARIABLE__INITIAL_VALUE = eINSTANCE.getValueVariable_InitialValue();

        /**
         * The meta object literal for the '<em><b>Value Variable Observers</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference VALUE_VARIABLE__VALUE_VARIABLE_OBSERVERS = eINSTANCE.getValueVariable_ValueVariableObservers();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VariableFormatImpl <em>Variable Format</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VariableFormatImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVariableFormat()
         * @generated
         */
        EClass VARIABLE_FORMAT = eINSTANCE.getVariableFormat();

        /**
         * The meta object literal for the '<em><b>Digits</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE_FORMAT__DIGITS = eINSTANCE.getVariableFormat_Digits();

        /**
         * The meta object literal for the '<em><b>Base Data Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE_FORMAT__BASE_DATA_TYPE = eINSTANCE.getVariableFormat_BaseDataType();

        /**
         * The meta object literal for the '<em><b>Upper Case</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VARIABLE_FORMAT__UPPER_CASE = eINSTANCE.getVariableFormat_UpperCase();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl <em>Abstract Observer</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractObserverImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractObserver()
         * @generated
         */
        EClass ABSTRACT_OBSERVER = eINSTANCE.getAbstractObserver();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_OBSERVER__NAME = eINSTANCE.getAbstractObserver_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_OBSERVER__DESCRIPTION = eINSTANCE.getAbstractObserver_Description();

        /**
         * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_OBSERVER__LOG_LEVEL = eINSTANCE.getAbstractObserver_LogLevel();

        /**
         * The meta object literal for the '<em><b>Value Ranges</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_OBSERVER__VALUE_RANGES = eINSTANCE.getAbstractObserver_ValueRanges();

        /**
         * The meta object literal for the '<em><b>Value Ranges Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM = eINSTANCE.getAbstractObserver_ValueRangesTmplParam();

        /**
         * The meta object literal for the '<em><b>Active At Start</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_OBSERVER__ACTIVE_AT_START = eINSTANCE.getAbstractObserver_ActiveAtStart();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl <em>Observer Value Range</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ObserverValueRangeImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getObserverValueRange()
         * @generated
         */
        EClass OBSERVER_VALUE_RANGE = eINSTANCE.getObserverValueRange();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute OBSERVER_VALUE_RANGE__VALUE = eINSTANCE.getObserverValueRange_Value();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute OBSERVER_VALUE_RANGE__DESCRIPTION = eINSTANCE.getObserverValueRange_Description();

        /**
         * The meta object literal for the '<em><b>Value Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute OBSERVER_VALUE_RANGE__VALUE_TYPE = eINSTANCE.getObserverValueRange_ValueType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SignalObserverImpl <em>Signal Observer</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SignalObserverImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalObserver()
         * @generated
         */
        EClass SIGNAL_OBSERVER = eINSTANCE.getSignalObserver();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceSetImpl <em>Signal Reference Set</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SignalReferenceSetImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalReferenceSet()
         * @generated
         */
        EClass SIGNAL_REFERENCE_SET = eINSTANCE.getSignalReferenceSet();

        /**
         * The meta object literal for the '<em><b>Messages</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SIGNAL_REFERENCE_SET__MESSAGES = eINSTANCE.getSignalReferenceSet_Messages();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceImpl <em>Signal Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SignalReferenceImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalReference()
         * @generated
         */
        EClass SIGNAL_REFERENCE = eINSTANCE.getSignalReference();

        /**
         * The meta object literal for the '<em><b>Signal</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SIGNAL_REFERENCE__SIGNAL = eINSTANCE.getSignalReference_Signal();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ISignalOrReference <em>ISignal Or Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ISignalOrReference
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getISignalOrReference()
         * @generated
         */
        EClass ISIGNAL_OR_REFERENCE = eINSTANCE.getISignalOrReference();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl <em>Bit Pattern Comparator Value</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.BitPatternComparatorValueImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBitPatternComparatorValue()
         * @generated
         */
        EClass BIT_PATTERN_COMPARATOR_VALUE = eINSTANCE.getBitPatternComparatorValue();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BIT_PATTERN_COMPARATOR_VALUE__VALUE = eINSTANCE.getBitPatternComparatorValue_Value();

        /**
         * The meta object literal for the '<em><b>Startbit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BIT_PATTERN_COMPARATOR_VALUE__STARTBIT = eINSTANCE.getBitPatternComparatorValue_Startbit();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.NotExpressionImpl <em>Not Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.NotExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNotExpression()
         * @generated
         */
        EClass NOT_EXPRESSION = eINSTANCE.getNotExpression();

        /**
         * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference NOT_EXPRESSION__EXPRESSION = eINSTANCE.getNotExpression_Expression();

        /**
         * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute NOT_EXPRESSION__OPERATOR = eINSTANCE.getNotExpression_Operator();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IOperand <em>IOperand</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IOperand
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIOperand()
         * @generated
         */
        EClass IOPERAND = eINSTANCE.getIOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IComputeVariableActionOperand <em>ICompute Variable Action Operand</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IComputeVariableActionOperand
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIComputeVariableActionOperand()
         * @generated
         */
        EClass ICOMPUTE_VARIABLE_ACTION_OPERAND = eINSTANCE.getIComputeVariableActionOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IStringOperand <em>IString Operand</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IStringOperand
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStringOperand()
         * @generated
         */
        EClass ISTRING_OPERAND = eINSTANCE.getIStringOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison Expression
         * Operand</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getISignalComparisonExpressionOperand()
         * @generated
         */
        EClass ISIGNAL_COMPARISON_EXPRESSION_OPERAND = eINSTANCE.getISignalComparisonExpressionOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IOperation <em>IOperation</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IOperation
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIOperation()
         * @generated
         */
        EClass IOPERATION = eINSTANCE.getIOperation();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IStringOperation <em>IString Operation</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IStringOperation
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStringOperation()
         * @generated
         */
        EClass ISTRING_OPERATION = eINSTANCE.getIStringOperation();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.INumericOperand <em>INumeric Operand</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.INumericOperand
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getINumericOperand()
         * @generated
         */
        EClass INUMERIC_OPERAND = eINSTANCE.getINumericOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.INumericOperation <em>INumeric Operation</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.INumericOperation
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getINumericOperation()
         * @generated
         */
        EClass INUMERIC_OPERATION = eINSTANCE.getINumericOperation();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ValueVariableObserverImpl <em>Value Variable Observer</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ValueVariableObserverImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValueVariableObserver()
         * @generated
         */
        EClass VALUE_VARIABLE_OBSERVER = eINSTANCE.getValueVariableObserver();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ParseStringToDoubleImpl <em>Parse String To Double</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ParseStringToDoubleImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getParseStringToDouble()
         * @generated
         */
        EClass PARSE_STRING_TO_DOUBLE = eINSTANCE.getParseStringToDouble();

        /**
         * The meta object literal for the '<em><b>String To Parse</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference PARSE_STRING_TO_DOUBLE__STRING_TO_PARSE = eINSTANCE.getParseStringToDouble_StringToParse();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StringExpressionImpl <em>String Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StringExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringExpression()
         * @generated
         */
        EClass STRING_EXPRESSION = eINSTANCE.getStringExpression();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.MatchesImpl <em>Matches</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.MatchesImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMatches()
         * @generated
         */
        EClass MATCHES = eINSTANCE.getMatches();

        /**
         * The meta object literal for the '<em><b>String To Check</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference MATCHES__STRING_TO_CHECK = eINSTANCE.getMatches_StringToCheck();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl <em>Extract</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ExtractImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExtract()
         * @generated
         */
        EClass EXTRACT = eINSTANCE.getExtract();

        /**
         * The meta object literal for the '<em><b>String To Extract</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference EXTRACT__STRING_TO_EXTRACT = eINSTANCE.getExtract_StringToExtract();

        /**
         * The meta object literal for the '<em><b>Group Index</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXTRACT__GROUP_INDEX = eINSTANCE.getExtract_GroupIndex();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SubstringImpl <em>Substring</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SubstringImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSubstring()
         * @generated
         */
        EClass SUBSTRING = eINSTANCE.getSubstring();

        /**
         * The meta object literal for the '<em><b>String To Extract</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SUBSTRING__STRING_TO_EXTRACT = eINSTANCE.getSubstring_StringToExtract();

        /**
         * The meta object literal for the '<em><b>Start Index</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SUBSTRING__START_INDEX = eINSTANCE.getSubstring_StartIndex();

        /**
         * The meta object literal for the '<em><b>End Index</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SUBSTRING__END_INDEX = eINSTANCE.getSubstring_EndIndex();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ParseNumericToStringImpl <em>Parse Numeric To String</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ParseNumericToStringImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getParseNumericToString()
         * @generated
         */
        EClass PARSE_NUMERIC_TO_STRING = eINSTANCE.getParseNumericToString();

        /**
         * The meta object literal for the '<em><b>Numeric Operand To Be Parsed</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED = eINSTANCE.getParseNumericToString_NumericOperandToBeParsed();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StringLengthImpl <em>String Length</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StringLengthImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringLength()
         * @generated
         */
        EClass STRING_LENGTH = eINSTANCE.getStringLength();

        /**
         * The meta object literal for the '<em><b>String Operand</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STRING_LENGTH__STRING_OPERAND = eINSTANCE.getStringLength_StringOperand();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractMessageImpl <em>Abstract Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractMessage()
         * @generated
         */
        EClass ABSTRACT_MESSAGE = eINSTANCE.getAbstractMessage();

        /**
         * The meta object literal for the '<em><b>Signals</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_MESSAGE__SIGNALS = eINSTANCE.getAbstractMessage_Signals();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_MESSAGE__NAME = eINSTANCE.getAbstractMessage_Name();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPMessageImpl <em>Some IP Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SomeIPMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMessage()
         * @generated
         */
        EClass SOME_IP_MESSAGE = eINSTANCE.getSomeIPMessage();

        /**
         * The meta object literal for the '<em><b>Some IP Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SOME_IP_MESSAGE__SOME_IP_FILTER = eINSTANCE.getSomeIPMessage_SomeIPFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl <em>Abstract Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractSignal()
         * @generated
         */
        EClass ABSTRACT_SIGNAL = eINSTANCE.getAbstractSignal();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_SIGNAL__NAME = eINSTANCE.getAbstractSignal_Name();

        /**
         * The meta object literal for the '<em><b>Decode Strategy</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SIGNAL__DECODE_STRATEGY = eINSTANCE.getAbstractSignal_DecodeStrategy();

        /**
         * The meta object literal for the '<em><b>Extract Strategy</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_SIGNAL__EXTRACT_STRATEGY = eINSTANCE.getAbstractSignal_ExtractStrategy();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ContainerSignalImpl <em>Container Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ContainerSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getContainerSignal()
         * @generated
         */
        EClass CONTAINER_SIGNAL = eINSTANCE.getContainerSignal();

        /**
         * The meta object literal for the '<em><b>Contained Signals</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CONTAINER_SIGNAL__CONTAINED_SIGNALS = eINSTANCE.getContainerSignal_ContainedSignals();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractFilterImpl <em>Abstract Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractFilter()
         * @generated
         */
        EClass ABSTRACT_FILTER = eINSTANCE.getAbstractFilter();

        /**
         * The meta object literal for the '<em><b>Payload Length</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_FILTER__PAYLOAD_LENGTH = eINSTANCE.getAbstractFilter_PayloadLength();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl <em>Ethernet Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetFilter()
         * @generated
         */
        EClass ETHERNET_FILTER = eINSTANCE.getEthernetFilter();

        /**
         * The meta object literal for the '<em><b>Source MAC</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__SOURCE_MAC = eINSTANCE.getEthernetFilter_SourceMAC();

        /**
         * The meta object literal for the '<em><b>Dest MAC</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__DEST_MAC = eINSTANCE.getEthernetFilter_DestMAC();

        /**
         * The meta object literal for the '<em><b>Ether Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__ETHER_TYPE = eINSTANCE.getEthernetFilter_EtherType();

        /**
         * The meta object literal for the '<em><b>Inner Vlan Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__INNER_VLAN_ID = eINSTANCE.getEthernetFilter_InnerVlanId();

        /**
         * The meta object literal for the '<em><b>Outer Vlan Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__OUTER_VLAN_ID = eINSTANCE.getEthernetFilter_OuterVlanId();

        /**
         * The meta object literal for the '<em><b>Inner Vlan CFI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__INNER_VLAN_CFI = eINSTANCE.getEthernetFilter_InnerVlanCFI();

        /**
         * The meta object literal for the '<em><b>Outer Vlan CFI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__OUTER_VLAN_CFI = eINSTANCE.getEthernetFilter_OuterVlanCFI();

        /**
         * The meta object literal for the '<em><b>Inner Vlan VID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__INNER_VLAN_VID = eINSTANCE.getEthernetFilter_InnerVlanVID();

        /**
         * The meta object literal for the '<em><b>Outer Vlan VID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__OUTER_VLAN_VID = eINSTANCE.getEthernetFilter_OuterVlanVID();

        /**
         * The meta object literal for the '<em><b>CRC</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__CRC = eINSTANCE.getEthernetFilter_CRC();

        /**
         * The meta object literal for the '<em><b>Inner Vlan TPID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__INNER_VLAN_TPID = eINSTANCE.getEthernetFilter_InnerVlanTPID();

        /**
         * The meta object literal for the '<em><b>Outer Vlan TPID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ETHERNET_FILTER__OUTER_VLAN_TPID = eINSTANCE.getEthernetFilter_OuterVlanTPID();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UDPFilterImpl <em>UDP Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UDPFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPFilter()
         * @generated
         */
        EClass UDP_FILTER = eINSTANCE.getUDPFilter();

        /**
         * The meta object literal for the '<em><b>Checksum</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UDP_FILTER__CHECKSUM = eINSTANCE.getUDPFilter_Checksum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl <em>TCP Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TCPFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTCPFilter()
         * @generated
         */
        EClass TCP_FILTER = eINSTANCE.getTCPFilter();

        /**
         * The meta object literal for the '<em><b>Sequence Number</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__SEQUENCE_NUMBER = eINSTANCE.getTCPFilter_SequenceNumber();

        /**
         * The meta object literal for the '<em><b>Acknowledgement Number</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__ACKNOWLEDGEMENT_NUMBER = eINSTANCE.getTCPFilter_AcknowledgementNumber();

        /**
         * The meta object literal for the '<em><b>Tcp Flags</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__TCP_FLAGS = eINSTANCE.getTCPFilter_TcpFlags();

        /**
         * The meta object literal for the '<em><b>Stream Analysis Flags</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__STREAM_ANALYSIS_FLAGS = eINSTANCE.getTCPFilter_StreamAnalysisFlags();

        /**
         * The meta object literal for the '<em><b>Stream Analysis Flags Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM = eINSTANCE.getTCPFilter_StreamAnalysisFlagsTmplParam();

        /**
         * The meta object literal for the '<em><b>Stream Valid Payload Offset</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET = eINSTANCE.getTCPFilter_StreamValidPayloadOffset();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl <em>IPv4 Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPv4Filter()
         * @generated
         */
        EClass IPV4_FILTER = eINSTANCE.getIPv4Filter();

        /**
         * The meta object literal for the '<em><b>Protocol Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IPV4_FILTER__PROTOCOL_TYPE = eINSTANCE.getIPv4Filter_ProtocolType();

        /**
         * The meta object literal for the '<em><b>Protocol Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM = eINSTANCE.getIPv4Filter_ProtocolTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Source IP</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IPV4_FILTER__SOURCE_IP = eINSTANCE.getIPv4Filter_SourceIP();

        /**
         * The meta object literal for the '<em><b>Dest IP</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IPV4_FILTER__DEST_IP = eINSTANCE.getIPv4Filter_DestIP();

        /**
         * The meta object literal for the '<em><b>Time To Live</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IPV4_FILTER__TIME_TO_LIVE = eINSTANCE.getIPv4Filter_TimeToLive();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl <em>Some IP Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SomeIPFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPFilter()
         * @generated
         */
        EClass SOME_IP_FILTER = eINSTANCE.getSomeIPFilter();

        /**
         * The meta object literal for the '<em><b>Service Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__SERVICE_ID = eINSTANCE.getSomeIPFilter_ServiceId();

        /**
         * The meta object literal for the '<em><b>Method Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__METHOD_TYPE = eINSTANCE.getSomeIPFilter_MethodType();

        /**
         * The meta object literal for the '<em><b>Method Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__METHOD_TYPE_TMPL_PARAM = eINSTANCE.getSomeIPFilter_MethodTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Method Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__METHOD_ID = eINSTANCE.getSomeIPFilter_MethodId();

        /**
         * The meta object literal for the '<em><b>Client Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__CLIENT_ID = eINSTANCE.getSomeIPFilter_ClientId();

        /**
         * The meta object literal for the '<em><b>Session Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__SESSION_ID = eINSTANCE.getSomeIPFilter_SessionId();

        /**
         * The meta object literal for the '<em><b>Protocol Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__PROTOCOL_VERSION = eINSTANCE.getSomeIPFilter_ProtocolVersion();

        /**
         * The meta object literal for the '<em><b>Interface Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__INTERFACE_VERSION = eINSTANCE.getSomeIPFilter_InterfaceVersion();

        /**
         * The meta object literal for the '<em><b>Message Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__MESSAGE_TYPE = eINSTANCE.getSomeIPFilter_MessageType();

        /**
         * The meta object literal for the '<em><b>Message Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__MESSAGE_TYPE_TMPL_PARAM = eINSTANCE.getSomeIPFilter_MessageTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Return Code</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__RETURN_CODE = eINSTANCE.getSomeIPFilter_ReturnCode();

        /**
         * The meta object literal for the '<em><b>Return Code Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__RETURN_CODE_TMPL_PARAM = eINSTANCE.getSomeIPFilter_ReturnCodeTmplParam();

        /**
         * The meta object literal for the '<em><b>Some IP Length</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IP_FILTER__SOME_IP_LENGTH = eINSTANCE.getSomeIPFilter_SomeIPLength();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ForEachExpressionImpl <em>For Each Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ForEachExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getForEachExpression()
         * @generated
         */
        EClass FOR_EACH_EXPRESSION = eINSTANCE.getForEachExpression();

        /**
         * The meta object literal for the '<em><b>Filter Expressions</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference FOR_EACH_EXPRESSION__FILTER_EXPRESSIONS = eINSTANCE.getForEachExpression_FilterExpressions();

        /**
         * The meta object literal for the '<em><b>Container Signal</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference FOR_EACH_EXPRESSION__CONTAINER_SIGNAL = eINSTANCE.getForEachExpression_ContainerSignal();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.MessageCheckExpressionImpl <em>Message Check Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.MessageCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMessageCheckExpression()
         * @generated
         */
        EClass MESSAGE_CHECK_EXPRESSION = eINSTANCE.getMessageCheckExpression();

        /**
         * The meta object literal for the '<em><b>Message</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference MESSAGE_CHECK_EXPRESSION__MESSAGE = eINSTANCE.getMessageCheckExpression_Message();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPSDFilterImpl <em>Some IPSD Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SomeIPSDFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDFilter()
         * @generated
         */
        EClass SOME_IPSD_FILTER = eINSTANCE.getSomeIPSDFilter();

        /**
         * The meta object literal for the '<em><b>Flags</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__FLAGS = eINSTANCE.getSomeIPSDFilter_Flags();

        /**
         * The meta object literal for the '<em><b>Flags Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__FLAGS_TMPL_PARAM = eINSTANCE.getSomeIPSDFilter_FlagsTmplParam();

        /**
         * The meta object literal for the '<em><b>Sd Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__SD_TYPE = eINSTANCE.getSomeIPSDFilter_SdType();

        /**
         * The meta object literal for the '<em><b>Sd Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM = eINSTANCE.getSomeIPSDFilter_SdTypeTmplParam();

        /**
         * The meta object literal for the '<em><b>Instance Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__INSTANCE_ID = eINSTANCE.getSomeIPSDFilter_InstanceId();

        /**
         * The meta object literal for the '<em><b>Ttl</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__TTL = eINSTANCE.getSomeIPSDFilter_Ttl();

        /**
         * The meta object literal for the '<em><b>Major Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__MAJOR_VERSION = eINSTANCE.getSomeIPSDFilter_MajorVersion();

        /**
         * The meta object literal for the '<em><b>Minor Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__MINOR_VERSION = eINSTANCE.getSomeIPSDFilter_MinorVersion();

        /**
         * The meta object literal for the '<em><b>Event Group Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__EVENT_GROUP_ID = eINSTANCE.getSomeIPSDFilter_EventGroupId();

        /**
         * The meta object literal for the '<em><b>Index First Option</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__INDEX_FIRST_OPTION = eINSTANCE.getSomeIPSDFilter_IndexFirstOption();

        /**
         * The meta object literal for the '<em><b>Index Second Option</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__INDEX_SECOND_OPTION = eINSTANCE.getSomeIPSDFilter_IndexSecondOption();

        /**
         * The meta object literal for the '<em><b>Number First Option</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__NUMBER_FIRST_OPTION = eINSTANCE.getSomeIPSDFilter_NumberFirstOption();

        /**
         * The meta object literal for the '<em><b>Number Second Option</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__NUMBER_SECOND_OPTION = eINSTANCE.getSomeIPSDFilter_NumberSecondOption();

        /**
         * The meta object literal for the '<em><b>Service Id Some IPSD</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD = eINSTANCE.getSomeIPSDFilter_ServiceId_SomeIPSD();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SomeIPSDMessageImpl <em>Some IPSD Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SomeIPSDMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDMessage()
         * @generated
         */
        EClass SOME_IPSD_MESSAGE = eINSTANCE.getSomeIPSDMessage();

        /**
         * The meta object literal for the '<em><b>Some IPSD Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference SOME_IPSD_MESSAGE__SOME_IPSD_FILTER = eINSTANCE.getSomeIPSDMessage_SomeIPSDFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl <em>Double Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleSignal()
         * @generated
         */
        EClass DOUBLE_SIGNAL = eINSTANCE.getDoubleSignal();

        /**
         * The meta object literal for the '<em><b>Ignore Invalid Values</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOUBLE_SIGNAL__IGNORE_INVALID_VALUES = eINSTANCE.getDoubleSignal_IgnoreInvalidValues();

        /**
         * The meta object literal for the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM = eINSTANCE.getDoubleSignal_IgnoreInvalidValuesTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StringSignalImpl <em>String Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StringSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringSignal()
         * @generated
         */
        EClass STRING_SIGNAL = eINSTANCE.getStringSignal();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DecodeStrategyImpl <em>Decode Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DecodeStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDecodeStrategy()
         * @generated
         */
        EClass DECODE_STRATEGY = eINSTANCE.getDecodeStrategy();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl <em>Double Decode Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DoubleDecodeStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleDecodeStrategy()
         * @generated
         */
        EClass DOUBLE_DECODE_STRATEGY = eINSTANCE.getDoubleDecodeStrategy();

        /**
         * The meta object literal for the '<em><b>Factor</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOUBLE_DECODE_STRATEGY__FACTOR = eINSTANCE.getDoubleDecodeStrategy_Factor();

        /**
         * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DOUBLE_DECODE_STRATEGY__OFFSET = eINSTANCE.getDoubleDecodeStrategy_Offset();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ExtractStrategyImpl <em>Extract Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getExtractStrategy()
         * @generated
         */
        EClass EXTRACT_STRATEGY = eINSTANCE.getExtractStrategy();

        /**
         * The meta object literal for the '<em><b>Abstract Signal</b></em>' container reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference EXTRACT_STRATEGY__ABSTRACT_SIGNAL = eINSTANCE.getExtractStrategy_AbstractSignal();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl <em>Universal Payload Extract
         * Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UniversalPayloadExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUniversalPayloadExtractStrategy()
         * @generated
         */
        EClass UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY = eINSTANCE.getUniversalPayloadExtractStrategy();

        /**
         * The meta object literal for the '<em><b>Extraction Rule</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE = eINSTANCE.getUniversalPayloadExtractStrategy_ExtractionRule();

        /**
         * The meta object literal for the '<em><b>Byte Order</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER = eINSTANCE.getUniversalPayloadExtractStrategy_ByteOrder();

        /**
         * The meta object literal for the '<em><b>Extraction Rule Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM = eINSTANCE.getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam();

        /**
         * The meta object literal for the '<em><b>Signal Data Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE = eINSTANCE.getUniversalPayloadExtractStrategy_SignalDataType();

        /**
         * The meta object literal for the '<em><b>Signal Data Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM = eINSTANCE.getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.EmptyExtractStrategyImpl <em>Empty Extract Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.EmptyExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEmptyExtractStrategy()
         * @generated
         */
        EClass EMPTY_EXTRACT_STRATEGY = eINSTANCE.getEmptyExtractStrategy();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.CANFilterImpl <em>CAN Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.CANFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANFilter()
         * @generated
         */
        EClass CAN_FILTER = eINSTANCE.getCANFilter();

        /**
         * The meta object literal for the '<em><b>Message Id Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__MESSAGE_ID_RANGE = eINSTANCE.getCANFilter_MessageIdRange();

        /**
         * The meta object literal for the '<em><b>Frame Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__FRAME_ID = eINSTANCE.getCANFilter_FrameId();

        /**
         * The meta object literal for the '<em><b>Rxtx Flag</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__RXTX_FLAG = eINSTANCE.getCANFilter_RxtxFlag();

        /**
         * The meta object literal for the '<em><b>Rxtx Flag Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__RXTX_FLAG_TMPL_PARAM = eINSTANCE.getCANFilter_RxtxFlagTmplParam();

        /**
         * The meta object literal for the '<em><b>Ext Identifier</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__EXT_IDENTIFIER = eINSTANCE.getCANFilter_ExtIdentifier();

        /**
         * The meta object literal for the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM = eINSTANCE.getCANFilter_ExtIdentifierTmplParam();

        /**
         * The meta object literal for the '<em><b>Frame Type</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__FRAME_TYPE = eINSTANCE.getCANFilter_FrameType();

        /**
         * The meta object literal for the '<em><b>Frame Type Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CAN_FILTER__FRAME_TYPE_TMPL_PARAM = eINSTANCE.getCANFilter_FrameTypeTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.LINFilterImpl <em>LIN Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.LINFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLINFilter()
         * @generated
         */
        EClass LIN_FILTER = eINSTANCE.getLINFilter();

        /**
         * The meta object literal for the '<em><b>Message Id Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_FILTER__MESSAGE_ID_RANGE = eINSTANCE.getLINFilter_MessageIdRange();

        /**
         * The meta object literal for the '<em><b>Frame Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIN_FILTER__FRAME_ID = eINSTANCE.getLINFilter_FrameId();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl <em>Flex Ray Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayFilter()
         * @generated
         */
        EClass FLEX_RAY_FILTER = eINSTANCE.getFlexRayFilter();

        /**
         * The meta object literal for the '<em><b>Message Id Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__MESSAGE_ID_RANGE = eINSTANCE.getFlexRayFilter_MessageIdRange();

        /**
         * The meta object literal for the '<em><b>Slot Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__SLOT_ID = eINSTANCE.getFlexRayFilter_SlotId();

        /**
         * The meta object literal for the '<em><b>Cycle Offset</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__CYCLE_OFFSET = eINSTANCE.getFlexRayFilter_CycleOffset();

        /**
         * The meta object literal for the '<em><b>Cycle Repetition</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__CYCLE_REPETITION = eINSTANCE.getFlexRayFilter_CycleRepetition();

        /**
         * The meta object literal for the '<em><b>Channel</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__CHANNEL = eINSTANCE.getFlexRayFilter_Channel();

        /**
         * The meta object literal for the '<em><b>Channel Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM = eINSTANCE.getFlexRayFilter_ChannelTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DLTFilterImpl <em>DLT Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DLTFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLTFilter()
         * @generated
         */
        EClass DLT_FILTER = eINSTANCE.getDLTFilter();

        /**
         * The meta object literal for the '<em><b>Ecu ID ECU</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__ECU_ID_ECU = eINSTANCE.getDLTFilter_EcuID_ECU();

        /**
         * The meta object literal for the '<em><b>Session ID SEID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__SESSION_ID_SEID = eINSTANCE.getDLTFilter_SessionID_SEID();

        /**
         * The meta object literal for the '<em><b>Application ID APID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__APPLICATION_ID_APID = eINSTANCE.getDLTFilter_ApplicationID_APID();

        /**
         * The meta object literal for the '<em><b>Context ID CTID</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__CONTEXT_ID_CTID = eINSTANCE.getDLTFilter_ContextID_CTID();

        /**
         * The meta object literal for the '<em><b>Message Type MSTP</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_TYPE_MSTP = eINSTANCE.getDLTFilter_MessageType_MSTP();

        /**
         * The meta object literal for the '<em><b>Message Log Info MSLI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_LOG_INFO_MSLI = eINSTANCE.getDLTFilter_MessageLogInfo_MSLI();

        /**
         * The meta object literal for the '<em><b>Message Trace Info MSTI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_TRACE_INFO_MSTI = eINSTANCE.getDLTFilter_MessageTraceInfo_MSTI();

        /**
         * The meta object literal for the '<em><b>Message Bus Info MSBI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_BUS_INFO_MSBI = eINSTANCE.getDLTFilter_MessageBusInfo_MSBI();

        /**
         * The meta object literal for the '<em><b>Message Control Info MSCI</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI = eINSTANCE.getDLTFilter_MessageControlInfo_MSCI();

        /**
         * The meta object literal for the '<em><b>Message Log Info MSLI Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM = eINSTANCE.getDLTFilter_MessageLogInfo_MSLITmplParam();

        /**
         * The meta object literal for the '<em><b>Message Trace Info MSTI Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM = eINSTANCE.getDLTFilter_MessageTraceInfo_MSTITmplParam();

        /**
         * The meta object literal for the '<em><b>Message Bus Info MSBI Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM = eINSTANCE.getDLTFilter_MessageBusInfo_MSBITmplParam();

        /**
         * The meta object literal for the '<em><b>Message Control Info MSCI Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM = eINSTANCE.getDLTFilter_MessageControlInfo_MSCITmplParam();

        /**
         * The meta object literal for the '<em><b>Message Type MSTP Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM = eINSTANCE.getDLTFilter_MessageType_MSTPTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UDPNMFilterImpl <em>UDPNM Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UDPNMFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPNMFilter()
         * @generated
         */
        EClass UDPNM_FILTER = eINSTANCE.getUDPNMFilter();

        /**
         * The meta object literal for the '<em><b>Source Node Identifier</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UDPNM_FILTER__SOURCE_NODE_IDENTIFIER = eINSTANCE.getUDPNMFilter_SourceNodeIdentifier();

        /**
         * The meta object literal for the '<em><b>Control Bit Vector</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UDPNM_FILTER__CONTROL_BIT_VECTOR = eINSTANCE.getUDPNMFilter_ControlBitVector();

        /**
         * The meta object literal for the '<em><b>Pwf Status</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UDPNM_FILTER__PWF_STATUS = eINSTANCE.getUDPNMFilter_PwfStatus();

        /**
         * The meta object literal for the '<em><b>Teilnetz Status</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UDPNM_FILTER__TEILNETZ_STATUS = eINSTANCE.getUDPNMFilter_TeilnetzStatus();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.CANMessageImpl <em>CAN Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.CANMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANMessage()
         * @generated
         */
        EClass CAN_MESSAGE = eINSTANCE.getCANMessage();

        /**
         * The meta object literal for the '<em><b>Can Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference CAN_MESSAGE__CAN_FILTER = eINSTANCE.getCANMessage_CanFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.LINMessageImpl <em>LIN Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.LINMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLINMessage()
         * @generated
         */
        EClass LIN_MESSAGE = eINSTANCE.getLINMessage();

        /**
         * The meta object literal for the '<em><b>Lin Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LIN_MESSAGE__LIN_FILTER = eINSTANCE.getLINMessage_LinFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageImpl <em>Flex Ray Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.FlexRayMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayMessage()
         * @generated
         */
        EClass FLEX_RAY_MESSAGE = eINSTANCE.getFlexRayMessage();

        /**
         * The meta object literal for the '<em><b>Flex Ray Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference FLEX_RAY_MESSAGE__FLEX_RAY_FILTER = eINSTANCE.getFlexRayMessage_FlexRayFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.DLTMessageImpl <em>DLT Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.DLTMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLTMessage()
         * @generated
         */
        EClass DLT_MESSAGE = eINSTANCE.getDLTMessage();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UDPMessageImpl <em>UDP Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UDPMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPMessage()
         * @generated
         */
        EClass UDP_MESSAGE = eINSTANCE.getUDPMessage();

        /**
         * The meta object literal for the '<em><b>Udp Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference UDP_MESSAGE__UDP_FILTER = eINSTANCE.getUDPMessage_UdpFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TCPMessageImpl <em>TCP Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TCPMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTCPMessage()
         * @generated
         */
        EClass TCP_MESSAGE = eINSTANCE.getTCPMessage();

        /**
         * The meta object literal for the '<em><b>Tcp Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference TCP_MESSAGE__TCP_FILTER = eINSTANCE.getTCPMessage_TcpFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UDPNMMessageImpl <em>UDPNM Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UDPNMMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUDPNMMessage()
         * @generated
         */
        EClass UDPNM_MESSAGE = eINSTANCE.getUDPNMMessage();

        /**
         * The meta object literal for the '<em><b>Udpnm Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference UDPNM_MESSAGE__UDPNM_FILTER = eINSTANCE.getUDPNMMessage_UdpnmFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTMessageImpl <em>Verbose DLT Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTMessage()
         * @generated
         */
        EClass VERBOSE_DLT_MESSAGE = eINSTANCE.getVerboseDLTMessage();

        /**
         * The meta object literal for the '<em><b>Dlt Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference VERBOSE_DLT_MESSAGE__DLT_FILTER = eINSTANCE.getVerboseDLTMessage_DltFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl <em>Universal Payload
         * With Legacy Extract Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.UniversalPayloadWithLegacyExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getUniversalPayloadWithLegacyExtractStrategy()
         * @generated
         */
        EClass UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY = eINSTANCE.getUniversalPayloadWithLegacyExtractStrategy();

        /**
         * The meta object literal for the '<em><b>Start Bit</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__START_BIT = eINSTANCE.getUniversalPayloadWithLegacyExtractStrategy_StartBit();

        /**
         * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute UNIVERSAL_PAYLOAD_WITH_LEGACY_EXTRACT_STRATEGY__LENGTH = eINSTANCE.getUniversalPayloadWithLegacyExtractStrategy_Length();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl <em>Abstract Bus Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractBusMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractBusMessage()
         * @generated
         */
        EClass ABSTRACT_BUS_MESSAGE = eINSTANCE.getAbstractBusMessage();

        /**
         * The meta object literal for the '<em><b>Bus Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_BUS_MESSAGE__BUS_ID = eINSTANCE.getAbstractBusMessage_BusId();

        /**
         * The meta object literal for the '<em><b>Bus Id Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM = eINSTANCE.getAbstractBusMessage_BusIdTmplParam();

        /**
         * The meta object literal for the '<em><b>Bus Id Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE = eINSTANCE.getAbstractBusMessage_BusIdRange();

        /**
         * The meta object literal for the '<em><b>Filters</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ABSTRACT_BUS_MESSAGE__FILTERS = eINSTANCE.getAbstractBusMessage_Filters();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginFilterImpl <em>Plugin Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginFilter()
         * @generated
         */
        EClass PLUGIN_FILTER = eINSTANCE.getPluginFilter();

        /**
         * The meta object literal for the '<em><b>Plugin Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_FILTER__PLUGIN_NAME = eINSTANCE.getPluginFilter_PluginName();

        /**
         * The meta object literal for the '<em><b>Plugin Version</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_FILTER__PLUGIN_VERSION = eINSTANCE.getPluginFilter_PluginVersion();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginMessageImpl <em>Plugin Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginMessage()
         * @generated
         */
        EClass PLUGIN_MESSAGE = eINSTANCE.getPluginMessage();

        /**
         * The meta object literal for the '<em><b>Plugin Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference PLUGIN_MESSAGE__PLUGIN_FILTER = eINSTANCE.getPluginMessage_PluginFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginSignalImpl <em>Plugin Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginSignal()
         * @generated
         */
        EClass PLUGIN_SIGNAL = eINSTANCE.getPluginSignal();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl <em>Plugin State Extract Strategy</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginStateExtractStrategy()
         * @generated
         */
        EClass PLUGIN_STATE_EXTRACT_STRATEGY = eINSTANCE.getPluginStateExtractStrategy();

        /**
         * The meta object literal for the '<em><b>States</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_STATE_EXTRACT_STRATEGY__STATES = eINSTANCE.getPluginStateExtractStrategy_States();

        /**
         * The meta object literal for the '<em><b>States Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM = eINSTANCE.getPluginStateExtractStrategy_StatesTmplParam();

        /**
         * The meta object literal for the '<em><b>States Active</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE = eINSTANCE.getPluginStateExtractStrategy_StatesActive();

        /**
         * The meta object literal for the '<em><b>States Active Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM = eINSTANCE.getPluginStateExtractStrategy_StatesActiveTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginResultExtractStrategyImpl <em>Plugin Result Extract
         * Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginResultExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginResultExtractStrategy()
         * @generated
         */
        EClass PLUGIN_RESULT_EXTRACT_STRATEGY = eINSTANCE.getPluginResultExtractStrategy();

        /**
         * The meta object literal for the '<em><b>Result Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE = eINSTANCE.getPluginResultExtractStrategy_ResultRange();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.EmptyDecodeStrategyImpl <em>Empty Decode Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.EmptyDecodeStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEmptyDecodeStrategy()
         * @generated
         */
        EClass EMPTY_DECODE_STRATEGY = eINSTANCE.getEmptyDecodeStrategy();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl <em>Plugin Check Expression</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginCheckExpression()
         * @generated
         */
        EClass PLUGIN_CHECK_EXPRESSION = eINSTANCE.getPluginCheckExpression();

        /**
         * The meta object literal for the '<em><b>Evaluation Behaviour</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR = eINSTANCE.getPluginCheckExpression_EvaluationBehaviour();

        /**
         * The meta object literal for the '<em><b>Signal To Check</b></em>' reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK = eINSTANCE.getPluginCheckExpression_SignalToCheck();

        /**
         * The meta object literal for the '<em><b>Evaluation Behaviour Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM = eINSTANCE.getPluginCheckExpression_EvaluationBehaviourTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl <em>Base Class With ID</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBaseClassWithID()
         * @generated
         */
        EClass BASE_CLASS_WITH_ID = eINSTANCE.getBaseClassWithID();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute BASE_CLASS_WITH_ID__ID = eINSTANCE.getBaseClassWithID_Id();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl <em>Header Signal</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.HeaderSignalImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHeaderSignal()
         * @generated
         */
        EClass HEADER_SIGNAL = eINSTANCE.getHeaderSignal();

        /**
         * The meta object literal for the '<em><b>Attribute</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute HEADER_SIGNAL__ATTRIBUTE = eINSTANCE.getHeaderSignal_Attribute();

        /**
         * The meta object literal for the '<em><b>Attribute Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute HEADER_SIGNAL__ATTRIBUTE_TMPL_PARAM = eINSTANCE.getHeaderSignal_AttributeTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.IVariableReaderWriter
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIVariableReaderWriter()
         * @generated
         */
        EClass IVARIABLE_READER_WRITER = eINSTANCE.getIVariableReaderWriter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.IStateTransitionReferenceImpl <em>IState Transition Reference</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.IStateTransitionReferenceImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIStateTransitionReference()
         * @generated
         */
        EClass ISTATE_TRANSITION_REFERENCE = eINSTANCE.getIStateTransitionReference();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.TPFilterImpl <em>TP Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.TPFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTPFilter()
         * @generated
         */
        EClass TP_FILTER = eINSTANCE.getTPFilter();

        /**
         * The meta object literal for the '<em><b>Source Port</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TP_FILTER__SOURCE_PORT = eINSTANCE.getTPFilter_SourcePort();

        /**
         * The meta object literal for the '<em><b>Dest Port</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute TP_FILTER__DEST_PORT = eINSTANCE.getTPFilter_DestPort();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.BaseClassWithSourceReferenceImpl <em>Base Class With Source
         * Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.BaseClassWithSourceReferenceImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBaseClassWithSourceReference()
         * @generated
         */
        EClass BASE_CLASS_WITH_SOURCE_REFERENCE = eINSTANCE.getBaseClassWithSourceReference();

        /**
         * The meta object literal for the '<em><b>Source Reference</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE = eINSTANCE.getBaseClassWithSourceReference_SourceReference();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.SourceReferenceImpl <em>Source Reference</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.SourceReferenceImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSourceReference()
         * @generated
         */
        EClass SOURCE_REFERENCE = eINSTANCE.getSourceReference();

        /**
         * The meta object literal for the '<em><b>Serialized Reference</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute SOURCE_REFERENCE__SERIALIZED_REFERENCE = eINSTANCE.getSourceReference_SerializedReference();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.EthernetMessageImpl <em>Ethernet Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.EthernetMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetMessage()
         * @generated
         */
        EClass ETHERNET_MESSAGE = eINSTANCE.getEthernetMessage();

        /**
         * The meta object literal for the '<em><b>Ethernet Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference ETHERNET_MESSAGE__ETHERNET_FILTER = eINSTANCE.getEthernetMessage_EthernetFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.IPv4MessageImpl <em>IPv4 Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.IPv4MessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPv4Message()
         * @generated
         */
        EClass IPV4_MESSAGE = eINSTANCE.getIPv4Message();

        /**
         * The meta object literal for the '<em><b>IPv4 Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference IPV4_MESSAGE__IPV4_FILTER = eINSTANCE.getIPv4Message_IPv4Filter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTMessageImpl <em>Non Verbose DLT Message</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.NonVerboseDLTMessageImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonVerboseDLTMessage()
         * @generated
         */
        EClass NON_VERBOSE_DLT_MESSAGE = eINSTANCE.getNonVerboseDLTMessage();

        /**
         * The meta object literal for the '<em><b>Non Verbose Dlt Filter</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference NON_VERBOSE_DLT_MESSAGE__NON_VERBOSE_DLT_FILTER = eINSTANCE.getNonVerboseDLTMessage_NonVerboseDltFilter();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StringDecodeStrategyImpl <em>String Decode Strategy</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StringDecodeStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStringDecodeStrategy()
         * @generated
         */
        EClass STRING_DECODE_STRATEGY = eINSTANCE.getStringDecodeStrategy();

        /**
         * The meta object literal for the '<em><b>String Termination</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STRING_DECODE_STRATEGY__STRING_TERMINATION = eINSTANCE.getStringDecodeStrategy_StringTermination();

        /**
         * The meta object literal for the '<em><b>String Termination Tmpl Param</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STRING_DECODE_STRATEGY__STRING_TERMINATION_TMPL_PARAM = eINSTANCE.getStringDecodeStrategy_StringTerminationTmplParam();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl <em>Non Verbose DLT Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonVerboseDLTFilter()
         * @generated
         */
        EClass NON_VERBOSE_DLT_FILTER = eINSTANCE.getNonVerboseDLTFilter();

        /**
         * The meta object literal for the '<em><b>Message Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute NON_VERBOSE_DLT_FILTER__MESSAGE_ID = eINSTANCE.getNonVerboseDLTFilter_MessageId();

        /**
         * The meta object literal for the '<em><b>Message Id Range</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE = eINSTANCE.getNonVerboseDLTFilter_MessageIdRange();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTExtractStrategyImpl <em>Verbose DLT Extract Strategy</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTExtractStrategyImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTExtractStrategy()
         * @generated
         */
        EClass VERBOSE_DLT_EXTRACT_STRATEGY = eINSTANCE.getVerboseDLTExtractStrategy();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.RegexOperation <em>Regex Operation</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.RegexOperation
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getRegexOperation()
         * @generated
         */
        EClass REGEX_OPERATION = eINSTANCE.getRegexOperation();

        /**
         * The meta object literal for the '<em><b>Regex</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute REGEX_OPERATION__REGEX = eINSTANCE.getRegexOperation_Regex();

        /**
         * The meta object literal for the '<em><b>Dynamic Regex</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference REGEX_OPERATION__DYNAMIC_REGEX = eINSTANCE.getRegexOperation_DynamicRegex();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl <em>Payload Filter</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPayloadFilter()
         * @generated
         */
        EClass PAYLOAD_FILTER = eINSTANCE.getPayloadFilter();

        /**
         * The meta object literal for the '<em><b>Index</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PAYLOAD_FILTER__INDEX = eINSTANCE.getPayloadFilter_Index();

        /**
         * The meta object literal for the '<em><b>Mask</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PAYLOAD_FILTER__MASK = eINSTANCE.getPayloadFilter_Mask();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute PAYLOAD_FILTER__VALUE = eINSTANCE.getPayloadFilter_Value();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl <em>Verbose DLT Payload Filter</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVerboseDLTPayloadFilter()
         * @generated
         */
        EClass VERBOSE_DLT_PAYLOAD_FILTER = eINSTANCE.getVerboseDLTPayloadFilter();

        /**
         * The meta object literal for the '<em><b>Regex</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VERBOSE_DLT_PAYLOAD_FILTER__REGEX = eINSTANCE.getVerboseDLTPayloadFilter_Regex();

        /**
         * The meta object literal for the '<em><b>Contains Any</b></em>' attribute list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY = eINSTANCE.getVerboseDLTPayloadFilter_ContainsAny();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.AbstractVariableImpl <em>Abstract Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.AbstractVariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAbstractVariable()
         * @generated
         */
        EClass ABSTRACT_VARIABLE = eINSTANCE.getAbstractVariable();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_VARIABLE__NAME = eINSTANCE.getAbstractVariable_Name();

        /**
         * The meta object literal for the '<em><b>Display Name</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_VARIABLE__DISPLAY_NAME = eINSTANCE.getAbstractVariable_DisplayName();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ABSTRACT_VARIABLE__DESCRIPTION = eINSTANCE.getAbstractVariable_Description();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.StructureVariableImpl <em>Structure Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.StructureVariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStructureVariable()
         * @generated
         */
        EClass STRUCTURE_VARIABLE = eINSTANCE.getStructureVariable();

        /**
         * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STRUCTURE_VARIABLE__VARIABLES = eINSTANCE.getStructureVariable_Variables();

        /**
         * The meta object literal for the '<em><b>Variable References</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STRUCTURE_VARIABLE__VARIABLE_REFERENCES = eINSTANCE.getStructureVariable_VariableReferences();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl <em>Computed Variable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.impl.ComputedVariableImpl
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComputedVariable()
         * @generated
         */
        EClass COMPUTED_VARIABLE = eINSTANCE.getComputedVariable();

        /**
         * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMPUTED_VARIABLE__OPERANDS = eINSTANCE.getComputedVariable_Operands();

        /**
         * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COMPUTED_VARIABLE__EXPRESSION = eINSTANCE.getComputedVariable_Expression();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.CheckType <em>Check Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.CheckType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCheckType()
         * @generated
         */
        EEnum CHECK_TYPE = eINSTANCE.getCheckType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.Comparator <em>Comparator</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.Comparator
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparator()
         * @generated
         */
        EEnum COMPARATOR = eINSTANCE.getComparator();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.LogicalOperatorType <em>Logical Operator Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalOperatorType()
         * @generated
         */
        EEnum LOGICAL_OPERATOR_TYPE = eINSTANCE.getLogicalOperatorType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SignalDataType <em>Signal Data Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SignalDataType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalDataType()
         * @generated
         */
        EEnum SIGNAL_DATA_TYPE = eINSTANCE.getSignalDataType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.FlexChannelType <em>Flex Channel Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.FlexChannelType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexChannelType()
         * @generated
         */
        EEnum FLEX_CHANNEL_TYPE = eINSTANCE.getFlexChannelType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ByteOrderType <em>Byte Order Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ByteOrderType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getByteOrderType()
         * @generated
         */
        EEnum BYTE_ORDER_TYPE = eINSTANCE.getByteOrderType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.FormatDataType <em>Format Data Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.FormatDataType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFormatDataType()
         * @generated
         */
        EEnum FORMAT_DATA_TYPE = eINSTANCE.getFormatDataType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection <em>Flex Ray Header Flag Selection</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getFlexRayHeaderFlagSelection()
         * @generated
         */
        EEnum FLEX_RAY_HEADER_FLAG_SELECTION = eINSTANCE.getFlexRayHeaderFlagSelection();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum <em>Boolean Or Template Placeholder
         * Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBooleanOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum BOOLEAN_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getBooleanOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum <em>Protocol Type Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getProtocolTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum PROTOCOL_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getProtocolTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection <em>Ethernet Protocol Type Selection</em>}'
         * enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.EthernetProtocolTypeSelection
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEthernetProtocolTypeSelection()
         * @generated
         */
        EEnum ETHERNET_PROTOCOL_TYPE_SELECTION = eINSTANCE.getEthernetProtocolTypeSelection();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum <em>Rx Tx Flag Type Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getRxTxFlagTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum RX_TX_FLAG_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getRxTxFlagTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ObserverValueType <em>Observer Value Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ObserverValueType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getObserverValueType()
         * @generated
         */
        EEnum OBSERVER_VALUE_TYPE = eINSTANCE.getObserverValueType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum <em>Plugin State Value Type Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.PluginStateValueTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPluginStateValueTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum PLUGIN_STATE_VALUE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getPluginStateValueTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum <em>Analysis Type Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAnalysisTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum ANALYSIS_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getAnalysisTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum <em>Some IP Message Type Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPMessageTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMessageTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IP_MESSAGE_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPMessageTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum <em>Some IP Method Type Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPMethodTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPMethodTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IP_METHOD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPMethodTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum <em>Some IP Return Code Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPReturnCodeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPReturnCodeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IP_RETURN_CODE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPReturnCodeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum <em>Some IPSD Flags Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPSDFlagsOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDFlagsOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IPSD_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPSDFlagsOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum <em>Some IPSD Type Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IPSD_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPSDTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum <em>Stream Analysis Flags Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getStreamAnalysisFlagsOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum STREAM_ANALYSIS_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getStreamAnalysisFlagsOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum <em>Some IPSD Entry Flags Or
         * Template Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum SOME_IPSD_ENTRY_FLAGS_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum <em>Can Ext Identifier Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCanExtIdentifierOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum CAN_EXT_IDENTIFIER_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getCanExtIdentifierOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DataType <em>Data Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DataType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDataType()
         * @generated
         */
        EEnum DATA_TYPE = eINSTANCE.getDataType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageType <em>DLT Message Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DLT_MessageType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageType()
         * @generated
         */
        EEnum DLT_MESSAGE_TYPE = eINSTANCE.getDLT_MessageType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo <em>DLT Message Trace Info</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DLT_MessageTraceInfo
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageTraceInfo()
         * @generated
         */
        EEnum DLT_MESSAGE_TRACE_INFO = eINSTANCE.getDLT_MessageTraceInfo();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageLogInfo <em>DLT Message Log Info</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DLT_MessageLogInfo
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageLogInfo()
         * @generated
         */
        EEnum DLT_MESSAGE_LOG_INFO = eINSTANCE.getDLT_MessageLogInfo();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageControlInfo <em>DLT Message Control Info</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DLT_MessageControlInfo
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageControlInfo()
         * @generated
         */
        EEnum DLT_MESSAGE_CONTROL_INFO = eINSTANCE.getDLT_MessageControlInfo();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.DLT_MessageBusInfo <em>DLT Message Bus Info</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.DLT_MessageBusInfo
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDLT_MessageBusInfo()
         * @generated
         */
        EEnum DLT_MESSAGE_BUS_INFO = eINSTANCE.getDLT_MessageBusInfo();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate <em>For Each Expression Modifier
         * Template</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.ForEachExpressionModifierTemplate
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getForEachExpressionModifierTemplate()
         * @generated
         */
        EEnum FOR_EACH_EXPRESSION_MODIFIER_TEMPLATE = eINSTANCE.getForEachExpressionModifierTemplate();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.AUTOSARDataType <em>AUTOSAR Data Type</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.AUTOSARDataType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getAUTOSARDataType()
         * @generated
         */
        EEnum AUTOSAR_DATA_TYPE = eINSTANCE.getAUTOSARDataType();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum <em>CAN Frame Type Or Template
         * Placeholder Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCANFrameTypeOrTemplatePlaceholderEnum()
         * @generated
         */
        EEnum CAN_FRAME_TYPE_OR_TEMPLATE_PLACEHOLDER_ENUM = eINSTANCE.getCANFrameTypeOrTemplatePlaceholderEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum <em>Evaluation Behaviour Or
         * Template Defined Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getEvaluationBehaviourOrTemplateDefinedEnum()
         * @generated
         */
        EEnum EVALUATION_BEHAVIOUR_OR_TEMPLATE_DEFINED_ENUM = eINSTANCE.getEvaluationBehaviourOrTemplateDefinedEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.SignalVariableLagEnum <em>Signal Variable Lag Enum</em>}' enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.SignalVariableLagEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getSignalVariableLagEnum()
         * @generated
         */
        EEnum SIGNAL_VARIABLE_LAG_ENUM = eINSTANCE.getSignalVariableLagEnum();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum <em>TTL Or Template Place Holder Enum</em>}'
         * enum.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getTTLOrTemplatePlaceHolderEnum()
         * @generated
         */
        EEnum TTL_OR_TEMPLATE_PLACE_HOLDER_ENUM = eINSTANCE.getTTLOrTemplatePlaceHolderEnum();

        /**
         * The meta object literal for the '<em>Check Type Object</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.CheckType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getCheckTypeObject()
         * @generated
         */
        EDataType CHECK_TYPE_OBJECT = eINSTANCE.getCheckTypeObject();

        /**
         * The meta object literal for the '<em>Comparator Object</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.Comparator
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getComparatorObject()
         * @generated
         */
        EDataType COMPARATOR_OBJECT = eINSTANCE.getComparatorObject();

        /**
         * The meta object literal for the '<em>Logical Operator Type Object</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLogicalOperatorTypeObject()
         * @generated
         */
        EDataType LOGICAL_OPERATOR_TYPE_OBJECT = eINSTANCE.getLogicalOperatorTypeObject();

        /**
         * The meta object literal for the '<em>Hex Or Int Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHexOrIntOrTemplatePlaceholder()
         * @generated
         */
        EDataType HEX_OR_INT_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getHexOrIntOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Int Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIntOrTemplatePlaceholder()
         * @generated
         */
        EDataType INT_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getIntOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Long Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getLongOrTemplatePlaceholder()
         * @generated
         */
        EDataType LONG_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getLongOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Double Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleOrTemplatePlaceholder()
         * @generated
         */
        EDataType DOUBLE_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getDoubleOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Double Or Hex Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getDoubleOrHexOrTemplatePlaceholder()
         * @generated
         */
        EDataType DOUBLE_OR_HEX_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getDoubleOrHexOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>IP Pattern Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPPatternOrTemplatePlaceholder()
         * @generated
         */
        EDataType IP_PATTERN_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getIPPatternOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>MAC Pattern Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMACPatternOrTemplatePlaceholder()
         * @generated
         */
        EDataType MAC_PATTERN_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getMACPatternOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Port Pattern Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPortPatternOrTemplatePlaceholder()
         * @generated
         */
        EDataType PORT_PATTERN_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getPortPatternOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Vlan Id Pattern Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVlanIdPatternOrTemplatePlaceholder()
         * @generated
         */
        EDataType VLAN_ID_PATTERN_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getVlanIdPatternOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>IP Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getIPOrTemplatePlaceholder()
         * @generated
         */
        EDataType IP_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getIPOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Port Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getPortOrTemplatePlaceholder()
         * @generated
         */
        EDataType PORT_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getPortOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Vlan Id Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getVlanIdOrTemplatePlaceholder()
         * @generated
         */
        EDataType VLAN_ID_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getVlanIdOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>MAC Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getMACOrTemplatePlaceholder()
         * @generated
         */
        EDataType MAC_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getMACOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Bitmask Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getBitmaskOrTemplatePlaceholder()
         * @generated
         */
        EDataType BITMASK_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getBitmaskOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Hex Or Int Or Null Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getHexOrIntOrNullOrTemplatePlaceholder()
         * @generated
         */
        EDataType HEX_OR_INT_OR_NULL_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getHexOrIntOrNullOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Non Zero Double Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getNonZeroDoubleOrTemplatePlaceholder()
         * @generated
         */
        EDataType NON_ZERO_DOUBLE_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getNonZeroDoubleOrTemplatePlaceholder();

        /**
         * The meta object literal for the '<em>Val Var Initial Value Double Or Template Placeholder Or String</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getValVarInitialValueDoubleOrTemplatePlaceholderOrString()
         * @generated
         */
        EDataType VAL_VAR_INITIAL_VALUE_DOUBLE_OR_TEMPLATE_PLACEHOLDER_OR_STRING = eINSTANCE.getValVarInitialValueDoubleOrTemplatePlaceholderOrString();

        /**
         * The meta object literal for the '<em>Byte Or Template Placeholder</em>' data type.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see java.lang.String
         * @see de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl#getByteOrTemplatePlaceholder()
         * @generated
         */
        EDataType BYTE_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getByteOrTemplatePlaceholder();

    }


}

