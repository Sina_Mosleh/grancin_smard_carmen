package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.PluginStateExtractStrategy;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin State Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl#getStates <em>States</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl#getStatesTmplParam <em>States Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl#getStatesActive <em>States Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginStateExtractStrategyImpl#getStatesActiveTmplParam <em>States Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginStateExtractStrategyImpl extends ExtractStrategyImpl implements PluginStateExtractStrategy {
    /**
     * The default value of the '{@link #getStates() <em>States</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStates()
     * @generated
     * @ordered
     */
    protected static final String STATES_EDEFAULT = "ALL";

    /**
     * The cached value of the '{@link #getStates() <em>States</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStates()
     * @generated
     * @ordered
     */
    protected String states = STATES_EDEFAULT;

    /**
     * The default value of the '{@link #getStatesTmplParam() <em>States Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STATES_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStatesTmplParam() <em>States Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesTmplParam()
     * @generated
     * @ordered
     */
    protected String statesTmplParam = STATES_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getStatesActive() <em>States Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesActive()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum STATES_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

    /**
     * The cached value of the '{@link #getStatesActive() <em>States Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesActive()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum statesActive = STATES_ACTIVE_EDEFAULT;

    /**
     * The default value of the '{@link #getStatesActiveTmplParam() <em>States Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesActiveTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STATES_ACTIVE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStatesActiveTmplParam() <em>States Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStatesActiveTmplParam()
     * @generated
     * @ordered
     */
    protected String statesActiveTmplParam = STATES_ACTIVE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PluginStateExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PLUGIN_STATE_EXTRACT_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStates() {
        return states;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStates(String newStates) {
        String oldStates = states;
        states = newStates;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES, oldStates, states));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStatesTmplParam() {
        return statesTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStatesTmplParam(String newStatesTmplParam) {
        String oldStatesTmplParam = statesTmplParam;
        statesTmplParam = newStatesTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM, oldStatesTmplParam, statesTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getStatesActive() {
        return statesActive;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStatesActive(BooleanOrTemplatePlaceholderEnum newStatesActive) {
        BooleanOrTemplatePlaceholderEnum oldStatesActive = statesActive;
        statesActive = newStatesActive == null ? STATES_ACTIVE_EDEFAULT : newStatesActive;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE, oldStatesActive, statesActive));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStatesActiveTmplParam() {
        return statesActiveTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStatesActiveTmplParam(String newStatesActiveTmplParam) {
        String oldStatesActiveTmplParam = statesActiveTmplParam;
        statesActiveTmplParam = newStatesActiveTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM, oldStatesActiveTmplParam, statesActiveTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATES_ACTIVE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(statesActive)
                        .error("_Validation_Conditions_PluginStateExtractStrategy_StatesActive_IsNull")
                        .build())

                .with(Validators.checkTemplate(statesActive)
                        .templateType(BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(statesActiveTmplParam)
                        .containsParameterError("_Validation_Conditions_PluginStateExtractStrategy_StatesActive_ContainsPlaceholders")
                        .tmplParamIsNullError("_Validation_Conditions_PluginStateExtractStrategy_StatesActiveTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_PluginStateExtractStrategy_StatesActiveTmplParam_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_PluginStateExtractStrategy_StatesActive_IllegalStatesActive")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(statesActive)
                        .error("_Validation_Conditions_PluginStateExtractStrategy_States_IsNull")
                        .build())

                .with(Validators.checkTemplate(statesActive)
                        .templateType(BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(statesActiveTmplParam)
                        .containsParameterError("_Validation_Conditions_PluginStateExtractStrategy_States_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_PluginStateExtractStrategy_StatesTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_PluginStateExtractStrategy_StatesTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
                return getStates();
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
                return getStatesTmplParam();
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
                return getStatesActive();
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
                return getStatesActiveTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
                setStates((String) newValue);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
                setStatesTmplParam((String) newValue);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
                setStatesActive((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
                setStatesActiveTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
                setStates(STATES_EDEFAULT);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
                setStatesTmplParam(STATES_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
                setStatesActive(STATES_ACTIVE_EDEFAULT);
                return;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
                setStatesActiveTmplParam(STATES_ACTIVE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
                return STATES_EDEFAULT == null ? states != null : !STATES_EDEFAULT.equals(states);
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
                return STATES_TMPL_PARAM_EDEFAULT == null ? statesTmplParam != null : !STATES_TMPL_PARAM_EDEFAULT.equals(statesTmplParam);
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
                return statesActive != STATES_ACTIVE_EDEFAULT;
            case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
                return STATES_ACTIVE_TMPL_PARAM_EDEFAULT == null ? statesActiveTmplParam != null : !STATES_ACTIVE_TMPL_PARAM_EDEFAULT
                        .equals(statesActiveTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (states: ");
        result.append(states);
        result.append(", statesTmplParam: ");
        result.append(statesTmplParam);
        result.append(", statesActive: ");
        result.append(statesActive);
        result.append(", statesActiveTmplParam: ");
        result.append(statesActiveTmplParam);
        result.append(')');
        return result.toString();
    }

} //PluginStateExtractStrategyImpl
