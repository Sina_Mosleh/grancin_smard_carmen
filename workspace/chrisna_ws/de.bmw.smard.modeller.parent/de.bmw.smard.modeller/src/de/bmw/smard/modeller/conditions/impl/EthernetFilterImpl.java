package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.EthernetFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ethernet Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getSourceMAC <em>Source MAC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getDestMAC <em>Dest MAC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getEtherType <em>Ether Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getInnerVlanId <em>Inner Vlan Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getOuterVlanId <em>Outer Vlan Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getInnerVlanCFI <em>Inner Vlan CFI</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getOuterVlanCFI <em>Outer Vlan CFI</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getInnerVlanVID <em>Inner Vlan VID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getOuterVlanVID <em>Outer Vlan VID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getCRC <em>CRC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getInnerVlanTPID <em>Inner Vlan TPID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetFilterImpl#getOuterVlanTPID <em>Outer Vlan TPID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EthernetFilterImpl extends AbstractFilterImpl implements EthernetFilter {
    /**
     * The default value of the '{@link #getSourceMAC() <em>Source MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSourceMAC()
     * @generated
     * @ordered
     */
    protected static final String SOURCE_MAC_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getSourceMAC() <em>Source MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSourceMAC()
     * @generated
     * @ordered
     */
    protected String sourceMAC = SOURCE_MAC_EDEFAULT;
    /**
     * The default value of the '{@link #getDestMAC() <em>Dest MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDestMAC()
     * @generated
     * @ordered
     */
    protected static final String DEST_MAC_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getDestMAC() <em>Dest MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDestMAC()
     * @generated
     * @ordered
     */
    protected String destMAC = DEST_MAC_EDEFAULT;
    /**
     * The default value of the '{@link #getEtherType() <em>Ether Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEtherType()
     * @generated
     * @ordered
     */
    protected static final String ETHER_TYPE_EDEFAULT = "0x0800";
    /**
     * The cached value of the '{@link #getEtherType() <em>Ether Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEtherType()
     * @generated
     * @ordered
     */
    protected String etherType = ETHER_TYPE_EDEFAULT;
    /**
     * The default value of the '{@link #getInnerVlanId() <em>Inner Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanId()
     * @generated
     * @ordered
     */
    protected static final String INNER_VLAN_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getInnerVlanId() <em>Inner Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanId()
     * @generated
     * @ordered
     */
    protected String innerVlanId = INNER_VLAN_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getOuterVlanId() <em>Outer Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanId()
     * @generated
     * @ordered
     */
    protected static final String OUTER_VLAN_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getOuterVlanId() <em>Outer Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanId()
     * @generated
     * @ordered
     */
    protected String outerVlanId = OUTER_VLAN_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getInnerVlanCFI() <em>Inner Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanCFI()
     * @generated
     * @ordered
     */
    protected static final String INNER_VLAN_CFI_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getInnerVlanCFI() <em>Inner Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanCFI()
     * @generated
     * @ordered
     */
    protected String innerVlanCFI = INNER_VLAN_CFI_EDEFAULT;
    /**
     * The default value of the '{@link #getOuterVlanCFI() <em>Outer Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanCFI()
     * @generated
     * @ordered
     */
    protected static final String OUTER_VLAN_CFI_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getOuterVlanCFI() <em>Outer Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanCFI()
     * @generated
     * @ordered
     */
    protected String outerVlanCFI = OUTER_VLAN_CFI_EDEFAULT;
    /**
     * The default value of the '{@link #getInnerVlanVID() <em>Inner Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanVID()
     * @generated
     * @ordered
     */
    protected static final String INNER_VLAN_VID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getInnerVlanVID() <em>Inner Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanVID()
     * @generated
     * @ordered
     */
    protected String innerVlanVID = INNER_VLAN_VID_EDEFAULT;
    /**
     * The default value of the '{@link #getOuterVlanVID() <em>Outer Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanVID()
     * @generated
     * @ordered
     */
    protected static final String OUTER_VLAN_VID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getOuterVlanVID() <em>Outer Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanVID()
     * @generated
     * @ordered
     */
    protected String outerVlanVID = OUTER_VLAN_VID_EDEFAULT;
    /**
     * The default value of the '{@link #getCRC() <em>CRC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCRC()
     * @generated
     * @ordered
     */
    protected static final String CRC_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getCRC() <em>CRC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCRC()
     * @generated
     * @ordered
     */
    protected String crc = CRC_EDEFAULT;
    /**
     * The default value of the '{@link #getInnerVlanTPID() <em>Inner Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanTPID()
     * @generated
     * @ordered
     */
    protected static final String INNER_VLAN_TPID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getInnerVlanTPID() <em>Inner Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInnerVlanTPID()
     * @generated
     * @ordered
     */
    protected String innerVlanTPID = INNER_VLAN_TPID_EDEFAULT;
    /**
     * The default value of the '{@link #getOuterVlanTPID() <em>Outer Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanTPID()
     * @generated
     * @ordered
     */
    protected static final String OUTER_VLAN_TPID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getOuterVlanTPID() <em>Outer Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOuterVlanTPID()
     * @generated
     * @ordered
     */
    protected String outerVlanTPID = OUTER_VLAN_TPID_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected EthernetFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.ETHERNET_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSourceMAC() {
        return sourceMAC;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSourceMAC(String newSourceMAC) {
        String oldSourceMAC = sourceMAC;
        sourceMAC = newSourceMAC;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC, oldSourceMAC, sourceMAC));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDestMAC() {
        return destMAC;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDestMAC(String newDestMAC) {
        String oldDestMAC = destMAC;
        destMAC = newDestMAC;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__DEST_MAC, oldDestMAC, destMAC));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getEtherType() {
        return etherType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEtherType(String newEtherType) {
        String oldEtherType = etherType;
        etherType = newEtherType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE, oldEtherType, etherType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInnerVlanId() {
        return innerVlanId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInnerVlanId(String newInnerVlanId) {
        String oldInnerVlanId = innerVlanId;
        innerVlanId = newInnerVlanId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID, oldInnerVlanId, innerVlanId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOuterVlanId() {
        return outerVlanId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOuterVlanId(String newOuterVlanId) {
        String oldOuterVlanId = outerVlanId;
        outerVlanId = newOuterVlanId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID, oldOuterVlanId, outerVlanId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInnerVlanCFI() {
        return innerVlanCFI;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInnerVlanCFI(String newInnerVlanCFI) {
        String oldInnerVlanCFI = innerVlanCFI;
        innerVlanCFI = newInnerVlanCFI;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI, oldInnerVlanCFI, innerVlanCFI));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOuterVlanCFI() {
        return outerVlanCFI;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOuterVlanCFI(String newOuterVlanCFI) {
        String oldOuterVlanCFI = outerVlanCFI;
        outerVlanCFI = newOuterVlanCFI;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI, oldOuterVlanCFI, outerVlanCFI));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInnerVlanVID() {
        return innerVlanVID;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInnerVlanVID(String newInnerVlanVID) {
        String oldInnerVlanVID = innerVlanVID;
        innerVlanVID = newInnerVlanVID;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID, oldInnerVlanVID, innerVlanVID));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOuterVlanVID() {
        return outerVlanVID;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOuterVlanVID(String newOuterVlanVID) {
        String oldOuterVlanVID = outerVlanVID;
        outerVlanVID = newOuterVlanVID;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID, oldOuterVlanVID, outerVlanVID));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCRC() {
        return crc;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCRC(String newCRC) {
        String oldCRC = crc;
        crc = newCRC;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__CRC, oldCRC, crc));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInnerVlanTPID() {
        return innerVlanTPID;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInnerVlanTPID(String newInnerVlanTPID) {
        String oldInnerVlanTPID = innerVlanTPID;
        innerVlanTPID = newInnerVlanTPID;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID, oldInnerVlanTPID, innerVlanTPID));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOuterVlanTPID() {
        return outerVlanTPID;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOuterVlanTPID(String newOuterVlanTPID) {
        String oldOuterVlanTPID = outerVlanTPID;
        outerVlanTPID = newOuterVlanTPID;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID, oldOuterVlanTPID, outerVlanTPID));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSourceMAC(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidSourceMAC = true;
        String errorMessageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        String warningMessageToSubstitute = null;
        String warningMessageIdentifier = null;
        String sourceMac = this.sourceMAC;
        List<String> errorMessages = new ArrayList<String>();
        List<String> warningMessages = new ArrayList<String>();
        boolean templateAllowed = TemplateUtils.getTemplateCategorization(this).allowsTemplateParameters();
        getValidatedMACAdress(sourceMac, "Source MAC", templateAllowed, errorMessages, warningMessages);
        if (errorMessages != null && errorMessages.size() > 0) {
            errorMessageIdentifier = "_Validation_Conditions_EthernetFilter_SourceMac_IllegalFormat";
            errorMessageToSubstitute = StringUtils.join(errorMessages, ";");
            hasValidSourceMAC = false;
        }
        if (warningMessages != null && warningMessages.size() > 0) {
            warningMessageIdentifier = "_Validation_Conditions_EthernetFilter_SourceMAC_WARNING";
            warningMessageToSubstitute = StringUtils.join(warningMessages, ";");
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_SOURCE_MAC)
                        .messageId(PluginActivator.INSTANCE, warningMessageIdentifier, warningMessageToSubstitute)
                        .data(this)
                        .build());
            }
        }
        if (!hasValidSourceMAC) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_SOURCE_MAC)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, errorMessageToSubstitute)
                        .data(this)
                        .build());
            }
            return hasValidSourceMAC;
        }
        return hasValidSourceMAC;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDestinationMAC(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidDestinationMAC = true;
        String errorMessageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        String warningMessageToSubstitute = null;
        String warningMessageIdentifier = null;
        String destMac = this.destMAC;
        List<String> errorMessages = new ArrayList<String>();
        List<String> warningMessages = new ArrayList<String>();
        boolean templateAllowed = TemplateUtils.getTemplateCategorization(this).allowsTemplateParameters();
        getValidatedMACAdress(destMac, "Dest MAC", templateAllowed, errorMessages, warningMessages);
        if (errorMessages != null && errorMessages.size() > 0) {
            errorMessageIdentifier = "_Validation_Conditions_EthernetFilter_DestMac_IllegalFormat";
            errorMessageToSubstitute = StringUtils.join(errorMessages, ";");
            hasValidDestinationMAC = false;
        }
        if (warningMessages != null && warningMessages.size() > 0) {
            warningMessageIdentifier = "_Validation_Conditions_EthernetFilter_DestMAC_WARNING";
            warningMessageToSubstitute = StringUtils.join(warningMessages, ";");
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_DESTINATION_MAC)
                        .messageId(PluginActivator.INSTANCE, warningMessageIdentifier, warningMessageToSubstitute)
                        .data(this)
                        .build());
            }
        }
        if (!hasValidDestinationMAC) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_DESTINATION_MAC)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, errorMessageToSubstitute)
                        .data(this)
                        .build());
            }
            return hasValidDestinationMAC;
        }
        return hasValidDestinationMAC;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidCRC(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_CRC)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(crc)
                        .name(ConditionsPackage.Literals.ETHERNET_FILTER__CRC.getName())
                        .warning("_Validation_Conditions_EthernetFilter_Crc_WARNING")
                        .error("_Validation_Conditions_EthernetFilter_Crc_IllegalCrc")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidEtherType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_ETHER_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(etherType)
                        .name(ConditionsPackage.Literals.ETHERNET_FILTER__ETHER_TYPE.getName())
                        .warning("_Validation_Conditions_EthernetFilter_EtherType_WARNING")
                        .error("_Validation_Conditions_EthernetFilter_EtherType_IllegalEtherType")
                        .max(65535)
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidInnerVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                innerVlanVID,
                ConditionsPackage.Literals.ETHERNET_FILTER__INNER_VLAN_VID.getName(),
                "_Validation_Conditions_EthernetFilter_InnerVlanVid_IllegalInnerVlanVid",
                "_Validation_Conditions_EthernetFilter_InnerVlanVid_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_VID,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOuterVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                outerVlanVID,
                ConditionsPackage.Literals.ETHERNET_FILTER__OUTER_VLAN_VID.getName(),
                "_Validation_Conditions_EthernetFilter_OuterVlanVid_IllegalOuterVlanVid",
                "_Validation_Conditions_EthernetFilter_OuterVlanVid_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_VID,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidInnerVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                innerVlanCFI,
                ConditionsPackage.Literals.ETHERNET_FILTER__INNER_VLAN_CFI.getName(),
                "_Validation_Conditions_EthernetFilter_InnerVlanCfi_IllegalInnerVlanCfi",
                "_Validation_Conditions_EthernetFilter_InnerVlanCfi_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_CFI,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOuterVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                outerVlanCFI,
                ConditionsPackage.Literals.ETHERNET_FILTER__OUTER_VLAN_CFI.getName(),
                "_Validation_Conditions_EthernetFilter_OuterVlanCfi_IllegalOuterVlanCfi",
                "_Validation_Conditions_EthernetFilter_OuterVlanCfi_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_CFI,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidInnerVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                innerVlanId,
                ConditionsPackage.Literals.ETHERNET_FILTER__INNER_VLAN_VID.getName(),
                "_Validation_Conditions_EthernetFilter_InnerVlanPcp_IllegalInnerVlanPcp",
                "_Validation_Conditions_EthernetFilter_InnerVlanPcp_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_PCP,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOuterVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                outerVlanId,
                ConditionsPackage.Literals.ETHERNET_FILTER__OUTER_VLAN_VID.getName(),
                "_Validation_Conditions_EthernetFilter_OuterVlanPcp_IllegalOuterVlanPcp",
                "_Validation_Conditions_EthernetFilter_OuterVlanPcp_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_PCP,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidInnerVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                innerVlanTPID,
                ConditionsPackage.Literals.ETHERNET_FILTER__INNER_VLAN_TPID.getName(),
                "_Validation_Conditions_EthernetFilter_InnerVlanTpid_IllegalInnerVlanTpid",
                "_Validation_Conditions_EthernetFilter_InnerVlanTpid_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_TPID,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOuterVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateVlanId(
                outerVlanTPID,
                ConditionsPackage.Literals.ETHERNET_FILTER__OUTER_VLAN_TPID.getName(),
                "_Validation_Conditions_EthernetFilter_OuterVlanTpid_IllegalOuterVlanTpid",
                "_Validation_Conditions_EthernetFilter_OuterVlanTpid_WARNING",
                ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_TPID,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    private boolean validateVlanId(String vlanId, String type, String illegalMessageId, String warningMessageId, int code, DiagnosticChain diagnostics) {
        boolean hasValidVlanId = true;
        String errorMessageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        String warningMessageIdentifier = null;
        String warningMessageToSubstitute = null;
        List<String> errorMessages = new ArrayList<String>();
        List<String> warningMessages = new ArrayList<String>();
        boolean templateAllowed = TemplateUtils.getTemplateCategorization(this).allowsTemplateParameters();
        getValidatedVlanId(vlanId, type, templateAllowed, errorMessages, warningMessages);
        if (errorMessages != null && errorMessages.size() > 0) {
            errorMessageIdentifier = illegalMessageId;
            errorMessageToSubstitute = StringUtils.join(errorMessages, ";");
        }
        if (warningMessages != null && warningMessages.size() > 0) {
            warningMessageIdentifier = warningMessageId;
            warningMessageToSubstitute = StringUtils.join(warningMessages, ";");
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(code)
                        .messageId(PluginActivator.INSTANCE, warningMessageIdentifier, warningMessageToSubstitute)
                        .data(this)
                        .build());
            }
        }
        if (!hasValidVlanId) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(code)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, errorMessageToSubstitute)
                        .data(this)
                        .build());
            }
            return hasValidVlanId;
        }
        return hasValidVlanId;
    }

    /**
     * @param macPatternString
     * @param type
     * @param templateAllowed
     * @param errorMessages
     * @param warningMessages
     * @return
     *         generated NOT
     */
    private static String getValidatedMACAdress(String macPatternString, String type, boolean templateAllowed,
            List<String> errorMessages, List<String> warningMessages) {

        String result = "";

        if (macPatternString != null && macPatternString.trim().length() != 0) {
            if (!templateAllowed) {
                // ... and must not contain template placeholders
                if (TemplateUtils.containsParameters(macPatternString)) {
                    errorMessages.add(type + " address must not contain template placeholders.");
                }
            } else {
                return macPatternString;
            }
        }

        if (macPatternString != null && !macPatternString.isEmpty()) {
            String[] parts = macPatternString.split("[:-]");
            if (parts.length != 6) {
                errorMessages.add(type + " address must be six characters long.");
                //errorMessageIdentifier = "_Validation_Conditions_EthernetFilter_SourceMac_IllegalFormat";
                //hasValidSourceMAC = false;
            } else {
                int i = 0;
                try {
                    for (i = 0; i < parts.length; i++) {
                        // mn This is only done to catch possible
                        // NumberFormatExceptions
                        Integer.valueOf(parts[i], 16);
                    }
                } catch (NumberFormatException e) {
                    errorMessages.add(type + " can only contain numbers.");
                    //errorMessageIdentifier = "_Validation_Conditions_EthernetFilter_SourceMac_IllegalFormatPart";
                    //hasValidSourceMAC = false;
                }
            }
        }
        result = macPatternString;

        return result;
    }

    /**
     * checks the given string if it matches the message ids (given in
     * {@link EthernetMessageCheckCondition#messageParameters}).
     * 
     * @param vlanIdPatternString
     *                            the string to check, must not be null
     * @param errorObject
     *                            the object to use as reference when reporting an error
     * @generated NOT
     */
    private static String getValidatedVlanId(String vlanIdPatternString, String type, boolean templateAllowed,
            List<String> errorMessages, List<String> warningMessages) {
        /*
         * vlanParameters ::= vlanParameter [ { vlanParameterSeparator }
         * vlanParameters ] vlanParameterSeparator ::= ',' | ';' | '/'
         * vlanParameter ::= vlanVal | '[' vlanRange ']' vlanRange ::= vlanVal |
         * vlanVal { vlanRangeSeparator } vlanVal vlanRangeSeparator ::= '-' |
         * '..' vlanVal ::= <vlan id value as int from 0 to 1095>
         */
        String res = "";
        boolean isRange = false;

        // Value is optional
        if (vlanIdPatternString != null && vlanIdPatternString.trim().length() != 0) {
            if (!templateAllowed) {
                // ...and must not contain template placeholders
                if (TemplateUtils.containsParameters(vlanIdPatternString)) {
                    errorMessages.add(type + " must not contain template placeholders.");
                }
            } else {
                return res;
            }

            // vlanParameters ::= vlanParameter [ { vlanParameterSeparator }
            // vlanParameters ]
            String[] vlanParameters = vlanIdPatternString.split("[,;/]");
            for (String vlanParameter : vlanParameters) {
                vlanParameter = vlanParameter.trim();

                // detect ranges
                // vlanParameter ::= vlanVal | '[' vlanRange ']'
                String vlanParameterContentString;
                if (vlanParameter.startsWith("[") && vlanParameter.endsWith("]")) {
                    // vlanRange -> remove brackets
                    vlanParameterContentString = vlanParameter.substring(1, vlanParameter.length() - 1);
                } else {
                    // vlanVal
                    vlanParameterContentString = vlanParameter;
                }

                vlanParameterContentString = vlanParameterContentString.trim();

                // split range values
                // vlanRange ::= vlanVal | vlanVal '-' vlanVal
                String[] vlanRangeParts = vlanParameterContentString.split("-");
                int[] vlanRangeValues = new int[vlanRangeParts.length];
                for (int i = 0; i < vlanRangeParts.length; i++) {
                    String vlanString = vlanRangeParts[i].trim();

                    try {
                        if (vlanString.toLowerCase().startsWith("0x")) {
                            vlanRangeValues[i] = Integer.parseInt(vlanString.substring(2), 16);
                        } else {
                            vlanRangeValues[i] = Integer.parseInt(vlanString);
                        }
                    } catch (NumberFormatException e) {
                        errorMessages.add(type + " must be an integer or hex value, not '" + vlanString + "'.");
                    }
                }
                if (vlanRangeValues.length == 1) {
                    // vlanVal ok
                } else if (vlanRangeValues.length == 2) {
                    isRange = true;

                    // vlanRange
                    if (vlanRangeValues[1] < vlanRangeValues[0]) {
                        errorMessages.add(type + " range must have first value less or equal to second value ''"
                                + vlanParameterContentString + "'.");
                    }
                } else {
                    // something else
                    errorMessages.add(
                            type + " must be a single value or a range, not '" + vlanParameterContentString + "'.");
                }
            }

            if (isRange == true) {
                warningMessages.add(type
                        + " contains a range value. The evaluation process in the engine can therefore take more time.");
            }

            res = vlanIdPatternString;
        }
        return res;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
                return getSourceMAC();
            case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
                return getDestMAC();
            case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
                return getEtherType();
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
                return getInnerVlanId();
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
                return getOuterVlanId();
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
                return getInnerVlanCFI();
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
                return getOuterVlanCFI();
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
                return getInnerVlanVID();
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
                return getOuterVlanVID();
            case ConditionsPackage.ETHERNET_FILTER__CRC:
                return getCRC();
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
                return getInnerVlanTPID();
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
                return getOuterVlanTPID();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
                setSourceMAC((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
                setDestMAC((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
                setEtherType((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
                setInnerVlanId((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
                setOuterVlanId((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
                setInnerVlanCFI((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
                setOuterVlanCFI((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
                setInnerVlanVID((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
                setOuterVlanVID((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__CRC:
                setCRC((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
                setInnerVlanTPID((String) newValue);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
                setOuterVlanTPID((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
                setSourceMAC(SOURCE_MAC_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
                setDestMAC(DEST_MAC_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
                setEtherType(ETHER_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
                setInnerVlanId(INNER_VLAN_ID_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
                setOuterVlanId(OUTER_VLAN_ID_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
                setInnerVlanCFI(INNER_VLAN_CFI_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
                setOuterVlanCFI(OUTER_VLAN_CFI_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
                setInnerVlanVID(INNER_VLAN_VID_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
                setOuterVlanVID(OUTER_VLAN_VID_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__CRC:
                setCRC(CRC_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
                setInnerVlanTPID(INNER_VLAN_TPID_EDEFAULT);
                return;
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
                setOuterVlanTPID(OUTER_VLAN_TPID_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
                return SOURCE_MAC_EDEFAULT == null ? sourceMAC != null : !SOURCE_MAC_EDEFAULT.equals(sourceMAC);
            case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
                return DEST_MAC_EDEFAULT == null ? destMAC != null : !DEST_MAC_EDEFAULT.equals(destMAC);
            case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
                return ETHER_TYPE_EDEFAULT == null ? etherType != null : !ETHER_TYPE_EDEFAULT.equals(etherType);
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
                return INNER_VLAN_ID_EDEFAULT == null ? innerVlanId != null : !INNER_VLAN_ID_EDEFAULT.equals(innerVlanId);
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
                return OUTER_VLAN_ID_EDEFAULT == null ? outerVlanId != null : !OUTER_VLAN_ID_EDEFAULT.equals(outerVlanId);
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
                return INNER_VLAN_CFI_EDEFAULT == null ? innerVlanCFI != null : !INNER_VLAN_CFI_EDEFAULT.equals(innerVlanCFI);
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
                return OUTER_VLAN_CFI_EDEFAULT == null ? outerVlanCFI != null : !OUTER_VLAN_CFI_EDEFAULT.equals(outerVlanCFI);
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
                return INNER_VLAN_VID_EDEFAULT == null ? innerVlanVID != null : !INNER_VLAN_VID_EDEFAULT.equals(innerVlanVID);
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
                return OUTER_VLAN_VID_EDEFAULT == null ? outerVlanVID != null : !OUTER_VLAN_VID_EDEFAULT.equals(outerVlanVID);
            case ConditionsPackage.ETHERNET_FILTER__CRC:
                return CRC_EDEFAULT == null ? crc != null : !CRC_EDEFAULT.equals(crc);
            case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
                return INNER_VLAN_TPID_EDEFAULT == null ? innerVlanTPID != null : !INNER_VLAN_TPID_EDEFAULT.equals(innerVlanTPID);
            case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
                return OUTER_VLAN_TPID_EDEFAULT == null ? outerVlanTPID != null : !OUTER_VLAN_TPID_EDEFAULT.equals(outerVlanTPID);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (sourceMAC: ");
        result.append(sourceMAC);
        result.append(", destMAC: ");
        result.append(destMAC);
        result.append(", etherType: ");
        result.append(etherType);
        result.append(", innerVlanId: ");
        result.append(innerVlanId);
        result.append(", outerVlanId: ");
        result.append(outerVlanId);
        result.append(", innerVlanCFI: ");
        result.append(innerVlanCFI);
        result.append(", outerVlanCFI: ");
        result.append(outerVlanCFI);
        result.append(", innerVlanVID: ");
        result.append(innerVlanVID);
        result.append(", outerVlanVID: ");
        result.append(outerVlanVID);
        result.append(", CRC: ");
        result.append(crc);
        result.append(", innerVlanTPID: ");
        result.append(innerVlanTPID);
        result.append(", outerVlanTPID: ");
        result.append(outerVlanTPID);
        result.append(')');
        return result.toString();
    }

} //EthernetFilterImpl
