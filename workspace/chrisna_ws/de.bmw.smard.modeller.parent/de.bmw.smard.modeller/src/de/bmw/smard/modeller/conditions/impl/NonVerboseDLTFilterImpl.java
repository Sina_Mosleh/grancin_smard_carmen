package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.NonVerboseDLTFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.ModellerDataConversions;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Verbose DLT Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl#getMessageId <em>Message Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.NonVerboseDLTFilterImpl#getMessageIdRange <em>Message Id Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NonVerboseDLTFilterImpl extends AbstractFilterImpl implements NonVerboseDLTFilter {
    /**
     * The default value of the '{@link #getMessageId() <em>Message Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageId()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_ID_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getMessageId() <em>Message Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageId()
     * @generated
     * @ordered
     */
    protected String messageId = MESSAGE_ID_EDEFAULT;
    /**
     * The default value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_ID_RANGE_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected String messageIdRange = MESSAGE_ID_RANGE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected NonVerboseDLTFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.NON_VERBOSE_DLT_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageId() {
        return messageId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageId(String newMessageId) {
        String oldMessageId = messageId;
        messageId = newMessageId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID, oldMessageId, messageId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageIdRange() {
        return messageIdRange;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageIdRange(String newMessageIdRange) {
        String oldMessageIdRange = messageIdRange;
        messageIdRange = newMessageIdRange;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE, oldMessageIdRange, messageIdRange));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidMessageIdOrMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {
        String errorMessageIdentifier = "";
        String messageToSubstitute = "";
        boolean isValid = true;

        if (StringUtils.nullOrEmpty(messageIdRange)) { // messageIdRange empty
            if (!StringUtils.nullOrEmpty(messageId)) { // for NonVerboseDLT it is ok, that both are empty
                if (TemplateUtils.containsParameters(messageId)) { // messageId filled and template param
                    if (TemplateUtils.getTemplateCategorization(this).isExportable()) { // no template context -> error
                        errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageId_ContainsPlaceholders";
                        isValid = false;
                    } else { // template context
                        messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(messageId);
                        if (messageToSubstitute != null && messageToSubstitute.length() > 0) {
                            errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageId_NotAValidPlaceholderName";
                            isValid = false;
                        }
                    }
                } else { // messageId filled, no template param
                    try {
                        ModellerDataConversions.getIntFromHexOrNumber(messageId);
                    } catch (NumberFormatException ex) {
                        errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageId_NotANumber";
                        isValid = false;
                    }
                }
            }

        } else { // messageIdRange filled

            if (TemplateUtils.containsParameters(messageIdRange)) { // messageIdRange filled and template param
                if (TemplateUtils.getTemplateCategorization(this).isExportable()) { // no template context -> error
                    errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageIdRange_ContainsPlaceholders";
                    isValid = false;
                } else { // template context
                    messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(messageIdRange);
                    if (messageToSubstitute != null && messageToSubstitute.length() > 0) {
                        errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageIdRange_NotAValidPlaceholderName";
                        isValid = false;
                    }
                }
            } else { // messageIdRange filled, no template param
                messageToSubstitute = TemplateUtils.checkCanIdRangePatternNotPlaceholder(messageIdRange, false); // TODO
                if (!StringUtils.nullOrEmpty(messageToSubstitute)) {
                    errorMessageIdentifier = "_Validation_Conditions_NonVerboseDLTFilter_MessageIdRange_IllegalMessageIdRange";
                    isValid = false;
                }
            }

        }

        if (!isValid) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.NON_VERBOSE_DLT_FILTER__IS_VALID_HAS_VALID_MESSAGE_ID_OR_MESSAGE_ID_RANGE)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                        .data(this)
                        .build());
            }
        }

        return isValid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID:
                return getMessageId();
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE:
                return getMessageIdRange();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID:
                setMessageId((String) newValue);
                return;
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID:
                setMessageId(MESSAGE_ID_EDEFAULT);
                return;
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange(MESSAGE_ID_RANGE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID:
                return MESSAGE_ID_EDEFAULT == null ? messageId != null : !MESSAGE_ID_EDEFAULT.equals(messageId);
            case ConditionsPackage.NON_VERBOSE_DLT_FILTER__MESSAGE_ID_RANGE:
                return MESSAGE_ID_RANGE_EDEFAULT == null ? messageIdRange != null : !MESSAGE_ID_RANGE_EDEFAULT.equals(messageIdRange);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (messageId: ");
        result.append(messageId);
        result.append(", messageIdRange: ");
        result.append(messageIdRange);
        result.append(')');
        return result.toString();
    }

} //NonVerboseDLTFilterImpl
