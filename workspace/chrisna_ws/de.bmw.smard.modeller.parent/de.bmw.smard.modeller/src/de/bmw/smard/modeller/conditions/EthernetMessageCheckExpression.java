package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ethernet Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getBusId <em>Bus Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getSourceMAC <em>Source MAC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getDestMAC <em>Dest MAC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getEtherType <em>Ether Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanId <em>Inner Vlan Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanId <em>Outer Vlan Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanCFI <em>Inner Vlan CFI</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanCFI <em>Outer Vlan CFI</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanVID <em>Inner Vlan VID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanVID <em>Outer Vlan VID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getCRC <em>CRC</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanTPID <em>Inner Vlan TPID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanTPID <em>Outer Vlan TPID</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getTypeL2 <em>Type L2</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getTypeL2TmplParam <em>Type L2 Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression()
 * @model
 * @generated
 */
public interface EthernetMessageCheckExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Bus Id</b></em>' attribute.
     * The default value is <code>"45000"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * WARNING: The bus id is filled by a special popup editor. The template parameter name is entered separately.
     * The value used here to indicate that a template parameter should be used is the value of {@link TemplateUtils#TEMPLATE_DEFINED_PLACEHOLDER_NAME}.
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Bus Id</em>' attribute.
     * @see #setBusId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_BusId()
     * @model default="45000" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getBusId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getBusId <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Bus Id</em>' attribute.
     * @see #getBusId()
     * @generated
     */
    void setBusId(String value);

    /**
     * Returns the value of the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Bus Id Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Bus Id Tmpl Param</em>' attribute.
     * @see #setBusIdTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_BusIdTmplParam()
     * @model
     * @generated
     */
    String getBusIdTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Bus Id Tmpl Param</em>' attribute.
     * @see #getBusIdTmplParam()
     * @generated
     */
    void setBusIdTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Source MAC</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source MAC</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Source MAC</em>' attribute.
     * @see #setSourceMAC(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_SourceMAC()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.MACPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getSourceMAC();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getSourceMAC <em>Source MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Source MAC</em>' attribute.
     * @see #getSourceMAC()
     * @generated
     */
    void setSourceMAC(String value);

    /**
     * Returns the value of the '<em><b>Dest MAC</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dest MAC</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Dest MAC</em>' attribute.
     * @see #setDestMAC(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_DestMAC()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.MACPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getDestMAC();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getDestMAC <em>Dest MAC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Dest MAC</em>' attribute.
     * @see #getDestMAC()
     * @generated
     */
    void setDestMAC(String value);

    /**
     * Returns the value of the '<em><b>Ether Type</b></em>' attribute.
     * The default value is <code>"0x0800"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ether Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ether Type</em>' attribute.
     * @see #setEtherType(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_EtherType()
     * @model default="0x0800" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getEtherType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getEtherType <em>Ether Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ether Type</em>' attribute.
     * @see #getEtherType()
     * @generated
     */
    void setEtherType(String value);

    /**
     * Returns the value of the '<em><b>Inner Vlan Id</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inner Vlan Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Inner Vlan Id</em>' attribute.
     * @see #setInnerVlanId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_InnerVlanId()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getInnerVlanId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanId <em>Inner Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Inner Vlan Id</em>' attribute.
     * @see #getInnerVlanId()
     * @generated
     */
    void setInnerVlanId(String value);

    /**
     * Returns the value of the '<em><b>Outer Vlan Id</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Outer Vlan Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Outer Vlan Id</em>' attribute.
     * @see #setOuterVlanId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_OuterVlanId()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getOuterVlanId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanId <em>Outer Vlan Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Outer Vlan Id</em>' attribute.
     * @see #getOuterVlanId()
     * @generated
     */
    void setOuterVlanId(String value);

    /**
     * Returns the value of the '<em><b>Inner Vlan CFI</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inner Vlan CFI</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Inner Vlan CFI</em>' attribute.
     * @see #setInnerVlanCFI(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_InnerVlanCFI()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getInnerVlanCFI();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanCFI <em>Inner Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Inner Vlan CFI</em>' attribute.
     * @see #getInnerVlanCFI()
     * @generated
     */
    void setInnerVlanCFI(String value);

    /**
     * Returns the value of the '<em><b>Outer Vlan CFI</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Outer Vlan CFI</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Outer Vlan CFI</em>' attribute.
     * @see #setOuterVlanCFI(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_OuterVlanCFI()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getOuterVlanCFI();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanCFI <em>Outer Vlan CFI</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Outer Vlan CFI</em>' attribute.
     * @see #getOuterVlanCFI()
     * @generated
     */
    void setOuterVlanCFI(String value);

    /**
     * Returns the value of the '<em><b>Inner Vlan VID</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inner Vlan VID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Inner Vlan VID</em>' attribute.
     * @see #setInnerVlanVID(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_InnerVlanVID()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getInnerVlanVID();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanVID <em>Inner Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Inner Vlan VID</em>' attribute.
     * @see #getInnerVlanVID()
     * @generated
     */
    void setInnerVlanVID(String value);

    /**
     * Returns the value of the '<em><b>Outer Vlan VID</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Outer Vlan VID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Outer Vlan VID</em>' attribute.
     * @see #setOuterVlanVID(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_OuterVlanVID()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getOuterVlanVID();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanVID <em>Outer Vlan VID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Outer Vlan VID</em>' attribute.
     * @see #getOuterVlanVID()
     * @generated
     */
    void setOuterVlanVID(String value);

    /**
     * Returns the value of the '<em><b>CRC</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>CRC</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>CRC</em>' attribute.
     * @see #setCRC(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_CRC()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getCRC();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getCRC <em>CRC</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>CRC</em>' attribute.
     * @see #getCRC()
     * @generated
     */
    void setCRC(String value);

    /**
     * Returns the value of the '<em><b>Inner Vlan TPID</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Inner Vlan TPID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Inner Vlan TPID</em>' attribute.
     * @see #setInnerVlanTPID(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_InnerVlanTPID()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getInnerVlanTPID();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getInnerVlanTPID <em>Inner Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Inner Vlan TPID</em>' attribute.
     * @see #getInnerVlanTPID()
     * @generated
     */
    void setInnerVlanTPID(String value);

    /**
     * Returns the value of the '<em><b>Outer Vlan TPID</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Outer Vlan TPID</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Outer Vlan TPID</em>' attribute.
     * @see #setOuterVlanTPID(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_OuterVlanTPID()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getOuterVlanTPID();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getOuterVlanTPID <em>Outer Vlan TPID</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Outer Vlan TPID</em>' attribute.
     * @see #getOuterVlanTPID()
     * @generated
     */
    void setOuterVlanTPID(String value);

    /**
     * Returns the value of the '<em><b>Type L2</b></em>' attribute.
     * The default value is <code>"ANY"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CheckType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type L2</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type L2</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #setTypeL2(CheckType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_TypeL2()
     * @model default="ANY" required="true"
     *        extendedMetaData="kind='attribute' name='typeL2' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    CheckType getTypeL2();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getTypeL2 <em>Type L2</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type L2</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #getTypeL2()
     * @generated
     */
    void setTypeL2(CheckType value);

    /**
     * Returns the value of the '<em><b>Type L2 Tmpl Param</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type L2 Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type L2 Tmpl Param</em>' attribute.
     * @see #setTypeL2TmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEthernetMessageCheckExpression_TypeL2TmplParam()
     * @model default=""
     * @generated
     */
    String getTypeL2TmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.EthernetMessageCheckExpression#getTypeL2TmplParam <em>Type L2 Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type L2 Tmpl Param</em>' attribute.
     * @see #getTypeL2TmplParam()
     * @generated
     */
    void setTypeL2TmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidL2Type(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSourceMAC(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidDestinationMAC(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidCRC(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidEtherType(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInnerVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOuterVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInnerVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOuterVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInnerVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOuterVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInnerVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOuterVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context);

} // EthernetMessageCheckExpression
