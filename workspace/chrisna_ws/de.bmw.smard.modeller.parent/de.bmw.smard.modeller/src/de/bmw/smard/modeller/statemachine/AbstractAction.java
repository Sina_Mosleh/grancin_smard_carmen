package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.BaseClassWithID;
import de.bmw.smard.modeller.conditions.IVariableReaderWriter;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractAction#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTrigger <em>Trigger</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractAction()
 * @model abstract="true"
 * @generated
 */
public interface AbstractAction extends BaseClassWithID, IVariableReaderWriter, BusMessageReferable {
    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractAction_Description()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Trigger</b></em>' attribute.
     * The default value is <code>"ON_ENTRY"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.statemachine.Trigger}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Trigger</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Trigger</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.Trigger
     * @see #setTrigger(Trigger)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractAction_Trigger()
     * @model default="ON_ENTRY" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    Trigger getTrigger();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTrigger <em>Trigger</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Trigger</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.Trigger
     * @see #getTrigger()
     * @generated
     */
    void setTrigger(Trigger value);

    /**
     * Returns the value of the '<em><b>Trigger Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Trigger Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Trigger Tmpl Param</em>' attribute.
     * @see #setTriggerTmplParam(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractAction_TriggerTmplParam()
     * @model
     * @generated
     */
    String getTriggerTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Trigger Tmpl Param</em>' attribute.
     * @see #getTriggerTmplParam()
     * @generated
     */
    void setTriggerTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidTrigger(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AbstractAction
