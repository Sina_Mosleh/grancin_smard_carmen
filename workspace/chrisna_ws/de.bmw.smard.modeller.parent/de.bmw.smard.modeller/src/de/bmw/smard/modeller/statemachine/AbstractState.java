package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.BaseClassWithID;
import de.bmw.smard.modeller.conditions.IVariableReaderWriter;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getOut <em>Out</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getIn <em>In</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getActions <em>Actions</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.AbstractState#getStateType <em>State Type</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState()
 * @model abstract="true"
 *        extendedMetaData="kind='mixed' name='AbstractState'"
 * @generated
 */
public interface AbstractState extends BaseClassWithID, IVariableReaderWriter, BusMessageReferable {
    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_Name()
     * @model extendedMetaData="kind='attribute'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractState#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Out</b></em>' reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.statemachine.Transition}.
     * It is bidirectional and its opposite is '{@link de.bmw.smard.modeller.statemachine.Transition#getFrom <em>From</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Out</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Out</em>' reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_Out()
     * @see de.bmw.smard.modeller.statemachine.Transition#getFrom
     * @model opposite="from" resolveProxies="false"
     *        extendedMetaData="kind='element' name='out'"
     * @generated
     */
    EList<Transition> getOut();

    /**
     * Returns the value of the '<em><b>In</b></em>' reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.statemachine.Transition}.
     * It is bidirectional and its opposite is '{@link de.bmw.smard.modeller.statemachine.Transition#getTo <em>To</em>}'.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>In</em>' reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>In</em>' reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_In()
     * @see de.bmw.smard.modeller.statemachine.Transition#getTo
     * @model opposite="to" resolveProxies="false"
     *        extendedMetaData="kind='element' name='in'"
     * @generated
     */
    EList<Transition> getIn();

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Description</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_Description()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractState#getDescription <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.statemachine.AbstractAction}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Actions</em>' containment reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_Actions()
     * @model containment="true"
     *        extendedMetaData="kind='element' name='Action'"
     * @generated
     */
    EList<AbstractAction> getActions();

    /**
     * Returns the value of the '<em><b>State Type</b></em>' attribute.
     * The default value is <code>"INFO"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.statemachine.StateType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>State Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>State Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.StateType
     * @see #setStateType(StateType)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAbstractState_StateType()
     * @model default="INFO" required="true"
     *        extendedMetaData="kind='element' name='StateType'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    StateType getStateType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.AbstractState#getStateType <em>State Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>State Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.StateType
     * @see #getStateType()
     * @generated
     */
    void setStateType(StateType value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AbstractState
