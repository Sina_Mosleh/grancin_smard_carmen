package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.IComputeVariableActionOperand;
import de.bmw.smard.modeller.conditions.IOperation;
import de.bmw.smard.modeller.conditions.ValueVariable;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Compute Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getTarget <em>Target</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getComputeVariable()
 * @model
 * @generated
 */
public interface ComputeVariable extends AbstractAction, IOperation {
    /**
     * Returns the value of the '<em><b>Action Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Action Expression</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Action Expression</em>' attribute.
     * @see #setActionExpression(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getComputeVariable_ActionExpression()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getActionExpression();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Action Expression</em>' attribute.
     * @see #getActionExpression()
     * @generated
     */
    void setActionExpression(String value);

    /**
     * Returns the value of the '<em><b>Target</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Target</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Target</em>' reference.
     * @see #setTarget(ValueVariable)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getComputeVariable_Target()
     * @model required="true"
     * @generated
     */
    ValueVariable getTarget();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.ComputeVariable#getTarget <em>Target</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Target</em>' reference.
     * @see #getTarget()
     * @generated
     */
    void setTarget(ValueVariable value);

    /**
     * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.IComputeVariableActionOperand}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operands</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Operands</em>' containment reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getComputeVariable_Operands()
     * @model containment="true"
     * @generated
     */
    EList<IComputeVariableActionOperand> getOperands();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ComputeVariable
