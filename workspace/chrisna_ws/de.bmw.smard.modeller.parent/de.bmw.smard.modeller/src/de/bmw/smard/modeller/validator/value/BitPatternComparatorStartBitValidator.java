package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class BitPatternComparatorStartBitValidator implements ValueValidator {

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		String errorMessageIdentifier = startBitGtZero(value);
		if (errorMessageIdentifier != null && baseValidator != null) {
			baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
		}
		return errorMessageIdentifier == null;
	}

	private String startBitGtZero(String startBit) {
		String errorMessageIdentifier = null;
		try {
			int startBitInt = TemplateUtils.getIntNotPlaceholder(startBit, false);
			if (startBitInt < 0) {
				errorMessageIdentifier = "_Validation_Conditions_BitPatternComparatorValue_StartBit_IsLowerEqualsZero";
			}
		} catch (NumberFormatException e) {
			errorMessageIdentifier = "_Validation_Conditions_BitPatternComparatorValue_StartBit_IsNull";
		}

		return errorMessageIdentifier;
	}
}
