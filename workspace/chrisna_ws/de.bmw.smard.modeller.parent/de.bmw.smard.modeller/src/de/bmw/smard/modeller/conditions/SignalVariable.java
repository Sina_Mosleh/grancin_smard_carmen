package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalVariable#getSignal <em>Signal</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalVariable#getSignalObservers <em>Signal Observers</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalVariable#getLag <em>Lag</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable()
 * @model
 * @generated
 */
public interface SignalVariable extends Variable {
    /**
     * Returns the value of the '<em><b>Interpreted Value</b></em>' attribute.
     * The default value is <code>"false"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Interpreted Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Interpreted Value</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #isSetInterpretedValue()
     * @see #unsetInterpretedValue()
     * @see #setInterpretedValue(BooleanOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable_InterpretedValue()
     * @model default="false" unsettable="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    BooleanOrTemplatePlaceholderEnum getInterpretedValue();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Interpreted Value</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #isSetInterpretedValue()
     * @see #unsetInterpretedValue()
     * @see #getInterpretedValue()
     * @generated
     */
    void setInterpretedValue(BooleanOrTemplatePlaceholderEnum value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetInterpretedValue()
     * @see #getInterpretedValue()
     * @see #setInterpretedValue(BooleanOrTemplatePlaceholderEnum)
     * @generated
     */
    void unsetInterpretedValue();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValue <em>Interpreted Value</em>}' attribute is
     * set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Interpreted Value</em>' attribute is set.
     * @see #unsetInterpretedValue()
     * @see #getInterpretedValue()
     * @see #setInterpretedValue(BooleanOrTemplatePlaceholderEnum)
     * @generated
     */
    boolean isSetInterpretedValue();

    /**
     * Returns the value of the '<em><b>Signal</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal</em>' reference.
     * @see #setSignal(ISignalOrReference)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable_Signal()
     * @model required="true"
     * @generated
     */
    ISignalOrReference getSignal();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getSignal <em>Signal</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal</em>' reference.
     * @see #getSignal()
     * @generated
     */
    void setSignal(ISignalOrReference value);

    /**
     * Returns the value of the '<em><b>Interpreted Value Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Interpreted Value Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Interpreted Value Tmpl Param</em>' attribute.
     * @see #setInterpretedValueTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable_InterpretedValueTmplParam()
     * @model
     * @generated
     */
    String getInterpretedValueTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Interpreted Value Tmpl Param</em>' attribute.
     * @see #getInterpretedValueTmplParam()
     * @generated
     */
    void setInterpretedValueTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Signal Observers</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.SignalObserver}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal Observers</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal Observers</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable_SignalObservers()
     * @model containment="true"
     * @generated
     */
    EList<SignalObserver> getSignalObservers();

    /**
     * Returns the value of the '<em><b>Lag</b></em>' attribute.
     * The default value is <code>"CURRENT"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.SignalVariableLagEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Lag</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Lag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SignalVariableLagEnum
     * @see #setLag(SignalVariableLagEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalVariable_Lag()
     * @model default="CURRENT" required="true"
     * @generated
     */
    SignalVariableLagEnum getLag();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SignalVariable#getLag <em>Lag</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Lag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SignalVariableLagEnum
     * @see #getLag()
     * @generated
     */
    void setLag(SignalVariableLagEnum value);

} // SignalVariable
