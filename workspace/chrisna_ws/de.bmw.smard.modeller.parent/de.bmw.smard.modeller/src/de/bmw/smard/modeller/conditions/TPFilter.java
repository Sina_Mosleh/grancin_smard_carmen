package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TPFilter#getSourcePort <em>Source Port</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TPFilter#getDestPort <em>Dest Port</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTPFilter()
 * @model abstract="true"
 * @generated
 */
public interface TPFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Source Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source Port</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Source Port</em>' attribute.
     * @see #setSourcePort(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTPFilter_SourcePort()
     * @model dataType="de.bmw.smard.modeller.conditions.PortPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getSourcePort();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TPFilter#getSourcePort <em>Source Port</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Source Port</em>' attribute.
     * @see #getSourcePort()
     * @generated
     */
    void setSourcePort(String value);

    /**
     * Returns the value of the '<em><b>Dest Port</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dest Port</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Dest Port</em>' attribute.
     * @see #setDestPort(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTPFilter_DestPort()
     * @model dataType="de.bmw.smard.modeller.conditions.PortPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getDestPort();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TPFilter#getDestPort <em>Dest Port</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Dest Port</em>' attribute.
     * @see #getDestPort()
     * @generated
     */
    void setDestPort(String value);

} // TPFilter
