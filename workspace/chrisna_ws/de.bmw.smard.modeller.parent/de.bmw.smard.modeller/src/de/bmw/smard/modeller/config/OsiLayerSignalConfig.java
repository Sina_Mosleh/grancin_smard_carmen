package de.bmw.smard.modeller.config;

import java.util.HashSet;
import java.util.Set;

import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.AbstractSignal;
import de.bmw.smard.modeller.conditions.CANMessage;
import de.bmw.smard.modeller.conditions.DoubleSignal;
import de.bmw.smard.modeller.conditions.EthernetMessage;
import de.bmw.smard.modeller.conditions.FlexRayMessage;
import de.bmw.smard.modeller.conditions.HeaderSignal;
import de.bmw.smard.modeller.conditions.IPv4Message;
import de.bmw.smard.modeller.conditions.LINMessage;
import de.bmw.smard.modeller.conditions.NonVerboseDLTMessage;
import de.bmw.smard.modeller.conditions.PluginMessage;
import de.bmw.smard.modeller.conditions.PluginSignal;
import de.bmw.smard.modeller.conditions.SomeIPMessage;
import de.bmw.smard.modeller.conditions.StringSignal;
import de.bmw.smard.modeller.conditions.TCPMessage;
import de.bmw.smard.modeller.conditions.UDPMessage;
import de.bmw.smard.modeller.conditions.VerboseDLTMessage;

/**
 * defines which Messages can contain which Signals
 * @author s.riethig
 *
 */
public class OsiLayerSignalConfig {
	
	private static final Set<Class<? extends AbstractMessage>> MESSAGES_WITH_DOUBLESIGNALS;
	static {
		MESSAGES_WITH_DOUBLESIGNALS = new HashSet<>();
		MESSAGES_WITH_DOUBLESIGNALS.add(CANMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(LINMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(FlexRayMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(EthernetMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(IPv4Message.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(TCPMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(UDPMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(VerboseDLTMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(NonVerboseDLTMessage.class);
		MESSAGES_WITH_DOUBLESIGNALS.add(SomeIPMessage.class);
	}
	
	private static final Set<Class<? extends AbstractMessage>> MESSAGES_WITH_STRINGSIGNALS;
	static {
		MESSAGES_WITH_STRINGSIGNALS = new HashSet<>();
		MESSAGES_WITH_STRINGSIGNALS.add(CANMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(LINMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(FlexRayMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(EthernetMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(IPv4Message.class);
		MESSAGES_WITH_STRINGSIGNALS.add(TCPMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(UDPMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(VerboseDLTMessage.class);
		MESSAGES_WITH_STRINGSIGNALS.add(SomeIPMessage.class);
	}
	
	public static boolean canAddSignalToMessage(AbstractSignal signal, AbstractMessage message) {
		return canAddSignalToMessage(signal.getClass(), message.getClass());
	}

	public static boolean canAddSignalToMessage(Class<? extends AbstractSignal> signal,
			Class<? extends AbstractMessage> message) {
		if(HeaderSignal.class.isAssignableFrom(signal)) {
			return true;
		}
		
		if(PluginSignal.class.isAssignableFrom(signal)) {
			return message.isAssignableFrom(PluginMessage.class);
		}
		
		if(DoubleSignal.class.isAssignableFrom(signal)) {
			return MESSAGES_WITH_DOUBLESIGNALS.stream()
					.filter(m -> m.isAssignableFrom(message))
					.findAny()
					.isPresent();
		}
		
		if(StringSignal.class.isAssignableFrom(signal)) {
			return MESSAGES_WITH_STRINGSIGNALS.stream()
					.filter(m -> m.isAssignableFrom(message))
					.findAny()
					.isPresent();
		}
		
		return false;
	}

}
