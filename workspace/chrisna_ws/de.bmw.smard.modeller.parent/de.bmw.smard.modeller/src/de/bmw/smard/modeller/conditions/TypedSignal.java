package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Typed Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TypedSignal#getEvaluationDataType <em>Evaluation Data Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TypedSignal#getFactor <em>Factor</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TypedSignal#getOffset <em>Offset</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TypedSignal#getSignalDataType <em>Signal Data Type</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTypedSignal()
 * @model
 * @generated
 */
public interface TypedSignal extends AbstractSignal {

    /**
     * Returns the value of the '<em><b>Evaluation Data Type</b></em>' attribute.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.DataType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Evaluation Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Evaluation Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.DataType
     * @see #setEvaluationDataType(DataType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTypedSignal_EvaluationDataType()
     * @model
     * @generated
     */
    DataType getEvaluationDataType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TypedSignal#getEvaluationDataType <em>Evaluation Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Evaluation Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.DataType
     * @see #getEvaluationDataType()
     * @generated
     */
    void setEvaluationDataType(DataType value);

    /**
     * Returns the value of the '<em><b>Factor</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Factor</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Factor</em>' attribute.
     * @see #setFactor(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTypedSignal_Factor()
     * @model dataType="de.bmw.smard.modeller.conditions.DoubleOrTemplatePlaceholder"
     * @generated
     */
    String getFactor();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TypedSignal#getFactor <em>Factor</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Factor</em>' attribute.
     * @see #getFactor()
     * @generated
     */
    void setFactor(String value);

    /**
     * Returns the value of the '<em><b>Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Offset</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Offset</em>' attribute.
     * @see #setOffset(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTypedSignal_Offset()
     * @model dataType="de.bmw.smard.modeller.conditions.DoubleOrTemplatePlaceholder"
     * @generated
     */
    String getOffset();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TypedSignal#getOffset <em>Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Offset</em>' attribute.
     * @see #getOffset()
     * @generated
     */
    void setOffset(String value);

    /**
     * Returns the value of the '<em><b>Signal Data Type</b></em>' attribute.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.SignalDataType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SignalDataType
     * @see #setSignalDataType(SignalDataType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTypedSignal_SignalDataType()
     * @model
     * @generated
     */
    SignalDataType getSignalDataType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TypedSignal#getSignalDataType <em>Signal Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SignalDataType
     * @see #getSignalDataType()
     * @generated
     */
    void setSignalDataType(SignalDataType value);
} // TypedSignal
