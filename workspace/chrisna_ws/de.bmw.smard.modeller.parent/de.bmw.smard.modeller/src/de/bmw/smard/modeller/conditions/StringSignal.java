package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStringSignal()
 * @model
 * @generated
 */
public interface StringSignal extends AbstractSignal {
} // StringSignal
