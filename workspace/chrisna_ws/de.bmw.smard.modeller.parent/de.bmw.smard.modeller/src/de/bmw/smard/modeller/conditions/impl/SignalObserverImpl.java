package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.ObserverValueRangeHelper;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DecodeStrategy;
import de.bmw.smard.modeller.conditions.DoubleDecodeStrategy;
import de.bmw.smard.modeller.conditions.DoubleSignal;
import de.bmw.smard.modeller.conditions.ISignalOrReference;
import de.bmw.smard.modeller.conditions.ObserverValueRange;
import de.bmw.smard.modeller.conditions.PluginSignal;
import de.bmw.smard.modeller.conditions.SignalObserver;
import de.bmw.smard.modeller.conditions.SignalVariable;
import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import java.util.ArrayList;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SignalObserverImpl extends AbstractObserverImpl implements SignalObserver {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SignalObserverImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SIGNAL_OBSERVER;
    }

    @Override
    /** @generated NOT */
    public boolean isValid_hasValidValueRanges(EList<String> errorMessages, EList<String> warningMessages) {

        // only validate normal and generated conditions files
        if (TemplateUtils.getTemplateCategorization(this).isExportable()) { // if not template context -> check if valid
            SignalVariable parent = (SignalVariable) eContainer();
            ISignalOrReference parentsSignal = parent.getSignal();

            if (parentsSignal != null) {
                boolean assumeIntegers = false;
                double rangeConversionFactor = 1.0;
                double rangeConversionOffset = 0.0;

                if (parentsSignal instanceof DoubleSignal && (parentsSignal instanceof PluginSignal) == false) {
                    DecodeStrategy ds = ((DoubleSignal) parentsSignal).getDecodeStrategy();
                    if (ds instanceof DoubleDecodeStrategy) {
                        DoubleDecodeStrategy doubleDs = (DoubleDecodeStrategy) ds;
                        rangeConversionFactor = TemplateUtils.getDoubleNotPlaceholder(doubleDs.getFactor(), false);
                        rangeConversionOffset = TemplateUtils.getDoubleNotPlaceholder(doubleDs.getOffset(), false);
                    }
                }

                // Check if we need a conversion of range values
                boolean variableInterpreted = TemplateUtils.getBooleanNotPlaceholder(parent.getInterpretedValue(), false);

                // Note: to decide if we check on integer boundaries of the value
                // ranges, we use interpreted flag of the
                // variable, not the one of the observer!
                if (!variableInterpreted) {
                    // the ranges will be raw data, use and check integer boundaries
                    assumeIntegers = true;
                }

                List<String> rangesAsStrings = new ArrayList<String>();
                if (valueRanges != null) {
                    for (ObserverValueRange obsValRange : valueRanges) {
                        if (obsValRange != null) {
                            rangesAsStrings.add(obsValRange.getValue());
                        }
                    }
                    if (!rangesAsStrings.isEmpty()) {
                        ObserverValueRangeHelper.validateValueRangeTexts(
                                rangesAsStrings, rangeConversionFactor, rangeConversionOffset,
                                assumeIntegers, errorMessages, warningMessages);
                    }
                }
            }
        } else {
            // if there is nothing specified as TmplParam -> check if Value Ranges are correct
            if (!StringUtils.nullOrEmpty(valueRangesTmplParam)) {
                String errorMessage = TemplateUtils.checkValidPlaceholderName(valueRangesTmplParam);
                if (errorMessage != null) {
                    errorMessages.add(errorMessage);
                }
            }
        }

        // for the purpose of reducing the value range gap warnings, this method is called by VariableSetImpl
        // which then works with the error- and warningMessages
        return errorMessages.isEmpty() && warningMessages.isEmpty();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (eContainer() instanceof SignalVariable) {
            SignalVariable var = (SignalVariable) eContainer();
            return var.getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

}
