package de.bmw.smard.modeller.util;

import de.bmw.smard.modeller.conditions.AbstractVariable;
import org.eclipse.core.resources.*;
import org.eclipse.emf.ecore.EObject;

import java.util.*;

public class VariableManager implements IResourceChangeListener {

    private static Map<String, VariableManager> instances = new HashMap<String, VariableManager>(5);

    private Map<String, AbstractVariable> variableMap;

    private VariableManager(EObject objectInProject) {
        ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
        variableMap = new HashMap<String, AbstractVariable>();
        List<AbstractVariable> variables = EMFUtils.getObjectsInProject(AbstractVariable.class,
                ResourceUtils.CONDITIONS_FILEEXTENSION,
                objectInProject,
                false,
                false,
                false);
        for (AbstractVariable var : variables) {
            variableMap.put(var.getName(), var);
        }
    }


    /**
     * get any EObject of a modeller-project and return an instance of VariableManager
     * containing a List of all Variables in the project
     * 
     * @param objectInProject
     * @return instance of VariableManager
     */
    public static VariableManager getInstance(EObject objectInProject) {
        IProject eclipseProject = EMFUtils.getEclipseProject(objectInProject.eResource());
        if(eclipseProject == null) {
            return null;
        }
        String projectName = eclipseProject.getName();
        VariableManager vm = instances.get(projectName);
        if (vm == null) {
            vm = new VariableManager(objectInProject);
            instances.put(projectName, vm);
        }
        return vm;
    }

    public AbstractVariable getVariableByName(String varName) {
        return variableMap.get(varName);
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        IResourceDelta delta = event.getDelta();
        IResourceDelta project = delta.getAffectedChildren()[0];
        for (int i = 0; i < project.getAffectedChildren().length; i++) {
            IResourceDelta file = project.getAffectedChildren()[i];
            if (file.getProjectRelativePath().toString().toLowerCase().contains(ResourceUtils.CONDITIONS_FILEEXTENSION)) {
                for (VariableManager vm : instances.values()) {
                    ResourcesPlugin.getWorkspace().removeResourceChangeListener(vm);
                }
                instances.clear();
                return;
            }
        }
    }
}
