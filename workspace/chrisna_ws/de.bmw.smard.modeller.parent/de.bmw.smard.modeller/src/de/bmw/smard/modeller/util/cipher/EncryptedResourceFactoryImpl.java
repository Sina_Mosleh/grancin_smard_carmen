package de.bmw.smard.modeller.util.cipher;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class EncryptedResourceFactoryImpl extends XMIResourceFactoryImpl {

	public EncryptedResourceFactoryImpl() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl#createResource(org.
	 * eclipse.emf.common.util.URI)
	 */
	public Resource createResource(URI uri) {
		XMLResource result = new EncryptedResourceImpl(uri);
		return result;
	}
}