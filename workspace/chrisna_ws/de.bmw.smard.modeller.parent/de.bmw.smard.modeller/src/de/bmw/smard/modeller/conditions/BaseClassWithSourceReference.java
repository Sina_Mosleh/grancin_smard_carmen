package de.bmw.smard.modeller.conditions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Class With Source Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference <em>Source Reference</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getBaseClassWithSourceReference()
 * @model abstract="true"
 * @generated
 */
public interface BaseClassWithSourceReference extends BaseClassWithID {
    /**
     * Returns the value of the '<em><b>Source Reference</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source Reference</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Source Reference</em>' containment reference.
     * @see #isSetSourceReference()
     * @see #unsetSourceReference()
     * @see #setSourceReference(SourceReference)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getBaseClassWithSourceReference_SourceReference()
     * @model containment="true" unsettable="true"
     * @generated
     */
    SourceReference getSourceReference();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference <em>Source Reference</em>}' containment
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Source Reference</em>' containment reference.
     * @see #isSetSourceReference()
     * @see #unsetSourceReference()
     * @see #getSourceReference()
     * @generated
     */
    void setSourceReference(SourceReference value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference <em>Source Reference</em>}' containment
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetSourceReference()
     * @see #getSourceReference()
     * @see #setSourceReference(SourceReference)
     * @generated
     */
    void unsetSourceReference();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference#getSourceReference <em>Source Reference</em>}'
     * containment reference is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Source Reference</em>' containment reference is set.
     * @see #unsetSourceReference()
     * @see #getSourceReference()
     * @see #setSourceReference(SourceReference)
     * @generated
     */
    boolean isSetSourceReference();

} // BaseClassWithSourceReference
