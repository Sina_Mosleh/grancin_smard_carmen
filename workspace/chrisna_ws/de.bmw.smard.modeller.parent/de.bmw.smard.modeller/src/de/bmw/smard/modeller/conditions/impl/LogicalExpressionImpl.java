package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.Expression;
import de.bmw.smard.modeller.conditions.LogicalExpression;
import de.bmw.smard.modeller.conditions.LogicalOperatorType;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Logical Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl#getExpressions <em>Expressions</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl#getOperator <em>Operator</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LogicalExpressionImpl#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LogicalExpressionImpl extends ExpressionImpl implements LogicalExpression {
    /**
     * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpressions()
     * @generated
     * @ordered
     */
    protected EList<Expression> expressions;

    /**
     * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected static final LogicalOperatorType OPERATOR_EDEFAULT = LogicalOperatorType.AND;

    /**
     * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperator()
     * @generated
     * @ordered
     */
    protected LogicalOperatorType operator = OPERATOR_EDEFAULT;

    /**
     * The default value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperatorTmplParam()
     * @generated
     * @ordered
     */
    protected static final String OPERATOR_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperatorTmplParam()
     * @generated
     * @ordered
     */
    protected String operatorTmplParam = OPERATOR_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected LogicalExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        for (Expression exp : getExpressions()) {
            messages.addAll(exp.getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.LOGICAL_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Expression> getExpressions() {
        if (expressions == null) {
            expressions = new EObjectContainmentEList<Expression>(Expression.class, this, ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS);
        }
        return expressions;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public LogicalOperatorType getOperator() {
        return operator;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOperator(LogicalOperatorType newOperator) {
        LogicalOperatorType oldOperator = operator;
        operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR, oldOperator, operator));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getOperatorTmplParam() {
        return operatorTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOperatorTmplParam(String newOperatorTmplParam) {
        String oldOperatorTmplParam = operatorTmplParam;
        operatorTmplParam = newOperatorTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM, oldOperatorTmplParam, operatorTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidOperator(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_OPERATOR)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(
                        operator == LogicalOperatorType.NAND
                            || operator == LogicalOperatorType.NOR
                            || operator == LogicalOperatorType.NOT)
                        .error("_Validation_Conditions_LogicalExpression_Operator_OperatorIsDeprecated")
                        .build())

                .with(Validators.notNull(operator)
                        .error("_Validation_Conditions_LogicalExpression_Operator_IsNull")
                        .build())

                .with(Validators.checkTemplate(operator)
                        .templateType(LogicalOperatorType.TEMPLATE_DEFINED)
                        .tmplParam(operatorTmplParam)
                        .containsParameterError("_Validation_Conditions_LogicalExpression_Operator_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_LogicalExpression_OperatorTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_LogicalExpression_OperatorTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(LogicalOperatorType.OR, LogicalOperatorType.AND),
                                "_Validation_Conditions_LogicalExpression_Operator_IllegalOperator")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */ //todo validation poor naming: better: isValid_hasExactly2Operands
    public boolean isValid_hasValidSubExpressions(DiagnosticChain diagnostics, Map<Object, Object> context) {
        String warningMessageIdentifier = "_Validation_Conditions_LogicalExpression_Expressions_WARNING";
        if (expressions != null && expressions.size() < 2) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_SUB_EXPRESSIONS)
                        .messageId(PluginActivator.INSTANCE, warningMessageIdentifier, expressions.size())
                        .data(this)
                        .build());
            }
        }
        return true; // as we add only warnings always return true from here
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
                return ((InternalEList<?>) getExpressions()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
                return getExpressions();
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
                return getOperator();
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
                return getOperatorTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
                getExpressions().clear();
                getExpressions().addAll((Collection<? extends Expression>) newValue);
                return;
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
                setOperator((LogicalOperatorType) newValue);
                return;
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
                setOperatorTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
                getExpressions().clear();
                return;
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
                setOperator(OPERATOR_EDEFAULT);
                return;
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
                setOperatorTmplParam(OPERATOR_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
                return expressions != null && !expressions.isEmpty();
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
                return operator != OPERATOR_EDEFAULT;
            case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
                return OPERATOR_TMPL_PARAM_EDEFAULT == null ? operatorTmplParam != null : !OPERATOR_TMPL_PARAM_EDEFAULT.equals(operatorTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (operator: ");
        result.append(operator);
        result.append(", operatorTmplParam: ");
        result.append(operatorTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>();
        for (Expression expression : getExpressions()) {
            result.addAll(expression.getReadVariables());
        }
        return result;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        EList<String> result = new BasicEList<String>();
        for (Expression expression : getExpressions()) {
            result.addAll(expression.getWriteVariables());
        }
        return result;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<AbstractState> getStateDependencies() {
        EList<AbstractState> result = new BasicEList<AbstractState>();
        for (Expression expression : expressions) {
            result.addAll(expression.getStateDependencies());
        }
        return result;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<Transition> getTransitionDependencies() {
        EList<Transition> result = new BasicEList<Transition>();
        for (Expression expression : expressions) {
            result.addAll(expression.getTransitionDependencies());
        }
        return result;
    }

} // LogicalExpressionImpl
