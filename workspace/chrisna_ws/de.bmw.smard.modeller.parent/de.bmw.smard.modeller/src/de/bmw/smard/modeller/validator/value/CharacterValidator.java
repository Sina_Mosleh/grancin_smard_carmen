package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class CharacterValidator implements ValueValidator {

    private final String errorId;

    CharacterValidator(String errorId) {
        this.errorId = errorId;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {
        final int len = value.length();
        char c;
        for (int i = 0; i < len; i++) {
            c = value.charAt(i);
            if (!Character.isLetter(c) && !(i == 0 || Character.isDigit(c)) && '_' != c) {
                if(baseValidator != null) {
                    baseValidator.attach(Severity.ERROR, errorId, null);
                }
                return false;
            }
        }
        return true;
    }
}
