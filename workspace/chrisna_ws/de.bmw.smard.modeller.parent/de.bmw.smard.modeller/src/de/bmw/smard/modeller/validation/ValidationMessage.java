package de.bmw.smard.modeller.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationMessage {

    private final Severity severity;
    private final String source;
    private final int code;
    private final String message;
    private final List<Object> data;

    public Severity getSeverity() {
        return severity;
    }

    public String getSource() {
        return source;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public List<Object> getData() {
        return new ArrayList<>(data);
    }

    public ValidationMessage(Severity severity, String source, int code, String message, List<Object> data) {

        this.severity = severity;
        this.source = source;
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Throwable getException() {
        if(data != null) {

            for(Object o : data) {
                if(o instanceof Throwable) {
                    return (Throwable)o;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "ValidationMessage{" +
                "severity=" + severity +
                ", source='" + source + '\'' +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
