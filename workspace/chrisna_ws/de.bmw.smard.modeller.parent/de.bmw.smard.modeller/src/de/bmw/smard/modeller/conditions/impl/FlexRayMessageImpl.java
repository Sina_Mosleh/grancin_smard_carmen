package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.FlexRayFilter;
import de.bmw.smard.modeller.conditions.FlexRayMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flex Ray Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageImpl#getFlexRayFilter <em>Flex Ray Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlexRayMessageImpl extends AbstractBusMessageImpl implements FlexRayMessage {
    /**
     * The cached value of the '{@link #getFlexRayFilter() <em>Flex Ray Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFlexRayFilter()
     * @generated
     * @ordered
     */
    protected FlexRayFilter flexRayFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FlexRayMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.FLEX_RAY_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayFilter getFlexRayFilter() {
        return flexRayFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetFlexRayFilter(FlexRayFilter newFlexRayFilter, NotificationChain msgs) {
        FlexRayFilter oldFlexRayFilter = flexRayFilter;
        flexRayFilter = newFlexRayFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER, oldFlexRayFilter, newFlexRayFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFlexRayFilter(FlexRayFilter newFlexRayFilter) {
        if (newFlexRayFilter != flexRayFilter) {
            NotificationChain msgs = null;
            if (flexRayFilter != null)
                msgs = ((InternalEObject) flexRayFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER,
                        null, msgs);
            if (newFlexRayFilter != null)
                msgs = ((InternalEObject) newFlexRayFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER,
                        null, msgs);
            msgs = basicSetFlexRayFilter(newFlexRayFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER, newFlexRayFilter, newFlexRayFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER:
                return basicSetFlexRayFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER:
                return getFlexRayFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER:
                setFlexRayFilter((FlexRayFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER:
                setFlexRayFilter((FlexRayFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE__FLEX_RAY_FILTER:
                return flexRayFilter != null;
        }
        return super.eIsSet(featureID);
    }

    @Override
    public AbstractFilter getPrimaryFilter() {
        return flexRayFilter;
    }

} //FlexRayMessageImpl
