/**
 *
 */
package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractVariable;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.StructureVariable;
import de.bmw.smard.modeller.conditions.VariableReference;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Structure Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StructureVariableImpl#getVariables <em>Variables</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StructureVariableImpl#getVariableReferences <em>Variable References</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StructureVariableImpl extends AbstractVariableImpl implements StructureVariable {
    /**
     * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVariables()
     * @generated
     * @ordered
     */
    protected EList<AbstractVariable> variables;

    /**
     * The cached value of the '{@link #getVariableReferences() <em>Variable References</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVariableReferences()
     * @generated
     * @ordered
     */
    protected EList<VariableReference> variableReferences;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StructureVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.STRUCTURE_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractVariable> getVariables() {
        if (variables == null) {
            variables = new EObjectContainmentEList<AbstractVariable>(AbstractVariable.class, this, ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES);
        }
        return variables;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<VariableReference> getVariableReferences() {
        if (variableReferences == null) {
            variableReferences =
                    new EObjectContainmentEList<VariableReference>(VariableReference.class, this, ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES);
        }
        return variableReferences;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        for (AbstractVariable var : getAllInheritedVariables(true)) {
            messages.addAll(var.getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES:
                return ((InternalEList<?>) getVariables()).basicRemove(otherEnd, msgs);
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES:
                return ((InternalEList<?>) getVariableReferences()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES:
                return getVariables();
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES:
                return getVariableReferences();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES:
                getVariables().clear();
                getVariables().addAll((Collection<? extends AbstractVariable>) newValue);
                return;
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES:
                getVariableReferences().clear();
                getVariableReferences().addAll((Collection<? extends VariableReference>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES:
                getVariables().clear();
                return;
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES:
                getVariableReferences().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLES:
                return variables != null && !variables.isEmpty();
            case ConditionsPackage.STRUCTURE_VARIABLE__VARIABLE_REFERENCES:
                return variableReferences != null && !variableReferences.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public Set<AbstractVariable> getAllInheritedVariables(boolean includeReferences) {
        Set<AbstractVariable> variables = new HashSet<AbstractVariable>();
        for (AbstractVariable abstractVariable : this.getVariables()) {
            if (abstractVariable instanceof StructureVariable) {
                variables.addAll(((StructureVariable) abstractVariable).getAllInheritedVariables(includeReferences));
            } else {
                variables.add(abstractVariable);
            }
        }
        //Add all Variables referenced in a VariableReference into the List
        if (includeReferences) {
            for (VariableReference reference : this.getVariableReferences()) {
                variables.add(reference.getVariable());
            }
        }
        return variables;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        EList<String> readVariables = new BasicEList<String>();
        for (AbstractVariable var : getAllInheritedVariables(true)) {
            readVariables.add(var.getName());
        }
        return readVariables;
    }

} //StructureVariableImpl
