package de.bmw.smard.modeller.util.migration.to770;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.ShowVariableActionExpressionParser;
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty;
import de.bmw.smard.modeller.util.migration.MigrationMarkerUtil;

public class CentralizeVariableAttributesMigration extends CustomMigration {
	
	private static final String ACTION = "statemachine.Action";
	private static final String ACTION_TYPE = "actionType";
	private static final String ACTION_EXPRESSION = "actionExpression";

	private static final String CONDITIONS_VARIABLE = "conditions.Variable";
	private static final String VARIABLE_NAME = "name";

	private static final String SIGNAL_VARIABLE = "conditions.SignalVariable";
	private static final String VALUE_VARIABLE = "conditions.ValueVariable";
	private static final String SIGNAL_VARIABLE_OBSERVERS = "signalObservers";
	private static final String VALUE_VARIABLE_OBSERVERS = "valueVariableObservers";

	private static final String FORMAT = "format";
	private static final String UNIT = "unit";

	private Map<Instance, SortedSet<String>> variableToFormatsMap = new HashMap<>();
	private Map<Instance, SortedSet<String>> variableToUnitsMap = new HashMap<>();

	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		variableToFormatsMap.putAll(collectShowVariablesAndTheirFormats(model));

		variableToUnitsMap.putAll(collectVariablesAndTheirUnits(model, SIGNAL_VARIABLE, SIGNAL_VARIABLE_OBSERVERS));
		variableToUnitsMap.putAll(collectVariablesAndTheirUnits(model, VALUE_VARIABLE, VALUE_VARIABLE_OBSERVERS));
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
		updateVariables(variableToFormatsMap, FORMAT);
		updateVariables(variableToUnitsMap, UNIT);
	}

	private void updateVariables(Map<Instance, SortedSet<String>> mapToAttribute, String attribute) {
		for(Entry<Instance, SortedSet<String>> entry : mapToAttribute.entrySet()) {
			Instance variable = entry.getKey();
			String newAttribute = entry.getValue().first();
			variable.set(attribute, newAttribute);
			createInfoMessage(variable, entry.getValue(), newAttribute, attribute);
		}
	}

	private Map<? extends Instance, ? extends SortedSet<String>> collectShowVariablesAndTheirFormats(Model model) {
		Map<Instance, SortedSet<String>> result = new HashMap<>();

		Map<String, Instance> variableNameMap = getMapOfAllVariableNames(model);
		// if there are no variables in project we can immediately return
		if(variableNameMap.isEmpty()) {
			return result;
		}

		// iterate over SHOWVARIABLE Actions and their actionExpression 
		for(Instance action : model.getAllInstances(ACTION)) {
			EEnumLiteral actionType = action.get(ACTION_TYPE);
			if(actionType.getName().equals(ActionTypeNotEmpty.SHOWVARIABLE.getName())) {
				String actionExpression = action.get(ACTION_EXPRESSION);
				// parse actionExpression and if there is one with "format" -> put variable and format in map
				if(!StringUtils.nullOrEmpty(actionExpression)) {
					Map<String, Object> actionExpressionMapping = 
							ShowVariableActionExpressionParser.parseShowVariableParameters(actionExpression, false);
					createVariableFormatMapping(result, variableNameMap, 
							extractVariableName(actionExpressionMapping), extractFormat(actionExpressionMapping));
					removeFormatFromActionExpression(actionExpressionMapping, action);
				}
			}
		}
		return result;
	}

	private void createVariableFormatMapping(Map<Instance, SortedSet<String>> result, Map<String, Instance> variableNameMap,
			String variableName, String format) {
		if(!StringUtils.nullOrEmpty(variableName) && !StringUtils.nullOrEmpty(format)) {
			Instance variable = variableNameMap.get(variableName);
			if(variable != null) {
				result.computeIfAbsent(variable, k -> new TreeSet<>(Comparator.naturalOrder())).add(format);
			}
		}
	}

	/**
	 * former actionExpression ("ValVar, format=[format], logLevel=...") 
	 * remove the format key value pair ("ValVar, logLevel=...")
	 * @param actionExpressionMapping
	 * @param action
	 */
	private void removeFormatFromActionExpression(Map<String, Object> actionExpressionMapping, Instance action) {
		// remove key value pair
		actionExpressionMapping.remove(ShowVariableActionExpressionParser.PARAM_KEY_FORMAT);

		// set Action's actionExpression to new value (null or empty would be ok)
		if(action != null && !actionExpressionMapping.isEmpty() ) {
			action.set(ACTION_EXPRESSION, getUpdatedActionExpression(actionExpressionMapping));
		}
	}
	
	/**
	 * remove "format=[...]" from actionExpression 
	 * @param actionExpressionMapping
	 * @return updated actionExpression String
	 */
	private String getUpdatedActionExpression(Map<String, Object> actionExpressionMapping) {
		// actionExpression should start with variable name
		String updatedActionExpression = (String) actionExpressionMapping.get(ShowVariableActionExpressionParser.PARAM_KEY_VALUE);
		// then add keys and values separated by comma
		for(Entry<String, Object> entry : actionExpressionMapping.entrySet()) {
			if(!ShowVariableActionExpressionParser.PARAM_KEY_VALUE.equals(entry.getKey())) {
				updatedActionExpression = updatedActionExpression.concat(", " + entry.getKey() + "=" + entry.getValue());
			}
		}
		return updatedActionExpression;
	}

	/**
	 * get name of referenced Variable from actionExpression
	 * @param actionExpressionMapping
	 * @return variableName
	 */
	private String extractVariableName(Map<String, Object> actionExpressionMapping) {
		Object variableName = actionExpressionMapping.get(ShowVariableActionExpressionParser.PARAM_KEY_VALUE);
		if(variableName instanceof String) {
			return (String) variableName;
		}
		return "";
	}

	/**
	 * get the value of "format=[value]" from actionExpression
	 * @param actionExpressionMapping
	 * @return format
	 */
	private String extractFormat(Map<String, Object> actionExpressionMapping) {
		Object format = actionExpressionMapping.get(ShowVariableActionExpressionParser.PARAM_KEY_FORMAT);
		if(format instanceof String) {
			return (String) format;
		}
		return "";
	}

	private Map<String, Instance> getMapOfAllVariableNames(Model model) {
		Map<String, Instance> result = new HashMap<>();
		for(Instance variable : model.getAllInstances(CONDITIONS_VARIABLE)) {
			String nameOfVariable = variable.get(VARIABLE_NAME);
			if(!StringUtils.nullOrEmpty(nameOfVariable)) {
				result.put(nameOfVariable, variable);
			}
		}

		return result;
	}

	/**
	 * iterate over Observers of a Variable to collect all possible unit attributes
	 * @param model
	 * @param className
	 * @param attributeName
	 * @return map of one Variable to a Set<String> of collected units
	 */
	private Map<Instance, SortedSet<String>> collectVariablesAndTheirUnits(Model model, String className, String attributeName) {
		Map<Instance, SortedSet<String>> result = new HashMap<>();

		for(Instance variable : model.getAllInstances(className)) {
			Object referencedObservers = variable.get(attributeName);
			if(referencedObservers instanceof List) {
				List<Instance> observers = (List<Instance>) referencedObservers;
				SortedSet<String> unitsOfObservers = new TreeSet<>(Comparator.naturalOrder());
				for(Instance observer : observers) {
					if(observer.get(UNIT) != null) {
						unitsOfObservers.add(observer.get(UNIT));
					}
				}
				if(!unitsOfObservers.isEmpty()) {
					result.put(variable, unitsOfObservers);
				}
			}
		}
		return result;
	}

	/**
	 * inform user about which Variables have been updated and which unit/format was chosen
	 * @param variableName
	 * @param values - all the possible attributes 
	 * @param attribute
	 */
	private void createInfoMessage(Instance variable, Set<String> values, String chosenValue, String attribute) {
		// if there were more units or formats, inform user which one was chosen
		if(values != null && values.size() > 1) {
			Object variableName = variable.get(VARIABLE_NAME);
			if(variableName instanceof String && !StringUtils.nullOrEmpty((String) variableName)) {
				String list = values.stream().collect(Collectors.joining(", "));
				String message = String.format("Migration of %1$s for variable %2$s"
						+ " found following %1$ss: %3$s"
						+ " ===> was set to: %4$s", attribute, variableName, list, chosenValue);
				MigrationMarkerUtil.createInfo(message);
			}
		}
	}
}