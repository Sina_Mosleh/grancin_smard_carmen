package de.bmw.smard.modeller.conditions.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.XMLHelper;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.util.ConditionsResourceFactoryImpl
 * @generated
 */
public class ConditionsResourceImpl extends XMLResourceImpl {
    /**
     * Creates an instance of the resource.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param uri the URI of the new resource.
     * @generated
     */
    public ConditionsResourceImpl(URI uri) {
        super(uri);
    }

    /** @generated NOT but added */
    @Override
    protected XMLHelper createXMLHelper() {
        return new ConditionsXMLHelperImpl(this);
    }

} //ConditionsResourceImpl
