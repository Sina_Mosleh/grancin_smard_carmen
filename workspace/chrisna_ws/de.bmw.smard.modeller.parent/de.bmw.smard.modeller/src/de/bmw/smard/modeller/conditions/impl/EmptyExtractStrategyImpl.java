package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.EmptyExtractStrategy;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Empty Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmptyExtractStrategyImpl extends ExtractStrategyImpl implements EmptyExtractStrategy {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected EmptyExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.EMPTY_EXTRACT_STRATEGY;
    }

} //EmptyExtractStrategyImpl
