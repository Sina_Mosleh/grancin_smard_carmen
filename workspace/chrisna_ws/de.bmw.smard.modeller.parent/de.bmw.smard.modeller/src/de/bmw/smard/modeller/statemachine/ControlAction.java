package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.AbstractObserver;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.ControlAction#getControlType <em>Control Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.ControlAction#getStateMachines <em>State Machines</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.ControlAction#getObservers <em>Observers</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getControlAction()
 * @model
 * @generated
 */
public interface ControlAction extends AbstractAction {
    /**
     * Returns the value of the '<em><b>Control Type</b></em>' attribute.
     * The default value is <code>"ON"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.statemachine.ControlType}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Control Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.ControlType
     * @see #setControlType(ControlType)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getControlAction_ControlType()
     * @model default="ON" required="true"
     * @generated
     */
    ControlType getControlType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.ControlAction#getControlType <em>Control Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Control Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.ControlType
     * @see #getControlType()
     * @generated
     */
    void setControlType(ControlType value);

    /**
     * Returns the value of the '<em><b>State Machines</b></em>' reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.statemachine.StateMachine}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>State Machines</em>' reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getControlAction_StateMachines()
     * @model
     * @generated
     */
    EList<StateMachine> getStateMachines();

    /**
     * Returns the value of the '<em><b>Observers</b></em>' reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractObserver}.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Observers</em>' reference list.
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getControlAction_Observers()
     * @model
     * @generated
     */
    EList<AbstractObserver> getObservers();

} // ControlAction
