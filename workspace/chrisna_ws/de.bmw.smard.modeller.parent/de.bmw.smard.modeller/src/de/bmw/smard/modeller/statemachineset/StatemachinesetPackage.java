/**
 * <copyright>
 * </copyright>
 * $Id$
 */
package de.bmw.smard.modeller.statemachineset;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 * @generated
 */
public interface StatemachinesetPackage extends EPackage {
    /**
     * The package name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "statemachineset";

    /**
     * The package namespace URI.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/statemachineset";

    /**
     * The package namespace name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "statemachineset";

    /**
     * The package content type ID.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    String eCONTENT_TYPE = "de.bmw.smard.modeller.statemachineset";

    /**
     * The singleton instance of the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    StatemachinesetPackage eINSTANCE = de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl.init();

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.DocumentRootImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getDocumentRoot()
     * @generated
     */
    int DOCUMENT_ROOT = 0;

    /**
     * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 0;

    /**
     * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 1;

    /**
     * The feature id for the '<em><b>State Machine Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT__STATE_MACHINE_SET = 2;

    /**
     * The number of structural features of the '<em>Document Root</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int DOCUMENT_ROOT_FEATURE_COUNT = 3;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl <em>General Info</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getGeneralInfo()
     * @generated
     */
    int GENERAL_INFO = 1;

    /**
     * The feature id for the '<em><b>Author</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int GENERAL_INFO__AUTHOR = 0;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int GENERAL_INFO__DESCRIPTION = 1;

    /**
     * The feature id for the '<em><b>Topic</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int GENERAL_INFO__TOPIC = 2;

    /**
     * The feature id for the '<em><b>Department</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int GENERAL_INFO__DEPARTMENT = 3;

    /**
     * The number of structural features of the '<em>General Info</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int GENERAL_INFO_FEATURE_COUNT = 4;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.OutputImpl <em>Output</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.OutputImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getOutput()
     * @generated
     */
    int OUTPUT = 2;

    /**
     * The feature id for the '<em><b>Target File Path</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OUTPUT__TARGET_FILE_PATH = 0;

    /**
     * The number of structural features of the '<em>Output</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int OUTPUT_FEATURE_COUNT = 1;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.BusMessageReferable <em>Bus Message Referable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.BusMessageReferable
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getBusMessageReferable()
     * @generated
     */
    int BUS_MESSAGE_REFERABLE = 6;

    /**
     * The number of structural features of the '<em>Bus Message Referable</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int BUS_MESSAGE_REFERABLE_FEATURE_COUNT = 0;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.StateMachineSetImpl <em>State Machine Set</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.StateMachineSetImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getStateMachineSet()
     * @generated
     */
    int STATE_MACHINE_SET = 3;

    /**
     * The feature id for the '<em><b>General Info</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__GENERAL_INFO = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Output</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__OUTPUT = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Export Version Counter</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__EXPORT_VERSION_COUNTER = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__ID = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Execution Config</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__EXECUTION_CONFIG = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Observers</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__OBSERVERS = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>State Machines</b></em>' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET__STATE_MACHINES = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>State Machine Set</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int STATE_MACHINE_SET_FEATURE_COUNT = BUS_MESSAGE_REFERABLE_FEATURE_COUNT + 7;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.KeyValuePairUserConfigImpl <em>Key Value Pair User Config</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.KeyValuePairUserConfigImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getKeyValuePairUserConfig()
     * @generated
     */
    int KEY_VALUE_PAIR_USER_CONFIG = 4;

    /**
     * The feature id for the '<em><b>Key</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int KEY_VALUE_PAIR_USER_CONFIG__KEY = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int KEY_VALUE_PAIR_USER_CONFIG__VALUE = 1;

    /**
     * The number of structural features of the '<em>Key Value Pair User Config</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int KEY_VALUE_PAIR_USER_CONFIG_FEATURE_COUNT = 2;

    /**
     * The meta object id for the '{@link de.bmw.smard.modeller.statemachineset.impl.ExecutionConfigImpl <em>Execution Config</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see de.bmw.smard.modeller.statemachineset.impl.ExecutionConfigImpl
     * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getExecutionConfig()
     * @generated
     */
    int EXECUTION_CONFIG = 5;

    /**
     * The feature id for the '<em><b>Needs Ethernet</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG__NEEDS_ETHERNET = 0;

    /**
     * The feature id for the '<em><b>Needs Error Frames</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG__NEEDS_ERROR_FRAMES = 1;

    /**
     * The feature id for the '<em><b>Key Value Pairs</b></em>' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG__KEY_VALUE_PAIRS = 2;

    /**
     * The feature id for the '<em><b>Log Level</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG__LOG_LEVEL = 3;

    /**
     * The feature id for the '<em><b>Sorter Value</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG__SORTER_VALUE = 4;

    /**
     * The number of structural features of the '<em>Execution Config</em>' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int EXECUTION_CONFIG_FEATURE_COUNT = 5;

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.DocumentRoot <em>Document Root</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Document Root</em>'.
     * @see de.bmw.smard.modeller.statemachineset.DocumentRoot
     * @generated
     */
    EClass getDocumentRoot();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.statemachineset.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
     * @see de.bmw.smard.modeller.statemachineset.DocumentRoot#getXMLNSPrefixMap()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XMLNSPrefixMap();

    /**
     * Returns the meta object for the map '{@link de.bmw.smard.modeller.statemachineset.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the map '<em>XSI Schema Location</em>'.
     * @see de.bmw.smard.modeller.statemachineset.DocumentRoot#getXSISchemaLocation()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_XSISchemaLocation();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachineset.DocumentRoot#getStateMachineSet <em>State Machine
     * Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>State Machine Set</em>'.
     * @see de.bmw.smard.modeller.statemachineset.DocumentRoot#getStateMachineSet()
     * @see #getDocumentRoot()
     * @generated
     */
    EReference getDocumentRoot_StateMachineSet();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.GeneralInfo <em>General Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>General Info</em>'.
     * @see de.bmw.smard.modeller.statemachineset.GeneralInfo
     * @generated
     */
    EClass getGeneralInfo();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.GeneralInfo#getAuthor <em>Author</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Author</em>'.
     * @see de.bmw.smard.modeller.statemachineset.GeneralInfo#getAuthor()
     * @see #getGeneralInfo()
     * @generated
     */
    EAttribute getGeneralInfo_Author();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.GeneralInfo#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see de.bmw.smard.modeller.statemachineset.GeneralInfo#getDescription()
     * @see #getGeneralInfo()
     * @generated
     */
    EAttribute getGeneralInfo_Description();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.GeneralInfo#getTopic <em>Topic</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Topic</em>'.
     * @see de.bmw.smard.modeller.statemachineset.GeneralInfo#getTopic()
     * @see #getGeneralInfo()
     * @generated
     */
    EAttribute getGeneralInfo_Topic();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.GeneralInfo#getDepartment <em>Department</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Department</em>'.
     * @see de.bmw.smard.modeller.statemachineset.GeneralInfo#getDepartment()
     * @see #getGeneralInfo()
     * @generated
     */
    EAttribute getGeneralInfo_Department();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.Output <em>Output</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Output</em>'.
     * @see de.bmw.smard.modeller.statemachineset.Output
     * @generated
     */
    EClass getOutput();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.Output#getTargetFilePath <em>Target File Path</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Target File Path</em>'.
     * @see de.bmw.smard.modeller.statemachineset.Output#getTargetFilePath()
     * @see #getOutput()
     * @generated
     */
    EAttribute getOutput_TargetFilePath();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet <em>State Machine Set</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>State Machine Set</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet
     * @generated
     */
    EClass getStateMachineSet();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getGeneralInfo <em>General
     * Info</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>General Info</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getGeneralInfo()
     * @see #getStateMachineSet()
     * @generated
     */
    EReference getStateMachineSet_GeneralInfo();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getOutput <em>Output</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Output</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getOutput()
     * @see #getStateMachineSet()
     * @generated
     */
    EReference getStateMachineSet_Output();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getStateMachines <em>State Machines</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>State Machines</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getStateMachines()
     * @see #getStateMachineSet()
     * @generated
     */
    EReference getStateMachineSet_StateMachines();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getExportVersionCounter <em>Export Version
     * Counter</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Export Version Counter</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getExportVersionCounter()
     * @see #getStateMachineSet()
     * @generated
     */
    EAttribute getStateMachineSet_ExportVersionCounter();

    /**
     * Returns the meta object for the reference list '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getObservers <em>Observers</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the reference list '<em>Observers</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getObservers()
     * @see #getStateMachineSet()
     * @generated
     */
    EReference getStateMachineSet_Observers();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getId <em>Id</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getId()
     * @see #getStateMachineSet()
     * @generated
     */
    EAttribute getStateMachineSet_Id();

    /**
     * Returns the meta object for the containment reference '{@link de.bmw.smard.modeller.statemachineset.StateMachineSet#getExecutionConfig <em>Execution
     * Config</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Execution Config</em>'.
     * @see de.bmw.smard.modeller.statemachineset.StateMachineSet#getExecutionConfig()
     * @see #getStateMachineSet()
     * @generated
     */
    EReference getStateMachineSet_ExecutionConfig();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig <em>Key Value Pair User Config</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Key Value Pair User Config</em>'.
     * @see de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig
     * @generated
     */
    EClass getKeyValuePairUserConfig();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig#getKey <em>Key</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Key</em>'.
     * @see de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig#getKey()
     * @see #getKeyValuePairUserConfig()
     * @generated
     */
    EAttribute getKeyValuePairUserConfig_Key();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig#getValue <em>Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig#getValue()
     * @see #getKeyValuePairUserConfig()
     * @generated
     */
    EAttribute getKeyValuePairUserConfig_Value();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig <em>Execution Config</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Execution Config</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig
     * @generated
     */
    EClass getExecutionConfig();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getLogLevel <em>Log Level</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Log Level</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig#getLogLevel()
     * @see #getExecutionConfig()
     * @generated
     */
    EAttribute getExecutionConfig_LogLevel();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getSorterValue <em>Sorter Value</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Sorter Value</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig#getSorterValue()
     * @see #getExecutionConfig()
     * @generated
     */
    EAttribute getExecutionConfig_SorterValue();

    /**
     * Returns the meta object for class '{@link de.bmw.smard.modeller.statemachineset.BusMessageReferable <em>Bus Message Referable</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Bus Message Referable</em>'.
     * @see de.bmw.smard.modeller.statemachineset.BusMessageReferable
     * @generated
     */
    EClass getBusMessageReferable();

    /**
     * Returns the meta object for the containment reference list '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#getKeyValuePairs <em>Key Value
     * Pairs</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Key Value Pairs</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig#getKeyValuePairs()
     * @see #getExecutionConfig()
     * @generated
     */
    EReference getExecutionConfig_KeyValuePairs();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsEthernet <em>Needs Ethernet</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Needs Ethernet</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsEthernet()
     * @see #getExecutionConfig()
     * @generated
     */
    EAttribute getExecutionConfig_NeedsEthernet();

    /**
     * Returns the meta object for the attribute '{@link de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsErrorFrames <em>Needs Error Frames</em>}'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Needs Error Frames</em>'.
     * @see de.bmw.smard.modeller.statemachineset.ExecutionConfig#isNeedsErrorFrames()
     * @see #getExecutionConfig()
     * @generated
     */
    EAttribute getExecutionConfig_NeedsErrorFrames();

    /**
     * Returns the factory that creates the instances of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    StatemachinesetFactory getStatemachinesetFactory();

    /**
     * <!-- begin-user-doc -->
     * Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.DocumentRootImpl <em>Document Root</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.DocumentRootImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getDocumentRoot()
         * @generated
         */
        EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

        /**
         * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

        /**
         * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

        /**
         * The meta object literal for the '<em><b>State Machine Set</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference DOCUMENT_ROOT__STATE_MACHINE_SET = eINSTANCE.getDocumentRoot_StateMachineSet();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl <em>General Info</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.GeneralInfoImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getGeneralInfo()
         * @generated
         */
        EClass GENERAL_INFO = eINSTANCE.getGeneralInfo();

        /**
         * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute GENERAL_INFO__AUTHOR = eINSTANCE.getGeneralInfo_Author();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute GENERAL_INFO__DESCRIPTION = eINSTANCE.getGeneralInfo_Description();

        /**
         * The meta object literal for the '<em><b>Topic</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute GENERAL_INFO__TOPIC = eINSTANCE.getGeneralInfo_Topic();

        /**
         * The meta object literal for the '<em><b>Department</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute GENERAL_INFO__DEPARTMENT = eINSTANCE.getGeneralInfo_Department();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.OutputImpl <em>Output</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.OutputImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getOutput()
         * @generated
         */
        EClass OUTPUT = eINSTANCE.getOutput();

        /**
         * The meta object literal for the '<em><b>Target File Path</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute OUTPUT__TARGET_FILE_PATH = eINSTANCE.getOutput_TargetFilePath();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.StateMachineSetImpl <em>State Machine Set</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.StateMachineSetImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getStateMachineSet()
         * @generated
         */
        EClass STATE_MACHINE_SET = eINSTANCE.getStateMachineSet();

        /**
         * The meta object literal for the '<em><b>General Info</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE_SET__GENERAL_INFO = eINSTANCE.getStateMachineSet_GeneralInfo();

        /**
         * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE_SET__OUTPUT = eINSTANCE.getStateMachineSet_Output();

        /**
         * The meta object literal for the '<em><b>State Machines</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE_SET__STATE_MACHINES = eINSTANCE.getStateMachineSet_StateMachines();

        /**
         * The meta object literal for the '<em><b>Export Version Counter</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_MACHINE_SET__EXPORT_VERSION_COUNTER = eINSTANCE.getStateMachineSet_ExportVersionCounter();

        /**
         * The meta object literal for the '<em><b>Observers</b></em>' reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE_SET__OBSERVERS = eINSTANCE.getStateMachineSet_Observers();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute STATE_MACHINE_SET__ID = eINSTANCE.getStateMachineSet_Id();

        /**
         * The meta object literal for the '<em><b>Execution Config</b></em>' containment reference feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference STATE_MACHINE_SET__EXECUTION_CONFIG = eINSTANCE.getStateMachineSet_ExecutionConfig();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.KeyValuePairUserConfigImpl <em>Key Value Pair User Config</em>}'
         * class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.KeyValuePairUserConfigImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getKeyValuePairUserConfig()
         * @generated
         */
        EClass KEY_VALUE_PAIR_USER_CONFIG = eINSTANCE.getKeyValuePairUserConfig();

        /**
         * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute KEY_VALUE_PAIR_USER_CONFIG__KEY = eINSTANCE.getKeyValuePairUserConfig_Key();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute KEY_VALUE_PAIR_USER_CONFIG__VALUE = eINSTANCE.getKeyValuePairUserConfig_Value();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.impl.ExecutionConfigImpl <em>Execution Config</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.impl.ExecutionConfigImpl
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getExecutionConfig()
         * @generated
         */
        EClass EXECUTION_CONFIG = eINSTANCE.getExecutionConfig();

        /**
         * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXECUTION_CONFIG__LOG_LEVEL = eINSTANCE.getExecutionConfig_LogLevel();

        /**
         * The meta object literal for the '<em><b>Sorter Value</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXECUTION_CONFIG__SORTER_VALUE = eINSTANCE.getExecutionConfig_SorterValue();

        /**
         * The meta object literal for the '{@link de.bmw.smard.modeller.statemachineset.BusMessageReferable <em>Bus Message Referable</em>}' class.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @see de.bmw.smard.modeller.statemachineset.BusMessageReferable
         * @see de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl#getBusMessageReferable()
         * @generated
         */
        EClass BUS_MESSAGE_REFERABLE = eINSTANCE.getBusMessageReferable();

        /**
         * The meta object literal for the '<em><b>Key Value Pairs</b></em>' containment reference list feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference EXECUTION_CONFIG__KEY_VALUE_PAIRS = eINSTANCE.getExecutionConfig_KeyValuePairs();

        /**
         * The meta object literal for the '<em><b>Needs Ethernet</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXECUTION_CONFIG__NEEDS_ETHERNET = eINSTANCE.getExecutionConfig_NeedsEthernet();

        /**
         * The meta object literal for the '<em><b>Needs Error Frames</b></em>' attribute feature.
         * <!-- begin-user-doc -->
         * <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute EXECUTION_CONFIG__NEEDS_ERROR_FRAMES = eINSTANCE.getExecutionConfig_NeedsErrorFrames();

    }

} //StatemachinesetPackage
