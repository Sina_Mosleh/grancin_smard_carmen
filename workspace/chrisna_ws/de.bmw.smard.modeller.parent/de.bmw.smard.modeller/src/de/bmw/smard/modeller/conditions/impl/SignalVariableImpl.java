package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.HeaderSignal;
import de.bmw.smard.modeller.conditions.ISignalOrReference;
import de.bmw.smard.modeller.conditions.SignalObserver;
import de.bmw.smard.modeller.conditions.SignalVariable;
import de.bmw.smard.modeller.conditions.SignalVariableLagEnum;
import de.bmw.smard.modeller.conditions.util.DataTypeSupplierUtil;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl#getInterpretedValue <em>Interpreted Value</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl#getSignal <em>Signal</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl#getInterpretedValueTmplParam <em>Interpreted Value Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl#getSignalObservers <em>Signal Observers</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalVariableImpl#getLag <em>Lag</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalVariableImpl extends VariableImpl implements SignalVariable {

    private boolean dataTypeSet = false;
    private Supplier<DataType> dataTypeSupplier;

    /**
     * The default value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValue()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum INTERPRETED_VALUE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.FALSE;

    /**
     * The cached value of the '{@link #getInterpretedValue() <em>Interpreted Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValue()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum interpretedValue = INTERPRETED_VALUE_EDEFAULT;

    /**
     * This is true if the Interpreted Value attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean interpretedValueESet;

    /**
     * The cached value of the '{@link #getSignal() <em>Signal</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignal()
     * @generated
     * @ordered
     */
    protected ISignalOrReference signal;

    /**
     * The default value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValueTmplParam()
     * @generated
     * @ordered
     */
    protected static final String INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getInterpretedValueTmplParam() <em>Interpreted Value Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getInterpretedValueTmplParam()
     * @generated
     * @ordered
     */
    protected String interpretedValueTmplParam = INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT;

    /**
     * The cached value of the '{@link #getSignalObservers() <em>Signal Observers</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalObservers()
     * @generated
     * @ordered
     */
    protected EList<SignalObserver> signalObservers;

    /**
     * The default value of the '{@link #getLag() <em>Lag</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLag()
     * @generated
     * @ordered
     */
    protected static final SignalVariableLagEnum LAG_EDEFAULT = SignalVariableLagEnum.CURRENT;

    /**
     * The cached value of the '{@link #getLag() <em>Lag</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLag()
     * @generated
     * @ordered
     */
    protected SignalVariableLagEnum lag = LAG_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SignalVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SIGNAL_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getInterpretedValue() {
        return interpretedValue;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInterpretedValue(BooleanOrTemplatePlaceholderEnum newInterpretedValue) {
        BooleanOrTemplatePlaceholderEnum oldInterpretedValue = interpretedValue;
        interpretedValue = newInterpretedValue == null ? INTERPRETED_VALUE_EDEFAULT : newInterpretedValue;
        boolean oldInterpretedValueESet = interpretedValueESet;
        interpretedValueESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE, oldInterpretedValue, interpretedValue, !oldInterpretedValueESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetInterpretedValue() {
        BooleanOrTemplatePlaceholderEnum oldInterpretedValue = interpretedValue;
        boolean oldInterpretedValueESet = interpretedValueESet;
        interpretedValue = INTERPRETED_VALUE_EDEFAULT;
        interpretedValueESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE, oldInterpretedValue, INTERPRETED_VALUE_EDEFAULT, oldInterpretedValueESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetInterpretedValue() {
        return interpretedValueESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ISignalOrReference getSignal() {
        if (signal != null && signal.eIsProxy()) {
            InternalEObject oldSignal = (InternalEObject) signal;
            signal = (ISignalOrReference) eResolveProxy(oldSignal);
            if (signal != oldSignal) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.SIGNAL_VARIABLE__SIGNAL, oldSignal, signal));
            }
        }
        return signal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISignalOrReference basicGetSignal() {
        return signal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public Supplier<DataType> getDataTypeSupplier() {
        if(dataTypeSupplier == null) {
            if (signal == null)
                return null;
            if (signal instanceof HeaderSignal) {
                if (signal.eContainer() != null) {
                    dataTypeSupplier = DataTypeSupplierUtil.getDataTypeSupplier(signal);
                }
            } else {
                dataTypeSupplier = DataTypeSupplierUtil.getDataTypeSupplier(signal);
            }
        }
        return dataTypeSupplier;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public void setSignal(ISignalOrReference newSignal) {
        ISignalOrReference oldSignal = signal;
        signal = newSignal;

        // signal and - for HeaderSignal - parent of signal must not be null
        if (signal != null) {
            dataTypeSupplier = null;
            Supplier<DataType> supplier = getDataTypeSupplier();
            if (supplier != null) {
                // try to set DataType for the Modeller - supplier may return null. the getDataType call will do an extra lazy load for the Engine!
                DataType dt = supplier.get();
                setDataType(dt);
                dataTypeSet = true;
            }
        } else {
            dataType = DATA_TYPE_EDEFAULT;
        }

        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_VARIABLE__SIGNAL, oldSignal, signal));
    }

    /**
     * <!-- begin-user-doc -->
     * setSignal() accesses a child objects property
     * During a model migration edapt does the construction in the following order (parent-first)
     * 1) Call ctor of Objects
     * - SignalVariableImpl.ctor()
     * - HeaderSignalImpl.ctor()
     * 2) Call initProperties
     * - SignalVariableImpl (now the access to HeaderSignal happens but not available yet
     * - HeaderSignalImpl
     * To get around this order issue, we save a Supplier function and set the value on first getDataType
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public DataType getDataType() {
        if(dataTypeSet)
            return super.getDataType();
        Supplier<DataType> supplier = getDataTypeSupplier();
        if (supplier != null) {
            DataType dt = supplier.get();
            if (dt != null) {
                dataType = dt;
                dataTypeSet = true;
            }
        }
        return super.getDataType();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getSignal() != null) {
            return getSignal().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getInterpretedValueTmplParam() {
        return interpretedValueTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setInterpretedValueTmplParam(String newInterpretedValueTmplParam) {
        String oldInterpretedValueTmplParam = interpretedValueTmplParam;
        interpretedValueTmplParam = newInterpretedValueTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM, oldInterpretedValueTmplParam, interpretedValueTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<SignalObserver> getSignalObservers() {
        if (signalObservers == null) {
            signalObservers = new EObjectContainmentEList<SignalObserver>(SignalObserver.class, this, ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS);
        }
        return signalObservers;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public SignalVariableLagEnum getLag() {
        return lag;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setLag(SignalVariableLagEnum newLag) {
        SignalVariableLagEnum oldLag = lag;
        lag = newLag == null ? LAG_EDEFAULT : newLag;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_VARIABLE__LAG, oldLag, lag));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS:
                return ((InternalEList<?>) getSignalObservers()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE:
                return getInterpretedValue();
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL:
                if (resolve) return getSignal();
                return basicGetSignal();
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM:
                return getInterpretedValueTmplParam();
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS:
                return getSignalObservers();
            case ConditionsPackage.SIGNAL_VARIABLE__LAG:
                return getLag();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE:
                setInterpretedValue((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL:
                setSignal((ISignalOrReference) newValue);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM:
                setInterpretedValueTmplParam((String) newValue);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS:
                getSignalObservers().clear();
                getSignalObservers().addAll((Collection<? extends SignalObserver>) newValue);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__LAG:
                setLag((SignalVariableLagEnum) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE:
                unsetInterpretedValue();
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL:
                setSignal((ISignalOrReference) null);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM:
                setInterpretedValueTmplParam(INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS:
                getSignalObservers().clear();
                return;
            case ConditionsPackage.SIGNAL_VARIABLE__LAG:
                setLag(LAG_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE:
                return isSetInterpretedValue();
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL:
                return signal != null;
            case ConditionsPackage.SIGNAL_VARIABLE__INTERPRETED_VALUE_TMPL_PARAM:
                return INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT == null ? interpretedValueTmplParam != null : !INTERPRETED_VALUE_TMPL_PARAM_EDEFAULT
                        .equals(interpretedValueTmplParam);
            case ConditionsPackage.SIGNAL_VARIABLE__SIGNAL_OBSERVERS:
                return signalObservers != null && !signalObservers.isEmpty();
            case ConditionsPackage.SIGNAL_VARIABLE__LAG:
                return lag != LAG_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (interpretedValue: ");
        if (interpretedValueESet) result.append(interpretedValue);
        else result.append("<unset>");
        result.append(", interpretedValueTmplParam: ");
        result.append(interpretedValueTmplParam);
        result.append(", lag: ");
        result.append(lag);
        result.append(')');
        return result.toString();
    }

    /** @generated NOT */
    @Override
    public EList<AbstractObserver> getObservers() {
        if (signalObservers == null || signalObservers.isEmpty())
            return ECollections.emptyEList();

        EList<AbstractObserver> observers = new BasicEList<AbstractObserver>(signalObservers.size());
        observers.addAll(signalObservers);
        return observers;
    }
} //SignalVariableImpl
