package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.StopExpression;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Stop Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StopExpressionImpl#getAnalyseArt <em>Analyse Art</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StopExpressionImpl#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StopExpressionImpl extends ExpressionImpl implements StopExpression {
    /**
     * The default value of the '{@link #getAnalyseArt() <em>Analyse Art</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAnalyseArt()
     * @generated
     * @ordered
     */
    protected static final AnalysisTypeOrTemplatePlaceholderEnum ANALYSE_ART_EDEFAULT = AnalysisTypeOrTemplatePlaceholderEnum.ALL;

    /**
     * The cached value of the '{@link #getAnalyseArt() <em>Analyse Art</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAnalyseArt()
     * @generated
     * @ordered
     */
    protected AnalysisTypeOrTemplatePlaceholderEnum analyseArt = ANALYSE_ART_EDEFAULT;

    /**
     * The default value of the '{@link #getAnalyseArtTmplParam() <em>Analyse Art Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAnalyseArtTmplParam()
     * @generated
     * @ordered
     */
    protected static final String ANALYSE_ART_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getAnalyseArtTmplParam() <em>Analyse Art Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAnalyseArtTmplParam()
     * @generated
     * @ordered
     */
    protected String analyseArtTmplParam = ANALYSE_ART_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StopExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.STOP_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AnalysisTypeOrTemplatePlaceholderEnum getAnalyseArt() {
        return analyseArt;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAnalyseArt(AnalysisTypeOrTemplatePlaceholderEnum newAnalyseArt) {
        AnalysisTypeOrTemplatePlaceholderEnum oldAnalyseArt = analyseArt;
        analyseArt = newAnalyseArt == null ? ANALYSE_ART_EDEFAULT : newAnalyseArt;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART, oldAnalyseArt, analyseArt));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAnalyseArtTmplParam() {
        return analyseArtTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAnalyseArtTmplParam(String newAnalyseArtTmplParam) {
        String oldAnalyseArtTmplParam = analyseArtTmplParam;
        analyseArtTmplParam = newAnalyseArtTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM, oldAnalyseArtTmplParam, analyseArtTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART:
                return getAnalyseArt();
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM:
                return getAnalyseArtTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART:
                setAnalyseArt((AnalysisTypeOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM:
                setAnalyseArtTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART:
                setAnalyseArt(ANALYSE_ART_EDEFAULT);
                return;
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM:
                setAnalyseArtTmplParam(ANALYSE_ART_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART:
                return analyseArt != ANALYSE_ART_EDEFAULT;
            case ConditionsPackage.STOP_EXPRESSION__ANALYSE_ART_TMPL_PARAM:
                return ANALYSE_ART_TMPL_PARAM_EDEFAULT == null ? analyseArtTmplParam != null : !ANALYSE_ART_TMPL_PARAM_EDEFAULT.equals(analyseArtTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (analyseArt: ");
        result.append(analyseArt);
        result.append(", analyseArtTmplParam: ");
        result.append(analyseArtTmplParam);
        result.append(')');
        return result.toString();
    }

} //StopExpressionImpl
