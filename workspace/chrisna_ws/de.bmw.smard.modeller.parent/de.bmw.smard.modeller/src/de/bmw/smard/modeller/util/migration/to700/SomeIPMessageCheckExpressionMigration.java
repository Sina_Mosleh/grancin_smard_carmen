/**
 * 
 */
package de.bmw.smard.modeller.util.migration.to700;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.AttributeSlot;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;
import org.eclipse.emf.edapt.spi.migration.ReferenceSlot;

import de.bmw.smard.modeller.util.migration.Utils;

public class SomeIPMessageCheckExpressionMigration extends CustomMigration {
	
	private static final Logger LOGGER = Logger.getLogger(SomeIPMessageCheckExpressionMigration.class);

	// list of attributes belonging to a filter
	List<String> someIPSDFilterList = Arrays.asList("flags", "flagsTmplParam", "sdType", "sdTypeTmplParam", "serviceId_L6",
			"instanceId", "ttl", "majorVersion", "minorVersion", "eventGroupId", "indexFirstOption", "indexSecondOption", 
			"numberFirstOption", "numberSecondOption");
	List<String> someIPFilterList = Arrays.asList("serviceId", "methodType", "methodTypeTmplParam", "methodId", "clientId", 
			"sessionId", "protocolVersion", "interfaceVersion", "messageType", "messageTypeTmplParam", "returnCode", 
			"returnCodeTmplParam", "someIPLength", "someIPLengthTmplParam");
	List<String> tcpFilterList = Arrays.asList("sourcePort", "destPort", "sequenceNumber", "acknowledgementNumber", "tcpFlags",
			"streamAnalysisFlags", "streamAnalysisFlagsTmplParam", "streamValidPayloadOffset");
	List<String> udpFilterList = Arrays.asList("sourcePort", "destPort", "checkSum");
	List<String> ipV4FilterList = Arrays.asList("protocolType", "protocolTypeTmplParam", "sourceIP", "destIP", "timeToLive");
	List<String> ethernetFilterList = Arrays.asList("sourceMAC", "destMAC", "etherType", "InnerVlanId", 
			"OuterVlanId", "InnerVlanCFI", "OuterVlanCFI", "InnerVlanVID", "OuterVlanVID", "CRC", "InnerVlanTPID", "OuterVlanTPID");
	
	// list of attributes belonging to AbstractMessage
	List<String> messageAttributesList = Arrays.asList("busId", "busIdTmplParam", "busIdRange");
	
	// map for SomeIP(SD/UDP/TCP)MessageCheckExpression and their attributes
	private Map<Instance, EList<EAttribute>> instancesToMigrate = new HashMap<Instance, EList<EAttribute>>();
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {

		// get all instances of SomeIPUDPMessageCheckExpression
		// SomeIPSDEntryMessageCheck extends SomeIPUDPMessageCheckExpression
		for(Instance messageCheckExpression : model.getAllInstances("conditions.SomeIPUDPMessageCheckExpression")) {
			EList<EAttribute> attributeList = messageCheckExpression.getEClass().getEAllAttributes();
			
			instancesToMigrate.put(messageCheckExpression, attributeList);
		}
		
		// get all instances of SomeIPTCPMessageCheckExpression
		for(Instance messageCheckExpression : model.getAllInstances("conditions.SomeIPTCPMessageCheckExpression")) {
			EList<EAttribute> attributeList = messageCheckExpression.getEClass().getEAllAttributes();
			
			instancesToMigrate.put(messageCheckExpression, attributeList);
		}
		
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
		
		// instance of the according SignalReferenceSet
		Instance signalReferenceSet = null;
		
		// instance for the SomeIPMessage or SomeIPSDMessage in SignalReferenceSet
		Instance messageInstance = null;
		
		// every AbstractMessage has one designated filter
		Instance designatedFilterInstance = null;
		
		// the name of the condition will be assigned as name to the SomeIP(SD)Message
		String conditionName = "";
		
		// instances for all filters, that may be necessary
		Instance someIpFilterInstance = null;
		Instance tcpFilterInstance = null;	
		Instance udpFilterInstance = null;
		Instance ipV4FilterInstance = null;
		Instance ethernetFilterInstance = null;
		
		
		// iterate through all instances of SomeIP(SD/UDP/TCP)MessageCheckExpression to migrate
		Iterator<Map.Entry<Instance, EList<EAttribute>>> iterator = instancesToMigrate.entrySet().iterator();
		while(iterator.hasNext()) {
			
			// get the original SomeIP(SD/UDP/TCP)MessageCheckExpression and its list of attributes
			Map.Entry<Instance, EList<EAttribute>> pair = (Map.Entry<Instance, EList<EAttribute>>) iterator.next();
			Instance someIPMsgCheckInstance = pair.getKey();
			EList<EAttribute> attributeList = pair.getValue();
						
			// create the SomeIP(SD)Message and the correct designated filter
			if(someIPMsgCheckInstance.instanceOf("conditions.SomeIPSDEntryMessageCheckExpression")) {
				messageInstance = model.newInstance("conditions.SomeIPSDMessage");
				designatedFilterInstance = model.newInstance("conditions.SomeIPSDFilter");
				messageInstance.set("someIPSDFilter", designatedFilterInstance);
			} else {
				messageInstance = model.newInstance("conditions.SomeIPMessage");
				designatedFilterInstance = model.newInstance("conditions.SomeIPFilter");
				messageInstance.set("someIPFilter", designatedFilterInstance);
			}
			
			// migrate the name of the containing condition (of the SomeIPxxxMessageCheckExpression) to the new SomeIPMessage
			// works, even if the name attribute is ""
			Instance condition = someIPMsgCheckInstance.getContainer();
			conditionName = condition.get("name");
			messageInstance.set("name", conditionName);
			
			// get the SignalReferenceSet in this conditions file and assign the SomeIP(SD)Message to it
			Instance conditionsDocument = condition.getContainer().getContainer(); 	// Condition -> ConditionsSet -> ConditionsDocument
			signalReferenceSet = conditionsDocument.get("signalReferencesSet"); 	// ConditionsDocument <- SignalReferenceSet
			signalReferenceSet.add("messages", messageInstance);
			
			// migrate attributes from xxxMessageCheckExpression to xxxFilter
			for(EAttribute attribute : attributeList) {
				
				String name = attribute.getName();
				
				// first step: warning for CheckType ALL for attribute Type Lx
				if(name.contains("typeL")) {
					if(someIPMsgCheckInstance.get(name).toString().equals("ALL")) {
						LOGGER.warn(MessageFormat.format(name + ": CheckType ALL", SomeIPMessageCheckExpressionMigration.class));
						//warnCheckType();
					}
				}
				
				// if attribute is something with busId -> migrate attribute to SomeIpMessage (instead of some filter)
				if(messageAttributesList.contains(name)) {
					messageInstance.set(name, someIPMsgCheckInstance.get(name));
				}

				// check if the future SomeIPFilter has an attribute with the given name
				if(someIPFilterList.contains(name)) {
					if(someIPMsgCheckInstance.instanceOf("conditions.SomeIPSDEntryMessageCheckExpression")) {
						someIpFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, someIpFilterInstance, "conditions.SomeIPFilter");		
					} else {
						designatedFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, designatedFilterInstance, "conditions.SomeIPFilter");
					}
				}
				
				// check if the SomeIPMessageCheckExpression has attributes of TCPFilter
				if(someIPMsgCheckInstance.instanceOf("conditions.SomeIPTCPMessageCheckExpression")) {
					if(tcpFilterList.contains(name)) {
						tcpFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, tcpFilterInstance, "conditions.TCPFilter");					
					}
				} 
				
				// check if the SomeIPMessageCheckExpression is some kind of SomeIPUDPMessageCheckExpression
				if(someIPMsgCheckInstance.instanceOf("conditions.SomeIPUDPMessageCheckExpression")) {
					if(someIPMsgCheckInstance.instanceOf("conditions.SomeIPSDEntryMessageCheckExpression")) {
						if(someIPSDFilterList.contains(name)) {
							// SomeIPSDEntryMessageCheckExpression::serviceId_L6 -> SomeIPSDFilter::serviceId
							if(name.equalsIgnoreCase("serviceId_L6")) {
								name = "serviceId";
							}
							designatedFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, designatedFilterInstance, "conditions.SomeIPSDFilter");
						}
					} else {
						if(udpFilterList.contains(name)) {
							udpFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, udpFilterInstance, "conditions.UDPFilter");
						}
					}
					
				}
				
				// check if the SomeIPMessageCheckExpression has attributes of IPv4Filter
				if(ipV4FilterList.contains(name)) {
					ipV4FilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, ipV4FilterInstance, "conditions.IPv4Filter");
				}

				// check if the SomeIPMessageCheckExpression has attributes of EthernetFilter
				if(ethernetFilterList.contains(name)) {
					ethernetFilterInstance = migrateAttribute(model, someIPMsgCheckInstance, name, ethernetFilterInstance, "conditions.EthernetFilter");
				}
			}
			
			// attach other filters to the SomeIPMessage/SomeIPSDMessage if they are not null
			if(someIpFilterInstance != null) {
				messageInstance.add("filters", someIpFilterInstance);
				someIpFilterInstance = null;
			}
			if(tcpFilterInstance != null) {
				messageInstance.add("filters", tcpFilterInstance);
				tcpFilterInstance = null;
			}
			if(udpFilterInstance != null) {
				messageInstance.add("filters", udpFilterInstance);
				udpFilterInstance = null;
			}
			if(ipV4FilterInstance != null) {
				messageInstance.add("filters", ipV4FilterInstance);
				ipV4FilterInstance = null;
			}
			if(ethernetFilterInstance != null) {
				messageInstance.add("filters", ethernetFilterInstance);
				ethernetFilterInstance = null;
			}
			
			// the original condition has to remain but the expression has to be removed
			condition.remove("expression", someIPMsgCheckInstance);
			model.delete(someIPMsgCheckInstance);
			
			// create a new generic MessageCheckExpression that references the created SomeIP(SD)Message
			Instance messageCheckExpression = model.newInstance("conditions.MessageCheckExpression");
			condition.set("expression", messageCheckExpression);
			messageCheckExpression.set("message", messageInstance);

			// finish
			someIPMsgCheckInstance = null;
			designatedFilterInstance = null;
			attributeList = null;
			iterator.remove();
		}
		
 	}
	
	/**
	 * 
	 * @param model
	 * @param sourceInstance
	 * @param attributeName
	 * @param destInstance
	 * @param instanceName
	 * @return
	 */
	private Instance migrateAttribute(Model model, Instance sourceInstance, String attributeName, 
			Instance destInstance, String instanceName) {
		try {
			if(sourceInstance.get(attributeName) != null && !sourceInstance.get(attributeName).equals("")) {
				if(destInstance == null) {
					destInstance = model.newInstance(instanceName);
				}
				destInstance.set(attributeName, sourceInstance.get(attributeName));
			}
		} catch (Exception e) {
			try { 
				destInstance.set(Utils.decapitalize(attributeName), sourceInstance.get(attributeName));
				
			} catch( Exception inner) { 
				LOGGER.error("error in migrateAttribute", inner); }
		}
		
		return destInstance;
	}
	
	private void warnCheckType() throws MigrationException {
		final Diagnostician diagnostician = new Diagnostician() {
			@Override
			public String getObjectLabel(EObject object) {
				if (object instanceof Instance) {
					final Instance instance = (Instance) object;
					return "Instance of type \"" //$NON-NLS-1$
						+ instance.getEClass().getName() + "\""; //$NON-NLS-1$
				} else if (object instanceof ReferenceSlot) {
					final ReferenceSlot referenceSlot = (ReferenceSlot) object;
					return "Reference \"" //$NON-NLS-1$
						+ referenceSlot.getEReference().getName()
						+ "\" of " //$NON-NLS-1$
						+ getObjectLabel((EObject) referenceSlot
							.getInstance());
				} else if (object instanceof AttributeSlot) {
					final AttributeSlot referenceSlot = (AttributeSlot) object;
					return "Attribute \"" //$NON-NLS-1$
						+ referenceSlot.getEAttribute().getName()
						+ "\" of " //$NON-NLS-1$
						+ getObjectLabel((EObject) referenceSlot
							.getInstance());
				}
				return super.getObjectLabel(object);
			}
		};
		
	}

}
