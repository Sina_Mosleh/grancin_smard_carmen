package de.bmw.smard.modeller.util.migration.to810;

import java.util.Collection;

import org.eclipse.emf.ecore.impl.EEnumLiteralImpl;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import de.bmw.smard.modeller.util.migration.MigrationMarkerUtil;

public class MigrateInterpretedValue extends CustomMigration {

    private static final String SIGNAL_VARIABLE = "conditions.SignalVariable";
    private static final String INTERPRETED_VALUE = "interpretedValue";
    private static final String SIGNAL_OBSERVERS = "signalObservers";
    private static final String NAME = "name";

    @Override
    public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {

        for (Instance signalVar : model.getAllInstances(SIGNAL_VARIABLE)) {
            EEnumLiteralImpl sigVarIntVal = signalVar.get(INTERPRETED_VALUE);
            String sigVarName = signalVar.get(NAME);
            boolean sigVarIntValBool = Boolean.valueOf(sigVarIntVal.getLiteral());
            Collection<Instance> list = signalVar.get(SIGNAL_OBSERVERS);

            for (Instance sigObs : list) {
                EEnumLiteralImpl sigObsIntVal = sigObs.get(INTERPRETED_VALUE);
                boolean sigObsIntValBool = Boolean.valueOf(sigObsIntVal.getLiteral());
                String sigObsName = sigObs.get(NAME);
                if (sigObsIntValBool != sigVarIntValBool) {
                    String warning = String.format("Interpreted Value of Signal-Variable %1$s: \"%2$s\" and "
                            + "Signal-Observer %3$s: \"%4$s\" were not equal. Interpreted Value Flag of Signal-Observer was removed.", sigVarName, sigVarIntValBool, sigObsName, sigObsIntValBool);
                    MigrationMarkerUtil.createWarning(warning);
                }
            }
        }

    }

}
