package de.bmw.smard.modeller.parser;

import java.util.List;
import java.util.regex.Pattern;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.math.Assignment;
import de.bmw.smard.common.math.BinaryOperation;
import de.bmw.smard.common.math.NumericLiteral;
import de.bmw.smard.common.math.StringLiteral;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.common.math.UnaryOperation;
import de.bmw.smard.common.math.VariableReference;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.util.Operation;

/**
 * Parse a string into a term.
 */
public class MathParser {

    private static final Pattern OP_PATTERN = Pattern.compile("^OP(\\d+)$");

    private MathParser(){
        // no instance
    }

    public static Term parse(String pExpression, List<? extends IOperand> operands) {
        return parse(pExpression,operands, false);
    }

    /**
     * Parse a String pExpression to a term.
     * if List<IOperand> operands is given, "OP"-Strings in the pExpression are recognized
     * The parser throws exceptions in case of an error. The exception message must be suitable for user presentation
     * only ComputeVariable::expression can handle StringLiterals atm (SMARD-2045)
     *
     * @param pExpression
     * @return
     */
    public static Term parse(String pExpression, List<? extends IOperand> operands, boolean parseStringLiterals) throws ActionExpressionParseException {
        if (pExpression == null)
            return null;
        String s = StringUtils.removeSpaces(pExpression);
        if (s.length() < 1)
            return null;

        boolean allIsLetter = true;
        boolean allIsDigits = true;
        boolean allBracketed = s.startsWith("(") && s.endsWith(")");
        boolean quoteOpened = false;
        int brackets = 0;
        char highestOperator = 'x';
        int highestOperatorPos = -1;

        // JIRA [SMARD-383] wzh: the character ^ inserted (exponent)
        if (s.contains("*-") || s.contains("/-") || s.contains("%-") || s.contains("^-")) {    // we don't accept an algebraic sign without brackets (e.g. wrong "4*-3", correct "4*(-3)"
            throw new ActionExpressionParseException("Combinations *-, /-, %- and ^- are not allowed, please use brackets.");
        }

        if (isNumeric(pExpression)) {
            try {
                return new NumericLiteral(Double.parseDouble(s));
            } catch (NumberFormatException e) {
                // no action
            }
        }
        
        char ch;
        final char chars[] = s.toCharArray();
        final int len = s.length();
        for (int chpos = 0; chpos < len; chpos++) {
            ch = chars[chpos];

            if(parseStringLiterals && ch == '"') {
                quoteOpened = !quoteOpened;
            }

            if ((chpos == 0 || !allIsLetter) && Character.isDigit(ch)) {
                allIsLetter = false;
            } else {
                allIsDigits = false;

                if (Character.isLetter(ch)
                        || (allIsLetter && (ch == '_' || ch == '.' || ch == '[' || ch == ']' || Character.isDigit(ch)))) {
                    // do nothing, if we are within a variable-name (e.g. xyz, x2, x_2)
                } else {
                    allIsLetter = false;

                    if(!quoteOpened) { // do not parse inside of a StringLiteral
                        if (ch == '(') {    // no check for opening bracket - just increase counter
                            brackets++;
                        } else if (ch == ')') {    // check closing bracket
                            if (brackets == 0)
                                throw new ActionExpressionParseException("Too many closing brackets.");

                            brackets--;
                            if (brackets == 0 && chpos != s.length() - 1)
                                allBracketed = false;
                        } else if (brackets == 0) {    // check syntax having no brackets or being outside all brackets
                            if ((ch == '=') ||
                                    ((ch == '*' || ch == '/' || ch == '%') && (highestOperator != '=' && highestOperator != '-' && highestOperator != '+' && highestOperator != '^')) ||
                                    ((ch == '^') && (highestOperator != '=' && highestOperator != '-' && highestOperator != '+' && highestOperator != '*' && highestOperator != '/' && highestOperator != '%')) ||
                                    ((ch == '-' || ch == '+') && highestOperator != '=')) {
                                highestOperator = ch;
                                highestOperatorPos = chpos;
                            }
                        }
                    }
                }
            }
        }

        if (brackets > 0)
            throw new ActionExpressionParseException("Not enough closing brackets");
        if (quoteOpened) {
            throw new ActionExpressionParseException("Opening quotes are never closed");
        }

        if (allBracketed) {
            return parse(s.substring(1, s.length() - 1), operands, parseStringLiterals);
        } else if (allIsLetter ) {
            if ( StringUtils.matches(s, OP_PATTERN.pattern()) ) {
                Integer opIndex = null;
                try {
                    opIndex = Integer.valueOf(StringUtils.extract(s, OP_PATTERN, 1)) - 1;
                } catch (Exception e) {
                    // we must have a VariableReference, continue
                }

                if (operands != null && opIndex != null && operands.size() > opIndex) {
                    IOperand op = operands.get(opIndex);
                    return new Operation(op);
                } else {
                    throw new ActionExpressionParseException("Child operand " + s + " not found!");
                }
            }
            // does NOT match OP_PATTERN
            return new VariableReference(s);

        } else if (allIsDigits) {
            return new NumericLiteral(Double.parseDouble(s));
        } else if (highestOperator != 'x') {
            if (highestOperator == '=') {
                return new Assignment(s.substring(0, highestOperatorPos),
                        parse(s.substring(highestOperatorPos + 1, s.length()), operands, parseStringLiterals));
            } else {
                if (highestOperatorPos == 0) {
                    return new UnaryOperation(parse(s.substring(1), operands, parseStringLiterals), highestOperator);
                } else {
                	Term left = parse(s.substring(0, highestOperatorPos), operands, parseStringLiterals);
                	Term right = parse(s.substring(highestOperatorPos + 1, s.length()), operands, parseStringLiterals);
                	if(left == null || right == null) {
                		throw new ActionExpressionParseException("Could not parse " + s);
                	}
                    return new BinaryOperation(left, right, highestOperator);
                }
            }
        } else if(parseStringLiterals && isStringLiteral(s)) {
            return new StringLiteral(s.replace("\"", ""));
        } else
            throw new ActionExpressionParseException("Could not parse " + s);
    }

    private static boolean isStringLiteral(String str) {
        return str.matches("\".*\"");
	}

	private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
