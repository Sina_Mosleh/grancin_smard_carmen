package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.EvaluationBehaviourOrTemplateDefinedEnum;
import de.bmw.smard.modeller.conditions.PluginCheckExpression;
import de.bmw.smard.modeller.conditions.PluginSignal;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl#getEvaluationBehaviour <em>Evaluation Behaviour</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl#getSignalToCheck <em>Signal To Check</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginCheckExpressionImpl#getEvaluationBehaviourTmplParam <em>Evaluation Behaviour Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginCheckExpressionImpl extends ExpressionImpl implements PluginCheckExpression {
    /**
     * The default value of the '{@link #getEvaluationBehaviour() <em>Evaluation Behaviour</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEvaluationBehaviour()
     * @generated
     * @ordered
     */
    protected static final EvaluationBehaviourOrTemplateDefinedEnum EVALUATION_BEHAVIOUR_EDEFAULT = EvaluationBehaviourOrTemplateDefinedEnum.PULL_FROM_PLUGIN;
    /**
     * The cached value of the '{@link #getEvaluationBehaviour() <em>Evaluation Behaviour</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEvaluationBehaviour()
     * @generated
     * @ordered
     */
    protected EvaluationBehaviourOrTemplateDefinedEnum evaluationBehaviour = EVALUATION_BEHAVIOUR_EDEFAULT;
    /**
     * The cached value of the '{@link #getSignalToCheck() <em>Signal To Check</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignalToCheck()
     * @generated
     * @ordered
     */
    protected PluginSignal signalToCheck;

    /**
     * The default value of the '{@link #getEvaluationBehaviourTmplParam() <em>Evaluation Behaviour Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEvaluationBehaviourTmplParam()
     * @generated
     * @ordered
     */
    protected static final String EVALUATION_BEHAVIOUR_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getEvaluationBehaviourTmplParam() <em>Evaluation Behaviour Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEvaluationBehaviourTmplParam()
     * @generated
     * @ordered
     */
    protected String evaluationBehaviourTmplParam = EVALUATION_BEHAVIOUR_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PluginCheckExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PLUGIN_CHECK_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EvaluationBehaviourOrTemplateDefinedEnum getEvaluationBehaviour() {
        return evaluationBehaviour;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEvaluationBehaviour(EvaluationBehaviourOrTemplateDefinedEnum newEvaluationBehaviour) {
        EvaluationBehaviourOrTemplateDefinedEnum oldEvaluationBehaviour = evaluationBehaviour;
        evaluationBehaviour = newEvaluationBehaviour == null ? EVALUATION_BEHAVIOUR_EDEFAULT : newEvaluationBehaviour;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR, oldEvaluationBehaviour, evaluationBehaviour));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public PluginSignal getSignalToCheck() {
        if (signalToCheck != null && signalToCheck.eIsProxy()) {
            InternalEObject oldSignalToCheck = (InternalEObject) signalToCheck;
            signalToCheck = (PluginSignal) eResolveProxy(oldSignalToCheck);
            if (signalToCheck != oldSignalToCheck) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK, oldSignalToCheck, signalToCheck));
            }
        }
        return signalToCheck;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public PluginSignal basicGetSignalToCheck() {
        return signalToCheck;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSignalToCheck(PluginSignal newSignalToCheck) {
        PluginSignal oldSignalToCheck = signalToCheck;
        signalToCheck = newSignalToCheck;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK, oldSignalToCheck, signalToCheck));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getEvaluationBehaviourTmplParam() {
        return evaluationBehaviourTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEvaluationBehaviourTmplParam(String newEvaluationBehaviourTmplParam) {
        String oldEvaluationBehaviourTmplParam = evaluationBehaviourTmplParam;
        evaluationBehaviourTmplParam = newEvaluationBehaviourTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM, oldEvaluationBehaviourTmplParam, evaluationBehaviourTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR:
                return getEvaluationBehaviour();
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK:
                if (resolve) return getSignalToCheck();
                return basicGetSignalToCheck();
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM:
                return getEvaluationBehaviourTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR:
                setEvaluationBehaviour((EvaluationBehaviourOrTemplateDefinedEnum) newValue);
                return;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK:
                setSignalToCheck((PluginSignal) newValue);
                return;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM:
                setEvaluationBehaviourTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR:
                setEvaluationBehaviour(EVALUATION_BEHAVIOUR_EDEFAULT);
                return;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK:
                setSignalToCheck((PluginSignal) null);
                return;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM:
                setEvaluationBehaviourTmplParam(EVALUATION_BEHAVIOUR_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR:
                return evaluationBehaviour != EVALUATION_BEHAVIOUR_EDEFAULT;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__SIGNAL_TO_CHECK:
                return signalToCheck != null;
            case ConditionsPackage.PLUGIN_CHECK_EXPRESSION__EVALUATION_BEHAVIOUR_TMPL_PARAM:
                return EVALUATION_BEHAVIOUR_TMPL_PARAM_EDEFAULT == null ? evaluationBehaviourTmplParam != null : !EVALUATION_BEHAVIOUR_TMPL_PARAM_EDEFAULT
                        .equals(evaluationBehaviourTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (evaluationBehaviour: ");
        result.append(evaluationBehaviour);
        result.append(", evaluationBehaviourTmplParam: ");
        result.append(evaluationBehaviourTmplParam);
        result.append(')');
        return result.toString();
    }

} //PluginCheckExpressionImpl
