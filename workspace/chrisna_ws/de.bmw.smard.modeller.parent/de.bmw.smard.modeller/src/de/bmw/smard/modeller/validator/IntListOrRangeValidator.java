package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.StringParseUtils;

import java.util.ArrayList;
import java.util.List;

public class IntListOrRangeValidator {

    private IntListOrRangeValidator() {}

    public static NameStep builder(String value) {
        return new Builder(value);
    }

    public interface NameStep {
        WarningStep name(String name);
    }

    public interface WarningStep {
        ErrorStep warning(String warningId);
    }

    public interface ErrorStep {
        OptionalStep error(String errorId);
    }

    public interface OptionalStep extends Build {
        OptionalStep min(int minimum);
        OptionalStep max(int maximum);
    }

    public static class Builder extends Validator implements
            NameStep,
            WarningStep,
            ErrorStep,
            OptionalStep {

        private String value;
        private String errorId;
        private String warningId;
        private String name;
        private Integer minimum;
        private Integer maximum;

        private Builder(String value) {
            this.value = value;
        }

        @Override
        public WarningStep name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public ErrorStep warning(String warningId) {
            this.warningId = warningId;
            return this;
        }

        @Override
        public OptionalStep error(String errorId) {
            this.errorId = errorId;
            return this;
        }

        @Override
        public OptionalStep min(int minimum) {
            this.minimum = minimum;
            return this;
        }

        @Override
        public OptionalStep max(int maximum) {
            this.maximum = maximum;
            return this;
        }

        @Override
        public boolean validate(BaseValidator baseValidator) {

            List<String> errorMessages = new ArrayList<>();
            List<String> warningMessages = new ArrayList<>();

            StringParseUtils.parseIntListOrRange(
                    value,
                    name,
                    false,
                    baseValidator.isTemplateAllowed(),
                    true,
                    minimum,
                    maximum,
                    errorMessages,
                    warningMessages);

            if (!errorMessages.isEmpty()) {
                attachError(baseValidator, errorId, StringUtils.join(errorMessages, ";"));
            }
            if (!warningMessages.isEmpty()) {
                attachWarn(baseValidator, warningId, StringUtils.join(warningMessages, ";"));
            }

            return !errorMessages.isEmpty();
        }
    }
}