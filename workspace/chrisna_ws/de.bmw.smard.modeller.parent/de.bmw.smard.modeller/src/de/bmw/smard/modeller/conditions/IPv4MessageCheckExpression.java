package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPv4 Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getProtocolType <em>Protocol Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getSourceIP <em>Source IP</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getDestIP <em>Dest IP</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTimeToLive <em>Time To Live</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTypeL3 <em>Type L3</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTypeL3TmplParam <em>Type L3 Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression()
 * @model
 * @generated
 */
public interface IPv4MessageCheckExpression extends EthernetMessageCheckExpression {
    /**
     * Returns the value of the '<em><b>Protocol Type</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Protocol Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Protocol Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum
     * @see #setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_ProtocolType()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    ProtocolTypeOrTemplatePlaceholderEnum getProtocolType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getProtocolType <em>Protocol Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Protocol Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum
     * @see #getProtocolType()
     * @generated
     */
    void setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Protocol Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Protocol Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Protocol Type Tmpl Param</em>' attribute.
     * @see #setProtocolTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_ProtocolTypeTmplParam()
     * @model
     * @generated
     */
    String getProtocolTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Protocol Type Tmpl Param</em>' attribute.
     * @see #getProtocolTypeTmplParam()
     * @generated
     */
    void setProtocolTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Source IP</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Source IP</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Source IP</em>' attribute.
     * @see #setSourceIP(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_SourceIP()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.IPPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getSourceIP();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getSourceIP <em>Source IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Source IP</em>' attribute.
     * @see #getSourceIP()
     * @generated
     */
    void setSourceIP(String value);

    /**
     * Returns the value of the '<em><b>Dest IP</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Dest IP</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Dest IP</em>' attribute.
     * @see #setDestIP(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_DestIP()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.IPPatternOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getDestIP();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getDestIP <em>Dest IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Dest IP</em>' attribute.
     * @see #getDestIP()
     * @generated
     */
    void setDestIP(String value);

    /**
     * Returns the value of the '<em><b>Time To Live</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Time To Live</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Time To Live</em>' attribute.
     * @see #setTimeToLive(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_TimeToLive()
     * @model default="" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getTimeToLive();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTimeToLive <em>Time To Live</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Time To Live</em>' attribute.
     * @see #getTimeToLive()
     * @generated
     */
    void setTimeToLive(String value);

    /**
     * Returns the value of the '<em><b>Type L3</b></em>' attribute.
     * The default value is <code>"ANY"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CheckType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type L3</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type L3</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #setTypeL3(CheckType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_TypeL3()
     * @model default="ANY" required="true"
     *        extendedMetaData="kind='attribute' name='typeL3' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    CheckType getTypeL3();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTypeL3 <em>Type L3</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type L3</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #getTypeL3()
     * @generated
     */
    void setTypeL3(CheckType value);

    /**
     * Returns the value of the '<em><b>Type L3 Tmpl Param</b></em>' attribute.
     * The default value is <code>""</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type L3 Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type L3 Tmpl Param</em>' attribute.
     * @see #setTypeL3TmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIPv4MessageCheckExpression_TypeL3TmplParam()
     * @model default=""
     * @generated
     */
    String getTypeL3TmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.IPv4MessageCheckExpression#getTypeL3TmplParam <em>Type L3 Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type L3 Tmpl Param</em>' attribute.
     * @see #getTypeL3TmplParam()
     * @generated
     */
    void setTypeL3TmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidL3Type(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSourceIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidDestinationIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidTimeToLive(DiagnosticChain diagnostics, Map<Object, Object> context);

} // IPv4MessageCheckExpression
