package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.PayloadFilter#getIndex <em>Index</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PayloadFilter#getMask <em>Mask</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PayloadFilter#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPayloadFilter()
 * @model
 * @generated
 */
public interface PayloadFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Index</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Index</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Index</em>' attribute.
     * @see #setIndex(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPayloadFilter_Index()
     * @model default="0" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getIndex();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getIndex <em>Index</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Index</em>' attribute.
     * @see #getIndex()
     * @generated
     */
    void setIndex(String value);

    /**
     * Returns the value of the '<em><b>Mask</b></em>' attribute.
     * The default value is <code>"0xFF"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mask</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Mask</em>' attribute.
     * @see #setMask(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPayloadFilter_Mask()
     * @model default="0xFF" dataType="de.bmw.smard.modeller.conditions.ByteOrTemplatePlaceholder" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getMask();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getMask <em>Mask</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Mask</em>' attribute.
     * @see #getMask()
     * @generated
     */
    void setMask(String value);

    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Value</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPayloadFilter_Value()
     * @model default="0" dataType="de.bmw.smard.modeller.conditions.ByteOrTemplatePlaceholder" required="true"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getValue();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PayloadFilter#getValue <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(String value);

    /**
     * @generated NOT
     */
    AbstractBusMessage getMessage();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidIndex(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMask(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context);

} // PayloadFilter
