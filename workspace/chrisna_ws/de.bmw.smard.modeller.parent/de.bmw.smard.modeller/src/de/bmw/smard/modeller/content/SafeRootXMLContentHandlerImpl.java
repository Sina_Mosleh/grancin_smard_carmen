package de.bmw.smard.modeller.content;

import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ContentHandler;
import org.eclipse.emf.ecore.xmi.impl.RootXMLContentHandlerImpl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;

/**
 * Without this class:
 * - base for encoding of a file is the parent directory!
 *     - whether a file has the same encoding as its parent directory or not
 *       it will be used from parent. does not matter what the xml header states
 *     - ends in destroyed characters because no conversion is done
 *
 */
public class SafeRootXMLContentHandlerImpl extends RootXMLContentHandlerImpl {

    SafeRootXMLContentHandlerImpl(Map<String, String> parameters) {
        super(parameters);
    }

    @Override
    protected String getCharset(URI uri, InputStream inputStream, Map<?, ?> options, Map<Object, Object> context) throws IOException {
        String result = (String) context.get(ContentHandler.CHARSET_PROPERTY);
        if (result == null) {

            // During move operation or save within eclipse text editor
            // we get a ReadableInputStream instead of a LazyInputStream
            //
            // XMLLoadImpl::load(XMLResource, InputStream, Map<?, ?> options)
            // does have a special case for this URIConverter.Readable
            // but it is wrapped in a BufferedInputStream in
            // XMLContentHandlerImpl::load(URI, InputStream, Map<?, ?>, Map<Object, Object>
            // Therefor loading of this stream does not work and we get no encoding
            // Then default Encoding would be used, and our special characters will be destroyed
            if (inputStream instanceof ResetableReadableInputStream) {
                result = ((ResetableReadableInputStream) inputStream).getEncoding();
            } else {
                // use default behavior instead
                result = load(uri, inputStream, options, context).getEncoding();
            }
            context.put(ContentHandler.CHARSET_PROPERTY, result);
        }
        return result;
    }

    public static class Describer extends RootXMLContentHandlerImpl.Describer {

        /***
         * This method provides our custom {@link:ResetableReadableInputStream} which does prevent
         * the exception throwing around.
         * The exeception is caused by eclipse itself because it calls super.reset() without implementation
         * (it throws IOException("mark/reset not supported"))
         */
        @Override
        public int describe(Reader reader, IContentDescription description) throws IOException {
            return describe(new ResetableReadableInputStream(reader), description);
        }

        /***
         * This method provides our custom {@link:SafeRootXMLContentHandlerImpl}
         * We provide this implementation because of the problem that eclipse wraps the
         * ReadableInputStream in a BufferedInputStream and do a instance-of test on the first one.
         * instance-of test fails during wrong type and everything gets worse.
         */
        @Override
        protected ContentHandler createContentHandler(Map<String, String> parameters) {
            return new SafeRootXMLContentHandlerImpl(parameters);
        }
    }
}