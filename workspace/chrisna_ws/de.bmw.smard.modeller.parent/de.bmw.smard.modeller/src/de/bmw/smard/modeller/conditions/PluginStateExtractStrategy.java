package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin State Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStates <em>States</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesTmplParam <em>States Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActive <em>States Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActiveTmplParam <em>States Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginStateExtractStrategy()
 * @model
 * @generated
 */
public interface PluginStateExtractStrategy extends ExtractStrategy {
    /**
     * Returns the value of the '<em><b>States</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>States</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>States</em>' attribute.
     * @see #setStates(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginStateExtractStrategy_States()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getStates();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStates <em>States</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>States</em>' attribute.
     * @see #getStates()
     * @generated
     */
    void setStates(String value);

    /**
     * Returns the value of the '<em><b>States Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>States Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>States Tmpl Param</em>' attribute.
     * @see #setStatesTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginStateExtractStrategy_StatesTmplParam()
     * @model
     * @generated
     */
    String getStatesTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesTmplParam <em>States Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>States Tmpl Param</em>' attribute.
     * @see #getStatesTmplParam()
     * @generated
     */
    void setStatesTmplParam(String value);

    /**
     * Returns the value of the '<em><b>States Active</b></em>' attribute.
     * The default value is <code>"true"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>States Active</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>States Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #setStatesActive(BooleanOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginStateExtractStrategy_StatesActive()
     * @model default="true" required="true"
     *        extendedMetaData="kind='attribute' name='checkStateActive' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    BooleanOrTemplatePlaceholderEnum getStatesActive();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActive <em>States Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>States Active</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum
     * @see #getStatesActive()
     * @generated
     */
    void setStatesActive(BooleanOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>States Active Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>States Active Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>States Active Tmpl Param</em>' attribute.
     * @see #setStatesActiveTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getPluginStateExtractStrategy_StatesActiveTmplParam()
     * @model
     * @generated
     */
    String getStatesActiveTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy#getStatesActiveTmplParam <em>States Active Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>States Active Tmpl Param</em>' attribute.
     * @see #getStatesActiveTmplParam()
     * @generated
     */
    void setStatesActiveTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context);

} // PluginStateExtractStrategy
