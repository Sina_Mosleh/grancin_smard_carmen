/**
 */
package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractMessage#getSignals <em>Signals</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractMessage#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractMessage()
 * @model abstract="true"
 * @generated
 */
public interface AbstractMessage extends BaseClassWithSourceReference {

    /**
     * Returns the value of the '<em><b>Signals</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractSignal}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signals</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signals</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractMessage_Signals()
     * @model containment="true"
     * @generated
     */
    EList<AbstractSignal> getSignals();


    /**
     * @generated NOT
     */
    boolean addSignal(AbstractSignal signal);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    AbstractFilter getPrimaryFilter();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model many="false"
     * @generated
     */
    @Deprecated
    EList<AbstractFilter> getFilters(boolean includingPrimaryFilter);

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Name</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractMessage_Name()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractMessage#getName <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);
} // AbstractMessage
