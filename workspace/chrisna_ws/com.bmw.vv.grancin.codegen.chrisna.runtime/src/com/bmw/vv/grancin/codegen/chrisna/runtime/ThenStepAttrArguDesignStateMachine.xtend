package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap
import chrisna.TimeWindow

class ThenStepAttrArguDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genAttriArguDesignStateMachine
	}
	
	def genAttriArguDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			/*scen?.currentStates?.state.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '0'))
				expressions})))*/
			
			scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check then attributes', 'Comparison', {
					expressions?.clear
					for(assign: thenEvent?.getAssigcontainer?.getAssignment)
						expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
					if(thenEvent?.time?.type?.toLowerCase.contains('timewindow')) {
						for(assign: (thenEvent?.time as TimeWindow)?.getAssigcontainer?.getAssignment)
							expressions?.add(genExpression((assign?.leftSide ?: ''), (assign.operation?.str ?: ''), (assign.rightSide ?: '')))
					}
					expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(0, genState('Ok_' + name, null))
			
			/*scen?.nextStates?.state.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '0'))
				expressions})))*/
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
					expressions?.clear
					expressions?.add(genExpression('true_' + name, '=', 'true'))
					expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
	}
	
}