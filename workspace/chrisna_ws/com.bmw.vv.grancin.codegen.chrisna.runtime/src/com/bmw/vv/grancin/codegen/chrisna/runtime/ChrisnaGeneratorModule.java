package com.bmw.vv.grancin.codegen.chrisna.runtime;

import org.eclipse.xtext.service.AbstractGenericModule;

public class ChrisnaGeneratorModule extends AbstractGenericModule {
	public Class<? extends org.eclipse.xtext.generator.IGenerator2> bindIGenerator2(){
		return ChrisnaGenerator.class;
	}
}
