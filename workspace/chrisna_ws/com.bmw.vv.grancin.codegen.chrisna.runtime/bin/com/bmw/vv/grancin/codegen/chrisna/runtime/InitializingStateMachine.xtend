package com.bmw.vv.grancin.codegen.chrisna.runtime

import java.util.ArrayList
import java.util.HashMap

class InitializingStateMachine extends BaseDesignStateMachine{
	protected val double period
	protected val int name
	
	new(String name, double per){
		this.period = per
		this.name = name.hashCode
		initializer
	}
	
	def getPeriod(){
		return this.period
	}
	
	/*def setPeriod(double peri){
		this.period = peri
	}*/
	
	protected def genInitState(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		var initst = genState('initial state', genEvent('set period ' + name, 'Compute', {
			expressions?.clear
			expressions?.add(genExpression('Period_' + name, '=', this.period.toString))
			expressions}))
		initst
	}
	
	protected def initializer(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(genInitState)
			
			scen?.nextStates?.state?.add(0, genState('Warning_Period_Not_Set' + name, genEvent('set period ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('Period_' + name, '=', '150 ms'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check period', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('Period_'+ name, '==', '0 ms'))
				expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
			
		})
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.nextStates?.state?.add(0, genState('No_Condition_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '0'))
				expressions?.add(genExpression('Wait_Time_' + name, '=', '300 ms'))
				expressions})))
			
			scen?.currentStates?.state.add(genInitState)
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check period', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('Period_'+ name, '!=', '0 ms'))
				expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen?.currentStates?.state.add(0, genState('Warning_Period_Not_Set' + name, genEvent('set period ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('Period_' + name, '=', '150 ms'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
			
		})
	} 
} 