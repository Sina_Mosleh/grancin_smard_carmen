package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap

/*enum StateType {
	INITIALSTATE,
	STATE
}*/

class ThenStepBaseDesignStateMachine extends BaseDesignStateMachine {
	
	protected val int name
	
	protected val SendEvent thenEvent
	//protected val StateType sttype
	
	new(String scenario_name, SendEvent event) {
		name = scenario_name.hashCode
		//sttype = typ
		thenEvent = event
		
		genBaseDesignStateMachine
	}
	
	def genBaseDesignStateMachine(){
		 
		//var expressions = new ArrayList<HashMap<String, String>>()	
	}
}