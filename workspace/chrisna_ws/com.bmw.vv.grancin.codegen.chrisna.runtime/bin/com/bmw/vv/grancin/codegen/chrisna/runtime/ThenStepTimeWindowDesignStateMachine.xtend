package com.bmw.vv.grancin.codegen.chrisna.runtime

import chrisna.SendEvent
import chrisna.TimeWindow
import java.util.ArrayList
import java.util.HashMap

class ThenStepTimeWindowDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genTimeWindowDesignStateMachine
	}
	
	def genTimeWindowDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			/*scen?.currentStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '1'))
				expressions})))*/
			
			scen?.nextStates?.state?.add(0, genState('Wait_for_values_' + name, genEvent('check values', 'Compute', new ArrayList<HashMap<String, String>>())))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check values', 'Comparison', {
				expressions?.clear
				for(assignment : (thenEvent?.time as TimeWindow)?.assigcontainer?.assignment)
					expressions?.add(genExpression((assignment?.leftSide ?: ''), (assignment.operation?.str ?: ''), (assignment.rightSide ?: '')))
				expressions
			}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(genState('Wait_for_values_' + name, genEvent('check values', 'Compute', new ArrayList<HashMap<String, String>>())))
			
			scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check method', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression(thenEvent?.method, '==', '1'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
			
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state.add(0, genState('Ok_' + name, null))
			
			/*scen?.nextStates?.state.add(0, genState('Given_When_Satisfied_' + name, genEvent('set reproduction ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('re_' + name, '=', '1'))
				expressions})))*/
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
	}
	
}