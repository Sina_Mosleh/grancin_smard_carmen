package com.bmw.vv.grancin.codegen.smardmodeller.runtime

enum BusType {
	CAN,//(0, "CAN"),
	LIN,//(1, "LIN"),
	FLEXRAY,//(2, "FlexRay"),
	ETHERNET,//(3, "Ethernet"),
	IPV4,//(4,"IPv4"),
	NONVERBOSEDLT,//(5, "NonVerboseDLT"),
	PLUGIN,//(6, "Plugin"),
	SOMEIP,//(7, "SomeIP"),
	SOMEIPSD,//(8, "SomeIPSD"),
	UDP,//(9, "UDP"),
	UDPNM,//(10, "UDPNM"),
	VERBOSEDLT,
	NONE//(11, "VerboseDLT")
}

enum DecodeType {
	NONE,
	DOUBLE,
	EMPTY,
	STRING
}

enum ExtractType {
	NONE,
	EMPTY,
	PLUGINRESULT,
	PLUGINSTATE,
	UNIVERSALPAYLOAD,
	UNIVERSALPAYLOADWITHLEGACY,
	VERBOSEDLT
}

enum SignalType {
	NONE,
	COMPARATOR,
	CONTAINER,
	DOUBLE,
	HEADER,
	PLUGIN,
	STRING
}

enum VariableType{
	NONE,
	SIGNAL,
	VALUE
}

enum ExpressionType{
	NONE,
	TIMING,
	LOGICAL,
	TRUE,
	NOT,
	MATCHES,
	STOP,
	MESSAGECHECK,
	STATECHECK,
	TRANSITIONCHECK,
	PLUGINCHECK,
	STRING,
	SIGNALCOMPARISON,
	REFERENCECONDITION,
	LINMESSAGECHECK,
	FLEXRAYMESSAGECHECK,
	CANMESSAGECHECK,
	CALCULATION
}

enum StateLinkedTransition{
	IN,
	OUT
}

enum TransitionDirection{
	FROM,
	TO
}

enum ActionType{
	NONE,
	COMPUTEVARIABLE,
	SHOWVARIABLE,
	FLEXRAY,
	CAN,
	LIN,
	PLUGIN
}

enum ActionTmpl{
	ACTION,
	COMPUTEVARIABLE,
	CONTROLACTION
}

enum CurrentNext{
	CURRENT,
	NEXT
}
