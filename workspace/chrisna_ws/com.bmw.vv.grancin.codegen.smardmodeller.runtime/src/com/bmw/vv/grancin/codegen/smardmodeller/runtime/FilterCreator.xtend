package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import de.bmw.smard.modeller.conditions.AbstractFilter
import java.util.UUID
import de.bmw.smard.modeller.conditions.SomeIPFilter
import java.util.HashMap
import java.io.Serializable
import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import de.bmw.smard.modeller.conditions.CANFilter
import de.bmw.smard.modeller.conditions.LINFilter
import de.bmw.smard.modeller.conditions.FlexRayFilter
import de.bmw.smard.modeller.conditions.EthernetFilter
import de.bmw.smard.modeller.conditions.IPv4Filter
import de.bmw.smard.modeller.conditions.NonVerboseDLTFilter
import de.bmw.smard.modeller.conditions.PluginFilter
import de.bmw.smard.modeller.conditions.SomeIPSDFilter
import de.bmw.smard.modeller.conditions.TPFilter
import de.bmw.smard.modeller.conditions.TCPFilter
import de.bmw.smard.modeller.conditions.UDPFilter
import de.bmw.smard.modeller.conditions.UDPNMFilter
import de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter
import de.bmw.smard.modeller.conditions.DLTFilter

class FilterCreator{
	
	protected var ConditionsFactoryImpl conditions
	protected var BusType filtertyp
	
	new (BusType filter){
		conditions = new ConditionsFactoryImpl
		filtertyp = filter
	}
	
	def BusType getFiltertyp(){
		filtertyp
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractFilter createAbstractFilter(AbstractFilter filter){
		filter?.setId(createUUID)
		//filter.sourceReference = ''
		filter
	}
	
	def CANFilter createCANFilter(HashMap<String, Serializable> map){
		var canfilter = conditions?.createCANFilter
		canfilter = createAbstractFilter(canfilter) as CANFilter
		//
		canfilter
	}
	
	def DLTFilter createDLTFilter(HashMap<String, Serializable> map){
		var dltfilter = conditions?.createDLTFilter
		dltfilter = createAbstractFilter(dltfilter) as DLTFilter
		//
		dltfilter
	}
	
	def LINFilter createLINFilter(HashMap<String, Serializable> map){
		var linfilter = conditions?.createLINFilter
		linfilter = createAbstractFilter(linfilter) as LINFilter
		//
		linfilter
	}
	
	def FlexRayFilter createFlexRayFilter(HashMap<String, Serializable> map){
		var flexrayfilter = conditions?.createFlexRayFilter
		flexrayfilter = createAbstractFilter(flexrayfilter) as FlexRayFilter
		//
		flexrayfilter
	}
	
	def EthernetFilter createEthernetFilter(HashMap<String, Serializable> map){
		var ethernetfilter = conditions?.createEthernetFilter
		ethernetfilter = createAbstractFilter(ethernetfilter) as EthernetFilter
		//
		ethernetfilter
	}
	
	def IPv4Filter createIPv4Filter(HashMap<String, Serializable> map){
		var ipv4filter = conditions?.createIPv4Filter
		ipv4filter = createAbstractFilter(ipv4filter) as IPv4Filter
		//
		ipv4filter
	}
	
	def NonVerboseDLTFilter createNonVerboseDLTFilter(HashMap<String, Serializable> map){
		var nonverbosedltfilter = conditions?.createNonVerboseDLTFilter
		nonverbosedltfilter = createAbstractFilter(nonverbosedltfilter) as NonVerboseDLTFilter
		//
		nonverbosedltfilter
	}
	
	def PluginFilter createPluginFilter(HashMap<String, Serializable> map){
		var pluginfilter = conditions?.createPluginFilter
		pluginfilter = createAbstractFilter(pluginfilter) as PluginFilter
		//
		pluginfilter
	}
	
	def SomeIPFilter createSomeIPFilter(HashMap<String, Serializable> map, String interfacever){
		var someipfilter = conditions?.createSomeIPFilter
		someipfilter = createAbstractFilter(someipfilter) as SomeIPFilter
		someipfilter?.setServiceId(searchInMap(map, 'serviceId'))
		//someipfilter.methodType = existInMap(map, "methodId") ? SomeIPMethodTypeOrTemplatePlaceholderEnum.METHOD : SomeIPMethodTypeOrTemplatePlaceholderEnum.EVENT)
		//someipfilter.methodTypeTmplParam = ''
		someipfilter?.setMethodId(searchInMap(map, 'methodId'))
		//someipfilter.clientId = ''
		//someipfilter.sessionId = ''
		//someipfilter.protocolVersion = ''
		someipfilter?.setInterfaceVersion(interfacever)//this.inter.version.major.toString
		//someipfilter.messageType = ''
		//someipfilter.messageTypeImplParam = ''
		someipfilter
	}
	
	def SomeIPSDFilter createSomeIPSDFilter(HashMap<String, Serializable> map){
		var someipsdfilter = conditions?.createSomeIPSDFilter
		someipsdfilter = createAbstractFilter(someipsdfilter) as SomeIPSDFilter
		//
		someipsdfilter
	}
	
	def TPFilter createTPFilter(TPFilter tpfilter){
		var newtpfilter = createAbstractFilter(tpfilter) as TPFilter
		//
		newtpfilter
	}
	
	def TCPFilter createTCPFilter(HashMap<String, Serializable> map){
		var tcpfilter = conditions?.createTCPFilter
		tcpfilter = createTPFilter(tcpfilter) as TCPFilter
		//
		tcpfilter
	}
	
	def UDPFilter createUDPFilter(HashMap<String, Serializable> map){
		var udpfilter = conditions?.createUDPFilter
		udpfilter = createTPFilter(udpfilter) as UDPFilter
		//
		udpfilter
	}
	
	def UDPNMFilter createUDPNMFilter(HashMap<String, Serializable> map){
		var udpnmfilter = conditions?.createUDPNMFilter
		udpnmfilter = createAbstractFilter(udpnmfilter) as UDPNMFilter
		//
		udpnmfilter
	}
	
	def VerboseDLTPayloadFilter createVerboseDLTPayloadFilter(HashMap<String, Serializable> map){
		var verbosedltpayloadfilter = conditions?.createVerboseDLTPayloadFilter
		verbosedltpayloadfilter = createAbstractFilter(verbosedltpayloadfilter) as VerboseDLTPayloadFilter
		//
		verbosedltpayloadfilter
	}
}