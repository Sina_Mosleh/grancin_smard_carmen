package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import java.util.UUID
import java.util.HashMap
import java.io.Serializable
import de.bmw.smard.modeller.conditions.SignalReferenceSet
import de.bmw.smard.modeller.conditions.AbstractSignal
import de.bmw.smard.modeller.conditions.Variable
import de.bmw.smard.modeller.conditions.VariableSet
import java.util.ArrayList
import de.bmw.smard.modeller.conditions.SignalReference
import de.bmw.smard.modeller.conditions.impl.ConditionsFactoryImpl
import de.bmw.smard.modeller.conditions.VariableReference
import de.bmw.smard.modeller.conditions.Comparator
import de.bmw.smard.modeller.conditions.ValueVariable
import org.eclipse.emf.common.util.EList
import de.bmw.smard.modeller.conditions.AbstractMessage
import de.bmw.smard.modeller.statemachine.State
import de.bmw.smard.modeller.statemachine.impl.StatemachineFactoryImpl
import de.bmw.smard.modeller.statemachineset.impl.StatemachinesetFactoryImpl
import chrisna.Assignment
import org.eclipse.gmf.runtime.notation.impl.NotationFactoryImpl

//import de.bmw.smard.modeller.conditions.Condition

class IBaseClassForAllAddMethods{
	
	protected var ConditionsFactoryImpl conditions
	protected var StatemachineFactoryImpl statemachine
	protected var StatemachinesetFactoryImpl statemachineset
	protected var NotationFactoryImpl notation
	//protected var statemachine.DocumentRoot statemachinedocu
	
	new(){
		conditions = new ConditionsFactoryImpl
		//conditions = ConditionUtils.createConditionsDocument
		statemachine = new StatemachineFactoryImpl
		statemachineset = new StatemachinesetFactoryImpl
		notation = new NotationFactoryImpl
	}
	
	def String ExtractDigitFromString(String str) {
		var String s = ''
		for (char ch : str?.toCharArray()) {
			if (Character.isDigit(ch)) {
    			s = s + ch
			}
		}
		if(s == '')
			'0'
		else
			s
	}
	
	def BusType ConvertIntToBusType(int busId){
		switch busId{
			case 45000:
				return BusType.SOMEIP
			default:
				return BusType.NONE
		}
	}
	
	def VariableType ConvertStringToVariableType(String variabletype){
		switch(variabletype) {
			case "SIGNAL" :
				VariableType.SIGNAL
			case "VALUE" :
				VariableType.VALUE
			default :
				VariableType.NONE
		}
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Object> map, String key){
		if(map.containsKey(key))
			map?.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractSignal searchInSignalSet(SignalReferenceSet signalslist, String signalname){
		var AbstractSignal foundsignal = null
		for (message : signalslist?.messages){
			for(signal : message?.signals){
				if (signal?.name?.contains(signalname)){
					foundsignal = signal		
				}
			}
		}
		foundsignal	
	}	
	
	def Variable searchInVariableSet(VariableSet variableset, String variablename){
		var Variable foundvariable = null
		variablename.replaceAll(' ','')
		for (variable : variableset?.variables){
			if (variable?.name == variablename){
				foundvariable = variable as Variable
			}
		}
		foundvariable	
	}
	
	def boolean existInVariables(ArrayList<HashMap<String, Object>> variables, String variablename){
		//var Variable foundvariable = null
		for (variable : variables){
			if (variable.containsKey('name')){
				if((variable.get('name') as String)?.contains(variablename))
					return true
			}
		}
		false	
	}
	
	def SignalReference convertAbstractSignalToSignalReference(AbstractSignal signal){
		var signalreference = conditions?.createSignalReference
		signalreference?.setDescription(signal?.name)
		signalreference?.setSignal(signal)
		signalreference
	}
	
	def VariableReference convertVariableToVariableReference(Variable variable){
		var refvariable = conditions?.createVariableReference
		refvariable?.setDescription(variable?.name)
		refvariable?.setVariable(variable)
		refvariable
	}
	
	def String substringBetween(String str, String start, String end){
		var s = str.substring(str.indexOf("(") + 1);
		s = s?.substring(0, s.indexOf(")"));
		s
	}
	
	def replaceAssignment(Assignment assignment){
		assignment.leftSide = assignment.leftSide.replaceAll('[-.]','_').replaceAll('\\[','_').replaceAll('\\]','_')
		if(assignment.rightSide != '0.0')
			assignment.rightSide = assignment.rightSide.replaceAll('[-.]','_').replaceAll('\\[','_').replaceAll('\\]','_')
	}
	
	def long findingTimeFromString(String string, VariableSet variableset){
		
		var str = string
		if(str?.contains(')') && str?.contains('('))
			str = str?.substringBetween('(', ')')
		val value = Double.parseDouble(((searchInVariableSet(variableset, str) as ValueVariable)?.initialValue) ?: '0').longValue
		value
	}
	
	def Comparator convertToComparator(String str){
		if(str == '=' || str == '==')
			Comparator.EQ
		else if(str == '<')
			Comparator.LT
		else if(str == '=<' || str == '<=')
			Comparator.LE
		else if(str == '>')
			Comparator.GT
		else if(str == '>=' || str == '=>')
			Comparator.GE
		else if(str == '!=' || str == '!' || str.toLowerCase.contains('not'))
			Comparator.NE
		else
			Comparator.TEMPLATE_DEFINED
	}
	
	protected def AbstractSignal searchForSignal(EList<AbstractMessage> messages, String name){
		var str = name
		if(name.contains('[') && name.contains(']')){
			str = name.replace(name.substring(name.indexOf('['), name.indexOf(']')+1), '')
		}
		var arrOfStr = str.split("\\.")
		
				
		for (mes : messages){
			for(sig : mes?.getSignals){
				var  notsig = false
				for(s : arrOfStr){
					if(!(sig?.name.toLowerCase.contains(s.toLowerCase)))
						notsig = true	
				}
				if(!notsig)
					return sig
			}
		}
		return null
	}
	
	protected def boolean isNumeric(String strNum) {
	    try {
	        Double.parseDouble(strNum)
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false
	    }
	    return true
	}
	
	protected def boolean existStateInStatemachine(EList<State> list, State st){
		for(state : list){
			if (state.name == st.name)
				return true
		}
		false
	}
	
	protected def State searchForState(EList<State> list, String name){
		for (state : list){
			if(state?.name.contains(name))
				return state
		}
		null
	}
	
	/*protected def Condition searchForCondition(EList<Condition> list, chrisna.Condition chrisCond){
		for (condition : list){
			if(condition?.to?.name == (name))
				return condition
		}
		null
	}*/
}