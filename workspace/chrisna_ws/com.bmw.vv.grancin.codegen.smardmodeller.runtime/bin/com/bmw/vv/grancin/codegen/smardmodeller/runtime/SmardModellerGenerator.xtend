package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import com.bmw.vv.client.RequestData
import com.bmw.vv.grancin.codegen.chrisna.runtime.ChrisnaFSMGenerator
import com.bmw.vv.grancin.codegen.chrisna.runtime.WriteToFile
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import java.util.ArrayList

class SmardModellerGenerator extends AbstractGenerator {	
    override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context){
    	println("SmardModeller is on board")
    	
    	val writer = new ProjectCreator
    	
    	val csa = new ChrisnaFSMGenerator(resource)
    	if(!csa?.name.contains("null"))
			fsa.generateFile(csa.name +  '/' + 'ChrisnaFSM/' + csa.name + '_chrisna_fsm' + '.csa', new WriteToFile(csa).WriteFile)
			//fsa.generateFile('ChrisnaFSM/' + csa.name + '_chrisna_fsm' + '.csa', xmlwriter.WriteChrisnaFSMXML(csa.scenariocontainer))
		
    	val requestData = new RequestData("q486705", "NTFuYTU4cmE=", "SP2021", "SP2021", "21-07-240", 45000, csa.getInterfaceName)
    	//requestData.request
    	
    	println('SmardModeller is ready to Hack')
    	val sdmodel = new SmardModellerCreator('Sina Mosleh', 'no description', 'no topic', 'EE-900', csa, requestData)
    	println('System is Hacked')
    	println('SmardModeller is ready to destroy and rock the system')
		if(!csa?.name.contains("null")){
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/Conditions/' + 'global_Sigs_Vars' + '.conditions', 
				writer.WriteConditionsXML(sdmodel.conditionDocuGenerator, '../' + 'Conditions/' + 'global_Sigs_Vars' + '.conditions'))
			for (var index = 0; index < sdmodel.getLength; index++){
				var conditionset = sdmodel.getConditionsets.get(index)
				var statemachine = sdmodel.getStmachines.get(index)
				
				fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/Conditions/' + conditionset.name + '.conditions', 
					writer.WriteConditionsXML(sdmodel.conditionDocuGenerator(conditionset), 
					'../' + 'Conditions/' + conditionset.name + '.conditions'))
				fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/StateMachines/' + statemachine.name + '.statemachine', 
					writer.WriteStatemachineXML((sdmodel.stateMachineDocuGenerator(statemachine)),  
					'../' + 'StateMachines/' + statemachine.name + '.statemachine'))
				fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/StateMachines/' + statemachine.name + '.statemachine_diagram', 
					writer.WriteStatemachineDiagram(sdmodel.stateMachineDiagramGenerator(statemachine), 
					'../' + 'StateMachines/' + statemachine.name + '.statemachine_diagram'))		
			}
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/.settings/org.eclipse.core.resources.prefs', writer.makesetting(sdmodel.getStmachines))
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/.project', writer.makeproject(csa.name))
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/Plugins/' + csa.name + '.txt', '')
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/Database/' + csa.name + '.txt', '')
			fsa.generateFile(csa.name +  '/' + 'SMARDAutomat/' + 'default.statemachine_set', 
				writer.WriteStatemachineSetXML(sdmodel.stateMachineSetDocuGenerator(csa.name + 'ExportSMARDArtifakt.zip'), 
				'../../' + csa.name +  '/' + 'SMARDAutomat'))
			
			fsa.generateFile(csa.name + '/' + 'SMARDAutomat/' + 'executionConfig.yaml', 
				writer.WriteConfig(40, 10000, true, true, new ArrayList<Double>())
			)
		}
		println('System is destroyed')
    }
} 