package com.bmw.vv.grancin.codegen.smardmodeller.runtime

import com.bmw.vv.grancin.AttributeAccess
import com.bmw.vv.grancin.Continuation
import com.bmw.vv.grancin.FieldAccess
import com.bmw.vv.grancin.InMethod
import com.bmw.vv.grancin.MemberAccess
import com.bmw.vv.grancin.SendMethod
import com.bmw.vv.grancin.SendNoMethod
import org.eclipse.emf.ecore.resource.Resource
import org.franca.core.franca.FInterface
import org.franca.core.franca.FTypeCollection
import static extension org.eclipse.xtext.EcoreUtil2.* 

class InterfaceDigging {
	
	val Resource res
	
	new (Resource resource) {
		res = resource
	}
	
	def FInterface searchForInterface() {
		val attacc1 = this.res?.allContents?.toIterable
		val attacc = attacc1?.filter(AttributeAccess)
		if (attacc.length != 0)
			attacc?.head?.name?.getContainerOfType(FTypeCollection) as FInterface
		else {
			val inmethod = this.res?.allContents?.toIterable?.filter(InMethod)
			if (inmethod.length != 0)
				inmethod?.head?.method?.getContainerOfType(FTypeCollection) as FInterface
			else {
				val continuation = this.res?.allContents?.toIterable?.filter(Continuation)
				if (continuation.length != 0)
					continuation?.head?.method?.getContainerOfType(FTypeCollection) as FInterface
				else {
					val fieldaccess = this.res?.allContents?.toIterable?.filter(FieldAccess)
					if(fieldaccess.length != 0)
						fieldaccess?.head?.name?.getContainerOfType(FTypeCollection) as FInterface
					else{
						val memberaccess = this.res?.allContents?.toIterable?.filter(MemberAccess)
						if(memberaccess.length != 0)
							memberaccess?.head?.name?.getContainerOfType(FTypeCollection) as FInterface
						else {
							val sendnomethod = this.res?.allContents?.toIterable?.filter(SendNoMethod)
							if(sendnomethod.length != 0)
								sendnomethod?.head?.method?.getContainerOfType(FTypeCollection) as FInterface
							else{
								val sendmethod = this.res?.allContents?.toIterable?.filter(SendMethod)
								if(sendmethod.length != 0)
									sendmethod?.head?.method?.getContainerOfType(FTypeCollection) as FInterface
								else
									null
							}
						}
					}
				}
			}
		}
			
	}
	
}