/**
 */
package conditions.provider;


import conditions.CANFilter;
import conditions.ConditionsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.CANFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CANFilterItemProvider extends AbstractFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CANFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMessageIdRangePropertyDescriptor(object);
			addFrameIdPropertyDescriptor(object);
			addRxtxFlagPropertyDescriptor(object);
			addRxtxFlagTmplParamPropertyDescriptor(object);
			addExtIdentifierPropertyDescriptor(object);
			addExtIdentifierTmplParamPropertyDescriptor(object);
			addFrameTypePropertyDescriptor(object);
			addFrameTypeTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Message Id Range feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageIdRangePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_messageIdRange_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_messageIdRange_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_MessageIdRange(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Frame Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFrameIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_frameId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_frameId_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_FrameId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rxtx Flag feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRxtxFlagPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_rxtxFlag_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_rxtxFlag_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_RxtxFlag(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Rxtx Flag Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRxtxFlagTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_rxtxFlagTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_rxtxFlagTmplParam_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_RxtxFlagTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ext Identifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtIdentifierPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_extIdentifier_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_extIdentifier_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_ExtIdentifier(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ext Identifier Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtIdentifierTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_extIdentifierTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_extIdentifierTmplParam_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_ExtIdentifierTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Frame Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFrameTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_frameType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_frameType_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_FrameType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Frame Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFrameTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CANFilter_frameTypeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CANFilter_frameTypeTmplParam_feature", "_UI_CANFilter_type"),
				 ConditionsPackage.eINSTANCE.getCANFilter_FrameTypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns CANFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CANFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CANFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_CANFilter_type") :
			getString("_UI_CANFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CANFilter.class)) {
			case ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE:
			case ConditionsPackage.CAN_FILTER__FRAME_ID:
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG:
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM:
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER:
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM:
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE:
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
