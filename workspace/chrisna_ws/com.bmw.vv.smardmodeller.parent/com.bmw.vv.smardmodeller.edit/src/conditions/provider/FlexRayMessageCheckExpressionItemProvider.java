/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.FlexRayMessageCheckExpression;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.FlexRayMessageCheckExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FlexRayMessageCheckExpressionItemProvider extends ExpressionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlexRayMessageCheckExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBusIdPropertyDescriptor(object);
			addPayloadPreamblePropertyDescriptor(object);
			addZeroFramePropertyDescriptor(object);
			addSyncFramePropertyDescriptor(object);
			addStartupFramePropertyDescriptor(object);
			addNetworkMgmtPropertyDescriptor(object);
			addFlexrayMessageIdPropertyDescriptor(object);
			addPayloadPreambleTmplParamPropertyDescriptor(object);
			addZeroFrameTmplParamPropertyDescriptor(object);
			addSyncFrameTmplParamPropertyDescriptor(object);
			addStartupFrameTmplParamPropertyDescriptor(object);
			addNetworkMgmtTmplParamPropertyDescriptor(object);
			addBusIdTmplParamPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addTypeTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Bus Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_busId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_busId_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_BusId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Payload Preamble feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPayloadPreamblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_payloadPreamble_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_payloadPreamble_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_PayloadPreamble(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Zero Frame feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addZeroFramePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_zeroFrame_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_zeroFrame_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_ZeroFrame(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sync Frame feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSyncFramePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_syncFrame_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_syncFrame_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_SyncFrame(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Startup Frame feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStartupFramePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_startupFrame_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_startupFrame_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_StartupFrame(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Network Mgmt feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNetworkMgmtPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_networkMgmt_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_networkMgmt_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_NetworkMgmt(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Flexray Message Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlexrayMessageIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_flexrayMessageId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_flexrayMessageId_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_FlexrayMessageId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Payload Preamble Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPayloadPreambleTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_payloadPreambleTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_payloadPreambleTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_PayloadPreambleTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Zero Frame Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addZeroFrameTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_zeroFrameTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_zeroFrameTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_ZeroFrameTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sync Frame Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSyncFrameTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_syncFrameTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_syncFrameTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_SyncFrameTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Startup Frame Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStartupFrameTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_startupFrameTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_startupFrameTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_StartupFrameTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Network Mgmt Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNetworkMgmtTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_networkMgmtTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_networkMgmtTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_NetworkMgmtTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bus Id Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBusIdTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_busIdTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_busIdTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_BusIdTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_type_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_Type(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FlexRayMessageCheckExpression_typeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FlexRayMessageCheckExpression_typeTmplParam_feature", "_UI_FlexRayMessageCheckExpression_type"),
				 ConditionsPackage.eINSTANCE.getFlexRayMessageCheckExpression_TypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns FlexRayMessageCheckExpression.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FlexRayMessageCheckExpression"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((FlexRayMessageCheckExpression)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_FlexRayMessageCheckExpression_type") :
			getString("_UI_FlexRayMessageCheckExpression_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FlexRayMessageCheckExpression.class)) {
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
			case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
