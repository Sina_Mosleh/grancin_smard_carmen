/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.DLTFilter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.DLTFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DLTFilterItemProvider extends AbstractFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLTFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEcuID_ECUPropertyDescriptor(object);
			addSessionID_SEIDPropertyDescriptor(object);
			addApplicationID_APIDPropertyDescriptor(object);
			addContextID_CTIDPropertyDescriptor(object);
			addMessageType_MSTPPropertyDescriptor(object);
			addMessageLogInfo_MSLIPropertyDescriptor(object);
			addMessageTraceInfo_MSTIPropertyDescriptor(object);
			addMessageBusInfo_MSBIPropertyDescriptor(object);
			addMessageControlInfo_MSCIPropertyDescriptor(object);
			addMessageLogInfo_MSLITmplParamPropertyDescriptor(object);
			addMessageTraceInfo_MSTITmplParamPropertyDescriptor(object);
			addMessageBusInfo_MSBITmplParamPropertyDescriptor(object);
			addMessageControlInfo_MSCITmplParamPropertyDescriptor(object);
			addMessageType_MSTPTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Ecu ID ECU feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEcuID_ECUPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_ecuID_ECU_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_ecuID_ECU_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_EcuID_ECU(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Session ID SEID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSessionID_SEIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_sessionID_SEID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_sessionID_SEID_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_SessionID_SEID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Application ID APID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addApplicationID_APIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_applicationID_APID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_applicationID_APID_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_ApplicationID_APID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Context ID CTID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContextID_CTIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_contextID_CTID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_contextID_CTID_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_ContextID_CTID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Type MSTP feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageType_MSTPPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageType_MSTP_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageType_MSTP_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageType_MSTP(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Log Info MSLI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageLogInfo_MSLIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageLogInfo_MSLI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageLogInfo_MSLI_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageLogInfo_MSLI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Trace Info MSTI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageTraceInfo_MSTIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageTraceInfo_MSTI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageTraceInfo_MSTI_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageTraceInfo_MSTI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Bus Info MSBI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageBusInfo_MSBIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageBusInfo_MSBI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageBusInfo_MSBI_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageBusInfo_MSBI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Control Info MSCI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageControlInfo_MSCIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageControlInfo_MSCI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageControlInfo_MSCI_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageControlInfo_MSCI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Log Info MSLI Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageLogInfo_MSLITmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageLogInfo_MSLITmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageLogInfo_MSLITmplParam_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageLogInfo_MSLITmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Trace Info MSTI Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageTraceInfo_MSTITmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageTraceInfo_MSTITmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageTraceInfo_MSTITmplParam_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageTraceInfo_MSTITmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Bus Info MSBI Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageBusInfo_MSBITmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageBusInfo_MSBITmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageBusInfo_MSBITmplParam_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageBusInfo_MSBITmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Control Info MSCI Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageControlInfo_MSCITmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageControlInfo_MSCITmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageControlInfo_MSCITmplParam_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageControlInfo_MSCITmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Type MSTP Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessageType_MSTPTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DLTFilter_messageType_MSTPTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DLTFilter_messageType_MSTPTmplParam_feature", "_UI_DLTFilter_type"),
				 ConditionsPackage.eINSTANCE.getDLTFilter_MessageType_MSTPTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns DLTFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DLTFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DLTFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_DLTFilter_type") :
			getString("_UI_DLTFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DLTFilter.class)) {
			case ConditionsPackage.DLT_FILTER__ECU_ID_ECU:
			case ConditionsPackage.DLT_FILTER__SESSION_ID_SEID:
			case ConditionsPackage.DLT_FILTER__APPLICATION_ID_APID:
			case ConditionsPackage.DLT_FILTER__CONTEXT_ID_CTID:
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP:
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI:
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI:
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI:
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI:
			case ConditionsPackage.DLT_FILTER__MESSAGE_LOG_INFO_MSLI_TMPL_PARAM:
			case ConditionsPackage.DLT_FILTER__MESSAGE_TRACE_INFO_MSTI_TMPL_PARAM:
			case ConditionsPackage.DLT_FILTER__MESSAGE_BUS_INFO_MSBI_TMPL_PARAM:
			case ConditionsPackage.DLT_FILTER__MESSAGE_CONTROL_INFO_MSCI_TMPL_PARAM:
			case ConditionsPackage.DLT_FILTER__MESSAGE_TYPE_MSTP_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
