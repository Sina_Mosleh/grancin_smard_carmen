/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.UniversalPayloadExtractStrategy;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.UniversalPayloadExtractStrategy} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UniversalPayloadExtractStrategyItemProvider extends ExtractStrategyItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalPayloadExtractStrategyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addExtractionRulePropertyDescriptor(object);
			addByteOrderPropertyDescriptor(object);
			addExtractionRuleTmplParamPropertyDescriptor(object);
			addSignalDataTypePropertyDescriptor(object);
			addSignalDataTypeTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Extraction Rule feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtractionRulePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UniversalPayloadExtractStrategy_extractionRule_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UniversalPayloadExtractStrategy_extractionRule_feature", "_UI_UniversalPayloadExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy_ExtractionRule(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Byte Order feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addByteOrderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UniversalPayloadExtractStrategy_byteOrder_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UniversalPayloadExtractStrategy_byteOrder_feature", "_UI_UniversalPayloadExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy_ByteOrder(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Extraction Rule Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExtractionRuleTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UniversalPayloadExtractStrategy_extractionRuleTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UniversalPayloadExtractStrategy_extractionRuleTmplParam_feature", "_UI_UniversalPayloadExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy_ExtractionRuleTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Signal Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSignalDataTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UniversalPayloadExtractStrategy_signalDataType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UniversalPayloadExtractStrategy_signalDataType_feature", "_UI_UniversalPayloadExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy_SignalDataType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Signal Data Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSignalDataTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_UniversalPayloadExtractStrategy_signalDataTypeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_UniversalPayloadExtractStrategy_signalDataTypeTmplParam_feature", "_UI_UniversalPayloadExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy_SignalDataTypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns UniversalPayloadExtractStrategy.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/UniversalPayloadExtractStrategy"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((UniversalPayloadExtractStrategy)object).getExtractionRule();
		return label == null || label.length() == 0 ?
			getString("_UI_UniversalPayloadExtractStrategy_type") :
			getString("_UI_UniversalPayloadExtractStrategy_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(UniversalPayloadExtractStrategy.class)) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
