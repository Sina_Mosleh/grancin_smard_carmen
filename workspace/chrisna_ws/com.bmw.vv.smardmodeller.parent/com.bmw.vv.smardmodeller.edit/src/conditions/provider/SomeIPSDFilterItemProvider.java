/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.SomeIPSDFilter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.SomeIPSDFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SomeIPSDFilterItemProvider extends AbstractFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFlagsPropertyDescriptor(object);
			addFlagsTmplParamPropertyDescriptor(object);
			addSdTypePropertyDescriptor(object);
			addSdTypeTmplParamPropertyDescriptor(object);
			addInstanceIdPropertyDescriptor(object);
			addTtlPropertyDescriptor(object);
			addMajorVersionPropertyDescriptor(object);
			addMinorVersionPropertyDescriptor(object);
			addEventGroupIdPropertyDescriptor(object);
			addIndexFirstOptionPropertyDescriptor(object);
			addIndexSecondOptionPropertyDescriptor(object);
			addNumberFirstOptionPropertyDescriptor(object);
			addNumberSecondOptionPropertyDescriptor(object);
			addServiceId_SomeIPSDPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Flags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_flags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_flags_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_Flags(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Flags Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFlagsTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_flagsTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_flagsTmplParam_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_FlagsTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sd Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSdTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_sdType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_sdType_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_SdType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sd Type Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSdTypeTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_sdTypeTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_sdTypeTmplParam_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_SdTypeTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Instance Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInstanceIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_instanceId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_instanceId_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_InstanceId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ttl feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTtlPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_ttl_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_ttl_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_Ttl(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Major Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMajorVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_majorVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_majorVersion_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_MajorVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Minor Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinorVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_minorVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_minorVersion_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_MinorVersion(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Event Group Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventGroupIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_eventGroupId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_eventGroupId_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_EventGroupId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Index First Option feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndexFirstOptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_indexFirstOption_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_indexFirstOption_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_IndexFirstOption(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Index Second Option feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIndexSecondOptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_indexSecondOption_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_indexSecondOption_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_IndexSecondOption(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number First Option feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberFirstOptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_numberFirstOption_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_numberFirstOption_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_NumberFirstOption(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Second Option feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumberSecondOptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_numberSecondOption_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_numberSecondOption_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_NumberSecondOption(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Service Id Some IPSD feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addServiceId_SomeIPSDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SomeIPSDFilter_serviceId_SomeIPSD_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SomeIPSDFilter_serviceId_SomeIPSD_feature", "_UI_SomeIPSDFilter_type"),
				 ConditionsPackage.eINSTANCE.getSomeIPSDFilter_ServiceId_SomeIPSD(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns SomeIPSDFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SomeIPSDFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SomeIPSDFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_SomeIPSDFilter_type") :
			getString("_UI_SomeIPSDFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SomeIPSDFilter.class)) {
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS:
			case ConditionsPackage.SOME_IPSD_FILTER__FLAGS_TMPL_PARAM:
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE:
			case ConditionsPackage.SOME_IPSD_FILTER__SD_TYPE_TMPL_PARAM:
			case ConditionsPackage.SOME_IPSD_FILTER__INSTANCE_ID:
			case ConditionsPackage.SOME_IPSD_FILTER__TTL:
			case ConditionsPackage.SOME_IPSD_FILTER__MAJOR_VERSION:
			case ConditionsPackage.SOME_IPSD_FILTER__MINOR_VERSION:
			case ConditionsPackage.SOME_IPSD_FILTER__EVENT_GROUP_ID:
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_FIRST_OPTION:
			case ConditionsPackage.SOME_IPSD_FILTER__INDEX_SECOND_OPTION:
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_FIRST_OPTION:
			case ConditionsPackage.SOME_IPSD_FILTER__NUMBER_SECOND_OPTION:
			case ConditionsPackage.SOME_IPSD_FILTER__SERVICE_ID_SOME_IPSD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
