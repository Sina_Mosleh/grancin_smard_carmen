/**
 */
package conditions.provider;


import conditions.ConditionsFactory;
import conditions.ConditionsPackage;
import conditions.Extract;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import statemachineset.provider.SmardmodellerEditPlugin;

/**
 * This is the item provider adapter for a {@link conditions.Extract} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtractItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtractItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addRegexPropertyDescriptor(object);
			addGroupIndexPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Regex feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRegexPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RegexOperation_regex_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RegexOperation_regex_feature", "_UI_RegexOperation_type"),
				 ConditionsPackage.eINSTANCE.getRegexOperation_Regex(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Group Index feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGroupIndexPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Extract_groupIndex_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Extract_groupIndex_feature", "_UI_Extract_type"),
				 ConditionsPackage.eINSTANCE.getExtract_GroupIndex(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex());
			childrenFeatures.add(ConditionsPackage.eINSTANCE.getExtract_StringToExtract());
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Extract.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Extract"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Extract)object).getRegex();
		return label == null || label.length() == 0 ?
			getString("_UI_Extract_type") :
			getString("_UI_Extract_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Extract.class)) {
			case ConditionsPackage.EXTRACT__REGEX:
			case ConditionsPackage.EXTRACT__GROUP_INDEX:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
			case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex(),
				 ConditionsFactory.eINSTANCE.createVariableReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex(),
				 ConditionsFactory.eINSTANCE.createSignalReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex(),
				 ConditionsFactory.eINSTANCE.createExtract()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex(),
				 ConditionsFactory.eINSTANCE.createSubstring()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex(),
				 ConditionsFactory.eINSTANCE.createParseNumericToString()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getExtract_StringToExtract(),
				 ConditionsFactory.eINSTANCE.createVariableReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getExtract_StringToExtract(),
				 ConditionsFactory.eINSTANCE.createSignalReference()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getExtract_StringToExtract(),
				 ConditionsFactory.eINSTANCE.createExtract()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getExtract_StringToExtract(),
				 ConditionsFactory.eINSTANCE.createSubstring()));

		newChildDescriptors.add
			(createChildParameter
				(ConditionsPackage.eINSTANCE.getExtract_StringToExtract(),
				 ConditionsFactory.eINSTANCE.createParseNumericToString()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ConditionsPackage.eINSTANCE.getRegexOperation_DynamicRegex() ||
			childFeature == ConditionsPackage.eINSTANCE.getExtract_StringToExtract();

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SmardmodellerEditPlugin.INSTANCE;
	}

}
