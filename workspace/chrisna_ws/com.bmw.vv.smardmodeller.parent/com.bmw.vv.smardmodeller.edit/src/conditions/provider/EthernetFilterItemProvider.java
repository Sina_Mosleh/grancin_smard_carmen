/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.EthernetFilter;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.EthernetFilter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EthernetFilterItemProvider extends AbstractFilterItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EthernetFilterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourceMACPropertyDescriptor(object);
			addDestMACPropertyDescriptor(object);
			addEtherTypePropertyDescriptor(object);
			addInnerVlanIdPropertyDescriptor(object);
			addOuterVlanIdPropertyDescriptor(object);
			addInnerVlanCFIPropertyDescriptor(object);
			addOuterVlanCFIPropertyDescriptor(object);
			addInnerVlanVIDPropertyDescriptor(object);
			addOuterVlanVIDPropertyDescriptor(object);
			addCRCPropertyDescriptor(object);
			addInnerVlanTPIDPropertyDescriptor(object);
			addOuterVlanTPIDPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source MAC feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceMACPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_sourceMAC_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_sourceMAC_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_SourceMAC(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Dest MAC feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDestMACPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_destMAC_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_destMAC_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_DestMAC(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Ether Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEtherTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_etherType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_etherType_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_EtherType(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Inner Vlan Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInnerVlanIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_innerVlanId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_innerVlanId_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_InnerVlanId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outer Vlan Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOuterVlanIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_outerVlanId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_outerVlanId_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_OuterVlanId(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Inner Vlan CFI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInnerVlanCFIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_innerVlanCFI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_innerVlanCFI_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_InnerVlanCFI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outer Vlan CFI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOuterVlanCFIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_outerVlanCFI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_outerVlanCFI_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_OuterVlanCFI(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Inner Vlan VID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInnerVlanVIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_innerVlanVID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_innerVlanVID_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_InnerVlanVID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outer Vlan VID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOuterVlanVIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_outerVlanVID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_outerVlanVID_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_OuterVlanVID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the CRC feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCRCPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_CRC_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_CRC_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_CRC(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Inner Vlan TPID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInnerVlanTPIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_innerVlanTPID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_innerVlanTPID_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_InnerVlanTPID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Outer Vlan TPID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOuterVlanTPIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EthernetFilter_outerVlanTPID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EthernetFilter_outerVlanTPID_feature", "_UI_EthernetFilter_type"),
				 ConditionsPackage.eINSTANCE.getEthernetFilter_OuterVlanTPID(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns EthernetFilter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EthernetFilter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((EthernetFilter)object).getId();
		return label == null || label.length() == 0 ?
			getString("_UI_EthernetFilter_type") :
			getString("_UI_EthernetFilter_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EthernetFilter.class)) {
			case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
			case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
			case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
			case ConditionsPackage.ETHERNET_FILTER__CRC:
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
