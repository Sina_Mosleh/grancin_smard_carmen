/**
 */
package conditions.provider;


import conditions.ConditionsPackage;
import conditions.PluginStateExtractStrategy;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link conditions.PluginStateExtractStrategy} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PluginStateExtractStrategyItemProvider extends ExtractStrategyItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginStateExtractStrategyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addStatesPropertyDescriptor(object);
			addStatesTmplParamPropertyDescriptor(object);
			addStatesActivePropertyDescriptor(object);
			addStatesActiveTmplParamPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the States feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PluginStateExtractStrategy_states_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PluginStateExtractStrategy_states_feature", "_UI_PluginStateExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getPluginStateExtractStrategy_States(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the States Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatesTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PluginStateExtractStrategy_statesTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PluginStateExtractStrategy_statesTmplParam_feature", "_UI_PluginStateExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getPluginStateExtractStrategy_StatesTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the States Active feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatesActivePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PluginStateExtractStrategy_statesActive_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PluginStateExtractStrategy_statesActive_feature", "_UI_PluginStateExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getPluginStateExtractStrategy_StatesActive(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the States Active Tmpl Param feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatesActiveTmplParamPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_PluginStateExtractStrategy_statesActiveTmplParam_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_PluginStateExtractStrategy_statesActiveTmplParam_feature", "_UI_PluginStateExtractStrategy_type"),
				 ConditionsPackage.eINSTANCE.getPluginStateExtractStrategy_StatesActiveTmplParam(),
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns PluginStateExtractStrategy.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/PluginStateExtractStrategy"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((PluginStateExtractStrategy)object).getStates();
		return label == null || label.length() == 0 ?
			getString("_UI_PluginStateExtractStrategy_type") :
			getString("_UI_PluginStateExtractStrategy_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(PluginStateExtractStrategy.class)) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
