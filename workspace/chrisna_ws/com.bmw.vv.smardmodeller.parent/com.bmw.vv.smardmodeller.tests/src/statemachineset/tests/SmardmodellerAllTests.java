/**
 */
package statemachineset.tests;

import conditions.tests.ConditionsTests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

import statemachine.tests.StatemachineTests;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Smardmodeller</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class SmardmodellerAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new SmardmodellerAllTests("Smardmodeller Tests");
		suite.addTest(StatemachinesetTests.suite());
		suite.addTest(ConditionsTests.suite());
		suite.addTest(StatemachineTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SmardmodellerAllTests(String name) {
		super(name);
	}

} //SmardmodellerAllTests
