/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.VerboseDLTMessage;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VerboseDLTMessageTest extends DLTMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VerboseDLTMessageTest.class);
	}

	/**
	 * Constructs a new Verbose DLT Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerboseDLTMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Verbose DLT Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VerboseDLTMessage getFixture() {
		return (VerboseDLTMessage)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createVerboseDLTMessage());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VerboseDLTMessageTest
