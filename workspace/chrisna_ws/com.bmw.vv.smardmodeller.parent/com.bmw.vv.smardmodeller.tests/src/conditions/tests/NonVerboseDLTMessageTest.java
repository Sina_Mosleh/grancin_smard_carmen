/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.NonVerboseDLTMessage;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Non Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class NonVerboseDLTMessageTest extends DLTMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(NonVerboseDLTMessageTest.class);
	}

	/**
	 * Constructs a new Non Verbose DLT Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NonVerboseDLTMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Non Verbose DLT Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NonVerboseDLTMessage getFixture() {
		return (NonVerboseDLTMessage)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createNonVerboseDLTMessage());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //NonVerboseDLTMessageTest
