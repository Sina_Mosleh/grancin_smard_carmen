/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.EmptyExtractStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Empty Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmptyExtractStrategyTest extends ExtractStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmptyExtractStrategyTest.class);
	}

	/**
	 * Constructs a new Empty Extract Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmptyExtractStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Empty Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmptyExtractStrategy getFixture() {
		return (EmptyExtractStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createEmptyExtractStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmptyExtractStrategyTest
