/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.VerboseDLTExtractStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Verbose DLT Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VerboseDLTExtractStrategyTest extends ExtractStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VerboseDLTExtractStrategyTest.class);
	}

	/**
	 * Constructs a new Verbose DLT Extract Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerboseDLTExtractStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Verbose DLT Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VerboseDLTExtractStrategy getFixture() {
		return (VerboseDLTExtractStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createVerboseDLTExtractStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VerboseDLTExtractStrategyTest
