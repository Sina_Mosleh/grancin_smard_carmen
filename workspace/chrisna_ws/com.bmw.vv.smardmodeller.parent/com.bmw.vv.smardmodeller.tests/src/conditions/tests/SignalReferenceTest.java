/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.SignalReference;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Signal Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.SignalReference#isValid_hasValidSignal(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SignalReferenceTest extends ComparatorSignalTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SignalReferenceTest.class);
	}

	/**
	 * Constructs a new Signal Reference test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SignalReferenceTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Signal Reference test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SignalReference getFixture() {
		return (SignalReference)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createSignalReference());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.SignalReference#isValid_hasValidSignal(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SignalReference#isValid_hasValidSignal(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSignal__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //SignalReferenceTest
