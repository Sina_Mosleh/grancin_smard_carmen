/**
 */
package conditions.tests;

import conditions.DLTMessage;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>DLT Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DLTMessageTest extends AbstractBusMessageTest {

	/**
	 * Constructs a new DLT Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DLTMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this DLT Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DLTMessage getFixture() {
		return (DLTMessage)fixture;
	}

} //DLTMessageTest
