/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.LogicalExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Logical Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.LogicalExpression#isValid_hasValidOperator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operator</em>}</li>
 *   <li>{@link conditions.LogicalExpression#isValid_hasValidSubExpressions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sub Expressions</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class LogicalExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LogicalExpressionTest.class);
	}

	/**
	 * Constructs a new Logical Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Logical Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LogicalExpression getFixture() {
		return (LogicalExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createLogicalExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.LogicalExpression#isValid_hasValidOperator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LogicalExpression#isValid_hasValidOperator(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOperator__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.LogicalExpression#isValid_hasValidSubExpressions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sub Expressions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LogicalExpression#isValid_hasValidSubExpressions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSubExpressions__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //LogicalExpressionTest
