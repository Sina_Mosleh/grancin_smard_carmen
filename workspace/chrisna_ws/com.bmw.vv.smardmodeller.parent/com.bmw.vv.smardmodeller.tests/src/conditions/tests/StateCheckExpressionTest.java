/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.StateCheckExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>State Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.StateCheckExpression#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid States Active</em>}</li>
 *   <li>{@link conditions.StateCheckExpression#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid State</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StateCheckExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StateCheckExpressionTest.class);
	}

	/**
	 * Constructs a new State Check Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateCheckExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this State Check Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StateCheckExpression getFixture() {
		return (StateCheckExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createStateCheckExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.StateCheckExpression#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid States Active</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.StateCheckExpression#isValid_hasValidStatesActive(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStatesActive__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.StateCheckExpression#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.StateCheckExpression#isValid_hasValidState(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidState__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //StateCheckExpressionTest
