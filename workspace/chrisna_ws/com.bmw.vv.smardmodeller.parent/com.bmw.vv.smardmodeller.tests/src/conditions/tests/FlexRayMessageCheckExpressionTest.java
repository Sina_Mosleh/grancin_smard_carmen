/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.FlexRayMessageCheckExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Flex Ray Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidStartupFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startup Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidSyncFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sync Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidZeroFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Zero Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidPayloadPreamble(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Payload Preamble</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidNetworkMgmt(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Network Mgmt</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidMessageId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class FlexRayMessageCheckExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FlexRayMessageCheckExpressionTest.class);
	}

	/**
	 * Constructs a new Flex Ray Message Check Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlexRayMessageCheckExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Flex Ray Message Check Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FlexRayMessageCheckExpression getFixture() {
		return (FlexRayMessageCheckExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createFlexRayMessageCheckExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidBusId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCheckType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidStartupFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Startup Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidStartupFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidStartupFrame__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidSyncFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sync Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidSyncFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSyncFrame__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidZeroFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Zero Frame</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidZeroFrame(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidZeroFrame__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidPayloadPreamble(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Payload Preamble</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidPayloadPreamble(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidPayloadPreamble__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidNetworkMgmt(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Network Mgmt</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidNetworkMgmt(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidNetworkMgmt__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayMessageCheckExpression#isValid_hasValidMessageId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayMessageCheckExpression#isValid_hasValidMessageId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //FlexRayMessageCheckExpressionTest
