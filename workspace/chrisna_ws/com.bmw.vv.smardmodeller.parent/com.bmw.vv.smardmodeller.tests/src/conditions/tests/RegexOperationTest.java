/**
 */
package conditions.tests;

import conditions.RegexOperation;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Regex Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class RegexOperationTest extends TestCase {

	/**
	 * The fixture for this Regex Operation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RegexOperation fixture = null;

	/**
	 * Constructs a new Regex Operation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RegexOperationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Regex Operation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(RegexOperation fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Regex Operation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RegexOperation getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Regex</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.RegexOperation#isValid_hasValidRegex(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidRegex__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //RegexOperationTest
