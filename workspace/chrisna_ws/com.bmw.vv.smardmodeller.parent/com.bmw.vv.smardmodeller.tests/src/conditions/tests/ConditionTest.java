/**
 */
package conditions.tests;

import conditions.Condition;
import conditions.ConditionsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.Condition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}</li>
 *   <li>{@link conditions.Condition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}</li>
 *   <li>{@link conditions.IStateTransitionReference#getStateDependencies() <em>Get State Dependencies</em>}</li>
 *   <li>{@link conditions.IStateTransitionReference#getTransitionDependencies() <em>Get Transition Dependencies</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ConditionTest extends BaseClassWithIDTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ConditionTest.class);
	}

	/**
	 * Constructs a new Condition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Condition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Condition getFixture() {
		return (Condition)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createCondition());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.Condition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Condition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidName__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.Condition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.Condition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDescription__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IStateTransitionReference#getStateDependencies() <em>Get State Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStateTransitionReference#getStateDependencies()
	 * @generated
	 */
	public void testGetStateDependencies() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IStateTransitionReference#getTransitionDependencies() <em>Get Transition Dependencies</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IStateTransitionReference#getTransitionDependencies()
	 * @generated
	 */
	public void testGetTransitionDependencies() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	public void testGetReadVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	public void testGetWriteVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ConditionTest
