/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.SomeIPSDMessage;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Some IPSD Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SomeIPSDMessageTest extends AbstractBusMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SomeIPSDMessageTest.class);
	}

	/**
	 * Constructs a new Some IPSD Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPSDMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Some IPSD Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SomeIPSDMessage getFixture() {
		return (SomeIPSDMessage)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createSomeIPSDMessage());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SomeIPSDMessageTest
