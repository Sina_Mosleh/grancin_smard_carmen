/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.SomeIPFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Some IP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidServiceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Service Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidMethodId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidSessionId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Session Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidClientId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Client Id</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Length</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidProtocolVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Protocol Version</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidInterfaceVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interface Version</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidReturnCode(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Return Code</em>}</li>
 *   <li>{@link conditions.SomeIPFilter#isValid_hasValidMethodType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class SomeIPFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SomeIPFilterTest.class);
	}

	/**
	 * Constructs a new Some IP Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SomeIPFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Some IP Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SomeIPFilter getFixture() {
		return (SomeIPFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createSomeIPFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidServiceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Service Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidServiceId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidServiceId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidMethodId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidMethodId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMethodId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidSessionId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Session Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidSessionId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSessionId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidClientId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Client Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidClientId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidClientId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidLength(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidLength__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidProtocolVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Protocol Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidProtocolVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidProtocolVersion__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidInterfaceVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Interface Version</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidInterfaceVersion(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidInterfaceVersion__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidMessageType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidReturnCode(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Return Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidReturnCode(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidReturnCode__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.SomeIPFilter#isValid_hasValidMethodType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Method Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.SomeIPFilter#isValid_hasValidMethodType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMethodType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //SomeIPFilterTest
