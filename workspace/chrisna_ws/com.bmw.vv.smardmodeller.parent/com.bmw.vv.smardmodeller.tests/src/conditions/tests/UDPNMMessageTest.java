/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.UDPNMMessage;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>UDPNM Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.UDPNMMessage#isValid_hasValidUDPFilter(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid UDP Filter</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UDPNMMessageTest extends AbstractBusMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UDPNMMessageTest.class);
	}

	/**
	 * Constructs a new UDPNM Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UDPNMMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this UDPNM Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UDPNMMessage getFixture() {
		return (UDPNMMessage)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createUDPNMMessage());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.UDPNMMessage#isValid_hasValidUDPFilter(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid UDP Filter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPNMMessage#isValid_hasValidUDPFilter(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidUDPFilter__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UDPNMMessageTest
