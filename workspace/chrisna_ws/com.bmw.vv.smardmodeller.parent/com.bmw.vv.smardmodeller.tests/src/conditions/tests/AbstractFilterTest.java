/**
 */
package conditions.tests;

import conditions.AbstractFilter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link conditions.AbstractFilter#getPayloadLength() <em>Payload Length</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractFilterTest extends BaseClassWithSourceReferenceTest {

	/**
	 * Constructs a new Abstract Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractFilter getFixture() {
		return (AbstractFilter)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractFilter#getPayloadLength() <em>Payload Length</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractFilter#getPayloadLength()
	 * @generated
	 */
	public void testGetPayloadLength() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractFilterTest
