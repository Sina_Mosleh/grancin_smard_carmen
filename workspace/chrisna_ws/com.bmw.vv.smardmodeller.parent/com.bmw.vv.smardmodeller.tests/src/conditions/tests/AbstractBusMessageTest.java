/**
 */
package conditions.tests;

import conditions.AbstractBusMessage;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Bus Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.AbstractBusMessage#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}</li>
 *   <li>{@link conditions.AbstractBusMessage#isValid_isOsiLayerConform(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid is Osi Layer Conform</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractBusMessageTest extends AbstractMessageTest {

	/**
	 * Constructs a new Abstract Bus Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractBusMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Bus Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractBusMessage getFixture() {
		return (AbstractBusMessage)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractBusMessage#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractBusMessage#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidBusId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractBusMessage#isValid_isOsiLayerConform(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid is Osi Layer Conform</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractBusMessage#isValid_isOsiLayerConform(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_isOsiLayerConform__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractBusMessageTest
