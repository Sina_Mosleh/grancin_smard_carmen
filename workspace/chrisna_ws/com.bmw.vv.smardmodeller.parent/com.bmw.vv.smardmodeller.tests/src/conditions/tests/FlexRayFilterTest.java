/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.FlexRayFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Flex Ray Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.FlexRayFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#isValid_hasValidChannelType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Channel Type</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#isValid_hasValidCycleOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Offset</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#isValid_hasValidCycleRepeatInterval(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Repeat Interval</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class FlexRayFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(FlexRayFilterTest.class);
	}

	/**
	 * Constructs a new Flex Ray Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlexRayFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Flex Ray Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FlexRayFilter getFixture() {
		return (FlexRayFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createFlexRayFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.FlexRayFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Frame Id Or Frame Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayFilter#isValid_hasValidFrameIdOrFrameIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidFrameIdOrFrameIdRange__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayFilter#isValid_hasValidChannelType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Channel Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayFilter#isValid_hasValidChannelType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidChannelType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayFilter#isValid_hasValidCycleOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayFilter#isValid_hasValidCycleOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCycleOffset__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.FlexRayFilter#isValid_hasValidCycleRepeatInterval(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Cycle Repeat Interval</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.FlexRayFilter#isValid_hasValidCycleRepeatInterval(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCycleRepeatInterval__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //FlexRayFilterTest
