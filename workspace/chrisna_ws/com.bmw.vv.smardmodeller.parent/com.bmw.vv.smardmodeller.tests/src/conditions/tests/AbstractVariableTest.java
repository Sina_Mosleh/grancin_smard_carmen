/**
 */
package conditions.tests;

import conditions.AbstractVariable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.AbstractVariable#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}</li>
 *   <li>{@link conditions.AbstractVariable#isValid_hasValidDisplayName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Display Name</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractVariableTest extends BaseClassWithIDTest {

	/**
	 * Constructs a new Abstract Variable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractVariableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Variable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractVariable getFixture() {
		return (AbstractVariable)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractVariable#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractVariable#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidName__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractVariable#isValid_hasValidDisplayName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Display Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractVariable#isValid_hasValidDisplayName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDisplayName__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	public void testGetReadVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	public void testGetWriteVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractVariableTest
