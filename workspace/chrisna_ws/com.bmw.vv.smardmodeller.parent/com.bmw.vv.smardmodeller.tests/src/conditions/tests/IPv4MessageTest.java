/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.IPv4Message;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>IPv4 Message</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class IPv4MessageTest extends AbstractBusMessageTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IPv4MessageTest.class);
	}

	/**
	 * Constructs a new IPv4 Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IPv4MessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this IPv4 Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected IPv4Message getFixture() {
		return (IPv4Message)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createIPv4Message());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //IPv4MessageTest
