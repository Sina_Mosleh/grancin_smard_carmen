/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.ReferenceConditionExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Reference Condition Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.ReferenceConditionExpression#isValid_hasValidReferencedCondition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Referenced Condition</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ReferenceConditionExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ReferenceConditionExpressionTest.class);
	}

	/**
	 * Constructs a new Reference Condition Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceConditionExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Reference Condition Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ReferenceConditionExpression getFixture() {
		return (ReferenceConditionExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createReferenceConditionExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.ReferenceConditionExpression#isValid_hasValidReferencedCondition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Referenced Condition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ReferenceConditionExpression#isValid_hasValidReferencedCondition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidReferencedCondition__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ReferenceConditionExpressionTest
