/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.DoubleDecodeStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Double Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.DoubleDecodeStrategy#isValid_hasValidFactor(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Factor</em>}</li>
 *   <li>{@link conditions.DoubleDecodeStrategy#isValid_hasValidOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Offset</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class DoubleDecodeStrategyTest extends DecodeStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DoubleDecodeStrategyTest.class);
	}

	/**
	 * Constructs a new Double Decode Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleDecodeStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Double Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DoubleDecodeStrategy getFixture() {
		return (DoubleDecodeStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createDoubleDecodeStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.DoubleDecodeStrategy#isValid_hasValidFactor(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Factor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DoubleDecodeStrategy#isValid_hasValidFactor(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidFactor__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.DoubleDecodeStrategy#isValid_hasValidOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Offset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.DoubleDecodeStrategy#isValid_hasValidOffset(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOffset__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //DoubleDecodeStrategyTest
