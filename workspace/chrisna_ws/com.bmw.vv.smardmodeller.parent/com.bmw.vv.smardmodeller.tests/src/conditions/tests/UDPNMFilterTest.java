/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.UDPNMFilter;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>UDPNM Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.UDPNMFilter#isValid_hasValidSourceNodeIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Node Identifier</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#isValid_hasValidControlBitVector(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Control Bit Vector</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#isValid_hasValidPwfStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Pwf Status</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#isValid_hasValidTeilnetzStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Teilnetz Status</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UDPNMFilterTest extends AbstractFilterTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UDPNMFilterTest.class);
	}

	/**
	 * Constructs a new UDPNM Filter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UDPNMFilterTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this UDPNM Filter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UDPNMFilter getFixture() {
		return (UDPNMFilter)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createUDPNMFilter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.UDPNMFilter#isValid_hasValidSourceNodeIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Source Node Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPNMFilter#isValid_hasValidSourceNodeIdentifier(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSourceNodeIdentifier__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UDPNMFilter#isValid_hasValidControlBitVector(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Control Bit Vector</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPNMFilter#isValid_hasValidControlBitVector(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidControlBitVector__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UDPNMFilter#isValid_hasValidPwfStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Pwf Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPNMFilter#isValid_hasValidPwfStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidPwfStatus__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UDPNMFilter#isValid_hasValidTeilnetzStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Teilnetz Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UDPNMFilter#isValid_hasValidTeilnetzStatus(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidTeilnetzStatus__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UDPNMFilterTest
