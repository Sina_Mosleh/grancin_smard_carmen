/**
 */
package conditions.tests;

import conditions.BaseClassWithSourceReference;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Base Class With Source Reference</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class BaseClassWithSourceReferenceTest extends BaseClassWithIDTest {

	/**
	 * Constructs a new Base Class With Source Reference test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BaseClassWithSourceReferenceTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Base Class With Source Reference test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected BaseClassWithSourceReference getFixture() {
		return (BaseClassWithSourceReference)fixture;
	}

} //BaseClassWithSourceReferenceTest
