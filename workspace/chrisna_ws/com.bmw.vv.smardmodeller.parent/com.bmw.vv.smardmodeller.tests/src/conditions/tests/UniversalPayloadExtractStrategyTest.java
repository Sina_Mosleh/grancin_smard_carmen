/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.UniversalPayloadExtractStrategy;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Universal Payload Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidExtractString(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Extract String</em>}</li>
 *   <li>{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidSignalDataType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal Data Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class UniversalPayloadExtractStrategyTest extends ExtractStrategyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(UniversalPayloadExtractStrategyTest.class);
	}

	/**
	 * Constructs a new Universal Payload Extract Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UniversalPayloadExtractStrategyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Universal Payload Extract Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected UniversalPayloadExtractStrategy getFixture() {
		return (UniversalPayloadExtractStrategy)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createUniversalPayloadExtractStrategy());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidExtractString(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Extract String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UniversalPayloadExtractStrategy#isValid_hasValidExtractString(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidExtractString__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.UniversalPayloadExtractStrategy#isValid_hasValidSignalDataType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Signal Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.UniversalPayloadExtractStrategy#isValid_hasValidSignalDataType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidSignalDataType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //UniversalPayloadExtractStrategyTest
