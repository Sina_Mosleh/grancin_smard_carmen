/**
 */
package conditions.tests;

import conditions.AbstractMessage;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.AbstractMessage#getPrimaryFilter() <em>Get Primary Filter</em>}</li>
 *   <li>{@link conditions.AbstractMessage#getFilters(boolean) <em>Get Filters</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractMessageTest extends BaseClassWithSourceReferenceTest {

	/**
	 * Constructs a new Abstract Message test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractMessageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Message test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractMessage getFixture() {
		return (AbstractMessage)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractMessage#getPrimaryFilter() <em>Get Primary Filter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractMessage#getPrimaryFilter()
	 * @generated
	 */
	public void testGetPrimaryFilter() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.AbstractMessage#getFilters(boolean) <em>Get Filters</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractMessage#getFilters(boolean)
	 * @generated
	 */
	public void testGetFilters__boolean() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractMessageTest
