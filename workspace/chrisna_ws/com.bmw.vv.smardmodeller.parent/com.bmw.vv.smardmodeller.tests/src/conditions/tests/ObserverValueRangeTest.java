/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.ObserverValueRange;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Observer Value Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.ObserverValueRange#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}</li>
 *   <li>{@link conditions.ObserverValueRange#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ObserverValueRangeTest extends TestCase {

	/**
	 * The fixture for this Observer Value Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObserverValueRange fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ObserverValueRangeTest.class);
	}

	/**
	 * Constructs a new Observer Value Range test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObserverValueRangeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Observer Value Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ObserverValueRange fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Observer Value Range test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObserverValueRange getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createObserverValueRange());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.ObserverValueRange#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ObserverValueRange#isValid_hasValidValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidValue__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.ObserverValueRange#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.ObserverValueRange#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidDescription__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ObserverValueRangeTest
