/**
 */
package conditions.tests;

import conditions.ConditionsFactory;
import conditions.LinMessageCheckExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Lin Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.LinMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}</li>
 *   <li>{@link conditions.LinMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}</li>
 *   <li>{@link conditions.LinMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class LinMessageCheckExpressionTest extends ExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LinMessageCheckExpressionTest.class);
	}

	/**
	 * Constructs a new Lin Message Check Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinMessageCheckExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Lin Message Check Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LinMessageCheckExpression getFixture() {
		return (LinMessageCheckExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ConditionsFactory.eINSTANCE.createLinMessageCheckExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link conditions.LinMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Bus Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidBusId(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidBusId__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.LinMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Message Id Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidMessageIdRange(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidMessageIdRange__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.LinMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Check Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.LinMessageCheckExpression#isValid_hasValidCheckType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidCheckType__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //LinMessageCheckExpressionTest
