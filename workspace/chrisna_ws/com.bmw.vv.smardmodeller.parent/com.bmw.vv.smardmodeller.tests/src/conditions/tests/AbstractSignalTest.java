/**
 */
package conditions.tests;

import conditions.AbstractSignal;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link conditions.AbstractSignal#getMessage() <em>Get Message</em>}</li>
 *   <li>{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}</li>
 *   <li>{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class AbstractSignalTest extends BaseClassWithSourceReferenceTest {

	/**
	 * Constructs a new Abstract Signal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractSignalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Signal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractSignal getFixture() {
		return (AbstractSignal)fixture;
	}

	/**
	 * Tests the '{@link conditions.AbstractSignal#getMessage() <em>Get Message</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.AbstractSignal#getMessage()
	 * @generated
	 */
	public void testGetMessage() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperand#get_EvaluationDataType()
	 * @generated
	 */
	public void testGet_EvaluationDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getReadVariables() <em>Get Read Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getReadVariables()
	 * @generated
	 */
	public void testGetReadVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IVariableReaderWriter#getWriteVariables() <em>Get Write Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IVariableReaderWriter#getWriteVariables()
	 * @generated
	 */
	public void testGetWriteVariables() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //AbstractSignalTest
