/**
 */
package conditions.tests;

import conditions.DecodeStrategy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DecodeStrategyTest extends TestCase {

	/**
	 * The fixture for this Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecodeStrategy fixture = null;

	/**
	 * Constructs a new Decode Strategy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecodeStrategyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DecodeStrategy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Decode Strategy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecodeStrategy getFixture() {
		return fixture;
	}

} //DecodeStrategyTest
