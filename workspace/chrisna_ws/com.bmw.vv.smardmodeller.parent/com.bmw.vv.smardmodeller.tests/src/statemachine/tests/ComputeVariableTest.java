/**
 */
package statemachine.tests;

import junit.textui.TestRunner;

import statemachine.ComputeVariable;
import statemachine.StatemachineFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Compute Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link statemachine.ComputeVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}</li>
 *   <li>{@link statemachine.ComputeVariable#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Action Expression</em>}</li>
 *   <li>{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}</li>
 *   <li>{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}</li>
 *   <li>{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class ComputeVariableTest extends AbstractActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ComputeVariableTest.class);
	}

	/**
	 * Constructs a new Compute Variable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputeVariableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Compute Variable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ComputeVariable getFixture() {
		return (ComputeVariable)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatemachineFactory.eINSTANCE.createComputeVariable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link statemachine.ComputeVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.ComputeVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidOperands__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link statemachine.ComputeVariable#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Action Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.ComputeVariable#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testIsValid_hasValidActionExpression__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_OperandDataType() <em>Get Operand Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_OperandDataType()
	 * @generated
	 */
	public void testGet_OperandDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperation#get_Operands() <em>Get Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperation#get_Operands()
	 * @generated
	 */
	public void testGet_Operands() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link conditions.IOperand#get_EvaluationDataType() <em>Get Evaluation Data Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see conditions.IOperand#get_EvaluationDataType()
	 * @generated
	 */
	public void testGet_EvaluationDataType() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //ComputeVariableTest
