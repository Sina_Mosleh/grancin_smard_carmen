/**
 */
package statemachine;

import conditions.AbstractVariable;
import conditions.BaseClassWithID;
import conditions.Condition;
import conditions.IVariableReaderWriter;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.Transition#getName <em>Name</em>}</li>
 *   <li>{@link statemachine.Transition#getFrom <em>From</em>}</li>
 *   <li>{@link statemachine.Transition#getTo <em>To</em>}</li>
 *   <li>{@link statemachine.Transition#getConditions <em>Conditions</em>}</li>
 *   <li>{@link statemachine.Transition#getDescription <em>Description</em>}</li>
 *   <li>{@link statemachine.Transition#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link statemachine.Transition#getRootcause <em>Rootcause</em>}</li>
 *   <li>{@link statemachine.Transition#getActions <em>Actions</em>}</li>
 *   <li>{@link statemachine.Transition#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends BaseClassWithID, SmardTraceElement, IVariableReaderWriter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see statemachine.StatemachinePackage#getTransition_Name()
	 * @model extendedMetaData="kind='attribute'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link statemachine.AbstractState#getOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(AbstractState)
	 * @see statemachine.StatemachinePackage#getTransition_From()
	 * @see statemachine.AbstractState#getOut
	 * @model opposite="out" required="true"
	 *        extendedMetaData="kind='element' name='from'"
	 * @generated
	 */
	AbstractState getFrom();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(AbstractState value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link statemachine.AbstractState#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(AbstractState)
	 * @see statemachine.StatemachinePackage#getTransition_To()
	 * @see statemachine.AbstractState#getIn
	 * @model opposite="in" required="true"
	 *        extendedMetaData="kind='element' name='to'"
	 * @generated
	 */
	AbstractState getTo();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(AbstractState value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' reference list.
	 * The list contents are of type {@link conditions.Condition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' reference list.
	 * @see statemachine.StatemachinePackage#getTransition_Conditions()
	 * @model required="true"
	 *        extendedMetaData="kind='attribute' name='conditions'"
	 * @generated
	 */
	EList<Condition> getConditions();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see statemachine.StatemachinePackage#getTransition_Description()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Log Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Log Level</em>' attribute.
	 * @see #setLogLevel(String)
	 * @see statemachine.StatemachinePackage#getTransition_LogLevel()
	 * @model default="0" dataType="statemachine.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getLogLevel();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getLogLevel <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Log Level</em>' attribute.
	 * @see #getLogLevel()
	 * @generated
	 */
	void setLogLevel(String value);

	/**
	 * Returns the value of the '<em><b>Rootcause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rootcause</em>' attribute.
	 * @see #setRootcause(String)
	 * @see statemachine.StatemachinePackage#getTransition_Rootcause()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getRootcause();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getRootcause <em>Rootcause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rootcause</em>' attribute.
	 * @see #getRootcause()
	 * @generated
	 */
	void setRootcause(String value);

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link statemachine.AbstractAction}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see statemachine.StatemachinePackage#getTransition_Actions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Action'"
	 * @generated
	 */
	EList<AbstractAction> getActions();

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' reference.
	 * @see #setEnvironment(AbstractVariable)
	 * @see statemachine.StatemachinePackage#getTransition_Environment()
	 * @model
	 * @generated
	 */
	AbstractVariable getEnvironment();

	/**
	 * Sets the value of the '{@link statemachine.Transition#getEnvironment <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(AbstractVariable value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidRootCause(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidConditions(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Transition
