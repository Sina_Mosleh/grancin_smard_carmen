/**
 */
package statemachine;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see statemachine.StatemachinePackage#getInitialState()
 * @model
 * @generated
 */
public interface InitialState extends AbstractState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidNoInTransition(DiagnosticChain diagnostics, Map<Object, Object> context);

} // InitialState
