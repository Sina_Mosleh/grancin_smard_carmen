/**
 */
package statemachine;

import conditions.BaseClassWithID;
import conditions.IVariableReaderWriter;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.StateMachine#getName <em>Name</em>}</li>
 *   <li>{@link statemachine.StateMachine#getInitialState <em>Initial State</em>}</li>
 *   <li>{@link statemachine.StateMachine#getStates <em>States</em>}</li>
 *   <li>{@link statemachine.StateMachine#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link statemachine.StateMachine#isActiveAtStart <em>Active At Start</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getStateMachine()
 * @model extendedMetaData="kind='element' name='stateMachine'"
 * @generated
 */
public interface StateMachine extends BaseClassWithID, IVariableReaderWriter {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * The default value is <code>"StateMachine"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see statemachine.StatemachinePackage#getStateMachine_Name()
	 * @model default="StateMachine"
	 *        extendedMetaData="kind='attribute'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link statemachine.StateMachine#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial State</em>' containment reference.
	 * @see #setInitialState(InitialState)
	 * @see statemachine.StatemachinePackage#getStateMachine_InitialState()
	 * @model containment="true" required="true"
	 * @generated
	 */
	InitialState getInitialState();

	/**
	 * Sets the value of the '{@link statemachine.StateMachine#getInitialState <em>Initial State</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial State</em>' containment reference.
	 * @see #getInitialState()
	 * @generated
	 */
	void setInitialState(InitialState value);

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link statemachine.State}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see statemachine.StatemachinePackage#getStateMachine_States()
	 * @model containment="true"
	 *        extendedMetaData="kind='element'"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Transitions</b></em>' containment reference list.
	 * The list contents are of type {@link statemachine.Transition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transitions</em>' containment reference list.
	 * @see statemachine.StatemachinePackage#getStateMachine_Transitions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element'"
	 * @generated
	 */
	EList<Transition> getTransitions();

	/**
	 * Returns the value of the '<em><b>Active At Start</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active At Start</em>' attribute.
	 * @see #setActiveAtStart(boolean)
	 * @see statemachine.StatemachinePackage#getStateMachine_ActiveAtStart()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isActiveAtStart();

	/**
	 * Sets the value of the '{@link statemachine.StateMachine#isActiveAtStart <em>Active At Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active At Start</em>' attribute.
	 * @see #isActiveAtStart()
	 * @generated
	 */
	void setActiveAtStart(boolean value);

} // StateMachine
