/**
 */
package statemachine;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.State#getStateTypeTmplParam <em>State Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getState()
 * @model extendedMetaData="name='State'"
 * @generated
 */
public interface State extends AbstractState {
	/**
	 * Returns the value of the '<em><b>State Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Type Tmpl Param</em>' attribute.
	 * @see #setStateTypeTmplParam(String)
	 * @see statemachine.StatemachinePackage#getState_StateTypeTmplParam()
	 * @model
	 * @generated
	 */
	String getStateTypeTmplParam();

	/**
	 * Sets the value of the '{@link statemachine.State#getStateTypeTmplParam <em>State Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>State Type Tmpl Param</em>' attribute.
	 * @see #getStateTypeTmplParam()
	 * @generated
	 */
	void setStateTypeTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // State
