/**
 */
package statemachine;

import conditions.AbstractVariable;
import conditions.Variable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Show Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachine.ShowVariable#getFormat <em>Format</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getPrefix <em>Prefix</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getPostfix <em>Postfix</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getRootCause <em>Root Cause</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getVariable <em>Variable</em>}</li>
 *   <li>{@link statemachine.ShowVariable#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @see statemachine.StatemachinePackage#getShowVariable()
 * @model
 * @generated
 */
public interface ShowVariable extends AbstractAction, SmardTraceElement {
	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see statemachine.StatemachinePackage#getShowVariable_Format()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefix</em>' attribute.
	 * @see #setPrefix(String)
	 * @see statemachine.StatemachinePackage#getShowVariable_Prefix()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getPrefix();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getPrefix <em>Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Prefix</em>' attribute.
	 * @see #getPrefix()
	 * @generated
	 */
	void setPrefix(String value);

	/**
	 * Returns the value of the '<em><b>Postfix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Postfix</em>' attribute.
	 * @see #setPostfix(String)
	 * @see statemachine.StatemachinePackage#getShowVariable_Postfix()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getPostfix();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getPostfix <em>Postfix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Postfix</em>' attribute.
	 * @see #getPostfix()
	 * @generated
	 */
	void setPostfix(String value);

	/**
	 * Returns the value of the '<em><b>Root Cause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Cause</em>' attribute.
	 * @see #setRootCause(String)
	 * @see statemachine.StatemachinePackage#getShowVariable_RootCause()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getRootCause();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getRootCause <em>Root Cause</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Cause</em>' attribute.
	 * @see #getRootCause()
	 * @generated
	 */
	void setRootCause(String value);

	/**
	 * Returns the value of the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Log Level</em>' attribute.
	 * @see #isSetLogLevel()
	 * @see #unsetLogLevel()
	 * @see #setLogLevel(String)
	 * @see statemachine.StatemachinePackage#getShowVariable_LogLevel()
	 * @model unsettable="true" dataType="statemachine.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getLogLevel();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getLogLevel <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Log Level</em>' attribute.
	 * @see #isSetLogLevel()
	 * @see #unsetLogLevel()
	 * @see #getLogLevel()
	 * @generated
	 */
	void setLogLevel(String value);

	/**
	 * Unsets the value of the '{@link statemachine.ShowVariable#getLogLevel <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLogLevel()
	 * @see #getLogLevel()
	 * @see #setLogLevel(String)
	 * @generated
	 */
	void unsetLogLevel();

	/**
	 * Returns whether the value of the '{@link statemachine.ShowVariable#getLogLevel <em>Log Level</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Log Level</em>' attribute is set.
	 * @see #unsetLogLevel()
	 * @see #getLogLevel()
	 * @see #setLogLevel(String)
	 * @generated
	 */
	boolean isSetLogLevel();

	/**
	 * Returns the value of the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' reference.
	 * @see #setVariable(Variable)
	 * @see statemachine.StatemachinePackage#getShowVariable_Variable()
	 * @model required="true"
	 * @generated
	 */
	Variable getVariable();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getVariable <em>Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' reference.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(Variable value);

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' reference.
	 * @see #setEnvironment(AbstractVariable)
	 * @see statemachine.StatemachinePackage#getShowVariable_Environment()
	 * @model
	 * @generated
	 */
	AbstractVariable getEnvironment();

	/**
	 * Sets the value of the '{@link statemachine.ShowVariable#getEnvironment <em>Environment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(AbstractVariable value);

} // ShowVariable
