/**
 *
 * $Id$
 */
package statemachine.validation;

import org.eclipse.emf.common.util.EList;

import statemachine.AbstractAction;
import statemachine.StateType;
import statemachine.Transition;

/**
 * A sample validator interface for {@link statemachine.AbstractState}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface AbstractStateValidator {
	boolean validate();

	boolean validateName(String value);
	boolean validateOut(EList<Transition> value);
	boolean validateIn(EList<Transition> value);
	boolean validateDescription(String value);
	boolean validateActions(EList<AbstractAction> value);
	boolean validateStateType(StateType value);
}
