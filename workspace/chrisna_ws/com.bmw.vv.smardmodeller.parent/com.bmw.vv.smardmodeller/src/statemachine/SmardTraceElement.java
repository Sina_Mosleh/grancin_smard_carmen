/**
 */
package statemachine;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Smard Trace Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see statemachine.StatemachinePackage#getSmardTraceElement()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface SmardTraceElement extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getIdentifier();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getIdentifier(String projectName);

} // SmardTraceElement
