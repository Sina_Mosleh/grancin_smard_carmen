/**
 */
package statemachine;

import conditions.ConditionsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see statemachine.StatemachineFactory
 * @model kind="package"
 *        extendedMetaData="qualified='true'"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 * @generated
 */
public interface StatemachinePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statemachine";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/statemachine";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "statemachine";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachinePackage eINSTANCE = statemachine.impl.StatemachinePackageImpl.init();

	/**
	 * The meta object id for the '{@link statemachine.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.DocumentRootImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 0;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>State Machine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__STATE_MACHINE = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachine.impl.AbstractStateImpl <em>Abstract State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.AbstractStateImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getAbstractState()
	 * @generated
	 */
	int ABSTRACT_STATE = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__OUT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__IN = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__ACTIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>State Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE__STATE_TYPE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Abstract State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE___GET_READ_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE___GET_WRITE_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Abstract State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_STATE_OPERATION_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link statemachine.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.TransitionImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__FROM = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__TO = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CONDITIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__LOG_LEVEL = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Rootcause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ROOTCAUSE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ACTIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ENVIRONMENT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___GET_IDENTIFIER = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___GET_IDENTIFIER__STRING = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___GET_READ_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___GET_WRITE_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Is Valid has Valid Root Cause</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___IS_VALID_HAS_VALID_ROOT_CAUSE__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Is Valid has Valid Conditions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION___IS_VALID_HAS_VALID_CONDITIONS__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_OPERATION_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link statemachine.impl.StateMachineImpl <em>State Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.StateMachineImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getStateMachine()
	 * @generated
	 */
	int STATE_MACHINE = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__NAME = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__INITIAL_STATE = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__STATES = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__TRANSITIONS = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Active At Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE__ACTIVE_AT_START = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE___GET_READ_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE___GET_WRITE_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_OPERATION_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link statemachine.impl.AbstractActionImpl <em>Abstract Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.AbstractActionImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getAbstractAction()
	 * @generated
	 */
	int ABSTRACT_ACTION = 7;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION__ID = ConditionsPackage.BASE_CLASS_WITH_ID__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION__DESCRIPTION = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION__TRIGGER = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION__TRIGGER_TMPL_PARAM = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Abstract Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION_FEATURE_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION___GET_READ_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION___GET_WRITE_VARIABLES = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_ACTION_OPERATION_COUNT = ConditionsPackage.BASE_CLASS_WITH_ID_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link statemachine.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.ActionImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ID = ABSTRACT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TRIGGER = ABSTRACT_ACTION__TRIGGER;

	/**
	 * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Action Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_TYPE = ABSTRACT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_EXPRESSION = ABSTRACT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Action Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ACTION_TYPE_TMPL_PARAM = ABSTRACT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ENVIRONMENT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___GET_READ_VARIABLES = ABSTRACT_ACTION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___GET_WRITE_VARIABLES = ABSTRACT_ACTION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___GET_IDENTIFIER = ABSTRACT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___GET_IDENTIFIER__STRING = ABSTRACT_ACTION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is Valid has Valid Action Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Action Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION___IS_VALID_HAS_VALID_ACTION_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = ABSTRACT_ACTION_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link statemachine.impl.InitialStateImpl <em>Initial State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.InitialStateImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getInitialState()
	 * @generated
	 */
	int INITIAL_STATE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__ID = ABSTRACT_STATE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__NAME = ABSTRACT_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__OUT = ABSTRACT_STATE__OUT;

	/**
	 * The feature id for the '<em><b>In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__IN = ABSTRACT_STATE__IN;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__DESCRIPTION = ABSTRACT_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__ACTIONS = ABSTRACT_STATE__ACTIONS;

	/**
	 * The feature id for the '<em><b>State Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE__STATE_TYPE = ABSTRACT_STATE__STATE_TYPE;

	/**
	 * The number of structural features of the '<em>Initial State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE___GET_READ_VARIABLES = ABSTRACT_STATE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE___GET_WRITE_VARIABLES = ABSTRACT_STATE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid No In Transition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE___IS_VALID_HAS_VALID_NO_IN_TRANSITION__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Initial State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_STATE_OPERATION_COUNT = ABSTRACT_STATE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link statemachine.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.StateImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getState()
	 * @generated
	 */
	int STATE = 6;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ID = ABSTRACT_STATE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = ABSTRACT_STATE__NAME;

	/**
	 * The feature id for the '<em><b>Out</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__OUT = ABSTRACT_STATE__OUT;

	/**
	 * The feature id for the '<em><b>In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__IN = ABSTRACT_STATE__IN;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__DESCRIPTION = ABSTRACT_STATE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__ACTIONS = ABSTRACT_STATE__ACTIONS;

	/**
	 * The feature id for the '<em><b>State Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__STATE_TYPE = ABSTRACT_STATE__STATE_TYPE;

	/**
	 * The feature id for the '<em><b>State Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__STATE_TYPE_TMPL_PARAM = ABSTRACT_STATE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = ABSTRACT_STATE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___GET_READ_VARIABLES = ABSTRACT_STATE___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___GET_WRITE_VARIABLES = ABSTRACT_STATE___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Is Valid has Valid Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = ABSTRACT_STATE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = ABSTRACT_STATE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link statemachine.impl.ComputeVariableImpl <em>Compute Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.ComputeVariableImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getComputeVariable()
	 * @generated
	 */
	int COMPUTE_VARIABLE = 8;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__ID = ABSTRACT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__TRIGGER = ABSTRACT_ACTION__TRIGGER;

	/**
	 * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Action Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__ACTION_EXPRESSION = ABSTRACT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__TARGET = ABSTRACT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operands</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE__OPERANDS = ABSTRACT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Compute Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___GET_READ_VARIABLES = ABSTRACT_ACTION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___GET_WRITE_VARIABLES = ABSTRACT_ACTION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Evaluation Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___GET_EVALUATION_DATA_TYPE = ABSTRACT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Operand Data Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___GET_OPERAND_DATA_TYPE = ABSTRACT_ACTION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___GET_OPERANDS = ABSTRACT_ACTION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid has Valid Operands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Valid has Valid Action Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Compute Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTE_VARIABLE_OPERATION_COUNT = ABSTRACT_ACTION_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link statemachine.impl.ShowVariableImpl <em>Show Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.ShowVariableImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getShowVariable()
	 * @generated
	 */
	int SHOW_VARIABLE = 9;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__ID = ABSTRACT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__TRIGGER = ABSTRACT_ACTION__TRIGGER;

	/**
	 * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__FORMAT = ABSTRACT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__PREFIX = ABSTRACT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Postfix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__POSTFIX = ABSTRACT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Root Cause</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__ROOT_CAUSE = ABSTRACT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__LOG_LEVEL = ABSTRACT_ACTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__VARIABLE = ABSTRACT_ACTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE__ENVIRONMENT = ABSTRACT_ACTION_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Show Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE___GET_READ_VARIABLES = ABSTRACT_ACTION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE___GET_WRITE_VARIABLES = ABSTRACT_ACTION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE___GET_IDENTIFIER = ABSTRACT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE___GET_IDENTIFIER__STRING = ABSTRACT_ACTION_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Show Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_VARIABLE_OPERATION_COUNT = ABSTRACT_ACTION_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link statemachine.SmardTraceElement <em>Smard Trace Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.SmardTraceElement
	 * @see statemachine.impl.StatemachinePackageImpl#getSmardTraceElement()
	 * @generated
	 */
	int SMARD_TRACE_ELEMENT = 10;

	/**
	 * The number of structural features of the '<em>Smard Trace Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMARD_TRACE_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMARD_TRACE_ELEMENT___GET_IDENTIFIER = 0;

	/**
	 * The operation id for the '<em>Get Identifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMARD_TRACE_ELEMENT___GET_IDENTIFIER__STRING = 1;

	/**
	 * The number of operations of the '<em>Smard Trace Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SMARD_TRACE_ELEMENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link statemachine.impl.ControlActionImpl <em>Control Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.impl.ControlActionImpl
	 * @see statemachine.impl.StatemachinePackageImpl#getControlAction()
	 * @generated
	 */
	int CONTROL_ACTION = 11;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__ID = ABSTRACT_ACTION__ID;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__DESCRIPTION = ABSTRACT_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__TRIGGER = ABSTRACT_ACTION__TRIGGER;

	/**
	 * The feature id for the '<em><b>Trigger Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__TRIGGER_TMPL_PARAM = ABSTRACT_ACTION__TRIGGER_TMPL_PARAM;

	/**
	 * The feature id for the '<em><b>Control Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__CONTROL_TYPE = ABSTRACT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__STATE_MACHINES = ABSTRACT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Observers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION__OBSERVERS = ABSTRACT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Control Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION_FEATURE_COUNT = ABSTRACT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Read Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION___GET_READ_VARIABLES = ABSTRACT_ACTION___GET_READ_VARIABLES;

	/**
	 * The operation id for the '<em>Get Write Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION___GET_WRITE_VARIABLES = ABSTRACT_ACTION___GET_WRITE_VARIABLES;

	/**
	 * The operation id for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP;

	/**
	 * The number of operations of the '<em>Control Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_ACTION_OPERATION_COUNT = ABSTRACT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link statemachine.Trigger <em>Trigger</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.Trigger
	 * @see statemachine.impl.StatemachinePackageImpl#getTrigger()
	 * @generated
	 */
	int TRIGGER = 12;

	/**
	 * The meta object id for the '{@link statemachine.StateType <em>State Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.StateType
	 * @see statemachine.impl.StatemachinePackageImpl#getStateType()
	 * @generated
	 */
	int STATE_TYPE = 13;

	/**
	 * The meta object id for the '{@link statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.ActionTypeNotEmpty
	 * @see statemachine.impl.StatemachinePackageImpl#getActionTypeNotEmpty()
	 * @generated
	 */
	int ACTION_TYPE_NOT_EMPTY = 14;

	/**
	 * The meta object id for the '{@link statemachine.ControlType <em>Control Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachine.ControlType
	 * @see statemachine.impl.StatemachinePackageImpl#getControlType()
	 * @generated
	 */
	int CONTROL_TYPE = 15;

	/**
	 * The meta object id for the '<em>Int Or Template Placeholder</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see statemachine.impl.StatemachinePackageImpl#getIntOrTemplatePlaceholder()
	 * @generated
	 */
	int INT_OR_TEMPLATE_PLACEHOLDER = 16;


	/**
	 * Returns the meta object for class '{@link statemachine.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see statemachine.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link statemachine.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see statemachine.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link statemachine.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see statemachine.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link statemachine.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see statemachine.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link statemachine.DocumentRoot#getStateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>State Machine</em>'.
	 * @see statemachine.DocumentRoot#getStateMachine()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_StateMachine();

	/**
	 * Returns the meta object for class '{@link statemachine.AbstractState <em>Abstract State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract State</em>'.
	 * @see statemachine.AbstractState
	 * @generated
	 */
	EClass getAbstractState();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractState#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see statemachine.AbstractState#getName()
	 * @see #getAbstractState()
	 * @generated
	 */
	EAttribute getAbstractState_Name();

	/**
	 * Returns the meta object for the reference list '{@link statemachine.AbstractState#getOut <em>Out</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Out</em>'.
	 * @see statemachine.AbstractState#getOut()
	 * @see #getAbstractState()
	 * @generated
	 */
	EReference getAbstractState_Out();

	/**
	 * Returns the meta object for the reference list '{@link statemachine.AbstractState#getIn <em>In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>In</em>'.
	 * @see statemachine.AbstractState#getIn()
	 * @see #getAbstractState()
	 * @generated
	 */
	EReference getAbstractState_In();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractState#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see statemachine.AbstractState#getDescription()
	 * @see #getAbstractState()
	 * @generated
	 */
	EAttribute getAbstractState_Description();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.AbstractState#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see statemachine.AbstractState#getActions()
	 * @see #getAbstractState()
	 * @generated
	 */
	EReference getAbstractState_Actions();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractState#getStateType <em>State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State Type</em>'.
	 * @see statemachine.AbstractState#getStateType()
	 * @see #getAbstractState()
	 * @generated
	 */
	EAttribute getAbstractState_StateType();

	/**
	 * Returns the meta object for the '{@link statemachine.AbstractState#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see statemachine.AbstractState#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractState__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.AbstractState#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Name</em>' operation.
	 * @see statemachine.AbstractState#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractState__IsValid_hasValidName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see statemachine.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Transition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see statemachine.Transition#getName()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Name();

	/**
	 * Returns the meta object for the reference '{@link statemachine.Transition#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see statemachine.Transition#getFrom()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_From();

	/**
	 * Returns the meta object for the reference '{@link statemachine.Transition#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see statemachine.Transition#getTo()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_To();

	/**
	 * Returns the meta object for the reference list '{@link statemachine.Transition#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Conditions</em>'.
	 * @see statemachine.Transition#getConditions()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Conditions();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Transition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see statemachine.Transition#getDescription()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Description();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Transition#getLogLevel <em>Log Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log Level</em>'.
	 * @see statemachine.Transition#getLogLevel()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_LogLevel();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Transition#getRootcause <em>Rootcause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rootcause</em>'.
	 * @see statemachine.Transition#getRootcause()
	 * @see #getTransition()
	 * @generated
	 */
	EAttribute getTransition_Rootcause();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.Transition#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see statemachine.Transition#getActions()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Actions();

	/**
	 * Returns the meta object for the reference '{@link statemachine.Transition#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment</em>'.
	 * @see statemachine.Transition#getEnvironment()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_Environment();

	/**
	 * Returns the meta object for the '{@link statemachine.Transition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see statemachine.Transition#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransition__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.Transition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Name</em>' operation.
	 * @see statemachine.Transition#isValid_hasValidName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransition__IsValid_hasValidName__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.Transition#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * @see statemachine.Transition#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransition__IsValid_hasValidLogLevel__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.Transition#isValid_hasValidRootCause(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Root Cause</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Root Cause</em>' operation.
	 * @see statemachine.Transition#isValid_hasValidRootCause(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransition__IsValid_hasValidRootCause__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.Transition#isValid_hasValidConditions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Conditions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Conditions</em>' operation.
	 * @see statemachine.Transition#isValid_hasValidConditions(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getTransition__IsValid_hasValidConditions__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.StateMachine <em>State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine</em>'.
	 * @see statemachine.StateMachine
	 * @generated
	 */
	EClass getStateMachine();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.StateMachine#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see statemachine.StateMachine#getName()
	 * @see #getStateMachine()
	 * @generated
	 */
	EAttribute getStateMachine_Name();

	/**
	 * Returns the meta object for the containment reference '{@link statemachine.StateMachine#getInitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initial State</em>'.
	 * @see statemachine.StateMachine#getInitialState()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_InitialState();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.StateMachine#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see statemachine.StateMachine#getStates()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_States();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.StateMachine#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see statemachine.StateMachine#getTransitions()
	 * @see #getStateMachine()
	 * @generated
	 */
	EReference getStateMachine_Transitions();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.StateMachine#isActiveAtStart <em>Active At Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active At Start</em>'.
	 * @see statemachine.StateMachine#isActiveAtStart()
	 * @see #getStateMachine()
	 * @generated
	 */
	EAttribute getStateMachine_ActiveAtStart();

	/**
	 * Returns the meta object for class '{@link statemachine.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see statemachine.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Action#getActionType <em>Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Type</em>'.
	 * @see statemachine.Action#getActionType()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_ActionType();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Action#getActionExpression <em>Action Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Expression</em>'.
	 * @see statemachine.Action#getActionExpression()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_ActionExpression();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.Action#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Type Tmpl Param</em>'.
	 * @see statemachine.Action#getActionTypeTmplParam()
	 * @see #getAction()
	 * @generated
	 */
	EAttribute getAction_ActionTypeTmplParam();

	/**
	 * Returns the meta object for the reference '{@link statemachine.Action#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment</em>'.
	 * @see statemachine.Action#getEnvironment()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Environment();

	/**
	 * Returns the meta object for the '{@link statemachine.Action#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Action Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Action Expression</em>' operation.
	 * @see statemachine.Action#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAction__IsValid_hasValidActionExpression__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.Action#isValid_hasValidActionType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Action Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Action Type</em>' operation.
	 * @see statemachine.Action#isValid_hasValidActionType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAction__IsValid_hasValidActionType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.InitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial State</em>'.
	 * @see statemachine.InitialState
	 * @generated
	 */
	EClass getInitialState();

	/**
	 * Returns the meta object for the '{@link statemachine.InitialState#isValid_hasValidNoInTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid No In Transition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid No In Transition</em>' operation.
	 * @see statemachine.InitialState#isValid_hasValidNoInTransition(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getInitialState__IsValid_hasValidNoInTransition__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see statemachine.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.State#getStateTypeTmplParam <em>State Type Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State Type Tmpl Param</em>'.
	 * @see statemachine.State#getStateTypeTmplParam()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_StateTypeTmplParam();

	/**
	 * Returns the meta object for the '{@link statemachine.State#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Type</em>' operation.
	 * @see statemachine.State#isValid_hasValidType(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getState__IsValid_hasValidType__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.AbstractAction <em>Abstract Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Action</em>'.
	 * @see statemachine.AbstractAction
	 * @generated
	 */
	EClass getAbstractAction();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractAction#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see statemachine.AbstractAction#getDescription()
	 * @see #getAbstractAction()
	 * @generated
	 */
	EAttribute getAbstractAction_Description();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractAction#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see statemachine.AbstractAction#getTrigger()
	 * @see #getAbstractAction()
	 * @generated
	 */
	EAttribute getAbstractAction_Trigger();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.AbstractAction#getTriggerTmplParam <em>Trigger Tmpl Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger Tmpl Param</em>'.
	 * @see statemachine.AbstractAction#getTriggerTmplParam()
	 * @see #getAbstractAction()
	 * @generated
	 */
	EAttribute getAbstractAction_TriggerTmplParam();

	/**
	 * Returns the meta object for the '{@link statemachine.AbstractAction#isValid_hasValidTrigger(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Trigger</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Trigger</em>' operation.
	 * @see statemachine.AbstractAction#isValid_hasValidTrigger(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getAbstractAction__IsValid_hasValidTrigger__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.ComputeVariable <em>Compute Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compute Variable</em>'.
	 * @see statemachine.ComputeVariable
	 * @generated
	 */
	EClass getComputeVariable();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ComputeVariable#getActionExpression <em>Action Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action Expression</em>'.
	 * @see statemachine.ComputeVariable#getActionExpression()
	 * @see #getComputeVariable()
	 * @generated
	 */
	EAttribute getComputeVariable_ActionExpression();

	/**
	 * Returns the meta object for the reference '{@link statemachine.ComputeVariable#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see statemachine.ComputeVariable#getTarget()
	 * @see #getComputeVariable()
	 * @generated
	 */
	EReference getComputeVariable_Target();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachine.ComputeVariable#getOperands <em>Operands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operands</em>'.
	 * @see statemachine.ComputeVariable#getOperands()
	 * @see #getComputeVariable()
	 * @generated
	 */
	EReference getComputeVariable_Operands();

	/**
	 * Returns the meta object for the '{@link statemachine.ComputeVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Operands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Operands</em>' operation.
	 * @see statemachine.ComputeVariable#isValid_hasValidOperands(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getComputeVariable__IsValid_hasValidOperands__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachine.ComputeVariable#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Action Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Action Expression</em>' operation.
	 * @see statemachine.ComputeVariable#isValid_hasValidActionExpression(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getComputeVariable__IsValid_hasValidActionExpression__DiagnosticChain_Map();

	/**
	 * Returns the meta object for class '{@link statemachine.ShowVariable <em>Show Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Show Variable</em>'.
	 * @see statemachine.ShowVariable
	 * @generated
	 */
	EClass getShowVariable();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ShowVariable#getFormat <em>Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Format</em>'.
	 * @see statemachine.ShowVariable#getFormat()
	 * @see #getShowVariable()
	 * @generated
	 */
	EAttribute getShowVariable_Format();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ShowVariable#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see statemachine.ShowVariable#getPrefix()
	 * @see #getShowVariable()
	 * @generated
	 */
	EAttribute getShowVariable_Prefix();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ShowVariable#getPostfix <em>Postfix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postfix</em>'.
	 * @see statemachine.ShowVariable#getPostfix()
	 * @see #getShowVariable()
	 * @generated
	 */
	EAttribute getShowVariable_Postfix();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ShowVariable#getRootCause <em>Root Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Root Cause</em>'.
	 * @see statemachine.ShowVariable#getRootCause()
	 * @see #getShowVariable()
	 * @generated
	 */
	EAttribute getShowVariable_RootCause();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ShowVariable#getLogLevel <em>Log Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log Level</em>'.
	 * @see statemachine.ShowVariable#getLogLevel()
	 * @see #getShowVariable()
	 * @generated
	 */
	EAttribute getShowVariable_LogLevel();

	/**
	 * Returns the meta object for the reference '{@link statemachine.ShowVariable#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variable</em>'.
	 * @see statemachine.ShowVariable#getVariable()
	 * @see #getShowVariable()
	 * @generated
	 */
	EReference getShowVariable_Variable();

	/**
	 * Returns the meta object for the reference '{@link statemachine.ShowVariable#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment</em>'.
	 * @see statemachine.ShowVariable#getEnvironment()
	 * @see #getShowVariable()
	 * @generated
	 */
	EReference getShowVariable_Environment();

	/**
	 * Returns the meta object for class '{@link statemachine.SmardTraceElement <em>Smard Trace Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Smard Trace Element</em>'.
	 * @see statemachine.SmardTraceElement
	 * @generated
	 */
	EClass getSmardTraceElement();

	/**
	 * Returns the meta object for the '{@link statemachine.SmardTraceElement#getIdentifier() <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Identifier</em>' operation.
	 * @see statemachine.SmardTraceElement#getIdentifier()
	 * @generated
	 */
	EOperation getSmardTraceElement__GetIdentifier();

	/**
	 * Returns the meta object for the '{@link statemachine.SmardTraceElement#getIdentifier(java.lang.String) <em>Get Identifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Identifier</em>' operation.
	 * @see statemachine.SmardTraceElement#getIdentifier(java.lang.String)
	 * @generated
	 */
	EOperation getSmardTraceElement__GetIdentifier__String();

	/**
	 * Returns the meta object for class '{@link statemachine.ControlAction <em>Control Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Action</em>'.
	 * @see statemachine.ControlAction
	 * @generated
	 */
	EClass getControlAction();

	/**
	 * Returns the meta object for the attribute '{@link statemachine.ControlAction#getControlType <em>Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Control Type</em>'.
	 * @see statemachine.ControlAction#getControlType()
	 * @see #getControlAction()
	 * @generated
	 */
	EAttribute getControlAction_ControlType();

	/**
	 * Returns the meta object for the reference list '{@link statemachine.ControlAction#getStateMachines <em>State Machines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>State Machines</em>'.
	 * @see statemachine.ControlAction#getStateMachines()
	 * @see #getControlAction()
	 * @generated
	 */
	EReference getControlAction_StateMachines();

	/**
	 * Returns the meta object for the reference list '{@link statemachine.ControlAction#getObservers <em>Observers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Observers</em>'.
	 * @see statemachine.ControlAction#getObservers()
	 * @see #getControlAction()
	 * @generated
	 */
	EReference getControlAction_Observers();

	/**
	 * Returns the meta object for enum '{@link statemachine.Trigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Trigger</em>'.
	 * @see statemachine.Trigger
	 * @generated
	 */
	EEnum getTrigger();

	/**
	 * Returns the meta object for enum '{@link statemachine.StateType <em>State Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State Type</em>'.
	 * @see statemachine.StateType
	 * @generated
	 */
	EEnum getStateType();

	/**
	 * Returns the meta object for enum '{@link statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Action Type Not Empty</em>'.
	 * @see statemachine.ActionTypeNotEmpty
	 * @generated
	 */
	EEnum getActionTypeNotEmpty();

	/**
	 * Returns the meta object for enum '{@link statemachine.ControlType <em>Control Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Control Type</em>'.
	 * @see statemachine.ControlType
	 * @generated
	 */
	EEnum getControlType();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Int Or Template Placeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Int Or Template Placeholder</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getIntOrTemplatePlaceholder();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatemachineFactory getStatemachineFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link statemachine.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.DocumentRootImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>State Machine</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__STATE_MACHINE = eINSTANCE.getDocumentRoot_StateMachine();

		/**
		 * The meta object literal for the '{@link statemachine.impl.AbstractStateImpl <em>Abstract State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.AbstractStateImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getAbstractState()
		 * @generated
		 */
		EClass ABSTRACT_STATE = eINSTANCE.getAbstractState();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STATE__NAME = eINSTANCE.getAbstractState_Name();

		/**
		 * The meta object literal for the '<em><b>Out</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STATE__OUT = eINSTANCE.getAbstractState_Out();

		/**
		 * The meta object literal for the '<em><b>In</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STATE__IN = eINSTANCE.getAbstractState_In();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STATE__DESCRIPTION = eINSTANCE.getAbstractState_Description();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ABSTRACT_STATE__ACTIONS = eINSTANCE.getAbstractState_Actions();

		/**
		 * The meta object literal for the '<em><b>State Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_STATE__STATE_TYPE = eINSTANCE.getAbstractState_StateType();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getAbstractState__IsValid_hasValidDescription__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = eINSTANCE.getAbstractState__IsValid_hasValidName__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.TransitionImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__NAME = eINSTANCE.getTransition_Name();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__FROM = eINSTANCE.getTransition_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__TO = eINSTANCE.getTransition_To();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__CONDITIONS = eINSTANCE.getTransition_Conditions();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__DESCRIPTION = eINSTANCE.getTransition_Description();

		/**
		 * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__LOG_LEVEL = eINSTANCE.getTransition_LogLevel();

		/**
		 * The meta object literal for the '<em><b>Rootcause</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSITION__ROOTCAUSE = eINSTANCE.getTransition_Rootcause();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ACTIONS = eINSTANCE.getTransition_Actions();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ENVIRONMENT = eINSTANCE.getTransition_Environment();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSITION___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTransition__IsValid_hasValidDescription__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSITION___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTransition__IsValid_hasValidName__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Log Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSITION___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTransition__IsValid_hasValidLogLevel__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Root Cause</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSITION___IS_VALID_HAS_VALID_ROOT_CAUSE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTransition__IsValid_hasValidRootCause__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Conditions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSITION___IS_VALID_HAS_VALID_CONDITIONS__DIAGNOSTICCHAIN_MAP = eINSTANCE.getTransition__IsValid_hasValidConditions__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.StateMachineImpl <em>State Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.StateMachineImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getStateMachine()
		 * @generated
		 */
		EClass STATE_MACHINE = eINSTANCE.getStateMachine();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_MACHINE__NAME = eINSTANCE.getStateMachine_Name();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__INITIAL_STATE = eINSTANCE.getStateMachine_InitialState();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__STATES = eINSTANCE.getStateMachine_States();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE__TRANSITIONS = eINSTANCE.getStateMachine_Transitions();

		/**
		 * The meta object literal for the '<em><b>Active At Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_MACHINE__ACTIVE_AT_START = eINSTANCE.getStateMachine_ActiveAtStart();

		/**
		 * The meta object literal for the '{@link statemachine.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.ActionImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Action Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__ACTION_TYPE = eINSTANCE.getAction_ActionType();

		/**
		 * The meta object literal for the '<em><b>Action Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__ACTION_EXPRESSION = eINSTANCE.getAction_ActionExpression();

		/**
		 * The meta object literal for the '<em><b>Action Type Tmpl Param</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION__ACTION_TYPE_TMPL_PARAM = eINSTANCE.getAction_ActionTypeTmplParam();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__ENVIRONMENT = eINSTANCE.getAction_Environment();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Action Expression</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTION___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getAction__IsValid_hasValidActionExpression__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Action Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ACTION___IS_VALID_HAS_VALID_ACTION_TYPE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getAction__IsValid_hasValidActionType__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.InitialStateImpl <em>Initial State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.InitialStateImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getInitialState()
		 * @generated
		 */
		EClass INITIAL_STATE = eINSTANCE.getInitialState();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid No In Transition</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation INITIAL_STATE___IS_VALID_HAS_VALID_NO_IN_TRANSITION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getInitialState__IsValid_hasValidNoInTransition__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.StateImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>State Type Tmpl Param</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__STATE_TYPE_TMPL_PARAM = eINSTANCE.getState_StateTypeTmplParam();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE___IS_VALID_HAS_VALID_TYPE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getState__IsValid_hasValidType__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.AbstractActionImpl <em>Abstract Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.AbstractActionImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getAbstractAction()
		 * @generated
		 */
		EClass ABSTRACT_ACTION = eINSTANCE.getAbstractAction();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ACTION__DESCRIPTION = eINSTANCE.getAbstractAction_Description();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ACTION__TRIGGER = eINSTANCE.getAbstractAction_Trigger();

		/**
		 * The meta object literal for the '<em><b>Trigger Tmpl Param</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_ACTION__TRIGGER_TMPL_PARAM = eINSTANCE.getAbstractAction_TriggerTmplParam();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Trigger</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP = eINSTANCE.getAbstractAction__IsValid_hasValidTrigger__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.ComputeVariableImpl <em>Compute Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.ComputeVariableImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getComputeVariable()
		 * @generated
		 */
		EClass COMPUTE_VARIABLE = eINSTANCE.getComputeVariable();

		/**
		 * The meta object literal for the '<em><b>Action Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTE_VARIABLE__ACTION_EXPRESSION = eINSTANCE.getComputeVariable_ActionExpression();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTE_VARIABLE__TARGET = eINSTANCE.getComputeVariable_Target();

		/**
		 * The meta object literal for the '<em><b>Operands</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTE_VARIABLE__OPERANDS = eINSTANCE.getComputeVariable_Operands();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Operands</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPUTE_VARIABLE___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP = eINSTANCE.getComputeVariable__IsValid_hasValidOperands__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Action Expression</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPUTE_VARIABLE___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getComputeVariable__IsValid_hasValidActionExpression__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '{@link statemachine.impl.ShowVariableImpl <em>Show Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.ShowVariableImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getShowVariable()
		 * @generated
		 */
		EClass SHOW_VARIABLE = eINSTANCE.getShowVariable();

		/**
		 * The meta object literal for the '<em><b>Format</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_VARIABLE__FORMAT = eINSTANCE.getShowVariable_Format();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_VARIABLE__PREFIX = eINSTANCE.getShowVariable_Prefix();

		/**
		 * The meta object literal for the '<em><b>Postfix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_VARIABLE__POSTFIX = eINSTANCE.getShowVariable_Postfix();

		/**
		 * The meta object literal for the '<em><b>Root Cause</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_VARIABLE__ROOT_CAUSE = eINSTANCE.getShowVariable_RootCause();

		/**
		 * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHOW_VARIABLE__LOG_LEVEL = eINSTANCE.getShowVariable_LogLevel();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHOW_VARIABLE__VARIABLE = eINSTANCE.getShowVariable_Variable();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHOW_VARIABLE__ENVIRONMENT = eINSTANCE.getShowVariable_Environment();

		/**
		 * The meta object literal for the '{@link statemachine.SmardTraceElement <em>Smard Trace Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.SmardTraceElement
		 * @see statemachine.impl.StatemachinePackageImpl#getSmardTraceElement()
		 * @generated
		 */
		EClass SMARD_TRACE_ELEMENT = eINSTANCE.getSmardTraceElement();

		/**
		 * The meta object literal for the '<em><b>Get Identifier</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SMARD_TRACE_ELEMENT___GET_IDENTIFIER = eINSTANCE.getSmardTraceElement__GetIdentifier();

		/**
		 * The meta object literal for the '<em><b>Get Identifier</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SMARD_TRACE_ELEMENT___GET_IDENTIFIER__STRING = eINSTANCE.getSmardTraceElement__GetIdentifier__String();

		/**
		 * The meta object literal for the '{@link statemachine.impl.ControlActionImpl <em>Control Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.impl.ControlActionImpl
		 * @see statemachine.impl.StatemachinePackageImpl#getControlAction()
		 * @generated
		 */
		EClass CONTROL_ACTION = eINSTANCE.getControlAction();

		/**
		 * The meta object literal for the '<em><b>Control Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_ACTION__CONTROL_TYPE = eINSTANCE.getControlAction_ControlType();

		/**
		 * The meta object literal for the '<em><b>State Machines</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_ACTION__STATE_MACHINES = eINSTANCE.getControlAction_StateMachines();

		/**
		 * The meta object literal for the '<em><b>Observers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_ACTION__OBSERVERS = eINSTANCE.getControlAction_Observers();

		/**
		 * The meta object literal for the '{@link statemachine.Trigger <em>Trigger</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.Trigger
		 * @see statemachine.impl.StatemachinePackageImpl#getTrigger()
		 * @generated
		 */
		EEnum TRIGGER = eINSTANCE.getTrigger();

		/**
		 * The meta object literal for the '{@link statemachine.StateType <em>State Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.StateType
		 * @see statemachine.impl.StatemachinePackageImpl#getStateType()
		 * @generated
		 */
		EEnum STATE_TYPE = eINSTANCE.getStateType();

		/**
		 * The meta object literal for the '{@link statemachine.ActionTypeNotEmpty <em>Action Type Not Empty</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.ActionTypeNotEmpty
		 * @see statemachine.impl.StatemachinePackageImpl#getActionTypeNotEmpty()
		 * @generated
		 */
		EEnum ACTION_TYPE_NOT_EMPTY = eINSTANCE.getActionTypeNotEmpty();

		/**
		 * The meta object literal for the '{@link statemachine.ControlType <em>Control Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachine.ControlType
		 * @see statemachine.impl.StatemachinePackageImpl#getControlType()
		 * @generated
		 */
		EEnum CONTROL_TYPE = eINSTANCE.getControlType();

		/**
		 * The meta object literal for the '<em>Int Or Template Placeholder</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see statemachine.impl.StatemachinePackageImpl#getIntOrTemplatePlaceholder()
		 * @generated
		 */
		EDataType INT_OR_TEMPLATE_PLACEHOLDER = eINSTANCE.getIntOrTemplatePlaceholder();

	}

} //StatemachinePackage
