/**
 */
package statemachine.impl;

import conditions.ConditionsPackage;
import conditions.IVariableReaderWriter;

import conditions.impl.BaseClassWithIDImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.AbstractAction;
import statemachine.StatemachinePackage;
import statemachine.Trigger;

import statemachine.util.StatemachineValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.impl.AbstractActionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link statemachine.impl.AbstractActionImpl#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link statemachine.impl.AbstractActionImpl#getTriggerTmplParam <em>Trigger Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractActionImpl extends BaseClassWithIDImpl implements AbstractAction {
	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected static final Trigger TRIGGER_EDEFAULT = Trigger.ON_ENTRY;

	/**
	 * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTrigger()
	 * @generated
	 * @ordered
	 */
	protected Trigger trigger = TRIGGER_EDEFAULT;

	/**
	 * The default value of the '{@link #getTriggerTmplParam() <em>Trigger Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String TRIGGER_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTriggerTmplParam() <em>Trigger Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggerTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String triggerTmplParam = TRIGGER_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.ABSTRACT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Trigger getTrigger() {
		return trigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTrigger(Trigger newTrigger) {
		Trigger oldTrigger = trigger;
		trigger = newTrigger == null ? TRIGGER_EDEFAULT : newTrigger;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__TRIGGER, oldTrigger, trigger));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTriggerTmplParam() {
		return triggerTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTriggerTmplParam(String newTriggerTmplParam) {
		String oldTriggerTmplParam = triggerTmplParam;
		triggerTmplParam = newTriggerTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM, oldTriggerTmplParam, triggerTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidTrigger(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.ABSTRACT_ACTION__IS_VALID_HAS_VALID_TRIGGER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidTrigger", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
				return getDescription();
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
				return getTrigger();
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
				return getTriggerTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
				setTrigger((Trigger)newValue);
				return;
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
				setTriggerTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
				setTrigger(TRIGGER_EDEFAULT);
				return;
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
				setTriggerTmplParam(TRIGGER_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
				return trigger != TRIGGER_EDEFAULT;
			case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
				return TRIGGER_TMPL_PARAM_EDEFAULT == null ? triggerTmplParam != null : !TRIGGER_TMPL_PARAM_EDEFAULT.equals(triggerTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IVariableReaderWriter.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_READ_VARIABLES: return StatemachinePackage.ABSTRACT_ACTION___GET_READ_VARIABLES;
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_WRITE_VARIABLES: return StatemachinePackage.ABSTRACT_ACTION___GET_WRITE_VARIABLES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinePackage.ABSTRACT_ACTION___IS_VALID_HAS_VALID_TRIGGER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidTrigger((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.ABSTRACT_ACTION___GET_READ_VARIABLES:
				return getReadVariables();
			case StatemachinePackage.ABSTRACT_ACTION___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (description: ");
		result.append(description);
		result.append(", trigger: ");
		result.append(trigger);
		result.append(", triggerTmplParam: ");
		result.append(triggerTmplParam);
		result.append(')');
		return result.toString();
	}

} //AbstractActionImpl
