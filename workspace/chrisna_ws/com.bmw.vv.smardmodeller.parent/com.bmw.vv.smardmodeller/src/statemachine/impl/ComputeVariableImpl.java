/**
 */
package statemachine.impl;

import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.IComputeVariableActionOperand;
import conditions.IOperand;
import conditions.IOperation;
import conditions.ISignalComparisonExpressionOperand;
import conditions.ValueVariable;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachine.ComputeVariable;
import statemachine.StatemachinePackage;

import statemachine.util.StatemachineValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compute Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.impl.ComputeVariableImpl#getActionExpression <em>Action Expression</em>}</li>
 *   <li>{@link statemachine.impl.ComputeVariableImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link statemachine.impl.ComputeVariableImpl#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputeVariableImpl extends AbstractActionImpl implements ComputeVariable {
	/**
	 * The default value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTION_EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActionExpression()
	 * @generated
	 * @ordered
	 */
	protected String actionExpression = ACTION_EXPRESSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected ValueVariable target;

	/**
	 * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperands()
	 * @generated
	 * @ordered
	 */
	protected EList<IComputeVariableActionOperand> operands;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComputeVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.COMPUTE_VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getActionExpression() {
		return actionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActionExpression(String newActionExpression) {
		String oldActionExpression = actionExpression;
		actionExpression = newActionExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION, oldActionExpression, actionExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ValueVariable getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (ValueVariable)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.COMPUTE_VARIABLE__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueVariable basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTarget(ValueVariable newTarget) {
		ValueVariable oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.COMPUTE_VARIABLE__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IComputeVariableActionOperand> getOperands() {
		if (operands == null) {
			operands = new EObjectContainmentEList<IComputeVariableActionOperand>(IComputeVariableActionOperand.class, this, StatemachinePackage.COMPUTE_VARIABLE__OPERANDS);
		}
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.COMPUTE_VARIABLE__IS_VALID_HAS_VALID_OPERANDS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOperands", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.COMPUTE_VARIABLE__IS_VALID_HAS_VALID_ACTION_EXPRESSION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidActionExpression", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
				return ((InternalEList<?>)getOperands()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
				return getActionExpression();
			case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
				return getOperands();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
				setActionExpression((String)newValue);
				return;
			case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
				setTarget((ValueVariable)newValue);
				return;
			case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
				getOperands().clear();
				getOperands().addAll((Collection<? extends IComputeVariableActionOperand>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
				setActionExpression(ACTION_EXPRESSION_EDEFAULT);
				return;
			case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
				setTarget((ValueVariable)null);
				return;
			case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
				getOperands().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
				return ACTION_EXPRESSION_EDEFAULT == null ? actionExpression != null : !ACTION_EXPRESSION_EDEFAULT.equals(actionExpression);
			case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
				return target != null;
			case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
				return operands != null && !operands.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IOperand.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERAND___GET_EVALUATION_DATA_TYPE: return StatemachinePackage.COMPUTE_VARIABLE___GET_EVALUATION_DATA_TYPE;
				default: return -1;
			}
		}
		if (baseClass == ISignalComparisonExpressionOperand.class) {
			switch (baseOperationID) {
				default: return -1;
			}
		}
		if (baseClass == IOperation.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IOPERATION___GET_OPERAND_DATA_TYPE: return StatemachinePackage.COMPUTE_VARIABLE___GET_OPERAND_DATA_TYPE;
				case ConditionsPackage.IOPERATION___GET_OPERANDS: return StatemachinePackage.COMPUTE_VARIABLE___GET_OPERANDS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinePackage.COMPUTE_VARIABLE___IS_VALID_HAS_VALID_OPERANDS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOperands((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.COMPUTE_VARIABLE___IS_VALID_HAS_VALID_ACTION_EXPRESSION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidActionExpression((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.COMPUTE_VARIABLE___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case StatemachinePackage.COMPUTE_VARIABLE___GET_OPERANDS:
				return get_Operands();
			case StatemachinePackage.COMPUTE_VARIABLE___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (actionExpression: ");
		result.append(actionExpression);
		result.append(')');
		return result.toString();
	}

} //ComputeVariableImpl
