/**
 */
package statemachine.impl;

import conditions.ConditionsPackage;
import conditions.IVariableReaderWriter;

import conditions.impl.BaseClassWithIDImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EObjectWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachine.AbstractAction;
import statemachine.AbstractState;
import statemachine.StateType;
import statemachine.StatemachinePackage;
import statemachine.Transition;

import statemachine.util.StatemachineValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getName <em>Name</em>}</li>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getOut <em>Out</em>}</li>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getIn <em>In</em>}</li>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link statemachine.impl.AbstractStateImpl#getStateType <em>State Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractStateImpl extends BaseClassWithIDImpl implements AbstractState {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOut() <em>Out</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOut()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> out;

	/**
	 * The cached value of the '{@link #getIn() <em>In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> in;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractAction> actions;

	/**
	 * The default value of the '{@link #getStateType() <em>State Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateType()
	 * @generated
	 * @ordered
	 */
	protected static final StateType STATE_TYPE_EDEFAULT = StateType.INFO;

	/**
	 * The cached value of the '{@link #getStateType() <em>State Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateType()
	 * @generated
	 * @ordered
	 */
	protected StateType stateType = STATE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinePackage.Literals.ABSTRACT_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Transition> getOut() {
		if (out == null) {
			out = new EObjectWithInverseEList<Transition>(Transition.class, this, StatemachinePackage.ABSTRACT_STATE__OUT, StatemachinePackage.TRANSITION__FROM);
		}
		return out;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Transition> getIn() {
		if (in == null) {
			in = new EObjectWithInverseEList<Transition>(Transition.class, this, StatemachinePackage.ABSTRACT_STATE__IN, StatemachinePackage.TRANSITION__TO);
		}
		return in;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractAction> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<AbstractAction>(AbstractAction.class, this, StatemachinePackage.ABSTRACT_STATE__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StateType getStateType() {
		return stateType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStateType(StateType newStateType) {
		StateType oldStateType = stateType;
		stateType = newStateType == null ? STATE_TYPE_EDEFAULT : newStateType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_STATE__STATE_TYPE, oldStateType, stateType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.ABSTRACT_STATE__IS_VALID_HAS_VALID_DESCRIPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDescription", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachineValidator.DIAGNOSTIC_SOURCE,
						 StatemachineValidator.ABSTRACT_STATE__IS_VALID_HAS_VALID_NAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidName", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getOut()).basicAdd(otherEnd, msgs);
			case StatemachinePackage.ABSTRACT_STATE__IN:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getIn()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				return ((InternalEList<?>)getOut()).basicRemove(otherEnd, msgs);
			case StatemachinePackage.ABSTRACT_STATE__IN:
				return ((InternalEList<?>)getIn()).basicRemove(otherEnd, msgs);
			case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__NAME:
				return getName();
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				return getOut();
			case StatemachinePackage.ABSTRACT_STATE__IN:
				return getIn();
			case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
				return getDescription();
			case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
				return getActions();
			case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
				return getStateType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__NAME:
				setName((String)newValue);
				return;
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				getOut().clear();
				getOut().addAll((Collection<? extends Transition>)newValue);
				return;
			case StatemachinePackage.ABSTRACT_STATE__IN:
				getIn().clear();
				getIn().addAll((Collection<? extends Transition>)newValue);
				return;
			case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends AbstractAction>)newValue);
				return;
			case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
				setStateType((StateType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				getOut().clear();
				return;
			case StatemachinePackage.ABSTRACT_STATE__IN:
				getIn().clear();
				return;
			case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
				getActions().clear();
				return;
			case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
				setStateType(STATE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinePackage.ABSTRACT_STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StatemachinePackage.ABSTRACT_STATE__OUT:
				return out != null && !out.isEmpty();
			case StatemachinePackage.ABSTRACT_STATE__IN:
				return in != null && !in.isEmpty();
			case StatemachinePackage.ABSTRACT_STATE__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case StatemachinePackage.ABSTRACT_STATE__ACTIONS:
				return actions != null && !actions.isEmpty();
			case StatemachinePackage.ABSTRACT_STATE__STATE_TYPE:
				return stateType != STATE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IVariableReaderWriter.class) {
			switch (baseOperationID) {
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_READ_VARIABLES: return StatemachinePackage.ABSTRACT_STATE___GET_READ_VARIABLES;
				case ConditionsPackage.IVARIABLE_READER_WRITER___GET_WRITE_VARIABLES: return StatemachinePackage.ABSTRACT_STATE___GET_WRITE_VARIABLES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinePackage.ABSTRACT_STATE___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDescription((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.ABSTRACT_STATE___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidName((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinePackage.ABSTRACT_STATE___GET_READ_VARIABLES:
				return getReadVariables();
			case StatemachinePackage.ABSTRACT_STATE___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", stateType: ");
		result.append(stateType);
		result.append(')');
		return result.toString();
	}

} //AbstractStateImpl
