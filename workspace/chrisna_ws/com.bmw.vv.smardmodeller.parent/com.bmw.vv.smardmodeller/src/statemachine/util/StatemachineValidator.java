/**
 */
package statemachine.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see statemachine.StatemachinePackage
 * @generated
 */
public class StatemachineValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final StatemachineValidator INSTANCE = new StatemachineValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "statemachine";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'Abstract State'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_STATE__IS_VALID_HAS_VALID_DESCRIPTION = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Name' of 'Abstract State'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_STATE__IS_VALID_HAS_VALID_NAME = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'Transition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION__IS_VALID_HAS_VALID_DESCRIPTION = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Name' of 'Transition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION__IS_VALID_HAS_VALID_NAME = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Log Level' of 'Transition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION__IS_VALID_HAS_VALID_LOG_LEVEL = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Root Cause' of 'Transition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION__IS_VALID_HAS_VALID_ROOT_CAUSE = 6;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Conditions' of 'Transition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TRANSITION__IS_VALID_HAS_VALID_CONDITIONS = 7;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Action Expression' of 'Action'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ACTION__IS_VALID_HAS_VALID_ACTION_EXPRESSION = 8;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Action Type' of 'Action'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ACTION__IS_VALID_HAS_VALID_ACTION_TYPE = 9;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid No In Transition' of 'Initial State'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int INITIAL_STATE__IS_VALID_HAS_VALID_NO_IN_TRANSITION = 10;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Type' of 'State'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int STATE__IS_VALID_HAS_VALID_TYPE = 11;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Trigger' of 'Abstract Action'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ABSTRACT_ACTION__IS_VALID_HAS_VALID_TRIGGER = 12;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Operands' of 'Compute Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPUTE_VARIABLE__IS_VALID_HAS_VALID_OPERANDS = 13;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Action Expression' of 'Compute Variable'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int COMPUTE_VARIABLE__IS_VALID_HAS_VALID_ACTION_EXPRESSION = 14;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 14;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachineValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return StatemachinePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case StatemachinePackage.DOCUMENT_ROOT:
				return validateDocumentRoot((DocumentRoot)value, diagnostics, context);
			case StatemachinePackage.ABSTRACT_STATE:
				return validateAbstractState((AbstractState)value, diagnostics, context);
			case StatemachinePackage.TRANSITION:
				return validateTransition((Transition)value, diagnostics, context);
			case StatemachinePackage.STATE_MACHINE:
				return validateStateMachine((StateMachine)value, diagnostics, context);
			case StatemachinePackage.ACTION:
				return validateAction((Action)value, diagnostics, context);
			case StatemachinePackage.INITIAL_STATE:
				return validateInitialState((InitialState)value, diagnostics, context);
			case StatemachinePackage.STATE:
				return validateState((State)value, diagnostics, context);
			case StatemachinePackage.ABSTRACT_ACTION:
				return validateAbstractAction((AbstractAction)value, diagnostics, context);
			case StatemachinePackage.COMPUTE_VARIABLE:
				return validateComputeVariable((ComputeVariable)value, diagnostics, context);
			case StatemachinePackage.SHOW_VARIABLE:
				return validateShowVariable((ShowVariable)value, diagnostics, context);
			case StatemachinePackage.SMARD_TRACE_ELEMENT:
				return validateSmardTraceElement((SmardTraceElement)value, diagnostics, context);
			case StatemachinePackage.CONTROL_ACTION:
				return validateControlAction((ControlAction)value, diagnostics, context);
			case StatemachinePackage.TRIGGER:
				return validateTrigger((Trigger)value, diagnostics, context);
			case StatemachinePackage.STATE_TYPE:
				return validateStateType((StateType)value, diagnostics, context);
			case StatemachinePackage.ACTION_TYPE_NOT_EMPTY:
				return validateActionTypeNotEmpty((ActionTypeNotEmpty)value, diagnostics, context);
			case StatemachinePackage.CONTROL_TYPE:
				return validateControlType((ControlType)value, diagnostics, context);
			case StatemachinePackage.INT_OR_TEMPLATE_PLACEHOLDER:
				return validateIntOrTemplatePlaceholder((String)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractState(AbstractState abstractState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(abstractState, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidDescription(abstractState, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidName(abstractState, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidDescription constraint of '<em>Abstract State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractState_isValid_hasValidDescription(AbstractState abstractState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractState.isValid_hasValidDescription(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidName constraint of '<em>Abstract State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractState_isValid_hasValidName(AbstractState abstractState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractState.isValid_hasValidName(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(transition, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransition_isValid_hasValidDescription(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransition_isValid_hasValidName(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransition_isValid_hasValidLogLevel(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransition_isValid_hasValidRootCause(transition, diagnostics, context);
		if (result || diagnostics != null) result &= validateTransition_isValid_hasValidConditions(transition, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidDescription constraint of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition_isValid_hasValidDescription(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transition.isValid_hasValidDescription(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidName constraint of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition_isValid_hasValidName(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transition.isValid_hasValidName(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidLogLevel constraint of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition_isValid_hasValidLogLevel(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transition.isValid_hasValidLogLevel(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidRootCause constraint of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition_isValid_hasValidRootCause(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transition.isValid_hasValidRootCause(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidConditions constraint of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransition_isValid_hasValidConditions(Transition transition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return transition.isValid_hasValidConditions(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateMachine(StateMachine stateMachine, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(stateMachine, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAction(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(action, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(action, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(action, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractAction_isValid_hasValidTrigger(action, diagnostics, context);
		if (result || diagnostics != null) result &= validateAction_isValid_hasValidActionExpression(action, diagnostics, context);
		if (result || diagnostics != null) result &= validateAction_isValid_hasValidActionType(action, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidActionExpression constraint of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAction_isValid_hasValidActionExpression(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return action.isValid_hasValidActionExpression(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidActionType constraint of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAction_isValid_hasValidActionType(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return action.isValid_hasValidActionType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInitialState(InitialState initialState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(initialState, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidDescription(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidName(initialState, diagnostics, context);
		if (result || diagnostics != null) result &= validateInitialState_isValid_hasValidNoInTransition(initialState, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidNoInTransition constraint of '<em>Initial State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInitialState_isValid_hasValidNoInTransition(InitialState initialState, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return initialState.isValid_hasValidNoInTransition(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(state, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(state, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(state, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidDescription(state, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractState_isValid_hasValidName(state, diagnostics, context);
		if (result || diagnostics != null) result &= validateState_isValid_hasValidType(state, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidType constraint of '<em>State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState_isValid_hasValidType(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return state.isValid_hasValidType(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractAction(AbstractAction abstractAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(abstractAction, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(abstractAction, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractAction_isValid_hasValidTrigger(abstractAction, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidTrigger constraint of '<em>Abstract Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbstractAction_isValid_hasValidTrigger(AbstractAction abstractAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return abstractAction.isValid_hasValidTrigger(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputeVariable(ComputeVariable computeVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(computeVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractAction_isValid_hasValidTrigger(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputeVariable_isValid_hasValidOperands(computeVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateComputeVariable_isValid_hasValidActionExpression(computeVariable, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isValid_hasValidOperands constraint of '<em>Compute Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputeVariable_isValid_hasValidOperands(ComputeVariable computeVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return computeVariable.isValid_hasValidOperands(diagnostics, context);
	}

	/**
	 * Validates the isValid_hasValidActionExpression constraint of '<em>Compute Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComputeVariable_isValid_hasValidActionExpression(ComputeVariable computeVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return computeVariable.isValid_hasValidActionExpression(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShowVariable(ShowVariable showVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(showVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(showVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractAction_isValid_hasValidTrigger(showVariable, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSmardTraceElement(SmardTraceElement smardTraceElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(smardTraceElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlAction(ControlAction controlAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(controlAction, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(controlAction, diagnostics, context);
		if (result || diagnostics != null) result &= validateAbstractAction_isValid_hasValidTrigger(controlAction, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTrigger(Trigger trigger, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStateType(StateType stateType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionTypeNotEmpty(ActionTypeNotEmpty actionTypeNotEmpty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateControlType(ControlType controlType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntOrTemplatePlaceholder(String intOrTemplatePlaceholder, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //StatemachineValidator
