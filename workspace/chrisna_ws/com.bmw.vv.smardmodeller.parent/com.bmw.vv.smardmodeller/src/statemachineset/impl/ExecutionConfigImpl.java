/**
 */
package statemachineset.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachineset.ExecutionConfig;
import statemachineset.KeyValuePairUserConfig;
import statemachineset.StatemachinesetPackage;

import statemachineset.util.StatemachinesetValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execution Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachineset.impl.ExecutionConfigImpl#isNeedsEthernet <em>Needs Ethernet</em>}</li>
 *   <li>{@link statemachineset.impl.ExecutionConfigImpl#isNeedsErrorFrames <em>Needs Error Frames</em>}</li>
 *   <li>{@link statemachineset.impl.ExecutionConfigImpl#getKeyValuePairs <em>Key Value Pairs</em>}</li>
 *   <li>{@link statemachineset.impl.ExecutionConfigImpl#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link statemachineset.impl.ExecutionConfigImpl#getSorterValue <em>Sorter Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecutionConfigImpl extends MinimalEObjectImpl.Container implements ExecutionConfig {
	/**
	 * The default value of the '{@link #isNeedsEthernet() <em>Needs Ethernet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNeedsEthernet()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NEEDS_ETHERNET_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNeedsEthernet() <em>Needs Ethernet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNeedsEthernet()
	 * @generated
	 * @ordered
	 */
	protected boolean needsEthernet = NEEDS_ETHERNET_EDEFAULT;

	/**
	 * The default value of the '{@link #isNeedsErrorFrames() <em>Needs Error Frames</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNeedsErrorFrames()
	 * @generated
	 * @ordered
	 */
	protected static final boolean NEEDS_ERROR_FRAMES_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isNeedsErrorFrames() <em>Needs Error Frames</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isNeedsErrorFrames()
	 * @generated
	 * @ordered
	 */
	protected boolean needsErrorFrames = NEEDS_ERROR_FRAMES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getKeyValuePairs() <em>Key Value Pairs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyValuePairs()
	 * @generated
	 * @ordered
	 */
	protected EList<KeyValuePairUserConfig> keyValuePairs;

	/**
	 * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int LOG_LEVEL_EDEFAULT = 40;

	/**
	 * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected int logLevel = LOG_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getSorterValue() <em>Sorter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSorterValue()
	 * @generated
	 * @ordered
	 */
	protected static final int SORTER_VALUE_EDEFAULT = 10000;

	/**
	 * The cached value of the '{@link #getSorterValue() <em>Sorter Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSorterValue()
	 * @generated
	 * @ordered
	 */
	protected int sorterValue = SORTER_VALUE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinesetPackage.Literals.EXECUTION_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isNeedsEthernet() {
		return needsEthernet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNeedsEthernet(boolean newNeedsEthernet) {
		boolean oldNeedsEthernet = needsEthernet;
		needsEthernet = newNeedsEthernet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ETHERNET, oldNeedsEthernet, needsEthernet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isNeedsErrorFrames() {
		return needsErrorFrames;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNeedsErrorFrames(boolean newNeedsErrorFrames) {
		boolean oldNeedsErrorFrames = needsErrorFrames;
		needsErrorFrames = newNeedsErrorFrames;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ERROR_FRAMES, oldNeedsErrorFrames, needsErrorFrames));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<KeyValuePairUserConfig> getKeyValuePairs() {
		if (keyValuePairs == null) {
			keyValuePairs = new EObjectContainmentEList<KeyValuePairUserConfig>(KeyValuePairUserConfig.class, this, StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS);
		}
		return keyValuePairs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getLogLevel() {
		return logLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLogLevel(int newLogLevel) {
		int oldLogLevel = logLevel;
		logLevel = newLogLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.EXECUTION_CONFIG__LOG_LEVEL, oldLogLevel, logLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getSorterValue() {
		return sorterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSorterValue(int newSorterValue) {
		int oldSorterValue = sorterValue;
		sorterValue = newSorterValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.EXECUTION_CONFIG__SORTER_VALUE, oldSorterValue, sorterValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachinesetValidator.DIAGNOSTIC_SOURCE,
						 StatemachinesetValidator.EXECUTION_CONFIG__IS_VALID_HAS_VALID_LOG_LEVEL,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidLogLevel", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSorterValue(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachinesetValidator.DIAGNOSTIC_SOURCE,
						 StatemachinesetValidator.EXECUTION_CONFIG__IS_VALID_HAS_VALID_SORTER_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSorterValue", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS:
				return ((InternalEList<?>)getKeyValuePairs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ETHERNET:
				return isNeedsEthernet();
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ERROR_FRAMES:
				return isNeedsErrorFrames();
			case StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS:
				return getKeyValuePairs();
			case StatemachinesetPackage.EXECUTION_CONFIG__LOG_LEVEL:
				return getLogLevel();
			case StatemachinesetPackage.EXECUTION_CONFIG__SORTER_VALUE:
				return getSorterValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ETHERNET:
				setNeedsEthernet((Boolean)newValue);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ERROR_FRAMES:
				setNeedsErrorFrames((Boolean)newValue);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS:
				getKeyValuePairs().clear();
				getKeyValuePairs().addAll((Collection<? extends KeyValuePairUserConfig>)newValue);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__LOG_LEVEL:
				setLogLevel((Integer)newValue);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__SORTER_VALUE:
				setSorterValue((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ETHERNET:
				setNeedsEthernet(NEEDS_ETHERNET_EDEFAULT);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ERROR_FRAMES:
				setNeedsErrorFrames(NEEDS_ERROR_FRAMES_EDEFAULT);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS:
				getKeyValuePairs().clear();
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__LOG_LEVEL:
				setLogLevel(LOG_LEVEL_EDEFAULT);
				return;
			case StatemachinesetPackage.EXECUTION_CONFIG__SORTER_VALUE:
				setSorterValue(SORTER_VALUE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ETHERNET:
				return needsEthernet != NEEDS_ETHERNET_EDEFAULT;
			case StatemachinesetPackage.EXECUTION_CONFIG__NEEDS_ERROR_FRAMES:
				return needsErrorFrames != NEEDS_ERROR_FRAMES_EDEFAULT;
			case StatemachinesetPackage.EXECUTION_CONFIG__KEY_VALUE_PAIRS:
				return keyValuePairs != null && !keyValuePairs.isEmpty();
			case StatemachinesetPackage.EXECUTION_CONFIG__LOG_LEVEL:
				return logLevel != LOG_LEVEL_EDEFAULT;
			case StatemachinesetPackage.EXECUTION_CONFIG__SORTER_VALUE:
				return sorterValue != SORTER_VALUE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinesetPackage.EXECUTION_CONFIG___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidLogLevel((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinesetPackage.EXECUTION_CONFIG___IS_VALID_HAS_VALID_SORTER_VALUE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSorterValue((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (needsEthernet: ");
		result.append(needsEthernet);
		result.append(", needsErrorFrames: ");
		result.append(needsErrorFrames);
		result.append(", logLevel: ");
		result.append(logLevel);
		result.append(", sorterValue: ");
		result.append(sorterValue);
		result.append(')');
		return result.toString();
	}

} //ExecutionConfigImpl
