/**
 */
package statemachineset.impl;

import conditions.AbstractObserver;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectValidator;

import statemachine.StateMachine;

import statemachineset.ExecutionConfig;
import statemachineset.GeneralInfo;
import statemachineset.Output;
import statemachineset.StateMachineSet;
import statemachineset.StatemachinesetPackage;

import statemachineset.util.StatemachinesetValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Machine Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getGeneralInfo <em>General Info</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getOutput <em>Output</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getExportVersionCounter <em>Export Version Counter</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getId <em>Id</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getExecutionConfig <em>Execution Config</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getObservers <em>Observers</em>}</li>
 *   <li>{@link statemachineset.impl.StateMachineSetImpl#getStateMachines <em>State Machines</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateMachineSetImpl extends MinimalEObjectImpl.Container implements StateMachineSet {
	/**
	 * The cached value of the '{@link #getGeneralInfo() <em>General Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralInfo()
	 * @generated
	 * @ordered
	 */
	protected GeneralInfo generalInfo;

	/**
	 * The cached value of the '{@link #getOutput() <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput()
	 * @generated
	 * @ordered
	 */
	protected Output output;

	/**
	 * The default value of the '{@link #getExportVersionCounter() <em>Export Version Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportVersionCounter()
	 * @generated
	 * @ordered
	 */
	protected static final int EXPORT_VERSION_COUNTER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExportVersionCounter() <em>Export Version Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExportVersionCounter()
	 * @generated
	 * @ordered
	 */
	protected int exportVersionCounter = EXPORT_VERSION_COUNTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExecutionConfig() <em>Execution Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionConfig()
	 * @generated
	 * @ordered
	 */
	protected ExecutionConfig executionConfig;

	/**
	 * The cached value of the '{@link #getObservers() <em>Observers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObservers()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractObserver> observers;

	/**
	 * The cached value of the '{@link #getStateMachines() <em>State Machines</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateMachines()
	 * @generated
	 * @ordered
	 */
	protected EList<StateMachine> stateMachines;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StateMachineSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatemachinesetPackage.Literals.STATE_MACHINE_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeneralInfo getGeneralInfo() {
		return generalInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGeneralInfo(GeneralInfo newGeneralInfo, NotificationChain msgs) {
		GeneralInfo oldGeneralInfo = generalInfo;
		generalInfo = newGeneralInfo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO, oldGeneralInfo, newGeneralInfo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGeneralInfo(GeneralInfo newGeneralInfo) {
		if (newGeneralInfo != generalInfo) {
			NotificationChain msgs = null;
			if (generalInfo != null)
				msgs = ((InternalEObject)generalInfo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO, null, msgs);
			if (newGeneralInfo != null)
				msgs = ((InternalEObject)newGeneralInfo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO, null, msgs);
			msgs = basicSetGeneralInfo(newGeneralInfo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO, newGeneralInfo, newGeneralInfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Output getOutput() {
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutput(Output newOutput, NotificationChain msgs) {
		Output oldOutput = output;
		output = newOutput;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT, oldOutput, newOutput);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOutput(Output newOutput) {
		if (newOutput != output) {
			NotificationChain msgs = null;
			if (output != null)
				msgs = ((InternalEObject)output).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT, null, msgs);
			if (newOutput != null)
				msgs = ((InternalEObject)newOutput).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT, null, msgs);
			msgs = basicSetOutput(newOutput, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT, newOutput, newOutput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getExportVersionCounter() {
		return exportVersionCounter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExportVersionCounter(int newExportVersionCounter) {
		int oldExportVersionCounter = exportVersionCounter;
		exportVersionCounter = newExportVersionCounter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__EXPORT_VERSION_COUNTER, oldExportVersionCounter, exportVersionCounter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExecutionConfig getExecutionConfig() {
		return executionConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExecutionConfig(ExecutionConfig newExecutionConfig, NotificationChain msgs) {
		ExecutionConfig oldExecutionConfig = executionConfig;
		executionConfig = newExecutionConfig;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG, oldExecutionConfig, newExecutionConfig);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExecutionConfig(ExecutionConfig newExecutionConfig) {
		if (newExecutionConfig != executionConfig) {
			NotificationChain msgs = null;
			if (executionConfig != null)
				msgs = ((InternalEObject)executionConfig).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG, null, msgs);
			if (newExecutionConfig != null)
				msgs = ((InternalEObject)newExecutionConfig).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG, null, msgs);
			msgs = basicSetExecutionConfig(newExecutionConfig, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG, newExecutionConfig, newExecutionConfig));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractObserver> getObservers() {
		if (observers == null) {
			observers = new EObjectResolvingEList<AbstractObserver>(AbstractObserver.class, this, StatemachinesetPackage.STATE_MACHINE_SET__OBSERVERS);
		}
		return observers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<StateMachine> getStateMachines() {
		if (stateMachines == null) {
			stateMachines = new EObjectResolvingEList<StateMachine>(StateMachine.class, this, StatemachinesetPackage.STATE_MACHINE_SET__STATE_MACHINES);
		}
		return stateMachines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 StatemachinesetValidator.DIAGNOSTIC_SOURCE,
						 StatemachinesetValidator.STATE_MACHINE_SET__IS_VALID_HAS_VALID_DESCRIPTION,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDescription", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void updateID() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String createUUID() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void incrementExportVersionCounter() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProjectName() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO:
				return basicSetGeneralInfo(null, msgs);
			case StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT:
				return basicSetOutput(null, msgs);
			case StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG:
				return basicSetExecutionConfig(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO:
				return getGeneralInfo();
			case StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT:
				return getOutput();
			case StatemachinesetPackage.STATE_MACHINE_SET__EXPORT_VERSION_COUNTER:
				return getExportVersionCounter();
			case StatemachinesetPackage.STATE_MACHINE_SET__ID:
				return getId();
			case StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG:
				return getExecutionConfig();
			case StatemachinesetPackage.STATE_MACHINE_SET__OBSERVERS:
				return getObservers();
			case StatemachinesetPackage.STATE_MACHINE_SET__STATE_MACHINES:
				return getStateMachines();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO:
				setGeneralInfo((GeneralInfo)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT:
				setOutput((Output)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__EXPORT_VERSION_COUNTER:
				setExportVersionCounter((Integer)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__ID:
				setId((String)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG:
				setExecutionConfig((ExecutionConfig)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__OBSERVERS:
				getObservers().clear();
				getObservers().addAll((Collection<? extends AbstractObserver>)newValue);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__STATE_MACHINES:
				getStateMachines().clear();
				getStateMachines().addAll((Collection<? extends StateMachine>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO:
				setGeneralInfo((GeneralInfo)null);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT:
				setOutput((Output)null);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__EXPORT_VERSION_COUNTER:
				setExportVersionCounter(EXPORT_VERSION_COUNTER_EDEFAULT);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__ID:
				setId(ID_EDEFAULT);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG:
				setExecutionConfig((ExecutionConfig)null);
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__OBSERVERS:
				getObservers().clear();
				return;
			case StatemachinesetPackage.STATE_MACHINE_SET__STATE_MACHINES:
				getStateMachines().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatemachinesetPackage.STATE_MACHINE_SET__GENERAL_INFO:
				return generalInfo != null;
			case StatemachinesetPackage.STATE_MACHINE_SET__OUTPUT:
				return output != null;
			case StatemachinesetPackage.STATE_MACHINE_SET__EXPORT_VERSION_COUNTER:
				return exportVersionCounter != EXPORT_VERSION_COUNTER_EDEFAULT;
			case StatemachinesetPackage.STATE_MACHINE_SET__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case StatemachinesetPackage.STATE_MACHINE_SET__EXECUTION_CONFIG:
				return executionConfig != null;
			case StatemachinesetPackage.STATE_MACHINE_SET__OBSERVERS:
				return observers != null && !observers.isEmpty();
			case StatemachinesetPackage.STATE_MACHINE_SET__STATE_MACHINES:
				return stateMachines != null && !stateMachines.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatemachinesetPackage.STATE_MACHINE_SET___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDescription((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case StatemachinesetPackage.STATE_MACHINE_SET___UPDATE_ID:
				updateID();
				return null;
			case StatemachinesetPackage.STATE_MACHINE_SET___CREATE_UUID:
				return createUUID();
			case StatemachinesetPackage.STATE_MACHINE_SET___INCREMENT_EXPORT_VERSION_COUNTER:
				incrementExportVersionCounter();
				return null;
			case StatemachinesetPackage.STATE_MACHINE_SET___GET_PROJECT_NAME:
				return getProjectName();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (exportVersionCounter: ");
		result.append(exportVersionCounter);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //StateMachineSetImpl
