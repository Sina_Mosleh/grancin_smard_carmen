/**
 */
package statemachineset.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import statemachineset.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StatemachinesetFactoryImpl extends EFactoryImpl implements StatemachinesetFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StatemachinesetFactory init() {
		try {
			StatemachinesetFactory theStatemachinesetFactory = (StatemachinesetFactory)EPackage.Registry.INSTANCE.getEFactory(StatemachinesetPackage.eNS_URI);
			if (theStatemachinesetFactory != null) {
				return theStatemachinesetFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StatemachinesetFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatemachinesetFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StatemachinesetPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case StatemachinesetPackage.GENERAL_INFO: return createGeneralInfo();
			case StatemachinesetPackage.OUTPUT: return createOutput();
			case StatemachinesetPackage.STATE_MACHINE_SET: return createStateMachineSet();
			case StatemachinesetPackage.KEY_VALUE_PAIR_USER_CONFIG: return createKeyValuePairUserConfig();
			case StatemachinesetPackage.EXECUTION_CONFIG: return createExecutionConfig();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public GeneralInfo createGeneralInfo() {
		GeneralInfoImpl generalInfo = new GeneralInfoImpl();
		return generalInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Output createOutput() {
		OutputImpl output = new OutputImpl();
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StateMachineSet createStateMachineSet() {
		StateMachineSetImpl stateMachineSet = new StateMachineSetImpl();
		return stateMachineSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public KeyValuePairUserConfig createKeyValuePairUserConfig() {
		KeyValuePairUserConfigImpl keyValuePairUserConfig = new KeyValuePairUserConfigImpl();
		return keyValuePairUserConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ExecutionConfig createExecutionConfig() {
		ExecutionConfigImpl executionConfig = new ExecutionConfigImpl();
		return executionConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StatemachinesetPackage getStatemachinesetPackage() {
		return (StatemachinesetPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StatemachinesetPackage getPackage() {
		return StatemachinesetPackage.eINSTANCE;
	}

} //StatemachinesetFactoryImpl
