/**
 */
package statemachineset;

import conditions.AbstractObserver;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import statemachine.StateMachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Machine Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link statemachineset.StateMachineSet#getGeneralInfo <em>General Info</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getOutput <em>Output</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getExportVersionCounter <em>Export Version Counter</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getId <em>Id</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getExecutionConfig <em>Execution Config</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getObservers <em>Observers</em>}</li>
 *   <li>{@link statemachineset.StateMachineSet#getStateMachines <em>State Machines</em>}</li>
 * </ul>
 *
 * @see statemachineset.StatemachinesetPackage#getStateMachineSet()
 * @model extendedMetaData="name='stateMachineSet' kind='element'"
 * @generated
 */
public interface StateMachineSet extends EObject {
	/**
	 * Returns the value of the '<em><b>General Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>General Info</em>' containment reference.
	 * @see #setGeneralInfo(GeneralInfo)
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_GeneralInfo()
	 * @model containment="true" required="true"
	 * @generated
	 */
	GeneralInfo getGeneralInfo();

	/**
	 * Sets the value of the '{@link statemachineset.StateMachineSet#getGeneralInfo <em>General Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>General Info</em>' containment reference.
	 * @see #getGeneralInfo()
	 * @generated
	 */
	void setGeneralInfo(GeneralInfo value);

	/**
	 * Returns the value of the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' containment reference.
	 * @see #setOutput(Output)
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_Output()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='output'"
	 * @generated
	 */
	Output getOutput();

	/**
	 * Sets the value of the '{@link statemachineset.StateMachineSet#getOutput <em>Output</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' containment reference.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(Output value);

	/**
	 * Returns the value of the '<em><b>Export Version Counter</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Export Version Counter</em>' attribute.
	 * @see #setExportVersionCounter(int)
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_ExportVersionCounter()
	 * @model default="0" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getExportVersionCounter();

	/**
	 * Sets the value of the '{@link statemachineset.StateMachineSet#getExportVersionCounter <em>Export Version Counter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Export Version Counter</em>' attribute.
	 * @see #getExportVersionCounter()
	 * @generated
	 */
	void setExportVersionCounter(int value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_Id()
	 * @model default="" id="true" required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link statemachineset.StateMachineSet#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Execution Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Config</em>' containment reference.
	 * @see #setExecutionConfig(ExecutionConfig)
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_ExecutionConfig()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ExecutionConfig getExecutionConfig();

	/**
	 * Sets the value of the '{@link statemachineset.StateMachineSet#getExecutionConfig <em>Execution Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Config</em>' containment reference.
	 * @see #getExecutionConfig()
	 * @generated
	 */
	void setExecutionConfig(ExecutionConfig value);

	/**
	 * Returns the value of the '<em><b>Observers</b></em>' reference list.
	 * The list contents are of type {@link conditions.AbstractObserver}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observers</em>' reference list.
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_Observers()
	 * @model
	 * @generated
	 */
	EList<AbstractObserver> getObservers();

	/**
	 * Returns the value of the '<em><b>State Machines</b></em>' reference list.
	 * The list contents are of type {@link statemachine.StateMachine}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machines</em>' reference list.
	 * @see statemachineset.StatemachinesetPackage#getStateMachineSet_StateMachines()
	 * @model
	 * @generated
	 */
	EList<StateMachine> getStateMachines();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDescription(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void updateID();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String createUUID();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void incrementExportVersionCounter();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getProjectName();

} // StateMachineSet
