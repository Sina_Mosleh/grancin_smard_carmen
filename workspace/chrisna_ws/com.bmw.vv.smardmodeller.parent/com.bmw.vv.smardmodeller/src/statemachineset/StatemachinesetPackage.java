/**
 */
package statemachineset;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see statemachineset.StatemachinesetFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/edapt historyURI='modeller.history'"
 * @generated
 */
public interface StatemachinesetPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statemachineset";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://de.bmw/smard.modeller/8.2.0/statemachineset";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "statemachineset";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatemachinesetPackage eINSTANCE = statemachineset.impl.StatemachinesetPackageImpl.init();

	/**
	 * The meta object id for the '{@link statemachineset.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.DocumentRootImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 0;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 1;

	/**
	 * The feature id for the '<em><b>State Machine Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__STATE_MACHINE_SET = 2;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachineset.impl.GeneralInfoImpl <em>General Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.GeneralInfoImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getGeneralInfo()
	 * @generated
	 */
	int GENERAL_INFO = 1;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO__AUTHOR = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO__TOPIC = 2;

	/**
	 * The feature id for the '<em><b>Department</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO__DEPARTMENT = 3;

	/**
	 * The number of structural features of the '<em>General Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>General Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERAL_INFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachineset.impl.OutputImpl <em>Output</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.OutputImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getOutput()
	 * @generated
	 */
	int OUTPUT = 2;

	/**
	 * The feature id for the '<em><b>Target File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT__TARGET_FILE_PATH = 0;

	/**
	 * The number of structural features of the '<em>Output</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Output</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachineset.impl.StateMachineSetImpl <em>State Machine Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.StateMachineSetImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getStateMachineSet()
	 * @generated
	 */
	int STATE_MACHINE_SET = 3;

	/**
	 * The feature id for the '<em><b>General Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__GENERAL_INFO = 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__OUTPUT = 1;

	/**
	 * The feature id for the '<em><b>Export Version Counter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__EXPORT_VERSION_COUNTER = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__ID = 3;

	/**
	 * The feature id for the '<em><b>Execution Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__EXECUTION_CONFIG = 4;

	/**
	 * The feature id for the '<em><b>Observers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__OBSERVERS = 5;

	/**
	 * The feature id for the '<em><b>State Machines</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET__STATE_MACHINES = 6;

	/**
	 * The number of structural features of the '<em>State Machine Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET_FEATURE_COUNT = 7;

	/**
	 * The operation id for the '<em>Is Valid has Valid Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The operation id for the '<em>Update ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET___UPDATE_ID = 1;

	/**
	 * The operation id for the '<em>Create UUID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET___CREATE_UUID = 2;

	/**
	 * The operation id for the '<em>Increment Export Version Counter</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET___INCREMENT_EXPORT_VERSION_COUNTER = 3;

	/**
	 * The operation id for the '<em>Get Project Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET___GET_PROJECT_NAME = 4;

	/**
	 * The number of operations of the '<em>State Machine Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_MACHINE_SET_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link statemachineset.impl.KeyValuePairUserConfigImpl <em>Key Value Pair User Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.KeyValuePairUserConfigImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getKeyValuePairUserConfig()
	 * @generated
	 */
	int KEY_VALUE_PAIR_USER_CONFIG = 4;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_PAIR_USER_CONFIG__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_PAIR_USER_CONFIG__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Key Value Pair User Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_PAIR_USER_CONFIG_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Key Value Pair User Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KEY_VALUE_PAIR_USER_CONFIG_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link statemachineset.impl.ExecutionConfigImpl <em>Execution Config</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see statemachineset.impl.ExecutionConfigImpl
	 * @see statemachineset.impl.StatemachinesetPackageImpl#getExecutionConfig()
	 * @generated
	 */
	int EXECUTION_CONFIG = 5;

	/**
	 * The feature id for the '<em><b>Needs Ethernet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG__NEEDS_ETHERNET = 0;

	/**
	 * The feature id for the '<em><b>Needs Error Frames</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG__NEEDS_ERROR_FRAMES = 1;

	/**
	 * The feature id for the '<em><b>Key Value Pairs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG__KEY_VALUE_PAIRS = 2;

	/**
	 * The feature id for the '<em><b>Log Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG__LOG_LEVEL = 3;

	/**
	 * The feature id for the '<em><b>Sorter Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG__SORTER_VALUE = 4;

	/**
	 * The number of structural features of the '<em>Execution Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = 0;

	/**
	 * The operation id for the '<em>Is Valid has Valid Sorter Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG___IS_VALID_HAS_VALID_SORTER_VALUE__DIAGNOSTICCHAIN_MAP = 1;

	/**
	 * The number of operations of the '<em>Execution Config</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_CONFIG_OPERATION_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link statemachineset.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see statemachineset.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the map '{@link statemachineset.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see statemachineset.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link statemachineset.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see statemachineset.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link statemachineset.DocumentRoot#getStateMachineSet <em>State Machine Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>State Machine Set</em>'.
	 * @see statemachineset.DocumentRoot#getStateMachineSet()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_StateMachineSet();

	/**
	 * Returns the meta object for class '{@link statemachineset.GeneralInfo <em>General Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>General Info</em>'.
	 * @see statemachineset.GeneralInfo
	 * @generated
	 */
	EClass getGeneralInfo();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.GeneralInfo#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Author</em>'.
	 * @see statemachineset.GeneralInfo#getAuthor()
	 * @see #getGeneralInfo()
	 * @generated
	 */
	EAttribute getGeneralInfo_Author();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.GeneralInfo#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see statemachineset.GeneralInfo#getDescription()
	 * @see #getGeneralInfo()
	 * @generated
	 */
	EAttribute getGeneralInfo_Description();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.GeneralInfo#getTopic <em>Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Topic</em>'.
	 * @see statemachineset.GeneralInfo#getTopic()
	 * @see #getGeneralInfo()
	 * @generated
	 */
	EAttribute getGeneralInfo_Topic();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.GeneralInfo#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Department</em>'.
	 * @see statemachineset.GeneralInfo#getDepartment()
	 * @see #getGeneralInfo()
	 * @generated
	 */
	EAttribute getGeneralInfo_Department();

	/**
	 * Returns the meta object for class '{@link statemachineset.Output <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output</em>'.
	 * @see statemachineset.Output
	 * @generated
	 */
	EClass getOutput();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.Output#getTargetFilePath <em>Target File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target File Path</em>'.
	 * @see statemachineset.Output#getTargetFilePath()
	 * @see #getOutput()
	 * @generated
	 */
	EAttribute getOutput_TargetFilePath();

	/**
	 * Returns the meta object for class '{@link statemachineset.StateMachineSet <em>State Machine Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State Machine Set</em>'.
	 * @see statemachineset.StateMachineSet
	 * @generated
	 */
	EClass getStateMachineSet();

	/**
	 * Returns the meta object for the containment reference '{@link statemachineset.StateMachineSet#getGeneralInfo <em>General Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>General Info</em>'.
	 * @see statemachineset.StateMachineSet#getGeneralInfo()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EReference getStateMachineSet_GeneralInfo();

	/**
	 * Returns the meta object for the containment reference '{@link statemachineset.StateMachineSet#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Output</em>'.
	 * @see statemachineset.StateMachineSet#getOutput()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EReference getStateMachineSet_Output();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.StateMachineSet#getExportVersionCounter <em>Export Version Counter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Export Version Counter</em>'.
	 * @see statemachineset.StateMachineSet#getExportVersionCounter()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EAttribute getStateMachineSet_ExportVersionCounter();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.StateMachineSet#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see statemachineset.StateMachineSet#getId()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EAttribute getStateMachineSet_Id();

	/**
	 * Returns the meta object for the containment reference '{@link statemachineset.StateMachineSet#getExecutionConfig <em>Execution Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Execution Config</em>'.
	 * @see statemachineset.StateMachineSet#getExecutionConfig()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EReference getStateMachineSet_ExecutionConfig();

	/**
	 * Returns the meta object for the reference list '{@link statemachineset.StateMachineSet#getObservers <em>Observers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Observers</em>'.
	 * @see statemachineset.StateMachineSet#getObservers()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EReference getStateMachineSet_Observers();

	/**
	 * Returns the meta object for the reference list '{@link statemachineset.StateMachineSet#getStateMachines <em>State Machines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>State Machines</em>'.
	 * @see statemachineset.StateMachineSet#getStateMachines()
	 * @see #getStateMachineSet()
	 * @generated
	 */
	EReference getStateMachineSet_StateMachines();

	/**
	 * Returns the meta object for the '{@link statemachineset.StateMachineSet#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Description</em>' operation.
	 * @see statemachineset.StateMachineSet#isValid_hasValidDescription(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getStateMachineSet__IsValid_hasValidDescription__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachineset.StateMachineSet#updateID() <em>Update ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update ID</em>' operation.
	 * @see statemachineset.StateMachineSet#updateID()
	 * @generated
	 */
	EOperation getStateMachineSet__UpdateID();

	/**
	 * Returns the meta object for the '{@link statemachineset.StateMachineSet#createUUID() <em>Create UUID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create UUID</em>' operation.
	 * @see statemachineset.StateMachineSet#createUUID()
	 * @generated
	 */
	EOperation getStateMachineSet__CreateUUID();

	/**
	 * Returns the meta object for the '{@link statemachineset.StateMachineSet#incrementExportVersionCounter() <em>Increment Export Version Counter</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Increment Export Version Counter</em>' operation.
	 * @see statemachineset.StateMachineSet#incrementExportVersionCounter()
	 * @generated
	 */
	EOperation getStateMachineSet__IncrementExportVersionCounter();

	/**
	 * Returns the meta object for the '{@link statemachineset.StateMachineSet#getProjectName() <em>Get Project Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Project Name</em>' operation.
	 * @see statemachineset.StateMachineSet#getProjectName()
	 * @generated
	 */
	EOperation getStateMachineSet__GetProjectName();

	/**
	 * Returns the meta object for class '{@link statemachineset.KeyValuePairUserConfig <em>Key Value Pair User Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Key Value Pair User Config</em>'.
	 * @see statemachineset.KeyValuePairUserConfig
	 * @generated
	 */
	EClass getKeyValuePairUserConfig();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.KeyValuePairUserConfig#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see statemachineset.KeyValuePairUserConfig#getKey()
	 * @see #getKeyValuePairUserConfig()
	 * @generated
	 */
	EAttribute getKeyValuePairUserConfig_Key();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.KeyValuePairUserConfig#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see statemachineset.KeyValuePairUserConfig#getValue()
	 * @see #getKeyValuePairUserConfig()
	 * @generated
	 */
	EAttribute getKeyValuePairUserConfig_Value();

	/**
	 * Returns the meta object for class '{@link statemachineset.ExecutionConfig <em>Execution Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execution Config</em>'.
	 * @see statemachineset.ExecutionConfig
	 * @generated
	 */
	EClass getExecutionConfig();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.ExecutionConfig#isNeedsEthernet <em>Needs Ethernet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Needs Ethernet</em>'.
	 * @see statemachineset.ExecutionConfig#isNeedsEthernet()
	 * @see #getExecutionConfig()
	 * @generated
	 */
	EAttribute getExecutionConfig_NeedsEthernet();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.ExecutionConfig#isNeedsErrorFrames <em>Needs Error Frames</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Needs Error Frames</em>'.
	 * @see statemachineset.ExecutionConfig#isNeedsErrorFrames()
	 * @see #getExecutionConfig()
	 * @generated
	 */
	EAttribute getExecutionConfig_NeedsErrorFrames();

	/**
	 * Returns the meta object for the containment reference list '{@link statemachineset.ExecutionConfig#getKeyValuePairs <em>Key Value Pairs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Key Value Pairs</em>'.
	 * @see statemachineset.ExecutionConfig#getKeyValuePairs()
	 * @see #getExecutionConfig()
	 * @generated
	 */
	EReference getExecutionConfig_KeyValuePairs();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.ExecutionConfig#getLogLevel <em>Log Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Log Level</em>'.
	 * @see statemachineset.ExecutionConfig#getLogLevel()
	 * @see #getExecutionConfig()
	 * @generated
	 */
	EAttribute getExecutionConfig_LogLevel();

	/**
	 * Returns the meta object for the attribute '{@link statemachineset.ExecutionConfig#getSorterValue <em>Sorter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sorter Value</em>'.
	 * @see statemachineset.ExecutionConfig#getSorterValue()
	 * @see #getExecutionConfig()
	 * @generated
	 */
	EAttribute getExecutionConfig_SorterValue();

	/**
	 * Returns the meta object for the '{@link statemachineset.ExecutionConfig#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Log Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Log Level</em>' operation.
	 * @see statemachineset.ExecutionConfig#isValid_hasValidLogLevel(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getExecutionConfig__IsValid_hasValidLogLevel__DiagnosticChain_Map();

	/**
	 * Returns the meta object for the '{@link statemachineset.ExecutionConfig#isValid_hasValidSorterValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Is Valid has Valid Sorter Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid has Valid Sorter Value</em>' operation.
	 * @see statemachineset.ExecutionConfig#isValid_hasValidSorterValue(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	EOperation getExecutionConfig__IsValid_hasValidSorterValue__DiagnosticChain_Map();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatemachinesetFactory getStatemachinesetFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link statemachineset.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.DocumentRootImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>State Machine Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__STATE_MACHINE_SET = eINSTANCE.getDocumentRoot_StateMachineSet();

		/**
		 * The meta object literal for the '{@link statemachineset.impl.GeneralInfoImpl <em>General Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.GeneralInfoImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getGeneralInfo()
		 * @generated
		 */
		EClass GENERAL_INFO = eINSTANCE.getGeneralInfo();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERAL_INFO__AUTHOR = eINSTANCE.getGeneralInfo_Author();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERAL_INFO__DESCRIPTION = eINSTANCE.getGeneralInfo_Description();

		/**
		 * The meta object literal for the '<em><b>Topic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERAL_INFO__TOPIC = eINSTANCE.getGeneralInfo_Topic();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERAL_INFO__DEPARTMENT = eINSTANCE.getGeneralInfo_Department();

		/**
		 * The meta object literal for the '{@link statemachineset.impl.OutputImpl <em>Output</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.OutputImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getOutput()
		 * @generated
		 */
		EClass OUTPUT = eINSTANCE.getOutput();

		/**
		 * The meta object literal for the '<em><b>Target File Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT__TARGET_FILE_PATH = eINSTANCE.getOutput_TargetFilePath();

		/**
		 * The meta object literal for the '{@link statemachineset.impl.StateMachineSetImpl <em>State Machine Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.StateMachineSetImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getStateMachineSet()
		 * @generated
		 */
		EClass STATE_MACHINE_SET = eINSTANCE.getStateMachineSet();

		/**
		 * The meta object literal for the '<em><b>General Info</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_SET__GENERAL_INFO = eINSTANCE.getStateMachineSet_GeneralInfo();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_SET__OUTPUT = eINSTANCE.getStateMachineSet_Output();

		/**
		 * The meta object literal for the '<em><b>Export Version Counter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_MACHINE_SET__EXPORT_VERSION_COUNTER = eINSTANCE.getStateMachineSet_ExportVersionCounter();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE_MACHINE_SET__ID = eINSTANCE.getStateMachineSet_Id();

		/**
		 * The meta object literal for the '<em><b>Execution Config</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_SET__EXECUTION_CONFIG = eINSTANCE.getStateMachineSet_ExecutionConfig();

		/**
		 * The meta object literal for the '<em><b>Observers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_SET__OBSERVERS = eINSTANCE.getStateMachineSet_Observers();

		/**
		 * The meta object literal for the '<em><b>State Machines</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATE_MACHINE_SET__STATE_MACHINES = eINSTANCE.getStateMachineSet_StateMachines();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE_MACHINE_SET___IS_VALID_HAS_VALID_DESCRIPTION__DIAGNOSTICCHAIN_MAP = eINSTANCE.getStateMachineSet__IsValid_hasValidDescription__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Update ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE_MACHINE_SET___UPDATE_ID = eINSTANCE.getStateMachineSet__UpdateID();

		/**
		 * The meta object literal for the '<em><b>Create UUID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE_MACHINE_SET___CREATE_UUID = eINSTANCE.getStateMachineSet__CreateUUID();

		/**
		 * The meta object literal for the '<em><b>Increment Export Version Counter</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE_MACHINE_SET___INCREMENT_EXPORT_VERSION_COUNTER = eINSTANCE.getStateMachineSet__IncrementExportVersionCounter();

		/**
		 * The meta object literal for the '<em><b>Get Project Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation STATE_MACHINE_SET___GET_PROJECT_NAME = eINSTANCE.getStateMachineSet__GetProjectName();

		/**
		 * The meta object literal for the '{@link statemachineset.impl.KeyValuePairUserConfigImpl <em>Key Value Pair User Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.KeyValuePairUserConfigImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getKeyValuePairUserConfig()
		 * @generated
		 */
		EClass KEY_VALUE_PAIR_USER_CONFIG = eINSTANCE.getKeyValuePairUserConfig();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_VALUE_PAIR_USER_CONFIG__KEY = eINSTANCE.getKeyValuePairUserConfig_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute KEY_VALUE_PAIR_USER_CONFIG__VALUE = eINSTANCE.getKeyValuePairUserConfig_Value();

		/**
		 * The meta object literal for the '{@link statemachineset.impl.ExecutionConfigImpl <em>Execution Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see statemachineset.impl.ExecutionConfigImpl
		 * @see statemachineset.impl.StatemachinesetPackageImpl#getExecutionConfig()
		 * @generated
		 */
		EClass EXECUTION_CONFIG = eINSTANCE.getExecutionConfig();

		/**
		 * The meta object literal for the '<em><b>Needs Ethernet</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONFIG__NEEDS_ETHERNET = eINSTANCE.getExecutionConfig_NeedsEthernet();

		/**
		 * The meta object literal for the '<em><b>Needs Error Frames</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONFIG__NEEDS_ERROR_FRAMES = eINSTANCE.getExecutionConfig_NeedsErrorFrames();

		/**
		 * The meta object literal for the '<em><b>Key Value Pairs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_CONFIG__KEY_VALUE_PAIRS = eINSTANCE.getExecutionConfig_KeyValuePairs();

		/**
		 * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONFIG__LOG_LEVEL = eINSTANCE.getExecutionConfig_LogLevel();

		/**
		 * The meta object literal for the '<em><b>Sorter Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_CONFIG__SORTER_VALUE = eINSTANCE.getExecutionConfig_SorterValue();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Log Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EXECUTION_CONFIG___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP = eINSTANCE.getExecutionConfig__IsValid_hasValidLogLevel__DiagnosticChain_Map();

		/**
		 * The meta object literal for the '<em><b>Is Valid has Valid Sorter Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EXECUTION_CONFIG___IS_VALID_HAS_VALID_SORTER_VALUE__DIAGNOSTICCHAIN_MAP = eINSTANCE.getExecutionConfig__IsValid_hasValidSorterValue__DiagnosticChain_Map();

	}

} //StatemachinesetPackage
