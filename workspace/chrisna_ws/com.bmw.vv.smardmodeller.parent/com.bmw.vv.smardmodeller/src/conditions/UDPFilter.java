/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UDP Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.UDPFilter#getChecksum <em>Checksum</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getUDPFilter()
 * @model
 * @generated
 */
public interface UDPFilter extends TPFilter {
	/**
	 * Returns the value of the '<em><b>Checksum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Checksum</em>' attribute.
	 * @see #setChecksum(String)
	 * @see conditions.ConditionsPackage#getUDPFilter_Checksum()
	 * @model dataType="conditions.HexOrIntOrNullOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getChecksum();

	/**
	 * Sets the value of the '{@link conditions.UDPFilter#getChecksum <em>Checksum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Checksum</em>' attribute.
	 * @see #getChecksum()
	 * @generated
	 */
	void setChecksum(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSourcePort(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDestinationPort(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidChecksum(DiagnosticChain diagnostics, Map<Object, Object> context);

} // UDPFilter
