/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.VerboseDLTMessage#getDltFilter <em>Dlt Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getVerboseDLTMessage()
 * @model
 * @generated
 */
public interface VerboseDLTMessage extends DLTMessage {
	/**
	 * Returns the value of the '<em><b>Dlt Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dlt Filter</em>' containment reference.
	 * @see #setDltFilter(DLTFilter)
	 * @see conditions.ConditionsPackage#getVerboseDLTMessage_DltFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DLTFilter getDltFilter();

	/**
	 * Sets the value of the '{@link conditions.VerboseDLTMessage#getDltFilter <em>Dlt Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dlt Filter</em>' containment reference.
	 * @see #getDltFilter()
	 * @generated
	 */
	void setDltFilter(DLTFilter value);

} // VerboseDLTMessage
