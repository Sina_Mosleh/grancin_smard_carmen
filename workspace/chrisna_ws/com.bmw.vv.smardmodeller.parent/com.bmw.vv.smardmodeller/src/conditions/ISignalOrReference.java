/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISignal Or Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getISignalOrReference()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISignalOrReference extends ISignalComparisonExpressionOperand {
} // ISignalOrReference
