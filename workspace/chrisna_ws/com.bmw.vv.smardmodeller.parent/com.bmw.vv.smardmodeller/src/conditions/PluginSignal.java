/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getPluginSignal()
 * @model
 * @generated
 */
public interface PluginSignal extends DoubleSignal {
} // PluginSignal
