/**
 */
package conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Reference Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SignalReferenceSet#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSignalReferenceSet()
 * @model
 * @generated
 */
public interface SignalReferenceSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.AbstractMessage}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Messages</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getSignalReferenceSet_Messages()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractMessage> getMessages();

} // SignalReferenceSet
