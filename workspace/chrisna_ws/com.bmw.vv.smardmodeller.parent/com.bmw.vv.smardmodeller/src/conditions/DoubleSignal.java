/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}</li>
 *   <li>{@link conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getDoubleSignal()
 * @model
 * @generated
 */
public interface DoubleSignal extends AbstractSignal {
	/**
	 * Returns the value of the '<em><b>Ignore Invalid Values</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * The literals are from the enumeration {@link conditions.BooleanOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignore Invalid Values</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #setIgnoreInvalidValues(BooleanOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getDoubleSignal_IgnoreInvalidValues()
	 * @model default="false" required="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	BooleanOrTemplatePlaceholderEnum getIgnoreInvalidValues();

	/**
	 * Sets the value of the '{@link conditions.DoubleSignal#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ignore Invalid Values</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #getIgnoreInvalidValues()
	 * @generated
	 */
	void setIgnoreInvalidValues(BooleanOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Ignore Invalid Values Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ignore Invalid Values Tmpl Param</em>' attribute.
	 * @see #setIgnoreInvalidValuesTmplParam(String)
	 * @see conditions.ConditionsPackage#getDoubleSignal_IgnoreInvalidValuesTmplParam()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getIgnoreInvalidValuesTmplParam();

	/**
	 * Sets the value of the '{@link conditions.DoubleSignal#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ignore Invalid Values Tmpl Param</em>' attribute.
	 * @see #getIgnoreInvalidValuesTmplParam()
	 * @generated
	 */
	void setIgnoreInvalidValuesTmplParam(String value);

} // DoubleSignal
