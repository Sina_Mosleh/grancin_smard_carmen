/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISignal Comparison Expression Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getISignalComparisonExpressionOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISignalComparisonExpressionOperand extends IOperand {
} // ISignalComparisonExpressionOperand
