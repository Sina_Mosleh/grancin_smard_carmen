/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Format</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.VariableFormat#getDigits <em>Digits</em>}</li>
 *   <li>{@link conditions.VariableFormat#getBaseDataType <em>Base Data Type</em>}</li>
 *   <li>{@link conditions.VariableFormat#isUpperCase <em>Upper Case</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getVariableFormat()
 * @model
 * @generated
 */
public interface VariableFormat extends EObject {
	/**
	 * Returns the value of the '<em><b>Digits</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Digits</em>' attribute.
	 * @see #setDigits(int)
	 * @see conditions.ConditionsPackage#getVariableFormat_Digits()
	 * @model default="0"
	 * @generated
	 */
	int getDigits();

	/**
	 * Sets the value of the '{@link conditions.VariableFormat#getDigits <em>Digits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Digits</em>' attribute.
	 * @see #getDigits()
	 * @generated
	 */
	void setDigits(int value);

	/**
	 * Returns the value of the '<em><b>Base Data Type</b></em>' attribute.
	 * The default value is <code>"Float"</code>.
	 * The literals are from the enumeration {@link conditions.FormatDataType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Data Type</em>' attribute.
	 * @see conditions.FormatDataType
	 * @see #setBaseDataType(FormatDataType)
	 * @see conditions.ConditionsPackage#getVariableFormat_BaseDataType()
	 * @model default="Float" required="true"
	 * @generated
	 */
	FormatDataType getBaseDataType();

	/**
	 * Sets the value of the '{@link conditions.VariableFormat#getBaseDataType <em>Base Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Data Type</em>' attribute.
	 * @see conditions.FormatDataType
	 * @see #getBaseDataType()
	 * @generated
	 */
	void setBaseDataType(FormatDataType value);

	/**
	 * Returns the value of the '<em><b>Upper Case</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Case</em>' attribute.
	 * @see #setUpperCase(boolean)
	 * @see conditions.ConditionsPackage#getVariableFormat_UpperCase()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isUpperCase();

	/**
	 * Sets the value of the '{@link conditions.VariableFormat#isUpperCase <em>Upper Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Case</em>' attribute.
	 * @see #isUpperCase()
	 * @generated
	 */
	void setUpperCase(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDigits(DiagnosticChain diagnostics, Map<Object, Object> context);

} // VariableFormat
