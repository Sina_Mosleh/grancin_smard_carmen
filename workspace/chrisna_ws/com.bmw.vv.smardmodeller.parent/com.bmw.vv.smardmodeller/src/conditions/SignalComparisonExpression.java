/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Comparison Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SignalComparisonExpression#getComparatorSignal <em>Comparator Signal</em>}</li>
 *   <li>{@link conditions.SignalComparisonExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link conditions.SignalComparisonExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSignalComparisonExpression()
 * @model extendedMetaData="name='elementaryCondition' kind='elementOnly'"
 * @generated
 */
public interface SignalComparisonExpression extends Expression, IOperation {
	/**
	 * Returns the value of the '<em><b>Comparator Signal</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.ISignalComparisonExpressionOperand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparator Signal</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getSignalComparisonExpression_ComparatorSignal()
	 * @model containment="true" lower="2" upper="2"
	 *        extendedMetaData="kind='element' name='canBusMessage' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<ISignalComparisonExpressionOperand> getComparatorSignal();

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The default value is <code>"EQ"</code>.
	 * The literals are from the enumeration {@link conditions.Comparator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see conditions.Comparator
	 * @see #setOperator(Comparator)
	 * @see conditions.ConditionsPackage#getSignalComparisonExpression_Operator()
	 * @model default="EQ" required="true"
	 *        extendedMetaData="kind='attribute' name='operator' namespace='##targetNamespace'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	Comparator getOperator();

	/**
	 * Sets the value of the '{@link conditions.SignalComparisonExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see conditions.Comparator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(Comparator value);

	/**
	 * Returns the value of the '<em><b>Operator Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator Tmpl Param</em>' attribute.
	 * @see #setOperatorTmplParam(String)
	 * @see conditions.ConditionsPackage#getSignalComparisonExpression_OperatorTmplParam()
	 * @model
	 * @generated
	 */
	String getOperatorTmplParam();

	/**
	 * Sets the value of the '{@link conditions.SignalComparisonExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator Tmpl Param</em>' attribute.
	 * @see #getOperatorTmplParam()
	 * @generated
	 */
	void setOperatorTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidComparator(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context);

} // SignalComparisonExpression
