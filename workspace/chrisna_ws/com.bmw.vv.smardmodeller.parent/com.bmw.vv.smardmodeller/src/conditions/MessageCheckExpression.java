/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.MessageCheckExpression#getMessage <em>Message</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getMessageCheckExpression()
 * @model
 * @generated
 */
public interface MessageCheckExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Message</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' reference.
	 * @see #setMessage(AbstractMessage)
	 * @see conditions.ConditionsPackage#getMessageCheckExpression_Message()
	 * @model required="true"
	 * @generated
	 */
	AbstractMessage getMessage();

	/**
	 * Sets the value of the '{@link conditions.MessageCheckExpression#getMessage <em>Message</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' reference.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(AbstractMessage value);

} // MessageCheckExpression
