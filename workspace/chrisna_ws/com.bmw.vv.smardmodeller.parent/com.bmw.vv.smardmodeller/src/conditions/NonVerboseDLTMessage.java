/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Verbose DLT Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.NonVerboseDLTMessage#getNonVerboseDltFilter <em>Non Verbose Dlt Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getNonVerboseDLTMessage()
 * @model
 * @generated
 */
public interface NonVerboseDLTMessage extends DLTMessage {
	/**
	 * Returns the value of the '<em><b>Non Verbose Dlt Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Verbose Dlt Filter</em>' containment reference.
	 * @see #setNonVerboseDltFilter(NonVerboseDLTFilter)
	 * @see conditions.ConditionsPackage#getNonVerboseDLTMessage_NonVerboseDltFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	NonVerboseDLTFilter getNonVerboseDltFilter();

	/**
	 * Sets the value of the '{@link conditions.NonVerboseDLTMessage#getNonVerboseDltFilter <em>Non Verbose Dlt Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Non Verbose Dlt Filter</em>' containment reference.
	 * @see #getNonVerboseDltFilter()
	 * @generated
	 */
	void setNonVerboseDltFilter(NonVerboseDLTFilter value);

} // NonVerboseDLTMessage
