/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IP Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSomeIPMessage()
 * @model
 * @generated
 */
public interface SomeIPMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Some IP Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Some IP Filter</em>' containment reference.
	 * @see #setSomeIPFilter(SomeIPFilter)
	 * @see conditions.ConditionsPackage#getSomeIPMessage_SomeIPFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	SomeIPFilter getSomeIPFilter();

	/**
	 * Sets the value of the '{@link conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Some IP Filter</em>' containment reference.
	 * @see #getSomeIPFilter()
	 * @generated
	 */
	void setSomeIPFilter(SomeIPFilter value);

} // SomeIPMessage
