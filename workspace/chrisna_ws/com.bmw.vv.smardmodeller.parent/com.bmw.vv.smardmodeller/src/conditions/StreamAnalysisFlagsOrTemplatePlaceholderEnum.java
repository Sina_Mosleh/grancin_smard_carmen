/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getStreamAnalysisFlagsOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum StreamAnalysisFlagsOrTemplatePlaceholderEnum implements Enumerator {
	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

	/**
	 * The '<em><b>SAF RETRANSMISSION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_RETRANSMISSION_VALUE
	 * @generated
	 * @ordered
	 */
	SAF_RETRANSMISSION(0, "SAF_RETRANSMISSION", "SAF_RETRANSMISSION"),

	/**
	 * The '<em><b>SAF LOST SEGMENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_LOST_SEGMENT_VALUE
	 * @generated
	 * @ordered
	 */
	SAF_LOST_SEGMENT(1, "SAF_LOST_SEGMENT", "SAF_LOST_SEGMENT"),

	/**
	 * The '<em><b>SAF DUP ACK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_DUP_ACK_VALUE
	 * @generated
	 * @ordered
	 */
	SAF_DUP_ACK(2, "SAF_DUP_ACK", "SAF_DUP_ACK"),

	/**
	 * The '<em><b>SAF PARTIAL RETRANSMIT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_PARTIAL_RETRANSMIT_VALUE
	 * @generated
	 * @ordered
	 */
	SAF_PARTIAL_RETRANSMIT(3, "SAF_PARTIAL_RETRANSMIT", "SAF_PARTIAL_RETRANSMIT"),

	/**
	 * The '<em><b>ALL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL_VALUE
	 * @generated
	 * @ordered
	 */
	ALL(4, "ALL", "ALL");

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * The '<em><b>SAF RETRANSMISSION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_RETRANSMISSION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAF_RETRANSMISSION_VALUE = 0;

	/**
	 * The '<em><b>SAF LOST SEGMENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_LOST_SEGMENT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAF_LOST_SEGMENT_VALUE = 1;

	/**
	 * The '<em><b>SAF DUP ACK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_DUP_ACK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAF_DUP_ACK_VALUE = 2;

	/**
	 * The '<em><b>SAF PARTIAL RETRANSMIT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAF_PARTIAL_RETRANSMIT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAF_PARTIAL_RETRANSMIT_VALUE = 3;

	/**
	 * The '<em><b>ALL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ALL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ALL_VALUE = 4;

	/**
	 * An array of all the '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final StreamAnalysisFlagsOrTemplatePlaceholderEnum[] VALUES_ARRAY =
		new StreamAnalysisFlagsOrTemplatePlaceholderEnum[] {
			TEMPLATE_DEFINED,
			SAF_RETRANSMISSION,
			SAF_LOST_SEGMENT,
			SAF_DUP_ACK,
			SAF_PARTIAL_RETRANSMIT,
			ALL,
		};

	/**
	 * A public read-only list of all the '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<StreamAnalysisFlagsOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StreamAnalysisFlagsOrTemplatePlaceholderEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StreamAnalysisFlagsOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StreamAnalysisFlagsOrTemplatePlaceholderEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			StreamAnalysisFlagsOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stream Analysis Flags Or Template Placeholder Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static StreamAnalysisFlagsOrTemplatePlaceholderEnum get(int value) {
		switch (value) {
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
			case SAF_RETRANSMISSION_VALUE: return SAF_RETRANSMISSION;
			case SAF_LOST_SEGMENT_VALUE: return SAF_LOST_SEGMENT;
			case SAF_DUP_ACK_VALUE: return SAF_DUP_ACK;
			case SAF_PARTIAL_RETRANSMIT_VALUE: return SAF_PARTIAL_RETRANSMIT;
			case ALL_VALUE: return ALL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private StreamAnalysisFlagsOrTemplatePlaceholderEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //StreamAnalysisFlagsOrTemplatePlaceholderEnum
