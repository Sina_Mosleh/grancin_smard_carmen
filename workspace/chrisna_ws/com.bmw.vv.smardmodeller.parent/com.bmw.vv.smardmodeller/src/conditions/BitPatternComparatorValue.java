/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bit Pattern Comparator Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.BitPatternComparatorValue#getValue <em>Value</em>}</li>
 *   <li>{@link conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getBitPatternComparatorValue()
 * @model
 * @generated
 */
public interface BitPatternComparatorValue extends ComparatorSignal {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(String)
	 * @see conditions.ConditionsPackage#getBitPatternComparatorValue_Value()
	 * @model default=""
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getValue();

	/**
	 * Sets the value of the '{@link conditions.BitPatternComparatorValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Returns the value of the '<em><b>Startbit</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Startbit</em>' attribute.
	 * @see #isSetStartbit()
	 * @see #unsetStartbit()
	 * @see #setStartbit(String)
	 * @see conditions.ConditionsPackage#getBitPatternComparatorValue_Startbit()
	 * @model default="0" unsettable="true" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getStartbit();

	/**
	 * Sets the value of the '{@link conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Startbit</em>' attribute.
	 * @see #isSetStartbit()
	 * @see #unsetStartbit()
	 * @see #getStartbit()
	 * @generated
	 */
	void setStartbit(String value);

	/**
	 * Unsets the value of the '{@link conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStartbit()
	 * @see #getStartbit()
	 * @see #setStartbit(String)
	 * @generated
	 */
	void unsetStartbit();

	/**
	 * Returns whether the value of the '{@link conditions.BitPatternComparatorValue#getStartbit <em>Startbit</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Startbit</em>' attribute is set.
	 * @see #unsetStartbit()
	 * @see #getStartbit()
	 * @see #setStartbit(String)
	 * @generated
	 */
	boolean isSetStartbit();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidBitPattern(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidStartbit(DiagnosticChain diagnostics, Map<Object, Object> context);

} // BitPatternComparatorValue
