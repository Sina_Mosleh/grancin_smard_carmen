/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Variable Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getValueVariableObserver()
 * @model
 * @generated
 */
public interface ValueVariableObserver extends AbstractObserver {
} // ValueVariableObserver
