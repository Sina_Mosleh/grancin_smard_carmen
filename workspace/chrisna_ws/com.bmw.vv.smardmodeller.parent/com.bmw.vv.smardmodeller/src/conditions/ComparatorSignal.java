/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparator Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ComparatorSignal#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getComparatorSignal()
 * @model abstract="true"
 * @generated
 */
public interface ComparatorSignal extends ISignalComparisonExpressionOperand {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see conditions.ConditionsPackage#getComparatorSignal_Description()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link conditions.ComparatorSignal#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

} // ComparatorSignal
