/**
 */
package conditions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>DLT Message Trace Info</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage#getDLT_MessageTraceInfo()
 * @model
 * @generated
 */
public enum DLT_MessageTraceInfo implements Enumerator {
	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_SPECIFIED(0, "NOT_SPECIFIED", "NOT_SPECIFIED"),

	/**
	 * The '<em><b>DLT TRACE VARIABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_TRACE_VARIABLE(1, "DLT_TRACE_VARIABLE", "DLT_TRACE_VARIABLE"),

	/**
	 * The '<em><b>DLT TRACE FUNCTION IN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_FUNCTION_IN_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_TRACE_FUNCTION_IN(2, "DLT_TRACE_FUNCTION_IN", "DLT_TRACE_FUNCTION_IN"),

	/**
	 * The '<em><b>DLT TRACE FUNCTION OUT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_FUNCTION_OUT_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_TRACE_FUNCTION_OUT(3, "DLT_TRACE_FUNCTION_OUT", "DLT_TRACE_FUNCTION_OUT"),

	/**
	 * The '<em><b>DLT TRACE STATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_STATE_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_TRACE_STATE(4, "DLT_TRACE_STATE", "DLT_TRACE_STATE"),

	/**
	 * The '<em><b>DLT TRACE VFB</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_VFB_VALUE
	 * @generated
	 * @ordered
	 */
	DLT_TRACE_VFB(5, "DLT_TRACE_VFB", "DLT_TRACE_VFB"),

	/**
	 * The '<em><b>Template Defined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined");

	/**
	 * The '<em><b>NOT SPECIFIED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_SPECIFIED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOT_SPECIFIED_VALUE = 0;

	/**
	 * The '<em><b>DLT TRACE VARIABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_VARIABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_TRACE_VARIABLE_VALUE = 1;

	/**
	 * The '<em><b>DLT TRACE FUNCTION IN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_FUNCTION_IN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_TRACE_FUNCTION_IN_VALUE = 2;

	/**
	 * The '<em><b>DLT TRACE FUNCTION OUT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_FUNCTION_OUT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_TRACE_FUNCTION_OUT_VALUE = 3;

	/**
	 * The '<em><b>DLT TRACE STATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_STATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_TRACE_STATE_VALUE = 4;

	/**
	 * The '<em><b>DLT TRACE VFB</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DLT_TRACE_VFB
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DLT_TRACE_VFB_VALUE = 5;

	/**
	 * The '<em><b>Template Defined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TEMPLATE_DEFINED
	 * @model name="TemplateDefined"
	 * @generated
	 * @ordered
	 */
	public static final int TEMPLATE_DEFINED_VALUE = -1;

	/**
	 * An array of all the '<em><b>DLT Message Trace Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DLT_MessageTraceInfo[] VALUES_ARRAY =
		new DLT_MessageTraceInfo[] {
			NOT_SPECIFIED,
			DLT_TRACE_VARIABLE,
			DLT_TRACE_FUNCTION_IN,
			DLT_TRACE_FUNCTION_OUT,
			DLT_TRACE_STATE,
			DLT_TRACE_VFB,
			TEMPLATE_DEFINED,
		};

	/**
	 * A public read-only list of all the '<em><b>DLT Message Trace Info</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DLT_MessageTraceInfo> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>DLT Message Trace Info</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageTraceInfo get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageTraceInfo result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Trace Info</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageTraceInfo getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DLT_MessageTraceInfo result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>DLT Message Trace Info</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DLT_MessageTraceInfo get(int value) {
		switch (value) {
			case NOT_SPECIFIED_VALUE: return NOT_SPECIFIED;
			case DLT_TRACE_VARIABLE_VALUE: return DLT_TRACE_VARIABLE;
			case DLT_TRACE_FUNCTION_IN_VALUE: return DLT_TRACE_FUNCTION_IN;
			case DLT_TRACE_FUNCTION_OUT_VALUE: return DLT_TRACE_FUNCTION_OUT;
			case DLT_TRACE_STATE_VALUE: return DLT_TRACE_STATE;
			case DLT_TRACE_VFB_VALUE: return DLT_TRACE_VFB;
			case TEMPLATE_DEFINED_VALUE: return TEMPLATE_DEFINED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DLT_MessageTraceInfo(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DLT_MessageTraceInfo
