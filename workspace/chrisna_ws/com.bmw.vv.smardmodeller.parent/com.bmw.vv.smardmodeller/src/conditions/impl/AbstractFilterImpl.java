/**
 */
package conditions.impl;

import conditions.AbstractFilter;
import conditions.ConditionsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.AbstractFilterImpl#getPayloadLength <em>Payload Length</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractFilterImpl extends BaseClassWithSourceReferenceImpl implements AbstractFilter {
	/**
	 * The default value of the '{@link #getPayloadLength() <em>Payload Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadLength()
	 * @generated
	 * @ordered
	 */
	protected static final String PAYLOAD_LENGTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPayloadLength() <em>Payload Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPayloadLength()
	 * @generated
	 * @ordered
	 */
	protected String payloadLength = PAYLOAD_LENGTH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getAbstractFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPayloadLength() {
		return payloadLength;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_FILTER__PAYLOAD_LENGTH:
				return getPayloadLength();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_FILTER__PAYLOAD_LENGTH:
				return PAYLOAD_LENGTH_EDEFAULT == null ? payloadLength != null : !PAYLOAD_LENGTH_EDEFAULT.equals(payloadLength);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (payloadLength: ");
		result.append(payloadLength);
		result.append(')');
		return result.toString();
	}

} //AbstractFilterImpl
