/**
 */
package conditions.impl;

import conditions.AUTOSARDataType;
import conditions.ByteOrderType;
import conditions.ConditionsPackage;
import conditions.UniversalPayloadExtractStrategy;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Universal Payload Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.UniversalPayloadExtractStrategyImpl#getExtractionRule <em>Extraction Rule</em>}</li>
 *   <li>{@link conditions.impl.UniversalPayloadExtractStrategyImpl#getByteOrder <em>Byte Order</em>}</li>
 *   <li>{@link conditions.impl.UniversalPayloadExtractStrategyImpl#getExtractionRuleTmplParam <em>Extraction Rule Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.UniversalPayloadExtractStrategyImpl#getSignalDataType <em>Signal Data Type</em>}</li>
 *   <li>{@link conditions.impl.UniversalPayloadExtractStrategyImpl#getSignalDataTypeTmplParam <em>Signal Data Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UniversalPayloadExtractStrategyImpl extends ExtractStrategyImpl implements UniversalPayloadExtractStrategy {
	/**
	 * The default value of the '{@link #getExtractionRule() <em>Extraction Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractionRule()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTRACTION_RULE_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getExtractionRule() <em>Extraction Rule</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractionRule()
	 * @generated
	 * @ordered
	 */
	protected String extractionRule = EXTRACTION_RULE_EDEFAULT;

	/**
	 * The default value of the '{@link #getByteOrder() <em>Byte Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getByteOrder()
	 * @generated
	 * @ordered
	 */
	protected static final ByteOrderType BYTE_ORDER_EDEFAULT = ByteOrderType.INTEL;

	/**
	 * The cached value of the '{@link #getByteOrder() <em>Byte Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getByteOrder()
	 * @generated
	 * @ordered
	 */
	protected ByteOrderType byteOrder = BYTE_ORDER_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtractionRuleTmplParam() <em>Extraction Rule Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractionRuleTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTRACTION_RULE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtractionRuleTmplParam() <em>Extraction Rule Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtractionRuleTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String extractionRuleTmplParam = EXTRACTION_RULE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSignalDataType() <em>Signal Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignalDataType()
	 * @generated
	 * @ordered
	 */
	protected static final AUTOSARDataType SIGNAL_DATA_TYPE_EDEFAULT = AUTOSARDataType.AUNICODE2STRING;

	/**
	 * The cached value of the '{@link #getSignalDataType() <em>Signal Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignalDataType()
	 * @generated
	 * @ordered
	 */
	protected AUTOSARDataType signalDataType = SIGNAL_DATA_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSignalDataTypeTmplParam() <em>Signal Data Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignalDataTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSignalDataTypeTmplParam() <em>Signal Data Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignalDataTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String signalDataTypeTmplParam = SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UniversalPayloadExtractStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getUniversalPayloadExtractStrategy();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtractionRule() {
		return extractionRule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtractionRule(String newExtractionRule) {
		String oldExtractionRule = extractionRule;
		extractionRule = newExtractionRule;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE, oldExtractionRule, extractionRule));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ByteOrderType getByteOrder() {
		return byteOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setByteOrder(ByteOrderType newByteOrder) {
		ByteOrderType oldByteOrder = byteOrder;
		byteOrder = newByteOrder == null ? BYTE_ORDER_EDEFAULT : newByteOrder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER, oldByteOrder, byteOrder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtractionRuleTmplParam() {
		return extractionRuleTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtractionRuleTmplParam(String newExtractionRuleTmplParam) {
		String oldExtractionRuleTmplParam = extractionRuleTmplParam;
		extractionRuleTmplParam = newExtractionRuleTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM, oldExtractionRuleTmplParam, extractionRuleTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AUTOSARDataType getSignalDataType() {
		return signalDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSignalDataType(AUTOSARDataType newSignalDataType) {
		AUTOSARDataType oldSignalDataType = signalDataType;
		signalDataType = newSignalDataType == null ? SIGNAL_DATA_TYPE_EDEFAULT : newSignalDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE, oldSignalDataType, signalDataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSignalDataTypeTmplParam() {
		return signalDataTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSignalDataTypeTmplParam(String newSignalDataTypeTmplParam) {
		String oldSignalDataTypeTmplParam = signalDataTypeTmplParam;
		signalDataTypeTmplParam = newSignalDataTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM, oldSignalDataTypeTmplParam, signalDataTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidExtractString(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_EXTRACT_STRING,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidExtractString", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSignalDataType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSignalDataType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
				return getExtractionRule();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
				return getByteOrder();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
				return getExtractionRuleTmplParam();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
				return getSignalDataType();
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
				return getSignalDataTypeTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
				setExtractionRule((String)newValue);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
				setByteOrder((ByteOrderType)newValue);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
				setExtractionRuleTmplParam((String)newValue);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
				setSignalDataType((AUTOSARDataType)newValue);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
				setSignalDataTypeTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
				setExtractionRule(EXTRACTION_RULE_EDEFAULT);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
				setByteOrder(BYTE_ORDER_EDEFAULT);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
				setExtractionRuleTmplParam(EXTRACTION_RULE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
				setSignalDataType(SIGNAL_DATA_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
				setSignalDataTypeTmplParam(SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE:
				return EXTRACTION_RULE_EDEFAULT == null ? extractionRule != null : !EXTRACTION_RULE_EDEFAULT.equals(extractionRule);
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__BYTE_ORDER:
				return byteOrder != BYTE_ORDER_EDEFAULT;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__EXTRACTION_RULE_TMPL_PARAM:
				return EXTRACTION_RULE_TMPL_PARAM_EDEFAULT == null ? extractionRuleTmplParam != null : !EXTRACTION_RULE_TMPL_PARAM_EDEFAULT.equals(extractionRuleTmplParam);
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE:
				return signalDataType != SIGNAL_DATA_TYPE_EDEFAULT;
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY__SIGNAL_DATA_TYPE_TMPL_PARAM:
				return SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT == null ? signalDataTypeTmplParam != null : !SIGNAL_DATA_TYPE_TMPL_PARAM_EDEFAULT.equals(signalDataTypeTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_EXTRACT_STRING__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidExtractString((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.UNIVERSAL_PAYLOAD_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_SIGNAL_DATA_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSignalDataType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (extractionRule: ");
		result.append(extractionRule);
		result.append(", byteOrder: ");
		result.append(byteOrder);
		result.append(", extractionRuleTmplParam: ");
		result.append(extractionRuleTmplParam);
		result.append(", signalDataType: ");
		result.append(signalDataType);
		result.append(", signalDataTypeTmplParam: ");
		result.append(signalDataTypeTmplParam);
		result.append(')');
		return result.toString();
	}

} //UniversalPayloadExtractStrategyImpl
