/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.EmptyDecodeStrategy;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Empty Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmptyDecodeStrategyImpl extends DecodeStrategyImpl implements EmptyDecodeStrategy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmptyDecodeStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getEmptyDecodeStrategy();
	}

} //EmptyDecodeStrategyImpl
