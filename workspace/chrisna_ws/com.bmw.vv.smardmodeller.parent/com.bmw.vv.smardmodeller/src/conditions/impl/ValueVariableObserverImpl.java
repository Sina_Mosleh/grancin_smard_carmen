/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.ValueVariableObserver;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Variable Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ValueVariableObserverImpl extends AbstractObserverImpl implements ValueVariableObserver {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValueVariableObserverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getValueVariableObserver();
	}

} //ValueVariableObserverImpl
