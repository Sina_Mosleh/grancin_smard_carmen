/**
 */
package conditions.impl;

import conditions.CANFilter;
import conditions.CANFrameTypeOrTemplatePlaceholderEnum;
import conditions.CanExtIdentifierOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.RxTxFlagTypeOrTemplatePlaceholderEnum;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CAN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.CANFilterImpl#getMessageIdRange <em>Message Id Range</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getFrameId <em>Frame Id</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getRxtxFlag <em>Rxtx Flag</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getRxtxFlagTmplParam <em>Rxtx Flag Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getExtIdentifier <em>Ext Identifier</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getFrameType <em>Frame Type</em>}</li>
 *   <li>{@link conditions.impl.CANFilterImpl#getFrameTypeTmplParam <em>Frame Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CANFilterImpl extends AbstractFilterImpl implements CANFilter {
	/**
	 * The default value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIdRange()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_ID_RANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageIdRange()
	 * @generated
	 * @ordered
	 */
	protected String messageIdRange = MESSAGE_ID_RANGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameId()
	 * @generated
	 * @ordered
	 */
	protected static final String FRAME_ID_EDEFAULT = "0x0";

	/**
	 * The cached value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameId()
	 * @generated
	 * @ordered
	 */
	protected String frameId = FRAME_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxtxFlag() <em>Rxtx Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlag()
	 * @generated
	 * @ordered
	 */
	protected static final RxTxFlagTypeOrTemplatePlaceholderEnum RXTX_FLAG_EDEFAULT = RxTxFlagTypeOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getRxtxFlag() <em>Rxtx Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlag()
	 * @generated
	 * @ordered
	 */
	protected RxTxFlagTypeOrTemplatePlaceholderEnum rxtxFlag = RXTX_FLAG_EDEFAULT;

	/**
	 * The default value of the '{@link #getRxtxFlagTmplParam() <em>Rxtx Flag Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlagTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String RXTX_FLAG_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRxtxFlagTmplParam() <em>Rxtx Flag Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRxtxFlagTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String rxtxFlagTmplParam = RXTX_FLAG_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtIdentifier() <em>Ext Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final CanExtIdentifierOrTemplatePlaceholderEnum EXT_IDENTIFIER_EDEFAULT = CanExtIdentifierOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getExtIdentifier() <em>Ext Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifier()
	 * @generated
	 * @ordered
	 */
	protected CanExtIdentifierOrTemplatePlaceholderEnum extIdentifier = EXT_IDENTIFIER_EDEFAULT;

	/**
	 * The default value of the '{@link #getExtIdentifierTmplParam() <em>Ext Identifier Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifierTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExtIdentifierTmplParam() <em>Ext Identifier Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtIdentifierTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String extIdentifierTmplParam = EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrameType() <em>Frame Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameType()
	 * @generated
	 * @ordered
	 */
	protected static final CANFrameTypeOrTemplatePlaceholderEnum FRAME_TYPE_EDEFAULT = CANFrameTypeOrTemplatePlaceholderEnum.STANDARD;

	/**
	 * The cached value of the '{@link #getFrameType() <em>Frame Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameType()
	 * @generated
	 * @ordered
	 */
	protected CANFrameTypeOrTemplatePlaceholderEnum frameType = FRAME_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFrameTypeTmplParam() <em>Frame Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String FRAME_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFrameTypeTmplParam() <em>Frame Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrameTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String frameTypeTmplParam = FRAME_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CANFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getCANFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getMessageIdRange() {
		return messageIdRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setMessageIdRange(String newMessageIdRange) {
		String oldMessageIdRange = messageIdRange;
		messageIdRange = newMessageIdRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE, oldMessageIdRange, messageIdRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFrameId() {
		return frameId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrameId(String newFrameId) {
		String oldFrameId = frameId;
		frameId = newFrameId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__FRAME_ID, oldFrameId, frameId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public RxTxFlagTypeOrTemplatePlaceholderEnum getRxtxFlag() {
		return rxtxFlag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum newRxtxFlag) {
		RxTxFlagTypeOrTemplatePlaceholderEnum oldRxtxFlag = rxtxFlag;
		rxtxFlag = newRxtxFlag == null ? RXTX_FLAG_EDEFAULT : newRxtxFlag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__RXTX_FLAG, oldRxtxFlag, rxtxFlag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getRxtxFlagTmplParam() {
		return rxtxFlagTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRxtxFlagTmplParam(String newRxtxFlagTmplParam) {
		String oldRxtxFlagTmplParam = rxtxFlagTmplParam;
		rxtxFlagTmplParam = newRxtxFlagTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM, oldRxtxFlagTmplParam, rxtxFlagTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CanExtIdentifierOrTemplatePlaceholderEnum getExtIdentifier() {
		return extIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum newExtIdentifier) {
		CanExtIdentifierOrTemplatePlaceholderEnum oldExtIdentifier = extIdentifier;
		extIdentifier = newExtIdentifier == null ? EXT_IDENTIFIER_EDEFAULT : newExtIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER, oldExtIdentifier, extIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getExtIdentifierTmplParam() {
		return extIdentifierTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setExtIdentifierTmplParam(String newExtIdentifierTmplParam) {
		String oldExtIdentifierTmplParam = extIdentifierTmplParam;
		extIdentifierTmplParam = newExtIdentifierTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM, oldExtIdentifierTmplParam, extIdentifierTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CANFrameTypeOrTemplatePlaceholderEnum getFrameType() {
		return frameType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrameType(CANFrameTypeOrTemplatePlaceholderEnum newFrameType) {
		CANFrameTypeOrTemplatePlaceholderEnum oldFrameType = frameType;
		frameType = newFrameType == null ? FRAME_TYPE_EDEFAULT : newFrameType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__FRAME_TYPE, oldFrameType, frameType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getFrameTypeTmplParam() {
		return frameTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setFrameTypeTmplParam(String newFrameTypeTmplParam) {
		String oldFrameTypeTmplParam = frameTypeTmplParam;
		frameTypeTmplParam = newFrameTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM, oldFrameTypeTmplParam, frameTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidFrameIdOrFrameIdRange", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidRxTxFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_FILTER__IS_VALID_HAS_VALID_RX_TX_FLAG,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidRxTxFlag", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidExtIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.CAN_FILTER__IS_VALID_HAS_VALID_EXT_IDENTIFIER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidExtIdentifier", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE:
				return getMessageIdRange();
			case ConditionsPackage.CAN_FILTER__FRAME_ID:
				return getFrameId();
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG:
				return getRxtxFlag();
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM:
				return getRxtxFlagTmplParam();
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER:
				return getExtIdentifier();
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM:
				return getExtIdentifierTmplParam();
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE:
				return getFrameType();
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM:
				return getFrameTypeTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE:
				setMessageIdRange((String)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_ID:
				setFrameId((String)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG:
				setRxtxFlag((RxTxFlagTypeOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM:
				setRxtxFlagTmplParam((String)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER:
				setExtIdentifier((CanExtIdentifierOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM:
				setExtIdentifierTmplParam((String)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE:
				setFrameType((CANFrameTypeOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM:
				setFrameTypeTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE:
				setMessageIdRange(MESSAGE_ID_RANGE_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_ID:
				setFrameId(FRAME_ID_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG:
				setRxtxFlag(RXTX_FLAG_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM:
				setRxtxFlagTmplParam(RXTX_FLAG_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER:
				setExtIdentifier(EXT_IDENTIFIER_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM:
				setExtIdentifierTmplParam(EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE:
				setFrameType(FRAME_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM:
				setFrameTypeTmplParam(FRAME_TYPE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.CAN_FILTER__MESSAGE_ID_RANGE:
				return MESSAGE_ID_RANGE_EDEFAULT == null ? messageIdRange != null : !MESSAGE_ID_RANGE_EDEFAULT.equals(messageIdRange);
			case ConditionsPackage.CAN_FILTER__FRAME_ID:
				return FRAME_ID_EDEFAULT == null ? frameId != null : !FRAME_ID_EDEFAULT.equals(frameId);
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG:
				return rxtxFlag != RXTX_FLAG_EDEFAULT;
			case ConditionsPackage.CAN_FILTER__RXTX_FLAG_TMPL_PARAM:
				return RXTX_FLAG_TMPL_PARAM_EDEFAULT == null ? rxtxFlagTmplParam != null : !RXTX_FLAG_TMPL_PARAM_EDEFAULT.equals(rxtxFlagTmplParam);
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER:
				return extIdentifier != EXT_IDENTIFIER_EDEFAULT;
			case ConditionsPackage.CAN_FILTER__EXT_IDENTIFIER_TMPL_PARAM:
				return EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT == null ? extIdentifierTmplParam != null : !EXT_IDENTIFIER_TMPL_PARAM_EDEFAULT.equals(extIdentifierTmplParam);
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE:
				return frameType != FRAME_TYPE_EDEFAULT;
			case ConditionsPackage.CAN_FILTER__FRAME_TYPE_TMPL_PARAM:
				return FRAME_TYPE_TMPL_PARAM_EDEFAULT == null ? frameTypeTmplParam != null : !FRAME_TYPE_TMPL_PARAM_EDEFAULT.equals(frameTypeTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.CAN_FILTER___IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidFrameIdOrFrameIdRange((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_FILTER___IS_VALID_HAS_VALID_RX_TX_FLAG__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidRxTxFlag((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.CAN_FILTER___IS_VALID_HAS_VALID_EXT_IDENTIFIER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidExtIdentifier((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (messageIdRange: ");
		result.append(messageIdRange);
		result.append(", frameId: ");
		result.append(frameId);
		result.append(", rxtxFlag: ");
		result.append(rxtxFlag);
		result.append(", rxtxFlagTmplParam: ");
		result.append(rxtxFlagTmplParam);
		result.append(", extIdentifier: ");
		result.append(extIdentifier);
		result.append(", extIdentifierTmplParam: ");
		result.append(extIdentifierTmplParam);
		result.append(", frameType: ");
		result.append(frameType);
		result.append(", frameTypeTmplParam: ");
		result.append(frameTypeTmplParam);
		result.append(')');
		return result.toString();
	}

} //CANFilterImpl
