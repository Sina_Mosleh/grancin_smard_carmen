/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum;
import conditions.TCPFilter;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.TCPFilterImpl#getSequenceNumber <em>Sequence Number</em>}</li>
 *   <li>{@link conditions.impl.TCPFilterImpl#getAcknowledgementNumber <em>Acknowledgement Number</em>}</li>
 *   <li>{@link conditions.impl.TCPFilterImpl#getTcpFlags <em>Tcp Flags</em>}</li>
 *   <li>{@link conditions.impl.TCPFilterImpl#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}</li>
 *   <li>{@link conditions.impl.TCPFilterImpl#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.TCPFilterImpl#getStreamValidPayloadOffset <em>Stream Valid Payload Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCPFilterImpl extends TPFilterImpl implements TCPFilter {
	/**
	 * The default value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequenceNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String SEQUENCE_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequenceNumber()
	 * @generated
	 * @ordered
	 */
	protected String sequenceNumber = SEQUENCE_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getAcknowledgementNumber() <em>Acknowledgement Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledgementNumber()
	 * @generated
	 * @ordered
	 */
	protected static final String ACKNOWLEDGEMENT_NUMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAcknowledgementNumber() <em>Acknowledgement Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAcknowledgementNumber()
	 * @generated
	 * @ordered
	 */
	protected String acknowledgementNumber = ACKNOWLEDGEMENT_NUMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getTcpFlags() <em>Tcp Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTcpFlags()
	 * @generated
	 * @ordered
	 */
	protected static final String TCP_FLAGS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTcpFlags() <em>Tcp Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTcpFlags()
	 * @generated
	 * @ordered
	 */
	protected String tcpFlags = TCP_FLAGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getStreamAnalysisFlags() <em>Stream Analysis Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamAnalysisFlags()
	 * @generated
	 * @ordered
	 */
	protected static final StreamAnalysisFlagsOrTemplatePlaceholderEnum STREAM_ANALYSIS_FLAGS_EDEFAULT = StreamAnalysisFlagsOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getStreamAnalysisFlags() <em>Stream Analysis Flags</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamAnalysisFlags()
	 * @generated
	 * @ordered
	 */
	protected StreamAnalysisFlagsOrTemplatePlaceholderEnum streamAnalysisFlags = STREAM_ANALYSIS_FLAGS_EDEFAULT;

	/**
	 * The default value of the '{@link #getStreamAnalysisFlagsTmplParam() <em>Stream Analysis Flags Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamAnalysisFlagsTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStreamAnalysisFlagsTmplParam() <em>Stream Analysis Flags Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamAnalysisFlagsTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String streamAnalysisFlagsTmplParam = STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getStreamValidPayloadOffset() <em>Stream Valid Payload Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamValidPayloadOffset()
	 * @generated
	 * @ordered
	 */
	protected static final String STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStreamValidPayloadOffset() <em>Stream Valid Payload Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStreamValidPayloadOffset()
	 * @generated
	 * @ordered
	 */
	protected String streamValidPayloadOffset = STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TCPFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getTCPFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSequenceNumber() {
		return sequenceNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSequenceNumber(String newSequenceNumber) {
		String oldSequenceNumber = sequenceNumber;
		sequenceNumber = newSequenceNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER, oldSequenceNumber, sequenceNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getAcknowledgementNumber() {
		return acknowledgementNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAcknowledgementNumber(String newAcknowledgementNumber) {
		String oldAcknowledgementNumber = acknowledgementNumber;
		acknowledgementNumber = newAcknowledgementNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER, oldAcknowledgementNumber, acknowledgementNumber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTcpFlags() {
		return tcpFlags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTcpFlags(String newTcpFlags) {
		String oldTcpFlags = tcpFlags;
		tcpFlags = newTcpFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__TCP_FLAGS, oldTcpFlags, tcpFlags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StreamAnalysisFlagsOrTemplatePlaceholderEnum getStreamAnalysisFlags() {
		return streamAnalysisFlags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStreamAnalysisFlags(StreamAnalysisFlagsOrTemplatePlaceholderEnum newStreamAnalysisFlags) {
		StreamAnalysisFlagsOrTemplatePlaceholderEnum oldStreamAnalysisFlags = streamAnalysisFlags;
		streamAnalysisFlags = newStreamAnalysisFlags == null ? STREAM_ANALYSIS_FLAGS_EDEFAULT : newStreamAnalysisFlags;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS, oldStreamAnalysisFlags, streamAnalysisFlags));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStreamAnalysisFlagsTmplParam() {
		return streamAnalysisFlagsTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStreamAnalysisFlagsTmplParam(String newStreamAnalysisFlagsTmplParam) {
		String oldStreamAnalysisFlagsTmplParam = streamAnalysisFlagsTmplParam;
		streamAnalysisFlagsTmplParam = newStreamAnalysisFlagsTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM, oldStreamAnalysisFlagsTmplParam, streamAnalysisFlagsTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStreamValidPayloadOffset() {
		return streamValidPayloadOffset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStreamValidPayloadOffset(String newStreamValidPayloadOffset) {
		String oldStreamValidPayloadOffset = streamValidPayloadOffset;
		streamValidPayloadOffset = newStreamValidPayloadOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET, oldStreamValidPayloadOffset, streamValidPayloadOffset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSourcePort(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_SOURCE_PORT,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSourcePort", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDestinationPort(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_DESTINATION_PORT,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDestinationPort", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSequenceNumber(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_SEQUENCE_NUMBER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSequenceNumber", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidAcknowledgementNumber(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_ACKNOWLEDGEMENT_NUMBER,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidAcknowledgementNumber", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidTcpFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_TCP_FLAG,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidTcpFlag", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStreamAnalysisFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_STREAM_ANALYSIS_FLAG,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStreamAnalysisFlag", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStreamValidPayloadOffset(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_STREAM_VALID_PAYLOAD_OFFSET,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStreamValidPayloadOffset", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
				return getSequenceNumber();
			case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
				return getAcknowledgementNumber();
			case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
				return getTcpFlags();
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
				return getStreamAnalysisFlags();
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
				return getStreamAnalysisFlagsTmplParam();
			case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
				return getStreamValidPayloadOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
				setSequenceNumber((String)newValue);
				return;
			case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
				setAcknowledgementNumber((String)newValue);
				return;
			case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
				setTcpFlags((String)newValue);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
				setStreamAnalysisFlags((StreamAnalysisFlagsOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
				setStreamAnalysisFlagsTmplParam((String)newValue);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
				setStreamValidPayloadOffset((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
				setSequenceNumber(SEQUENCE_NUMBER_EDEFAULT);
				return;
			case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
				setAcknowledgementNumber(ACKNOWLEDGEMENT_NUMBER_EDEFAULT);
				return;
			case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
				setTcpFlags(TCP_FLAGS_EDEFAULT);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
				setStreamAnalysisFlags(STREAM_ANALYSIS_FLAGS_EDEFAULT);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
				setStreamAnalysisFlagsTmplParam(STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
				setStreamValidPayloadOffset(STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
				return SEQUENCE_NUMBER_EDEFAULT == null ? sequenceNumber != null : !SEQUENCE_NUMBER_EDEFAULT.equals(sequenceNumber);
			case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
				return ACKNOWLEDGEMENT_NUMBER_EDEFAULT == null ? acknowledgementNumber != null : !ACKNOWLEDGEMENT_NUMBER_EDEFAULT.equals(acknowledgementNumber);
			case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
				return TCP_FLAGS_EDEFAULT == null ? tcpFlags != null : !TCP_FLAGS_EDEFAULT.equals(tcpFlags);
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
				return streamAnalysisFlags != STREAM_ANALYSIS_FLAGS_EDEFAULT;
			case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
				return STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT == null ? streamAnalysisFlagsTmplParam != null : !STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT.equals(streamAnalysisFlagsTmplParam);
			case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
				return STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT == null ? streamValidPayloadOffset != null : !STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT.equals(streamValidPayloadOffset);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_SOURCE_PORT__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSourcePort((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_DESTINATION_PORT__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDestinationPort((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_SEQUENCE_NUMBER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSequenceNumber((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_ACKNOWLEDGEMENT_NUMBER__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidAcknowledgementNumber((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_TCP_FLAG__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidTcpFlag((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_STREAM_ANALYSIS_FLAG__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStreamAnalysisFlag((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.TCP_FILTER___IS_VALID_HAS_VALID_STREAM_VALID_PAYLOAD_OFFSET__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStreamValidPayloadOffset((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sequenceNumber: ");
		result.append(sequenceNumber);
		result.append(", acknowledgementNumber: ");
		result.append(acknowledgementNumber);
		result.append(", tcpFlags: ");
		result.append(tcpFlags);
		result.append(", streamAnalysisFlags: ");
		result.append(streamAnalysisFlags);
		result.append(", streamAnalysisFlagsTmplParam: ");
		result.append(streamAnalysisFlagsTmplParam);
		result.append(", streamValidPayloadOffset: ");
		result.append(streamValidPayloadOffset);
		result.append(')');
		return result.toString();
	}

} //TCPFilterImpl
