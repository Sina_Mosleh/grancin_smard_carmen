/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.IPv4Filter;
import conditions.ProtocolTypeOrTemplatePlaceholderEnum;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPv4 Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.IPv4FilterImpl#getProtocolType <em>Protocol Type</em>}</li>
 *   <li>{@link conditions.impl.IPv4FilterImpl#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.IPv4FilterImpl#getSourceIP <em>Source IP</em>}</li>
 *   <li>{@link conditions.impl.IPv4FilterImpl#getDestIP <em>Dest IP</em>}</li>
 *   <li>{@link conditions.impl.IPv4FilterImpl#getTimeToLive <em>Time To Live</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IPv4FilterImpl extends AbstractFilterImpl implements IPv4Filter {
	/**
	 * The default value of the '{@link #getProtocolType() <em>Protocol Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolType()
	 * @generated
	 * @ordered
	 */
	protected static final ProtocolTypeOrTemplatePlaceholderEnum PROTOCOL_TYPE_EDEFAULT = ProtocolTypeOrTemplatePlaceholderEnum.ALL;

	/**
	 * The cached value of the '{@link #getProtocolType() <em>Protocol Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolType()
	 * @generated
	 * @ordered
	 */
	protected ProtocolTypeOrTemplatePlaceholderEnum protocolType = PROTOCOL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getProtocolTypeTmplParam() <em>Protocol Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getProtocolTypeTmplParam() <em>Protocol Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProtocolTypeTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String protocolTypeTmplParam = PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceIP() <em>Source IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceIP()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_IP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceIP() <em>Source IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceIP()
	 * @generated
	 * @ordered
	 */
	protected String sourceIP = SOURCE_IP_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestIP() <em>Dest IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestIP()
	 * @generated
	 * @ordered
	 */
	protected static final String DEST_IP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDestIP() <em>Dest IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestIP()
	 * @generated
	 * @ordered
	 */
	protected String destIP = DEST_IP_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeToLive() <em>Time To Live</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeToLive()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_TO_LIVE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimeToLive() <em>Time To Live</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeToLive()
	 * @generated
	 * @ordered
	 */
	protected String timeToLive = TIME_TO_LIVE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPv4FilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getIPv4Filter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ProtocolTypeOrTemplatePlaceholderEnum getProtocolType() {
		return protocolType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum newProtocolType) {
		ProtocolTypeOrTemplatePlaceholderEnum oldProtocolType = protocolType;
		protocolType = newProtocolType == null ? PROTOCOL_TYPE_EDEFAULT : newProtocolType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE, oldProtocolType, protocolType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getProtocolTypeTmplParam() {
		return protocolTypeTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setProtocolTypeTmplParam(String newProtocolTypeTmplParam) {
		String oldProtocolTypeTmplParam = protocolTypeTmplParam;
		protocolTypeTmplParam = newProtocolTypeTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM, oldProtocolTypeTmplParam, protocolTypeTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceIP() {
		return sourceIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceIP(String newSourceIP) {
		String oldSourceIP = sourceIP;
		sourceIP = newSourceIP;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__SOURCE_IP, oldSourceIP, sourceIP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDestIP() {
		return destIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDestIP(String newDestIP) {
		String oldDestIP = destIP;
		destIP = newDestIP;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__DEST_IP, oldDestIP, destIP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getTimeToLive() {
		return timeToLive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setTimeToLive(String newTimeToLive) {
		String oldTimeToLive = timeToLive;
		timeToLive = newTimeToLive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE, oldTimeToLive, timeToLive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSourceIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_SOURCE_IP_ADDRESS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSourceIpAddress", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDestinationIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_DESTINATION_IP_ADDRESS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDestinationIpAddress", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidTimeToLive(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_TIME_TO_LIVE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidTimeToLive", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
				return getProtocolType();
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
				return getProtocolTypeTmplParam();
			case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
				return getSourceIP();
			case ConditionsPackage.IPV4_FILTER__DEST_IP:
				return getDestIP();
			case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
				return getTimeToLive();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
				setProtocolType((ProtocolTypeOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
				setProtocolTypeTmplParam((String)newValue);
				return;
			case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
				setSourceIP((String)newValue);
				return;
			case ConditionsPackage.IPV4_FILTER__DEST_IP:
				setDestIP((String)newValue);
				return;
			case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
				setTimeToLive((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
				setProtocolType(PROTOCOL_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
				setProtocolTypeTmplParam(PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
				setSourceIP(SOURCE_IP_EDEFAULT);
				return;
			case ConditionsPackage.IPV4_FILTER__DEST_IP:
				setDestIP(DEST_IP_EDEFAULT);
				return;
			case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
				setTimeToLive(TIME_TO_LIVE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
				return protocolType != PROTOCOL_TYPE_EDEFAULT;
			case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
				return PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT == null ? protocolTypeTmplParam != null : !PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT.equals(protocolTypeTmplParam);
			case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
				return SOURCE_IP_EDEFAULT == null ? sourceIP != null : !SOURCE_IP_EDEFAULT.equals(sourceIP);
			case ConditionsPackage.IPV4_FILTER__DEST_IP:
				return DEST_IP_EDEFAULT == null ? destIP != null : !DEST_IP_EDEFAULT.equals(destIP);
			case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
				return TIME_TO_LIVE_EDEFAULT == null ? timeToLive != null : !TIME_TO_LIVE_EDEFAULT.equals(timeToLive);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.IPV4_FILTER___IS_VALID_HAS_VALID_SOURCE_IP_ADDRESS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSourceIpAddress((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.IPV4_FILTER___IS_VALID_HAS_VALID_DESTINATION_IP_ADDRESS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDestinationIpAddress((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.IPV4_FILTER___IS_VALID_HAS_VALID_TIME_TO_LIVE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidTimeToLive((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (protocolType: ");
		result.append(protocolType);
		result.append(", protocolTypeTmplParam: ");
		result.append(protocolTypeTmplParam);
		result.append(", sourceIP: ");
		result.append(sourceIP);
		result.append(", destIP: ");
		result.append(destIP);
		result.append(", timeToLive: ");
		result.append(timeToLive);
		result.append(')');
		return result.toString();
	}

} //IPv4FilterImpl
