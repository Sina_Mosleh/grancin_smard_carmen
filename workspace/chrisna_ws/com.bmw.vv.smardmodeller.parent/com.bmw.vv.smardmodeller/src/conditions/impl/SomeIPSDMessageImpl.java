/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.SomeIPSDFilter;
import conditions.SomeIPSDMessage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Some IPSD Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SomeIPSDMessageImpl#getSomeIPSDFilter <em>Some IPSD Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SomeIPSDMessageImpl extends AbstractBusMessageImpl implements SomeIPSDMessage {
	/**
	 * The cached value of the '{@link #getSomeIPSDFilter() <em>Some IPSD Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSomeIPSDFilter()
	 * @generated
	 * @ordered
	 */
	protected SomeIPSDFilter someIPSDFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SomeIPSDMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSomeIPSDMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPSDFilter getSomeIPSDFilter() {
		return someIPSDFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSomeIPSDFilter(SomeIPSDFilter newSomeIPSDFilter, NotificationChain msgs) {
		SomeIPSDFilter oldSomeIPSDFilter = someIPSDFilter;
		someIPSDFilter = newSomeIPSDFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER, oldSomeIPSDFilter, newSomeIPSDFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSomeIPSDFilter(SomeIPSDFilter newSomeIPSDFilter) {
		if (newSomeIPSDFilter != someIPSDFilter) {
			NotificationChain msgs = null;
			if (someIPSDFilter != null)
				msgs = ((InternalEObject)someIPSDFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER, null, msgs);
			if (newSomeIPSDFilter != null)
				msgs = ((InternalEObject)newSomeIPSDFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER, null, msgs);
			msgs = basicSetSomeIPSDFilter(newSomeIPSDFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER, newSomeIPSDFilter, newSomeIPSDFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER:
				return basicSetSomeIPSDFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER:
				return getSomeIPSDFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER:
				setSomeIPSDFilter((SomeIPSDFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER:
				setSomeIPSDFilter((SomeIPSDFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IPSD_MESSAGE__SOME_IPSD_FILTER:
				return someIPSDFilter != null;
		}
		return super.eIsSet(featureID);
	}

} //SomeIPSDMessageImpl
