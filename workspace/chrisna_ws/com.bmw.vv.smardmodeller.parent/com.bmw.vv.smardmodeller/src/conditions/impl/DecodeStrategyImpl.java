/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DecodeStrategy;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DecodeStrategyImpl extends MinimalEObjectImpl.Container implements DecodeStrategy {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecodeStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getDecodeStrategy();
	}

} //DecodeStrategyImpl
