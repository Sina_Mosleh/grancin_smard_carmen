/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.Expression;
import conditions.LogicalExpression;
import conditions.LogicalOperatorType;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Logical Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.LogicalExpressionImpl#getExpressions <em>Expressions</em>}</li>
 *   <li>{@link conditions.impl.LogicalExpressionImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link conditions.impl.LogicalExpressionImpl#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LogicalExpressionImpl extends ExpressionImpl implements LogicalExpression {
	/**
	 * The cached value of the '{@link #getExpressions() <em>Expressions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressions()
	 * @generated
	 * @ordered
	 */
	protected EList<Expression> expressions;

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final LogicalOperatorType OPERATOR_EDEFAULT = LogicalOperatorType.AND;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected LogicalOperatorType operator = OPERATOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String OPERATOR_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperatorTmplParam() <em>Operator Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatorTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String operatorTmplParam = OPERATOR_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getLogicalExpression();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Expression> getExpressions() {
		if (expressions == null) {
			expressions = new EObjectContainmentEList<Expression>(Expression.class, this, ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS);
		}
		return expressions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public LogicalOperatorType getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOperator(LogicalOperatorType newOperator) {
		LogicalOperatorType oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR, oldOperator, operator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOperatorTmplParam() {
		return operatorTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOperatorTmplParam(String newOperatorTmplParam) {
		String oldOperatorTmplParam = operatorTmplParam;
		operatorTmplParam = newOperatorTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM, oldOperatorTmplParam, operatorTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOperator(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_OPERATOR,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOperator", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSubExpressions(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.LOGICAL_EXPRESSION__IS_VALID_HAS_VALID_SUB_EXPRESSIONS,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSubExpressions", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
				return ((InternalEList<?>)getExpressions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
				return getExpressions();
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
				return getOperator();
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
				return getOperatorTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
				getExpressions().clear();
				getExpressions().addAll((Collection<? extends Expression>)newValue);
				return;
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
				setOperator((LogicalOperatorType)newValue);
				return;
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
				setOperatorTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
				getExpressions().clear();
				return;
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
				setOperator(OPERATOR_EDEFAULT);
				return;
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
				setOperatorTmplParam(OPERATOR_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.LOGICAL_EXPRESSION__EXPRESSIONS:
				return expressions != null && !expressions.isEmpty();
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR:
				return operator != OPERATOR_EDEFAULT;
			case ConditionsPackage.LOGICAL_EXPRESSION__OPERATOR_TMPL_PARAM:
				return OPERATOR_TMPL_PARAM_EDEFAULT == null ? operatorTmplParam != null : !OPERATOR_TMPL_PARAM_EDEFAULT.equals(operatorTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.LOGICAL_EXPRESSION___IS_VALID_HAS_VALID_OPERATOR__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOperator((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.LOGICAL_EXPRESSION___IS_VALID_HAS_VALID_SUB_EXPRESSIONS__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSubExpressions((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (operator: ");
		result.append(operator);
		result.append(", operatorTmplParam: ");
		result.append(operatorTmplParam);
		result.append(')');
		return result.toString();
	}

} //LogicalExpressionImpl
