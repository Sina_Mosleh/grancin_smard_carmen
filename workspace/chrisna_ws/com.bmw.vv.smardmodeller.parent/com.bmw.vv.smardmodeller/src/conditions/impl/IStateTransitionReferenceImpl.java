/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.IStateTransitionReference;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import statemachine.AbstractState;
import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IState Transition Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class IStateTransitionReferenceImpl extends MinimalEObjectImpl.Container implements IStateTransitionReference {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IStateTransitionReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getIStateTransitionReference();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractState> getStateDependencies() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Transition> getTransitionDependencies() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.ISTATE_TRANSITION_REFERENCE___GET_STATE_DEPENDENCIES:
				return getStateDependencies();
			case ConditionsPackage.ISTATE_TRANSITION_REFERENCE___GET_TRANSITION_DEPENDENCIES:
				return getTransitionDependencies();
		}
		return super.eInvoke(operationID, arguments);
	}

} //IStateTransitionReferenceImpl
