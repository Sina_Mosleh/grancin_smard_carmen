/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.TrueExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>True Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TrueExpressionImpl extends ExpressionImpl implements TrueExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrueExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getTrueExpression();
	}

} //TrueExpressionImpl
