/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.TPFilter;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.TPFilterImpl#getSourcePort <em>Source Port</em>}</li>
 *   <li>{@link conditions.impl.TPFilterImpl#getDestPort <em>Dest Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class TPFilterImpl extends AbstractFilterImpl implements TPFilter {
	/**
	 * The default value of the '{@link #getSourcePort() <em>Source Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePort()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_PORT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourcePort() <em>Source Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourcePort()
	 * @generated
	 * @ordered
	 */
	protected String sourcePort = SOURCE_PORT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestPort() <em>Dest Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestPort()
	 * @generated
	 * @ordered
	 */
	protected static final String DEST_PORT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDestPort() <em>Dest Port</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestPort()
	 * @generated
	 * @ordered
	 */
	protected String destPort = DEST_PORT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TPFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getTPFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourcePort() {
		return sourcePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourcePort(String newSourcePort) {
		String oldSourcePort = sourcePort;
		sourcePort = newSourcePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TP_FILTER__SOURCE_PORT, oldSourcePort, sourcePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDestPort() {
		return destPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDestPort(String newDestPort) {
		String oldDestPort = destPort;
		destPort = newDestPort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TP_FILTER__DEST_PORT, oldDestPort, destPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.TP_FILTER__SOURCE_PORT:
				return getSourcePort();
			case ConditionsPackage.TP_FILTER__DEST_PORT:
				return getDestPort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.TP_FILTER__SOURCE_PORT:
				setSourcePort((String)newValue);
				return;
			case ConditionsPackage.TP_FILTER__DEST_PORT:
				setDestPort((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TP_FILTER__SOURCE_PORT:
				setSourcePort(SOURCE_PORT_EDEFAULT);
				return;
			case ConditionsPackage.TP_FILTER__DEST_PORT:
				setDestPort(DEST_PORT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.TP_FILTER__SOURCE_PORT:
				return SOURCE_PORT_EDEFAULT == null ? sourcePort != null : !SOURCE_PORT_EDEFAULT.equals(sourcePort);
			case ConditionsPackage.TP_FILTER__DEST_PORT:
				return DEST_PORT_EDEFAULT == null ? destPort != null : !DEST_PORT_EDEFAULT.equals(destPort);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sourcePort: ");
		result.append(sourcePort);
		result.append(", destPort: ");
		result.append(destPort);
		result.append(')');
		return result.toString();
	}

} //TPFilterImpl
