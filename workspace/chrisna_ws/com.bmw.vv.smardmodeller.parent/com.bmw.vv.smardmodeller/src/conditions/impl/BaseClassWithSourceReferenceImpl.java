/**
 */
package conditions.impl;

import conditions.BaseClassWithSourceReference;
import conditions.ConditionsPackage;
import conditions.SourceReference;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Base Class With Source Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.BaseClassWithSourceReferenceImpl#getSourceReference <em>Source Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class BaseClassWithSourceReferenceImpl extends BaseClassWithIDImpl implements BaseClassWithSourceReference {
	/**
	 * The cached value of the '{@link #getSourceReference() <em>Source Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceReference()
	 * @generated
	 * @ordered
	 */
	protected SourceReference sourceReference;

	/**
	 * This is true if the Source Reference containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sourceReferenceESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BaseClassWithSourceReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getBaseClassWithSourceReference();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SourceReference getSourceReference() {
		return sourceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceReference(SourceReference newSourceReference, NotificationChain msgs) {
		SourceReference oldSourceReference = sourceReference;
		sourceReference = newSourceReference;
		boolean oldSourceReferenceESet = sourceReferenceESet;
		sourceReferenceESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, oldSourceReference, newSourceReference, !oldSourceReferenceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceReference(SourceReference newSourceReference) {
		if (newSourceReference != sourceReference) {
			NotificationChain msgs = null;
			if (sourceReference != null)
				msgs = ((InternalEObject)sourceReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, null, msgs);
			if (newSourceReference != null)
				msgs = ((InternalEObject)newSourceReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, null, msgs);
			msgs = basicSetSourceReference(newSourceReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldSourceReferenceESet = sourceReferenceESet;
			sourceReferenceESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, newSourceReference, newSourceReference, !oldSourceReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetSourceReference(NotificationChain msgs) {
		SourceReference oldSourceReference = sourceReference;
		sourceReference = null;
		boolean oldSourceReferenceESet = sourceReferenceESet;
		sourceReferenceESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, oldSourceReference, null, oldSourceReferenceESet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void unsetSourceReference() {
		if (sourceReference != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject)sourceReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, null, msgs);
			msgs = basicUnsetSourceReference(msgs);
			if (msgs != null) msgs.dispatch();
		}
		else {
			boolean oldSourceReferenceESet = sourceReferenceESet;
			sourceReferenceESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE, null, null, oldSourceReferenceESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isSetSourceReference() {
		return sourceReferenceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE:
				return basicUnsetSourceReference(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE:
				return getSourceReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE:
				setSourceReference((SourceReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE:
				unsetSourceReference();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.BASE_CLASS_WITH_SOURCE_REFERENCE__SOURCE_REFERENCE:
				return isSetSourceReference();
		}
		return super.eIsSet(featureID);
	}

} //BaseClassWithSourceReferenceImpl
