/**
 */
package conditions.impl;

import conditions.AbstractObserver;
import conditions.ConditionsPackage;
import conditions.ObserverValueRange;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import statemachine.SmardTraceElement;
import statemachine.StatemachinePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Observer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.AbstractObserverImpl#getName <em>Name</em>}</li>
 *   <li>{@link conditions.impl.AbstractObserverImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link conditions.impl.AbstractObserverImpl#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link conditions.impl.AbstractObserverImpl#getValueRanges <em>Value Ranges</em>}</li>
 *   <li>{@link conditions.impl.AbstractObserverImpl#getValueRangesTmplParam <em>Value Ranges Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.AbstractObserverImpl#isActiveAtStart <em>Active At Start</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractObserverImpl extends BaseClassWithSourceReferenceImpl implements AbstractObserver {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected static final String LOG_LEVEL_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogLevel()
	 * @generated
	 * @ordered
	 */
	protected String logLevel = LOG_LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValueRanges() <em>Value Ranges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueRanges()
	 * @generated
	 * @ordered
	 */
	protected EList<ObserverValueRange> valueRanges;

	/**
	 * The default value of the '{@link #getValueRangesTmplParam() <em>Value Ranges Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueRangesTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_RANGES_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueRangesTmplParam() <em>Value Ranges Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueRangesTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String valueRangesTmplParam = VALUE_RANGES_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActiveAtStart()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACTIVE_AT_START_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isActiveAtStart() <em>Active At Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActiveAtStart()
	 * @generated
	 * @ordered
	 */
	protected boolean activeAtStart = ACTIVE_AT_START_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractObserverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getAbstractObserver();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getLogLevel() {
		return logLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLogLevel(String newLogLevel) {
		String oldLogLevel = logLevel;
		logLevel = newLogLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL, oldLogLevel, logLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ObserverValueRange> getValueRanges() {
		if (valueRanges == null) {
			valueRanges = new EObjectContainmentEList<ObserverValueRange>(ObserverValueRange.class, this, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES);
		}
		return valueRanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getValueRangesTmplParam() {
		return valueRangesTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValueRangesTmplParam(String newValueRangesTmplParam) {
		String oldValueRangesTmplParam = valueRangesTmplParam;
		valueRangesTmplParam = newValueRangesTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM, oldValueRangesTmplParam, valueRangesTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isActiveAtStart() {
		return activeAtStart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setActiveAtStart(boolean newActiveAtStart) {
		boolean oldActiveAtStart = activeAtStart;
		activeAtStart = newActiveAtStart;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START, oldActiveAtStart, activeAtStart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_NAME,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidName", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_LOG_LEVEL,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidLogLevel", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidValueRanges(EList<String> errorMessages, EList<String> warningMessages) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidValueRangesTmplParam(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ABSTRACT_OBSERVER__IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidValueRangesTmplParam", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getIdentifier(String projectName) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
				return ((InternalEList<?>)getValueRanges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
				return getName();
			case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
				return getDescription();
			case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
				return getLogLevel();
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
				return getValueRanges();
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
				return getValueRangesTmplParam();
			case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
				return isActiveAtStart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
				setName((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
				setLogLevel((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
				getValueRanges().clear();
				getValueRanges().addAll((Collection<? extends ObserverValueRange>)newValue);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
				setValueRangesTmplParam((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
				setActiveAtStart((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
				setLogLevel(LOG_LEVEL_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
				getValueRanges().clear();
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
				setValueRangesTmplParam(VALUE_RANGES_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
				setActiveAtStart(ACTIVE_AT_START_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_OBSERVER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ConditionsPackage.ABSTRACT_OBSERVER__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case ConditionsPackage.ABSTRACT_OBSERVER__LOG_LEVEL:
				return LOG_LEVEL_EDEFAULT == null ? logLevel != null : !LOG_LEVEL_EDEFAULT.equals(logLevel);
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES:
				return valueRanges != null && !valueRanges.isEmpty();
			case ConditionsPackage.ABSTRACT_OBSERVER__VALUE_RANGES_TMPL_PARAM:
				return VALUE_RANGES_TMPL_PARAM_EDEFAULT == null ? valueRangesTmplParam != null : !VALUE_RANGES_TMPL_PARAM_EDEFAULT.equals(valueRangesTmplParam);
			case ConditionsPackage.ABSTRACT_OBSERVER__ACTIVE_AT_START:
				return activeAtStart != ACTIVE_AT_START_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == SmardTraceElement.class) {
			switch (baseOperationID) {
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER: return ConditionsPackage.ABSTRACT_OBSERVER___GET_IDENTIFIER;
				case StatemachinePackage.SMARD_TRACE_ELEMENT___GET_IDENTIFIER__STRING: return ConditionsPackage.ABSTRACT_OBSERVER___GET_IDENTIFIER__STRING;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_NAME__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidName((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_LOG_LEVEL__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidLogLevel((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES__ELIST_ELIST:
				return isValid_hasValidValueRanges((EList<String>)arguments.get(0), (EList<String>)arguments.get(1));
			case ConditionsPackage.ABSTRACT_OBSERVER___IS_VALID_HAS_VALID_VALUE_RANGES_TMPL_PARAM__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidValueRangesTmplParam((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ABSTRACT_OBSERVER___GET_IDENTIFIER:
				return getIdentifier();
			case ConditionsPackage.ABSTRACT_OBSERVER___GET_IDENTIFIER__STRING:
				return getIdentifier((String)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", logLevel: ");
		result.append(logLevel);
		result.append(", valueRangesTmplParam: ");
		result.append(valueRangesTmplParam);
		result.append(", activeAtStart: ");
		result.append(activeAtStart);
		result.append(')');
		return result.toString();
	}

} //AbstractObserverImpl
