/**
 */
package conditions.impl;

import conditions.BooleanOrTemplatePlaceholderEnum;
import conditions.ConditionsPackage;
import conditions.PluginStateExtractStrategy;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin State Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.PluginStateExtractStrategyImpl#getStates <em>States</em>}</li>
 *   <li>{@link conditions.impl.PluginStateExtractStrategyImpl#getStatesTmplParam <em>States Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.PluginStateExtractStrategyImpl#getStatesActive <em>States Active</em>}</li>
 *   <li>{@link conditions.impl.PluginStateExtractStrategyImpl#getStatesActiveTmplParam <em>States Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginStateExtractStrategyImpl extends ExtractStrategyImpl implements PluginStateExtractStrategy {
	/**
	 * The default value of the '{@link #getStates() <em>States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected static final String STATES_EDEFAULT = "ALL";

	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected String states = STATES_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatesTmplParam() <em>States Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STATES_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatesTmplParam() <em>States Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String statesTmplParam = STATES_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatesActive() <em>States Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesActive()
	 * @generated
	 * @ordered
	 */
	protected static final BooleanOrTemplatePlaceholderEnum STATES_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

	/**
	 * The cached value of the '{@link #getStatesActive() <em>States Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesActive()
	 * @generated
	 * @ordered
	 */
	protected BooleanOrTemplatePlaceholderEnum statesActive = STATES_ACTIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatesActiveTmplParam() <em>States Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String STATES_ACTIVE_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatesActiveTmplParam() <em>States Active Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatesActiveTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String statesActiveTmplParam = STATES_ACTIVE_TMPL_PARAM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PluginStateExtractStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getPluginStateExtractStrategy();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStates() {
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStates(String newStates) {
		String oldStates = states;
		states = newStates;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES, oldStates, states));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStatesTmplParam() {
		return statesTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStatesTmplParam(String newStatesTmplParam) {
		String oldStatesTmplParam = statesTmplParam;
		statesTmplParam = newStatesTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM, oldStatesTmplParam, statesTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public BooleanOrTemplatePlaceholderEnum getStatesActive() {
		return statesActive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStatesActive(BooleanOrTemplatePlaceholderEnum newStatesActive) {
		BooleanOrTemplatePlaceholderEnum oldStatesActive = statesActive;
		statesActive = newStatesActive == null ? STATES_ACTIVE_EDEFAULT : newStatesActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE, oldStatesActive, statesActive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getStatesActiveTmplParam() {
		return statesActiveTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStatesActiveTmplParam(String newStatesActiveTmplParam) {
		String oldStatesActiveTmplParam = statesActiveTmplParam;
		statesActiveTmplParam = newStatesActiveTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM, oldStatesActiveTmplParam, statesActiveTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATES_ACTIVE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidStatesActive", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.PLUGIN_STATE_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_STATE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidState", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
				return getStates();
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
				return getStatesTmplParam();
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
				return getStatesActive();
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
				return getStatesActiveTmplParam();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
				setStates((String)newValue);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
				setStatesTmplParam((String)newValue);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
				setStatesActive((BooleanOrTemplatePlaceholderEnum)newValue);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
				setStatesActiveTmplParam((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
				setStates(STATES_EDEFAULT);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
				setStatesTmplParam(STATES_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
				setStatesActive(STATES_ACTIVE_EDEFAULT);
				return;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
				setStatesActiveTmplParam(STATES_ACTIVE_TMPL_PARAM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES:
				return STATES_EDEFAULT == null ? states != null : !STATES_EDEFAULT.equals(states);
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_TMPL_PARAM:
				return STATES_TMPL_PARAM_EDEFAULT == null ? statesTmplParam != null : !STATES_TMPL_PARAM_EDEFAULT.equals(statesTmplParam);
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE:
				return statesActive != STATES_ACTIVE_EDEFAULT;
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY__STATES_ACTIVE_TMPL_PARAM:
				return STATES_ACTIVE_TMPL_PARAM_EDEFAULT == null ? statesActiveTmplParam != null : !STATES_ACTIVE_TMPL_PARAM_EDEFAULT.equals(statesActiveTmplParam);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STATES_ACTIVE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidStatesActive((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.PLUGIN_STATE_EXTRACT_STRATEGY___IS_VALID_HAS_VALID_STATE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidState((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (states: ");
		result.append(states);
		result.append(", statesTmplParam: ");
		result.append(statesTmplParam);
		result.append(", statesActive: ");
		result.append(statesActive);
		result.append(", statesActiveTmplParam: ");
		result.append(statesActiveTmplParam);
		result.append(')');
		return result.toString();
	}

} //PluginStateExtractStrategyImpl
