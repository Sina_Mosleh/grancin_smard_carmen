/**
 */
package conditions.impl;

import conditions.AbstractBusMessage;
import conditions.AbstractFilter;
import conditions.ConditionsPackage;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Bus Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.AbstractBusMessageImpl#getBusId <em>Bus Id</em>}</li>
 *   <li>{@link conditions.impl.AbstractBusMessageImpl#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 *   <li>{@link conditions.impl.AbstractBusMessageImpl#getBusIdRange <em>Bus Id Range</em>}</li>
 *   <li>{@link conditions.impl.AbstractBusMessageImpl#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractBusMessageImpl extends AbstractMessageImpl implements AbstractBusMessage {
	/**
	 * The default value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusId()
	 * @generated
	 * @ordered
	 */
	protected static final String BUS_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusId()
	 * @generated
	 * @ordered
	 */
	protected String busId = BUS_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdTmplParam()
	 * @generated
	 * @ordered
	 */
	protected static final String BUS_ID_TMPL_PARAM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdTmplParam()
	 * @generated
	 * @ordered
	 */
	protected String busIdTmplParam = BUS_ID_TMPL_PARAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getBusIdRange() <em>Bus Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdRange()
	 * @generated
	 * @ordered
	 */
	protected static final String BUS_ID_RANGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBusIdRange() <em>Bus Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusIdRange()
	 * @generated
	 * @ordered
	 */
	protected String busIdRange = BUS_ID_RANGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFilters() <em>Filters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilters()
	 * @generated
	 * @ordered
	 */
	protected EList<AbstractFilter> filters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractBusMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getAbstractBusMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusId() {
		return busId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusId(String newBusId) {
		String oldBusId = busId;
		busId = newBusId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID, oldBusId, busId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusIdTmplParam() {
		return busIdTmplParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusIdTmplParam(String newBusIdTmplParam) {
		String oldBusIdTmplParam = busIdTmplParam;
		busIdTmplParam = newBusIdTmplParam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM, oldBusIdTmplParam, busIdTmplParam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getBusIdRange() {
		return busIdRange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBusIdRange(String newBusIdRange) {
		String oldBusIdRange = busIdRange;
		busIdRange = newBusIdRange;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE, oldBusIdRange, busIdRange));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AbstractFilter> getFilters() {
		if (filters == null) {
			filters = new EObjectContainmentEList<AbstractFilter>(AbstractFilter.class, this, ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS);
		}
		return filters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_HAS_VALID_BUS_ID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidBusId", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_isOsiLayerConform(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ABSTRACT_BUS_MESSAGE__IS_VALID_IS_OSI_LAYER_CONFORM,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_isOsiLayerConform", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				return ((InternalEList<?>)getFilters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
				return getBusId();
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
				return getBusIdTmplParam();
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
				return getBusIdRange();
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				return getFilters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
				setBusId((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
				setBusIdTmplParam((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
				setBusIdRange((String)newValue);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				getFilters().clear();
				getFilters().addAll((Collection<? extends AbstractFilter>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
				setBusId(BUS_ID_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
				setBusIdTmplParam(BUS_ID_TMPL_PARAM_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
				setBusIdRange(BUS_ID_RANGE_EDEFAULT);
				return;
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				getFilters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID:
				return BUS_ID_EDEFAULT == null ? busId != null : !BUS_ID_EDEFAULT.equals(busId);
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_TMPL_PARAM:
				return BUS_ID_TMPL_PARAM_EDEFAULT == null ? busIdTmplParam != null : !BUS_ID_TMPL_PARAM_EDEFAULT.equals(busIdTmplParam);
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__BUS_ID_RANGE:
				return BUS_ID_RANGE_EDEFAULT == null ? busIdRange != null : !BUS_ID_RANGE_EDEFAULT.equals(busIdRange);
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE__FILTERS:
				return filters != null && !filters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE___IS_VALID_HAS_VALID_BUS_ID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidBusId((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ABSTRACT_BUS_MESSAGE___IS_VALID_IS_OSI_LAYER_CONFORM__DIAGNOSTICCHAIN_MAP:
				return isValid_isOsiLayerConform((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (busId: ");
		result.append(busId);
		result.append(", busIdTmplParam: ");
		result.append(busIdTmplParam);
		result.append(", busIdRange: ");
		result.append(busIdRange);
		result.append(')');
		return result.toString();
	}

} //AbstractBusMessageImpl
