/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.SomeIPFilter;
import conditions.SomeIPMessage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Some IP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SomeIPMessageImpl#getSomeIPFilter <em>Some IP Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SomeIPMessageImpl extends AbstractBusMessageImpl implements SomeIPMessage {
	/**
	 * The cached value of the '{@link #getSomeIPFilter() <em>Some IP Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSomeIPFilter()
	 * @generated
	 * @ordered
	 */
	protected SomeIPFilter someIPFilter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SomeIPMessageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSomeIPMessage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SomeIPFilter getSomeIPFilter() {
		return someIPFilter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSomeIPFilter(SomeIPFilter newSomeIPFilter, NotificationChain msgs) {
		SomeIPFilter oldSomeIPFilter = someIPFilter;
		someIPFilter = newSomeIPFilter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER, oldSomeIPFilter, newSomeIPFilter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSomeIPFilter(SomeIPFilter newSomeIPFilter) {
		if (newSomeIPFilter != someIPFilter) {
			NotificationChain msgs = null;
			if (someIPFilter != null)
				msgs = ((InternalEObject)someIPFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER, null, msgs);
			if (newSomeIPFilter != null)
				msgs = ((InternalEObject)newSomeIPFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER, null, msgs);
			msgs = basicSetSomeIPFilter(newSomeIPFilter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER, newSomeIPFilter, newSomeIPFilter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER:
				return basicSetSomeIPFilter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER:
				return getSomeIPFilter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER:
				setSomeIPFilter((SomeIPFilter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER:
				setSomeIPFilter((SomeIPFilter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SOME_IP_MESSAGE__SOME_IP_FILTER:
				return someIPFilter != null;
		}
		return super.eIsSet(featureID);
	}

} //SomeIPMessageImpl
