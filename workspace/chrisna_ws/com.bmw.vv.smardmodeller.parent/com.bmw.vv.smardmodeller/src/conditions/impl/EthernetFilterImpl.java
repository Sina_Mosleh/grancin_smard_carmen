/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.EthernetFilter;

import conditions.util.ConditionsValidator;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ethernet Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getSourceMAC <em>Source MAC</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getDestMAC <em>Dest MAC</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getEtherType <em>Ether Type</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getInnerVlanId <em>Inner Vlan Id</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getOuterVlanId <em>Outer Vlan Id</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getInnerVlanCFI <em>Inner Vlan CFI</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getOuterVlanCFI <em>Outer Vlan CFI</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getInnerVlanVID <em>Inner Vlan VID</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getOuterVlanVID <em>Outer Vlan VID</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getCRC <em>CRC</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getInnerVlanTPID <em>Inner Vlan TPID</em>}</li>
 *   <li>{@link conditions.impl.EthernetFilterImpl#getOuterVlanTPID <em>Outer Vlan TPID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EthernetFilterImpl extends AbstractFilterImpl implements EthernetFilter {
	/**
	 * The default value of the '{@link #getSourceMAC() <em>Source MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMAC()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_MAC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceMAC() <em>Source MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceMAC()
	 * @generated
	 * @ordered
	 */
	protected String sourceMAC = SOURCE_MAC_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestMAC() <em>Dest MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestMAC()
	 * @generated
	 * @ordered
	 */
	protected static final String DEST_MAC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDestMAC() <em>Dest MAC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestMAC()
	 * @generated
	 * @ordered
	 */
	protected String destMAC = DEST_MAC_EDEFAULT;

	/**
	 * The default value of the '{@link #getEtherType() <em>Ether Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEtherType()
	 * @generated
	 * @ordered
	 */
	protected static final String ETHER_TYPE_EDEFAULT = "0x0800";

	/**
	 * The cached value of the '{@link #getEtherType() <em>Ether Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEtherType()
	 * @generated
	 * @ordered
	 */
	protected String etherType = ETHER_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInnerVlanId() <em>Inner Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanId()
	 * @generated
	 * @ordered
	 */
	protected static final String INNER_VLAN_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInnerVlanId() <em>Inner Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanId()
	 * @generated
	 * @ordered
	 */
	protected String innerVlanId = INNER_VLAN_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOuterVlanId() <em>Outer Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanId()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTER_VLAN_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOuterVlanId() <em>Outer Vlan Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanId()
	 * @generated
	 * @ordered
	 */
	protected String outerVlanId = OUTER_VLAN_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getInnerVlanCFI() <em>Inner Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanCFI()
	 * @generated
	 * @ordered
	 */
	protected static final String INNER_VLAN_CFI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInnerVlanCFI() <em>Inner Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanCFI()
	 * @generated
	 * @ordered
	 */
	protected String innerVlanCFI = INNER_VLAN_CFI_EDEFAULT;

	/**
	 * The default value of the '{@link #getOuterVlanCFI() <em>Outer Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanCFI()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTER_VLAN_CFI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOuterVlanCFI() <em>Outer Vlan CFI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanCFI()
	 * @generated
	 * @ordered
	 */
	protected String outerVlanCFI = OUTER_VLAN_CFI_EDEFAULT;

	/**
	 * The default value of the '{@link #getInnerVlanVID() <em>Inner Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanVID()
	 * @generated
	 * @ordered
	 */
	protected static final String INNER_VLAN_VID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInnerVlanVID() <em>Inner Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanVID()
	 * @generated
	 * @ordered
	 */
	protected String innerVlanVID = INNER_VLAN_VID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOuterVlanVID() <em>Outer Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanVID()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTER_VLAN_VID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOuterVlanVID() <em>Outer Vlan VID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanVID()
	 * @generated
	 * @ordered
	 */
	protected String outerVlanVID = OUTER_VLAN_VID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCRC() <em>CRC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCRC()
	 * @generated
	 * @ordered
	 */
	protected static final String CRC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCRC() <em>CRC</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCRC()
	 * @generated
	 * @ordered
	 */
	protected String crc = CRC_EDEFAULT;

	/**
	 * The default value of the '{@link #getInnerVlanTPID() <em>Inner Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanTPID()
	 * @generated
	 * @ordered
	 */
	protected static final String INNER_VLAN_TPID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInnerVlanTPID() <em>Inner Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerVlanTPID()
	 * @generated
	 * @ordered
	 */
	protected String innerVlanTPID = INNER_VLAN_TPID_EDEFAULT;

	/**
	 * The default value of the '{@link #getOuterVlanTPID() <em>Outer Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanTPID()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTER_VLAN_TPID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOuterVlanTPID() <em>Outer Vlan TPID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOuterVlanTPID()
	 * @generated
	 * @ordered
	 */
	protected String outerVlanTPID = OUTER_VLAN_TPID_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EthernetFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getEthernetFilter();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getSourceMAC() {
		return sourceMAC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSourceMAC(String newSourceMAC) {
		String oldSourceMAC = sourceMAC;
		sourceMAC = newSourceMAC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC, oldSourceMAC, sourceMAC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDestMAC() {
		return destMAC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDestMAC(String newDestMAC) {
		String oldDestMAC = destMAC;
		destMAC = newDestMAC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__DEST_MAC, oldDestMAC, destMAC));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getEtherType() {
		return etherType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEtherType(String newEtherType) {
		String oldEtherType = etherType;
		etherType = newEtherType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE, oldEtherType, etherType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInnerVlanId() {
		return innerVlanId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInnerVlanId(String newInnerVlanId) {
		String oldInnerVlanId = innerVlanId;
		innerVlanId = newInnerVlanId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID, oldInnerVlanId, innerVlanId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOuterVlanId() {
		return outerVlanId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOuterVlanId(String newOuterVlanId) {
		String oldOuterVlanId = outerVlanId;
		outerVlanId = newOuterVlanId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID, oldOuterVlanId, outerVlanId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInnerVlanCFI() {
		return innerVlanCFI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInnerVlanCFI(String newInnerVlanCFI) {
		String oldInnerVlanCFI = innerVlanCFI;
		innerVlanCFI = newInnerVlanCFI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI, oldInnerVlanCFI, innerVlanCFI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOuterVlanCFI() {
		return outerVlanCFI;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOuterVlanCFI(String newOuterVlanCFI) {
		String oldOuterVlanCFI = outerVlanCFI;
		outerVlanCFI = newOuterVlanCFI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI, oldOuterVlanCFI, outerVlanCFI));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInnerVlanVID() {
		return innerVlanVID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInnerVlanVID(String newInnerVlanVID) {
		String oldInnerVlanVID = innerVlanVID;
		innerVlanVID = newInnerVlanVID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID, oldInnerVlanVID, innerVlanVID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOuterVlanVID() {
		return outerVlanVID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOuterVlanVID(String newOuterVlanVID) {
		String oldOuterVlanVID = outerVlanVID;
		outerVlanVID = newOuterVlanVID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID, oldOuterVlanVID, outerVlanVID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCRC() {
		return crc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCRC(String newCRC) {
		String oldCRC = crc;
		crc = newCRC;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__CRC, oldCRC, crc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getInnerVlanTPID() {
		return innerVlanTPID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInnerVlanTPID(String newInnerVlanTPID) {
		String oldInnerVlanTPID = innerVlanTPID;
		innerVlanTPID = newInnerVlanTPID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID, oldInnerVlanTPID, innerVlanTPID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getOuterVlanTPID() {
		return outerVlanTPID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setOuterVlanTPID(String newOuterVlanTPID) {
		String oldOuterVlanTPID = outerVlanTPID;
		outerVlanTPID = newOuterVlanTPID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID, oldOuterVlanTPID, outerVlanTPID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidSourceMAC(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_SOURCE_MAC,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidSourceMAC", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidDestinationMAC(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_DESTINATION_MAC,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidDestinationMAC", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidCRC(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_CRC,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidCRC", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidEtherType(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_ETHER_TYPE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidEtherType", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInnerVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_VID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInnerVlanVid", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOuterVlanVid(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_VID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOuterVlanVid", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInnerVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_CFI,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInnerVlanCFI", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOuterVlanCFI(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_CFI,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOuterVlanCFI", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInnerVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_PCP,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInnerVlanPCP", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOuterVlanPCP(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_PCP,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOuterVlanPCP", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidInnerVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_INNER_VLAN_TPID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidInnerVlanTPID", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isValid_hasValidOuterVlanTPID(DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 ConditionsValidator.DIAGNOSTIC_SOURCE,
						 ConditionsValidator.ETHERNET_FILTER__IS_VALID_HAS_VALID_OUTER_VLAN_TPID,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isValid_hasValidOuterVlanTPID", EObjectValidator.getObjectLabel(this, context) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
				return getSourceMAC();
			case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
				return getDestMAC();
			case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
				return getEtherType();
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
				return getInnerVlanId();
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
				return getOuterVlanId();
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
				return getInnerVlanCFI();
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
				return getOuterVlanCFI();
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
				return getInnerVlanVID();
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
				return getOuterVlanVID();
			case ConditionsPackage.ETHERNET_FILTER__CRC:
				return getCRC();
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
				return getInnerVlanTPID();
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
				return getOuterVlanTPID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
				setSourceMAC((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
				setDestMAC((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
				setEtherType((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
				setInnerVlanId((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
				setOuterVlanId((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
				setInnerVlanCFI((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
				setOuterVlanCFI((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
				setInnerVlanVID((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
				setOuterVlanVID((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__CRC:
				setCRC((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
				setInnerVlanTPID((String)newValue);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
				setOuterVlanTPID((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
				setSourceMAC(SOURCE_MAC_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
				setDestMAC(DEST_MAC_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
				setEtherType(ETHER_TYPE_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
				setInnerVlanId(INNER_VLAN_ID_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
				setOuterVlanId(OUTER_VLAN_ID_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
				setInnerVlanCFI(INNER_VLAN_CFI_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
				setOuterVlanCFI(OUTER_VLAN_CFI_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
				setInnerVlanVID(INNER_VLAN_VID_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
				setOuterVlanVID(OUTER_VLAN_VID_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__CRC:
				setCRC(CRC_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
				setInnerVlanTPID(INNER_VLAN_TPID_EDEFAULT);
				return;
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
				setOuterVlanTPID(OUTER_VLAN_TPID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.ETHERNET_FILTER__SOURCE_MAC:
				return SOURCE_MAC_EDEFAULT == null ? sourceMAC != null : !SOURCE_MAC_EDEFAULT.equals(sourceMAC);
			case ConditionsPackage.ETHERNET_FILTER__DEST_MAC:
				return DEST_MAC_EDEFAULT == null ? destMAC != null : !DEST_MAC_EDEFAULT.equals(destMAC);
			case ConditionsPackage.ETHERNET_FILTER__ETHER_TYPE:
				return ETHER_TYPE_EDEFAULT == null ? etherType != null : !ETHER_TYPE_EDEFAULT.equals(etherType);
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_ID:
				return INNER_VLAN_ID_EDEFAULT == null ? innerVlanId != null : !INNER_VLAN_ID_EDEFAULT.equals(innerVlanId);
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_ID:
				return OUTER_VLAN_ID_EDEFAULT == null ? outerVlanId != null : !OUTER_VLAN_ID_EDEFAULT.equals(outerVlanId);
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_CFI:
				return INNER_VLAN_CFI_EDEFAULT == null ? innerVlanCFI != null : !INNER_VLAN_CFI_EDEFAULT.equals(innerVlanCFI);
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_CFI:
				return OUTER_VLAN_CFI_EDEFAULT == null ? outerVlanCFI != null : !OUTER_VLAN_CFI_EDEFAULT.equals(outerVlanCFI);
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_VID:
				return INNER_VLAN_VID_EDEFAULT == null ? innerVlanVID != null : !INNER_VLAN_VID_EDEFAULT.equals(innerVlanVID);
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_VID:
				return OUTER_VLAN_VID_EDEFAULT == null ? outerVlanVID != null : !OUTER_VLAN_VID_EDEFAULT.equals(outerVlanVID);
			case ConditionsPackage.ETHERNET_FILTER__CRC:
				return CRC_EDEFAULT == null ? crc != null : !CRC_EDEFAULT.equals(crc);
			case ConditionsPackage.ETHERNET_FILTER__INNER_VLAN_TPID:
				return INNER_VLAN_TPID_EDEFAULT == null ? innerVlanTPID != null : !INNER_VLAN_TPID_EDEFAULT.equals(innerVlanTPID);
			case ConditionsPackage.ETHERNET_FILTER__OUTER_VLAN_TPID:
				return OUTER_VLAN_TPID_EDEFAULT == null ? outerVlanTPID != null : !OUTER_VLAN_TPID_EDEFAULT.equals(outerVlanTPID);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_SOURCE_MAC__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidSourceMAC((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_DESTINATION_MAC__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidDestinationMAC((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_CRC__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidCRC((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_ETHER_TYPE__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidEtherType((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_VID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInnerVlanVid((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_VID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOuterVlanVid((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_CFI__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInnerVlanCFI((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_CFI__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOuterVlanCFI((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_PCP__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInnerVlanPCP((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_PCP__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOuterVlanPCP((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_INNER_VLAN_TPID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidInnerVlanTPID((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
			case ConditionsPackage.ETHERNET_FILTER___IS_VALID_HAS_VALID_OUTER_VLAN_TPID__DIAGNOSTICCHAIN_MAP:
				return isValid_hasValidOuterVlanTPID((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (sourceMAC: ");
		result.append(sourceMAC);
		result.append(", destMAC: ");
		result.append(destMAC);
		result.append(", etherType: ");
		result.append(etherType);
		result.append(", innerVlanId: ");
		result.append(innerVlanId);
		result.append(", outerVlanId: ");
		result.append(outerVlanId);
		result.append(", innerVlanCFI: ");
		result.append(innerVlanCFI);
		result.append(", outerVlanCFI: ");
		result.append(outerVlanCFI);
		result.append(", innerVlanVID: ");
		result.append(innerVlanVID);
		result.append(", outerVlanVID: ");
		result.append(outerVlanVID);
		result.append(", CRC: ");
		result.append(crc);
		result.append(", innerVlanTPID: ");
		result.append(innerVlanTPID);
		result.append(", outerVlanTPID: ");
		result.append(outerVlanTPID);
		result.append(')');
		return result.toString();
	}

} //EthernetFilterImpl
