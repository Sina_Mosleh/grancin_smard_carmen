/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.DataType;
import conditions.IOperand;
import conditions.IStringOperand;
import conditions.Substring;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Substring</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link conditions.impl.SubstringImpl#getStringToExtract <em>String To Extract</em>}</li>
 *   <li>{@link conditions.impl.SubstringImpl#getStartIndex <em>Start Index</em>}</li>
 *   <li>{@link conditions.impl.SubstringImpl#getEndIndex <em>End Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SubstringImpl extends MinimalEObjectImpl.Container implements Substring {
	/**
	 * The cached value of the '{@link #getStringToExtract() <em>String To Extract</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringToExtract()
	 * @generated
	 * @ordered
	 */
	protected IStringOperand stringToExtract;

	/**
	 * The default value of the '{@link #getStartIndex() <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int START_INDEX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getStartIndex() <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartIndex()
	 * @generated
	 * @ordered
	 */
	protected int startIndex = START_INDEX_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndIndex() <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int END_INDEX_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getEndIndex() <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndIndex()
	 * @generated
	 * @ordered
	 */
	protected int endIndex = END_INDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SubstringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getSubstring();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public IStringOperand getStringToExtract() {
		return stringToExtract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringToExtract(IStringOperand newStringToExtract, NotificationChain msgs) {
		IStringOperand oldStringToExtract = stringToExtract;
		stringToExtract = newStringToExtract;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT, oldStringToExtract, newStringToExtract);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStringToExtract(IStringOperand newStringToExtract) {
		if (newStringToExtract != stringToExtract) {
			NotificationChain msgs = null;
			if (stringToExtract != null)
				msgs = ((InternalEObject)stringToExtract).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT, null, msgs);
			if (newStringToExtract != null)
				msgs = ((InternalEObject)newStringToExtract).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT, null, msgs);
			msgs = basicSetStringToExtract(newStringToExtract, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT, newStringToExtract, newStringToExtract));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setStartIndex(int newStartIndex) {
		int oldStartIndex = startIndex;
		startIndex = newStartIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SUBSTRING__START_INDEX, oldStartIndex, startIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getEndIndex() {
		return endIndex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setEndIndex(int newEndIndex) {
		int oldEndIndex = endIndex;
		endIndex = newEndIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SUBSTRING__END_INDEX, oldEndIndex, endIndex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_OperandDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IOperand> get_Operands() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public DataType get_EvaluationDataType() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getReadVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<String> getWriteVariables() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT:
				return basicSetStringToExtract(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT:
				return getStringToExtract();
			case ConditionsPackage.SUBSTRING__START_INDEX:
				return getStartIndex();
			case ConditionsPackage.SUBSTRING__END_INDEX:
				return getEndIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT:
				setStringToExtract((IStringOperand)newValue);
				return;
			case ConditionsPackage.SUBSTRING__START_INDEX:
				setStartIndex((Integer)newValue);
				return;
			case ConditionsPackage.SUBSTRING__END_INDEX:
				setEndIndex((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT:
				setStringToExtract((IStringOperand)null);
				return;
			case ConditionsPackage.SUBSTRING__START_INDEX:
				setStartIndex(START_INDEX_EDEFAULT);
				return;
			case ConditionsPackage.SUBSTRING__END_INDEX:
				setEndIndex(END_INDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConditionsPackage.SUBSTRING__STRING_TO_EXTRACT:
				return stringToExtract != null;
			case ConditionsPackage.SUBSTRING__START_INDEX:
				return startIndex != START_INDEX_EDEFAULT;
			case ConditionsPackage.SUBSTRING__END_INDEX:
				return endIndex != END_INDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConditionsPackage.SUBSTRING___GET_OPERAND_DATA_TYPE:
				return get_OperandDataType();
			case ConditionsPackage.SUBSTRING___GET_OPERANDS:
				return get_Operands();
			case ConditionsPackage.SUBSTRING___GET_EVALUATION_DATA_TYPE:
				return get_EvaluationDataType();
			case ConditionsPackage.SUBSTRING___GET_READ_VARIABLES:
				return getReadVariables();
			case ConditionsPackage.SUBSTRING___GET_WRITE_VARIABLES:
				return getWriteVariables();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startIndex: ");
		result.append(startIndex);
		result.append(", endIndex: ");
		result.append(endIndex);
		result.append(')');
		return result.toString();
	}

} //SubstringImpl
