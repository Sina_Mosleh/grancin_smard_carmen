/**
 */
package conditions.impl;

import conditions.ConditionsPackage;
import conditions.StringSignal;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringSignalImpl extends AbstractSignalImpl implements StringSignal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringSignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConditionsPackage.eINSTANCE.getStringSignal();
	}

} //StringSignalImpl
