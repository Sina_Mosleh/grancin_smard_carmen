/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Length</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.StringLength#getStringOperand <em>String Operand</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getStringLength()
 * @model
 * @generated
 */
public interface StringLength extends INumericOperation {
	/**
	 * Returns the value of the '<em><b>String Operand</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Operand</em>' containment reference.
	 * @see #setStringOperand(IStringOperand)
	 * @see conditions.ConditionsPackage#getStringLength_StringOperand()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IStringOperand getStringOperand();

	/**
	 * Sets the value of the '{@link conditions.StringLength#getStringOperand <em>String Operand</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Operand</em>' containment reference.
	 * @see #getStringOperand()
	 * @generated
	 */
	void setStringOperand(IStringOperand value);

} // StringLength
