/**
 */
package conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import statemachine.AbstractState;
import statemachine.Transition;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IState Transition Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getIStateTransitionReference()
 * @model abstract="true"
 * @generated
 */
public interface IStateTransitionReference extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<AbstractState> getStateDependencies();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Transition> getTransitionDependencies();

} // IStateTransitionReference
