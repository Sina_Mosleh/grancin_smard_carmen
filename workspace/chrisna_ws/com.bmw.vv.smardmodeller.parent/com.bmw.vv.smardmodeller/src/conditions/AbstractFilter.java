/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.AbstractFilter#getPayloadLength <em>Payload Length</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getAbstractFilter()
 * @model abstract="true"
 * @generated
 */
public interface AbstractFilter extends BaseClassWithSourceReference {
	/**
	 * Returns the value of the '<em><b>Payload Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payload Length</em>' attribute.
	 * @see conditions.ConditionsPackage#getAbstractFilter_PayloadLength()
	 * @model dataType="conditions.LongOrTemplatePlaceholder" transient="true" changeable="false" derived="true"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getPayloadLength();

} // AbstractFilter
