/**
 */
package conditions;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ConditionSet#getGroup <em>Group</em>}</li>
 *   <li>{@link conditions.ConditionSet#getBaureihe <em>Baureihe</em>}</li>
 *   <li>{@link conditions.ConditionSet#getDescription <em>Description</em>}</li>
 *   <li>{@link conditions.ConditionSet#getIstufe <em>Istufe</em>}</li>
 *   <li>{@link conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}</li>
 *   <li>{@link conditions.ConditionSet#getUuid <em>Uuid</em>}</li>
 *   <li>{@link conditions.ConditionSet#getVersion <em>Version</em>}</li>
 *   <li>{@link conditions.ConditionSet#getConditions <em>Conditions</em>}</li>
 *   <li>{@link conditions.ConditionSet#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getConditionSet()
 * @model extendedMetaData="name='conditionSet' kind='elementOnly'"
 * @generated
 */
public interface ConditionSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see conditions.ConditionsPackage#getConditionSet_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Baureihe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Baureihe</em>' attribute.
	 * @see #setBaureihe(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Baureihe()
	 * @model extendedMetaData="kind='attribute' name='baureihe' namespace='##targetNamespace'"
	 * @generated
	 */
	String getBaureihe();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getBaureihe <em>Baureihe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Baureihe</em>' attribute.
	 * @see #getBaureihe()
	 * @generated
	 */
	void setBaureihe(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Description()
	 * @model extendedMetaData="kind='attribute' name='description' namespace='##targetNamespace'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Istufe</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Istufe</em>' attribute.
	 * @see #setIstufe(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Istufe()
	 * @model extendedMetaData="kind='attribute' name='istufe' namespace='##targetNamespace'"
	 * @generated
	 */
	String getIstufe();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getIstufe <em>Istufe</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Istufe</em>' attribute.
	 * @see #getIstufe()
	 * @generated
	 */
	void setIstufe(String value);

	/**
	 * Returns the value of the '<em><b>Schemaversion</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schemaversion</em>' attribute.
	 * @see #isSetSchemaversion()
	 * @see #unsetSchemaversion()
	 * @see #setSchemaversion(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Schemaversion()
	 * @model default="0" unsettable="true"
	 *        extendedMetaData="kind='attribute' name='schemaversion' namespace='##targetNamespace'"
	 * @generated
	 */
	String getSchemaversion();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Schemaversion</em>' attribute.
	 * @see #isSetSchemaversion()
	 * @see #unsetSchemaversion()
	 * @see #getSchemaversion()
	 * @generated
	 */
	void setSchemaversion(String value);

	/**
	 * Unsets the value of the '{@link conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSchemaversion()
	 * @see #getSchemaversion()
	 * @see #setSchemaversion(String)
	 * @generated
	 */
	void unsetSchemaversion();

	/**
	 * Returns whether the value of the '{@link conditions.ConditionSet#getSchemaversion <em>Schemaversion</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Schemaversion</em>' attribute is set.
	 * @see #unsetSchemaversion()
	 * @see #getSchemaversion()
	 * @see #setSchemaversion(String)
	 * @generated
	 */
	boolean isSetSchemaversion();

	/**
	 * Returns the value of the '<em><b>Uuid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uuid</em>' attribute.
	 * @see #setUuid(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Uuid()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID"
	 *        extendedMetaData="kind='attribute' name='uuid' namespace='##targetNamespace'"
	 * @generated
	 */
	String getUuid();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getUuid <em>Uuid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uuid</em>' attribute.
	 * @see #getUuid()
	 * @generated
	 */
	void setUuid(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Version()
	 * @model extendedMetaData="kind='attribute' name='version' namespace='##targetNamespace'"
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.Condition}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conditions</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getConditionSet_Conditions()
	 * @model containment="true"
	 *        extendedMetaData="kind='element'"
	 * @generated
	 */
	EList<Condition> getConditions();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see conditions.ConditionsPackage#getConditionSet_Name()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link conditions.ConditionSet#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // ConditionSet
