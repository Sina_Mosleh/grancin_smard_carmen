/**
 *
 * $Id$
 */
package conditions.validation;

import conditions.BooleanOrTemplatePlaceholderEnum;

import statemachine.Transition;

/**
 * A sample validator interface for {@link conditions.TransitionCheckExpression}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface TransitionCheckExpressionValidator {
	boolean validate();

	boolean validateStayActive(BooleanOrTemplatePlaceholderEnum value);
	boolean validateCheckTransition(Transition value);
	boolean validateStayActiveTmplParam(String value);
}
