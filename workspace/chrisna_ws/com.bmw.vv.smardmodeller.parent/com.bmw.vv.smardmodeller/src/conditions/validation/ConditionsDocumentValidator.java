/**
 *
 * $Id$
 */
package conditions.validation;

import conditions.ConditionSet;
import conditions.SignalReferenceSet;
import conditions.VariableSet;

/**
 * A sample validator interface for {@link conditions.ConditionsDocument}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface ConditionsDocumentValidator {
	boolean validate();

	boolean validateSignalReferencesSet(SignalReferenceSet value);
	boolean validateVariableSet(VariableSet value);
	boolean validateConditionSet(ConditionSet value);
}
