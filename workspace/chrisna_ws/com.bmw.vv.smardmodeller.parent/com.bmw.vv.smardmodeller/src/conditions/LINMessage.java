/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LIN Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.LINMessage#getLinFilter <em>Lin Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getLINMessage()
 * @model
 * @generated
 */
public interface LINMessage extends AbstractBusMessage {
	/**
	 * Returns the value of the '<em><b>Lin Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lin Filter</em>' containment reference.
	 * @see #setLinFilter(LINFilter)
	 * @see conditions.ConditionsPackage#getLINMessage_LinFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	LINFilter getLinFilter();

	/**
	 * Sets the value of the '{@link conditions.LINMessage#getLinFilter <em>Lin Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lin Filter</em>' containment reference.
	 * @see #getLinFilter()
	 * @generated
	 */
	void setLinFilter(LINFilter value);

} // LINMessage
