/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flex Ray Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getBusId <em>Bus Id</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getPayloadPreamble <em>Payload Preamble</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getZeroFrame <em>Zero Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getSyncFrame <em>Sync Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getStartupFrame <em>Startup Frame</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmt <em>Network Mgmt</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getFlexrayMessageId <em>Flexray Message Id</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam <em>Payload Preamble Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam <em>Zero Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam <em>Sync Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam <em>Startup Frame Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam <em>Network Mgmt Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getType <em>Type</em>}</li>
 *   <li>{@link conditions.FlexRayMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression()
 * @model
 * @generated
 */
public interface FlexRayMessageCheckExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Bus Id</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus Id</em>' attribute.
	 * @see #setBusId(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_BusId()
	 * @model default="0" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getBusId();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getBusId <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus Id</em>' attribute.
	 * @see #getBusId()
	 * @generated
	 */
	void setBusId(String value);

	/**
	 * Returns the value of the '<em><b>Payload Preamble</b></em>' attribute.
	 * The default value is <code>"ignore"</code>.
	 * The literals are from the enumeration {@link conditions.FlexRayHeaderFlagSelection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payload Preamble</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #setPayloadPreamble(FlexRayHeaderFlagSelection)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_PayloadPreamble()
	 * @model default="ignore"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	FlexRayHeaderFlagSelection getPayloadPreamble();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getPayloadPreamble <em>Payload Preamble</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Payload Preamble</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #getPayloadPreamble()
	 * @generated
	 */
	void setPayloadPreamble(FlexRayHeaderFlagSelection value);

	/**
	 * Returns the value of the '<em><b>Zero Frame</b></em>' attribute.
	 * The default value is <code>"ignore"</code>.
	 * The literals are from the enumeration {@link conditions.FlexRayHeaderFlagSelection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zero Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #setZeroFrame(FlexRayHeaderFlagSelection)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_ZeroFrame()
	 * @model default="ignore"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	FlexRayHeaderFlagSelection getZeroFrame();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getZeroFrame <em>Zero Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zero Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #getZeroFrame()
	 * @generated
	 */
	void setZeroFrame(FlexRayHeaderFlagSelection value);

	/**
	 * Returns the value of the '<em><b>Sync Frame</b></em>' attribute.
	 * The default value is <code>"ignore"</code>.
	 * The literals are from the enumeration {@link conditions.FlexRayHeaderFlagSelection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #setSyncFrame(FlexRayHeaderFlagSelection)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_SyncFrame()
	 * @model default="ignore"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	FlexRayHeaderFlagSelection getSyncFrame();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getSyncFrame <em>Sync Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #getSyncFrame()
	 * @generated
	 */
	void setSyncFrame(FlexRayHeaderFlagSelection value);

	/**
	 * Returns the value of the '<em><b>Startup Frame</b></em>' attribute.
	 * The default value is <code>"ignore"</code>.
	 * The literals are from the enumeration {@link conditions.FlexRayHeaderFlagSelection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Startup Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #setStartupFrame(FlexRayHeaderFlagSelection)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_StartupFrame()
	 * @model default="ignore"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	FlexRayHeaderFlagSelection getStartupFrame();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getStartupFrame <em>Startup Frame</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Startup Frame</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #getStartupFrame()
	 * @generated
	 */
	void setStartupFrame(FlexRayHeaderFlagSelection value);

	/**
	 * Returns the value of the '<em><b>Network Mgmt</b></em>' attribute.
	 * The default value is <code>"ignore"</code>.
	 * The literals are from the enumeration {@link conditions.FlexRayHeaderFlagSelection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Mgmt</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #setNetworkMgmt(FlexRayHeaderFlagSelection)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_NetworkMgmt()
	 * @model default="ignore"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	FlexRayHeaderFlagSelection getNetworkMgmt();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmt <em>Network Mgmt</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network Mgmt</em>' attribute.
	 * @see conditions.FlexRayHeaderFlagSelection
	 * @see #getNetworkMgmt()
	 * @generated
	 */
	void setNetworkMgmt(FlexRayHeaderFlagSelection value);

	/**
	 * Returns the value of the '<em><b>Flexray Message Id</b></em>' attribute.
	 * The default value is <code>"<Channel>.<SlotID>.<Offset>.<Repetition>"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Flexray Message Id</em>' attribute.
	 * @see #setFlexrayMessageId(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_FlexrayMessageId()
	 * @model default="&lt;Channel&gt;.&lt;SlotID&gt;.&lt;Offset&gt;.&lt;Repetition&gt;"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getFlexrayMessageId();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getFlexrayMessageId <em>Flexray Message Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Flexray Message Id</em>' attribute.
	 * @see #getFlexrayMessageId()
	 * @generated
	 */
	void setFlexrayMessageId(String value);

	/**
	 * Returns the value of the '<em><b>Payload Preamble Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Payload Preamble Tmpl Param</em>' attribute.
	 * @see #setPayloadPreambleTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_PayloadPreambleTmplParam()
	 * @model
	 * @generated
	 */
	String getPayloadPreambleTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getPayloadPreambleTmplParam <em>Payload Preamble Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Payload Preamble Tmpl Param</em>' attribute.
	 * @see #getPayloadPreambleTmplParam()
	 * @generated
	 */
	void setPayloadPreambleTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Zero Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Zero Frame Tmpl Param</em>' attribute.
	 * @see #setZeroFrameTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_ZeroFrameTmplParam()
	 * @model
	 * @generated
	 */
	String getZeroFrameTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getZeroFrameTmplParam <em>Zero Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Zero Frame Tmpl Param</em>' attribute.
	 * @see #getZeroFrameTmplParam()
	 * @generated
	 */
	void setZeroFrameTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Sync Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sync Frame Tmpl Param</em>' attribute.
	 * @see #setSyncFrameTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_SyncFrameTmplParam()
	 * @model
	 * @generated
	 */
	String getSyncFrameTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getSyncFrameTmplParam <em>Sync Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sync Frame Tmpl Param</em>' attribute.
	 * @see #getSyncFrameTmplParam()
	 * @generated
	 */
	void setSyncFrameTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Startup Frame Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Startup Frame Tmpl Param</em>' attribute.
	 * @see #setStartupFrameTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_StartupFrameTmplParam()
	 * @model
	 * @generated
	 */
	String getStartupFrameTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getStartupFrameTmplParam <em>Startup Frame Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Startup Frame Tmpl Param</em>' attribute.
	 * @see #getStartupFrameTmplParam()
	 * @generated
	 */
	void setStartupFrameTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Network Mgmt Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Network Mgmt Tmpl Param</em>' attribute.
	 * @see #setNetworkMgmtTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_NetworkMgmtTmplParam()
	 * @model
	 * @generated
	 */
	String getNetworkMgmtTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getNetworkMgmtTmplParam <em>Network Mgmt Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Network Mgmt Tmpl Param</em>' attribute.
	 * @see #getNetworkMgmtTmplParam()
	 * @generated
	 */
	void setNetworkMgmtTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus Id Tmpl Param</em>' attribute.
	 * @see #setBusIdTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_BusIdTmplParam()
	 * @model
	 * @generated
	 */
	String getBusIdTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus Id Tmpl Param</em>' attribute.
	 * @see #getBusIdTmplParam()
	 * @generated
	 */
	void setBusIdTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"ANY"</code>.
	 * The literals are from the enumeration {@link conditions.CheckType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see conditions.CheckType
	 * @see #setType(CheckType)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_Type()
	 * @model default="ANY" required="true"
	 *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	CheckType getType();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see conditions.CheckType
	 * @see #getType()
	 * @generated
	 */
	void setType(CheckType value);

	/**
	 * Returns the value of the '<em><b>Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Tmpl Param</em>' attribute.
	 * @see #setTypeTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayMessageCheckExpression_TypeTmplParam()
	 * @model
	 * @generated
	 */
	String getTypeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Tmpl Param</em>' attribute.
	 * @see #getTypeTmplParam()
	 * @generated
	 */
	void setTypeTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidStartupFrame(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSyncFrame(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidZeroFrame(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidPayloadPreamble(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidNetworkMgmt(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidMessageId(DiagnosticChain diagnostics, Map<Object, Object> context);

} // FlexRayMessageCheckExpression
