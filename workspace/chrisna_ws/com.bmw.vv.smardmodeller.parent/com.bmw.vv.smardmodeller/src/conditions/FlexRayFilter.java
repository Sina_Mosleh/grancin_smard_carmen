/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flex Ray Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#getChannel <em>Channel</em>}</li>
 *   <li>{@link conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getFlexRayFilter()
 * @model
 * @generated
 */
public interface FlexRayFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Id Range</em>' attribute.
	 * @see #setMessageIdRange(String)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_MessageIdRange()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getMessageIdRange();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Id Range</em>' attribute.
	 * @see #getMessageIdRange()
	 * @generated
	 */
	void setMessageIdRange(String value);

	/**
	 * Returns the value of the '<em><b>Slot Id</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slot Id</em>' attribute.
	 * @see #setSlotId(String)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_SlotId()
	 * @model default="0" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSlotId();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slot Id</em>' attribute.
	 * @see #getSlotId()
	 * @generated
	 */
	void setSlotId(String value);

	/**
	 * Returns the value of the '<em><b>Cycle Offset</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cycle Offset</em>' attribute.
	 * @see #setCycleOffset(String)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_CycleOffset()
	 * @model default="0" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getCycleOffset();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cycle Offset</em>' attribute.
	 * @see #getCycleOffset()
	 * @generated
	 */
	void setCycleOffset(String value);

	/**
	 * Returns the value of the '<em><b>Cycle Repetition</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cycle Repetition</em>' attribute.
	 * @see #setCycleRepetition(String)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_CycleRepetition()
	 * @model default="1" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getCycleRepetition();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cycle Repetition</em>' attribute.
	 * @see #getCycleRepetition()
	 * @generated
	 */
	void setCycleRepetition(String value);

	/**
	 * Returns the value of the '<em><b>Channel</b></em>' attribute.
	 * The default value is <code>"A"</code>.
	 * The literals are from the enumeration {@link conditions.FlexChannelType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Channel</em>' attribute.
	 * @see conditions.FlexChannelType
	 * @see #setChannel(FlexChannelType)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_Channel()
	 * @model default="A"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	FlexChannelType getChannel();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getChannel <em>Channel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Channel</em>' attribute.
	 * @see conditions.FlexChannelType
	 * @see #getChannel()
	 * @generated
	 */
	void setChannel(FlexChannelType value);

	/**
	 * Returns the value of the '<em><b>Channel Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Channel Tmpl Param</em>' attribute.
	 * @see #setChannelTmplParam(String)
	 * @see conditions.ConditionsPackage#getFlexRayFilter_ChannelTmplParam()
	 * @model
	 * @generated
	 */
	String getChannelTmplParam();

	/**
	 * Sets the value of the '{@link conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Channel Tmpl Param</em>' attribute.
	 * @see #getChannelTmplParam()
	 * @generated
	 */
	void setChannelTmplParam(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidChannelType(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidCycleOffset(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidCycleRepeatInterval(DiagnosticChain diagnostics, Map<Object, Object> context);

} // FlexRayFilter
