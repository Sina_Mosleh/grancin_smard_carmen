/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Substring</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.Substring#getStringToExtract <em>String To Extract</em>}</li>
 *   <li>{@link conditions.Substring#getStartIndex <em>Start Index</em>}</li>
 *   <li>{@link conditions.Substring#getEndIndex <em>End Index</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getSubstring()
 * @model
 * @generated
 */
public interface Substring extends IStringOperation {
	/**
	 * Returns the value of the '<em><b>String To Extract</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String To Extract</em>' containment reference.
	 * @see #setStringToExtract(IStringOperand)
	 * @see conditions.ConditionsPackage#getSubstring_StringToExtract()
	 * @model containment="true" required="true"
	 * @generated
	 */
	IStringOperand getStringToExtract();

	/**
	 * Sets the value of the '{@link conditions.Substring#getStringToExtract <em>String To Extract</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String To Extract</em>' containment reference.
	 * @see #getStringToExtract()
	 * @generated
	 */
	void setStringToExtract(IStringOperand value);

	/**
	 * Returns the value of the '<em><b>Start Index</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Index</em>' attribute.
	 * @see #setStartIndex(int)
	 * @see conditions.ConditionsPackage#getSubstring_StartIndex()
	 * @model default="0"
	 * @generated
	 */
	int getStartIndex();

	/**
	 * Sets the value of the '{@link conditions.Substring#getStartIndex <em>Start Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Index</em>' attribute.
	 * @see #getStartIndex()
	 * @generated
	 */
	void setStartIndex(int value);

	/**
	 * Returns the value of the '<em><b>End Index</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Index</em>' attribute.
	 * @see #setEndIndex(int)
	 * @see conditions.ConditionsPackage#getSubstring_EndIndex()
	 * @model default="-1"
	 * @generated
	 */
	int getEndIndex();

	/**
	 * Sets the value of the '{@link conditions.Substring#getEndIndex <em>End Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Index</em>' attribute.
	 * @see #getEndIndex()
	 * @generated
	 */
	void setEndIndex(int value);

} // Substring
