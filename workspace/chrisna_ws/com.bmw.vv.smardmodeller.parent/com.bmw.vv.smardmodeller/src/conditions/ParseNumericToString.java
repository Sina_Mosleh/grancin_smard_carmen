/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parse Numeric To String</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.ParseNumericToString#getNumericOperandToBeParsed <em>Numeric Operand To Be Parsed</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getParseNumericToString()
 * @model
 * @generated
 */
public interface ParseNumericToString extends IStringOperation {
	/**
	 * Returns the value of the '<em><b>Numeric Operand To Be Parsed</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numeric Operand To Be Parsed</em>' containment reference.
	 * @see #setNumericOperandToBeParsed(INumericOperand)
	 * @see conditions.ConditionsPackage#getParseNumericToString_NumericOperandToBeParsed()
	 * @model containment="true" required="true"
	 * @generated
	 */
	INumericOperand getNumericOperandToBeParsed();

	/**
	 * Sets the value of the '{@link conditions.ParseNumericToString#getNumericOperandToBeParsed <em>Numeric Operand To Be Parsed</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Numeric Operand To Be Parsed</em>' containment reference.
	 * @see #getNumericOperandToBeParsed()
	 * @generated
	 */
	void setNumericOperandToBeParsed(INumericOperand value);

} // ParseNumericToString
