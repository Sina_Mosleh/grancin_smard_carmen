/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.DoubleDecodeStrategy#getFactor <em>Factor</em>}</li>
 *   <li>{@link conditions.DoubleDecodeStrategy#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getDoubleDecodeStrategy()
 * @model
 * @generated
 */
public interface DoubleDecodeStrategy extends DecodeStrategy {
	/**
	 * Returns the value of the '<em><b>Factor</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Factor</em>' attribute.
	 * @see #setFactor(String)
	 * @see conditions.ConditionsPackage#getDoubleDecodeStrategy_Factor()
	 * @model default="1.0" dataType="conditions.DoubleOrHexOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getFactor();

	/**
	 * Sets the value of the '{@link conditions.DoubleDecodeStrategy#getFactor <em>Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Factor</em>' attribute.
	 * @see #getFactor()
	 * @generated
	 */
	void setFactor(String value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(String)
	 * @see conditions.ConditionsPackage#getDoubleDecodeStrategy_Offset()
	 * @model default="0.0" dataType="conditions.DoubleOrHexOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getOffset();

	/**
	 * Sets the value of the '{@link conditions.DoubleDecodeStrategy#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidFactor(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidOffset(DiagnosticChain diagnostics, Map<Object, Object> context);

} // DoubleDecodeStrategy
