/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UDPNM Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.UDPNMFilter#getSourceNodeIdentifier <em>Source Node Identifier</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#getControlBitVector <em>Control Bit Vector</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#getPwfStatus <em>Pwf Status</em>}</li>
 *   <li>{@link conditions.UDPNMFilter#getTeilnetzStatus <em>Teilnetz Status</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getUDPNMFilter()
 * @model
 * @generated
 */
public interface UDPNMFilter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Source Node Identifier</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Node Identifier</em>' attribute.
	 * @see #setSourceNodeIdentifier(String)
	 * @see conditions.ConditionsPackage#getUDPNMFilter_SourceNodeIdentifier()
	 * @model default="" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSourceNodeIdentifier();

	/**
	 * Sets the value of the '{@link conditions.UDPNMFilter#getSourceNodeIdentifier <em>Source Node Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Node Identifier</em>' attribute.
	 * @see #getSourceNodeIdentifier()
	 * @generated
	 */
	void setSourceNodeIdentifier(String value);

	/**
	 * Returns the value of the '<em><b>Control Bit Vector</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control Bit Vector</em>' attribute.
	 * @see #setControlBitVector(String)
	 * @see conditions.ConditionsPackage#getUDPNMFilter_ControlBitVector()
	 * @model default="" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getControlBitVector();

	/**
	 * Sets the value of the '{@link conditions.UDPNMFilter#getControlBitVector <em>Control Bit Vector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Control Bit Vector</em>' attribute.
	 * @see #getControlBitVector()
	 * @generated
	 */
	void setControlBitVector(String value);

	/**
	 * Returns the value of the '<em><b>Pwf Status</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pwf Status</em>' attribute.
	 * @see #setPwfStatus(String)
	 * @see conditions.ConditionsPackage#getUDPNMFilter_PwfStatus()
	 * @model default="" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getPwfStatus();

	/**
	 * Sets the value of the '{@link conditions.UDPNMFilter#getPwfStatus <em>Pwf Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pwf Status</em>' attribute.
	 * @see #getPwfStatus()
	 * @generated
	 */
	void setPwfStatus(String value);

	/**
	 * Returns the value of the '<em><b>Teilnetz Status</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teilnetz Status</em>' attribute.
	 * @see #setTeilnetzStatus(String)
	 * @see conditions.ConditionsPackage#getUDPNMFilter_TeilnetzStatus()
	 * @model default="" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getTeilnetzStatus();

	/**
	 * Sets the value of the '{@link conditions.UDPNMFilter#getTeilnetzStatus <em>Teilnetz Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Teilnetz Status</em>' attribute.
	 * @see #getTeilnetzStatus()
	 * @generated
	 */
	void setTeilnetzStatus(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSourceNodeIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidControlBitVector(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidPwfStatus(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidTeilnetzStatus(DiagnosticChain diagnostics, Map<Object, Object> context);

} // UDPNMFilter
