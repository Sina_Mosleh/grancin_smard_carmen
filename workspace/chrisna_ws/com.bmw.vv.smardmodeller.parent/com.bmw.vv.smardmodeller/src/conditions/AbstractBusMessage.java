/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Bus Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}</li>
 *   <li>{@link conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 *   <li>{@link conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}</li>
 *   <li>{@link conditions.AbstractBusMessage#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getAbstractBusMessage()
 * @model abstract="true"
 * @generated
 */
public interface AbstractBusMessage extends AbstractMessage {
	/**
	 * Returns the value of the '<em><b>Bus Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus Id</em>' attribute.
	 * @see #setBusId(String)
	 * @see conditions.ConditionsPackage#getAbstractBusMessage_BusId()
	 * @model dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getBusId();

	/**
	 * Sets the value of the '{@link conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus Id</em>' attribute.
	 * @see #getBusId()
	 * @generated
	 */
	void setBusId(String value);

	/**
	 * Returns the value of the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus Id Tmpl Param</em>' attribute.
	 * @see #setBusIdTmplParam(String)
	 * @see conditions.ConditionsPackage#getAbstractBusMessage_BusIdTmplParam()
	 * @model
	 * @generated
	 */
	String getBusIdTmplParam();

	/**
	 * Sets the value of the '{@link conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus Id Tmpl Param</em>' attribute.
	 * @see #getBusIdTmplParam()
	 * @generated
	 */
	void setBusIdTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Bus Id Range</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus Id Range</em>' attribute.
	 * @see #setBusIdRange(String)
	 * @see conditions.ConditionsPackage#getAbstractBusMessage_BusIdRange()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getBusIdRange();

	/**
	 * Sets the value of the '{@link conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus Id Range</em>' attribute.
	 * @see #getBusIdRange()
	 * @generated
	 */
	void setBusIdRange(String value);

	/**
	 * Returns the value of the '<em><b>Filters</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.AbstractFilter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filters</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getAbstractBusMessage_Filters()
	 * @model containment="true"
	 * @generated
	 */
	EList<AbstractFilter> getFilters();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_isOsiLayerConform(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AbstractBusMessage
