/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getEmptyDecodeStrategy()
 * @model
 * @generated
 */
public interface EmptyDecodeStrategy extends DecodeStrategy {
} // EmptyDecodeStrategy
