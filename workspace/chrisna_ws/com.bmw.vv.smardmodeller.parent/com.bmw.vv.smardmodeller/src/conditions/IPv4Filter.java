/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPv4 Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.IPv4Filter#getProtocolType <em>Protocol Type</em>}</li>
 *   <li>{@link conditions.IPv4Filter#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}</li>
 *   <li>{@link conditions.IPv4Filter#getSourceIP <em>Source IP</em>}</li>
 *   <li>{@link conditions.IPv4Filter#getDestIP <em>Dest IP</em>}</li>
 *   <li>{@link conditions.IPv4Filter#getTimeToLive <em>Time To Live</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getIPv4Filter()
 * @model
 * @generated
 */
public interface IPv4Filter extends AbstractFilter {
	/**
	 * Returns the value of the '<em><b>Protocol Type</b></em>' attribute.
	 * The default value is <code>"ALL"</code>.
	 * The literals are from the enumeration {@link conditions.ProtocolTypeOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Type</em>' attribute.
	 * @see conditions.ProtocolTypeOrTemplatePlaceholderEnum
	 * @see #setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getIPv4Filter_ProtocolType()
	 * @model default="ALL"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	ProtocolTypeOrTemplatePlaceholderEnum getProtocolType();

	/**
	 * Sets the value of the '{@link conditions.IPv4Filter#getProtocolType <em>Protocol Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Type</em>' attribute.
	 * @see conditions.ProtocolTypeOrTemplatePlaceholderEnum
	 * @see #getProtocolType()
	 * @generated
	 */
	void setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>Protocol Type Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Protocol Type Tmpl Param</em>' attribute.
	 * @see #setProtocolTypeTmplParam(String)
	 * @see conditions.ConditionsPackage#getIPv4Filter_ProtocolTypeTmplParam()
	 * @model
	 * @generated
	 */
	String getProtocolTypeTmplParam();

	/**
	 * Sets the value of the '{@link conditions.IPv4Filter#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Protocol Type Tmpl Param</em>' attribute.
	 * @see #getProtocolTypeTmplParam()
	 * @generated
	 */
	void setProtocolTypeTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Source IP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source IP</em>' attribute.
	 * @see #setSourceIP(String)
	 * @see conditions.ConditionsPackage#getIPv4Filter_SourceIP()
	 * @model dataType="conditions.IPPatternOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getSourceIP();

	/**
	 * Sets the value of the '{@link conditions.IPv4Filter#getSourceIP <em>Source IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source IP</em>' attribute.
	 * @see #getSourceIP()
	 * @generated
	 */
	void setSourceIP(String value);

	/**
	 * Returns the value of the '<em><b>Dest IP</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dest IP</em>' attribute.
	 * @see #setDestIP(String)
	 * @see conditions.ConditionsPackage#getIPv4Filter_DestIP()
	 * @model dataType="conditions.IPPatternOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getDestIP();

	/**
	 * Sets the value of the '{@link conditions.IPv4Filter#getDestIP <em>Dest IP</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dest IP</em>' attribute.
	 * @see #getDestIP()
	 * @generated
	 */
	void setDestIP(String value);

	/**
	 * Returns the value of the '<em><b>Time To Live</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time To Live</em>' attribute.
	 * @see #setTimeToLive(String)
	 * @see conditions.ConditionsPackage#getIPv4Filter_TimeToLive()
	 * @model dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
	 * @generated
	 */
	String getTimeToLive();

	/**
	 * Sets the value of the '{@link conditions.IPv4Filter#getTimeToLive <em>Time To Live</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time To Live</em>' attribute.
	 * @see #getTimeToLive()
	 * @generated
	 */
	void setTimeToLive(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidSourceIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidDestinationIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidTimeToLive(DiagnosticChain diagnostics, Map<Object, Object> context);

} // IPv4Filter
