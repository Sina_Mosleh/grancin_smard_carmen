/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.AbstractSignal#getName <em>Name</em>}</li>
 *   <li>{@link conditions.AbstractSignal#getDecodeStrategy <em>Decode Strategy</em>}</li>
 *   <li>{@link conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getAbstractSignal()
 * @model abstract="true"
 * @generated
 */
public interface AbstractSignal extends BaseClassWithSourceReference, ISignalOrReference {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see conditions.ConditionsPackage#getAbstractSignal_Name()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link conditions.AbstractSignal#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Decode Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decode Strategy</em>' containment reference.
	 * @see #setDecodeStrategy(DecodeStrategy)
	 * @see conditions.ConditionsPackage#getAbstractSignal_DecodeStrategy()
	 * @model containment="true" required="true"
	 * @generated
	 */
	DecodeStrategy getDecodeStrategy();

	/**
	 * Sets the value of the '{@link conditions.AbstractSignal#getDecodeStrategy <em>Decode Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decode Strategy</em>' containment reference.
	 * @see #getDecodeStrategy()
	 * @generated
	 */
	void setDecodeStrategy(DecodeStrategy value);

	/**
	 * Returns the value of the '<em><b>Extract Strategy</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link conditions.ExtractStrategy#getAbstractSignal <em>Abstract Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extract Strategy</em>' containment reference.
	 * @see #setExtractStrategy(ExtractStrategy)
	 * @see conditions.ConditionsPackage#getAbstractSignal_ExtractStrategy()
	 * @see conditions.ExtractStrategy#getAbstractSignal
	 * @model opposite="abstractSignal" containment="true" required="true"
	 * @generated
	 */
	ExtractStrategy getExtractStrategy();

	/**
	 * Sets the value of the '{@link conditions.AbstractSignal#getExtractStrategy <em>Extract Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extract Strategy</em>' containment reference.
	 * @see #getExtractStrategy()
	 * @generated
	 */
	void setExtractStrategy(ExtractStrategy value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	AbstractMessage getMessage();

} // AbstractSignal
