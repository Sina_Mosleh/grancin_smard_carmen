/**
 */
package conditions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see conditions.ConditionsPackage
 * @generated
 */
public interface ConditionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConditionsFactory eINSTANCE = conditions.impl.ConditionsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	DocumentRoot createDocumentRoot();

	/**
	 * Returns a new object of class '<em>Condition Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Condition Set</em>'.
	 * @generated
	 */
	ConditionSet createConditionSet();

	/**
	 * Returns a new object of class '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Condition</em>'.
	 * @generated
	 */
	Condition createCondition();

	/**
	 * Returns a new object of class '<em>Signal Comparison Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Comparison Expression</em>'.
	 * @generated
	 */
	SignalComparisonExpression createSignalComparisonExpression();

	/**
	 * Returns a new object of class '<em>Can Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Can Message Check Expression</em>'.
	 * @generated
	 */
	CanMessageCheckExpression createCanMessageCheckExpression();

	/**
	 * Returns a new object of class '<em>Lin Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Lin Message Check Expression</em>'.
	 * @generated
	 */
	LinMessageCheckExpression createLinMessageCheckExpression();

	/**
	 * Returns a new object of class '<em>State Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State Check Expression</em>'.
	 * @generated
	 */
	StateCheckExpression createStateCheckExpression();

	/**
	 * Returns a new object of class '<em>Timing Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timing Expression</em>'.
	 * @generated
	 */
	TimingExpression createTimingExpression();

	/**
	 * Returns a new object of class '<em>True Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>True Expression</em>'.
	 * @generated
	 */
	TrueExpression createTrueExpression();

	/**
	 * Returns a new object of class '<em>Logical Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Logical Expression</em>'.
	 * @generated
	 */
	LogicalExpression createLogicalExpression();

	/**
	 * Returns a new object of class '<em>Reference Condition Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Reference Condition Expression</em>'.
	 * @generated
	 */
	ReferenceConditionExpression createReferenceConditionExpression();

	/**
	 * Returns a new object of class '<em>Transition Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition Check Expression</em>'.
	 * @generated
	 */
	TransitionCheckExpression createTransitionCheckExpression();

	/**
	 * Returns a new object of class '<em>Flex Ray Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flex Ray Message Check Expression</em>'.
	 * @generated
	 */
	FlexRayMessageCheckExpression createFlexRayMessageCheckExpression();

	/**
	 * Returns a new object of class '<em>Stop Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Stop Expression</em>'.
	 * @generated
	 */
	StopExpression createStopExpression();

	/**
	 * Returns a new object of class '<em>Constant Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Comparator Value</em>'.
	 * @generated
	 */
	ConstantComparatorValue createConstantComparatorValue();

	/**
	 * Returns a new object of class '<em>Calculation Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Calculation Expression</em>'.
	 * @generated
	 */
	CalculationExpression createCalculationExpression();

	/**
	 * Returns a new object of class '<em>Variable Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Reference</em>'.
	 * @generated
	 */
	VariableReference createVariableReference();

	/**
	 * Returns a new object of class '<em>Document</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document</em>'.
	 * @generated
	 */
	ConditionsDocument createConditionsDocument();

	/**
	 * Returns a new object of class '<em>Variable Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Set</em>'.
	 * @generated
	 */
	VariableSet createVariableSet();

	/**
	 * Returns a new object of class '<em>Signal Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Variable</em>'.
	 * @generated
	 */
	SignalVariable createSignalVariable();

	/**
	 * Returns a new object of class '<em>Value Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Variable</em>'.
	 * @generated
	 */
	ValueVariable createValueVariable();

	/**
	 * Returns a new object of class '<em>Variable Format</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Format</em>'.
	 * @generated
	 */
	VariableFormat createVariableFormat();

	/**
	 * Returns a new object of class '<em>Observer Value Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Observer Value Range</em>'.
	 * @generated
	 */
	ObserverValueRange createObserverValueRange();

	/**
	 * Returns a new object of class '<em>Signal Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Observer</em>'.
	 * @generated
	 */
	SignalObserver createSignalObserver();

	/**
	 * Returns a new object of class '<em>Signal Reference Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Reference Set</em>'.
	 * @generated
	 */
	SignalReferenceSet createSignalReferenceSet();

	/**
	 * Returns a new object of class '<em>Signal Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Signal Reference</em>'.
	 * @generated
	 */
	SignalReference createSignalReference();

	/**
	 * Returns a new object of class '<em>Bit Pattern Comparator Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bit Pattern Comparator Value</em>'.
	 * @generated
	 */
	BitPatternComparatorValue createBitPatternComparatorValue();

	/**
	 * Returns a new object of class '<em>Not Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Not Expression</em>'.
	 * @generated
	 */
	NotExpression createNotExpression();

	/**
	 * Returns a new object of class '<em>Value Variable Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Value Variable Observer</em>'.
	 * @generated
	 */
	ValueVariableObserver createValueVariableObserver();

	/**
	 * Returns a new object of class '<em>Parse String To Double</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parse String To Double</em>'.
	 * @generated
	 */
	ParseStringToDouble createParseStringToDouble();

	/**
	 * Returns a new object of class '<em>Matches</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Matches</em>'.
	 * @generated
	 */
	Matches createMatches();

	/**
	 * Returns a new object of class '<em>Extract</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Extract</em>'.
	 * @generated
	 */
	Extract createExtract();

	/**
	 * Returns a new object of class '<em>Substring</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Substring</em>'.
	 * @generated
	 */
	Substring createSubstring();

	/**
	 * Returns a new object of class '<em>Parse Numeric To String</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parse Numeric To String</em>'.
	 * @generated
	 */
	ParseNumericToString createParseNumericToString();

	/**
	 * Returns a new object of class '<em>String Length</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Length</em>'.
	 * @generated
	 */
	StringLength createStringLength();

	/**
	 * Returns a new object of class '<em>Some IP Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Some IP Message</em>'.
	 * @generated
	 */
	SomeIPMessage createSomeIPMessage();

	/**
	 * Returns a new object of class '<em>Container Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Container Signal</em>'.
	 * @generated
	 */
	ContainerSignal createContainerSignal();

	/**
	 * Returns a new object of class '<em>Ethernet Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ethernet Filter</em>'.
	 * @generated
	 */
	EthernetFilter createEthernetFilter();

	/**
	 * Returns a new object of class '<em>UDP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UDP Filter</em>'.
	 * @generated
	 */
	UDPFilter createUDPFilter();

	/**
	 * Returns a new object of class '<em>TCP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TCP Filter</em>'.
	 * @generated
	 */
	TCPFilter createTCPFilter();

	/**
	 * Returns a new object of class '<em>IPv4 Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPv4 Filter</em>'.
	 * @generated
	 */
	IPv4Filter createIPv4Filter();

	/**
	 * Returns a new object of class '<em>Some IP Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Some IP Filter</em>'.
	 * @generated
	 */
	SomeIPFilter createSomeIPFilter();

	/**
	 * Returns a new object of class '<em>For Each Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Each Expression</em>'.
	 * @generated
	 */
	ForEachExpression createForEachExpression();

	/**
	 * Returns a new object of class '<em>Message Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Check Expression</em>'.
	 * @generated
	 */
	MessageCheckExpression createMessageCheckExpression();

	/**
	 * Returns a new object of class '<em>Some IPSD Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Some IPSD Filter</em>'.
	 * @generated
	 */
	SomeIPSDFilter createSomeIPSDFilter();

	/**
	 * Returns a new object of class '<em>Some IPSD Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Some IPSD Message</em>'.
	 * @generated
	 */
	SomeIPSDMessage createSomeIPSDMessage();

	/**
	 * Returns a new object of class '<em>Double Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double Signal</em>'.
	 * @generated
	 */
	DoubleSignal createDoubleSignal();

	/**
	 * Returns a new object of class '<em>String Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Signal</em>'.
	 * @generated
	 */
	StringSignal createStringSignal();

	/**
	 * Returns a new object of class '<em>Double Decode Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double Decode Strategy</em>'.
	 * @generated
	 */
	DoubleDecodeStrategy createDoubleDecodeStrategy();

	/**
	 * Returns a new object of class '<em>Universal Payload Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Universal Payload Extract Strategy</em>'.
	 * @generated
	 */
	UniversalPayloadExtractStrategy createUniversalPayloadExtractStrategy();

	/**
	 * Returns a new object of class '<em>Empty Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Empty Extract Strategy</em>'.
	 * @generated
	 */
	EmptyExtractStrategy createEmptyExtractStrategy();

	/**
	 * Returns a new object of class '<em>CAN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CAN Filter</em>'.
	 * @generated
	 */
	CANFilter createCANFilter();

	/**
	 * Returns a new object of class '<em>LIN Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LIN Filter</em>'.
	 * @generated
	 */
	LINFilter createLINFilter();

	/**
	 * Returns a new object of class '<em>Flex Ray Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flex Ray Filter</em>'.
	 * @generated
	 */
	FlexRayFilter createFlexRayFilter();

	/**
	 * Returns a new object of class '<em>DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DLT Filter</em>'.
	 * @generated
	 */
	DLTFilter createDLTFilter();

	/**
	 * Returns a new object of class '<em>UDPNM Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UDPNM Filter</em>'.
	 * @generated
	 */
	UDPNMFilter createUDPNMFilter();

	/**
	 * Returns a new object of class '<em>CAN Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>CAN Message</em>'.
	 * @generated
	 */
	CANMessage createCANMessage();

	/**
	 * Returns a new object of class '<em>LIN Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>LIN Message</em>'.
	 * @generated
	 */
	LINMessage createLINMessage();

	/**
	 * Returns a new object of class '<em>Flex Ray Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Flex Ray Message</em>'.
	 * @generated
	 */
	FlexRayMessage createFlexRayMessage();

	/**
	 * Returns a new object of class '<em>UDP Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UDP Message</em>'.
	 * @generated
	 */
	UDPMessage createUDPMessage();

	/**
	 * Returns a new object of class '<em>TCP Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>TCP Message</em>'.
	 * @generated
	 */
	TCPMessage createTCPMessage();

	/**
	 * Returns a new object of class '<em>UDPNM Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UDPNM Message</em>'.
	 * @generated
	 */
	UDPNMMessage createUDPNMMessage();

	/**
	 * Returns a new object of class '<em>Verbose DLT Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Verbose DLT Message</em>'.
	 * @generated
	 */
	VerboseDLTMessage createVerboseDLTMessage();

	/**
	 * Returns a new object of class '<em>Universal Payload With Legacy Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Universal Payload With Legacy Extract Strategy</em>'.
	 * @generated
	 */
	UniversalPayloadWithLegacyExtractStrategy createUniversalPayloadWithLegacyExtractStrategy();

	/**
	 * Returns a new object of class '<em>Plugin Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin Filter</em>'.
	 * @generated
	 */
	PluginFilter createPluginFilter();

	/**
	 * Returns a new object of class '<em>Plugin Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin Message</em>'.
	 * @generated
	 */
	PluginMessage createPluginMessage();

	/**
	 * Returns a new object of class '<em>Plugin Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin Signal</em>'.
	 * @generated
	 */
	PluginSignal createPluginSignal();

	/**
	 * Returns a new object of class '<em>Plugin State Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin State Extract Strategy</em>'.
	 * @generated
	 */
	PluginStateExtractStrategy createPluginStateExtractStrategy();

	/**
	 * Returns a new object of class '<em>Plugin Result Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin Result Extract Strategy</em>'.
	 * @generated
	 */
	PluginResultExtractStrategy createPluginResultExtractStrategy();

	/**
	 * Returns a new object of class '<em>Empty Decode Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Empty Decode Strategy</em>'.
	 * @generated
	 */
	EmptyDecodeStrategy createEmptyDecodeStrategy();

	/**
	 * Returns a new object of class '<em>Plugin Check Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Plugin Check Expression</em>'.
	 * @generated
	 */
	PluginCheckExpression createPluginCheckExpression();

	/**
	 * Returns a new object of class '<em>Header Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Header Signal</em>'.
	 * @generated
	 */
	HeaderSignal createHeaderSignal();

	/**
	 * Returns a new object of class '<em>Source Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Source Reference</em>'.
	 * @generated
	 */
	SourceReference createSourceReference();

	/**
	 * Returns a new object of class '<em>Ethernet Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ethernet Message</em>'.
	 * @generated
	 */
	EthernetMessage createEthernetMessage();

	/**
	 * Returns a new object of class '<em>IPv4 Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IPv4 Message</em>'.
	 * @generated
	 */
	IPv4Message createIPv4Message();

	/**
	 * Returns a new object of class '<em>Non Verbose DLT Message</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Verbose DLT Message</em>'.
	 * @generated
	 */
	NonVerboseDLTMessage createNonVerboseDLTMessage();

	/**
	 * Returns a new object of class '<em>String Decode Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>String Decode Strategy</em>'.
	 * @generated
	 */
	StringDecodeStrategy createStringDecodeStrategy();

	/**
	 * Returns a new object of class '<em>Non Verbose DLT Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Verbose DLT Filter</em>'.
	 * @generated
	 */
	NonVerboseDLTFilter createNonVerboseDLTFilter();

	/**
	 * Returns a new object of class '<em>Verbose DLT Extract Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Verbose DLT Extract Strategy</em>'.
	 * @generated
	 */
	VerboseDLTExtractStrategy createVerboseDLTExtractStrategy();

	/**
	 * Returns a new object of class '<em>Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Payload Filter</em>'.
	 * @generated
	 */
	PayloadFilter createPayloadFilter();

	/**
	 * Returns a new object of class '<em>Verbose DLT Payload Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Verbose DLT Payload Filter</em>'.
	 * @generated
	 */
	VerboseDLTPayloadFilter createVerboseDLTPayloadFilter();

	/**
	 * Returns a new object of class '<em>Variable Structure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Structure</em>'.
	 * @generated
	 */
	VariableStructure createVariableStructure();

	/**
	 * Returns a new object of class '<em>Computed Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Computed Variable</em>'.
	 * @generated
	 */
	ComputedVariable createComputedVariable();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ConditionsPackage getConditionsPackage();

} //ConditionsFactory
