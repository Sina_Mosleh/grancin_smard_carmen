/**
 */
package conditions;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import statemachine.SmardTraceElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.AbstractObserver#getName <em>Name</em>}</li>
 *   <li>{@link conditions.AbstractObserver#getDescription <em>Description</em>}</li>
 *   <li>{@link conditions.AbstractObserver#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link conditions.AbstractObserver#getValueRanges <em>Value Ranges</em>}</li>
 *   <li>{@link conditions.AbstractObserver#getValueRangesTmplParam <em>Value Ranges Tmpl Param</em>}</li>
 *   <li>{@link conditions.AbstractObserver#isActiveAtStart <em>Active At Start</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getAbstractObserver()
 * @model abstract="true"
 * @generated
 */
public interface AbstractObserver extends BaseClassWithSourceReference, SmardTraceElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see conditions.ConditionsPackage#getAbstractObserver_Name()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link conditions.AbstractObserver#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see conditions.ConditionsPackage#getAbstractObserver_Description()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link conditions.AbstractObserver#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Log Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Log Level</em>' attribute.
	 * @see #setLogLevel(String)
	 * @see conditions.ConditionsPackage#getAbstractObserver_LogLevel()
	 * @model default="0" dataType="conditions.IntOrTemplatePlaceholder"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
	 * @generated
	 */
	String getLogLevel();

	/**
	 * Sets the value of the '{@link conditions.AbstractObserver#getLogLevel <em>Log Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Log Level</em>' attribute.
	 * @see #getLogLevel()
	 * @generated
	 */
	void setLogLevel(String value);

	/**
	 * Returns the value of the '<em><b>Value Ranges</b></em>' containment reference list.
	 * The list contents are of type {@link conditions.ObserverValueRange}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Ranges</em>' containment reference list.
	 * @see conditions.ConditionsPackage#getAbstractObserver_ValueRanges()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObserverValueRange> getValueRanges();

	/**
	 * Returns the value of the '<em><b>Value Ranges Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Ranges Tmpl Param</em>' attribute.
	 * @see #setValueRangesTmplParam(String)
	 * @see conditions.ConditionsPackage#getAbstractObserver_ValueRangesTmplParam()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getValueRangesTmplParam();

	/**
	 * Sets the value of the '{@link conditions.AbstractObserver#getValueRangesTmplParam <em>Value Ranges Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Ranges Tmpl Param</em>' attribute.
	 * @see #getValueRangesTmplParam()
	 * @generated
	 */
	void setValueRangesTmplParam(String value);

	/**
	 * Returns the value of the '<em><b>Active At Start</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active At Start</em>' attribute.
	 * @see #setActiveAtStart(boolean)
	 * @see conditions.ConditionsPackage#getAbstractObserver_ActiveAtStart()
	 * @model default="true" required="true"
	 * @generated
	 */
	boolean isActiveAtStart();

	/**
	 * Sets the value of the '{@link conditions.AbstractObserver#isActiveAtStart <em>Active At Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active At Start</em>' attribute.
	 * @see #isActiveAtStart()
	 * @generated
	 */
	void setActiveAtStart(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidName(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidLogLevel(DiagnosticChain diagnostics, Map<Object, Object> context);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model errorMessagesMany="false" warningMessagesMany="false"
	 * @generated
	 */
	boolean isValid_hasValidValueRanges(EList<String> errorMessages, EList<String> warningMessages);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isValid_hasValidValueRangesTmplParam(DiagnosticChain diagnostics, Map<Object, Object> context);

} // AbstractObserver
