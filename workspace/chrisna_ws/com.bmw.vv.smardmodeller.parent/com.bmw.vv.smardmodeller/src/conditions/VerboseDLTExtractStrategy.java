/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Verbose DLT Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getVerboseDLTExtractStrategy()
 * @model
 * @generated
 */
public interface VerboseDLTExtractStrategy extends ExtractStrategy {
} // VerboseDLTExtractStrategy
