/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.StringDecodeStrategy#getStringTermination <em>String Termination</em>}</li>
 *   <li>{@link conditions.StringDecodeStrategy#getStringTerminationTmplParam <em>String Termination Tmpl Param</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getStringDecodeStrategy()
 * @model
 * @generated
 */
public interface StringDecodeStrategy extends DecodeStrategy {
	/**
	 * Returns the value of the '<em><b>String Termination</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * The literals are from the enumeration {@link conditions.BooleanOrTemplatePlaceholderEnum}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Termination</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #setStringTermination(BooleanOrTemplatePlaceholderEnum)
	 * @see conditions.ConditionsPackage#getStringDecodeStrategy_StringTermination()
	 * @model default="true"
	 *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
	 * @generated
	 */
	BooleanOrTemplatePlaceholderEnum getStringTermination();

	/**
	 * Sets the value of the '{@link conditions.StringDecodeStrategy#getStringTermination <em>String Termination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Termination</em>' attribute.
	 * @see conditions.BooleanOrTemplatePlaceholderEnum
	 * @see #getStringTermination()
	 * @generated
	 */
	void setStringTermination(BooleanOrTemplatePlaceholderEnum value);

	/**
	 * Returns the value of the '<em><b>String Termination Tmpl Param</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Termination Tmpl Param</em>' attribute.
	 * @see #setStringTerminationTmplParam(String)
	 * @see conditions.ConditionsPackage#getStringDecodeStrategy_StringTerminationTmplParam()
	 * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
	 * @generated
	 */
	String getStringTerminationTmplParam();

	/**
	 * Sets the value of the '{@link conditions.StringDecodeStrategy#getStringTerminationTmplParam <em>String Termination Tmpl Param</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String Termination Tmpl Param</em>' attribute.
	 * @see #getStringTerminationTmplParam()
	 * @generated
	 */
	void setStringTerminationTmplParam(String value);

} // StringDecodeStrategy
