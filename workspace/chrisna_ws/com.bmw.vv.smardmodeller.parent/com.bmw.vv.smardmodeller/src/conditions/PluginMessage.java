/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Plugin Message</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link conditions.PluginMessage#getPluginFilter <em>Plugin Filter</em>}</li>
 * </ul>
 *
 * @see conditions.ConditionsPackage#getPluginMessage()
 * @model
 * @generated
 */
public interface PluginMessage extends AbstractMessage {
	/**
	 * Returns the value of the '<em><b>Plugin Filter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Plugin Filter</em>' containment reference.
	 * @see #setPluginFilter(PluginFilter)
	 * @see conditions.ConditionsPackage#getPluginMessage_PluginFilter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PluginFilter getPluginFilter();

	/**
	 * Sets the value of the '{@link conditions.PluginMessage#getPluginFilter <em>Plugin Filter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Plugin Filter</em>' containment reference.
	 * @see #getPluginFilter()
	 * @generated
	 */
	void setPluginFilter(PluginFilter value);

} // PluginMessage
