/**
 */
package conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see conditions.ConditionsPackage#getEmptyExtractStrategy()
 * @model
 * @generated
 */
public interface EmptyExtractStrategy extends ExtractStrategy {
} // EmptyExtractStrategy
