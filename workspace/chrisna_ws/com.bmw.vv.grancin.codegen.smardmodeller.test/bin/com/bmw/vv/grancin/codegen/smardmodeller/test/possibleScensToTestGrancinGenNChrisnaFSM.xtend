package com.bmw.vv.grancin.codegen.smardmodeller.test

/*
 * '''<input>''' -> '''<output after code generation>'''
 */

interface possibleScensToTestGrancinGenNChrisnaFSM {
	
	val Tab = 
	'''
		\t
	'''
	
	val FeatureTitle = 
	'''
		Feature: ApprovalUnit
	''' ->
  '''
		ApprovalUnit
		   -init state: initial state
		    -actions: 
		     -trigger: OnEntry'''
	
	val oneScenWithOneGiven = 
	'''
		Scenario: one
			Given in method methoda with outa == true
			Then attribute a is set to 5 
	'''
	-> 
	'''
		Transition: one
		  -current states: 
		   -state: state1
		    -actions: 
		     -event:       in method methoda 
		       -assignments:      outa == true
		      -trigger: OnEntry
		  -conditions: 
		  -next states: 
		   -state: state2
		    -actions: 
		     -event:       = 
		       -assignments:         a= 5 
		     -trigger: OnEntry
	'''
	
	val oneScenWithOneGivenAndMultipleParameterAssignments =
	'''
		Scenario: one
			Given in method methoda with outa == true
			And in method methodb with outb == 1
			And in method methodc with outc == 3.4
			And in method methodd with outd == "Hello World!"
			Then attribute a = "Goodbye World!" 
	'''
	->
	'''
		Transition: one
		  -current states: 
		   -state: state1
		    -actions: 
		     -event:       in method methoda 
		       -assignments:      outa == true
		     -trigger: OnEntry
		   -state: state2
   		    -actions: 
   		     -event:       in method methodb 
       		   -assignments:      outb == 1
   		     -trigger: OnEntry
   		   -state: state3
  		    -actions: 
  		     -event:       in method methodc 
  		       -assignments:      outc == 3.4
  		     -trigger: OnEntry
  		   -state: state4
 		    -actions: 
 		     -event:       in method methodd 
 		       -assignments:      outc == HelloWorld!
 		     -trigger: OnEntry
		  -conditions: 
		  -next states: 
		   -state: state5
		    -actions: 
		     -event:       = 
		       -assignments:         a= Goodbye World! 
		     -trigger: OnEntry
	'''
	
	val oneScenWithOneWhenAndMultipleParameterAssignments =  
	'''
		Scenario: one
			When in method methoda with outa == true
			And in method methodb with outb == 1
			And in method methodc with outc == 3.4
			And in method methodd with outd == "Hello World!"
			Then attribute a = "Goodbye World!" 
	'''
	->
	'''
		Transition: one
		 -current states:
		 -conditions: 
 		  -condition: condition1
 		   -actions: 
 		    -event:       in method methoda 
 		      -assignments:      outa == true
 		     -trigger: OnEntry
 		  -condition: condition2
 		   -actions: 
 		    -event:       in method methodb 
 	   		  -assignments:      outb == 1
 		     -trigger: OnEntry
 		  -condition: condition3
 		   -actions: 
 		    -event:       in method methodc 
 		      -assignments:      outc == 3.4
 		     -trigger: OnEntry
 		  -condition: condition4
 		   -actions: 
 		    -event:       in method methodd 
 		      -assignments:      outc == HelloWorld!
 		     -trigger: OnEntry
		 -next states: 
		  -state: state1
		   -actions: 
		    -event:       = 
		      -assignments:         a= Goodbye World! 
		     -trigger: OnEntry
	'''

  val oneScenResultingToInitState =
  '''
		Scenario: one
		  Given  no precondition
		  Then  send method methoda with ina=true
	'''
  ->
  '''
		Transition: one
		 -current states: 
		  -state: state1
		   -actions: 
		    -event:       no precondition 
		      -assignments:      outa == true
		     -trigger: OnEntry
		 -conditions: 
		 -next states: 
		  -state: state2
		   -actions: 
		    -event:       do send methoda 
		      -assignments:         ina = true 
		     -trigger: OnEntry
	'''
  
  val twoScensResultingToInitState =
  '''
		Scenario: one
		  When  in method methoda with outa = true
		  Then  send method methodb with inb = true
		  And   send method methodb with inc = true
		  
		Scenario: two
		  When  in method SelectingPWF with PWF="PARKEN"
		  Then  send method WindowApproval with hasWindowApproval=False
		  And   send method ApprovalResultInterface with hasPanicApproval=False
	'''
  ->
  '''
		Transition: one
		 -current states:
		 -conditions: 
 		  -condition: condition1
 		   -actions: 
 		    -event:       in method methoda 
 		      -assignments:      outa == true 
 		     -trigger: OnEntry
		 -next states: 
		  -state: state1
		   -actions: 
		    -event:       do send methodb 
		      -assignments:         inb = true 
		     -trigger: OnEntry
 		   -state: state2
 		    -actions: 
 		     -event:       do send methodb 
 		       -assignments:         inc = true 
 		      -trigger: OnEntry
		
		Transition: two
		 -current states:
		 -conditions: 
 		  -condition: condition1
 		   -actions: 
 		    -event:       in method SelectingPWF 
 		      -assignments:      PWF="PARKEN"
 		     -trigger: OnEntry
		 -next states: 
		  -state: state1
		   -actions: 
		    -event:       do send WindowApproval 
		      -assignments:         hasWindowApproval = False 
		     -trigger: OnEntry
 		   -state: state2
 		    -actions: 
 		     -event:       do send ApprovalResultInterface 
 		       -assignments:          hasPanicApproval = False 
 		      -trigger: OnEntry
	'''
  
  val approvalUnitGrateful =
  '''
		Feature: ApprovalUnit
		  In order to evaluate if the windows are allowed to move
		  As a dedicated service 'ApprovalUnit'
		  I evaluate 'window approval' for both, normal mode and panic mode
		
		  Scenario: start-up behavior
		    When  no trigger
		    Then  send method ApprovalResultInterface with hasWindowApproval=False
		    And   send method ApprovalResultInterface with hasPanicApproval=False
		
		  Scenario: Car is driving and doors are all closed - full approval is granted
		    When  in method SelectingPWF with PWF="FAHREN"
		    And   in method ResultKeySearchPosition with Key="KeyNotInCar"
		    And   in method SelectingDoor with isDriverDoorOpen = False
		    Then  send method ApprovalResultInterface with hasWindowApproval=True
		    And   send method ApprovalResultInterface with hasPanicApproval=True
		
		
		  Scenario: Car is driving and doors are NOT all closed - panic approval is NOT granted
		    Given in method SelectingPWF with PWF="FAHREN"
		    When  in method ResultKeySearchPosition with Key="KeyNotInCar"
		    And   in method SelectingDoor with isDriverDoorOpen = True
		    Then  send method ApprovalResultInterface with hasWindowApproval=True
		    And   send method ApprovalResultInterface is fired with hasPanicApproval=False
		
		  # recognize that the following scenario does not mention any key location
		  # in this scenario, key location represents a "don't care".
		  Scenario: Car is parking - no approval is granted
		    Given in method SelectingPWF with PWF="PARKEN"
		    Then  send method ApprovalResultInterface is fired with hasWindowApproval=False
		    And   send method ApprovalResultInterface is fired with hasPanicApproval=False
		
		  Scenario Outline: Key is not inside the car and car is neither driving nor preparing to drive
		    Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		    And   in method SelectingPWF with PWF=<pwf>
		    Then  send method ApprovalResultInterface is fired with hasWindowApproval=False
		    And   send method ApprovalResultInterface is fired with hasPanicApproval=False
		
		    Examples:
		      | pwf                                 |
		      | "PARKEN_BN_NIO"                     |
		      | "PARKEN_BN_IO"                      |
		      | "STANDFUNKTION_KUNDE_NICHT_IM_FZG"  |
		      | "WOHNEN"                            |
		      | "PRUEFEN_ANALYSE_DIAGNOSE"          |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		      | "SIGNAL_UNBEFUELLT"                 |
		
		  Scenario Outline: Key is inside the car, car is not parking and >=1 door is open
		    Given in method ResultKeySearchPosition with Key="KeyInCar"
		    And   in method SelectingPWF with PWF=<pwf>
		    And   in method SelectingDoor with isDriverDoorOpen = True
		    Then  send method ApprovalResultInterface is fired with hasWindowApproval=True
		    And   send method ApprovalResultInterface is fired with hasPanicApproval=False
		
		    Examples:
		      | pwf                                 |
		      | "WOHNEN"                            |
		      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
		      | "FAHREN"                            |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		
		  # What happened (or should happen) if the following table contained "FAHREN"
		  # This would be redundant information in regard to a previous scenario
		  Scenario Outline: Key is inside the car, car is not parking and all doors are closed
		    When  in method ResultKeySearchPosition with Key="KeyInCar"
		    And   in method SelectingPWF with PWF=<pwf>
		    And   in method event SelectingDoor with isDriverDoorOpen = False
		    Then  send broadcast ApprovalResultInterface
		    And   send method ApprovalResultInterfacemethod with hasPanicApproval=True
		
		    Examples:
		      | pwf                                 |
		      | "WOHNEN"                            |
		      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		      | "FAHREN"                            |
		
	'''
  ->
  '''
  
  '''
  
  val featureWithOnlyAGivenStep = 
  '''
		Scenario: one
		  Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		  And   in method SelectingDoor with isDoorOpen="MAYBE"
		  When  in method SelectingDoor with isDoorOpen="SURE"
		  Then  send broadcast ResultKeyPosition
	'''
  ->
  '''
	'''
  
  val featureWithOneScenGivenWhenThen = 
  '''
		Scenario: one
		  Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		  And   in method SelectingDoor with isDoorOpen="MAYBE"
		  When  in method SelectingPWF with PWF="FLIEGEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  
  val scenWithOnlyAThenStep = 
  '''
		Scenario: one
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithOneScenWithoutDoNothing = 
  '''
		Scenario: one
		  When  in method SelectingPWF with PWF="FLIEGEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithOneScenWithDoNothing = 
  '''
		Scenario: one
			Given  do Nothing
			When  in method SelectingPWF with PWF="FLIEGEN"
			Then  send method ApprovalResultInterface with hasWindowApproval=False
			And   send method KuckucksKasperle        with    hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithOneScenAlsoWithDoNothing = 
  '''
		Scenario: one
		  Given in method SelectingPWF with PWF="FLIEGEN"
		  When  do Nothing
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And  send method KuckucksKasperle        with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithOneScenWithTwoDoNothing = 
  '''
		Scenario: one
			Given do Nothing
			 When do Nothing
			 Then  send method ApprovalResultInterface with hasWindowApproval=False
			 And   send method KuckucksKasperle        with hasPanicApproval=False
  '''
  ->
  '''
  '''
  
  val scenWithTwoScenWithoutDoNothing = 
  '''
		Scenario: one
		  When  in method SelectingPWF with PWF="FLIEGEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
		  
		Scenario: two
		  When  in method SelectingPWF with PWF="SCHWIMMEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method ApprovalResultInterface with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithGiven = 
  '''
		Scenario: one
		  Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		  And   in method SelectingDoor with isDoorOpen="MAYBE"
		  Then  send method ApprovalResultInterface is fired with hasWindowApproval=False
		  And   send method KuckucksKasperle        is fired with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method ApprovalResultInterface with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val scenWithGivenAndSimpleWhen = 
  '''
		Scenario: one
		  Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		  And   in method SelectingDoor with isDoorOpen="MAYBE"
		  When  in method SelectingPWF with PWF="FLIEGEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  When  in method SelectingPWF with PWF="PARKEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method ApprovalResultInterface with hasPanicApproval=False 
	'''
  ->
  '''
	'''
  
  val scenWithGivenAndComplexWhen = 
  '''
		Scenario: one
		  Given in method ResultKeySearchPosition with Key="KeyNotInCar"
		  And   in method SelectingDoor with isDoorOpen="MAYBE"
		  When  in method SelectingPWF with PWF="FLIEGEN"
		  And   in method SelectingPWF with PWF="SCHWIMMEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   send method KuckucksKasperle        with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  When  in method SelectingPWF with PWF="PARKEN"
		  And   in method SelectingPWF with PWF="SCHWIMMEN"
		  Then  send method ApprovalResultInterface with hasWindowApproval=False
		  And   event ApprovalResultInterface       with hasPanicApproval=False 
	'''
  ->
  '''
	'''
  
}
