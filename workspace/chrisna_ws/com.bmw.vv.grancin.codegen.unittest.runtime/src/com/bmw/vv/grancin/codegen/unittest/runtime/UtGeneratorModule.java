package com.bmw.vv.grancin.codegen.unittest.runtime;

import org.eclipse.xtext.service.AbstractGenericModule;   

public class UtGeneratorModule extends AbstractGenericModule {
    public Class<? extends org.eclipse.xtext.generator.IGenerator2> bindIGenerator2() { 
        return UtGenerator.class;
    }
}