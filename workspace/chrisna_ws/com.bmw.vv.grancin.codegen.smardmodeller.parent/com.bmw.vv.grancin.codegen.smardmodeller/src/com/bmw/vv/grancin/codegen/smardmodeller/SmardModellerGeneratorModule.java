package com.bmw.vv.grancin.codegen.smardmodeller;

import org.eclipse.xtext.service.AbstractGenericModule;

public class SmardModellerGeneratorModule extends AbstractGenericModule {
	public Class<? extends org.eclipse.xtext.generator.IGenerator2> bindIGenerate(){
		return SmardModellerGenerator.class;
	}
}
