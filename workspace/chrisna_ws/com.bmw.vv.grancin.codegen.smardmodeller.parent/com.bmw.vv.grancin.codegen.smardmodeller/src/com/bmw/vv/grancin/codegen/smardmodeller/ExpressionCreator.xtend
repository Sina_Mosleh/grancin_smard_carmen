package com.bmw.vv.grancin.codegen.smardmodeller

import conditions.impl.ConditionsFactoryImpl
import java.util.UUID
import conditions.Expression
import conditions.SignalComparisonExpression
//import org.eclipse.emf.common.util.EList
import conditions.SignalReference
import conditions.VariableReference
import conditions.Comparator
import conditions.TimingExpression
import conditions.LogicalExpression
import conditions.LogicalOperatorType
import conditions.TrueExpression
import conditions.NotExpression
import conditions.StringExpression
import conditions.Matches
import conditions.IStringOperand
import conditions.StopExpression
import conditions.AnalysisTypeOrTemplatePlaceholderEnum
import conditions.MessageCheckExpression
import conditions.AbstractMessage
import conditions.CanMessageCheckExpression
import java.util.HashMap
import java.io.Serializable
import conditions.CheckType
import conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
import conditions.CanExtIdentifierOrTemplatePlaceholderEnum
import conditions.LinMessageCheckExpression
import conditions.FlexRayMessageCheckExpression
import conditions.FlexRayHeaderFlagSelection
import conditions.PluginCheckExpression
import conditions.EvaluationBehaviourOrTemplateDefinedEnum
import conditions.PluginSignal
import conditions.StateCheckExpression
import conditions.BooleanOrTemplatePlaceholderEnum
import conditions.TransitionCheckExpression
import statemachine.State
import statemachine.Transition
import chrisna.Assignment
import java.util.ArrayList

class ExpressionCreator {
	
	protected var ConditionsFactoryImpl conditions
	protected var Assignment _assignment
	
	new (Assignment assign){
		conditions = new ConditionsFactoryImpl
		this._assignment = assign	
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def Expression createExpression(Expression expression){
		expression?.setId(createUUID)
		//expression.readVariables.add()
		//expression.writeVariables.add()
		expression.setDescription((_assignment?.leftSide?:'') + ' ' + (_assignment?.operation?.str?: '') + ' ' + (_assignment?.rightSide?: '')) 
		expression
	}
	
	def SignalComparisonExpression createSignalComparisonExpression(ArrayList<VariableReference> refvars, ArrayList<SignalReference> refsigs, Comparator operator){
		var signalcomparisonexpression = conditions.createSignalComparisonExpression
		signalcomparisonexpression = createExpression(signalcomparisonexpression) as SignalComparisonExpression
		for(refsig : refsigs){
			signalcomparisonexpression.comparatorSignal.add(refsig)
		}
		
		for(refvar : refvars){
			signalcomparisonexpression.comparatorSignal.add(refvar)
		}
		signalcomparisonexpression.operator = operator
		signalcomparisonexpression.operatorTmplParam = operator.toString
		signalcomparisonexpression
	}
	
	def TimingExpression createTimingExpression(String minTime, String maxTime){
		var timingexpression = conditions.createTimingExpression
		timingexpression = createExpression(timingexpression) as TimingExpression
		timingexpression.mintime = minTime
		timingexpression.maxtime = maxTime
		timingexpression
	}
		
	def LogicalExpression createLogicalExpression(ArrayList<Expression> expressions, LogicalOperatorType operator){
		var logicalexpression = conditions.createLogicalExpression
		logicalexpression = createExpression(logicalexpression) as LogicalExpression
		for(expression : expressions)
			logicalexpression.expressions.add(expression)
		logicalexpression.operator = operator
		logicalexpression.operatorTmplParam = operator.toString
		logicalexpression
	}
	
	def TrueExpression createTrueExpression(){
		var trueexpression = conditions.createTrueExpression
		trueexpression = createExpression(trueexpression) as TrueExpression
		trueexpression
	}
	
	def NotExpression createNotExpression(Expression expression, LogicalOperatorType operator){
		var notexpression = conditions.createNotExpression
		notexpression = createExpression(notexpression) as NotExpression
		notexpression.expression = expression
		notexpression
	}
	
	def StringExpression createStringExpression(StringExpression stringexpression){
		var newstringexpression = createExpression(stringexpression) as StringExpression
		newstringexpression
	}
	
	def Matches createMatchesExpression(String regex, IStringOperand stringToCheck, IStringOperand dynamicRegex){
		var matchesexpression = conditions.createMatches
		matchesexpression = createStringExpression(matchesexpression as StringExpression) as Matches
		//matchesexpression.regex = regex
		//matchesexpression.dynamicRegex = dynamicRegex
		//matchesexpression.stringToCheck = stringToCheck
		matchesexpression
	}
	
	def StopExpression createStopExpression(AnalysisTypeOrTemplatePlaceholderEnum stopType){
		var stopexpression = conditions.createStopExpression
		stopexpression = createExpression(stopexpression) as StopExpression
		stopexpression.analyseArt = stopType
		stopexpression.analyseArtTmplParam = stopType.toString
		stopexpression
	}
	
	def MessageCheckExpression createMessageCheckExpression(AbstractMessage message){
		var messagecheckexpression = conditions.createMessageCheckExpression
		messagecheckexpression = createExpression(messagecheckexpression) as MessageCheckExpression
		messagecheckexpression.message = message
		messagecheckexpression
	}
	
	def CanMessageCheckExpression createCanMessageCheckExpression(HashMap<String, Serializable> map){
		var canmessagecheckexpression = conditions.createCanMessageCheckExpression
		canmessagecheckexpression = createExpression(canmessagecheckexpression) as CanMessageCheckExpression
		canmessagecheckexpression.busid = searchInMap(map, 'Busid')
		canmessagecheckexpression.type = CheckType.get(searchInMap(map, 'Type'))
		canmessagecheckexpression.messageIDs = searchInMap(map, 'Message IDs')
		canmessagecheckexpression.typeTmplParam = searchInMap(map, 'Type')
		canmessagecheckexpression.busidTmplParam = searchInMap(map, 'Busid')
		canmessagecheckexpression.setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum.get(searchInMap(map, 'Rxtx Flag')))
		canmessagecheckexpression.setRxtxFlagTypeTmplParam(searchInMap(map, 'Rxtx Flag'))
		canmessagecheckexpression.extIdentifier = CanExtIdentifierOrTemplatePlaceholderEnum.get(searchInMap(map, 'Ext Identifier'))
		canmessagecheckexpression.extIdentifierTmplParam = searchInMap(map, 'Ext Identifier')
		canmessagecheckexpression
	}
	
	def LinMessageCheckExpression createLinMessageCheckExpression(HashMap<String, Serializable> map){
		var linmessagecheckexpression = conditions.createLinMessageCheckExpression
		linmessagecheckexpression = createExpression(linmessagecheckexpression) as LinMessageCheckExpression
		linmessagecheckexpression.busid = searchInMap(map, 'Busid')
		linmessagecheckexpression.type = CheckType.get(searchInMap(map, 'Type'))
		linmessagecheckexpression.messageIDs = searchInMap(map, 'Message IDs')
		linmessagecheckexpression.typeTmplParam = searchInMap(map, 'Type')
		linmessagecheckexpression.busidTmplParam = searchInMap(map, 'Busid')
		linmessagecheckexpression
	}
	
	def FlexRayMessageCheckExpression createFlexRayMessageCheckExpression(HashMap<String, Serializable> map){
		var flexraymessagecheckexpression = conditions.createFlexRayMessageCheckExpression
		flexraymessagecheckexpression = createExpression(flexraymessagecheckexpression) as FlexRayMessageCheckExpression
		flexraymessagecheckexpression.busId = searchInMap(map, 'Busid')
		flexraymessagecheckexpression.busIdTmplParam = searchInMap(map, 'Busid')
		flexraymessagecheckexpression.flexrayMessageId = searchInMap(map, 'Message IDs')
		flexraymessagecheckexpression.type = CheckType.get(searchInMap(map, 'Type'))
		flexraymessagecheckexpression.typeTmplParam = searchInMap(map, 'Type')
		flexraymessagecheckexpression.payloadPreamble = FlexRayHeaderFlagSelection.get(searchInMap(map, 'Payload Preamble'))
		flexraymessagecheckexpression.zeroFrame = FlexRayHeaderFlagSelection.get(searchInMap(map, 'Null Frame'))
		flexraymessagecheckexpression.syncFrame = FlexRayHeaderFlagSelection.get(searchInMap(map, 'Sync Frame'))
		flexraymessagecheckexpression.startupFrame = FlexRayHeaderFlagSelection.get(searchInMap(map, 'Startup Frame'))
		flexraymessagecheckexpression.networkMgmt = FlexRayHeaderFlagSelection.get(searchInMap(map, 'Network Mgmt'))
		flexraymessagecheckexpression.payloadPreambleTmplParam = searchInMap(map, 'Payload Preamble')
		flexraymessagecheckexpression.zeroFrameTmplParam = searchInMap(map, 'Null Frame')
		flexraymessagecheckexpression.syncFrameTmplParam = searchInMap(map, 'Sync Frame')
		flexraymessagecheckexpression.startupFrameTmplParam = searchInMap(map, 'Startup Frame')
		flexraymessagecheckexpression.networkMgmtTmplParam = searchInMap(map, 'Network Mgmt')
		flexraymessagecheckexpression
	}
	
	def Expression createMessageCheckExpression(HashMap<String, Serializable> map, ExpressionType type){
		var expressiongen = new ExpressionCreator(null)
		if(type.toString.contains("MESSAGECHECK")){
			switch (type) {
				case MESSAGECHECK: {
					expressiongen.createMessageCheckExpression(null)
				}
				case LINMESSAGECHECK: {
					expressiongen.createLinMessageCheckExpression(map)
				}
				case FLEXRAYMESSAGECHECK: {
					expressiongen.createFlexRayMessageCheckExpression(map)
				}
				case CANMESSAGECHECK: {
					expressiongen.createCanMessageCheckExpression(map)
				}
				default: {
				}
			}
		}
	}
	
	def PluginCheckExpression createPluginExpression(EvaluationBehaviourOrTemplateDefinedEnum evalbehaviour, PluginSignal plugin){
		var plugincheckexpression = conditions.createPluginCheckExpression
		plugincheckexpression = createExpression(plugincheckexpression) as PluginCheckExpression
		plugincheckexpression.evaluationBehaviour = evalbehaviour
		plugincheckexpression.signalToCheck = plugin
		plugincheckexpression.evaluationBehaviourTmplParam = evalbehaviour.toString
		plugincheckexpression
	}
	
	def StateCheckExpression createStateCheckExpression(BooleanOrTemplatePlaceholderEnum checkstate, State st){
		var statecheckexpression = conditions.createStateCheckExpression
		statecheckexpression = createExpression(statecheckexpression) as StateCheckExpression
		statecheckexpression.checkStateActive = checkstate
		statecheckexpression.checkStateActiveTmplParam = checkstate.toString
		statecheckexpression.checkState = st
		statecheckexpression
	}
	
	def TransitionCheckExpression createTransitionCheckExpression(BooleanOrTemplatePlaceholderEnum stayactive, Transition tran){
		var transitioncheckexpression = conditions.createTransitionCheckExpression
		transitioncheckexpression = createExpression(transitioncheckexpression) as TransitionCheckExpression
		transitioncheckexpression.stayActive = stayactive
		transitioncheckexpression.stayActiveTmplParam = stayactive.toString
		transitioncheckexpression.checkTransition = tran
		transitioncheckexpression
	}
	
}