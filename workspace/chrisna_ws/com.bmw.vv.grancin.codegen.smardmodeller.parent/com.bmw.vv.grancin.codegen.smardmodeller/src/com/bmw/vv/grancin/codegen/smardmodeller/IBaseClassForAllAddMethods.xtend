package com.bmw.vv.grancin.codegen.smardmodeller

import java.util.UUID
import java.util.HashMap
import java.io.Serializable
import conditions.SignalReferenceSet
import conditions.AbstractSignal
import conditions.Variable
import conditions.VariableSet
import java.util.ArrayList
import conditions.SignalReference
import conditions.impl.ConditionsFactoryImpl
import conditions.VariableReference
import conditions.Comparator
import conditions.ValueVariable
import org.eclipse.emf.common.util.EList
import conditions.AbstractMessage
import statemachine.State
import conditions.Condition

class IBaseClassForAllAddMethods{
	
	protected var ConditionsFactoryImpl conditions
	
	new(){
		conditions = new ConditionsFactoryImpl
	}
	
	def BusType ConvertIntToBusType(int busId){
		switch busId{
			case 45000:
				return BusType.SOMEIP
			default:
				return BusType.NONE
		}
	}
	
	def VariableType ConvertStringToVariableType(String variabletype){
		switch(variabletype) {
			case "SIGNAL" :
				VariableType.SIGNAL
			case "VALUE" :
				VariableType.VALUE
			default :
				VariableType.NONE
		}
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	/*def String searchInMap(HashMap<String, Serializable> map, String key){
		if(map.containsKey(key))
			map.get(key).toString
		else
			""
	}*/
	
	def String searchInMap(HashMap<String, Object> map, String key){
		if(map.containsKey(key))
			map?.get(key).toString
		else
			""
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractSignal searchInSignalSet(SignalReferenceSet signalslist, String signalname){
		var AbstractSignal foundsignal = null
		for (message : signalslist?.messages){
			for(signal : message?.signals){
				if (signal?.name?.contains(signalname)){
					foundsignal = signal		
				}
			}
		}
		foundsignal	
	}	
	
	def Variable searchInVariableSet(VariableSet variableset, String variablename){
		var Variable foundvariable = null
		for (variable : variableset?.variables){
			if (variable?.name.contains(variablename)){
				foundvariable = variable as Variable
			}
		}
		foundvariable	
	}
	
	def boolean existInVariables(ArrayList<HashMap<String, Object>> variables, String variablename){
		//var Variable foundvariable = null
		for (variable : variables){
			if (variable.containsKey('name')){
				if((variable.get('name') as String)?.contains(variablename))
					return true
			}
		}
		false	
	}
	
	def SignalReference convertAbstractSignalToSignalReference(AbstractSignal signal){
		var signalreference = conditions?.createSignalReference
		signalreference?.setDescription(signal?.name)
		signalreference?.setSignal(signal)
		signalreference
	}
	
	def VariableReference convertVariableToVariableReference(Variable variable){
		var refvariable = conditions?.createVariableReference
		refvariable?.setDescription(variable?.name)
		refvariable?.setVariable(variable)
		refvariable
	}
	
	def String substringBetween(String str, String start, String end){
		var s = str.substring(str.indexOf("(") + 1);
		s = s?.substring(0, s.indexOf(")"));
		s
	}
	
	def double findingTimeFromString(String string, VariableSet variableset){
		var str = string
		if(str?.contains(')') && str?.contains('('))
			str = str?.substringBetween('(', ')')
		var value = 0.0
		value = Double.parseDouble((searchInVariableSet(variableset, str) as ValueVariable)?.initialValue)
		value
	}
	
	def Comparator convertToComparator(String str){
		if(str == '=' || str == '==')
			Comparator.EQ
		else if(str == '<')
			Comparator.LT
		else if(str == '=<' || str == '<=')
			Comparator.LE
		else if(str == '>')
			Comparator.GT
		else if(str == '>=' || str == '=>')
			Comparator.GE
		else if(str == '!=' || str == '!' || str.toLowerCase.contains('not'))
			Comparator.LT
		else
			Comparator.TEMPLATE_DEFINED
	}
	
	protected def AbstractSignal searchForSignal(EList<AbstractMessage> messages, String name){
		var str = name
		if(name.contains('[') && name.contains(']')){
			str = name.replace(name.substring(name.indexOf('['), name.indexOf(']')+1), '')
		}
		var arrOfStr = str.split("\\.")
		
				
		for (mes : messages){
			for(sig : mes?.getSignals){
				var  notsig = false
				for(s : arrOfStr){
					if(!(sig?.name.toLowerCase.contains(s.toLowerCase)))
						notsig = true	
				}
				if(!notsig)
					return sig
			}
		}
		return null
	}
	
	protected def boolean isNumeric(String strNum) {
	    try {
	        Double.parseDouble(strNum)
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false
	    }
	    return true
	}
	
	protected def boolean existStateInStatemachine(EList<State> list, State st){
		for(state : list){
			if (state.name == st.name)
				return true
		}
		false
	}
	
	protected def State searchForState(EList<State> list, String name){
		for (state : list){
			if(state?.name.contains(name))
				return state
		}
		null
	}
	
	protected def Condition searchForCondition(EList<Condition> list, String name){
		for (condition : list){
			if(condition?.name.contains(name))
				return condition
		}
		null
	}
}