package com.bmw.vv.grancin.codegen.smardmodeller

//import chrisna.SendEvent

import chrisna.Assignment
import chrisna.SendEvent
import chrisna.TimeWindow
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.state
import com.bmw.vv.client.RequestData
import com.bmw.vv.grancin.codegen.chrisna.ChrisnaFSMGenerator
import conditions.AbstractMessage
import conditions.AbstractVariable
import conditions.AnalysisTypeOrTemplatePlaceholderEnum
import conditions.BooleanOrTemplatePlaceholderEnum
import conditions.Condition
import conditions.ConditionSet
import conditions.ConditionsDocument
import conditions.ConditionsFactory
import conditions.ConditionsPackage
import conditions.DataType
import conditions.Expression
import conditions.LogicalOperatorType
import conditions.SignalReference
import conditions.SignalReferenceSet
import conditions.SignalVariableLagEnum
import conditions.ValueVariable
import conditions.VariableReference
import conditions.VariableSet
import java.io.Serializable
import java.util.ArrayList
import java.util.HashMap
import org.eclipse.emf.common.util.EList
import org.franca.core.franca.FInterface
import statemachine.ActionTypeNotEmpty
import statemachine.ControlType
import statemachine.InitialState
import statemachine.State
import statemachine.StateMachine
import statemachine.StateType
import statemachine.StatemachineFactory
import statemachine.StatemachinePackage
import statemachine.Transition
import statemachine.impl.StatemachineFactoryImpl
import statemachineset.impl.StatemachinesetFactoryImpl

//import org.eclipse.emf.ecore.EClass
//import org.eclipse.emf.ecore.EObject

class SmardModellerCreator extends IBaseClassForAllAddMethods {
	protected var ChrisnaFSMGenerator chrisnafsm
	protected var RequestData restapireqdata
	protected var FInterface inter
	
	protected var StatemachineFactoryImpl statemachine
	protected var StatemachinesetFactoryImpl statemachineset
	
	public var SignalReferenceSet signalreferenceset
	public var VariableSet variableset
	public var ConditionSet conditionset
	
	public val ConditionsFactory conditionsfactory
	public val ConditionsPackage conditionspackage
	public val StatemachineFactory statemachinefactory
	public val StatemachinePackage statemachinepackage
	
	protected var EList<Condition> conditionscontainer
	protected var EList<AbstractMessage> signalscontainer
	protected var EList<AbstractVariable> variablescontainer
	
	protected var ChrisnaFactoryImpl gen
	protected val offset = 1
	
	new (FInterface intface, ChrisnaFSMGenerator csa, RequestData reqdata){
		conditionsfactory = ConditionsFactory.eINSTANCE
		conditionspackage = conditionsfactory?.conditionsPackage
		
		statemachinefactory = StatemachineFactory.eINSTANCE
		statemachinepackage = statemachinefactory?.statemachinePackage
		
		statemachine = new StatemachineFactoryImpl
		statemachineset = new StatemachinesetFactoryImpl
		
		gen = new ChrisnaFactoryImpl
		
		signalreferenceset = conditions?.createSignalReferenceSet
		variableset = conditions?.createVariableSet
		conditionset = conditions?.createConditionSet
		
		this.inter = intface
		this.chrisnafsm = csa
		this.restapireqdata = reqdata
		
		this.CreateAll
		
		 //conditionDocuGenerator
		 //stateMachineDocuGenerator
	}
	
	/*def EList<AbstractMessage> getMessages(){
		this.signalreferenceset.getMessages
	} 
	
	def VariableSet getVariableset(){
		this.variableset
	}
	
	def ConditionSet getConditionset(){
		this.conditionset	
	}*/
	
	def ChrisnaFSMGenerator getChrisnaFsm(){
		return this.chrisnafsm
	}
	
	def void setChrinaFsm(ChrisnaFSMGenerator csa){
		this.chrisnafsm = csa
	}
	
	def RequestData getRequestedData(){
		return this.restapireqdata
	}
	
	def void setRequestedData(RequestData arr){
		this.restapireqdata = arr
	}
	
	def void CreateAll(){
	}
	
	def AbstractMessage createMessage(HashMap<String, Serializable> map, BusType bustype){
		var signals = new ArrayList<SignalCreator>()
		signals.add(new SignalCreator(DecodeType.DOUBLE, ExtractType.UNIVERSALPAYLOADWITHLEGACY, SignalType.DOUBLE))
		var filters = new ArrayList<FilterCreator>()
		//filters.add(new FilterCreator(BusType.NONE))
		var MessageCreator messagegen = new MessageCreator(ConvertIntToBusType(restapireqdata.getCon.getbusID),
															this.inter?.version?.major?.toString,
															signals, filters);
		switch bustype {
			case SOMEIP: {
				messagegen?._filters.add(new FilterCreator(BusType.SOMEIP))
				messagegen?.createSomeIPMessage(map)				
			}
			case CAN: {
				messagegen?._filters.add(new FilterCreator(BusType.CAN))
				messagegen?.createCANMessage(map)
			}
			case LIN: {
				messagegen?._filters.add(new FilterCreator(BusType.LIN))
				messagegen?.createLINMessage(map)
			}
			case FLEXRAY: {
				messagegen?._filters.add(new FilterCreator(BusType.FLEXRAY))
				messagegen?.createFlexRayMessage(map)
			}
			case ETHERNET: {
				messagegen?._filters.add(new FilterCreator(BusType.ETHERNET))
				messagegen?.createEthernetMessage(map)
			}
			case IPV4: {
				messagegen?._filters.add(new FilterCreator(BusType.IPV4))
				messagegen?.createIPv4Message(map)
			}
			case NONVERBOSEDLT: {
				messagegen?._filters.add(new FilterCreator(BusType.NONVERBOSEDLT))
				messagegen?.createNonVerboseDLTMessage(map)
			}
			case PLUGIN: {
				messagegen?._filters.add(new FilterCreator(BusType.PLUGIN))
				messagegen?.createPluginMessage(map)
			}
			case SOMEIPSD: {
				messagegen?._filters.add(new FilterCreator(BusType.SOMEIPSD))
				messagegen?.createSomeIPSDMessage(map)
			}
			case UDP: {
				messagegen?._filters.add(new FilterCreator(BusType.UDP))
				messagegen?.createUDPMessage(map)
			}
			case UDPNM: {
				messagegen?._filters.add(new FilterCreator(BusType.UDPNM))
				messagegen?.createUDPNMMessage(map)
			}
			case VERBOSEDLT: {
				messagegen?._filters.add(new FilterCreator(BusType.VERBOSEDLT))
				messagegen?.createVerboseDLTPayloadMessage(map)
			}
			case NONE: {
				messagegen?._filters.add(new FilterCreator(BusType.NONE))
				messagegen?.createAbstractBusMessage(null, map)
			}
		}
		
	}
	
	def AbstractVariable createVariable(HashMap<String, Object> map){
		var variablegen = new VariableCreator()
		var vartyp = ConvertStringToVariableType(searchInMap(map, 'variableType'))
		switch(vartyp) {
			case NONE: {
				variablegen?.createAbstractVariable(null, map)
			}
			case SIGNAL: {
				variablegen?.createSignalVariable(map)
			}
			case VALUE: {
				variablegen?.createValueVariable(map)
			}
		}
	}
	
	def ArrayList<VariableReference> createVariablesForExpression(Assignment assignment){
		var ArrayList<VariableReference> variables = new ArrayList<VariableReference>()
		if(searchInVariableSet(this.variableset, assignment.rightSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment.leftSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment.leftSide)))
			//foundLeftSide = true
		}
		variables
	}
	
	def createRefSignalsAndVariablesForExpression(Assignment assignment){
		/*var boolean foundLeftSide = false
		var boolean foundRightSide = false*/
		var EList<SignalReference> refsignals
		var EList<VariableReference> variables
		if(searchInSignalSet(this.signalreferenceset, assignment?.leftSide) !== null){
			refsignals.add(convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, assignment?.leftSide)))
			//foundLeftSide = true
		}
		if(searchInSignalSet(this.signalreferenceset, assignment?.rightSide) !== null){
			refsignals.add(convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, assignment?.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment?.rightSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment?.rightSide)))
			//foundRightSide = true	
		}
		if(searchInVariableSet(this.variableset, assignment?.leftSide) !== null){
			variables.add(convertVariableToVariableReference(searchInVariableSet(this.variableset, assignment?.leftSide)))
			//foundLeftSide = true
		}
		
		/*if(!foundLeftSide){
			var variablegen = new VariableCreator()
			var newvariable = variablegen.createValueVariable()
			variables.add(convertVariableToVariableReference(newvariable as Variable))
			this.variableset.variables.add(newvariable)
		}
		if(!foundRightSide){
			var variablegen = new VariableCreator()
			var newvariable = variablegen.createValueVariable()
			variables.add(convertVariableToVariableReference(newvariable as Variable))
			this.variableset.variables.add(newvariable)
		}*/
	}
	
	//def Condition createCondition(chrisna.Condition condition, EList<AbstractVariable> variables,  EList<AbstractMessage> messages) {
	def Condition createCondition(EList<chrisna.Condition> conds) {
		var cond = conditions?.createCondition
		var expressiongen = new ExpressionCreator(null)
		var ArrayList<Expression> conditionsexpressions = new ArrayList<Expression>()
		for(condition : conds){
			cond?.setId(createUUID)
			cond?.setName(condition?.name)// + condition.name
			cond.description = condition?.description + condition?.name + '\n'
			var ArrayList<Expression> eventsexpressions = new ArrayList<Expression>()
			for(event : condition?.action?.sendevent){
				var ArrayList<Expression> assignsexpressions = new ArrayList<Expression>()
				var assignments = event?.getAssigcontainer?.getAssignment
				switch(event?.getMethod){
					case 'Not': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							var variables = createVariablesForExpression(assign)
							var ArrayList<SignalReference> refsignals = new ArrayList<SignalReference>()
							assignsexpressions.add(expressiongen?.createSignalComparisonExpression(variables, refsignals, 
								convertToComparator(assign?.getOperation?.getStr)
							))
						}
						eventsexpressions.add(expressiongen.createNotExpression(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND) as Expression, LogicalOperatorType.NOT))
					}
					case 'Comparison': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							var variables = createVariablesForExpression(assign)
							var ArrayList<SignalReference> refsignals = new ArrayList<SignalReference>()
							assignsexpressions.add(expressiongen?.createSignalComparisonExpression(variables, refsignals, 
								convertToComparator(assign?.getOperation.getStr)
							))
						}
						eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
					}
					case 'Timing': {
						for(var i = 0 ; i< assignments.length; i++){
							var assign = assignments?.get(i)
							assignsexpressions.add(expressiongen.createTimingExpression(
								(assign?.getLeftSide.findingTimeFromString(this.variableset) - offset).toString,
								(assign?.getLeftSide.findingTimeFromString(this.variableset) + offset).toString
							))
						}
						eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
					}
					case 'True': {
						for(var i = 0 ; i< assignments.length; i++){
							assignsexpressions.add(expressiongen?.createTrueExpression)	
						}
						eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
					}
					case 'Stop': {
						for(var i = 0 ; i< assignments.length; i++){
							assignsexpressions.add(expressiongen?.createStopExpression(AnalysisTypeOrTemplatePlaceholderEnum.ALL))	
						}
						eventsexpressions.add(expressiongen?.createLogicalExpression(assignsexpressions,LogicalOperatorType.AND))
					}
					case 'MessageCheck': {
						/*for(assignment : event?.getAssigcontainer?.getAssignment){
							assignsexpressions.add(expressiongen.createMessageCheckExpression({
								convertVariableToVariableReference(searchInVariableSet(this.variableset, str))
							}))	
						}*/
						/*if(searchInSignalSet(this.signalreferenceset, event?.method) !== null){
							var signal = convertAbstractSignalToSignalReference(searchInSignalSet(this.signalreferenceset, event?.method))
							//foundLeftSide = true
						}
						if(convertVariableToVariableReference(searchInVariableSet(this.variableset, event?.method)) !== null){
							var variable = convertVariableToVariableReference(searchInVariableSet(this.variableset, event?.method))
							eventsexpressions.add(expressiongen.createMessageCheckExpression(assignsexpressions,LogicalOperatorType.AND))
						}*/
					}
				}
			}
			conditionsexpressions.add(expressiongen?.createLogicalExpression(eventsexpressions,LogicalOperatorType.AND))
		}
		
		cond?.setExpression(expressiongen?.createLogicalExpression(conditionsexpressions,LogicalOperatorType.AND))
		
		/*for(event : condition.action.sendevent){
			var EList<Expression> expressions
			for(assignment : event.assigcontainer.assignment){
				var gen = new ExpressionCreator(assignment)
				switch(event.event) {
					case 'no trigger': {
						
					}
					case '==': {
						var EList<SignalReference> refsignals
						var EList<VariableReference> variables
						createRefSignalsAndVariablesForExpression(refsignals, variables, assignment)
						val operator = Comparator.get(assignment.operation.str)
						expressions.add(gen.createSignalComparisonExpression(variables, refsignals, operator))
					}
					case 'is set to': {
						var EList<SignalReference> refsignals
						var EList<VariableReference> variables
						createRefSignalsAndVariablesForExpression(refsignals, variables, assignment)
						val operator = Comparator.get(assignment.operation.str)
						expressions.add(gen.createSignalComparisonExpression(variables, refsignals, operator))
					}
					case 'equals': {
						var EList<SignalReference> refsignals
						var EList<VariableReference> variables
						createRefSignalsAndVariablesForExpression(refsignals, variables, assignment)
						val operator = Comparator.get(assignment.operation.str)
						expressions.add(gen.createSignalComparisonExpression(variables, refsignals, operator))
					}
					case "in method": {
						//expressions.add(gen.createMessageCheckExpression())
					}
					case "continuation": {
						//expressions.add(gen.createTimingExpression())
					}
					case "pass": {
						if (event.time.type == "Periodicaly") {
							if((event.time as Periodically).unit.contains('ms'))
								expressions.add(gen.createTimingExpression(((event.time as Periodically).time + offset).toString, ((event.time as Periodically).time - offset).toString))
							else if((event.time as Periodically).unit.contains('us'))	
								expressions.add(gen.createTimingExpression((((event.time as Periodically).time + offset)/1000).toString, (((event.time as Periodically).time - offset)/1000).toString))
							else if((event.time as Periodically).unit.contains('ns'))
								expressions.add(gen.createTimingExpression((((event.time as Periodically).time + offset)/1000000).toString, (((event.time as Periodically).time - offset)/1000000).toString))
							else if((event.time as Periodically).unit.contains('s') )
								expressions.add(gen.createTimingExpression((((event.time as Periodically).time + offset)*1000).toString, (((event.time as Periodically).time - offset)*1000).toString))
						}
					}
				}
			}
			eventexpressions.add(expressiongen.createLogicalExpression(expressions,LogicalOperatorType.AND))
		}
		cond.expression = expressiongen.createLogicalExpression(eventexpressions,LogicalOperatorType.AND)*/
		/*for(event : condition.action.sendevent){
			var expressiongen = new ExpressionCreator(event)
			var exptyp = chooseExpressionType(event)
			switch (exptyp) {
				case NONE:{
					cond.expression = expressiongen.createExpression(null)
				}
				case TIMING: {
					//var offset = 10
					cond.expression = expressiongen.createTimingExpression(event.time.toString, event.time.toString)
				}
				case LOGICAL: {
				}
				case TRUE: {
				}
				case NOT: {
				}
				case MATCHES: {
				}
				case STOP: {
				}
				case MESSAGECHECK: {
				}
				case STATECHECK: {
				}
				case TRANSITIONCHECK: {
				}
				case PLUGINCHECK: {
				}
				case STRING: {
				}
				case SIGNALCOMPARISON: {
				}
				case REFERENCECONDITION: {
				}
				case LINMESSAGECHECK: {
				}
				case FLEXRAYMESSAGECHECK: {
				}
				case CANMESSAGECHECK: {
				}
				case CALCULATION: {
				}
			}}*/
		//var conditiongen = new
		//cond*/
		
		cond
	}
	
	def InitialState createInitialState(state st){
		var state = statemachine?.createInitialState
		state.setId(createUUID)
		state.setName(st?.name)
		//state.readVariables.add()
		//state.writeVariables.add()
		state?.setDescription(st?.description.toString)
		/*val actiongen = new ActionCreator()
			for(event: st.action.sendevent){
				for(assignment: event.assigcontainer.assignment){
					switch(event.event){
						case 'no precondition': {
								
						}
						case '==': {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
						case 'is set to': {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
						case 'equals': {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
						case "in method": {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
						case "continuation": {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
						case "pass": {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
						}
					}
					
					switch(event.event){
						case '==': {
							var variable = searchInVariableSet(this.variableset, assignment.leftSide)
							state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
						}
						case 'is set to': {
							var variable = searchInVariableSet(this.variableset, assignment.leftSide)
							state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
						}
						case 'equals': {
							var variable = searchInVariableSet(this.variableset, assignment.leftSide)
							state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
						}
						case "do not send": {
							//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.OFF))
						}
						case "do send":{
							state.actions.add(actiongen.createAction(assignment, st.action.trigger, ActionTypeNotEmpty.PLUGIN))
						}
					}
				}
			}*/
		state?.setStateType(StateType.INIT)
		
		state
	}
	
	def State createState(state st){
		var state = statemachine?.createState
		state?.setId(createUUID)
		state?.setName(st?.name)
		//state.readVariables.add()
		//state.writeVariables.add()
		state?.setDescription(st?.description?.toString)
		for(event : st?.action?.sendevent){
			val actiongen = new ActionCreator()
			switch(event?.method){
				case 'Compute': {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						state?.actions.add(actiongen?.createComputeVariable(assignment, event?.trigger, 
							(searchInVariableSet(this.variableset, assignment?.leftSide) as ValueVariable)))
					}
				}
				case 'Control': {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						state?.actions.add(actiongen?.createControlAction(assignment, event?.trigger, null, ControlType.ON))
					}
				}
				default: {
					for(assignment : event?.getAssigcontainer?.getAssignment){
						state?.actions.add(actiongen?.createAction(assignment, event?.trigger, ActionTypeNotEmpty.COMPUTE))
					}
				}
			}
		}
		
		/*state.name = '' + ([
			for(st : states)
				st.name + ' '
		])
		state.description = '' + ([
			for(st: states)
				st.description.toString + ' '
		])
		for(st: states){
			val actiongen = new ActionCreator()
			for(event: st.action.sendevent){
				for(assignment: event.assigcontainer.assignment){
					if(statetyp == CurrentNext.CURRENT){
						switch(event.event){
							case 'no precondition': {
									
							}
							case 'no trigger': {
								
							}
							case '==': {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
							case 'is set to': {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
							case 'equals': {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
							case "in method": {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
							case "continuation": {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
							case "pass": {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.ON))
							}
						}
					}
					
					if(state == CurrentNext.NEXT){
						switch(event.event){
							case '==': {
								var variable = searchInVariableSet(this.variableset, assignment.leftSide)
								state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
							}
							case 'is set to': {
								var variable = searchInVariableSet(this.variableset, assignment.leftSide)
								state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
							}
							case 'equals': {
								var variable = searchInVariableSet(this.variableset, assignment.leftSide)
								state.actions.add(actiongen.createComputeVariable(assignment, st.action.trigger, variable as ValueVariable))
							}
							case "do not send": {
								//state.actions.add(actiongen.createControlAction(assignment, st.action.trigger, , ControlType.OFF))
							}
							case "do send":{
								state.actions.add(actiongen.createAction(assignment, st.action.trigger, ActionTypeNotEmpty.PLUGIN))
							}
						}	
					}
				}
			}
		}*/
		//state.stateType = sttyp
		//state.stateTypeTmplParam = sttyp.toString
		
		if (state?.name?.toLowerCase.contains('ok'))
			state?.setStateType(StateType.OK)
		else if (state?.name?.toLowerCase.contains('warning'))
			state?.setStateType(StateType.WARN)
		else if (state?.name?.toLowerCase.contains('error'))
			state?.setStateType(StateType.DEFECT)
		else
			state?.setStateType(StateType.INFO)
		state?.setStateTypeTmplParam(state?.stateType.toString)
		
		state
	}
	
	def Transition createTransition(chrisna.Condition condition, EList<State> states, EList<Condition> conditions){
		var transition = statemachine.createTransition
		transition?.setId(createUUID)
		transition?.setName(condition?.name) 
		//transition.readVariables.add()
		//transition.writeVariables.add()
		transition?.setDescription(condition?.description?.toString)
		transition?.setFrom(searchForState(states, condition?.from?.name))
		transition?.setTo(searchForState(states, condition?.to?.name))
		if(searchForCondition(conditions, condition?.name) !== null)
			transition?.conditions.add(searchForCondition(conditions, condition?.name))
		/*transition.name = '' + [
			for(condition : conditions){
				condition?.name + ' '
			}
		]
		transition.description = '' + [
			for(condition : conditions){
				condition?.description.toString + ' '	
			}
		]
		transition.from = currentState
		transition.to = nextState
		for(condition : conditions)
			transition.conditions.add(createCondition(condition))*/
		transition
	}
	
	protected def HashMap<String, Object> createVariableForAssignmentSide(SendEvent event, EList<AbstractMessage> messages, String leftside, String rightside){
		var map = new HashMap<String, Object>()
		if (searchForSignal(signalreferenceset?.getMessages, leftside) !== null){
			map.put('variableType', VariableType.SIGNAL)
			map.put('signal', searchForSignal(messages, leftside))
			map.put('description', '')
			map.put('name', /*searchForSignal(messages, leftside)?.name*/ leftside)
			map.put('dataType', DataType.DOUBLE)
			map.put('format', {
					//var p = Pattern.compile('\\d+')
					//var m = p.matcher(assignment?.getLeftSide)
					//if(rightside.isNumeric){
						//var d = Double.parseDouble(rightside)
						//if(d.toString.contains('.'))
							//if(m.group.contains('.'))
							'double'
						//else
							//'int'		
					//}
					//else 
						//''
				})
			map.put('interpretedValue', BooleanOrTemplatePlaceholderEnum.FALSE)
			map.put('lag', SignalVariableLagEnum.CURRENT)
			if(event?.getMethod.contains('Timing') && event?.getTime?.getType.contains('TimeWindow'))
				map.put('unit', ((event.getTime) as TimeWindow)?.getUnit)
			else
				map.put('unit', '')
		}
		else {
			map.put('variableType', VariableType.VALUE)
			//if (rightside.isNumeric){
				map.put('dataType', DataType.DOUBLE)
				map.put('initialValue', 0 as double)
				map.put('format', {
					//var p = Pattern.compile('\\d+')
					//var m = p.matcher(assignment?.getLeftSide)
					//var d = Double.parseDouble(rightside)
					//if(d.toString.contains('.'))
						//if(m.group.contains('.'))
						'double'
					//else
						//'int'
				})
			//}
			/*else {
				map.put('dataType', DataType.STRING)
			}*/
			map.put('description', '')
			map.put('name', leftside)
			if((event?.getMethod.contains('Timing') && event?.getTime?.getType?.contains('TimeWindow')))
				map.put('unit', ((event.getTime) as TimeWindow)?.getUnit)
			else if (event?.getMethod.contains('Compute') || event?.getMethod.contains('Timing')/*&& event?.getTime?.getType.contains('TimeWindow')*/){
				if(rightside.toLowerCase.contains(' ms'))
					map.put('unit', 'ms')
				else if(rightside.toLowerCase.contains(' us'))
					map.put('unit', 'us')
				else if(rightside.toLowerCase.contains(' ns'))
					map.put('unit', 'ns')
				else if(rightside.toLowerCase.contains(' s'))
					map.put('unit', 's')
				else if(rightside.toLowerCase.contains(' min'))
					map.put('unit', 'min')
				else if(rightside.toLowerCase.contains(' h'))
					map.put('unit', 'h')
				else if(rightside.toLowerCase.contains(' day'))
					map.put('unit', 'day')
			}
			else
				map.put('unit', '')
		}
		map
	}
	
	def ConditionsDocument conditionDocuGenerator(){
		//<.conditions>
		//val document = conditions.createDocumentRoot
		val conditionsdocument = conditions?.createConditionsDocument	
				
		
		//val bustype = ConvertIntToBusType(restapireqdata.getCon.getbusID)
		val bustype = ConvertIntToBusType(45000)
		var requestedinfo = new ArrayList<HashMap<String, Serializable>>()
		//requestedinfo = restapireqdata?.getParsed?.getListData
			requestedinfo.forEach[
				signalreferenceset?.getMessages.add(createMessage(it, bustype))
			]
			
				
		
		var additionalvariables = new ArrayList<HashMap<String, Object>>()
		for( scen : this.chrisnafsm?.getScenariocontainer?.getScenario){
			for(state : scen?.getCurrentStates?.getState){
				for( event : state?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
			
			for(state : scen?.getNextStates?.getState){
				for( event : state?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
			
			for(condition : scen?.getCondcontainer?.getCondition){
				for( event : condition?.getAction?.getSendevent){
					for(assignment : event?.getAssigcontainer?.getAssignment){
						//var string = assignment?.getLeftSide
						//if(string?.contains('bdcIdentityMappingTable'))
							//assignment?.setLeftSide('bdcIdentityMappingTable[0].piaProfileId')
						if(!existInVariables(additionalvariables, assignment?.getLeftSide))
							additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getLeftSide, assignment?.getRightSide))
						//additionalvariables.add(createVariableForAssignmentSide(event, signalreferenceset?.getMessages, assignment?.getRightSide, assignment?.getRightSide))
					}
				}
			}
		}
			variableset?.setUuid(createUUID) 
			additionalvariables.forEach[
				variableset?.variables.add(createVariable(it))
			]
			
			
			
			conditionset.baureihe = ''
			conditionset.description = ''
			conditionset.istufe = ''
			conditionset.schemaversion = ''
			conditionset.uuid = createUUID
			conditionset.version = ''
			conditionset.name = ''	
			
		var condconditions = gen?.createCondContainer
		var scenarios = this.chrisnafsm?.getScenariocontainer?.getScenario
		for( scen : scenarios){
			var condis = scen?.condcontainer?.condition
			for(var i = 0; i<condis.length; i++) {
				condconditions?.condition.add(condis.get(i))
			}
		} 
			
			var Bedingungen = condconditions?.condition
			for (var i = 0; i<Bedingungen.length; i++){
				var conditioncontainer = gen?.createCondContainer
				var cond = Bedingungen?.get(i)
				conditioncontainer?.getCondition.add(cond)
				var Bedingungen_temp = condconditions?.condition
				for (var j = 0; j< Bedingungen_temp.length; j++){
					var con = Bedingungen_temp?.get(j)
					if(con?.to?.name == cond?.to?.name && con?.from?.name == cond?.from?.name){
						conditioncontainer?.getCondition.add(con)
						Bedingungen.remove(con)	
					}
				}
				conditionset.conditions.add(createCondition(conditioncontainer?.getCondition))
			}
		//var conditioncontainer = chrisnafsm.condcontainer.condition
			
			/*conditioncontainer.forEach[
				//conditionset.conditions.add(createCondition(it, variableset.variables, signalreferenceset.getMessages))
				conditionset.conditions.add(createCondition(it))
			]*/
			
		
		conditionsdocument?.setSignalReferencesSet(signalreferenceset)
		conditionsdocument?.setVariableSet(variableset)
		conditionsdocument?.setConditionSet(conditionset)
		
		//document?.setConditionsDocument(conditionsdocument)
		
		
		
		//conditionspackage?.getEClassifiers().add(conditionsdocument as EClass)
		//conditionspackage?.getEClassifiers().add(document.eClass)
		//(conditionspackage?.getDocumentRoot as DocumentRoot)
		//conditionspackage.documentRootEClass
		//(conditionspackage?.getDocumentRoot as DocumentRoot)?.setConditionsDocument(conditionsdocument)
		
		conditionsdocument
		//document
	}
	
	def StateMachine stateMachineDocuGenerator(){
		//val document = statemachine.createDocumentRoot
		
		val statemachine = statemachine?.createStateMachine
		statemachine?.setId(createUUID)
		//statemachine.readVariables.add()
		//statemachine.writeVariables.add()
		statemachine?.setName(chrisnafsm?.name)
		statemachine?.setInitialState(createInitialState(chrisnafsm?.initState))
		for (scen : chrisnafsm?.scenariocontainer?.scenario){
			for(st : scen?.currentStates?.state)
				if (!st?.name.contains('init') && !statemachine?.states.existStateInStatemachine(createState(st)))
					statemachine?.states.add(createState(st))
			for(st : scen?.nextStates?.state)
				if (!st?.name.contains('init') && !statemachine?.states.existStateInStatemachine(createState(st)))
					statemachine?.states.add(createState(st))
			//statemachine.states.add(createState(it.nextStates.state, it.condcontainer.condition, StateLinkedTransition.IN, StateType.OK, CurrentNext.NEXT))
			/*statemachine.transitions.add(createTransition(it.condcontainer.condition, 
				createState(it.currentStates.state, it.condcontainer.condition, StateLinkedTransition.OUT, StateType.OK, CurrentNext.CURRENT),
				createState(it.nextStates.state, it.condcontainer.condition, StateLinkedTransition.IN, StateType.OK, CurrentNext.NEXT)
			))*/
			for(cond : scen?.condcontainer?.condition){
				var transition = createTransition(cond, statemachine?.states, conditionset?.conditions)
				statemachine?.transitions.add(transition)
				(searchForState(statemachine?.states, cond?.from?.name))?.out.add(transition)
				(searchForState(statemachine?.states, cond?.to?.name))?.in.add(transition)
			}
			//statemachine.states.head.in.add(statemachine.transitions.head)
			//statemachine.states.get(statemachine.states.length - 1).out.add(statemachine.transitions.head)
		}

		//(statemachinepackage?.getDocumentRoot as statemachine.DocumentRoot)?.setStateMachine(statemachine)
		
		statemachine
		//document
	}
}