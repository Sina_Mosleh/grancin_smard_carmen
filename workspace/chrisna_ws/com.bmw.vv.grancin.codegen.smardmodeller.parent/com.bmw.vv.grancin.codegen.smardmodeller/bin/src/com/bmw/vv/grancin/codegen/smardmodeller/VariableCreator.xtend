package com.bmw.vv.grancin.codegen.smardmodeller

import conditions.impl.ConditionsFactoryImpl
import conditions.AbstractVariable
import java.util.HashMap
import java.io.Serializable
import java.util.UUID
import conditions.SignalVariable
import conditions.ValueVariable
import conditions.SignalVariableLagEnum
import conditions.BooleanOrTemplatePlaceholderEnum
import java.util.ArrayList
import conditions.DataType
import conditions.Variable
import conditions.FormatDataType
import conditions.AbstractSignal

class VariableCreator {
	
	protected var ConditionsFactoryImpl conditions
	protected var ObserverCreator _obsgen
	
	new (){
		conditions = new ConditionsFactoryImpl	
	}
	
	def String createUUID(){
		val uuid = UUID.randomUUID();
		uuid.toString
	}
	
	def Object searchInMap(HashMap<String, Object> map, String key){
		if(map.containsKey(key))
			map.get(key)
		else
			""
	}
	
	def ArrayList<HashMap<String, Serializable>> keyToList(HashMap<String, Object> map, String key){
		var list = new ArrayList<HashMap<String, Serializable>>()
		if(map.containsKey(key))
			list = map.get(key) as ArrayList<HashMap<String, Serializable>>
		list
			
	}
	
	def boolean existInMap(HashMap<String, Serializable> map, String key){
		map.containsKey(key)
	}
	
	def AbstractVariable createAbstractVariable(AbstractVariable variable, HashMap<String, Object> map){
		variable?.setId(createUUID)
		//variable.readVariables.add()
		//variable.writeVariables.add()
		variable?.setName(searchInMap(map, 'name') as String)
		variable?.setDisplayName(searchInMap(map, 'displayName') as String)
		variable.setDescription(searchInMap(map, 'discription') as String)
		variable
	}
	
	def FormatDataType convertToFormatDataType(String format){
		switch (format) {
			case 'Float':
				FormatDataType.FLOAT
			case 'Dec':
				FormatDataType.DEC
			case 'Hex':
				FormatDataType.HEX
			case 'Bin':
				FormatDataType.BIN
			case 'Bool':
				FormatDataType.BOOL
			case 'Oct':
				FormatDataType.OCT
				
		}
	}
	
	def Variable createVariable(Variable variable, HashMap<String, Object> map){
		var tmpvariable = createAbstractVariable(variable, map) as Variable
		//tmpvariable?.setDataType((searchInMap(map, 'dataType') as String) == 'Double' ? DataType.DOUBLE : DataType.STRING)
		tmpvariable?.setDataType(DataType.DOUBLE)
		tmpvariable?.setUnit(searchInMap(map, 'unit') as String)
		tmpvariable?.setVariableFormat(({
			var variableformat = conditions.createVariableFormat
			//variableformat.digits = 
			variableformat?.setBaseDataType((searchInMap(map, 'baseDataType') as String).convertToFormatDataType)
			variableformat?.setUpperCase(false)
			variableformat
		}))
		//tmpvariable.variableFormatTmplParam = ''
		tmpvariable
	}
	
	def SignalVariable createSignalVariable(HashMap<String, Object> map){
		var signalvariable = conditions.createSignalVariable
		signalvariable = createVariable(signalvariable, map) as SignalVariable
		signalvariable?.setInterpretedValue((searchInMap(map, 'interupptedValue') as String)=='true' ? BooleanOrTemplatePlaceholderEnum.TRUE : BooleanOrTemplatePlaceholderEnum.FALSE)
		signalvariable?.setSignal(({
			searchInMap(map, 'signal') as AbstractSignal
		}))
		//signalvariable.interpretedValueTmplParam = ''
		val observers = keyToList(map, 'observers')
		for(observer : observers){
			signalvariable.signalObservers.add(_obsgen.createSignalObserver(observer))
		}
		signalvariable.lag = SignalVariableLagEnum.CURRENT//SignalVariableLagEnum.get(searchInMap(map, 'lag') as String)//CURRENT
		signalvariable
	}
	
	def ValueVariable createValueVariable(HashMap<String, Object> map){
		var valuevariable = conditions.createValueVariable
		valuevariable = createVariable(valuevariable, map) as ValueVariable
		valuevariable.initialValue = (searchInMap(map, 'initialValue') as Double).toString
		val observers = keyToList(map, 'observers')
		for(observer : observers){
			valuevariable.valueVariableObservers.add(_obsgen.createValueVariableObserver(observer))
		}
		valuevariable
	}
}