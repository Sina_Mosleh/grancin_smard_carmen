package com.bmw.vv.grancin.codegen.unittest

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import com.bmw.vv.grancin.FieldAccess
import com.bmw.vv.gherkin.AbstractScenario
import java.util.List
import com.bmw.vv.gherkin.ScenarioOutline

import static extension org.eclipse.xtext.EcoreUtil2.*
import org.franca.core.franca.FModel
import com.bmw.vv.gherkin.Scenario
import com.bmw.vv.grancin.Payload
import org.franca.core.franca.FModelElement
import org.eclipse.emf.ecore.EObject
import java.util.AbstractSet

//import com.bmw.vv.grancin.Operation

class UtGenerator extends AbstractGenerator {
	val List<String> l = newArrayList
    override void doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
    	println("+++ UtGenerator +++")
    	
    	val ops = resource.allContents.toList.filter(FieldAccess)
//    	ops.forEach[ println(it.oper)] 
    	
//    	print(ops.toList.length)
//    	
//    	val model = ops.head.name.getContainerOfType(FModelElement)
//    	print(model.name ?: "is leider null")
    	
    	val scs = resource.allContents.toList.filter(AbstractScenario)
    	
    	/*
    	 * First Alternative
    	 */
    	scs.forEach[
    		val x = it.which 
    		if(x instanceof Scenario)  
    			l.add(x.name.name)
    		else 
    		if(x instanceof ScenarioOutline)  
    			l.add(x.name.name)
    	]
    	l.forEach[println(it)]
    
    	/*
    	 * Second Alternative
    	 */
    	scs.forEach[
    		val x = it.which
    		switch x {
    			Scenario 		: l.add(x.name.name)
    			ScenarioOutline : l.add(x.name.name)
    		} 
    	]
    	l.forEach[println(it)]	
}
    
    
/*
 * TODO For any given 'Scenario Outline' determine the number of resulting test cases
 * 		where each line in the corresponding 'Examples' table shall be represented by
 * 		a separate test case i.e. count the number of lines in the 'Examples' table 
 */
	def int numberOfTestCases(ScenarioOutline so) {
		42
	}
} 
    
/*
 * TODO Provide meaningful titles for Test Cases by implementing method 'build()'
 * 
 * Given an AbstractScenario (Scenario OR Scenario Outline)
 * provide a name for the corresponging GTest case(s).
 * The name of the corresponding test case ..
 * 	1) shall be based on the title of the scenario
 *  2) shall contain the given delimiter instead of any space 
 * 	3) shall remove all given 'stop words' from the scenario title
 */
class TestCaseNameBuilder {
	static def Builder load(AbstractScenario asc) {
		return new Builder(asc)
	}
	
	/*
	 * application of the builder pattern ..
	 */
	static final class Builder {
		var AbstractScenario a
		var String delimiter = '_'
		var List<String> stopWords = List.of("a","is")
		
		new(AbstractScenario asc) {
			a=asc
		}
		
		def Builder withDelimiter(String d) {
			delimiter=d
			this
		}
		
		def Builder withStopWords(List<String> sw) {
			stopWords = sw
			this
		}
		
		def String build() {
			"<to be implemented>"
		}
	}
} 
