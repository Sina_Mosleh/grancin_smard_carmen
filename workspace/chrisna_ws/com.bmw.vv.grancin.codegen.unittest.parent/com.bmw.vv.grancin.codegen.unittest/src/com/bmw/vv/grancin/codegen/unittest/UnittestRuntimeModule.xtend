/*
 * generated by Xtext 2.16.0
 */
package com.bmw.vv.grancin.codegen.unittest


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class UnittestRuntimeModule extends AbstractUnittestRuntimeModule {
}
