package com.bmw.vv.grancin.codegen.unittest;

import org.eclipse.xtext.service.AbstractGenericModule;   

public class UtGeneratorModule extends AbstractGenericModule {
    public Class<? extends org.eclipse.xtext.generator.IGenerator2> bindIGenerate() { 
        return UtGenerator.class;
    }
}