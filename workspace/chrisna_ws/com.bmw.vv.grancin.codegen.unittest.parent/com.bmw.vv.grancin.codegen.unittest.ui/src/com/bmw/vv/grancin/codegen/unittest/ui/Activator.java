package com.bmw.vv.grancin.codegen.unittest.ui;

import org.apache.log4j.Logger;   
import org.eclipse.ui.plugin.AbstractUIPlugin;   
import org.eclipse.xtext.ui.shared.SharedStateModule;   
import org.eclipse.xtext.util.Modules2;   
import org.osgi.framework.BundleContext;   
import com.bmw.vv.GrancinRuntimeModule;   
import com.bmw.vv.grancin.codegen.unittest.UtGeneratorModule;   
import com.bmw.vv.ui.GrancinUiModule;   
import com.google.inject.Guice;   
import com.google.inject.Injector;   

public class Activator extends AbstractUIPlugin {  
    private Injector injector;  
    private static Activator INSTANCE;  
  
    @Override public void start(BundleContext context) throws Exception {  
        super.start(context);  
        INSTANCE = this;  
        try { 
            injector = Guice.createInjector(Modules2.mixin(  
                new GrancinRuntimeModule(),
                new SharedStateModule(),
                new GrancinUiModule(this),
                new UtGeneratorModule(),
                new UtGeneratorUIModule(this)));
        } catch (Exception e) {
            Logger.getLogger(getClass()).error(e.getMessage(),e);  throw e;}
        }
  
    @Override public void stop(BundleContext context) throws Exception {
        injector = null;  
        super.stop(context);
    } 
  
    public static Activator getInstance() {
        return INSTANCE;
    }  
  
    public Injector getInjector() {
        return injector;
    }
}
