package com.bmw.vv.grancin.codegen.chrisna.tests

import com.bmw.vv.grancin.File
import com.bmw.vv.grancin.codegen.chrisna.ChrisnaFSMGenerator
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import java.util.List

@ExtendWith(InjectionExtension)
@InjectWith(GrancinInjectorProvider)
class GrancinIntermediateModelTest implements possibleScensToTestGrancinGenNChrisnaFSM {
	@Inject ParseHelper<File> parser
   
	def List<Integer> counting(File model){
		val csa = new ChrisnaFSMGenerator(model.eResource)
		var totalassignemnts = 0
		var totalevents = 0
		var totalstates = 0
		var totalconditions = 0
		var totalscenarios = 0
		totalscenarios= csa.scenariocontainer.getScenario.length
		for(scen : csa.scenariocontainer.getScenario){
			totalstates = totalstates + scen.getCurrentStates.getState.length + scen.getNextStates.getState.length
			totalconditions = totalconditions + scen.getCondcontainer.getCondition.length
			for(state : scen.getCurrentStates.getState){
				totalevents = totalevents + state.getAction.getSendevent.length
				for(event : state.getAction.getSendevent){
						totalassignemnts = totalassignemnts + event.getAssigcontainer.getAssignment.length	
				}
			}
			for(state : scen.getNextStates.getState){
				totalevents = totalevents + state.getAction.getSendevent.length
				for(event : state.getAction.getSendevent){
						totalassignemnts = totalassignemnts + event.getAssigcontainer.getAssignment.length	
				}
			}
			for(condition : scen.getCondcontainer.getCondition){
				totalevents = totalevents + condition.getAction.getSendevent.length
				for(event : condition.getAction.getSendevent){
						totalassignemnts = totalassignemnts + event.getAssigcontainer.getAssignment.length	
				}
			}
		}
		val List<Integer> array = #[totalassignemnts, totalevents, totalstates, totalconditions, totalscenarios]
		array
	} 
	
	@Test
	def void titleOnly() {
		val model = parser.parse(FeatureTitle.key)
		
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 0)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 0)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 0)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 0)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 0)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)
		Assertions.assertTrue(res.get(1) == 0)
		Assertions.assertTrue(res.get(2) == 0)
		Assertions.assertTrue(res.get(3) == 0)
		Assertions.assertTrue(res.get(4) == 0)
	}
	
	@Test
	def void titleAndDescription() {
		val model = parser.parse(
		'''
		@bla @blaBla
		Feature: ApprovalUnit
		Some description is informative''')
			
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 0)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 0)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 0)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 0)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 0)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)
		Assertions.assertTrue(res.get(1) == 0)
		Assertions.assertTrue(res.get(2) == 0)
		Assertions.assertTrue(res.get(3) == 0)
		Assertions.assertTrue(res.get(4) == 0)
	}

	@Test
	def void oneScenarioOnly() {
		val model = parser.parse(FeatureTitle.key + '\n' + oneScenWithOneGiven.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 2)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 2)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 2)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 0)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 25)
		Assertions.assertTrue(res.get(1) == 29)
		Assertions.assertTrue(res.get(2) == 22)
		Assertions.assertTrue(res.get(3) == 19)
		Assertions.assertTrue(res.get(4) == 8)
	}
	
	@Test
	def void oneScenarioResultingToInitState() {
		val model = parser.parse(FeatureTitle.key + '\n' + oneScenResultingToInitState.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 1)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 1)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 2)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 0)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 26)
		Assertions.assertTrue(res.get(1) == 26)
		Assertions.assertTrue(res.get(2) == 20)
		Assertions.assertTrue(res.get(3) == 18)
		Assertions.assertTrue(res.get(4) == 7)
	}
	
	@Test
	def void twoScenariosResultingToInitState() {
		val model = parser.parse(FeatureTitle.key + '\n' + twoScensResultingToInitState.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 6)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 6)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 4)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 2)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 2)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 96)
		Assertions.assertTrue(res.get(1) == 98)
		Assertions.assertTrue(res.get(2) == 68)
		Assertions.assertTrue(res.get(3) == 66)
		Assertions.assertTrue(res.get(4) == 22)
	}
	
	@Test
	def void featureWithOneScenarioWithoutDoNothing() {
		val model = parser.parse(FeatureTitle.key + '\n' + scenWithOneScenWithoutDoNothing.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 3)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 3)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 2)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 1)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 48)
		Assertions.assertTrue(res.get(1) == 49)
		Assertions.assertTrue(res.get(2) == 34)
		Assertions.assertTrue(res.get(3) == 33)
		Assertions.assertTrue(res.get(4) == 11)
	}
	
	@Test
	def void featureWithOneScenarioWithDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenWithDoNothing.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 3)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 4)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 3)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 1)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 48)
		Assertions.assertTrue(res.get(1) == 49)
		Assertions.assertTrue(res.get(2) == 34)
		Assertions.assertTrue(res.get(3) == 33)
		Assertions.assertTrue(res.get(4) == 11)
	}
	
	@Test
	def void featureWithOneScenarioAlsoWithDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenAlsoWithDoNothing.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 3)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 4)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 3)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 1)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 48)
		Assertions.assertTrue(res.get(1) == 49)
		Assertions.assertTrue(res.get(2) == 34)
		Assertions.assertTrue(res.get(3) == 33)
		Assertions.assertTrue(res.get(4) == 11)
	}
	
	@Test
	def void featureWithOneScenarioWithTwoDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenWithTwoDoNothing.key)		
		//val csa = new ChrisnaFSMGenerator(model.eResource)
		//Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 2)
		//Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 4)
		//Assertions.assertTrue(csa.stcontainer.getState.length == 3)
		//Assertions.assertTrue(csa.condcontainer.getCondition.length == 1)
		//Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		//Assertions.assertEquals(csa.name,"ApprovalUnit")
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 49)
		Assertions.assertTrue(res.get(1) == 49)
		Assertions.assertTrue(res.get(2) == 34)
		Assertions.assertTrue(res.get(3) == 33)
		Assertions.assertTrue(res.get(4) == 11)
	}
	
	@Test
	def void featureWithTwoScenariosWithoutDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithTwoScenWithoutDoNothing.key)		
		/*val csa = new ChrisnaFSMGenerator(model?.eResource)
		Assertions.assertTrue(csa?.assigcontainer?.getAssignment.length == 6)
		Assertions.assertTrue(csa?.eventcontainer?.getSendevent.length == 6)
		Assertions.assertTrue(csa?.stcontainer?.getState.length == 4)
		Assertions.assertTrue(csa?.condcontainer?.getCondition.length == 2)
		Assertions.assertTrue(csa?.scenariocontainer?.getScenario.length == 2)
		Assertions.assertEquals(csa?.name,"ApprovalUnit")*/
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 96)
		Assertions.assertTrue(res.get(1) == 98)
		Assertions.assertTrue(res.get(2) == 68)
		Assertions.assertTrue(res.get(3) == 66)
		Assertions.assertTrue(res.get(4) == 22)
	}
	
	@Test
	def void featureWithOnlyAThenStep() {
		val model = parser.parse(FeatureTitle.key + scenWithOnlyAThenStep.key)		
		/*val csa = new ChrisnaFSMGenerator(model.eResource)
		Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 2)
		Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 2)
		Assertions.assertTrue(csa.stcontainer.getState.length == 2)
		Assertions.assertTrue(csa.condcontainer.getCondition.length == 0)
		Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		Assertions.assertEquals(csa.name,"ApprovalUnit")*/
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 49)
		Assertions.assertTrue(res.get(1) == 49)
		Assertions.assertTrue(res.get(2) == 34)
		Assertions.assertTrue(res.get(3) == 33)
		Assertions.assertTrue(res.get(4) == 11)
	}
	@Test
	def void featureWithOnlyAGivenStep() {
		val model = parser.parse(FeatureTitle.key + featureWithOnlyAGivenStep.key)		
		/*val csa = new ChrisnaFSMGenerator(model.eResource)
		Assertions.assertTrue(csa.assigcontainer.getAssignment.length == 3)
		Assertions.assertTrue(csa.eventcontainer.getSendevent.length == 4)
		Assertions.assertTrue(csa.stcontainer.getState.length == 3)
		Assertions.assertTrue(csa.condcontainer.getCondition.length == 1)
		Assertions.assertTrue(csa.scenariocontainer.getScenario.length == 1)
		Assertions.assertEquals(csa.name,"ApprovalUnit")*/
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 20)
		Assertions.assertTrue(res.get(1) == 27)
		Assertions.assertTrue(res.get(2) == 20)
		Assertions.assertTrue(res.get(3) == 19)
		Assertions.assertTrue(res.get(4) == 7)
	}
}