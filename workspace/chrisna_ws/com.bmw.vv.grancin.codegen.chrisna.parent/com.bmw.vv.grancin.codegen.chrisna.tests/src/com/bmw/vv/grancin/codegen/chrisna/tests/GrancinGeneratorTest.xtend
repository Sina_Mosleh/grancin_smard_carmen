package com.bmw.vv.grancin.codegen.chrisna.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.xbase.testing.CompilationTestHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(GrancinInjectorProvider)

class GrancinGeneratorTest implements possibleScensToTestGrancinGenNChrisnaFSM {
	@Inject extension CompilationTestHelper
     
  @Test 
    def dummy() {
    	Assertions.assertEquals(1,1)
    }
    
  @Test
    def FeatureTitle(){
    	FeatureTitle.key.assertCompilesTo(FeatureTitle.value)
    }
    
  /*@Test
    def oneScenarioWithOneGiven(){
    	(FeatureTitle.key + oneScenWithOneGiven.key).assertCompilesTo(FeatureTitle.value + oneScenWithOneGiven.value)
    }
    
  @Test
    def oneScenarioResultingToInitState(){
    	(FeatureTitle.key + oneScenResultingToInitState.key).assertCompilesTo(FeatureTitle.value + oneScenResultingToInitState.value)
    }
    
  @Test
    def twoScenariosResultingToInitState(){
    	(FeatureTitle.key + twoScensResultingToInitState.key).assertCompilesTo(FeatureTitle.value + twoScensResultingToInitState.value)
    }*/
}