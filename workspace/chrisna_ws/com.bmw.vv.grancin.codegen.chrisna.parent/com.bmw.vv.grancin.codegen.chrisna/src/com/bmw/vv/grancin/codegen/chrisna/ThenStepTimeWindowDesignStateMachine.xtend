package com.bmw.vv.grancin.codegen.chrisna

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap
import chrisna.TimeWindow

class ThenStepTimeWindowDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genTimeWindowDesignStateMachine
	}
	
	def genTimeWindowDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		insertState(0, 0, name, genState('Wait_for_change_' + name, genEvent('set pre value', 'Compute', new ArrayList<HashMap<String, String>>()
		)), genCondition(name, genEvent('is changed', 'Not', {
			expressions?.clear
			for(assignment : (sendedevent?.time as TimeWindow)?.assigcontainer?.assignment)
				expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
			expressions
		}), null, null))
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Ok_' + name, null))
			//scen.nextStates.state.add(statemachine?.initstate)
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
	}
	
}