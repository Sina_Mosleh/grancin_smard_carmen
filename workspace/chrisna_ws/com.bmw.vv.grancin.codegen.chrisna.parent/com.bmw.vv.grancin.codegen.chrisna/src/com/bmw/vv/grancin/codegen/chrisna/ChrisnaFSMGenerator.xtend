package com.bmw.vv.grancin.codegen.chrisna



//import java.util.*

import chrisna.AssigContainer
import chrisna.Assignment
import chrisna.ChrisnaFSM
import chrisna.CondContainer
import chrisna.Counter
import chrisna.EventContainer
import chrisna.Filter
import chrisna.HistoryDispach
import chrisna.NodeFinder
import chrisna.OnChange
import chrisna.Periodically
import chrisna.ScenContainer
import chrisna.ScenContainerWithTime
import chrisna.ScenContainerWithoutTime
import chrisna.Scenario
import chrisna.SendEvent
import chrisna.Time
import chrisna.Trigger
import chrisna.WarningError
import chrisna.impl.ChrisnaFactoryImpl
import chrisna.initState
import chrisna.state
import chrisna.stateContainer
import com.bmw.vv.gherkin.AbstractScenario
import com.bmw.vv.gherkin.AndInputStep
import com.bmw.vv.gherkin.AndOutputStep
import com.bmw.vv.gherkin.Feature
import com.bmw.vv.gherkin.GivenStep
import com.bmw.vv.gherkin.ScenarioOutline
import com.bmw.vv.gherkin.ThenStep
import com.bmw.vv.gherkin.WhenStep
import com.bmw.vv.gherkin.impl.GherkinFactoryImpl
import com.bmw.vv.gherkin.impl.GivenStepImpl
import com.bmw.vv.gherkin.impl.ScenarioImpl
import com.bmw.vv.gherkin.impl.ScenarioOutlineImpl
import com.bmw.vv.gherkin.impl.ThenStepImpl
import com.bmw.vv.gherkin.impl.WhenStepImpl
import com.bmw.vv.grancin.Attribute
import com.bmw.vv.grancin.Constant
import com.bmw.vv.grancin.FloatConstant
import com.bmw.vv.grancin.GenericTimeConstraint
import com.bmw.vv.grancin.InMethod
import com.bmw.vv.grancin.IntConstant
import com.bmw.vv.grancin.Payload
import com.bmw.vv.grancin.PeriodicTiming
import com.bmw.vv.grancin.Reference
import com.bmw.vv.grancin.ReferenceID
import com.bmw.vv.grancin.SendNoMethod
import com.bmw.vv.grancin.StepInBody
import com.bmw.vv.grancin.StepOutBody
import com.bmw.vv.grancin.TimeWindow
import com.bmw.vv.grancin.impl.AttributeImpl
import com.bmw.vv.grancin.impl.BoolConstantImpl
import com.bmw.vv.grancin.impl.ContinuationImpl
import com.bmw.vv.grancin.impl.FloatConstantImpl
import com.bmw.vv.grancin.impl.InMethodImpl
import com.bmw.vv.grancin.impl.IntConstantImpl
import com.bmw.vv.grancin.impl.NotANumberImpl
import com.bmw.vv.grancin.impl.OnChangeTimingImpl
import com.bmw.vv.grancin.impl.PeriodicTimingImpl
import com.bmw.vv.grancin.impl.SendMethodImpl
import com.bmw.vv.grancin.impl.SendNoMethodImpl
import com.bmw.vv.grancin.impl.StringConstantImpl
import com.bmw.vv.grancin.impl.TableCellsImpl
import com.bmw.vv.grancin.impl.TimeWindowImpl
import com.bmw.vv.grancin.impl.WaitImpl
import java.util.ArrayList
import java.util.HashMap
//import org.eclipse.emf.common.util.BasicEList
//import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.resource.Resource
import org.franca.core.franca.impl.FModelElementImpl
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.BasicEList
import chrisna.Condition

class ChrisnaFSMGenerator {
	
	
	static final HashMap<String, String> NoTable = null;
	protected var int stateCounter;
	protected var int condCounter;
	//static final boolean stepin = false;
	//static final boolean stepout = true;
	//static final Object lock = new Object()
	protected var String name
	protected var AssigContainer assigcontainer
	//var EList<Assignment> listassignments
	protected var EventContainer eventcontainer
	protected var stateContainer stcontainer
	protected var CondContainer condcontainer
	protected var ScenContainer scenariocontainer
	protected var ScenContainerWithTime scencontainerwithtime
	protected var state initst
	protected var ScenContainerWithoutTime scencontainerwithouttime
	protected var HistoryDispach historydispach
	protected var Filter filter
	protected var ChrisnaFSM chrisnafsm
	protected var Counter counter
	protected var WarningError warningerrors
	protected var NodeFinder nodes
	protected var Scenario scen
	protected var boolean hasGiven = false
	protected var boolean hasWhen = false
	protected var boolean hasThen = false
	//var chrisna.stateContainer statcontainer
	//var chrisna.CondContainer concontainer
	
	protected val Resource Res
	protected var ChrisnaFactoryImpl gen = new ChrisnaFactoryImpl()
	protected var GherkinFactoryImpl gher = new GherkinFactoryImpl()
	
	
	new (Resource R){
		this.Res = R
		this.CreateAll()
		generator(this.Res)
	}
	
	def String getName(){
		return this.name
	}
	
	def AssigContainer getAssigcontainer(){
		return this.assigcontainer
	}
	
	def EventContainer getEventcontainer(){
		return this.eventcontainer
	}
	
	def stateContainer getStcontainer(){
		return this.stcontainer
	}
	
	def CondContainer getCondcontainer(){
		return this.condcontainer
	}
	
	def ScenContainer getScenariocontainer(){
		return this.scenariocontainer
	}
	
	def ChrisnaFSM getchrisnafsm(){
		return this.chrisnafsm
	}
	
	def state getInitState(){
		return this.initst
	}
	
	def setInitState(state initialstate){
		this.initst = initialstate
	}
	
	def void CreateAll(){
		//gen = com.bmw.vv.chrisna.impl.ChrisnaFactoryImpl.init()
		stateCounter = 0;
		condCounter = 0;
		assigcontainer = gen?.createAssigContainer()
		//listassignments = assigcontainer.getAssignment
		eventcontainer = gen?.createEventContainer()
		stcontainer = gen?.createstateContainer()
		condcontainer = gen?.createCondContainer()
		scenariocontainer = gen?.createScenContainer()
		scencontainerwithtime = gen?.createScenContainerWithTime()
		//initst = gen?.createinitState()
		scencontainerwithouttime = gen?.createScenContainerWithoutTime()
		historydispach = gen?.createHistoryDispach()
		filter = gen?.createFilter()
		chrisnafsm = gen?.createChrisnaFSM()
		counter = gen?.createCounter()
		warningerrors = gen?.createWarningError()
		nodes = gen?.createNodeFinder()
	}
	
	/*protected def com.bmw.vv.chrisna.initState findingInit(Resource feature){
		//val initial = gen.createinitState()
		val allScenarios = feature.allContents.toIterable.filter(AbstractScenario)
		allScenarios.forEach[
			val allSteps = (it.getWhich() as com.bmw.vv.gherkin.Scenario).getSteps()
			allSteps.forEach[
				switch(it.getWhich()){
					case GivenStep:
						
				}
				if((!((it.getWhich()).eIsSet(GherkinPackage.GIVEN_STEP)) && !it.eIsSet(GherkinPackage.WHEN_STEP)) || it.eGet(GherkinPackage.THEN_STEP, false, false).toString().ToLower().contain("nothing"))
				{
					if(it.eIsSet(GherkinPackage.THEN_STEP)
					{
						val actions = new actionContainer()
						actions.action.add(it.eGet(GherkinPackage.THEN_STEP, false, false).toString() - "Then")
						gen.createinitState.setActioncontainer(actions)
					}
				}
			]
		]
		if (initial == null)
		{
			throw new IllegalStateException("No Init")
		}
		initial
	}*/
	
	def Time getTimer(TimeWindow time){
		var chrisna.TimeWindow timer = gen?.createTimeWindow
		var Assignment assign = gen?.createAssignment
		var Assignment assign_tmp = gen?.createAssignment
		var AssigContainer assigncontainer = gen?.createAssigContainer()
		(timer as chrisna.TimeWindow)?.setType("com.bmw.vv.chrisna.TimeWindow")
		switch ((time as TimeWindow)?.getValT()?.getClass){
			case (IntConstantImpl): (timer as chrisna.TimeWindow)?.setTime((((time as TimeWindow)?.getValT() as IntConstantImpl).getConst() as float))
			case FloatConstantImpl: (timer as chrisna.TimeWindow).setTime(((time as TimeWindow)?.getValT() as FloatConstantImpl).getConst())
			default: (timer as chrisna.TimeWindow)?.setTime(0)
		}
		(timer as chrisna.TimeWindow)?.setUnit((time as TimeWindow)?.getUnitT())
		if ((time as TimeWindow)?.getParLeft() !== null){
			assign?.setLeftSide((time as TimeWindow)?.getParLeft())
			assign_tmp?.setLeftSide((time as TimeWindow)?.getParLeft())
		}
		if((time as TimeWindow)?.getParRight()!== null){
			assign?.setRightSide((time as TimeWindow)?.getParRight())
			assign_tmp?.setRightSide((time as TimeWindow)?.getParRight())
		}
		if((time as TimeWindow)?.getOper() !== null){
			assign?.setOperation(gen?.createOperation)
			assign?.getOperation()?.setStr((time as TimeWindow)?.getOper())
			assign_tmp?.setOperation(gen?.createOperation)
			assign_tmp?.getOperation()?.setStr((time as TimeWindow)?.getOper())
		}
		if((time as TimeWindow)?.getParLeft() !== null && (time as TimeWindow)?.getOper() !== null)
		{
			this.assigcontainer?.getAssignment()?.add(assign_tmp)
			//addingEleinListAssign(assign)
			//this.listassignments?.add(assign)
			assigncontainer?.getAssignment()?.add(assign)
		}
		
		(timer as chrisna.TimeWindow)?.setAssigcontainer(assigncontainer)
		timer
	}
	
	protected def Time getTimer(GenericTimeConstraint time){
		var Time timer = gen?.createTime()
		switch time?.getType()?.getClass{
			case PeriodicTimingImpl:
			{
				(timer as Periodically)?.setType("com.bmw.vv.chrisna.Periodically")
				if (((time?.getType as PeriodicTiming)?.getVal()).getClass == IntConstantImpl){
					(timer as Periodically)?.setTime((((time?.getType() as PeriodicTimingImpl)?.getVal() as IntConstant).getConst() as float))
				}
				if(((time?.getType as PeriodicTiming)?.getVal()).getClass == FloatConstantImpl){
						(timer as Periodically)?.setTime(((time?.getType() as PeriodicTimingImpl)?.getVal() as FloatConstant).getConst())
				}
				(timer as Periodically)?.setUnit(((time?.getType() as PeriodicTimingImpl)?.getUnitT()))
				//timer
			}
				
			case OnChangeTimingImpl:
			{
				(timer as OnChange)?.setType("com.bmw.vv.chrisna.OnChange")
				(timer as OnChange)?.setName((time?.getType() as OnChangeTimingImpl)?.getName()?.toString())
				//timer	
			}
				
			case (TimeWindowImpl):
			{
				getTimer(time?.getType() as TimeWindowImpl)
			}
		}
		timer
	}
	
	protected def void PayLoadTranslation(Payload payload, AssigContainer assigncontainer, HashMap<String, String> map){
		payload?.getField()?.forEach[
			var Assignment assign = gen?.createAssignment
			var Assignment assign_tmp = gen?.createAssignment
			assign?.setLeftSide(it?.getName?.getName + 
				if(it?.getHasMembers?.getName?.getName !== null)
					'.' + it?.getHasMembers?.getName?.getName
				else
					"")
			assign?.setRightSide(ReferenceTranslation(it?.getVal(), map))
			assign?.setOperation(gen?.createOperation)
			assign?.getOperation()?.setStr(it?.getOper())
			
			assign_tmp?.setLeftSide(it?.getName?.getName + 
				if(it?.getHasMembers?.getName?.getName !== null)
					'.' + it?.getHasMembers?.getName?.getName
				else
					"")
			assign_tmp?.setRightSide(ReferenceTranslation(it?.getVal(), map))
			assign_tmp?.setOperation(gen?.createOperation)
			assign_tmp?.getOperation()?.setStr(it?.getOper())
			
			this.assigcontainer?.getAssignment()?.add(assign_tmp)
			//addingEleinListAssign(assign)
			//this.listassignments?.add(assign)
			assigncontainer?.getAssignment()?.add(assign)
		]
	}
	
	protected def String ConstantTranslation(Constant const){
		switch const?.getType?.getClass {
				case IntConstantImpl: (const?.getType as IntConstantImpl).getConst.toString
				case FloatConstantImpl: (const?.getType as FloatConstantImpl).getConst.toString
				case BoolConstantImpl: (const?.getType as BoolConstantImpl).isConst.toString
				case StringConstantImpl: (const?.getType as StringConstantImpl)?.getConst
				case NotANumberImpl: (const?.getType as NotANumberImpl)?.getConst
				default: 'None'
			}
	}
	
	protected def String ReferenceTranslation(Reference ref, HashMap<String, String> map){
		if(ref?.getConst !== null)
		{
			ConstantTranslation(ref?.getConst)
		}
		else if(ref?.getRefe !== null){
			ReferenceidTranslation(ref.getRefe, map)
			//ref?.getRefe?.getRef
		}
		else
			'None'
	}
	
	protected def String ReferenceidTranslation(ReferenceID ref, HashMap<String , String> map){
		if(map.containsKey(ref?.getRef))
				map.get(ref?.getRef)
	}
	
	protected def SendEvent AttributeTranslation(Attribute attribute, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
		var SendEvent event_tmp = gen?.createSendEvent
		var Assignment assign = gen?.createAssignment
		var Assignment assign_tmp = gen?.createAssignment
		var AssigContainer assigncontainer = gen?.createAssigContainer
		//event?.setEvent(attribute?.getEvent)
		event?.setEvent('attribute')
		
		//event_tmp?.setEvent(attribute?.getEvent)
		event_tmp?.setEvent('attribute')
		
		if (attribute?.getAttribute?.getIdx?.getInt?.const !== 0){
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + attribute?.getAttribute?.getIdx?.getInt.const.toString + "]" +
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				("." + attribute?.getAttribute?.getField?.getName?.getName)
			else
				"" )
				
			assign_tmp?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + attribute?.getAttribute?.getIdx?.getInt.const.toString + "]" +
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				("." + attribute?.getAttribute?.getField?.getName?.getName)
			else
				"" )
		}
		else if(attribute?.getAttribute?.getIdx?.getRef !== null){
			val value = ReferenceidTranslation(attribute?.getAttribute?.getIdx?.getRef, map)
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + value + "]" + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				"." + attribute?.getAttribute?.getField?.getName?.getName
			else
				"" )
				
			assign_tmp?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			"[" + value + "]" + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null) 
				"." + attribute?.getAttribute?.getField?.getName?.getName
			else
				"" )
		}
		else {
			assign?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null)
				'.' + attribute?.getAttribute?.getField?.getName?.getName
			else
				"")
				
			assign_tmp?.setLeftSide(attribute?.getAttribute?.getName?.getName + 
			if(attribute?.getAttribute?.getField?.getName?.getName !== null)
				'.' + attribute?.getAttribute?.getField?.getName?.getName
			else
				"")
		}	
		assign?.setRightSide(ReferenceTranslation(attribute?.getPayload(), map))
		assign?.setOperation(gen?.createOperation)
		assign?.getOperation()?.setStr(attribute?.getEvent)
		
		assign_tmp?.setRightSide(ReferenceTranslation(attribute?.getPayload(), map))
		assign_tmp?.setOperation(gen?.createOperation)
		assign_tmp?.getOperation()?.setStr(attribute?.getEvent)
		
		//assign_tmp = assign
		this.assigcontainer?.getAssignment()?.add(assign_tmp)
		//addingEleinListAssign(assign_tmp)
		//this.listassignments?.add(assign_tmp)
		assigncontainer?.getAssignment()?.add(assign)
		
		event?.setAssigcontainer(assigncontainer)
		event?.setMethod("")
		event?.setTime(getTimer((attribute)?.getTw()))
		
		event_tmp?.setAssigcontainer(assigncontainer)
		event_tmp?.setMethod("")
		event_tmp?.setTime(getTimer((attribute)?.getTw()))
		
		event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
		
		this.eventcontainer?.getSendevent()?.add(event_tmp)
		event
	}
	
	protected def SendEvent StepInBodyTranslation(StepInBody stepinbody, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
		var SendEvent event_tmp = gen?.createSendEvent
		if (stepinbody?.getStub() !== null)
		{
			//event?.setEvent(stepinbody?.getStub())
			if(stepinbody?.getStub().toLowerCase.contains('no') && stepinbody?.getStub().toLowerCase.contains('precondition')){
				event?.setEvent('no precondition')
				event?.setMethod('')
				event?.setAssigcontainer(gen?.createAssigContainer)
				
				event_tmp?.setEvent('no precondition')
				event_tmp?.setMethod('')
				event_tmp?.setAssigcontainer(gen?.createAssigContainer)
				
				hasGiven = false
			}
			if(stepinbody?.getStub().toLowerCase.contains('no') && stepinbody?.getStub().toLowerCase.contains('trigger')){
				event?.setEvent('no trigger')
				event?.setMethod('')
				event?.setAssigcontainer(gen?.createAssigContainer)
				
				event_tmp?.setEvent('no trigger')
				event_tmp?.setMethod('')
				event_tmp?.setAssigcontainer(gen?.createAssigContainer)
				
				hasWhen = false
			}
		}
		else{
			switch stepinbody?.getType()?.getClass {
				case InMethodImpl:
				{
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					event?.setEvent("in method")
					event?.setMethod(((stepinbody?.getType() as InMethodImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					event_tmp?.setEvent("in method")
					event_tmp?.setMethod(((stepinbody?.getType() as InMethodImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					PayLoadTranslation((stepinbody?.getType() as InMethodImpl)?.getPayload(), assigncontainer, map)
					event?.setAssigcontainer(assigncontainer)
					event?.setTime(getTimer((stepinbody?.getType() as InMethod)?.getTw()))
					
					event_tmp?.setAssigcontainer(assigncontainer)
					event_tmp?.setTime(getTimer((stepinbody?.getType() as InMethod)?.getTw()))
					
					event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
					
					this.eventcontainer?.getSendevent()?.add(event_tmp)
				}
				case AttributeImpl:
					event = AttributeTranslation(stepinbody?.getType() as AttributeImpl, map)
					
				//case Reproduction:
				
				case ContinuationImpl:
				{
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					event?.setEvent("continuation")
					event?.setMethod(((stepinbody?.getType() as ContinuationImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					event_tmp?.setEvent("continuation")
					event_tmp?.setMethod(((stepinbody?.getType() as ContinuationImpl)?.getMethod() as FModelElementImpl)?.getName)
					
					PayLoadTranslation((stepinbody?.getType() as ContinuationImpl)?.getPayload(), assigncontainer, map)
					event?.setAssigcontainer(assigncontainer)
					
					event_tmp?.setAssigcontainer(assigncontainer)
					
					event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
					
					//event?.setTime(getTimer((stepinbody?.getType() as com.bmw.vv.grancin.impl.ContinuationImpl)?.isTw()))
					this.eventcontainer?.getSendevent()?.add(event_tmp)
				}
					
				case WaitImpl:
				{
					var chrisna.TimeWindow timer = gen?.createTimeWindow
					event?.setEvent("wait to pass")
					event_tmp?.setEvent("wait to pass")
					
					switch ((stepinbody?.getType() as WaitImpl)?.getVal()){
						case IntConstant: (timer as chrisna.TimeWindow)?.setTime((((stepinbody?.getType() as WaitImpl)?.getVal() as IntConstant).getConst() as float))
						case FloatConstant: (timer as chrisna.TimeWindow)?.setTime(((stepinbody?.getType() as WaitImpl)?.getVal() as FloatConstant).getConst())
						default: (timer as chrisna.TimeWindow)?.setTime(0)
					}
					(timer as chrisna.TimeWindow)?.setType('com.bmw.vv.chrisna.TimeWindow')
					(timer as chrisna.TimeWindow)?.setUnit((stepinbody?.getType() as WaitImpl)?.getUnit())
					
					event?.setTime(timer)
					event_tmp?.setTime(timer)
					
					event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
					
					this.eventcontainer?.getSendevent()?.add(event_tmp)
				}
					
			}
		}
		event
	}
	
	protected def SendEvent StepOutBodyTranslation(StepOutBody stepoutbody, HashMap<String, String> map){
		var SendEvent event = gen?.createSendEvent
		var SendEvent event_tmp = gen?.createSendEvent
			switch stepoutbody?.getType()?.getClass {
				case SendNoMethodImpl:
				{
					event?.setEvent("do not send")
					event_tmp?.setEvent("do not send")
					//event?.setPayload(null as AssigContainer)
					event?.setMethod(((stepoutbody?.getType() as SendNoMethod)?.getMethod() as FModelElementImpl)?.getName)
					event_tmp?.setMethod(((stepoutbody?.getType() as SendNoMethod)?.getMethod() as FModelElementImpl)?.getName)
					//event?.setTime(null as Time)
					
					event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
					
					this.eventcontainer?.getSendevent()?.add(event_tmp)
				}
					
				case SendMethodImpl:
				{
					event?.setEvent("do send")
					event_tmp?.setEvent("do send")
					var AssigContainer assigncontainer = gen?.createAssigContainer()
					PayLoadTranslation((stepoutbody?.getType() as SendMethodImpl)?.getPayload(),assigncontainer, map)
					
					event?.setAssigcontainer(assigncontainer)
					event?.setMethod(((stepoutbody?.getType() as SendMethodImpl)?.getMethod() as FModelElementImpl)?.getName)
					event?.setTime(getTimer((stepoutbody?.getType() as SendMethodImpl)?.getTc()))
					
					event_tmp?.setAssigcontainer(assigncontainer)
					event_tmp?.setMethod(((stepoutbody?.getType() as SendMethodImpl)?.getMethod() as FModelElementImpl)?.getName)
					event_tmp?.setTime(getTimer((stepoutbody?.getType() as SendMethodImpl)?.getTc()))
					
					event?.setTrigger(Trigger.ON_ENTRY)
					event_tmp?.setTrigger(Trigger.ON_ENTRY)
					
					this.eventcontainer?.getSendevent()?.add(event_tmp) 
				}
					
				case AttributeImpl:
				{
					event = AttributeTranslation(stepoutbody?.getType() as Attribute, map)
					//event_tmp = AttributeTranslation(stepoutbody?.getType() as Attribute, map)	
					
					//this.eventcontainer?.getSendevent()?.add(event_tmp)
				}
				
				//default: return null as SendEvent
			}
		event
	}
	
	/*protected def void ThenStepTranslation(com.bmw.vv.gherkin.ThenStep thenstep){
		var com.bmw.vv.chrisna.state stat = gen?.createstate
		var com.bmw.vv.chrisna.action act = gen?.createaction()
		//var list = (gen?.createEventContainer()).getSendevent
		act?.getSendevent()?.add(StepOutBodyTranslation(thenstep?.getAction() as com.bmw.vv.grancin.StepOutBody))
		act?.setTrigger(com.bmw.vv.chrisna.Trigger.ON_ENTRY)
		stat?.setAction(act)
		stat?.setDescription(thenstep?.getDesc())
		this.stcontainer?.getState()?.add(stat)
		thenstep?.getAnd()?.forEach[
			var com.bmw.vv.chrisna.state sta = gen?.createstate
			var com.bmw.vv.chrisna.action acti = gen?.createaction
			acti?.getSendevent()?.add(StepOutBodyTranslation(it?.getAction() as com.bmw.vv.grancin.StepOutBody))
			acti?.setTrigger(com.bmw.vv.chrisna.Trigger.ON_ENTRY)
			sta?.setAction(acti)
			sta?.setDescription(it?.getDesc())
			this.stcontainer?.getState()?.add(sta)
		]
		//statecontaier
	}*/
	
	protected def ChrisnaFSM decideWhichStateMachineToGen(SendEvent event, String scenname){
		switch (event?.event){
			case 'attribute':{
				if (event?.time?.type?.contains('TimeWindow')/* && (event?.time as chrisna.TimeWindow)?.time != 0*/){
					var design = new ThenStepTimeWindowDesignStateMachine(scenname, event)
					design?.statemachine
				}
				else {
					var design = new ThenStepAttrArguDesignStateMachine(scenname, event)
					design?.statemachine	
				}
				
			}
			case 'do not send':{
				var design = new ThenStepAttrArguDesignStateMachine(scenname, event)
				design?.statemachine
			}
			case 'do send' :{
				if (event?.time?.type?.contains('Periodically') && (event?.time as Periodically)?.time != 0) {
					var design = new ThenStepPeriodicallySendMethodDesignStateMachine(scenname, event)
					design?.statemachine
				}
				else if (event?.time?.type?.contains('OnChange')) {
					var design = new ThenStepOnChangeDesignStateMachine(scenname, event)
					design?.statemachine
				}
				else if (event?.time?.type?.contains('TimeWindow') && (event?.time as TimeWindow)?.valT != 0){
					var design = new ThenStepTimeWindowDesignStateMachine(scenname, event)
					design?.statemachine
				}
				else {
					var design = new ThenStepAttrArguDesignStateMachine(scenname, event)
					design?.statemachine	
				}
			}
		}
	}
	
	protected def ArrayList<ChrisnaFSM> ThenStepTranslation(ThenStep thenstep, stateContainer statcontainer, HashMap<String, String> map, String scenname){
		//statcontainer = gen?.createstateContainer
		
		//<changed>
		/*var state stat = gen?.createstate
		var state stat_tmp = gen?.createstate
		
		var action act = gen?.createaction()
		act?.getSendevent()?.add(StepOutBodyTranslation(thenstep?.getAction() as StepOutBody, map))*/
		//</changed>
		//act?.setTrigger(Trigger.ON_ENTRY)
		var liststatemachine = new ArrayList<ChrisnaFSM>() 
		var event = StepOutBodyTranslation(thenstep?.getAction() as StepOutBody, map)
		liststatemachine?.add(decideWhichStateMachineToGen(event, scenname))
		
		//<changed>
		/*stat?.setAction(act)
		stat?.setDescription(thenstep?.getDesc())
		stateCounter++
		stat?.setName('state' + stateCounter)
		
		stat_tmp?.setAction(act)
		stat_tmp?.setDescription(thenstep?.getDesc())
		stat_tmp?.setName('state' + stateCounter)
		
		this.stcontainer?.getState()?.add(stat_tmp)
		statcontainer?.getState()?.add(stat)*/
		//</changed>
		
		for(AndOutputStep element : thenstep?.getAnd()){
			//var actions = it?.getAction()
			
			
			//<changed>
			/*var state sta = gen?.createstate
			var state sta_tmp = gen?.createstate
			var action acti = gen?.createaction 
			act?.getSendevent()?.add(StepOutBodyTranslation(element?.getAction() as StepOutBody, map))*/
			//</changed>
			
			var eve = StepOutBodyTranslation(element?.getAction() as StepOutBody, map)
			liststatemachine?.add(decideWhichStateMachineToGen(eve, scenname))
			//acti?.setTrigger(Trigger.ON_ENTRY)
			
			//<changed>
			/*sta?.setAction(acti)
			sta?.setDescription(element?.getDesc())
			
			stateCounter++
			sta?.setName('state' + stateCounter)
			
			sta_tmp?.setAction(acti)
			sta_tmp?.setDescription(element?.getDesc())
			sta_tmp?.setName('state' + stateCounter)
			
			this.stcontainer?.getState()?.add(sta_tmp)
			statcontainer?.getState()?.add(sta)*/
			//</changed>
		}
		liststatemachine
	}
	
	/*protected def void WhenStepTranslation(com.bmw.vv.gherkin.WhenStep whenstep){
		var com.bmw.vv.chrisna.Condition cond = gen?.createCondition
		var com.bmw.vv.chrisna.action act = gen?.createaction()
		act?.getSendevent()?.add(StepInBodyTranslation(whenstep?.getAction() as com.bmw.vv.grancin.StepInBody))
		act?.setTrigger(com.bmw.vv.chrisna.Trigger.ON_ENTRY)
		cond?.setAction(act)
		cond?.setDescription(whenstep?.getDesc())
		this.condcontainer?.getCondition()?.add(cond)
		whenstep?.getAnd()?.forEach[
			var com.bmw.vv.chrisna.Condition con = gen?.createCondition
			var com.bmw.vv.chrisna.action acti = gen?.createaction()
			acti?.getSendevent()?.add(StepInBodyTranslation(it?.getAction() as com.bmw.vv.grancin.StepInBody))
			acti?.setTrigger(com.bmw.vv.chrisna.Trigger.ON_ENTRY)
			con?.setAction(acti)
			con?.setDescription(it?.getDesc())
			this.condcontainer?.getCondition()?.add(con)
		]
	}*/
	
	protected def EventContainer WhenStepTranslation(WhenStep whenstep, CondContainer concontainer, HashMap<String, String> map){
		//this.concontainer = gen?.createCondContainer
		
		//<changed>
		/*var Condition cond = gen?.createCondition
		var Condition cond_tmp = gen?.createCondition
		var action act = gen?.createaction()
		act?.getSendevent()?.add(StepInBodyTranslation(whenstep?.getAction() as StepInBody, map))
		
		
		//act?.setTrigger(Trigger.ON_ENTRY)
		
		cond?.setAction(act)
		cond?.setDescription(whenstep?.getDesc())
		condCounter++
		cond?.setName('condition' + condCounter)
		
		cond_tmp?.setAction(act)
		cond_tmp?.setDescription(whenstep?.getDesc())
		cond_tmp?.setName('condition' + condCounter)
		
		this.condcontainer?.getCondition()?.add(cond_tmp)
		concontainer?.getCondition()?.add(cond)*/
		//</changed>
		
		var listevents = gen?.createEventContainer 
		listevents?.sendevent?.add(StepInBodyTranslation(whenstep?.getAction() as StepInBody, map))
		
		for( AndInputStep element : whenstep?.getAnd()){
			/*var Condition con = gen?.createCondition
			var Condition con_tmp = gen?.createCondition
			var action acti = gen?.createaction()
			acti?.getSendevent()?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
			//acti?.setTrigger(Trigger.ON_ENTRY)
			
			con?.setAction(acti)
			con?.setDescription(element?.getDesc())
			condCounter++
			con?.setName('condition' + condCounter)
			
			con_tmp?.setAction(acti)
			con_tmp?.setDescription(element?.getDesc())
			con_tmp?.setName('condition' + condCounter)
			
			this.condcontainer?.getCondition()?.add(con_tmp)
			concontainer?.getCondition()?.add(con)*/
			
			listevents?.sendevent?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
		}
		
		listevents
	}
	
	protected def EventContainer GivenStepTranslation(GivenStep givenstep, stateContainer statcontainer, HashMap<String, String> map){
		//this.statcontainer = gen?.createstateContainer
		
		//<change>
		/*var state stat = gen?.createstate
		var state stat_tmp = gen?.createstate
		var action act = gen?.createaction()
		act?.getSendevent()?.add(StepInBodyTranslation(givenstep?.getAction() as StepInBody, map))
		//act?.setTrigger(Trigger.ON_ENTRY)
		
		stat?.setAction(act)
		stat?.setDescription(givenstep?.getDesc())
		stateCounter++
		stat?.setName('state' + stateCounter)
		
		stat_tmp?.setAction(act)
		stat_tmp?.setDescription(givenstep?.getDesc())
		stat_tmp?.setName('state' + stateCounter)
		
		this.stcontainer?.getState()?.add(stat_tmp)
		statcontainer?.getState()?.add(stat)*/
		//</change>
		
		var listevents = gen?.createEventContainer 
		listevents?.sendevent?.add(StepInBodyTranslation(givenstep?.getAction() as StepInBody, map))
		
		
		for (AndInputStep element : givenstep?.getAnd()){
			/*var state sta = gen?.createstate
			var state sta_tmp = gen?.createstate
			var action acti = gen?.createaction()
			acti?.getSendevent()?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
			//acti?.setTrigger(Trigger.ON_ENTRY)
			
			sta?.setAction(acti)
			sta?.setDescription(element?.getDesc())
			stateCounter++
			sta?.setName('state' + stateCounter)
			
			sta_tmp?.setAction(acti)
			sta_tmp?.setDescription(element?.getDesc())
			sta_tmp?.setName('state' + stateCounter)
			
			this.stcontainer?.getState()?.add(sta_tmp)
			statcontainer?.getState()?.add(sta)*/
			
			listevents?.sendevent?.add(StepInBodyTranslation(element?.getAction() as StepInBody, map))
			
		}
		listevents
	}
	
	protected def state genState(state st){
		var stat = gen?.createstate
		stat.description = null
		stat.name = st.name
		stat.action = gen?.createaction
		for(event : st.action.sendevent){
			var eve = gen?.createSendEvent
			eve.method = event.method
			eve.event = event.event
			eve.assigcontainer = gen?.createAssigContainer
			for(assignment : event.assigcontainer.assignment){
				var assign = gen?.createAssignment
				assign.leftSide = assignment.leftSide
				assign.operation = gen?.createOperation
				assign.operation.str = assignment.operation.str
				assign.rightSide = assignment.rightSide
				eve.assigcontainer.assignment.add(assign)
			}
			stat.action.sendevent.add(eve)
		}
		stat
	}
	
	protected def Condition genCondition(Condition cond){
		var condition = gen?.createCondition 
		condition.description = null
		condition.name = cond.name
		condition.action = gen?.createaction
		for(event : cond.action.sendevent){
			var eve = gen?.createSendEvent
			eve.method = event.method
			eve.event = event.event
			eve.assigcontainer = gen?.createAssigContainer
			for(assignment : event.assigcontainer.assignment){
				var assign = gen?.createAssignment
				assign.leftSide = assignment.leftSide
				assign.operation = gen?.createOperation
				assign.operation.str = assignment.operation.str
				assign.rightSide = assignment.rightSide
				eve.assigcontainer.assignment.add(assign)
			}
			condition.action.sendevent.add(eve)
		}
		condition.from = cond.from
		condition.to = cond.to
		condition
	}
	
	protected def Scenario genScenario(Scenario scen){
		var scenario = gen?.createScenario
		scenario.currentStates = gen?.createstateContainer
		scenario.condcontainer = gen?.createCondContainer
		scenario.nextStates = gen?.createstateContainer
		scenario.name = scen.name
		for(st : scen.currentStates.state)
			scenario.currentStates.state.add(genState(st))
		for(st : scen.nextStates.state)
			scenario.nextStates.state.add(genState(st))
		for(con : scen.condcontainer.condition)
			scenario.condcontainer.condition.add(genCondition(con))
		scenario
	}
	
	protected def void ScenarioTranslation(ScenarioImpl scenario, HashMap<String, String> map){
		scen = gen?.createScenario
		scen?.setCurrentStates(gen?.createstateContainer())
		scen?.setCondcontainer(gen?.createCondContainer())
		scen?.setNextStates(gen?.createstateContainer())
		hasGiven = false
		hasWhen = false
		hasThen = false
		scen?.setName((scenario)?.getName?.getName)
		var allSteps = ((scenario)?.getSteps())
		var givenevents = gen?.createEventContainer
		var whenevents = gen?.createEventContainer
		var thenstatemachines = new ArrayList<ChrisnaFSM>()
		for (step : allSteps){
			switch(step?.getWhich()?.getClass){
				case(GivenStepImpl):
				{
					hasGiven = true
					givenevents = GivenStepTranslation(step?.getWhich() as GivenStepImpl, scen?.getCurrentStates, map)
					//scen?.setCurrentStates(this.statcontainer)
				}
					
				case(WhenStepImpl):
				{
					hasWhen = true
					whenevents = WhenStepTranslation(step?.getWhich() as WhenStepImpl, scen?.getCondcontainer, map)
					//scen?.setCondcontainer(this.concontainer)
				}
					
				case(ThenStepImpl):
				{
					hasThen = true
					thenstatemachines = ThenStepTranslation(step?.getWhich() as ThenStepImpl, scen?.getNextStates, map, scen?.name)
					//scen?.setNextStates(this.statcontainer)
				}
			}
		}
		
		var design = new GivenWhenStepBaseDesignStateMachine(scen?.name, givenevents, whenevents)
		var givenwhenstatemachine = design?.statemachine
		
		givenwhenstatemachine?.scencontainer?.scenario?.get(0)?.currentStates?.state?.add(initst)
		for (con : givenwhenstatemachine?.scencontainer?.scenario?.get(0)?.condcontainer?.condition)
			con?.setFrom(initst)
			
		var states = gen?.createstateContainer
		for(st : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates.state){
			states.state.add(genState(st))
		}
		
		for( statemachine : thenstatemachines){
			var statesContainer = states?.state
			val length = statesContainer.length
			if(length != 0){
				for (var i = 0; i<length; i++) {
					var state = genState(statesContainer.get(i))
					var state1 = genState(statesContainer.get(i))
					statemachine?.scencontainer?.scenario?.get(0)?.currentStates?.state?.add(state)
					statemachine?.scencontainer?.scenario?.get(1)?.nextStates?.state?.add(state1)
				}
			}
		}
			
			//for(eachstate : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates?.state)
				//listtmp.state.add(eachstate)
		var states1 = gen?.createstateContainer
		for(st : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates.state){
			/*var stat = gen?.createstate
			stat.description = null
			stat.name = st.name
			stat.action = gen?.createaction
			for(event : st.action.sendevent){
				var eve = gen?.createSendEvent
				eve.method = event.method
				eve.event = event.event
				eve.assigcontainer = gen?.createAssigContainer
				for(assignment : event.assigcontainer.assignment){
					var assign = gen?.createAssignment
					assign.leftSide = assignment.leftSide
					assign.operation = gen?.createOperation
					assign.operation.str = assignment.operation.str
					assign.rightSide = assignment.rightSide
					eve.assigcontainer.assignment.add(assign)
				}
				stat.action.sendevent.add(eve)
			}*/
			states1.state.add(genState(st))
		}
		
		for( statemachine : thenstatemachines){
			var statesContainer = states1?.state
			for (con : statemachine?.scencontainer?.scenario?.get(0)?.condcontainer?.condition)
				con?.setFrom(statesContainer.head)		
		}
				
			//for(eachstate : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates?.state)
				//listtmp.state.add(eachstate)
		var states2 = gen?.createstateContainer
		for(st : givenwhenstatemachine?.scencontainer?.scenario?.last?.nextStates.state){
			/*var stat = gen?.createstate
			stat.description = null
			stat.name = st.name
			stat.action = gen?.createaction
			for(event : st.action.sendevent){
				var eve = gen?.createSendEvent
				eve.method = event.method
				eve.event = event.event
				eve.assigcontainer = gen?.createAssigContainer
				for(assignment : event.assigcontainer.assignment){
					var assign = gen?.createAssignment
					assign.leftSide = assignment.leftSide
					assign.operation = gen?.createOperation
					assign.operation.str = assignment.operation.str
					assign.rightSide = assignment.rightSide
					eve.assigcontainer.assignment.add(assign)
				}
				stat.action.sendevent.add(eve)
			}*/
			states2.state.add(genState(st))
		}
		for( statemachine : thenstatemachines){
			var statesContainer = states2?.state
			for (con : statemachine?.scencontainer?.scenario?.get(1)?.condcontainer?.condition)
				con?.setTo(statesContainer?.head)		
		}
		
		var states3 = gen?.createstateContainer
		for(st : givenwhenstatemachine?.scencontainer?.scenario?.head?.nextStates.state){
			/*var stat = gen?.createstate
			stat.description = null
			stat.name = st.name
			stat.action = gen?.createaction
			for(event : st.action.sendevent){
				var eve = gen?.createSendEvent
				eve.method = event.method
				eve.event = event.event
				eve.assigcontainer = gen?.createAssigContainer
				for(assignment : event.assigcontainer.assignment){
					var assign = gen?.createAssignment
					assign.leftSide = assignment.leftSide
					assign.operation = gen?.createOperation
					assign.operation.str = assignment.operation.str
					assign.rightSide = assignment.rightSide
					eve.assigcontainer.assignment.add(assign)
				}
				stat.action.sendevent.add(eve)
			}*/
			states3.state.add(genState(st))
		}
		
		for( statemachine : thenstatemachines){
			var statesContainer = states3?.state
			var headstate = genState(statesContainer.head) 
			//for(st : statesContainer)
			statemachine?.scencontainer?.scenario?.last?.nextStates?.state.add(headstate)
			for (co : statemachine?.scencontainer?.scenario?.last?.condcontainer?.condition)
				co.to = headstate
		}
			/*if(!hasGiven)
			{
				if(!hasWhen)
				{
					initst = gen?.createinitState()
					//initst?.setAction(gen?.createaction)
					var action initact = gen?.createaction
					for( state element : scen?.getNextStates?.getState){
						for (SendEvent ele :element?.getAction?.getSendevent){					 		
							if (ele !== null){
								//initact?.getSendevent?.add(ele)
							}
						}
					}
					(this?.initst as state)?.setAction(initact)
					//var EList<String> des = new BasicEList<String>(1)
					//des?.add('initial state')
					//this?.initst?.setDescription(des)
					this?.initst?.setName('initial state')
				}
				//else
					//scen?.getCurrentStates?.getState?.add(this.initst)
			}*/
				
			//if(!hasWhen)
			
			
			//if(!hasThen)
				//throw new IllegalStateException("Kidding me! one Scenario without Then! I wish I was there to know how you compile and build this grancin file!! I got you! you may trick xtext but CAN NOT trick me! HAHAHAHA!")
			
			//if (hasGiven || hasWhen){
			
			for (element : givenwhenstatemachine?.scencontainer?.scenario)
				this.scenariocontainer?.getScenario?.add(genScenario(element))
			//var length = givenwhenstatemachine?.scencontainer?.scenario.length
			//for(var i = 0; i < length; i++)
				//this.scenariocontainer?.getScenario?.add(givenwhenstatemachine?.scencontainer?.scenario.get(i))
			for (statemachine : thenstatemachines) {
				for (element : statemachine?.scencontainer?.scenario)
					this.scenariocontainer?.scenario?.add(genScenario(element))
			}
			
				
				//<changed>
				//this.scenariocontainer?.getScenario?.add(scen)
				//</changed>
				
				//this.scencontainerwithtime?.getScencontainer?.getScenario?.add(scen)
				//this.scencontainerwithouttime?.getScencontainer?.getScenario?.add(scen)
			//}
	}
	
	protected def void ScenarioOutlineTranslation(ScenarioOutline scenarioOutline){
		
		//val toSearch = ""
		val scenario = (gher?.createScenario as ScenarioImpl)
		scenario.setName(gher?.createScenarioTitle)
		//var tag = gher?.createTag
		if(scenario?.getTags !== null)
		{
			for(i : scenarioOutline?.getTags.size >.. 0){
				scenario?.getTags?.add(scenarioOutline?.getTags.get(i))
			}
		}
		scenario?.getName?.setName(scenarioOutline?.getName?.getName)
		if(scenario?.getElements !== null){
			for(i : scenarioOutline?.getElements.size >.. 0){
				scenario?.getElements?.add(scenarioOutline?.getElements.get(i))
			}
		}
		if(scenario?.getSteps !== null){
				for(i : scenarioOutline?.getSteps.size >.. 0){
					scenario?.getSteps?.add(scenarioOutline?.getSteps.get(i)) 
				}
		}
		
		scenarioOutline?.getExample?.getTable?.getRows?.forEach[
			var map = new HashMap<String, String>()
			var i = 0
			for(cell : (it?.getCells as TableCellsImpl).getCell){
				val key = scenarioOutline?.getExample?.getTable?.getHeader?.getCells?.getName.get(i)
				i++
				map.put(key, ConstantTranslation(cell))
			}
			
			ScenarioTranslation(scenario as ScenarioImpl, map)
			
			/*scenarioOutline?.getSteps?.forEach[
				switch(it?.getWhich?.getClass){
					case GivenStep:
						((it?.getWhich) as GivenStepImpl).getAction.
					case WhenStep:
					case ThenStep
				}
			]*/
		]
	}
	
	/*protected def void runfiltering(){
		this.filter?.GivenToWhenAttr()
		synchronized(this.lock){
			this.filter?.HierarchieCheck()
			this.filter?.OrtogonalCheck()
		}
		this.filter?.sevCondCheck()
		this.filter?.ParametrizationCheck()
		this.filter?.RekurAktCheck()
		this.filter?.OverlappCheck()
	}*/
	
	protected def void generator(Resource feature){
		
		this.name = feature.allContents.filter(Feature)?.map[title?.name].join
		
		initst = gen?.createinitState()
		var EList<String> des = new BasicEList<String>(1) 
		des.add('initial state')
		initst.description = des
		initst.name = 'initial state'
		initst.action = gen?.createaction
		
		/*val allThen = feature.allContents.toIterable.filter(ThenStep)
		allThen?.forEach[
			ThenStepTranslation(it)
		]
		
		val allWhen = feature.allContents.toIterable.filter(WhenStep)
		allWhen?.forEach[
			WhenStepTranslation(it)
		]*/
		
		//val allScenarios = feature.allContents.toIterable.filter(AbstractScenario)
		//var allScenarios_temp = allScenarios
		for(scenario : feature.allContents.toIterable.filter(AbstractScenario)){//?.forEach[
			switch(scenario?.getWhich()?.getClass){
				case(ScenarioImpl):
				{
					ScenarioTranslation(scenario?.getWhich as ScenarioImpl, NoTable)
				}
				case(ScenarioOutlineImpl):
				{
					ScenarioOutlineTranslation(scenario?.getWhich as ScenarioOutline)
				}
			}
		}//]
		
		//<changed>
		/*if (this.initst === null){
			initst = gen?.createinitState
			var action act = gen?.createaction
			var SendEvent event = gen?.createSendEvent
			event?.setEvent("do ")
			event?.setMethod("nothing")
			event?.setTrigger(Trigger.ON_ENTRY)
			act?.getSendevent()?.add(event)
			//act?.setTrigger(Trigger.ON_ENTRY)
			initst.setAction(act)
			var EList<String> des = new BasicEList<String>(1) 
			des?.add('initial state')
			this?.initst?.setDescription(des)
		}*/
		//scen?.getCurrentStates?.getState?.add(this.initst)
		//</changed>
		
		//this.filter?.setScencontainer(scenariocontainer)
		//runfiltering()
		//this.warningerrors?.setScencontainer(this.filter?.getScencontainer)
		
		this.chrisnafsm?.setInitstate(this.initst as initState)
		this.chrisnafsm?.setScencontainer(scenariocontainer)
	}
}