package com.bmw.vv.grancin.codegen.chrisna

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap
import chrisna.OnChange

class ThenStepOnChangeDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genOnChangeDesignStateMachine
	}
	
	def genOnChangeDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		insertState(0, 0, name, genState('Wait_for_change_' + name, genEvent('set pre value', 'Compute', {
			expressions?.clear
			expressions?.add(genExpression('pre_' + (sendedevent?.time as OnChange)?.name, '=', (sendedevent?.time as OnChange)?.name))
			expressions
		})), genCondition(name, genEvent('is changed', 'Not', {
			expressions?.clear
			expressions?.add(genExpression('pre_' + (sendedevent?.time as OnChange)?.name, '!=', (sendedevent?.time as OnChange)?.name))
			expressions
		}), null, null))
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Ok_' + name, null))
			//scen.nextStates.state.add(statemachine?.initstate)
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
	}
	
}