package com.bmw.vv.grancin.codegen.chrisna

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap

/*enum StateType {
	INITIALSTATE,
	STATE
}*/

class ThenStepBaseDesignStateMachine extends BaseDesignStateMachine {
	
	protected val int name
	
	protected val SendEvent sendedevent
	//protected val StateType sttype
	
	new(String scenario_name, SendEvent event) {
		name = scenario_name.hashCode
		//sttype = typ
		sendedevent = event
		
		genBaseDesignStateMachine
	}
	
	def genBaseDesignStateMachine(){
		//if (sttype == StateType.INITIALSTATE){
		/*statemachine?.setInitstate(generatorFactory?.createinitState)
		statemachine?.initstate.description.add(name)
		statemachine?.initstate.action = generatorFactory?.createaction
		statemachine?.initstate.name = 'init ' + name*/
		 
		var expressions = new ArrayList<HashMap<String, String>>()	
		
		statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			//scen.currentStates.state.add(statemachine?.initstate)
			scen?.nextStates?.state?.add(0, genState('Warning_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '150 ms'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check time', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '0 ms'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.nextStates?.state?.add(0, genState('Check_' + name, genEvent('set loop num ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '=', 'loopnum_' + name + ' + 1'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '!=', '0 ms'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
		})
		
		statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			//scen.nextStates.state.add(statemachine?.initstate)
			
			scen?.currentStates?.state?.add(0, genState('Warning_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '150 ms'))
				expressions})))				
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.currentStates?.state?.add(0, genState('QuickError_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '/ (2 ^ loop num ' + name + '))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('reset', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen?.currentStates?.state?.add(0, genState('Error_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '* (2 ^ (loop num ' + name + ' + 1))/ (2 ^ 8))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('reset', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		statemachine?.scencontainer?.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Check_' + name, genEvent('set loop num ' + name, 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '++'))
				expressions})))
			scen?.nextStates?.state?.add(0, genState('Ok_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name , genEvent('is equal', 'Comparison', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.nextStates?.state?.add(0, genState('QuickError_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '/ (2 ^ loopnum_' + name + '))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is smaller', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '<=', '8' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen?.nextStates?.state?.add(0, genState('Error_' + name, genEvent('set time', 'Compute', {
				expressions?.clear
				expressions?.add(genExpression('time_' + name, '=', '(time_' + name + '* (2 ^ (loopnum_' + name + ' + 1))/ (2 ^ 8))'))
				expressions})))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is bigger', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '>', '8' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is smaller', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '<=', '20' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen?.nextStates?.state?.add(0, genState('Error_after_a_while_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is not equal', 'Not', {
				expressions?.clear
				for(assignment : sendedevent?.assigcontainer?.assignment)
					expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('is bigger', 'Comparison', {
				expressions?.clear
				expressions?.add(genExpression('loopnum_' + name, '>', '20' ))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
				
			scen
		})
		
		//}
	}
}