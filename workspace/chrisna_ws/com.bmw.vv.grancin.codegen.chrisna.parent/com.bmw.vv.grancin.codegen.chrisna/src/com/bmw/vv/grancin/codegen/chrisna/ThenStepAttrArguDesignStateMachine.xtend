package com.bmw.vv.grancin.codegen.chrisna

import chrisna.SendEvent
import java.util.ArrayList
import java.util.HashMap

class ThenStepAttrArguDesignStateMachine extends ThenStepBaseDesignStateMachine {
	
	new(String scenario_name, SendEvent event) {
		super(scenario_name, event)
		genAttriArguDesignStateMachine
	}
	
	def genAttriArguDesignStateMachine(){
		
		var expressions = new ArrayList<HashMap<String, String>>()
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('Ok_' + name, null))
			//scen.nextStates.state.add(statemachine?.initstate)
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_' + name, '=', 'true'))
				expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			condcounter = condcounter + 1 
				
			scen
		})
		scencounter = scencounter + 1
	}
	
}