package com.bmw.vv.grancin.codegen.chrisna

import chrisna.CondContainer
import chrisna.state
import chrisna.stateContainer

class WriteToFile{
	
	val ChrisnaFSMGenerator csa
	var CharSequence text = ''
	
	
	new(ChrisnaFSMGenerator chrisnagenerator) {
		this.csa = chrisnagenerator
	}
	
	def WriteStates(stateContainer stcontainer){
				stcontainer?.getState?.forEach[
					text =  text +'   -state: ' + ((it?.getName()) ?: 'no name') + '\n'
						text = text + '    -actions: \n'
						it?.getAction?.getSendevent?.forEach[
							text = text + '     -event: '
								text = text + '      ' + ((it?.getEvent) ?: '') + ' ' + ((it?.getMethod) ?: '') + '\n' 
									text = text + '       -assignments: '
									it?.getAssigcontainer?.getAssignment?.forEach[
										text = text + '        ' + ((it?.getLeftSide) ?: '') + ' ' + ((it?.getOperation?.getStr) ?: '') + ' ' + ((it?.getRightSide) ?: '') + ' \n'
									]
									text = text + '      -trigger: ' + ((it?.getTrigger) ?: '') + '\n'
						]
						
				]
	}
	
	def WriteConditions(CondContainer condcontainer){
				condcontainer?.getCondition?.forEach[
					text =  text +'   -condition: ' + ((it.getName) ?: 'no name')  + '\n'
						text = text + '    -actions: \n'
						it?.getAction?.getSendevent?.forEach[
							text = text + '     -event: '
								text = text + '      ' + ((it?.getEvent) ?: '') + ' ' + ((it?.getMethod) ?: '') + '\n' 
									text = text + '       -assignments: '
									it?.getAssigcontainer?.getAssignment?.forEach[
										text = text + '        ' + ((it.getLeftSide) ?: '') + ' ' + ((it.getOperation.getStr) ?: '') + ' ' + ((it.getRightSide) ?: '') + ' \n'
									]
									text = text + '      -trigger: ' + ((it?.getTrigger) ?: '') + '\n'
						]
						text = text + '      -from: ' + ((it?.getFrom) ?: '') + '\n'
						text = text + '      -to: ' + ((it?.getTo) ?: '') + '\n'
				]
	}
	
	def Writeinit(state ini){
						text =  text +'   -init state: ' + ((ini?.getName) ?: '') + '\n'
						text = text + '    -actions: \n'
						ini?.getAction?.getSendevent?.forEach[
							text = text + '     -event: '
								text = text + '      ' + ((it?.getEvent) ?: '') + ((it?.getMethod) ?: '') + '\n' 
									text = text + '       -assignments: '
									it?.getAssigcontainer?.getAssignment?.forEach[
										text = text + '        ' + ((it?.getLeftSide) ?: '') + ' ' + ((it?.getOperation?.getStr) ?: '') + ' ' + ((it?.getRightSide) ?: '') + ' \n'
									]
									text = text + '      -trigger: ' + ((it?.getTrigger) ?: '') + '\n'
						]
						
	}
	
	def WriteFile() {
		text = text + ((this.csa?.name) ?: 'chrisna fsm') + '\n'
		
		Writeinit(this.csa?.initst)
		
		this.csa?.scenariocontainer?.getScenario?.forEach[
			text = text + '\nTransition: ' + ((it?.getName) ?: 'no name') + '\n'
			
				text = text + '  -current states: \n'
				WriteStates(it?.getCurrentStates)
				
				text = text + '  -conditions: \n'
				WriteConditions(it?.getCondcontainer)
				
				text = text + '  -next states: \n'
				WriteStates(it?.getNextStates)
		]
		text
	}
}