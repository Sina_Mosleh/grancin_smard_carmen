package com.bmw.vv.grancin.codegen.chrisna

import chrisna.EventContainer
import java.util.ArrayList
import java.util.HashMap
import chrisna.TimeWindow

class GivenWhenStepBaseDesignStateMachine extends BaseDesignStateMachine {
	protected val int name
	protected val EventContainer givenevents
	protected val EventContainer whenevents
	
	new(String scenario_name, EventContainer giveneves, EventContainer wheneves) {
		name = scenario_name.hashCode
		givenevents = giveneves
		whenevents = wheneves
		
		genBaseDesignStateMachine
	}
	
	def genBaseDesignStateMachine(){
		 
		var expressions = new ArrayList<HashMap<String, String>>()	
		
		statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			//scen.currentStates.state.add(statemachine?.initstate)
			scen?.nextStates?.state?.add(0, genState('no_condition_' + name, null))
			scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
				expressions?.clear
				expressions?.add(genExpression('true_'+ name, '=', 'true'))
				expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			
			scen
			
		})
		
		statemachine?.scencontainer?.scenario?.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('no_condition_' + name, null))
			scen?.nextStates?.state?.add(0, genState('given_conditions_satisfied_' + name, null))
			if (givenevents?.sendevent !== null && givenevents?.sendevent.length != 0){
				for (event : givenevents?.sendevent){
					/*if (event.time.type.contains('Periodically')) {
						
					}*/
					if (event?.time?.type !== null){
						if (event?.time?.type.contains('TimeWindow')) {
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
								expressions?.clear  
								if((event?.time as TimeWindow).unit !== null) 
									expressions?.add(genExpression('const_time_given_when_' + name, '==', (event?.time as TimeWindow).time.toString + (event?.time as TimeWindow).unit ?: ''))
								for(assignment : (event?.time as TimeWindow)?.assigcontainer?.assignment)
									expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
								expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						}
						else {
							if (event?.assigcontainer?.assignment.length != 0)
								scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check conditions', 'Comparison', {
									expressions?.clear  
									for(assignment : event?.assigcontainer?.assignment)
										expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
									expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
							else
								scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
								expressions?.clear
								expressions?.add(genExpression('true_' + name, '=', 'true'))
								expressions
							}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						}
					}
					else {
						if (event?.assigcontainer?.assignment.length != 0)
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check conditions', 'Comparison', {
								expressions?.clear  
								for(assignment : event?.assigcontainer?.assignment)
									expressions?.add(genExpression(assignment?.leftSide, assignment?.operation?.str, assignment?.rightSide))
								expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						else
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
							expressions?.clear
							expressions?.add(genExpression('true_'+ name, '=', 'true'))
							expressions
						}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
					}
				}	
			}
			else {
				scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
					expressions?.clear
					expressions?.add(genExpression('true_' + name, '=', 'true'))
					expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			}
			
			scen
		})
		
		statemachine?.scencontainer.scenario.add({
			var scen = genScenario(name)
			
			scen?.currentStates?.state?.add(0, genState('given_conditions_satisfied_' + name, null))
			scen?.nextStates?.state?.add(0, genState('when_conditions_satisfied_' + name, null))
			if (whenevents?.sendevent !== null && whenevents?.sendevent.length != 0) {
				for (event : whenevents?.sendevent){
					/*if (event.time.type.contains('Periodically')) {
						
					}*/
					if (event?.time?.type !== null){
						if (event?.time?.type.contains('TimeWindow')) {
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('wait', 'Timing', {
								expressions?.clear 
								if((event?.time as TimeWindow).unit !== null) 
									expressions?.add(genExpression('const_time_given_when_' + name, '==', (event?.time as TimeWindow).time.toString + (event?.time as TimeWindow).unit ?: ''))
								for(assignment : (event?.time as TimeWindow)?.assigcontainer?.assignment)
									expressions?.add(genExpression(assignment?.leftSide?: '', assignment?.operation?.str?: '', assignment?.rightSide?: ''))
								expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						}
						else {
							if (event?.assigcontainer?.assignment.length != 0)
								scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check conditions', 'Comparison', {
									expressions?.clear  
									for(assignment : event?.assigcontainer?.assignment)
										expressions?.add(genExpression(assignment?.leftSide?: '', assignment?.operation?.str?: '', assignment?.rightSide?: ''))
									expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
							else
								scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
								expressions?.clear
								expressions?.add(genExpression('true_' + name, '=', 'true'))
								expressions
							}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						}
					}
					else {
						if (event?.assigcontainer?.assignment.length != 0)
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('check conditions', 'Comparison', {
								expressions?.clear  
								for(assignment : event?.assigcontainer?.assignment)
									expressions?.add(genExpression(assignment?.leftSide?: '', assignment?.operation?.str?: '', assignment?.rightSide?: ''))
								expressions}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
						else
							scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
							expressions?.clear
							expressions?.add(genExpression('true_'+ name, '=', 'true'))
							expressions
						}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
					}
				}	
			}
			else {
				scen?.condcontainer?.condition?.add(0, genCondition(name, genEvent('always', 'True', {
					expressions?.clear
					expressions?.add(genExpression('true_' + name, '=', 'true'))
					expressions
				}), scen?.currentStates?.state?.head, scen?.nextStates?.state?.head))
			}
				
			scen
		})
	}
}