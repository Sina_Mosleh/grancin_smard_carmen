grammar com.bmw.vv.Gherkin hidden(WS, SL_COMMENT)
import "http://www.eclipse.org/emf/2002/Ecore" as ecore
generate gherkin "http://www.bmw.com/vv/Gherkin"

Feature:
	{Feature}
	NEWLINE*
	((tags+=Tag)? title=FeatureTitle)?
    (elements+=NarrativeElement)*
    (backgroundInfo+=Background)?
    (scenarios+=AbstractScenario)*
    ; 
  
Tag:
	('@' tagID+=ID NEWLINE*)+ NEWLINE
;
  
FeatureTitle:
    ('Feature:' | 'Narrative:') name=TEXT NEWLINE+;

NarrativeElement:
	content = (InOrderTo | AsA | IWantTo | FreeText)
;

AsA: 
	('As' ('a' | 'an')) role=TEXT NEWLINE+
;

InOrderTo: 
	InOrderToKey intent=TEXT NEWLINE+
;

// https://blogs.itemis.com/en/xtext-hint-content-assist-for-multiple-consecutive-keywords
InOrderToKey:
	'In' 'order' 'to'
;


IWantTo:
	IWantToKey doWhat=TEXT NEWLINE+
;

// https://blogs.itemis.com/en/xtext-hint-content-assist-for-multiple-consecutive-keywords
IWantToKey:
	'I' 'want' 'to'
;

FreeText:
	text=TEXT NEWLINE+
;

AbstractScenario:
	which = (Scenario | ScenarioOutline)
;

Background:
	(tags+=Tag)? 
	title=BackgroundTitle
	(steps+=GivenStep)*
	;

BackgroundTitle:
	{BackgroundTitle} 'Background:' (name=TEXT)? NEWLINE+
;

Scenario:
	(tags+=Tag)? 
	name=ScenarioTitle
	(elements+=NarrativeElement)*
	(steps+=StepType)*
;

ScenarioTitle:
	'Scenario:' name=TEXT NEWLINE+
;

ScenarioOutline:
	(tags+=Tag)? 
	name=ScenarioOutlineTitle
	(elements+=NarrativeElement)*
	(steps+=StepType)*
	example=Example
;

ScenarioOutlineTitle:
	'Scenario' 'Outline:' name=TEXT NEWLINE+
;

Example:
	title=ExampleTitle
	table=TableRows
;

ExampleTitle:
	'Examples:' NEWLINE+
;

TableRows:
	header = HeaderRow 	
	(rows += TableRow)* 
;

HeaderRow:
	cells=HeaderCells '|' NEWLINE+
;

TableRow:
	cells=TableCells  '|' NEWLINE+
;

HeaderCells:
	('|' name+=ID)+
;

TableCells:
	('|' name+=CELL_CONTENT)+
;

StepType:
	which = (GivenStep | WhenStep | ThenStep)  
;

GivenStep:
	'Given' action=StepInBody NEWLINE+ (desc+=OptionalText NEWLINE+)* (rows=TableRows)?
	(and+=AndInputStep)*
;

WhenStep:
	'When' action=StepInBody NEWLINE+ (desc+=OptionalText NEWLINE+)* (rows=TableRows)?
	(and+=AndInputStep)*
;

ThenStep:
	'Then' action=StepOutBody NEWLINE+ (desc+=OptionalText NEWLINE+)* (rows=TableRows)?
	(and+=AndOutputStep)*
	(but+=ButStep)* 
;

AndInputStep:
	'And' action=StepInBody NEWLINE+ (desc+=OptionalText NEWLINE+)*
;

AndOutputStep:
	'And' action=StepOutBody NEWLINE+ (desc+=OptionalText NEWLINE+)* 
;

ButStep:
	'But' action=StepOutBody NEWLINE+ (desc+=OptionalText NEWLINE+)* 
;

OptionalText returns ecore::EString:
	TEXT | CODE
;
	
StepInBody:
	inBody+=StepBody	
;

StepOutBody:
	outBody+=StepBody
;

StepBody:
	body=TEXT
;

CELL_CONTENT returns ecore::EString:
	(KEYWORD|ID|WORD|'@')+
;

TEXT returns ecore::EString:
	(KEYWORD|ID|WORD)+
;

//https://blogs.itemis.com/en/xtext-hint-identifiers-conflicting-with-keywords
KEYWORD: 'I'|'a'|'an'|'to'|'on'|'is';

terminal NEWLINE:'\r'? '\n';
terminal SL_COMMENT : '#' !('\n'|'\r')* ('\r'? '\n')?;
terminal CODE: '"""' -> '"""' | "'''" -> "'''";
terminal ID: '^'?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
terminal WORD: !('|'|' '|'\t'|'\r'|'\n'|'@')+;
terminal WS  : (' '|'\t')+;