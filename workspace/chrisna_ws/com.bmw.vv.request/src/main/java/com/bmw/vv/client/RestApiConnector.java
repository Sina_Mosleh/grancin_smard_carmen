package com.bmw.vv.client;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Connects to the BN Provider and retrieves specific signal information
 * 
 * @author QXX8616
 * @editor Q486705
 * @date 27.08.2019
 */
public class RestApiConnector
{
    private static final String tokenBasePath = "https://keycloak-eeanalytics.bmwgroup.net";
    private static final String tokenURI = "/auth/realms/bnRealm/protocol/openid-connect/token";
    private static String user;
    private static String pw; //base64 encoded

    private static final String clientID = "bn-provider";
    private static String modelLine;
    private static String model;
    private static final String version = "v3";
    private static final String old_version = "v1";
    private static String integration;
	private static int busID;

    private static Token token;
    private static Client client;

    /**
     * Initializes the client with disabled cert. verification
     */
    public RestApiConnector()
    {
        SSLContext sslContext = null;
        try
        {
            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManager, null);
        }
        catch (NoSuchAlgorithmException | KeyManagementException e)
        {
            e.printStackTrace();
        }

        client = ClientBuilder.newBuilder().sslContext(sslContext).build();
    }
	
	public void setmodelLine(String modelline){
		modelLine = modelline;
	}
	
	public String getmodelLine(){
		return modelLine;
	}
	
	public void setmodel(String Model){
		model = Model;
	}
	
	public String getmodel(){
		return model;
	}
	
	public void setintegration(String Integrate){
		integration = Integrate;
	}
	
	public String getintegration(){
		return integration;
	}
	
	public void setbusID(int BusID){
		busID = BusID;
	}
	
	public int getbusID(){
		return busID;
	}
	
	public void setusername(String username){
		user = username;
	}
	
	public String getusername(){
		return user;
	}
	
	public void setpassword(String password){
		pw = password;
	}
	
	public String getpassword(){
		return pw;
	}

    /**
     * Requests a new token or refreshes an old expired one.
     */
    public void requestToken()
    {
        MultivaluedMap<String, String> data = null;

        if (token == null || token.hasExpired())
        {
            if (token == null)
            {
                data = new MultivaluedHashMap<String, String>();
                data.add("grant_type", "password");
                data.add("client_id", clientID);
                data.add("username", user);
                data.add("password", new String(Base64.getDecoder().decode(pw), StandardCharsets.UTF_8));
            }
            else if (token.hasExpired())
            {
                data = new MultivaluedHashMap<String, String>();
                data.add("grant_type", "refresh_token");
                data.add("client_id", clientID);
                data.add("refresh_token", token.getRefreshToken());
            }

            Response responseFromServer = client.target(tokenBasePath)
                    .path(tokenURI)
                    .request(MediaType.TEXT_PLAIN)
                    .post(Entity.form(data));

            String answer = responseFromServer.readEntity(String.class);
            token = new Token(answer);
            System.out.println("Token:" + token.getAccessToken());
        }
    }
	
	/**
     * Requests a signal with a certain short name
     * 
     * @param shortname a signal name
     * @return a json formated string containing signal information
     */
	public String request(String request, String ident, String shortname)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
		switch(request){
			case "ModelLines":
			{
				uri.append("/ModelLines");
				break;
			}
			case "Models":
			{
				uri.append("/ModelLines");
				uri.append("/" + ident);
				uri.append("/models");
				break;
			}
			case "FileNames":
			{
				uri.append("/Steps");
				uri.append("/" + ident);
				uri.append("/fileNames");
				break;
			}
			case "Buses":
			{
				uri.append("/Buses");
				uri.append("/" + ident);
				break;
			}
			case "Data":
			{
				uri.append("/search/BnSignals/search/findByText");
				uri.append("?text=*" + shortname);
				//busBNID für SomeIP = _9EFBCFCBF21C490AA5705F6FF3E3F085
				uri.append("*&busBNID=" + ident);
				uri.append("&model=" + model);
				uri.append("&modelLine=" + modelLine);
				uri.append("&integration=" + integration);
				break;
			}
			case "ServiceID":
			{
				uri.append("/ServiceInterfaces");
				uri.append("/" + ident);
				break;
			}
			case "MethodID":
			{
				uri.append("/ServiceInterfaces");
				uri.append("/" + ident);
				uri.append("/methods");
				break;
			}
			case "Members":
			{
				uri.append("/Members");
				uri.append("/" + ident);
				break;
			}
			case "factor":
			{
				uri.append("/Members");
				uri.append("/" + ident);
				uri.append("/factor");
				break;
			}
			case "offset":
			{
				uri.append("/Members");
				uri.append("/" + ident);
				uri.append("/offset");
				break;
			}
		}
		Response response = client.target(URI.create(uri.toString()))
    			.request(MediaType.APPLICATION_JSON)
    			.header("Authorization", "Bearer" + token.getAccessToken())
    			.get();
    	
    	return response.readEntity(String.class);
	}
	
	/**
     * parse the data from Rest Api
     * 
     * @param 
     * @return 
     */
    public String parser(String response, String Data){
    	ObjectMapper objectMapper = new ObjectMapper();
		try
        {
            JsonNode root = objectMapper.readTree(response);
            return root.get(Data).asText();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
	}
	
	/**
     * Run the application to request data from Rest Api
     * 
     * @param 
     * @return 
	 * @throws IOException 
     */
    /*public void run(String shortname) throws IOException{
        
    	modelLine = "SP2021";//?
		this.requestToken();
		String ModelLine = this.request("ModelLines", "accessToken" , "");
        //String modelLineident = parser();//?
		String modelLineident = "5d5e9b6ef695510001141fa7";
		
		model = "SP2021";//?
		this.requestToken();
		String Model = this.request("Models", modelLineident, "");
		//String modelident = parser();//?
		String modelident = "5d5e9b6ef695510001141f9d";
		
		integration = "21-07-240";//?
		this.requestToken();
		String Integration = this.request("FileNames", modelident, "");
		//String fileident = parser();//?
		String fileident = "5d5f1d84f695510001464a31";
		
		busID = 45000;
		this.requestToken();
		String BusBNID = this.request("Buses", fileident, "");
		//String busbnident = parser();//?
		String busbnident = "_9EFBCFCBF21C490AA5705F6FF3E3F085";
		
		this.requestToken();
		String data = this.request("Data", busbnident, shortname);
		
		Parser parsed = new Parser(data);
        String serviceidident = parsed.search("BdcIdentityMappingTable.PiaProfileId", "ServiceInterface");
        String methodidident = parsed.search("setEntryInMappingTable", "Method");
        
        this.requestToken();
        String ServiceId = this.request("ServiceID", serviceidident, "");
        String MethodId = this.request("MethodID", methodidident, "");
	}*/

    /**
     * Requests a signal with a certain short name
     * 
     * @param shortname a signal name
     * @return a json formated string containing signal information
     */
    public String requestData(String shortname, String ident)
    {
    	modelLine = "SP2021";
    	model = "SP2021";
    	integration = "21-07-240";
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/search/BnSignals/search/findByText");
        uri.append("?text=*" + shortname);
		//busBNID für SomeIP = _9EFBCFCBF21C490AA5705F6FF3E3F085
        uri.append("*&busBNID=" + ident);
        uri.append("&model=" + model);
        uri.append("&modelLine=" + modelLine);
        uri.append("&integration=" + integration);

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }
	
	/**
     * Requests ModelLines
     * 
     * @param 
     * @return a json formated string containing Model Lines information
     */
    public String requestModelLines()
    {
    	StringBuilder uri = new StringBuilder();
    	uri.append("https://" + clientID + ".bmwgroup.net");
    	uri.append("/" + version);
    	uri.append("/ModelLines");
    	Response response = client.target(URI.create(uri.toString()))
    			.request(MediaType.APPLICATION_JSON)
    			.header("Authorization", "Bearer" + token.getAccessToken())
    			.get();
    	
    	return response.readEntity(String.class);
    }
	
	/**
     * Requests Models
     * 
     * @param 
     * @return a json formated string containing Models information
     */
    public String requestModels(String ident)
    {
    	StringBuilder uri = new StringBuilder();
    	uri.append("https://" + clientID + ".bmwgroup.net");
    	uri.append("/" + old_version);
    	uri.append("/ModelLines");
		//uri.append("/5d5e9b6ef695510001141fa7");
    	uri.append("/" + ident);
		uri.append("/models");
    	Response response = client.target(URI.create(uri.toString()))
    			.request(MediaType.APPLICATION_JSON)
    			.header("Authorization", "Bearer" + token.getAccessToken())
    			.get();
    	
    	return response.readEntity(String.class);
    }
    
	/**
     * Requests File Names
     * 
     * @param 
     * @return a json formated string containing Models information
     */
    public String requestFileNames(String ident)
    {
    	StringBuilder uri = new StringBuilder();
    	uri.append("https://" + clientID + ".bmwgroup.net");
    	uri.append("/" + old_version);
    	uri.append("/Steps");
		//uri.append("/5d5e9b6ef695510001141f9d");
    	uri.append("/" + ident);
		uri.append("/fileNames");
    	Response response = client.target(URI.create(uri.toString()))
    			.request(MediaType.APPLICATION_JSON)
    			.header("Authorization", "Bearer" + token.getAccessToken())
    			.get();
    	
    	return response.readEntity(String.class);
    }
	
	/**
     * Requests Buses
     * 
     * @param 
     * @return a json formated string containing Models information
     */
    public String requestBuses(String ident)
    {
    	StringBuilder uri = new StringBuilder();
    	uri.append("https://" + clientID + ".bmwgroup.net");
    	uri.append("/" + version);
    	uri.append("/Buses");
		//uri.append("/5d5f1d84f695510001464a31");
    	uri.append("/" + ident);
		
    	Response response = client.target(URI.create(uri.toString()))
    			.request(MediaType.APPLICATION_JSON)
    			.header("Authorization", "Bearer" + token.getAccessToken())
    			.get();
    	
    	return response.readEntity(String.class);
    }
	
    /**
     * Requests a service interface description
     * 
     * @param
     * @return a description of the service interface
     */
    public String requestServiceInterface(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/ServiceInterfaces");
		//uri.append("/5d5f1d85f6955100014650a2");
		uri.append("/" + ident);

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }
	
	/**
     * Requests a method id
     * 
     * @param
     * @return a json formated string containing the method id
     */
    public String requestMethod(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/Methods");
		//uri.append("/5d5f1d85f6955100014650a3");
		uri.append("/" + ident);

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }
    
    /**
     * Requests a method id
     * 
     * @param
     * @return a json formated string containing the method id
     */
    public String requestMethodID(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/ServiceInterfaces");
		//uri.append("/5d5f1d85f6955100014650a2");
        uri.append("/" + ident);
		uri.append("/methods");

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }

	/**
     * Requests some factor value
     * 
     * @param
     * @return a json formatted string containing the factor value
     */
    public String requestMember(String ident, String info)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/Members");
		//uri.append("/5d5f1d85f6955100014650b5");
		uri.append("/" + ident);
		uri.append("/" + info);

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }
    
    /**
     * Requests some factor value
     * 
     * @param
     * @return a json formatted string containing the factor value
     */
    public String requestMemberID(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/ServiceInterfaces");
		uri.append("/5d5f1d85f6955100014650a2");
		uri.append("/" + ident);
		uri.append("/fields");

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }
	
    /**
     * Requests some factor value
     * 
     * @param service_id some service id
     * @return a json formatted string containing the factor value
     */
    public String requestFactor(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/Members");
		//uri.append("/5d5f1d85f6955100014650ab");
        uri.append("/" + ident);
		uri.append("/factor");

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }

    /**
     * Requests some offset value
     * 
     * @param service_id some service id
     * @return a json formatted string containing the offset value
     */
    public String requestOffset(String ident)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append("/Members");
		//uri.append("/5d5f1d85f6955100014650ab");
        uri.append("/" + ident);
		uri.append("/offset");

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        return response.readEntity(String.class);
    }

    /**
     * For rest api exploration purposes only
     * 
     * @param query some information from the rest api
     */
    public void send(String query)
    {
        StringBuilder uri = new StringBuilder();
        uri.append("https://" + clientID + ".bmwgroup.net");
        uri.append("/" + version);
        uri.append(query);

        Response response = client.target(URI.create(uri.toString()))
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + token.getAccessToken())
                .get();

        System.out.println(response.readEntity(String.class));
    }

    /**
     * Overrides the Trustmanager to circumvent certificate verification
     */
    TrustManager[] trustManager = new X509TrustManager[]
    {
            new X509TrustManager()
            {
                @Override
                public X509Certificate[] getAcceptedIssuers()
                {
                    return null;
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType)
                {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType)
                {

                }
            } };
}
