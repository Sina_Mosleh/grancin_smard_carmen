package com.bmw.vv.client;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Token
{
	private long lastRefresh;
    private String accessToken;
    private int refreshExpires;
    private int tokenExpires;
    private String refreshToken;

    public Token(String token)
    {
        ObjectMapper objectMapper = new ObjectMapper();
        try
        {
            lastRefresh = System.currentTimeMillis();
            JsonNode root = objectMapper.readTree(token);
            accessToken = root.get("access_token").asText();
            tokenExpires = Integer.parseInt(root.get("expires_in").asText());
            refreshExpires = Integer.parseInt(root.get("refresh_expires_in").asText());
            refreshToken = root.get("refresh_token").asText();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public String getAccessToken()
    {
        return accessToken;
    }

    public String getRefreshToken()
    {
        return refreshToken;
    }

    public int getExpirationToken()
    {
        return tokenExpires;
    }

    public int getExpirationRefresh()
    {
        return refreshExpires;
    }

    public long getLastRefresh()
    {
        return lastRefresh;
    }

    public boolean hasExpired()
    {
        return System.currentTimeMillis() >= (lastRefresh + (tokenExpires * 1000));
    }
}
