package com.bmw.vv.utils

import com.google.inject.Inject
import org.eclipse.emf.common.util.URI
import org.eclipse.xtext.resource.XtextResourceSet
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.util.StringInputStream
import org.franca.core.franca.FModel
import com.bmw.vv.franca.FInterfaces
import org.junit.jupiter.api.Assertions
import com.google.inject.Provider
import java.lang.reflect.Parameter
import java.util.Optional
import java.util.List
import java.util.Arrays
import java.util.stream.Stream

class ResourceLoader implements FInterfaces {
	@Inject Provider<XtextResourceSet> resourceSetProvider	
	@Inject extension ValidationTestHelper vTH

	def XtextResourceSet loadFrancaInterface(String fInterface) {
		loadFrancaInterface(fInterface, "fInterface")
	}
	
	def XtextResourceSet loadFrancaInterface(String fInterface, String IName) {
		val resourceSet = resourceSetProvider.get
		loadFrancaInterface(resourceSet, fInterface, IName)
	}
	
	def XtextResourceSet loadFrancaInterface(XtextResourceSet rs, String fInterface) {
		loadFrancaInterface(rs, fInterface, "fInterface")
	}
		
	def XtextResourceSet loadFrancaInterface(XtextResourceSet rs, String fInterface, String IName) {
		val fidl 		= rs.createResource(URI.createURI(IName + ".fidl"))
		fidl.load(new StringInputStream(fInterface), emptyMap)
		val fModel 	= fidl.contents.head as FModel
		
		// ensure it's not null
		Assertions.assertNotNull(fModel)
		
		// and valid
		vTH.assertNoIssues(fModel)
		
		rs
	}
}
