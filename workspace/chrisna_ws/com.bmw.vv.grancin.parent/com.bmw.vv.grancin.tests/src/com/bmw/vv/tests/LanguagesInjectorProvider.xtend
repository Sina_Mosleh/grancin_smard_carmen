package com.bmw.vv.tests

import org.franca.core.dsl.tests.FrancaIDLInjectorProvider

/*
 * https://zarnekow.blogspot.com/2014/10/testing-multiple-xtext-dsls.html
 */

class LanguagesInjectorProvider extends GrancinInjectorProvider {
	override protected internalCreateInjector() {
		// trigger injector creation of other language
		new FrancaIDLInjectorProvider().injector
		
		super.internalCreateInjector()
	}	
}
