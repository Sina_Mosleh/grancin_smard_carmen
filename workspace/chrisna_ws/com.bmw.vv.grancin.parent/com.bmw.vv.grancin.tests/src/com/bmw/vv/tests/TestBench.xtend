package com.bmw.vv.tests

/*
 * '''<input>''' -> '''<output after code generation>'''
 */

interface TestBench {
	val importFKeySearchPosition =
	'''
		import KeySearch.KeySearchPos.*
		import KeySearch.KeySearchPos.REQREPKeySearchPos.*
		import KeySearch.KeySearchPos.PUBKeySearchPos.*
	'''
	
	val importFDriverSelectionModule =
	'''
		import someip.BMW.INFRASTRUCTURE.*
		import someip.BMW.INFRASTRUCTURE.DriverSelectionModule.*
		import someip.BMW.INFRASTRUCTURE.DriverSelectionModule.setEntryInMappingTable.*
	'''
	
	val importFGeneralTypes =
	'''
		import someip.generalTypes.BdcIdentityMappingEntry.*
		import someip.generalTypes.*
	'''
	
	val importFAlienDetection =
	'''
		import ADAR.AlienDetection.*
		import ADAR.AlienDetection.isContactConfirmed.*
		import ADAR.AlienDetection.ConfirmationStatus.*
		import ADAR.AlienDetection.publishADARResults.*
	'''
	
	val oneGivenWithAnd = 		
	'''
		Scenario: one
			Given in method PUBKeySearchPos 	with KeySearchPos== "WECH"
			And   in method REQREPKeySearchPos	with REP		 == 123
	'''
	->
	'''
	'''
	
	val scenarioWithKeywordInTitle =
	''' 
		Scenario: Provide estimated key position on demand
			Given in method PUBKeySearchPos with KeySearchPos=="WECH" at T_i
			Then send method REQREPKeySearchPos with REQ="WECH"
	'''
	->
	'''
	'''
	
	val scenarioWithOtherKeywordsInTitle =
	''' 
		Scenario: Method publishADARResults is not sent if setActive is deactivated (setActive equals false)
	'''
	->
	'''
	'''
	
	val sendMethodOnTriggerWithGivenMaxLatency =
	''' 
		Scenario: Provide estimated key position
			Given in method PUBKeySearchPos with KeySearchPos=="WECH" at t0
			Then send method REQREPKeySearchPos with REQ="WECH" at t1 <= t0 + 40 ms
	'''
	->
	'''
	'''
	
	val inMethodAtPointInTime =
	''' 
		Scenario: one
			Given in method PUBKeySearchPos with KeySearchPos=="WECH" at T_i 
	'''
	->
	'''
	'''
	
	val inMethodWithParameters =
	''' 
		Scenario: one
			Given in method PUBKeySearchPos 		with REP== true
			Given in method PUBKeySearchPos 		with REP ==False
			Given in method PUBKeySearchPos 		with REP==9999
			Given in method PUBKeySearchPos 		with REP == "BULLSHIT"
			And   in method REQREPKeySearchPos 	with REP == 12345.54321 
	'''
	->
	'''
	'''
	
	val noInput =
	''' 
		Scenario: one
			Given no precondition
			Given no trigger
			When  no precondition
			When  no trigger 
	'''
	->
	'''
	'''
	
	val reproduction =
	''' 
		Scenario: one
		
		Scenario: two
			
		Scenario: three
			Given a reproduction of "one"
			When  a reproduction of "two"
	'''
	->
	'''
	'''
	
	val continuation =
	''' 
		Scenario: one
			Given no precondition
			Then send broadcast PUBKeySearchPos on change
			
		Scenario: two
			Given a reproduction of "one"
			And PUBKeySearchPos with KeySearchPos=="anything"
	'''
	->
	'''
	'''
	
	val givenAnAttribute = 		
	'''
		Scenario: one
			Given attribute ResultKeySearchPos is set to 0
			When  attribute ResultKeySearchPos is set to 1
			And   in method REQREPKeySearchPos	with REP == 123
	'''
	->
	'''
	'''
	
	val anAttributeWithValuesOfDifferentType = 		
	'''
		Scenario Outline: one
			Given attribute setActive is set to <val>
			And attribute setActive is set to true
			And attribute setActive is set to ConfirmationStatus.VALID
			
		Examples:
			| val   |
			| true  |
			| false |
	'''
	->
	'''
	'''
	
	val attributeValueAsResult = 		
	'''
		Scenario: one
			Then attribute ResultKeySearchPos is set to 2
	'''
	->
	'''
	'''
	
	val aSingleSendMethod =
	''' 
		Scenario: one
			Then send method REQREPKeySearchPos with REQ="WECH"
	'''
	->
	'''
	'''
	
	val aSingleSendMethodOnChange =
	''' 
		Scenario: one
			Then send method REQREPKeySearchPos with REQ="WECH" on change
	'''
	->
	'''
	'''
	
	val aSinglePeriodicSendMethod =
	''' 
		Scenario: hundred
			Then send method REQREPKeySearchPos with REQ="WECH" periodically with period T_i = 1 s
	'''
	->
	'''
	'''
	
	val sendMethodPeriodicallyWithDifferentUnits =
	''' 
		Scenario: one
			Then send method REQREPKeySearchPos with REQ="WECH" periodically with period T_i=1 ms 
			And  send method REQREPKeySearchPos with REQ="WECH" periodically with period x =1 us 
			And  send method REQREPKeySearchPos with REQ="WECH" periodically with period y=5 d
	'''
	->
	'''
	'''
	
	val inMethodWithMultipleParameters =
	''' 
		Scenario: one
			Given in method PUBKeySearchPos with KeySearchPos=="WECH", KeySearchPos=="FLIEGEN"
			And   in method PUBKeySearchPos with KeySearchPos==90,   KeySearchPos==False
			When  in method PUBKeySearchPos with KeySearchPos==90.0, KeySearchPos==False
			Then  send method REQREPKeySearchPos
	'''
	->
	'''
	'''
	
	val inMethodWithTimeConstraints =
	''' 
		Scenario: one
			Given in method PUBKeySearchPos with KeySearchPos=="FLIEGEN" at Ti
			When  in method PUBKeySearchPos with KeySearchPos=="WECH"    at To <= Ti + 40 ms
			And   in method PUBKeySearchPos with KeySearchPos=="WECH"    at To >  Ti + 40 ms
			And   in method PUBKeySearchPos with KeySearchPos=="WECH"    at To >  Ti - 10 ms
			Then  send method REQREPKeySearchPos
	'''
	->
	'''
	'''
	
	val completeYetSimpleFeature =
	''' 
		Feature: this is some test feature
		  first compound - Gherkin, Franca gives Grancin
		
		  Scenario: first scenario
		  some description
		   #  Comment
		    Given in method REQREPKeySearchPos with REP==0, REP==1 at T_rel==0 us
		    When in method REQREPKeySearchPos at T_rel< 40 ms
		    Then send method REQREPKeySearchPos periodically with period Tp = 100 ms
		
		  Scenario: second scenario
		    Given in method PUBKeySearchPos
		    When in method REQREPKeySearchPos with REQ==90, REQ==False at T_rel< 40 ms
		    Then send method PUBKeySearchPos periodically with period Ti = 100 ms 
	'''
	->
	'''
	'''
	
	val scenarioOneGivenWithFKeySearchPosition =
	'''
		Feature:   ApprovalUnit
		  This is some bla bla bla comment and/or description
		  Scenario: one
		  some more description
		   #  some comment
		    Given in method REQREPKeySearchPos with REQ				==42
		    And   in method PUBKeySearchPos    with KeySearchPos	== 2
		    
	'''
	
	val singleNestingLevel =
	'''
		Scenario: wtf
			Given in method setEntryInMappingTable with newEntry->tokenId == 42, newEntry->BdcIdentityMappingEntry.piaProfileId == 42
	'''
	->
	'''
	'''
	
	val multipleNestingLevels =
	'''
		Scenario: wtf
			# the following member accesses do not make sense from a semantical point of view
			# this is for unit testing purpose of syntax only
			Given in method setEntryInMappingTable with newEntry->tokenId->BdcIdentityMappingEntry.piaProfileId == 42
	'''
	->
	'''
	'''
	
	val attributeAccess =
	'''
		Scenario: wtf
			Given no precondition
			Then attribute bdcIdentityMappingTable[<tokenId>]->BdcIdentityMappingEntry.piaProfileId equals 15
			And  attribute bdcIdentityMappingTable[   42    ]->BdcIdentityMappingEntry.piaProfileId equals 42
			And  attribute bdcIdentityMappingTable[123456789]->BdcIdentityMappingEntry.piaProfileId equals 24
	'''
	->
	'''
	'''
}
