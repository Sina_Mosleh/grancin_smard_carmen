/**
 */
package chrisna.tests;

import chrisna.ChrisnaFactory;
import chrisna.ScenContainerWithoutTime;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Scen Container Without Time</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScenContainerWithoutTimeTest extends TestCase {

	/**
	 * The fixture for this Scen Container Without Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenContainerWithoutTime fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ScenContainerWithoutTimeTest.class);
	}

	/**
	 * Constructs a new Scen Container Without Time test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainerWithoutTimeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Scen Container Without Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ScenContainerWithoutTime fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Scen Container Without Time test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenContainerWithoutTime getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ChrisnaFactory.eINSTANCE.createScenContainerWithoutTime());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ScenContainerWithoutTimeTest
