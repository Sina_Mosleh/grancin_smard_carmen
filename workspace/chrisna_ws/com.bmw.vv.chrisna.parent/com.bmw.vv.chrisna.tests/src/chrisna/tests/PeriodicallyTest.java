/**
 */
package chrisna.tests;

import chrisna.ChrisnaFactory;
import chrisna.Periodically;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Periodically</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class PeriodicallyTest extends TimeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PeriodicallyTest.class);
	}

	/**
	 * Constructs a new Periodically test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PeriodicallyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Periodically test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Periodically getFixture() {
		return (Periodically)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ChrisnaFactory.eINSTANCE.createPeriodically());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //PeriodicallyTest
