/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.Assignment#getLeftSide <em>Left Side</em>}</li>
 *   <li>{@link chrisna.Assignment#getRightSide <em>Right Side</em>}</li>
 *   <li>{@link chrisna.Assignment#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends EObject {
	/**
	 * Returns the value of the '<em><b>Left Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Side</em>' attribute.
	 * @see #setLeftSide(String)
	 * @see chrisna.ChrisnaPackage#getAssignment_LeftSide()
	 * @model required="true"
	 * @generated
	 */
	String getLeftSide();

	/**
	 * Sets the value of the '{@link chrisna.Assignment#getLeftSide <em>Left Side</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Side</em>' attribute.
	 * @see #getLeftSide()
	 * @generated
	 */
	void setLeftSide(String value);

	/**
	 * Returns the value of the '<em><b>Right Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Side</em>' attribute.
	 * @see #setRightSide(String)
	 * @see chrisna.ChrisnaPackage#getAssignment_RightSide()
	 * @model required="true"
	 * @generated
	 */
	String getRightSide();

	/**
	 * Sets the value of the '{@link chrisna.Assignment#getRightSide <em>Right Side</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Side</em>' attribute.
	 * @see #getRightSide()
	 * @generated
	 */
	void setRightSide(String value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #setOperation(Operation)
	 * @see chrisna.ChrisnaPackage#getAssignment_Operation()
	 * @model
	 * @generated
	 */
	Operation getOperation();

	/**
	 * Sets the value of the '{@link chrisna.Assignment#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(Operation value);

} // Assignment
