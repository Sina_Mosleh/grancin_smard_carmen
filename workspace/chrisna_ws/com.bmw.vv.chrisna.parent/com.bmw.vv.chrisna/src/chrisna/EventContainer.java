/**
 */
package chrisna;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.EventContainer#getSendevent <em>Sendevent</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getEventContainer()
 * @model
 * @generated
 */
public interface EventContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Sendevent</b></em>' containment reference list.
	 * The list contents are of type {@link chrisna.SendEvent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sendevent</em>' containment reference list.
	 * @see chrisna.ChrisnaPackage#getEventContainer_Sendevent()
	 * @model containment="true"
	 * @generated
	 */
	EList<SendEvent> getSendevent();

} // EventContainer
