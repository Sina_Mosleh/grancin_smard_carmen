/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Finder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.NodeFinder#getScencontainer <em>Scencontainer</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getNodeFinder()
 * @model
 * @generated
 */
public interface NodeFinder extends EObject {
	/**
	 * Returns the value of the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainer</em>' reference.
	 * @see #setScencontainer(ScenContainer)
	 * @see chrisna.ChrisnaPackage#getNodeFinder_Scencontainer()
	 * @model
	 * @generated
	 */
	ScenContainer getScencontainer();

	/**
	 * Sets the value of the '{@link chrisna.NodeFinder#getScencontainer <em>Scencontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainer</em>' reference.
	 * @see #getScencontainer()
	 * @generated
	 */
	void setScencontainer(ScenContainer value);

} // NodeFinder
