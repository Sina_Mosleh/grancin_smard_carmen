/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.Counter;
import chrisna.HistoryDispach;
import chrisna.NodeFinder;
import chrisna.ScenContainerWithTime;
import chrisna.ScenContainerWithoutTime;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History Dispach</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.HistoryDispachImpl#getScencontainerwithtime <em>Scencontainerwithtime</em>}</li>
 *   <li>{@link chrisna.impl.HistoryDispachImpl#getScencontainerwithouttime <em>Scencontainerwithouttime</em>}</li>
 *   <li>{@link chrisna.impl.HistoryDispachImpl#getNodefinder <em>Nodefinder</em>}</li>
 *   <li>{@link chrisna.impl.HistoryDispachImpl#getCounter <em>Counter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HistoryDispachImpl extends MinimalEObjectImpl.Container implements HistoryDispach {
	/**
	 * The cached value of the '{@link #getScencontainerwithtime() <em>Scencontainerwithtime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScencontainerwithtime()
	 * @generated
	 * @ordered
	 */
	protected ScenContainerWithTime scencontainerwithtime;

	/**
	 * The cached value of the '{@link #getScencontainerwithouttime() <em>Scencontainerwithouttime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScencontainerwithouttime()
	 * @generated
	 * @ordered
	 */
	protected ScenContainerWithoutTime scencontainerwithouttime;

	/**
	 * The cached value of the '{@link #getNodefinder() <em>Nodefinder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodefinder()
	 * @generated
	 * @ordered
	 */
	protected NodeFinder nodefinder;

	/**
	 * The cached value of the '{@link #getCounter() <em>Counter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCounter()
	 * @generated
	 * @ordered
	 */
	protected Counter counter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HistoryDispachImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.HISTORY_DISPACH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainerWithTime getScencontainerwithtime() {
		if (scencontainerwithtime != null && scencontainerwithtime.eIsProxy()) {
			InternalEObject oldScencontainerwithtime = (InternalEObject)scencontainerwithtime;
			scencontainerwithtime = (ScenContainerWithTime)eResolveProxy(oldScencontainerwithtime);
			if (scencontainerwithtime != oldScencontainerwithtime) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME, oldScencontainerwithtime, scencontainerwithtime));
			}
		}
		return scencontainerwithtime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainerWithTime basicGetScencontainerwithtime() {
		return scencontainerwithtime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setScencontainerwithtime(ScenContainerWithTime newScencontainerwithtime) {
		ScenContainerWithTime oldScencontainerwithtime = scencontainerwithtime;
		scencontainerwithtime = newScencontainerwithtime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME, oldScencontainerwithtime, scencontainerwithtime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainerWithoutTime getScencontainerwithouttime() {
		if (scencontainerwithouttime != null && scencontainerwithouttime.eIsProxy()) {
			InternalEObject oldScencontainerwithouttime = (InternalEObject)scencontainerwithouttime;
			scencontainerwithouttime = (ScenContainerWithoutTime)eResolveProxy(oldScencontainerwithouttime);
			if (scencontainerwithouttime != oldScencontainerwithouttime) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME, oldScencontainerwithouttime, scencontainerwithouttime));
			}
		}
		return scencontainerwithouttime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainerWithoutTime basicGetScencontainerwithouttime() {
		return scencontainerwithouttime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setScencontainerwithouttime(ScenContainerWithoutTime newScencontainerwithouttime) {
		ScenContainerWithoutTime oldScencontainerwithouttime = scencontainerwithouttime;
		scencontainerwithouttime = newScencontainerwithouttime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME, oldScencontainerwithouttime, scencontainerwithouttime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NodeFinder getNodefinder() {
		if (nodefinder != null && nodefinder.eIsProxy()) {
			InternalEObject oldNodefinder = (InternalEObject)nodefinder;
			nodefinder = (NodeFinder)eResolveProxy(oldNodefinder);
			if (nodefinder != oldNodefinder) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.HISTORY_DISPACH__NODEFINDER, oldNodefinder, nodefinder));
			}
		}
		return nodefinder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeFinder basicGetNodefinder() {
		return nodefinder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNodefinder(NodeFinder newNodefinder) {
		NodeFinder oldNodefinder = nodefinder;
		nodefinder = newNodefinder;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.HISTORY_DISPACH__NODEFINDER, oldNodefinder, nodefinder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Counter getCounter() {
		if (counter != null && counter.eIsProxy()) {
			InternalEObject oldCounter = (InternalEObject)counter;
			counter = (Counter)eResolveProxy(oldCounter);
			if (counter != oldCounter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.HISTORY_DISPACH__COUNTER, oldCounter, counter));
			}
		}
		return counter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Counter basicGetCounter() {
		return counter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCounter(Counter newCounter) {
		Counter oldCounter = counter;
		counter = newCounter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.HISTORY_DISPACH__COUNTER, oldCounter, counter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME:
				if (resolve) return getScencontainerwithtime();
				return basicGetScencontainerwithtime();
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME:
				if (resolve) return getScencontainerwithouttime();
				return basicGetScencontainerwithouttime();
			case ChrisnaPackage.HISTORY_DISPACH__NODEFINDER:
				if (resolve) return getNodefinder();
				return basicGetNodefinder();
			case ChrisnaPackage.HISTORY_DISPACH__COUNTER:
				if (resolve) return getCounter();
				return basicGetCounter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME:
				setScencontainerwithtime((ScenContainerWithTime)newValue);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME:
				setScencontainerwithouttime((ScenContainerWithoutTime)newValue);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__NODEFINDER:
				setNodefinder((NodeFinder)newValue);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__COUNTER:
				setCounter((Counter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME:
				setScencontainerwithtime((ScenContainerWithTime)null);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME:
				setScencontainerwithouttime((ScenContainerWithoutTime)null);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__NODEFINDER:
				setNodefinder((NodeFinder)null);
				return;
			case ChrisnaPackage.HISTORY_DISPACH__COUNTER:
				setCounter((Counter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHTIME:
				return scencontainerwithtime != null;
			case ChrisnaPackage.HISTORY_DISPACH__SCENCONTAINERWITHOUTTIME:
				return scencontainerwithouttime != null;
			case ChrisnaPackage.HISTORY_DISPACH__NODEFINDER:
				return nodefinder != null;
			case ChrisnaPackage.HISTORY_DISPACH__COUNTER:
				return counter != null;
		}
		return super.eIsSet(featureID);
	}

} //HistoryDispachImpl
