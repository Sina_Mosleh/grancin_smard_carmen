/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.ScenContainer;
import chrisna.ScenContainerWithTime;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scen Container With Time</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.ScenContainerWithTimeImpl#getScencontainer <em>Scencontainer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScenContainerWithTimeImpl extends MinimalEObjectImpl.Container implements ScenContainerWithTime {
	/**
	 * The cached value of the '{@link #getScencontainer() <em>Scencontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScencontainer()
	 * @generated
	 * @ordered
	 */
	protected ScenContainer scencontainer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenContainerWithTimeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.SCEN_CONTAINER_WITH_TIME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainer getScencontainer() {
		if (scencontainer != null && scencontainer.eIsProxy()) {
			InternalEObject oldScencontainer = (InternalEObject)scencontainer;
			scencontainer = (ScenContainer)eResolveProxy(oldScencontainer);
			if (scencontainer != oldScencontainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER, oldScencontainer, scencontainer));
			}
		}
		return scencontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScenContainer basicGetScencontainer() {
		return scencontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setScencontainer(ScenContainer newScencontainer) {
		ScenContainer oldScencontainer = scencontainer;
		scencontainer = newScencontainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER, oldScencontainer, scencontainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER:
				if (resolve) return getScencontainer();
				return basicGetScencontainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER:
				setScencontainer((ScenContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER:
				setScencontainer((ScenContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.SCEN_CONTAINER_WITH_TIME__SCENCONTAINER:
				return scencontainer != null;
		}
		return super.eIsSet(featureID);
	}

} //ScenContainerWithTimeImpl
