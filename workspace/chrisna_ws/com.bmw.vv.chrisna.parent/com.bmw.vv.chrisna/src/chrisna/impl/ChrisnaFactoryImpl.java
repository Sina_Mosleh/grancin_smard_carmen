/**
 */
package chrisna.impl;

import chrisna.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ChrisnaFactoryImpl extends EFactoryImpl implements ChrisnaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ChrisnaFactory init() {
		try {
			ChrisnaFactory theChrisnaFactory = (ChrisnaFactory)EPackage.Registry.INSTANCE.getEFactory(ChrisnaPackage.eNS_URI);
			if (theChrisnaFactory != null) {
				return theChrisnaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ChrisnaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChrisnaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ChrisnaPackage.OPERATION: return createOperation();
			case ChrisnaPackage.ACTION: return createaction();
			case ChrisnaPackage.STATE: return createstate();
			case ChrisnaPackage.STATE_CONTAINER: return createstateContainer();
			case ChrisnaPackage.SCEN_CONTAINER_WITH_TIME: return createScenContainerWithTime();
			case ChrisnaPackage.INIT_STATE: return createinitState();
			case ChrisnaPackage.SCEN_CONTAINER_WITHOUT_TIME: return createScenContainerWithoutTime();
			case ChrisnaPackage.HISTORY_DISPACH: return createHistoryDispach();
			case ChrisnaPackage.FILTER: return createFilter();
			case ChrisnaPackage.CHRISNA_FSM: return createChrisnaFSM();
			case ChrisnaPackage.COUNTER: return createCounter();
			case ChrisnaPackage.WARNING_ERROR: return createWarningError();
			case ChrisnaPackage.NODE_FINDER: return createNodeFinder();
			case ChrisnaPackage.ASSIGNMENT: return createAssignment();
			case ChrisnaPackage.ASSIG_CONTAINER: return createAssigContainer();
			case ChrisnaPackage.SEND_EVENT: return createSendEvent();
			case ChrisnaPackage.TIME: return createTime();
			case ChrisnaPackage.ON_CHANGE: return createOnChange();
			case ChrisnaPackage.PERIODICALLY: return createPeriodically();
			case ChrisnaPackage.TIME_WINDOW: return createTimeWindow();
			case ChrisnaPackage.EVENT_CONTAINER: return createEventContainer();
			case ChrisnaPackage.SCENARIO: return createScenario();
			case ChrisnaPackage.CONDITION: return createCondition();
			case ChrisnaPackage.COND_CONTAINER: return createCondContainer();
			case ChrisnaPackage.SCEN_CONTAINER: return createScenContainer();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ChrisnaPackage.TRIGGER:
				return createTriggerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ChrisnaPackage.TRIGGER:
				return convertTriggerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public action createaction() {
		actionImpl action = new actionImpl();
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public state createstate() {
		stateImpl state = new stateImpl();
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public stateContainer createstateContainer() {
		stateContainerImpl stateContainer = new stateContainerImpl();
		return stateContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainerWithTime createScenContainerWithTime() {
		ScenContainerWithTimeImpl scenContainerWithTime = new ScenContainerWithTimeImpl();
		return scenContainerWithTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public initState createinitState() {
		initStateImpl initState = new initStateImpl();
		return initState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainerWithoutTime createScenContainerWithoutTime() {
		ScenContainerWithoutTimeImpl scenContainerWithoutTime = new ScenContainerWithoutTimeImpl();
		return scenContainerWithoutTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public HistoryDispach createHistoryDispach() {
		HistoryDispachImpl historyDispach = new HistoryDispachImpl();
		return historyDispach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Filter createFilter() {
		FilterImpl filter = new FilterImpl();
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ChrisnaFSM createChrisnaFSM() {
		ChrisnaFSMImpl chrisnaFSM = new ChrisnaFSMImpl();
		return chrisnaFSM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Counter createCounter() {
		CounterImpl counter = new CounterImpl();
		return counter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public WarningError createWarningError() {
		WarningErrorImpl warningError = new WarningErrorImpl();
		return warningError;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NodeFinder createNodeFinder() {
		NodeFinderImpl nodeFinder = new NodeFinderImpl();
		return nodeFinder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Assignment createAssignment() {
		AssignmentImpl assignment = new AssignmentImpl();
		return assignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssigContainer createAssigContainer() {
		AssigContainerImpl assigContainer = new AssigContainerImpl();
		return assigContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SendEvent createSendEvent() {
		SendEventImpl sendEvent = new SendEventImpl();
		return sendEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Time createTime() {
		TimeImpl time = new TimeImpl();
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public OnChange createOnChange() {
		OnChangeImpl onChange = new OnChangeImpl();
		return onChange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Periodically createPeriodically() {
		PeriodicallyImpl periodically = new PeriodicallyImpl();
		return periodically;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public TimeWindow createTimeWindow() {
		TimeWindowImpl timeWindow = new TimeWindowImpl();
		return timeWindow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EventContainer createEventContainer() {
		EventContainerImpl eventContainer = new EventContainerImpl();
		return eventContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Scenario createScenario() {
		ScenarioImpl scenario = new ScenarioImpl();
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CondContainer createCondContainer() {
		CondContainerImpl condContainer = new CondContainerImpl();
		return condContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ScenContainer createScenContainer() {
		ScenContainerImpl scenContainer = new ScenContainerImpl();
		return scenContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trigger createTriggerFromString(EDataType eDataType, String initialValue) {
		Trigger result = Trigger.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTriggerToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ChrisnaPackage getChrisnaPackage() {
		return (ChrisnaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ChrisnaPackage getPackage() {
		return ChrisnaPackage.eINSTANCE;
	}

} //ChrisnaFactoryImpl
