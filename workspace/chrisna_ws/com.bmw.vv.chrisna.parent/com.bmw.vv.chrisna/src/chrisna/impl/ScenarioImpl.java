/**
 */
package chrisna.impl;

import chrisna.ChrisnaPackage;
import chrisna.CondContainer;
import chrisna.Scenario;
import chrisna.stateContainer;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.ScenarioImpl#getCurrentStates <em>Current States</em>}</li>
 *   <li>{@link chrisna.impl.ScenarioImpl#getNextStates <em>Next States</em>}</li>
 *   <li>{@link chrisna.impl.ScenarioImpl#getCondcontainer <em>Condcontainer</em>}</li>
 *   <li>{@link chrisna.impl.ScenarioImpl#getName <em>Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScenarioImpl extends MinimalEObjectImpl.Container implements Scenario {
	/**
	 * The cached value of the '{@link #getCurrentStates() <em>Current States</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentStates()
	 * @generated
	 * @ordered
	 */
	protected stateContainer currentStates;

	/**
	 * The cached value of the '{@link #getNextStates() <em>Next States</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextStates()
	 * @generated
	 * @ordered
	 */
	protected stateContainer nextStates;

	/**
	 * The cached value of the '{@link #getCondcontainer() <em>Condcontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondcontainer()
	 * @generated
	 * @ordered
	 */
	protected CondContainer condcontainer;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public stateContainer getCurrentStates() {
		if (currentStates != null && currentStates.eIsProxy()) {
			InternalEObject oldCurrentStates = (InternalEObject)currentStates;
			currentStates = (stateContainer)eResolveProxy(oldCurrentStates);
			if (currentStates != oldCurrentStates) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.SCENARIO__CURRENT_STATES, oldCurrentStates, currentStates));
			}
		}
		return currentStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public stateContainer basicGetCurrentStates() {
		return currentStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCurrentStates(stateContainer newCurrentStates) {
		stateContainer oldCurrentStates = currentStates;
		currentStates = newCurrentStates;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.SCENARIO__CURRENT_STATES, oldCurrentStates, currentStates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public stateContainer getNextStates() {
		if (nextStates != null && nextStates.eIsProxy()) {
			InternalEObject oldNextStates = (InternalEObject)nextStates;
			nextStates = (stateContainer)eResolveProxy(oldNextStates);
			if (nextStates != oldNextStates) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.SCENARIO__NEXT_STATES, oldNextStates, nextStates));
			}
		}
		return nextStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public stateContainer basicGetNextStates() {
		return nextStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNextStates(stateContainer newNextStates) {
		stateContainer oldNextStates = nextStates;
		nextStates = newNextStates;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.SCENARIO__NEXT_STATES, oldNextStates, nextStates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public CondContainer getCondcontainer() {
		if (condcontainer != null && condcontainer.eIsProxy()) {
			InternalEObject oldCondcontainer = (InternalEObject)condcontainer;
			condcontainer = (CondContainer)eResolveProxy(oldCondcontainer);
			if (condcontainer != oldCondcontainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.SCENARIO__CONDCONTAINER, oldCondcontainer, condcontainer));
			}
		}
		return condcontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CondContainer basicGetCondcontainer() {
		return condcontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setCondcontainer(CondContainer newCondcontainer) {
		CondContainer oldCondcontainer = condcontainer;
		condcontainer = newCondcontainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.SCENARIO__CONDCONTAINER, oldCondcontainer, condcontainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.SCENARIO__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.SCENARIO__CURRENT_STATES:
				if (resolve) return getCurrentStates();
				return basicGetCurrentStates();
			case ChrisnaPackage.SCENARIO__NEXT_STATES:
				if (resolve) return getNextStates();
				return basicGetNextStates();
			case ChrisnaPackage.SCENARIO__CONDCONTAINER:
				if (resolve) return getCondcontainer();
				return basicGetCondcontainer();
			case ChrisnaPackage.SCENARIO__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.SCENARIO__CURRENT_STATES:
				setCurrentStates((stateContainer)newValue);
				return;
			case ChrisnaPackage.SCENARIO__NEXT_STATES:
				setNextStates((stateContainer)newValue);
				return;
			case ChrisnaPackage.SCENARIO__CONDCONTAINER:
				setCondcontainer((CondContainer)newValue);
				return;
			case ChrisnaPackage.SCENARIO__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.SCENARIO__CURRENT_STATES:
				setCurrentStates((stateContainer)null);
				return;
			case ChrisnaPackage.SCENARIO__NEXT_STATES:
				setNextStates((stateContainer)null);
				return;
			case ChrisnaPackage.SCENARIO__CONDCONTAINER:
				setCondcontainer((CondContainer)null);
				return;
			case ChrisnaPackage.SCENARIO__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.SCENARIO__CURRENT_STATES:
				return currentStates != null;
			case ChrisnaPackage.SCENARIO__NEXT_STATES:
				return nextStates != null;
			case ChrisnaPackage.SCENARIO__CONDCONTAINER:
				return condcontainer != null;
			case ChrisnaPackage.SCENARIO__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ScenarioImpl
