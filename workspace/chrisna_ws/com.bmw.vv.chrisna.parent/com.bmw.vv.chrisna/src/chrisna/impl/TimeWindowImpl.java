/**
 */
package chrisna.impl;

import chrisna.AssigContainer;
import chrisna.ChrisnaPackage;
import chrisna.TimeWindow;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time Window</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chrisna.impl.TimeWindowImpl#getAssigcontainer <em>Assigcontainer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeWindowImpl extends PeriodicallyImpl implements TimeWindow {
	/**
	 * The cached value of the '{@link #getAssigcontainer() <em>Assigcontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssigcontainer()
	 * @generated
	 * @ordered
	 */
	protected AssigContainer assigcontainer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeWindowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ChrisnaPackage.Literals.TIME_WINDOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AssigContainer getAssigcontainer() {
		if (assigcontainer != null && assigcontainer.eIsProxy()) {
			InternalEObject oldAssigcontainer = (InternalEObject)assigcontainer;
			assigcontainer = (AssigContainer)eResolveProxy(oldAssigcontainer);
			if (assigcontainer != oldAssigcontainer) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER, oldAssigcontainer, assigcontainer));
			}
		}
		return assigcontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssigContainer basicGetAssigcontainer() {
		return assigcontainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAssigcontainer(AssigContainer newAssigcontainer) {
		AssigContainer oldAssigcontainer = assigcontainer;
		assigcontainer = newAssigcontainer;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER, oldAssigcontainer, assigcontainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER:
				if (resolve) return getAssigcontainer();
				return basicGetAssigcontainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER:
				setAssigcontainer((AssigContainer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER:
				setAssigcontainer((AssigContainer)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ChrisnaPackage.TIME_WINDOW__ASSIGCONTAINER:
				return assigcontainer != null;
		}
		return super.eIsSet(featureID);
	}

} //TimeWindowImpl
