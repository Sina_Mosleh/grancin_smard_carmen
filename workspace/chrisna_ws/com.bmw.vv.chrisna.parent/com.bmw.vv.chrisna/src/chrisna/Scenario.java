/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.Scenario#getCurrentStates <em>Current States</em>}</li>
 *   <li>{@link chrisna.Scenario#getNextStates <em>Next States</em>}</li>
 *   <li>{@link chrisna.Scenario#getCondcontainer <em>Condcontainer</em>}</li>
 *   <li>{@link chrisna.Scenario#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getScenario()
 * @model
 * @generated
 */
public interface Scenario extends EObject {
	/**
	 * Returns the value of the '<em><b>Current States</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current States</em>' reference.
	 * @see #setCurrentStates(stateContainer)
	 * @see chrisna.ChrisnaPackage#getScenario_CurrentStates()
	 * @model
	 * @generated
	 */
	stateContainer getCurrentStates();

	/**
	 * Sets the value of the '{@link chrisna.Scenario#getCurrentStates <em>Current States</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current States</em>' reference.
	 * @see #getCurrentStates()
	 * @generated
	 */
	void setCurrentStates(stateContainer value);

	/**
	 * Returns the value of the '<em><b>Next States</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next States</em>' reference.
	 * @see #setNextStates(stateContainer)
	 * @see chrisna.ChrisnaPackage#getScenario_NextStates()
	 * @model
	 * @generated
	 */
	stateContainer getNextStates();

	/**
	 * Sets the value of the '{@link chrisna.Scenario#getNextStates <em>Next States</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next States</em>' reference.
	 * @see #getNextStates()
	 * @generated
	 */
	void setNextStates(stateContainer value);

	/**
	 * Returns the value of the '<em><b>Condcontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condcontainer</em>' reference.
	 * @see #setCondcontainer(CondContainer)
	 * @see chrisna.ChrisnaPackage#getScenario_Condcontainer()
	 * @model
	 * @generated
	 */
	CondContainer getCondcontainer();

	/**
	 * Sets the value of the '{@link chrisna.Scenario#getCondcontainer <em>Condcontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condcontainer</em>' reference.
	 * @see #getCondcontainer()
	 * @generated
	 */
	void setCondcontainer(CondContainer value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see chrisna.ChrisnaPackage#getScenario_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link chrisna.Scenario#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Scenario
