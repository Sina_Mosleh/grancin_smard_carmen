/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FSM</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.ChrisnaFSM#getInitstate <em>Initstate</em>}</li>
 *   <li>{@link chrisna.ChrisnaFSM#getScencontainer <em>Scencontainer</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getChrisnaFSM()
 * @model
 * @generated
 */
public interface ChrisnaFSM extends EObject {
	/**
	 * Returns the value of the '<em><b>Initstate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initstate</em>' reference.
	 * @see #setInitstate(initState)
	 * @see chrisna.ChrisnaPackage#getChrisnaFSM_Initstate()
	 * @model
	 * @generated
	 */
	initState getInitstate();

	/**
	 * Sets the value of the '{@link chrisna.ChrisnaFSM#getInitstate <em>Initstate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initstate</em>' reference.
	 * @see #getInitstate()
	 * @generated
	 */
	void setInitstate(initState value);

	/**
	 * Returns the value of the '<em><b>Scencontainer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainer</em>' reference.
	 * @see #setScencontainer(ScenContainer)
	 * @see chrisna.ChrisnaPackage#getChrisnaFSM_Scencontainer()
	 * @model
	 * @generated
	 */
	ScenContainer getScencontainer();

	/**
	 * Sets the value of the '{@link chrisna.ChrisnaFSM#getScencontainer <em>Scencontainer</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainer</em>' reference.
	 * @see #getScencontainer()
	 * @generated
	 */
	void setScencontainer(ScenContainer value);

} // ChrisnaFSM
