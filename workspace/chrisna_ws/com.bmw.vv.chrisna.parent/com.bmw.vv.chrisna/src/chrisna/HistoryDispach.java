/**
 */
package chrisna;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Dispach</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chrisna.HistoryDispach#getScencontainerwithtime <em>Scencontainerwithtime</em>}</li>
 *   <li>{@link chrisna.HistoryDispach#getScencontainerwithouttime <em>Scencontainerwithouttime</em>}</li>
 *   <li>{@link chrisna.HistoryDispach#getNodefinder <em>Nodefinder</em>}</li>
 *   <li>{@link chrisna.HistoryDispach#getCounter <em>Counter</em>}</li>
 * </ul>
 *
 * @see chrisna.ChrisnaPackage#getHistoryDispach()
 * @model
 * @generated
 */
public interface HistoryDispach extends EObject {
	/**
	 * Returns the value of the '<em><b>Scencontainerwithtime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainerwithtime</em>' reference.
	 * @see #setScencontainerwithtime(ScenContainerWithTime)
	 * @see chrisna.ChrisnaPackage#getHistoryDispach_Scencontainerwithtime()
	 * @model
	 * @generated
	 */
	ScenContainerWithTime getScencontainerwithtime();

	/**
	 * Sets the value of the '{@link chrisna.HistoryDispach#getScencontainerwithtime <em>Scencontainerwithtime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainerwithtime</em>' reference.
	 * @see #getScencontainerwithtime()
	 * @generated
	 */
	void setScencontainerwithtime(ScenContainerWithTime value);

	/**
	 * Returns the value of the '<em><b>Scencontainerwithouttime</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scencontainerwithouttime</em>' reference.
	 * @see #setScencontainerwithouttime(ScenContainerWithoutTime)
	 * @see chrisna.ChrisnaPackage#getHistoryDispach_Scencontainerwithouttime()
	 * @model
	 * @generated
	 */
	ScenContainerWithoutTime getScencontainerwithouttime();

	/**
	 * Sets the value of the '{@link chrisna.HistoryDispach#getScencontainerwithouttime <em>Scencontainerwithouttime</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scencontainerwithouttime</em>' reference.
	 * @see #getScencontainerwithouttime()
	 * @generated
	 */
	void setScencontainerwithouttime(ScenContainerWithoutTime value);

	/**
	 * Returns the value of the '<em><b>Nodefinder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nodefinder</em>' reference.
	 * @see #setNodefinder(NodeFinder)
	 * @see chrisna.ChrisnaPackage#getHistoryDispach_Nodefinder()
	 * @model
	 * @generated
	 */
	NodeFinder getNodefinder();

	/**
	 * Sets the value of the '{@link chrisna.HistoryDispach#getNodefinder <em>Nodefinder</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nodefinder</em>' reference.
	 * @see #getNodefinder()
	 * @generated
	 */
	void setNodefinder(NodeFinder value);

	/**
	 * Returns the value of the '<em><b>Counter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Counter</em>' reference.
	 * @see #setCounter(Counter)
	 * @see chrisna.ChrisnaPackage#getHistoryDispach_Counter()
	 * @model
	 * @generated
	 */
	Counter getCounter();

	/**
	 * Sets the value of the '{@link chrisna.HistoryDispach#getCounter <em>Counter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Counter</em>' reference.
	 * @see #getCounter()
	 * @generated
	 */
	void setCounter(Counter value);

} // HistoryDispach
