package com.bmw.vv.tests

/*
 * '''<input>''' -> '''<output after code generation>'''
 */

interface completeScenarioWithFrancaReferences {
	val justAFeatureTitle = 
	'''
		Feature: ApprovalUnit
	''' ->
  '''
		statechart:
		  name: ApprovalUnit
		  description: <add a meaningful description of the feature's purpose here>
		  root state:
		    name: active
	'''
	
	val oneScenarioWithOneGiven = 
	'''
		Scenario: one
			Given I send event x
	'''
	-> 
	'''
		statechart:
		  name: <REPLACE_THIS_BY_A_MEANINGFUL_NAME>
		  description: <add a meaningful description of the feature's purpose here>
		  root state:
		    name: active
	'''
	
	val oneScenarioWithOneGivenAndMultipleParameterAssignments =
	'''
		Scenario: one
			Given I send event Xy with jk = 42.24
			When  I send event Xy with jk =7
			And   I send event Xy with jk= 7
			And   I send event Xy with jk=7
	'''
	
	val oneScenarioWithMultipleGivenAndMultipleParameterAssignments =  
	'''
		Scenario: one
		  Given I send event Xy with jK = true
		  Given I send event Xy with jK = False
		  Given I send event xY with Jk = 9999
		  Given I send event xY with Jk = "BULLSHIT"
		  And   I send event z  with ab = 12345.54321
	'''

  val oneScenarioResultingToInitState =
  '''
		Scenario: one
		  When  I do nothing
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False
	'''
  ->
  '''
		statechart:
		  name: <REPLACE_THIS_BY_A_MEANINGFUL_NAME>
		  description: <add a meaningful description of the feature's purpose here>
		  root state:
		    name: active
		    parallel states:
		      - name: CONTROLLER
		        initial: //@file.0/@scenarios.0/@which/@steps.1
		        states:
		          - name: //@file.0/@scenarios.0/@which/@steps.1
		            on entry: |
		              send('ApprovalResultInterface',hasWindowApproval=False,hasPanicApproval=False)
	'''
  
  val twoScenariosResultingToInitState =
  '''
		Scenario: one
		  When  I do nothing
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False
		  
		Scenario: two
		  When  I send event SelectingPWF with PWF="PARKEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False
	'''
  ->
  '''
		statechart:
		  name: <REPLACE_THIS_BY_A_MEANINGFUL_NAME>
		  description: <add a meaningful description of the feature's purpose here>
		  preamble: |
		    PWF = "PARKEN"
		  root state:
		    name: active
		    parallel states:
		      - name: CONTROLLER
		        initial: //@file.0/@scenarios.1/@which/@steps.1
		        states:
		          - name: //@file.0/@scenarios.1/@which/@steps.1
		            on entry: |
		              send('ApprovalResultInterface',hasWindowApproval=False,hasPanicApproval=False)
		            transitions:
		            
		      - name: LISTENER
		        initial: listening
		        states:
		          - name: listening
		            transitions:
		              - target: listening
		                event: SelectingPWF
		                action: PWF = event.PWF
	'''
  
  val approvalUnitGrateful =
  '''
		# sismic-bdd ApprovalUnitGenerated.yaml --features ApprovalUnitGrateful.feature
		Feature: ApprovalUnit
		  In order to evaluate if the windows are allowed to move
		  As a dedicated service 'ApprovalUnit'
		  I evaluate 'window approval' for both, normal mode and panic mode
		
		  Scenario: start-up behavior
		    When  I do nothing
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		    And   event ApprovalResultInterface is fired with hasPanicApproval=False
		
		  Scenario: Car is driving and doors are all closed - full approval is granted
		    When  I send event SelectingPWF with PWF="FAHREN"
		    And   I send event ResultKeySearchPosition with Key="KeyNotInCar"
		    And   I send event SelectingDoor with isDriverDoorOpen = False
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
		    And   event ApprovalResultInterface is fired with hasPanicApproval=True
		
		
		  Scenario: Car is driving and doors are NOT all closed - panic approval is NOT granted
		    When  I send event SelectingPWF with PWF="FAHREN"
		    And   I send event ResultKeySearchPosition with Key="KeyNotInCar"
		    And   I send event SelectingDoor with isDriverDoorOpen = True
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
		    And   event ApprovalResultInterface is fired with hasPanicApproval=False
		
		  # recognize that the following scenario does not mention any key location
		  # in this scenario, key location represents a "don't care".
		  Scenario: Car is parking - no approval is granted
		    When  I send event SelectingPWF with PWF="PARKEN"
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		    And   event ApprovalResultInterface is fired with hasPanicApproval=False
		
		  Scenario Outline: Key is not inside the car and car is neither driving nor preparing to drive
		    When  I send event ResultKeySearchPosition with Key="KeyNotInCar"
		    And   I send event SelectingPWF with PWF=<pwf>
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		    And   event ApprovalResultInterface is fired with hasPanicApproval=False
		
		    Examples:
		      | pwf                                 |
		      | "PARKEN_BN_NIO"                     |
		      | "PARKEN_BN_IO"                      |
		      | "STANDFUNKTION_KUNDE_NICHT_IM_FZG"  |
		      | "WOHNEN"                            |
		      | "PRUEFEN_ANALYSE_DIAGNOSE"          |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		      | "SIGNAL_UNBEFUELLT"                 |
		
		  Scenario Outline: Key is inside the car, car is not parking and >=1 door is open
		    When  I send event ResultKeySearchPosition with Key="KeyInCar"
		    And   I send event SelectingPWF with PWF=<pwf>
		    And   I send event SelectingDoor with isDriverDoorOpen = True
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
		    And   event ApprovalResultInterface is fired with hasPanicApproval=False
		
		    Examples:
		      | pwf                                 |
		      | "WOHNEN"                            |
		      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
		      | "FAHREN"                            |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		
		  # What happened (or should happen) if the following table contained "FAHREN"
		  # This would be redundant information in regard to a previous scenario
		  Scenario Outline: Key is inside the car, car is not parking and all doors are closed
		    When  I send event ResultKeySearchPosition with Key="KeyInCar"
		    And   I send event SelectingPWF with PWF=<pwf>
		    And   I send event SelectingDoor with isDriverDoorOpen = False
		    Then  event ApprovalResultInterface is fired with hasWindowApproval=True
		    And   event ApprovalResultInterface is fired with hasPanicApproval=True
		
		    Examples:
		      | pwf                                 |
		      | "WOHNEN"                            |
		      | "FAHRBEREITSCHAFT_HERSTELLEN"       |
		      | "FAHRBEREITSCHAFT_BEENDEN"          |
		      | "FAHREN"                            |
		
	'''
  ->
  '''
		statechart:
		  name: ApprovalUnit
		  description: In order to evaluate if the windows are allowed to move As a dedicated service 'ApprovalUnit' I evaluate 'window approval' for both, normal mode and panic mode  
		  preamble: |
		    isDriverDoorOpen = False
		    PWF = "PARKEN"
		    Key = "KeyNotInCar"
		  root state:
		    name: active
		    parallel states:
		      - name: CONTROLLER
		        initial: //@file.0/@scenarios.4/@which/@steps.1
		        states:
		          - name: //@file.0/@scenarios.6/@which/@steps.1
		            on entry: |
		              send('ApprovalResultInterface',hasWindowApproval=True,hasPanicApproval=True)
		            transitions:
		              - target: //@file.0/@scenarios.5/@which/@steps.1
		                guard: isDriverDoorOpen==True and PWF=="FAHREN" and Key=="KeyNotInCar"
		              - target: //@file.0/@scenarios.4/@which/@steps.1
		                guard: PWF=="PARKEN"
		              - target: //@file.0/@scenarios.4/@which/@steps.1
		                guard: Key=="KeyNotInCar" and (PWF=="PARKEN_BN_NIO" or PWF=="PARKEN_BN_IO" or PWF=="STANDFUNKTION_KUNDE_NICHT_IM_FZG" or PWF=="WOHNEN" or PWF=="PRUEFEN_ANALYSE_DIAGNOSE" or PWF=="FAHRBEREITSCHAFT_BEENDEN" or PWF=="SIGNAL_UNBEFUELLT")
		              - target: //@file.0/@scenarios.5/@which/@steps.1
		                guard: isDriverDoorOpen==True and Key=="KeyInCar" and (PWF=="WOHNEN" or PWF=="FAHRBEREITSCHAFT_HERSTELLEN" or PWF=="FAHREN" or PWF=="FAHRBEREITSCHAFT_BEENDEN")
		            
		          - name: //@file.0/@scenarios.5/@which/@steps.1
		            on entry: |
		              send('ApprovalResultInterface',hasWindowApproval=True,hasPanicApproval=False)
		            transitions:
		              - target: //@file.0/@scenarios.6/@which/@steps.1
		                guard: isDriverDoorOpen==False and PWF=="FAHREN" and Key=="KeyNotInCar"
		              - target: //@file.0/@scenarios.6/@which/@steps.1
		                guard: isDriverDoorOpen==False and Key=="KeyInCar" and (PWF=="WOHNEN" or PWF=="FAHRBEREITSCHAFT_HERSTELLEN" or PWF=="FAHRBEREITSCHAFT_BEENDEN" or PWF=="FAHREN")
		              - target: //@file.0/@scenarios.4/@which/@steps.1
		                guard: PWF=="PARKEN"
		              - target: //@file.0/@scenarios.4/@which/@steps.1
		                guard: Key=="KeyNotInCar" and (PWF=="PARKEN_BN_NIO" or PWF=="PARKEN_BN_IO" or PWF=="STANDFUNKTION_KUNDE_NICHT_IM_FZG" or PWF=="WOHNEN" or PWF=="PRUEFEN_ANALYSE_DIAGNOSE" or PWF=="FAHRBEREITSCHAFT_BEENDEN" or PWF=="SIGNAL_UNBEFUELLT")
		            
		          - name: //@file.0/@scenarios.4/@which/@steps.1
		            on entry: |
		              send('ApprovalResultInterface',hasWindowApproval=False,hasPanicApproval=False)
		            transitions:
		              - target: //@file.0/@scenarios.6/@which/@steps.1
		                guard: isDriverDoorOpen==False and PWF=="FAHREN" and Key=="KeyNotInCar"
		              - target: //@file.0/@scenarios.5/@which/@steps.1
		                guard: isDriverDoorOpen==True and PWF=="FAHREN" and Key=="KeyNotInCar"
		              - target: //@file.0/@scenarios.5/@which/@steps.1
		                guard: isDriverDoorOpen==True and Key=="KeyInCar" and (PWF=="WOHNEN" or PWF=="FAHRBEREITSCHAFT_HERSTELLEN" or PWF=="FAHREN" or PWF=="FAHRBEREITSCHAFT_BEENDEN")
		              - target: //@file.0/@scenarios.6/@which/@steps.1
		                guard: isDriverDoorOpen==False and Key=="KeyInCar" and (PWF=="WOHNEN" or PWF=="FAHRBEREITSCHAFT_HERSTELLEN" or PWF=="FAHRBEREITSCHAFT_BEENDEN" or PWF=="FAHREN")
		            
		      - name: LISTENER
		        initial: listening
		        states:
		          - name: listening
		            transitions:
		              - target: listening
		                event: SelectingDoor
		                action: isDriverDoorOpen = event.isDriverDoorOpen
		              - target: listening
		                event: ResultKeySearchPosition
		                action: Key = event.Key
		              - target: listening
		                event: SelectingPWF
		                action: PWF = event.PWF
	'''
  
  val featureWithOnlyAGivenStep = 
  '''
		Scenario: one
		  Given I send event ResultKeySearchPosition with Key="KeyNotInCar"
		  And   I send event SelectingDoor with isDoorOpen="MAYBE"
	'''
  ->
  '''
	'''
  
  val featureWithOneScenarioGivenWhenThen = 
  '''
		Scenario: one
		  Given I send event ResultKeySearchPosition with Key="KeyNotInCar"
		  And   I send event SelectingDoor with isDoorOpen="MAYBE"
		  When  I send event SelectingPWF with PWF="FLIEGEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  
  val featureWithOnlyAThenStep = 
  '''
		Scenario: one
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithOneScenarioWithoutDoNothing = 
  '''
		Scenario: one
		  When  I send event SelectingPWF with PWF="FLIEGEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithOneScenarioWithDoNothing = 
  '''
		Scenario: one
			Given I do Nothing
			 When  I send event SelectingPWF with PWF="FLIEGEN"
			 Then  event ApprovalResultInterface is fired with hasWindowApproval=False
			 And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithOneScenarioAlsoWithDoNothing = 
  '''
		Scenario: one
		  Given I send event SelectingPWF with PWF="FLIEGEN"
			When  I do Nothing
			 Then  event ApprovalResultInterface is fired with hasWindowApproval=False
			 And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithOneScenarioWithTwoDoNothing = 
  '''
		Scenario: one
			Given I do Nothing
			 When  I do Nothing
			 Then  event ApprovalResultInterface is fired with hasWindowApproval=False
			 And   event KuckucksKasperle        is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithTwoScenariosWithoutDoNothing = 
  '''
		Scenario: one
		  When  I send event SelectingPWF with PWF="FLIEGEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
		  
		Scenario: two
		  When  I send event SelectingPWF with PWF="SCHWIMMEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithGiven = 
  '''
		Scenario: one
		  Given I send event ResultKeySearchPosition with Key="KeyNotInCar"
		  And   I send event SelectingDoor with isDoorOpen="MAYBE"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False
	'''
  ->
  '''
	'''
  
  val featureWithGivenAndSimpleWhen = 
  '''
		Scenario: one
		  Given I send event ResultKeySearchPosition with Key="KeyNotInCar"
		  And   I send event SelectingDoor with isDoorOpen="MAYBE"
		  When  I send event SelectingPWF with PWF="FLIEGEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  When  I send event SelectingPWF with PWF="PARKEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False 
	'''
  ->
  '''
	'''
  
  val featureWithGivenAndComplexWhen = 
  '''
		Scenario: one
		  Given I send event ResultKeySearchPosition with Key="KeyNotInCar"
		  And   I send event SelectingDoor with isDoorOpen="MAYBE"
		  When  I send event SelectingPWF with PWF="FLIEGEN"
		  And   I send event SelectingPWF with PWF="SCHWIMMEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event KuckucksKasperle        is fired with hasPanicApproval=False
		  
		Scenario: Car is parking - no approval is granted
		  When  I send event SelectingPWF with PWF="PARKEN"
		  And   I send event SelectingPWF with PWF="SCHWIMMEN"
		  Then  event ApprovalResultInterface is fired with hasWindowApproval=False
		  And   event ApprovalResultInterface is fired with hasPanicApproval=False 
	'''
  ->
  '''
	'''
  
}
