 package com.bmw.vv.tests

import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.xbase.testing.CompilationTestHelper
import org.eclipse.xtext.testing.XtextRunner
import org.junit.runner.RunWith
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(SismicInjectorProvider)
class SismicGenerationTest implements completeScenarioWithFrancaReferences {
	@Inject extension CompilationTestHelper
     
  @Test 
    def dummy() {
    	Assertions.assertEquals(1,1)
    }
    
  @Test
    def justAFeatureTitle(){
    	justAFeatureTitle.key.assertCompilesTo(justAFeatureTitle.value)
    }
    
  @Test
    def oneScenarioWithOneGiven(){
    	oneScenarioWithOneGiven.key.assertCompilesTo(oneScenarioWithOneGiven.value)
    }
    
  @Test
    def oneScenarioResultingToInitState(){
    	oneScenarioResultingToInitState.key.assertCompilesTo(oneScenarioResultingToInitState.value)
    }
    
  @Test
    def twoScenariosResultingToInitState(){
    	twoScenariosResultingToInitState.key.assertCompilesTo(twoScenariosResultingToInitState.value)
    }
    
  @Test
    def approvalUnitGrateful(){
    	approvalUnitGrateful.key.assertCompilesTo(approvalUnitGrateful.value)
    }
    
  @Test
    def featureWithGiven(){
    	featureWithGiven.key.assertCompilesTo(featureWithGiven.value)
    }
    
  @Test
    def featureWithGivenAndSimpleWhen(){
    	featureWithGivenAndSimpleWhen.key.assertCompilesTo(featureWithGivenAndSimpleWhen.value)
    }
    
  @Test
    def featureWithGivenAndComplexWhen(){
    	featureWithGivenAndComplexWhen.key.assertCompilesTo(featureWithGivenAndComplexWhen.value)
    }
}
