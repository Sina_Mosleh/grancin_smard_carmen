package com.bmw.vv.generator

import org.eclipse.emf.ecore.resource.Resource

class FsmStrategyTrivial extends FsmGeneration implements StatechartGenerator{
  protected new(Resource resource) {
    super(resource)
  }
  
  override isWhichGenStrategy() {
    GenStrategy.TRIVIAL
  }
  
  override generate() {
    val statechart = initStatechart(factory, feature) 
    loadDescription(statechart,feature)
  }
}