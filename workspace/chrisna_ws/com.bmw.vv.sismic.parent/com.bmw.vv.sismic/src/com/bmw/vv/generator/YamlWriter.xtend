package com.bmw.vv.generator

import java.util.ArrayList
import org.eclipse.xtend.lib.annotations.Data
import java.util.Map
import org.eclipse.emf.common.util.EList
import java.util.Iterator
import com.bmw.vv.fsm.SendEvent

class YamlWriter {
	extension YamlBricks 	 yb = new YamlBricks
	extension YamlSerializer ys = new YamlSerializer
	
	val com.bmw.vv.fsm.Statechart sc
	
	new(com.bmw.vv.fsm.Statechart s) {
		sc = s
	}
	
	def writeYaml() {
		val s = new Statechart => [
			defineHeader [
				buildDict("name")[Text(sc.name)]
				buildDict("description")[
				   for (line : sc?.description) {
				   	 Text(line)
				   }
				]
				
				if(sc.preamble !== null && sc.preamble.operations?.length > 0 ) {
					preamble [
						for (parameter : sc.preamble.operations) {
							assign((parameter as com.bmw.vv.fsm.Assignment).leftSide,(parameter as com.bmw.vv.fsm.Assignment).rightSide.toFirstUpper) 
						}
					]
				}

				entitle("root state")[
					buildDict("name")[Text("active")]
					if(sc.states !== null && sc.states.size > 0) {
  					entitle("parallel states")[
  						itemize("name", "CONTROLLER")[
  							buildDict("initial")[Text(sc.initState.name)]	
  							entitle("states")[
  								for (state : sc.states) {
  									itemize("name", state.name)[
  										if(state.on_entry !== null && state.on_entry.operations.size > 0) {
	  										onEntry[
	  											for (e : state.on_entry.operations) {
	  												sendEvent((e as SendEvent).event)[
	  													for (Map.Entry<String, String> p : (e as SendEvent).payload.entrySet()) {
	  														assign(p.key,p.value.toFirstUpper)
	  													}
	  												]
	  											}
	  										]
  										}
  										if(sc.transitions.size > 0) {
    										entitle("transitions")[
    											for (t : sc.transitions) {
    										  		if(t.source == state) {
    										  		  if(t.target !== null) {
      													  itemize("target", t.target.name)[
      													    guard [
      													   	  for (Map.Entry<String, EList<String>> trigger : t.trigger.entrySet()) {
      													   	    if(trigger.value.length > 1)
      													   	    	 Text("(")
      													   	    for (value : trigger.value) {
      														   	 	  equal(trigger.key,"==",value.toFirstUpper)
      														   	 	  if(trigger.value.indexOf(value) < trigger.value.length - 1)
      														   	 	 	 Text(" or ")
      													   	    }
      													   	    if(trigger.value.length > 1)
      													   	   	  Text(")")
      													   	   	 
      													   	    if(!(trigger == t.trigger.entrySet().last))
      													   	   	  Text(" and ")
      													   	  }
      														  ]
      												   	]
    													  }										       
    										   	  }
    											 }
    										]
    										Text("\n")
  										}
  									]
  								}
  							]
  						]
  						if(sc.acceptedEvents !== null && sc.acceptedEvents.size > 0) {
    						itemize("name", "LISTENER")[
    							buildDict("initial")[Text("listening")]
    							entitle("states")[
    							   itemize("name", "listening")[
    							      entitle("transitions")[
    							      	for (Map.Entry<String, EList<String>> event : sc.acceptedEvents.entrySet()) {
    							      		itemize("target", "listening")[
    								      		buildDict("event") [Text(event.key)]
    								      		buildDict("action")[Text(event.value.head + " = event." + event.value.head)]
    							      		]
    							      	}
    							      ]
    							   ]
    							]		   
    						]
  						}
  					]
  				}
				]
			]
		]
	s.toText
	}
}

class YamlSerializer {
	
	def CharSequence toText(Node n) {
		switch n {
			Contents : n.text 
			
			Container:
			'''�n.applyContents�'''
			
			Shipping : 
				'''
				send('�n.event�'�n.listContents�)
			'''
				
			Equality : 
				'''�n.left��n.operand��n.right�'''
				
			Assignment : 
				'''
				�n.left� = �IF n.isEventAssign�event.�ENDIF��n.right�
			'''
				
			Item : 
				'''
				�IF n.bullet�- �ENDIF��n.key�: �n.value�
				  �n.applyContents�
			'''
				
			Dict : 
				'''
				�n.key�: �n.applyContents�
			'''
				
			default:
			'''
				�n.key�:
				  �n.applyContents�
			'''	
		}
	}
	
	def private applyContents(Node n) {
		n.contents?.map[ toText ]?.join
	}
	
	def private listContents(Node n) {
		n.contents?.map[ "," + toText.toString.toCharArray.reverse.drop(2).toList.reverse.join.replaceAll("\\s+","") ]?.join
	}
}


class YamlBricks {
	
	def defineHeader(Statechart it, (Header)=>void init) {
		addAndApply(it, new Header, init)
	}
	
	def Text(Node it, CharSequence contents) {
		val text = new Contents(contents)
		it.contents += text
	}
	
	def entitle(Node it, String key, (Node)=>void init) {
		val n = new Node(key)
		addAndApply(it, n, init)
	}
	
	def buildDict(Node it, String key, (Dict)=>void init) {
		val d = new Dict(key)
		addAndApply(it, d, init)
	}
	
	def preamble(Node it, (Node)=>void init){
		val i = new Item("preamble","|", false)
		addAndApply(it, i, init)
	}
	
	def onEntry(Node it, (Node)=>void init){
		val i = new Item("on entry","|", false)
		addAndApply(it, i, init)
	}
	
	def startAction(Node it){
		buildDict(it, "action", [Text("|")])
	}
	
	def itemize(Node it, String key, String value, (Item)=>void init) {
		val i = new Item(key, value, true)
		addAndApply(it, i, init)
	}
	
	def sendEvent(Node it, String event, (Shipping)=>void init) {
		val s = new Shipping(event)
		addAndApply(it, s, init)
	}
	
	def guard(Node it, (Dict)=>void init) {
		val g = new Dict("guard")
		addAndApply(it, g, init)
	}
	
	def eventAssign(Node it, String left, String right) {
		val a = new Assignment(left, right, true, false)
		addAndApply(it, a)
	}
	
	def assign(Node it, String left, String right) {
		val a = new Assignment(left, right, false, false)
		addAndApply(it, a)
	}
	
	def equal(Node it, String left, String operand, String right) {
		val e = new Equality(left, right, false, operand)
		addAndApply(it, e)
	}
	
	def private <T extends Node> void addAndApply(Node parent, T t) {
		parent.contents += t
	}
	
	def private <T extends Node> void addAndApply(Node parent, T t, (T)=>void init) {
		parent.contents += t
		init.apply(t)
	}
}


@Data class Node {
	String key
	ArrayList<Node> contents = newArrayList
	new(){key = getClass.simpleName.toLowerCase}
	new(String s){key = s}
//	def String getKey() {getClass.simpleName.toLowerCase}
}

@Data class Contents extends Node {
	CharSequence text
}

@Data class ContentNode extends Node {}
@Data class Dict extends Node {
	new(String s) {super(s)}
}
@Data class Shipping extends Dict {
	def event() {key}
}
@Data class Item extends Dict {
	String value
	Boolean bullet
}
@Data class Assignment extends Item {
	Boolean isEventAssign
	def left() {key}
	def right() {value}
	}
@Data class Equality extends Item {
	String operand
	def left() {key}
	def right() {value}
}
@Data class Statechart extends Node {
	new() {super()}	
}
@Data class Header extends Container {}
@Data class Container extends Node {}
