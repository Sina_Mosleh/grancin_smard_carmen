package com.bmw.vv.generator

import com.bmw.vv.fsm.FsmFactory
import com.bmw.vv.fsm.FsmPackage
import com.bmw.vv.fsm.OnEntry
import com.bmw.vv.fsm.SendEvent
import com.bmw.vv.fsm.State
import com.bmw.vv.fsm.impl.OnEntryImpl
import com.bmw.vv.fsm.impl.SendEventImpl
import com.bmw.vv.fsm.impl.StatechartImpl
import com.bmw.vv.gherkin.AbstractScenario
import com.bmw.vv.gherkin.Feature
import com.bmw.vv.gherkin.GivenStep
import com.bmw.vv.gherkin.StepType
import com.bmw.vv.gherkin.WhenStep
import com.bmw.vv.sismic.BoolConstant
import com.bmw.vv.sismic.EventHandler
import com.bmw.vv.sismic.FloatConstant
import com.bmw.vv.sismic.IntConstant
import com.bmw.vv.sismic.NotANumber
import com.bmw.vv.sismic.StepInBody
import com.bmw.vv.sismic.StepOutBody
import com.bmw.vv.sismic.StringConstant
import com.bmw.vv.sismic.Value
import java.util.ArrayList
import java.util.Arrays
import java.util.HashMap
import java.util.Iterator
import java.util.List
import java.util.Map
import java.util.TreeMap
import org.eclipse.emf.common.util.BasicEList
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.nodemodel.util.NodeModelUtils

import static org.eclipse.emf.ecore.util.EcoreUtil.*

import static extension org.eclipse.xtext.EcoreUtil2.*

abstract class FsmGeneration {
	protected val Resource feature
  
  new(Resource f) {
    feature = f
  }
	
	static enum GenStrategy {
		INVALID,
		UNKNOWN,
		TRIVIAL,
		WITH_PRECONDITIONS,
		WITHOUT_PRECONDITIONS
	}
	
	private static def GenStrategy whichGenStrategy(Resource feature) {
    val gs = feature.evaluateGenStrategy
    print("Evaluation of generation strategy .. ")
    println("selected generation strategy is '" + gs.toString + "'")
      
    gs
  }
	
	static def StatechartGenerator createGenerator(Resource feature) {
		val gs = whichGenStrategy(feature)
		
		val StatechartGenerator generator = 
		switch gs {
			case GenStrategy.TRIVIAL: 					      new FsmStrategyTrivial(feature)
			case GenStrategy.WITHOUT_PRECONDITIONS: 	new FsmStrategyWithoutPreconditionsProvider(feature)
			case GenStrategy.WITH_PRECONDITIONS:      new FsmStrategyWithPreconditionsProvider(feature)
			case GenStrategy.UNKNOWN,
			case GenStrategy.INVALID : null
		}
		
		if(generator === null)
		  throw new IllegalStateException("For given kind of text input there is no implementation available.")
		
    generator
	}
	
	private static def evaluateGenStrategy(Resource feature) {
		val i = feature.allContents.toList.filter(StepOutBody)
		if (i.empty) return GenStrategy.TRIVIAL 
		
		val j = feature.allContents.toList.filter(GivenStep)
		if (j.empty) return GenStrategy.WITHOUT_PRECONDITIONS 
		
		GenStrategy.WITH_PRECONDITIONS
	}
	
	protected static def String whichScenario(EObject eo){
    eo.getContainerOfType(AbstractScenario).URIFragment
	}
	
	protected static def String whichStepID(EObject eo){
	  eo.getContainerOfType(StepType).URIFragment
	}
	
	protected static def StepType whichStepType(EObject eo){
	  eo.getContainerOfType(StepType)
	}
	
	protected static def String value2String(Value v) {
		if(v === null) return null
		
		val type = v.type
		switch type {
		    IntConstant : 	type.const.toString
		    FloatConstant : type.const.toString
		    BoolConstant: 	type.const.toString
		    StringConstant: '"' + type.const + '"'
		    NotANumber: 	type.const
		    default : "<unknown type>"
  		}		
	}
	
	  static def com.bmw.vv.fsm.Statechart initStatechart(FsmFactory factory, Resource feature) {
    val statechart = factory.createStatechart
    statechart.name = 
      feature.allContents.toIterable.filter(Feature)
      .take(1).map[ e | if (e.title===null) return "" e.title.name ].join.toString
    
    if(statechart.name.equals("")) statechart.name = "<REPLACE_THIS_BY_A_MEANINGFUL_NAME>"
    
    statechart
  }
  
  static def com.bmw.vv.fsm.Statechart loadDescription(com.bmw.vv.fsm.Statechart sc, Resource feature) {
    sc.description = new BasicEList<String>()
    val f = feature.allContents.toList.filter(Feature).map[ elements ].flatten
    if(f.empty) 
      sc.description.add("<add a meaningful description of the feature's purpose here>")
    else 
      f.forEach[ 
        sc.description
        .add(NodeModelUtils.getTokenText(NodeModelUtils.getNode(it)).replace("\n", " ").replace("\r", ""))
      ]
    sc
  }
  
	protected static def printTransitions(com.bmw.vv.fsm.Statechart sc) {
		println("\nAll Transitions")
		sc.transitions
		.forEach[ println("Origin: " + origin + ", Source: " + source.name + ", Target: " + target.name) ]
	}
	
	static def printStates(com.bmw.vv.fsm.Statechart sc) {
		println("\nAll States")
		sc.states.forEach[ println( "name:\t" + name ) foldedScenarios?.forEach[ println("folded:\t" + it) ] println() ]	
	}
	
	protected static def <T extends EventHandler> SendEvent eventHandler2SendEvent(T eh, FsmFactory factory) {
		val sendEvent 	= factory.createSendEvent
		sendEvent.event = eh.event
		
		// TODO: prevent the exception which is thrown in case parameter and/or value are null !!!!!!!
		// TODO: a definition of multiple values in form of a 'Given table' is currently not supported 
		val m = newHashMap( eh.payload.parameter  -> value2String(eh?.payload.value?.v) )
		(sendEvent as SendEventImpl).eSet(FsmPackage.SEND_EVENT__PAYLOAD,m)
		sendEvent		
	}
	
	private static def List<State> foldStates(TreeMap<String, OnEntryImpl> m, FsmFactory factory) {
		val List<State> foldedStates = new ArrayList<State>();
		val List<OnEntry> foundOEs = new ArrayList<OnEntry>();
		
		while(m.size > 0) {
			val EList<String> foundSCs = new BasicEList<String>();
			foundOEs.clear
			foundSCs.clear
			
			val lastItem = m.pollLastEntry
			val Iterator<Map.Entry<String, OnEntryImpl>> it = m.entrySet().iterator();
			while (it.hasNext()) {
				val current			= it.next()
				val corrspdSC 	= current.key
				val currentOE 	= current.value
				if (equals(currentOE, lastItem.value)) {
					foundOEs.add(currentOE)
					foundSCs.add(corrspdSC)
				}
			}
			
			for(e : foundSCs) {
				if(m.containsKey(e))
					m.remove(e)
			}
			
			val newState = factory.createState 
			newState.name = lastItem.key
			newState.foldedScenarios = foundSCs
			newState.on_entry = lastItem.value
			 
			foldedStates.add(newState)
		}
		foldedStates
	}
	
	protected static def StepOutBody identifyUnboundedThenStep(Resource feature) {
		val allThenSteps = feature.allContents.toIterable.filter(StepOutBody)
		
		return allThenSteps.findFirst[
			val whichScenario = it.whichScenario
			feature.allContents.toIterable.filter(StepInBody).findFirst[ 
				it.whichScenario.equals(whichScenario) && type !== null
			] === null 
		] 
	}
	
	protected static def boolean isFeatureInNeedOfAnInitialGuard(Resource feature) {
		val allGivenAndWhen = feature.allContents.toIterable.filter(StepInBody) 
		
		val Iterator iterator = allGivenAndWhen.iterator(); 
		while(iterator.hasNext) {
			if((iterator.next as StepInBody).type !== null )
				return true
		}
		false
	}
	
	protected static def addDispatchAsDedicatedState(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
    println("#States: " + sc.states.size)
    if(sc.states === null || sc.states.size < 1) {
      return 
    }

		// TODO the question is rather if an 'initial guard' is needed or not
		if(isFeatureInNeedOfAnInitialGuard(feature)) { 
			val dispatch = factory.createState
	    dispatch.name = StatechartGenerator.DISPATCH_STATE_ID
	    sc.states.add(dispatch)	  
    }
	}
	
	protected static def void identifyAllStates(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
	  FsmGeneration.identifyStatesFromGivenSteps(factory,feature,sc)
		FsmGeneration.identifyStatesFromThenSteps(factory,feature,sc)
    FsmGeneration.addDispatchAsDedicatedState(factory, feature, sc)
	}
	
	protected static def identifyStatesFromGivenSteps(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
		identifyStatesFromStep(StepInBody, factory, feature, sc)
	}
	
	protected static def void identifyStatesFromThenSteps(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
		identifyStatesFromStep(StepOutBody, factory, feature, sc)
	}
	
	private static def identifyStatesFromStep(Class<? extends EObject> SelectedStepType, FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
		val stateMap = new TreeMap<String, OnEntryImpl>()
		val toBeAdded= new ArrayList<Pair<String,SendEvent>>()

		for (sst : feature.allContents.toIterable.filter(SelectedStepType)
			.filter[it.getContainerOfType(WhenStep) === null]) {	
		
		  val SendEvent sendEvent = switch SelectedStepType {
		    case StepOutBody:        (sst as StepOutBody).type.eventHandler2SendEvent(factory)
		    case StepInBody:  switch (sst as StepInBody ).type {
		    	com.bmw.vv.sismic.SendEvent:  ((sst as StepInBody ).type as com.bmw.vv.sismic.SendEvent).eventHandler2SendEvent(factory)
		    	case ((sst as StepInBody ).type === null && ((sst as StepInBody ).stub !== null)),
		    	default: null
		    	}
			}
			if(sendEvent !== null) {
				val stepID 	= sst.whichStepID
			
				if(stateMap.containsKey(stepID)) {
					stateMap.get(stepID).operations.forEach[
						switch it {
							SendEvent case it.event.equals(sendEvent.event) : 
								(it as SendEvent).payload.putAll(sendEvent.payload)
								
							SendEvent case !it.event.equals(sendEvent.event): 
								toBeAdded.add(stepID -> sendEvent)					
						}
					]
					toBeAdded.forEach[stateMap.get(it.key).operations.add(it.value)]
				} else {
					val oe = factory.createOnEntry as OnEntryImpl
					oe.eSet(FsmPackage.ON_ENTRY__OPERATIONS, Arrays.asList(sendEvent))
					stateMap.put(stepID, oe)
				}
			}
		}
		if(stateMap.empty == false) {
			val List<State> l = foldStates(stateMap, factory)
			(sc as StatechartImpl).states.addAll(l)
		}
	}
	
	protected static def State defineInitState(Resource feature, com.bmw.vv.fsm.Statechart sc) {
		switch sc {
		  case sc.initState !== null: return sc.initState
		  case sc.states    === null: return null
		  case sc.states.size < 1   : return null
     	case sc.states.findFirst[name.equals(StatechartGenerator.DISPATCH_STATE_ID)] !== null: {
					 sc.initState = sc.states.findFirst[name.equals(StatechartGenerator.DISPATCH_STATE_ID)]
					 return sc.initState } 
      default: {
				// TODO there is an undefined situation here if doNothingSteps contain more than one element
				val initStep 		     	 = identifyUnboundedThenStep(feature)
				if(initStep !== null) {
					val initStepScenario = initStep.whichScenario
					val stateOfInitStep  = 
					sc.states.findFirst[ 
						name.contains(initStepScenario) || 
						(foldedScenarios.findFirst[it.contains(initStepScenario)] !== null )
					] 
					sc.initState 		     = stateOfInitStep 
					return stateOfInitStep
				}
			}
		}
	}
	
	protected static def definePreamble(FsmFactory factory, Resource feature, com.bmw.vv.fsm.Statechart sc) {
		val initStep = defineInitState(feature,sc)
		if(initStep === null) 
			return
		  
		val m = new HashMap<String,EList<String>>()
		
		val allParameters  = sc.transitions.map[ trigger.filter[k, v | v.length == 1] ]
		allParameters.forEach[ m.putAll(it) ]
		
		val initParameters = sc.transitions.filter[ target == initStep ].map[ trigger.filter[k, v | v.length == 1] ]
		initParameters.forEach[ m.putAll(it) ]
		
		val p = factory.createPreamble 
		sc.preamble = p
		
		m.forEach[ k, v| 
			val assignment = factory.createAssignment
			assignment.leftSide  = k
			assignment.operand   = "="
			assignment.rightSide = v.head
			p.operations.add(assignment)
		]
	}
}