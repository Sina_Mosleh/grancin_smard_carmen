package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.VariableSet#getUuid <em>Uuid</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.VariableSet#getVariables <em>Variables</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariableSet()
 * @model
 * @generated
 */
public interface VariableSet extends EObject {
    /**
     * Returns the value of the '<em><b>Uuid</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Uuid</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Uuid</em>' attribute.
     * @see #setUuid(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariableSet_Uuid()
     * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.ID"
     *        extendedMetaData="kind='attribute' name='uuid' namespace='##targetNamespace'"
     * @generated
     */
    String getUuid();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.VariableSet#getUuid <em>Uuid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Uuid</em>' attribute.
     * @see #getUuid()
     * @generated
     */
    void setUuid(String value);

    /**
     * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractVariable}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variables</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariableSet_Variables()
     * @model containment="true"
     * @generated
     */
    EList<AbstractVariable> getVariables();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidValueRanges(DiagnosticChain diagnostics, Map<Object, Object> context);

} // VariableSet
