package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.common.interfaces.bus.MessageType;
import de.bmw.smard.modeller.conditions.DataType;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HeaderSignalStaticAttributes {

    private static final String FLEXRAY_CYCLE = "flexRayCycle";

    private static final String MESSAGE_LENGTH = "messageLength";
    private static final String MESSAGE_SLOW_BITS = "messageSlowBits";
    private static final String MESSAGE_FAST_BITS = "messageFastBits";

    private static final String TIMESTAMP = "Timestamp";

    private static final String BUS_ID = "BusId";

    private static Map<String, DataType> staticAttributesGeneric = new HashMap<String, DataType>() {
        {
            put(BUS_ID, DataType.DOUBLE);
            put(TIMESTAMP, DataType.DOUBLE);
        }
    };

    private static Map<String, DataType> staticAttributesCAN = new HashMap<String, DataType>() {
        {
            put(MESSAGE_LENGTH, DataType.DOUBLE);
            put(MESSAGE_SLOW_BITS, DataType.DOUBLE);
            put(MESSAGE_FAST_BITS, DataType.DOUBLE);
        }
    };

    private static Map<String, DataType> staticAttributesFlexRay = new HashMap<String, DataType>() {
        {
            put(FLEXRAY_CYCLE, DataType.DOUBLE);
        }
    };

    private static Map<String, DataType> staticAttributes = new HashMap<String, DataType>() {
        {
            putAll(staticAttributesGeneric);
            putAll(staticAttributesCAN);
            putAll(staticAttributesFlexRay);
        }
    };

    public static Map<String, DataType> getStaticAttributes(MessageType messageType) {
        switch (messageType) {
            case UNKNOWN_BUS_NAME:
                return staticAttributesGeneric;
            case CAN:
                return staticAttributesCAN;
            case FLEXRAY:
                return staticAttributesFlexRay;
            default:
                return null;
        }
    }

    public static Set<String> getKeySet() {
        return staticAttributes.keySet();
    }

    public static DataType get(String attributeName) {
        return staticAttributes.get(attributeName);
    }

    public static String getBusId() {
        return BUS_ID;
    }

    public static String getTimestamp() {
        return TIMESTAMP;
    }

    public static String getMessageLength() {
        return MESSAGE_LENGTH;
    }


    public static String getMessageSlowBits() {
        return MESSAGE_SLOW_BITS;
    }

    public static String getMessageFastBits() {
        return MESSAGE_FAST_BITS;
    }

    public static String getFlexRayCycle() {
        return FLEXRAY_CYCLE;
    }

}
