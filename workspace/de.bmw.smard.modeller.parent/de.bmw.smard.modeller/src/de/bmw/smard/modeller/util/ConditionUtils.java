package de.bmw.smard.modeller.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;

import de.bmw.smard.modeller.conditions.BitPatternComparatorValue;
import de.bmw.smard.modeller.conditions.Comparator;
import de.bmw.smard.modeller.conditions.ConditionsDocument;
import de.bmw.smard.modeller.conditions.ConditionsFactory;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DocumentRoot;
import de.bmw.smard.modeller.conditions.SignalComparisonExpression;

/**
 * helper class for conditions
 */
public final class ConditionUtils
{

	/** no instance */
	private ConditionUtils() {}
	
	/**
	 * creates a new conditions document with the propert initial data setup.
	 * 
	 * @return the new document root
	 */
	public static DocumentRoot createConditionsDocument()
	{
		ConditionsFactory conditionsFactory = ConditionsFactory.eINSTANCE;
		DocumentRoot documentRoot = conditionsFactory .createDocumentRoot();
		ConditionsDocument conditionsDocument = conditionsFactory.createConditionsDocument();
		conditionsDocument.setConditionSet( conditionsFactory.createConditionSet());
		conditionsDocument.setSignalReferencesSet( conditionsFactory.createSignalReferenceSet());
		conditionsDocument.setVariableSet( conditionsFactory.createVariableSet());
		documentRoot.setConditionsDocument(conditionsDocument);
		return documentRoot;
	}
	
	/**
	 * creates a new conditions document with the initial data setup.
	 * 
	 * @return the new document root
	 */
	public static DocumentRoot createConditionsDocument4ObserverTemplates()
	{
		ConditionsFactory conditionsFactory = ConditionsFactory.eINSTANCE;
		de.bmw.smard.modeller.conditions.DocumentRoot documentRoot = conditionsFactory .createDocumentRoot();
		ConditionsDocument conditionsDocument = conditionsFactory.createConditionsDocument();
		conditionsDocument.setVariableSet( conditionsFactory.createVariableSet());
		documentRoot.setConditionsDocument(conditionsDocument );
		return documentRoot;
	}
	
	/**
	 * Checks if the given command parameter to create a child depends on data situation.
	 * This is used by the ConditionsActionBarContributor to check if a create child or create sibling action should 
	 * be aware of the data context and call {@link #createObjectActionIsPossibleWithParent(CommandParameter, EObject)}
	 * to determine if the action should be displayed in the menu or not.
	 * <P>
	 * Currently, this is used for the following:
	 * <UL>
	 * <LI>the {@link BitPatternComparatorValue} may only be created if the {@link SignalComparisonExpression}
	 * has the operator equals or not equals (or it is template defined).
	 * </UL>
	 *    
	 * @param commandParameter the command parameter for creating the new child
	 * @return true if creating the new child requires data consideration
	 */
	public static boolean createObjectActionRequiresDataContext(CommandParameter commandParameter) 
	{
		if (commandParameter.getFeature()==ConditionsPackage.Literals.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL
				&& commandParameter.getValue() instanceof BitPatternComparatorValue)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the create child action can be executed for the given parent.
	 * See {@link #createObjectActionRequiresDataContext(CommandParameter)} for details. 
	 * @param commandParameter the command parameter for creating the new child
	 * @param parent the parent object where the child should be created
	 * @return true if the creation is possible and the menu entry should be visible
	 */
	public static boolean createObjectActionIsPossibleWithParent(CommandParameter commandParameter, EObject parent)
	{
		if (commandParameter.getFeature()==ConditionsPackage.Literals.SIGNAL_COMPARISON_EXPRESSION__COMPARATOR_SIGNAL
				&& commandParameter.getValue() instanceof BitPatternComparatorValue)
		{
			if (!(parent instanceof SignalComparisonExpression))
			{
				return false;
			}
			Comparator operator = ((SignalComparisonExpression)parent).getOperator();
			if (operator==Comparator.EQ || operator==Comparator.NE || operator==Comparator.TEMPLATE_DEFINED)
			{
				return true;
			}
			return false;
		}
		throw new IllegalStateException("Unexpected command parameter: "+commandParameter);
	}
	
	
}
