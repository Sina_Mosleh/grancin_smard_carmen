package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.AbstractVariable;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.Variable;
import de.bmw.smard.modeller.conditions.VariableSet;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VariableSetImpl#getUuid <em>Uuid</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VariableSetImpl#getVariables <em>Variables</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableSetImpl extends EObjectImpl implements VariableSet {
    /**
     * The default value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getUuid()
     * @generated
     * @ordered
     */
    protected static final String UUID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getUuid() <em>Uuid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getUuid()
     * @generated
     * @ordered
     */
    protected String uuid = UUID_EDEFAULT;

    /**
     * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVariables()
     * @generated
     * @ordered
     */
    protected EList<AbstractVariable> variables;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected VariableSetImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VARIABLE_SET;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getUuid() {
        return uuid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUuid(String newUuid) {
        String oldUuid = uuid;
        uuid = newUuid;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VARIABLE_SET__UUID, oldUuid, uuid));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractVariable> getVariables() {
        if (variables == null) {
            variables = new EObjectContainmentEList<AbstractVariable>(AbstractVariable.class, this, ConditionsPackage.VARIABLE_SET__VARIABLES);
        }
        return variables;
    }

    /**
     * <!-- begin-user-doc -->
     * iterates over all Observers in a Variable Set and summarizes all Value Range Gap Warnings
     * thus there are less warnings in general
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidValueRanges(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValid_hasValidValueRanges = true;
        String errMsgID = "_Validation_Conditions_SignalObserver_ObserverValueRanges";

        if (variables == null || variables.isEmpty()) {
            return isValid_hasValidValueRanges;
        }

        List<Diagnostic> childrenOfVariableSet = new ArrayList<Diagnostic>();
        int countOfProblemsInVariableSet = 0;

        for (AbstractVariable abstractVariable : variables) {
            if (abstractVariable instanceof Variable) {
                Variable variable = (Variable) abstractVariable;
                if (variable.getObservers() != null && !variable.getObservers().isEmpty()) {
                    EList<AbstractObserver> observers = variable.getObservers();
                    List<Diagnostic> childrenOfObserver = new ArrayList<Diagnostic>();

                    for (AbstractObserver observer : observers) {
                        EList<String> errorMessages = new BasicEList<String>();
                        EList<String> warningMessages = new BasicEList<String>();

                        boolean result = observer.isValid_hasValidValueRanges(errorMessages, warningMessages);
                        isValid_hasValidValueRanges &= result;

                        if (!result) {
                            // handle error messages
                            if (!errorMessages.isEmpty()) {
                                for (String errorMessage : errorMessages) {
                                    // for every error message create a Basic Diagnostic
                                    // and add it to the children of Observer
                                    Diagnostic diagnostic = DiagnosticBuilder.error()
                                            .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                                            .code(ConditionsValidator.VARIABLE_SET__IS_VALID_HAS_VALID_VALUE_RANGES)
                                            .messageId(PluginActivator.getInstance(), errMsgID, errorMessage)
                                            .data(this)
                                            .build();

                                    childrenOfObserver.add(diagnostic);
                                    countOfProblemsInVariableSet++;
                                }
                            }

                            // handle warning messages
                            if (!warningMessages.isEmpty()) {
                                for (String warningMessage : warningMessages) {
                                    Diagnostic diagnostic = DiagnosticBuilder.warn()
                                            .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                                            .code(ConditionsValidator.VARIABLE_SET__IS_VALID_HAS_VALID_VALUE_RANGES)
                                            .messageId(PluginActivator.getInstance(), errMsgID, warningMessage)
                                            .data(this)
                                            .build();

                                    childrenOfObserver.add(diagnostic);
                                    countOfProblemsInVariableSet++;
                                }
                            }

                            Diagnostic diagnosticOfObserver =
                                    new BasicDiagnostic(ConditionsValidator.DIAGNOSTIC_SOURCE, 0, childrenOfObserver, "There are multiple problems with value ranges of "
                                            + observer.getName(), new Object[] {
                                                    this
                                    });
                            childrenOfVariableSet.add(diagnosticOfObserver);
                        }
                    }
                }
            }
        }

        if (!isValid_hasValidValueRanges) {
            if (diagnostics != null) {
                diagnostics.add(
                        new BasicDiagnostic(ConditionsValidator.DIAGNOSTIC_SOURCE, 0, childrenOfVariableSet, "There are multiple ("
                                + countOfProblemsInVariableSet + ") problems " +
                                "concerning Observer value ranges", new Object[] {
                                        this
                        }));
            }
            return false;
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_SET__VARIABLES:
                return ((InternalEList<?>) getVariables()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_SET__UUID:
                return getUuid();
            case ConditionsPackage.VARIABLE_SET__VARIABLES:
                return getVariables();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_SET__UUID:
                setUuid((String) newValue);
                return;
            case ConditionsPackage.VARIABLE_SET__VARIABLES:
                getVariables().clear();
                getVariables().addAll((Collection<? extends AbstractVariable>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_SET__UUID:
                setUuid(UUID_EDEFAULT);
                return;
            case ConditionsPackage.VARIABLE_SET__VARIABLES:
                getVariables().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VARIABLE_SET__UUID:
                return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
            case ConditionsPackage.VARIABLE_SET__VARIABLES:
                return variables != null && !variables.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (uuid: ");
        result.append(uuid);
        result.append(')');
        return result.toString();
    }

} //VariableSetImpl
