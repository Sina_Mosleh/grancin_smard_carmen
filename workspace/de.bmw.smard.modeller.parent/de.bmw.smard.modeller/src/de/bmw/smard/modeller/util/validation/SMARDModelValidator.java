package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.base.exception.SmardRuntimeException;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.PluginActionExpressionParser;
import de.bmw.smard.common.data.SendMessageActionExpressionParser;
import de.bmw.smard.common.data.ShowVariableActionExpressionParser;
import de.bmw.smard.common.data.Variables;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.statemachine.*;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import de.bmw.smard.modeller.util.ActionExpressionUtils;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.Diagnostician;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class SMARDModelValidator {

    /**
     * id of the validation marker, must match the value in the plugin.xml
     */
    public static final String ID_VALIDATION_MARKER = PluginActivator.PLUGIN_ID + ".validation";
    private static final String DIAGNOSTIC_SOURCE = "de.bmw.smard.modeller.globalvalidation";
    private static final int GLOBAL_VALIDATION_CODE = Integer.MAX_VALUE;

    private static final Pattern PATTERN_VARIABLE_VALUE = Pattern.compile("\\$<[^\\$\\%<>]*>\\$");

    public enum ValidationContext {
        EXPORT_VALIDATION_CONTEXT(1),
        TEMPLATE_VALIDATION_CONTEXT(2);

        private int _value;

        ValidationContext(int value) {
            _value = value;
        }

        public int value() {
            return _value;
        }
    }

    private Map<String, AbstractVariable> variableName2Variable;
    private Map<ValueVariable, StateMachine> valueVariable2WriterStatemachine;
    private Set<ConditionsDocument> additionalConditionDocs = new HashSet<>();
    private List<StateMachine> allStatemachines = new ArrayList<>();
    private List<IModelValidator> validators = new ArrayList<>();

    public boolean isValid(StateMachineSet stateMachineSet, ValidationContext validCtxt) {
        DiagnosticChain diagChain = Diagnostician.INSTANCE.createDefaultDiagnostic(stateMachineSet);
        Map<Object, Object> context = Diagnostician.INSTANCE.createDefaultContext();
        if (validCtxt != null)
            context.put(ValidationContext.class, validCtxt);

        return isValid(stateMachineSet, diagChain, context);
    }

    public boolean isValid(StateMachineSet stateMachineSet, DiagnosticChain diagChain, Severity severity) {
        return isValid(stateMachineSet, diagChain, new HashMap<>(), severity);
    }

    public boolean isValid(StateMachineSet stateMachineSet, DiagnosticChain diagChain, Map<Object, Object> context) {
        return isValid(stateMachineSet, diagChain, context, Severity.WARNING);
    }

    private boolean isValid(StateMachineSet stateMachineSet, DiagnosticChain diagChain, Map<Object, Object> context,
                           Severity maxLevel2ConsiderValid) {
        boolean isValid = stateMachineSet != null;
        if (isValid) {
            if (context == null) {
                context = Diagnostician.INSTANCE.createDefaultContext();
            }

            if (diagChain == null)
                diagChain = Diagnostician.INSTANCE.createDefaultDiagnostic(stateMachineSet);

            // first validate call should be with stateMachineSet as it adds
            // itself as circular containment root into context
            isValid = Diagnostician.INSTANCE.validate(stateMachineSet, diagChain, context);

            // if StateMachineSet is valid - validate further
            if(isValid) {
	            Set<EObject> relevantObjects =
	            		ReferencedResourcesCollector.getRelevantObjectsForStatemachineSet(stateMachineSet, additionalConditionDocs);

	            // validate all relevant object individually
	            for (EObject relevantObj : relevantObjects) {
	                Diagnostician.INSTANCE.validate(relevantObj, diagChain, context);
	            }


	        	isValid_hasValidResourceName(relevantObjects, diagChain, context);

	            isValid_uniqueVariableNames_andRegisterThemInMap(relevantObjects, diagChain, context);

	            // SMARD-2577 Observer names must be unique for the whole project
	            isValid_uniqueObserverNames(relevantObjects, diagChain, context);

	            isValid_checkAllActionExpressions(relevantObjects, diagChain, context);

	            // check the StatemachineSet for cyclic dependencies and missing statemachines
	            // from statemachineset
	            isValid_checkForCyclicDependencies(stateMachineSet, diagChain, context);

	            isValid_checkForMissingWriterStatemachines(stateMachineSet, diagChain, context);
            }
        }

        BasicDiagnostic baseDiag = (BasicDiagnostic) diagChain;

        return SeverityConverter.convert(baseDiag.getSeverity()).value() <= maxLevel2ConsiderValid.value();
    }

    private boolean isInvalid(DiagnosticChain diagChain) {
    	if ((diagChain instanceof Diagnostic)==false)
			return false;
    	return ((Diagnostic)diagChain).getSeverity() > Diagnostic.WARNING;
    }

    public void add(ConditionsDocument cd) {
        additionalConditionDocs.add(cd);
    }

    public void add(StateMachine stateMachine) {
        allStatemachines.add(stateMachine);
    }

    public void putValidator(IModelValidator validator) {
        if (validator == null)
            return;
        validators.add(validator);
    }

    /**
     * put a message into the DiagnosticChain
     * @deprecated use DiagnosticBuilder and DiagnosticChain instead
     */
    @Deprecated
	private void message(Severity severityLevel, EObject eObject, DiagnosticChain diagnostics,
                        Map<Object, Object> context, String errMessageID, List<String> subMessages) {
        if (severityLevel == null || eObject == null) {
            throw new IllegalArgumentException("SMARDModelValidator::message(): param eObject or severityLevel null");
        }

        Object[] subMsgArr = subMessages != null && !subMessages.isEmpty() ? subMessages.toArray() : null;

        if (diagnostics != null) {

            diagnostics.add(DiagnosticBuilder.builder(severityLevel)
                    .source(DIAGNOSTIC_SOURCE)
                    .code(GLOBAL_VALIDATION_CODE)
                    .messageId(PluginActivator.INSTANCE, errMessageID, subMsgArr)
                    .data(eObject)
                    .build());
        }
    }

    private void isValid_checkForCyclicDependencies(StateMachineSet stateMachineSet, DiagnosticChain diagChain,
                                                       Map<Object, Object> context) {
    	if (isInvalid(diagChain))
    		return;

        // check for cyclic dependencies using the StateMachineSorter
        StateMachineSorter sorter = new StateMachineSorter(false);
        List<StateMachine> stateMachinesInSet = stateMachineSet.getStateMachines();
        try {
            sorter.sort(stateMachinesInSet);
        } catch (CyclicDependencyException e) {
            message(Severity.ERROR, stateMachineSet, diagChain, context, "_Validation_CyclicDependencies",
                    Collections.singletonList(e.getMessage() + " " + e.getCauseForCyclicDependency()));
        } catch (SmardRuntimeException e) {
            message(Severity.ERROR, stateMachineSet, diagChain, context, "_Validation_FileName_Invalid",
                    Collections.singletonList(e.getMessage()));
        } catch (IllegalStateException e) {
            message(Severity.ERROR, stateMachineSet, diagChain, context, "_Validation_UnresolvedProxy",
                    Collections.singletonList(e.getMessage()));
        }

        if(!sorter.getMissingStateMachines().isEmpty()) {
            message(Severity.ERROR, stateMachineSet, diagChain, context,
                    "_Validation_Condition_Dependencies_StateMachineNotFound",
                    Collections.singletonList(getCommaSeparatedListOfSMs(sorter.getMissingStateMachines())));
        }
    }

    private void isValid_checkForMissingWriterStatemachines(StateMachineSet stateMachineSet, DiagnosticChain diagChain,
            Map<Object, Object> context) {
        List<StateMachine> stateMachinesInSet = stateMachineSet.getStateMachines();

        if (isInvalid(diagChain))
            return;

        VariableUsageChecker variableUsageSupervisor = new VariableUsageChecker();
        Map<String, Set<StateMachine>> missingStateMachinesForVariable =
                variableUsageSupervisor.checkMissingStateMachines(allStatemachines, stateMachinesInSet);
        for(Map.Entry<String, Set<StateMachine>> entry : missingStateMachinesForVariable.entrySet()) {
            message(Severity.ERROR, stateMachineSet, diagChain, context,
                    "_Validation_global_StatemachineSet_MissingWriter",
                    Arrays.asList(entry.getKey(), getCommaSeparatedListOfSMs(entry.getValue())));
        }
    }

    private String getCommaSeparatedListOfSMs(Collection<StateMachine> stateMachines) {
        if(stateMachines != null) {
            return stateMachines.stream()
                    .map(StateMachine::getName)
                    .collect(Collectors.joining(", "));
        }
        return "";
    }

    private void isValid_hasValidResourceName(Set<EObject> relevantObjects, DiagnosticChain diagChain,
            Map<Object, Object> context) {
        if (isInvalid(diagChain))
            return;

        for (EObject obj : relevantObjects) {
            Resource res = obj.eResource();
            if (res == null) {
                message(Severity.ERROR, obj, diagChain, context, "_Validation_FileName_Invalid", null);
            } else {
                String[] path = res.getURI().segments();

                for (int i = 0; i < path.length; i++) {
                    String name = path[i];
                    if (!StringUtils.isValidFileName(name)) {
                        message(Severity.ERROR, obj, diagChain, context, "_Validation_FileName_Invalid", Collections.singletonList(name));
                    }
                }
            }
        }

    }

    // variable names must be unique and may not collide with predefined names
    private void isValid_uniqueVariableNames_andRegisterThemInMap(Set<EObject> relevantObjects, DiagnosticChain diagChain,
            Map<Object, Object> context) {

        if (isInvalid(diagChain))
            return;

        variableName2Variable = new HashMap<>(); // reset map

        if (relevantObjects == null)
            return;

        boolean isUniqueVariableName = true;
        for (EObject eObject : relevantObjects) {
            if (eObject instanceof ConditionsDocument && eObject.eResource() != null) {
                for (Iterator<EObject> it = eObject.eResource().getAllContents(); it.hasNext(); ) {
                    EObject current = it.next();
                    if (current instanceof AbstractVariable) {
                        AbstractVariable varIN = ((AbstractVariable) current);

                        AbstractVariable varOUT = variableName2Variable.put(varIN.getName(), varIN);
                        isUniqueVariableName = (varOUT == null || varOUT == varIN);
                        if (!isUniqueVariableName) {
                            message(Severity.ERROR, current, diagChain, context,
                                    "_Validation_global_Variable_Name_IsNotUnique", Collections.singletonList(varIN.getName()));
                        }
                    }
                }
            }
        }
    }

    private void isValid_uniqueObserverNames(Set<EObject> relevantObjects, DiagnosticChain diagChain, Map<Object, Object> context) {

        if (isInvalid(diagChain))
            return;

        List<String> observerNames = new ArrayList<>();
        for (EObject eObject : relevantObjects) {
            if (eObject instanceof ConditionsDocument && eObject.eResource() != null) {
                for (Iterator<EObject> it = eObject.eResource().getAllContents(); it.hasNext(); ) {
                    EObject current = it.next();
                    if (current instanceof AbstractObserver) {
                        // check the names of all Observers for explicitness
                        String observerName = ((AbstractObserver) current).getName();
                        if (!observerNames.contains(observerName)) {
                            observerNames.add(observerName);
                        } else {
                            message(Severity.ERROR, current, diagChain, context,
                                    "_Validation_global_AbstractObserver_Name_IsNotUnique", Collections.singletonList(observerName));
                        }
                    }
                }
            }
        }
    }

    /*
     * global because actionExpressions refer to variables by name thus it can only be
     * validated by a component that knows about all relevant variables
     *
     * only known Variables may occur in an ActionExpressionString String Variables
     * must be referenced only by OP<n>-child-mechanism
     */
    private void isValid_checkAllActionExpressions(Set<EObject> relevantObjects, DiagnosticChain diagChain,
            Map<Object, Object> context) {

        if (isInvalid(diagChain))
            return;

        if (variableName2Variable == null)
            throw new IllegalStateException("isValid_checkAllActionExpressions: variableName2Variable map is null");

        // reset map
        valueVariable2WriterStatemachine = new HashMap<>();

        if (relevantObjects == null)
            return;

        Set<EObject> objectsWithActionExpressions = new HashSet<>();

        // extract all objects that have an action expression
        for (EObject eObject : relevantObjects) {
            if ((eObject instanceof ConditionsDocument || eObject instanceof StateMachine)
                && eObject.eResource() != null) {
                for (Iterator<EObject> it = eObject.eResource().getAllContents(); it.hasNext(); ) {
                    EObject current = it.next();
                    if (current instanceof Action || current instanceof ComputeVariable
                        || current instanceof CalculationExpression || current instanceof ComputedVariable) {
                        objectsWithActionExpressions.add(current);
                    }


                    validateWithInjectedValidators(current, diagChain, context);

                }
            }
        }

        for (EObject obj : objectsWithActionExpressions) {

            if (obj instanceof ComputeVariable || obj instanceof CalculationExpression || obj instanceof ComputedVariable) {
                isValid_computeVariable_ActionExpression(obj, diagChain, context);
            } else if (obj instanceof Action) {
                Action action = (Action) obj;


                if (action.getActionType() == ActionTypeNotEmpty.COMPUTE) {
                    isValid_computeVariable_ActionExpression(obj, diagChain, context);
                } else if (action.getActionType() == ActionTypeNotEmpty.CAN
                    || action.getActionType() == ActionTypeNotEmpty.LIN
                    || action.getActionType() == ActionTypeNotEmpty.FLEXRAY) {
                        isValid_sendOnBus_ActionExpression((Action) obj, diagChain, context);
                    } else if (action.getActionType() == ActionTypeNotEmpty.SHOWVARIABLE) {
                        isValid_showVariable_ActionExpression((Action) obj, diagChain, context);
                    }

                if (action.getActionType() == ActionTypeNotEmpty.PLUGIN) {
                    isValid_plugin_ActionExpression((Action) obj, diagChain, context);
                }
            } else
                throw new IllegalArgumentException("SMARDModelValidator::isValid_sendOnBus_ActionExpression. " +
                        "Parameter \"obj\" is "
                        + obj != null ? "of unhandled type: " + obj.getClass().getSimpleName() : "null");
        }
    }

    private void validateWithInjectedValidators(EObject obj, DiagnosticChain diagChain, Map<Object, Object> context) {

        if (obj == null || validators == null || diagChain == null || context == null)
            return;

        for (IModelValidator iModelValidator : validators) {
            boolean contextFits = true;
            if (context.containsKey(ValidationContext.class)) {
                contextFits = iModelValidator.isApplicableIn((ValidationContext) context.get(ValidationContext.class));
            }
            if (contextFits && iModelValidator.accept4Validation(obj)) {
                iModelValidator.validate(obj, diagChain, context);
            }
        }
    }

    private void isValid_sendOnBus_ActionExpression(Action sendOnBusAction, DiagnosticChain diagChain,
            Map<Object, Object> context) {

        if (variableName2Variable == null)
            throw new IllegalStateException("isValid_sendOnBus_ActionExpression: variableName2Variable map is null");


        ActionTypeNotEmpty actionType = sendOnBusAction.getActionType();

        if (!(actionType == ActionTypeNotEmpty.CAN || actionType == ActionTypeNotEmpty.LIN
            || actionType == ActionTypeNotEmpty.FLEXRAY)) {
            throw new IllegalArgumentException("isValid_sendOnBus_ActionExpression: wrong action type " + actionType);
        }

        String errMsgID = "_Validation_global_Action_ActionExpression_SendOnBus";

        String payloadVariable = SendMessageActionExpressionParser.parseParameters(actionType.getLiteral(),
                sendOnBusAction.getActionExpression());
        if (!StringUtils.nullOrEmpty(payloadVariable)) {
            if (!checkVariableName_isPredefOrKnownVariable(payloadVariable)) {
                message(Severity.ERROR, sendOnBusAction, diagChain, context, errMsgID, Collections.singletonList(payloadVariable));
            }
        }
    }

    private void isValid_plugin_ActionExpression(Action pluginAction, DiagnosticChain diagChain, Map<Object, Object> context) {

        if (variableName2Variable == null)
            throw new IllegalStateException("SMARDModelValidator::isValid_plugin_ActionExpression: variableName2Variable map is null");


        if (!(pluginAction.getActionType() == ActionTypeNotEmpty.PLUGIN)) {
            throw new IllegalStateException("SMARDModelValidator::isValid_plugin_ActionExpression: wrong action type " + pluginAction.getActionType());
        }

        Vector<String> pluginVariableNames = new Vector<>();
        Vector<String> engineVariableNames = new Vector<>();
        Vector<String> engineVariableNamesOrDoubles = new Vector<>();
        Map<String, Object> parseResult = PluginActionExpressionParser.parsePluginParameters(
                pluginAction.getActionExpression(), pluginVariableNames, engineVariableNames,
                engineVariableNamesOrDoubles);

        if (parseResult.containsKey(PluginActionExpressionParser.KEY_ERROR)) {
            String errMsgID = "_Validation_global_Action_ActionExpression_Plugin";
            message(Severity.ERROR, pluginAction, diagChain, context, errMsgID,
                    Collections.singletonList((String) parseResult.get(PluginActionExpressionParser.KEY_ERROR)));
        } else {
            String errMsgID = "_Validation_global_Action_ActionExpression_UnknownVariable";
            for (String engineVariable : engineVariableNames) {
                if (!checkVariableName_isPredefOrKnownVariable(engineVariable)) {
                    message(Severity.ERROR, pluginAction, diagChain, context, errMsgID, Collections.singletonList(engineVariable));
                }
            }
        }
    }

    private void isValid_showVariable_ActionExpression(Action showVarAction, DiagnosticChain diagChain,
            Map<Object, Object> context) {

        ActionTypeNotEmpty actionType = showVarAction.getActionType();

        if (!(actionType == ActionTypeNotEmpty.SHOWVARIABLE)) {
            throw new IllegalArgumentException("isValid_showVariable_ActionExpression: wrong action type " + actionType);
        }

        // several parameters ...
        Map<String, Object> parseResult = ShowVariableActionExpressionParser
                .parseShowVariableParameters(showVarAction.getActionExpression());
        if (parseResult.containsKey(ShowVariableActionExpressionParser.KEY_ERROR)) {
            String errMsgID = "_Validation_global_Action_ActionExpression_ParseError";
            message(Severity.ERROR, showVarAction, diagChain, context, errMsgID,
                    Collections.singletonList((String) parseResult.get(ShowVariableActionExpressionParser.KEY_ERROR)));
        } else {
            String errMsgID = "_Validation_global_Action_ActionExpression_ShowVariable_Unknown";
            String varName = (String) parseResult.get(ShowVariableActionExpressionParser.PARAM_KEY_VALUE);

            if (variableName2Variable == null)
                throw new IllegalStateException("isValid_showVariable_ActionExpression: variableName2Variable map is null");

            if (!checkVariableName_isPredefOrKnownVariable(varName)) {
                message(Severity.ERROR, showVarAction, diagChain, context, errMsgID, Collections.singletonList(varName));
            }
            if(isInvalidVariableType(varName)) {
                message(Severity.ERROR, showVarAction, diagChain, context,
                        "_Validation_global_StructureVariable_not_allowed_in_ActionExpression", Collections.singletonList(varName));
            }
        }
    }

    private boolean isInvalidVariableType(String varName) {
        AbstractVariable var = variableName2Variable.get(varName);
        return (var != null && var instanceof StructureVariable);
    }

    private boolean checkVariableName_isPredefOrKnownVariable(String varName) {
        if (variableName2Variable == null) {
            throw new IllegalStateException("checkVariableName_isPredefOrKnownVariable: variableName2Variable map is null");
        }
        return !StringUtils.nullOrEmpty(varName)
            && (Variables.isPreDefinedName(varName) || variableName2Variable.containsKey(varName));
    }

    private void isValid_computeVariable_ActionExpression(EObject computeAction, DiagnosticChain diagChain,
            Map<Object, Object> context) {

        if (variableName2Variable == null)
            throw new IllegalStateException("SMARDModelValidator::isValid_computeVariable_ActionExpression: valueVariable2WriterStatemachine map is null");

        String actionExpression = null;
        List<? extends IOperand> operands = Collections.emptyList();

        if (computeAction instanceof ComputeVariable) {
            operands = ((ComputeVariable) computeAction).get_Operands();
            actionExpression = ((ComputeVariable) computeAction).getActionExpression();
        } else if (computeAction instanceof Action
            && ((Action) computeAction).getActionType() == ActionTypeNotEmpty.COMPUTE) {
                actionExpression = ((Action) computeAction).getActionExpression();
            } else if (computeAction instanceof CalculationExpression) {
                operands = ((CalculationExpression) computeAction).getOperands();
                actionExpression = ((CalculationExpression) computeAction).getExpression();

            } else if (computeAction instanceof ComputedVariable) {
                operands = ((ComputedVariable) computeAction).getOperands();
                actionExpression = ((ComputedVariable) computeAction).getExpression();
            } else
                throw new IllegalStateException("isValid_computeVariable_ActionExpression: parameter \"computeAction\" is " +
                        computeAction != null ? "of unhandled type: " + computeAction.getClass().getSimpleName() : "null");


        boolean parseStringLiterals = computeAction instanceof ComputeVariable || computeAction instanceof ComputedVariable;
        Term t = MathParser.parse(actionExpression, operands, parseStringLiterals);
        if (t == null) {
            message(Severity.ERROR, computeAction, diagChain, context, "_Validation_Statemachine_ComputeVariable_ActionExpression", Collections.singletonList(actionExpression));
            return;
        }

        // check if referenced variables are known
        List<String> referencedVarNames = t.getReferencedVariableNames();
        if (referencedVarNames != null) {
            String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_ReferencedVariable";
            for (String varName : referencedVarNames)
                if (Variables.isPreDefinedName(varName) == false && variableName2Variable.containsKey(varName) == false)
                    message(Severity.ERROR, computeAction, diagChain, context, errMsgID,
                            Collections.singletonList("action expression string references unknown variable names: " + varName));
        }

        //check for StructureVariable in Expression
        List<String> allVarNames = new ArrayList<>();
        allVarNames.addAll(t.getAssignedVariableNames());
        allVarNames.addAll(t.getReferencedVariableNames());
        if (!allVarNames.isEmpty()) {
            for (String varName : allVarNames) {
                if (isInvalidVariableType(varName)) {
                    message(Severity.ERROR, computeAction, diagChain, context,
                            "_Validation_global_StructureVariable_not_allowed_in_ActionExpression", Collections.singletonList(varName));
                }
            }
        }

        // check if assigned variables are known
        String varName = null;
        List<String> assignedVarNames = t.getAssignedVariableNames();
        if (assignedVarNames != null && assignedVarNames.isEmpty() == false) {

            varName = assignedVarNames.get(0);
            if (StringUtils.nullOrEmpty(varName) == false && Variables.isPreDefinedName(varName) == false
                && variableName2Variable.containsKey(varName) == false) {
                String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_AssignedVariable";
                message(Severity.ERROR, computeAction, diagChain, context, errMsgID,
                        Collections.singletonList("action expression string references unknown variable names: " + varName));
            }
        }

        // if all assigned and referenced variables are known, check if Term is valid with resolved variables
        if((computeAction instanceof ComputeVariable || computeAction instanceof ComputedVariable)
            && !ActionExpressionUtils.isValidTermWithResolvedVariableReferences(t, variableName2Variable)) {
            String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_StringConcat_NoPlusOperator";
            message(Severity.ERROR, computeAction, diagChain, context, errMsgID, Collections.singletonList(actionExpression));
        }

        // validate that assigned variable has only one writer statemachine
        if (computeAction instanceof ComputeVariable ||
            (computeAction instanceof Action && ((Action) computeAction).getActionType() == ActionTypeNotEmpty.COMPUTE)) {
            List<String> errors = new ArrayList<>();
            if (!isValid_checkHasOneWriterStatemachine((AbstractAction) computeAction, varName, errors)) {
                String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_Writer_Statemachines";
                if (!errors.isEmpty()) {
                    message(Severity.ERROR, computeAction, diagChain, context, errMsgID, errors);
                }
            }
        }

        // check if assignment is valid -> must not assign String to Double (ComputeVar) and vice versa (ActionExpression COMPUTE)
        AbstractVariable assignedVariable = null;

        if(referencedVarNames != null && !referencedVarNames.isEmpty()) {

            // retrieve assigned variable (and its name)
            if (computeAction instanceof ComputeVariable) {
                assignedVariable = ((ComputeVariable) computeAction).getTarget();
            } else if (computeAction instanceof Action
                && ((Action) computeAction).getActionType() == ActionTypeNotEmpty.COMPUTE) {
	        	if(variableName2Variable.containsKey(varName)) {
                        assignedVariable = variableName2Variable.get(varName);
                    }
	        } else if(computeAction instanceof ComputedVariable) {
                    assignedVariable = (ComputedVariable) computeAction;
                }

            if(assignedVariable != null && assignedVariable instanceof Variable) {
                // for all actionExpressions -> cannot assign String to Double
                if(DataType.DOUBLE.equals(((Variable)assignedVariable).getDataType()) ) {
                    for(String referencedVarName : referencedVarNames) {
                        if(!StringUtils.nullOrEmpty(referencedVarName) && variableName2Variable.containsKey(referencedVarName)) {
                            AbstractVariable referencedVariable = variableName2Variable.get(referencedVarName);
                            if (referencedVariable instanceof Variable) {
                                Variable refVar = (Variable) referencedVariable;
                                if (DataType.STRING.equals(refVar.getDataType()) &&
                                    isVariableNotInOperandChildrenList(operands, refVar)) {
                                    String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_IllegalAssignment_StringToDouble";
                                    message(Severity.ERROR, computeAction, diagChain, context, errMsgID,
                                            Arrays.asList(assignedVariable.getName(), referencedVarName));
                                }
                            }
                        }
                    }
                }
                // for COMPUTE action -> also cannot assign Double to String
                if(computeAction instanceof Action && DataType.STRING.equals(((Variable)assignedVariable).getDataType())) {
                    for(String referencedVarName : referencedVarNames) {
                        if(!StringUtils.nullOrEmpty(referencedVarName) && variableName2Variable.containsKey(referencedVarName)) {
                            AbstractVariable referencedVariable = variableName2Variable.get(referencedVarName);
                            if (referencedVariable instanceof Variable) {
                                Variable refVar = (Variable) referencedVariable;
                                if (DataType.DOUBLE.equals(refVar.getDataType()) &&
                                    isVariableNotInOperandChildrenList(operands, refVar)) {
                                    String errMsgID = "_Validation_global_Action_ActionExpression_ComputeVariable_IllegalAssignment_DoubleToString";
                                    message(Severity.ERROR, computeAction, diagChain, context, errMsgID,
                                            Arrays.asList(assignedVariable.getName(), referencedVarName));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isVariableNotInOperandChildrenList(List<? extends IOperand> operands, Variable variable) {
        if (variable == null)
            throw new IllegalArgumentException("param variable is null");

        if (operands.isEmpty())
            return true;

        Set<EObject> variableReferencesAsOperands;
        for (IOperand operand : operands) {
            variableReferencesAsOperands = filterSubtree(operand, VariableReference.class);
            for (EObject varRef : variableReferencesAsOperands)
                if (((VariableReference) varRef).getVariable().equals(variable))
                    return false;
        }
        return true;


    }

    private Set<EObject> filterSubtree(EObject root, Class filterType) {

        if (root == null)
            return Collections.emptySet();

        Set<EObject> result = new HashSet<EObject>();

    	for (Iterator<EObject> it = root.eResource().getAllContents(); it.hasNext(); ) {
            EObject current = it.next();
                if ( filterType.isAssignableFrom(current.getClass()) )
                    result.add( current );

        }

        return result;
    }

    private boolean isValid_checkHasOneWriterStatemachine(AbstractAction computeAction, String varName,
            List<String> errMessages) {

        if (variableName2Variable == null) {
            throw new IllegalStateException("isValid_checkHasOneWriterStatemachine: variableName2Variable map is null");
        }

        if (((computeAction instanceof ComputeVariable) ||
            ((computeAction instanceof Action) && ((Action) computeAction).getActionType() == ActionTypeNotEmpty.COMPUTE)) == false) {
            throw new IllegalArgumentException("isValid_checkHasOneWriterStatemachine: parameter \"computeAction\" is " +
                    "null or of wrong type");
        }

        if (this.valueVariable2WriterStatemachine == null) {
            this.valueVariable2WriterStatemachine = new HashMap<>();
        }

        ValueVariable assignedValueVariable = null;

        if (computeAction instanceof ComputeVariable) {
            ComputeVariable cv = (ComputeVariable) computeAction;
            assignedValueVariable = cv.getTarget();
            if (assignedValueVariable == null) {
                errMessages.add("assignment without left value for action: " + cv.getId());
                return false;
            }

        } else if (computeAction instanceof Action
            && ((Action) computeAction).getActionType() == ActionTypeNotEmpty.COMPUTE) {

                if (StringUtils.nullOrEmpty(varName)) {
                    errMessages.add("failed to check since variable name is null or empty " + computeAction.getId());
                    return false;
                }

                if (variableName2Variable.containsKey(varName) == false) {
                    errMessages.add("variable is unknown " + varName);
                    return false;
                }


                AbstractVariable v = variableName2Variable.get(varName);
                if ((v instanceof ValueVariable) == false) {
                    errMessages.add("variable is of invalid type " + varName + " (" + v.getClass().getSimpleName() + ")");
                    return false;
                }
                assignedValueVariable = (ValueVariable) v;

            }

        StateMachine writerStatemachine = (StateMachine) computeAction.eResource().getContents().get(0);
        if (writerStatemachine == null) {
            errMessages.add("could not find compute actions statemachine: " + computeAction.getId());
            return false;
        }


        StateMachine previousWriteSM = valueVariable2WriterStatemachine.put(assignedValueVariable, writerStatemachine);

        boolean isDifferentWriteStatemachine = previousWriteSM != null && previousWriteSM != writerStatemachine;
        if (isDifferentWriteStatemachine) {
            String variableName = varName;
            if (StringUtils.nullOrEmpty(variableName)) {
                if (assignedValueVariable != null)
                    variableName = assignedValueVariable.getName();
                else
                    variableName = "";
            }


            errMessages.add("value variable " + variableName + " assigned in multiple statemachines: "
                    + writerStatemachine.getName() + ", " + previousWriteSM.getName());
            return false;
        }
        return true;
    }

}
