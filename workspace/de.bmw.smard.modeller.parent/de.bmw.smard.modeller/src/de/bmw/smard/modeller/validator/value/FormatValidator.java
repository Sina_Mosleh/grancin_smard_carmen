package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.common.data.ShowVariableActionExpressionParser;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FormatValidator implements ValueValidator {
	
	private final String errorId;
	
	FormatValidator(String errorId) {
        this.errorId = errorId;
    }

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		if(!ShowVariableActionExpressionParser.isValidFormat(value)) {
			if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorId, null);
                return false;
            }
		}
		return true;
	}

}
