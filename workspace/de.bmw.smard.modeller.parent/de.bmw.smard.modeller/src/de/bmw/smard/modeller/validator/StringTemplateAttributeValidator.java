package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.validator.value.ValueValidator;

public class StringTemplateAttributeValidator {

    public static TemplateAttributeValidator.TmplParamStep<IllegalValueErrorStep> builder(String value) {
        return new StringTemplateAttributeValidator.Builder(value);
    }

    public interface IllegalValueErrorStep extends Build {
       Build illegalValueError(ValueValidator valueValidator);
    }

    public static class Builder extends TemplateAttributeValidator.Builder<String, IllegalValueErrorStep> implements
            IllegalValueErrorStep {

        private ValueValidator valueValidator;

        Builder(String value) {
            super(value);
        }

        @Override
        public Build illegalValueError(ValueValidator valueValidator) {
            this.valueValidator = valueValidator;
            return this;
        }

        @Override
        protected boolean validateValue(BaseValidator baseValidator, boolean exportable) {
            if(valueValidator != null) {
                return valueValidator.validate(getValue(), baseValidator);
            }
            return true;
        }
    }
}
