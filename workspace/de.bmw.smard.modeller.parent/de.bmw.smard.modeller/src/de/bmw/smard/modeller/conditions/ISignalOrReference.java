package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.statemachineset.BusMessageReferable;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISignal Or Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getISignalOrReference()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISignalOrReference extends ISignalComparisonExpressionOperand, BusMessageReferable {
} // ISignalOrReference
