package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalObserver()
 * @model
 * @generated
 */
public interface SignalObserver extends AbstractObserver {

} // SignalObserver
