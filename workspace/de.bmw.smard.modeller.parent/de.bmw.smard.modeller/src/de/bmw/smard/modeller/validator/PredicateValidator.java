package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.validation.Severity;

public class PredicateValidator {

    public interface AndStep extends ErrorStep, WarningStep {
        AndStep and(boolean valid);
    }

    public interface WarningStep {
        Build warning(String warningId);
        Build warning(String warningId, Object substitution);
    }

    public interface ErrorStep {
        Build error(String errorId);
        Build error(String errorId, Object substitution);
    }

    public static class Builder extends Validator implements ErrorStep, WarningStep, AndStep {

        private boolean predicate;
        private Severity severity;
        private String messageId;
        private Object substitution;

        Builder(boolean predicate) {
            this.predicate = predicate;
        }

        @Override
        public AndStep and(boolean result) {
            this.predicate &= result;
            return this;
        }

        @Override
        public Build warning(String warningId) {
            severity = Severity.WARNING;
            this.messageId = warningId;
            return this;
        }

        @Override
        public Build warning(String warningId, Object substitution) {
            severity = Severity.WARNING;
            this.messageId = warningId;
            this.substitution = substitution;
            return this;
        }

        @Override
        public Build error(String errorId) {
            severity = Severity.ERROR;
            this.messageId = errorId;
            return this;
        }

        @Override
        public Build error(String errorId, Object substitution) {
            severity = Severity.ERROR;
            this.messageId = errorId;
            this.substitution = substitution;
            return this;
        }

        @Override
        protected boolean validate(BaseValidator baseValidator) {
            if(predicate) {
                attach(baseValidator, severity, messageId, substitution != null ? substitution.toString() : null);
            }
            return !predicate;
        }

    }
}
