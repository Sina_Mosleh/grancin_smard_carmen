package de.bmw.smard.modeller.util;

import java.util.Collections;
import java.util.List;

import de.bmw.smard.common.interfaces.conditions.DataType;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.modeller.conditions.IOperand;

/**
 * Created by j.schmid on 20.01.2017.
 */
public class Operation implements Term {

    private final IOperand operand;

    public Object getOperand() {
        return operand;
    }

    public Operation(IOperand operand) {
        this.operand = operand;
    }

    @Override
    public List<String> getReferencedVariableNames() {
        if(operand != null) {
            return operand.getReadVariables();
        }
        return Collections.emptyList();
    }

    @Override
    public List<String> getAssignedVariableNames() {
        if(operand != null) {
            return operand.getWriteVariables();
        }
        return Collections.emptyList();
    }

    @Override
    public DataType getEvaluationDataType() {
        return DataType.getValue(operand.get_EvaluationDataType().getName());
    }
}
