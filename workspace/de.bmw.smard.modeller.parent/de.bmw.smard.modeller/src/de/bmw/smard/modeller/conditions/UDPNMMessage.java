package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UDPNM Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.UDPNMMessage#getUdpnmFilter <em>Udpnm Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUDPNMMessage()
 * @model
 * @generated
 */
public interface UDPNMMessage extends AbstractBusMessage {
    /**
     * Returns the value of the '<em><b>Udpnm Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Udpnm Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Udpnm Filter</em>' containment reference.
     * @see #setUdpnmFilter(UDPNMFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getUDPNMMessage_UdpnmFilter()
     * @model containment="true" required="true"
     * @generated
     */
    UDPNMFilter getUdpnmFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.UDPNMMessage#getUdpnmFilter <em>Udpnm Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Udpnm Filter</em>' containment reference.
     * @see #getUdpnmFilter()
     * @generated
     */
    void setUdpnmFilter(UDPNMFilter value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidUDPFilter(DiagnosticChain diagnostics, Map<Object, Object> context);

} // UDPNMMessage
