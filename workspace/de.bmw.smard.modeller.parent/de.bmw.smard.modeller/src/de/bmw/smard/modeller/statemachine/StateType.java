package de.bmw.smard.modeller.statemachine;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>State Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getStateType()
 * @model
 * @generated
 */
public enum StateType implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>INFO</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #INFO_VALUE
     * @generated
     * @ordered
     */
    INFO(0, "INFO", "INFO"),

    /**
     * The '<em><b>WARN</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #WARN_VALUE
     * @generated
     * @ordered
     */
    WARN(1, "WARN", "WARN"),

    /**
     * The '<em><b>DEFECT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DEFECT_VALUE
     * @generated
     * @ordered
     */
    DEFECT(2, "DEFECT", "DEFECT"),

    /**
     * The '<em><b>INIT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #INIT_VALUE
     * @generated
     * @ordered
     */
    INIT(3, "INIT", "INIT"),

    /**
     * The '<em><b>OK</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #OK_VALUE
     * @generated
     * @ordered
     */
    OK(4, "OK", "OK");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * The '<em><b>INFO</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>INFO</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #INFO
     * @model
     * @generated
     * @ordered
     */
    public static final int INFO_VALUE = 0;

    /**
     * The '<em><b>WARN</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>WARN</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #WARN
     * @model
     * @generated
     * @ordered
     */
    public static final int WARN_VALUE = 1;

    /**
     * The '<em><b>DEFECT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>DEFECT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #DEFECT
     * @model
     * @generated
     * @ordered
     */
    public static final int DEFECT_VALUE = 2;

    /**
     * The '<em><b>INIT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>INIT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #INIT
     * @model
     * @generated
     * @ordered
     */
    public static final int INIT_VALUE = 3;

    /**
     * The '<em><b>OK</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>OK</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #OK
     * @model
     * @generated
     * @ordered
     */
    public static final int OK_VALUE = 4;

    /**
     * An array of all the '<em><b>State Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final StateType[] VALUES_ARRAY =
            new StateType[] {
                    TEMPLATE_DEFINED,
                    INFO,
                    WARN,
                    DEFECT,
                    INIT,
                    OK,
            };

    /**
     * A public read-only list of all the '<em><b>State Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<StateType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>State Type</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static StateType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            StateType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>State Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static StateType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            StateType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>State Type</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static StateType get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case INFO_VALUE:
                return INFO;
            case WARN_VALUE:
                return WARN;
            case DEFECT_VALUE:
                return DEFECT;
            case INIT_VALUE:
                return INIT;
            case OK_VALUE:
                return OK;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private StateType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //StateType
