package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.UDPFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UDP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UDPFilterImpl#getChecksum <em>Checksum</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UDPFilterImpl extends TPFilterImpl implements UDPFilter {
    /**
     * The default value of the '{@link #getChecksum() <em>Checksum</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChecksum()
     * @generated
     * @ordered
     */
    protected static final String CHECKSUM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getChecksum() <em>Checksum</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChecksum()
     * @generated
     * @ordered
     */
    protected String checksum = CHECKSUM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UDPFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.UDP_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getChecksum() {
        return checksum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setChecksum(String newChecksum) {
        String oldChecksum = checksum;
        checksum = newChecksum;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDP_FILTER__CHECKSUM, oldChecksum, checksum));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSourcePort(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.UDP_FILTER__IS_VALID_HAS_VALID_SOURCE_PORT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(sourcePort)
                        .name(ConditionsPackage.Literals.TP_FILTER__SOURCE_PORT.getName())
                        .warning("_Validation_Conditions_UDPFilter_SourcePort_WARNING")
                        .error("_Validation_Conditions_UDPFilter_SourcePort_IllegalSourcePort")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDestinationPort(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.UDP_FILTER__IS_VALID_HAS_VALID_DESTINATION_PORT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(destPort)
                        .name(ConditionsPackage.Literals.TP_FILTER__DEST_PORT.getName())
                        .warning("_Validation_Conditions_UDPFilter_DestinationPort_WARNING")
                        .error("_Validation_Conditions_UDPFilter_DestinationPort_IllegalDestinationPort")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidChecksum(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.UDP_FILTER__IS_VALID_HAS_VALID_CHECKSUM)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(getChecksum())
                        .name(ConditionsPackage.Literals.UDP_FILTER__CHECKSUM.getName())
                        .warning("_Validation_Conditions_UDPFilter_Checksum_WARNING")
                        .error("_Validation_Conditions_UDPFilter_Checksum_IllegalChecksum")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.UDP_FILTER__CHECKSUM:
                return getChecksum();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.UDP_FILTER__CHECKSUM:
                setChecksum((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDP_FILTER__CHECKSUM:
                setChecksum(CHECKSUM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDP_FILTER__CHECKSUM:
                return CHECKSUM_EDEFAULT == null ? checksum != null : !CHECKSUM_EDEFAULT.equals(checksum);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (checksum: ");
        result.append(checksum);
        result.append(')');
        return result.toString();
    }

} //UDPFilterImpl
