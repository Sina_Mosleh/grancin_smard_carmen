package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPMessage#getTcpFilter <em>Tcp Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPMessage()
 * @model
 * @generated
 */
public interface TCPMessage extends AbstractBusMessage {
    /**
     * Returns the value of the '<em><b>Tcp Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Tcp Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Tcp Filter</em>' containment reference.
     * @see #setTcpFilter(TCPFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPMessage_TcpFilter()
     * @model containment="true" required="true"
     * @generated
     */
    TCPFilter getTcpFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPMessage#getTcpFilter <em>Tcp Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Tcp Filter</em>' containment reference.
     * @see #getTcpFilter()
     * @generated
     */
    void setTcpFilter(TCPFilter value);

} // TCPMessage
