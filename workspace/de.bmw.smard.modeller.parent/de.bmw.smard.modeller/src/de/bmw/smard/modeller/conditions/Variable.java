package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.Variable#getDataType <em>Data Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.Variable#getUnit <em>Unit</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormat <em>Variable Format</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormatTmplParam <em>Variable Format Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariable()
 * @model abstract="true"
 * @generated
 */
public interface Variable extends AbstractVariable {
    /**
     * Returns the value of the '<em><b>Data Type</b></em>' attribute.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.DataType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.DataType
     * @see #setDataType(DataType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariable_DataType()
     * @model
     * @generated
     */
    DataType getDataType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.Variable#getDataType <em>Data Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Data Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.DataType
     * @see #getDataType()
     * @generated
     */
    void setDataType(DataType value);

    /**
     * Returns the value of the '<em><b>Unit</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Unit</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Unit</em>' attribute.
     * @see #setUnit(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariable_Unit()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getUnit();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.Variable#getUnit <em>Unit</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Unit</em>' attribute.
     * @see #getUnit()
     * @generated
     */
    void setUnit(String value);

    /**
     * Returns the value of the '<em><b>Variable Format</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variable Format</em>' containment reference.
     * @see #setVariableFormat(VariableFormat)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariable_VariableFormat()
     * @model containment="true"
     * @generated
     */
    VariableFormat getVariableFormat();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormat <em>Variable Format</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Variable Format</em>' containment reference.
     * @see #getVariableFormat()
     * @generated
     */
    void setVariableFormat(VariableFormat value);

    /**
     * Returns the value of the '<em><b>Variable Format Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variable Format Tmpl Param</em>' attribute.
     * @see #setVariableFormatTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVariable_VariableFormatTmplParam()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getVariableFormatTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.Variable#getVariableFormatTmplParam <em>Variable Format Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Variable Format Tmpl Param</em>' attribute.
     * @see #getVariableFormatTmplParam()
     * @generated
     */
    void setVariableFormatTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInterpretedValue(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidType(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidVariableFormatTmplParam(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidUnit(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation" many="false"
     * @generated
     */
    EList<AbstractObserver> getObservers();

} // Variable
