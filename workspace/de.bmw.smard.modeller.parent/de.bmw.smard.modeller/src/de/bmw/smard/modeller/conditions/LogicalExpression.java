package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.LogicalExpression#getExpressions <em>Expressions</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperator <em>Operator</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLogicalExpression()
 * @model extendedMetaData="name='logicalOperator' kind='elementOnly'"
 * @generated
 */
public interface LogicalExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Expressions</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.Expression}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Expressions</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Expressions</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLogicalExpression_Expressions()
     * @model containment="true" required="true"
     *        extendedMetaData="kind='element' name='logicaloperations' namespace='##targetNamespace'"
     * @generated
     */
    EList<Expression> getExpressions();

    /**
     * Returns the value of the '<em><b>Operator</b></em>' attribute.
     * The default value is <code>"AND"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.LogicalOperatorType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operator</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Operator</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @see #setOperator(LogicalOperatorType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLogicalExpression_Operator()
     * @model default="AND" required="true"
     *        extendedMetaData="kind='attribute' name='operator' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    LogicalOperatorType getOperator();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperator <em>Operator</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Operator</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.LogicalOperatorType
     * @see #getOperator()
     * @generated
     */
    void setOperator(LogicalOperatorType value);

    /**
     * Returns the value of the '<em><b>Operator Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operator Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Operator Tmpl Param</em>' attribute.
     * @see #setOperatorTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLogicalExpression_OperatorTmplParam()
     * @model
     * @generated
     */
    String getOperatorTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LogicalExpression#getOperatorTmplParam <em>Operator Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Operator Tmpl Param</em>' attribute.
     * @see #getOperatorTmplParam()
     * @generated
     */
    void setOperatorTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOperator(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSubExpressions(DiagnosticChain diagnostics, Map<Object, Object> context);

} // LogicalExpression
