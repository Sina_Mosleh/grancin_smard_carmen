package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEmptyExtractStrategy()
 * @model
 * @generated
 */
public interface EmptyExtractStrategy extends ExtractStrategy {
} // EmptyExtractStrategy
