package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.ISignalOrReference;
import de.bmw.smard.modeller.conditions.SignalReference;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceImpl#getSignal <em>Signal</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalReferenceImpl extends ComparatorSignalImpl implements SignalReference {
    /**
     * The cached value of the '{@link #getSignal() <em>Signal</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSignal()
     * @generated
     * @ordered
     */
    protected ISignalOrReference signal;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SignalReferenceImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SIGNAL_REFERENCE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ISignalOrReference getSignal() {
        if (signal != null && signal.eIsProxy()) {
            InternalEObject oldSignal = (InternalEObject) signal;
            signal = (ISignalOrReference) eResolveProxy(oldSignal);
            if (signal != oldSignal) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.SIGNAL_REFERENCE__SIGNAL, oldSignal, signal));
            }
        }
        return signal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ISignalOrReference basicGetSignal() {
        return signal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSignal(ISignalOrReference newSignal) {
        ISignalOrReference oldSignal = signal;
        signal = newSignal;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.SIGNAL_REFERENCE__SIGNAL, oldSignal, signal));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSignal(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidSignal = true;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";

        // check if referenced state is in a folder that is applicable
        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            if (!TemplateUtils.getTemplateCategorization(this.signal).isExportable()) {
                errorMessageIdentifier = "_Validation_Conditions_SignalReference_Signal_isTemplateSignal";
                hasValidSignal = false;
            }
        }

        if (!hasValidSignal) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.SIGNAL_REFERENCE__IS_VALID_HAS_VALID_SIGNAL)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier)
                        .data(this)
                        .build());
            }
            return hasValidSignal;
        }
        return hasValidSignal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getSignal() != null) {
            return getSignal().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE__SIGNAL:
                if (resolve) return getSignal();
                return basicGetSignal();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE__SIGNAL:
                setSignal((ISignalOrReference) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE__SIGNAL:
                setSignal((ISignalOrReference) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE__SIGNAL:
                return signal != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public DataType get_EvaluationDataType() {
        if (signal != null)
            return signal.get_EvaluationDataType();
        return null;
    }

} //SignalReferenceImpl
