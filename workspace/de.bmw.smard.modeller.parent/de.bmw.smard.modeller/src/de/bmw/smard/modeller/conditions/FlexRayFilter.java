package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flex Ray Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannel <em>Channel</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter()
 * @model
 * @generated
 */
public interface FlexRayFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message Id Range</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message Id Range</em>' attribute.
     * @see #setMessageIdRange(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_MessageIdRange()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIdRange();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message Id Range</em>' attribute.
     * @see #getMessageIdRange()
     * @generated
     */
    void setMessageIdRange(String value);

    /**
     * Returns the value of the '<em><b>Slot Id</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Slot Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Slot Id</em>' attribute.
     * @see #setSlotId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_SlotId()
     * @model default="0" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getSlotId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getSlotId <em>Slot Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Slot Id</em>' attribute.
     * @see #getSlotId()
     * @generated
     */
    void setSlotId(String value);

    /**
     * Returns the value of the '<em><b>Cycle Offset</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cycle Offset</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Cycle Offset</em>' attribute.
     * @see #setCycleOffset(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_CycleOffset()
     * @model default="0" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getCycleOffset();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleOffset <em>Cycle Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Cycle Offset</em>' attribute.
     * @see #getCycleOffset()
     * @generated
     */
    void setCycleOffset(String value);

    /**
     * Returns the value of the '<em><b>Cycle Repetition</b></em>' attribute.
     * The default value is <code>"1"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Cycle Repetition</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Cycle Repetition</em>' attribute.
     * @see #setCycleRepetition(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_CycleRepetition()
     * @model default="1" dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getCycleRepetition();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getCycleRepetition <em>Cycle Repetition</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Cycle Repetition</em>' attribute.
     * @see #getCycleRepetition()
     * @generated
     */
    void setCycleRepetition(String value);

    /**
     * Returns the value of the '<em><b>Channel</b></em>' attribute.
     * The default value is <code>"A"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.FlexChannelType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Channel</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Channel</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.FlexChannelType
     * @see #setChannel(FlexChannelType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_Channel()
     * @model default="A"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    FlexChannelType getChannel();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannel <em>Channel</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Channel</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.FlexChannelType
     * @see #getChannel()
     * @generated
     */
    void setChannel(FlexChannelType value);

    /**
     * Returns the value of the '<em><b>Channel Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Channel Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Channel Tmpl Param</em>' attribute.
     * @see #setChannelTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayFilter_ChannelTmplParam()
     * @model
     * @generated
     */
    String getChannelTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayFilter#getChannelTmplParam <em>Channel Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Channel Tmpl Param</em>' attribute.
     * @see #getChannelTmplParam()
     * @generated
     */
    void setChannelTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidChannelType(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidCycleOffset(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidCycleRepeatInterval(DiagnosticChain diagnostics, Map<Object, Object> context);

} // FlexRayFilter
