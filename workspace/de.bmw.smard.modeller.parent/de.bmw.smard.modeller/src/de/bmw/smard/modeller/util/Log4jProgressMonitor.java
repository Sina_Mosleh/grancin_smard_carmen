package de.bmw.smard.modeller.util;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.NullProgressMonitor;

/**
 * Created by j.schmid on 07.02.2017.
 */
public class Log4jProgressMonitor extends NullProgressMonitor {
    private final Logger out;
    int step = 1;

    public Log4jProgressMonitor(Logger out) {
        this.out = out;
    }

    public void beginTask(String name, int totalWork) {
        this.out.info(name + "...");
    }

    public void done() {
        this.out.info("...done");
    }

    public void subTask(String name) {
        this.out.info(name);
        this.step = 1;
    }

    public void worked(int work) {
        this.out.info("Step " + this.step);
        ++this.step;
    }
}