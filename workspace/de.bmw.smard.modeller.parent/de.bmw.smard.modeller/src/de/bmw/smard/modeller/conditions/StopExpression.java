package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stop Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStopExpression()
 * @model extendedMetaData="name='stopCondition' kind='empty'"
 * @generated
 */
public interface StopExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Analyse Art</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Analyse Art</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Analyse Art</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
     * @see #setAnalyseArt(AnalysisTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStopExpression_AnalyseArt()
     * @model default="ALL" required="true"
     *        extendedMetaData="kind='attribute' name='analyseArt' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    AnalysisTypeOrTemplatePlaceholderEnum getAnalyseArt();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArt <em>Analyse Art</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Analyse Art</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.AnalysisTypeOrTemplatePlaceholderEnum
     * @see #getAnalyseArt()
     * @generated
     */
    void setAnalyseArt(AnalysisTypeOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Analyse Art Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Analyse Art Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Analyse Art Tmpl Param</em>' attribute.
     * @see #setAnalyseArtTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStopExpression_AnalyseArtTmplParam()
     * @model
     * @generated
     */
    String getAnalyseArtTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.StopExpression#getAnalyseArtTmplParam <em>Analyse Art Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Analyse Art Tmpl Param</em>' attribute.
     * @see #getAnalyseArtTmplParam()
     * @generated
     */
    void setAnalyseArtTmplParam(String value);

} // StopExpression
