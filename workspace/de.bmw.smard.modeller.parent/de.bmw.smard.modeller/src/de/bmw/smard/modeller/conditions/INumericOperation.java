package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>INumeric Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getINumericOperation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface INumericOperation extends INumericOperand, IOperation {
} // INumericOperation
