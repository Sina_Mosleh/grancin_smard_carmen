package de.bmw.smard.modeller.statemachine;

import de.bmw.smard.modeller.conditions.AbstractVariable;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.Action#getActionType <em>Action Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.Action#getActionExpression <em>Action Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.Action#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.Action#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAction()
 * @model extendedMetaData="kind='elementOnly' name='Action'"
 * @generated
 */
public interface Action extends AbstractAction, SmardTraceElement {
    /**
     * Returns the value of the '<em><b>Action Type</b></em>' attribute.
     * The default value is <code>"COMPUTE"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Action Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Action Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
     * @see #setActionType(ActionTypeNotEmpty)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAction_ActionType()
     * @model default="COMPUTE"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    ActionTypeNotEmpty getActionType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.Action#getActionType <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Action Type</em>' attribute.
     * @see de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty
     * @see #getActionType()
     * @generated
     */
    void setActionType(ActionTypeNotEmpty value);

    /**
     * Returns the value of the '<em><b>Action Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Action Expression</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Action Expression</em>' attribute.
     * @see #setActionExpression(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAction_ActionExpression()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getActionExpression();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.Action#getActionExpression <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Action Expression</em>' attribute.
     * @see #getActionExpression()
     * @generated
     */
    void setActionExpression(String value);

    /**
     * Returns the value of the '<em><b>Action Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Action Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Action Type Tmpl Param</em>' attribute.
     * @see #setActionTypeTmplParam(String)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAction_ActionTypeTmplParam()
     * @model
     * @generated
     */
    String getActionTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.Action#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Action Type Tmpl Param</em>' attribute.
     * @see #getActionTypeTmplParam()
     * @generated
     */
    void setActionTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Environment</b></em>' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Environment</em>' reference.
     * @see #setEnvironment(AbstractVariable)
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getAction_Environment()
     * @model
     * @generated
     */
    AbstractVariable getEnvironment();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachine.Action#getEnvironment <em>Environment</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Environment</em>' reference.
     * @see #getEnvironment()
     * @generated
     */
    void setEnvironment(AbstractVariable value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidActionType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // Action
