package de.bmw.smard.modeller.statemachineset.util;

import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import de.bmw.smard.modeller.statemachineset.DocumentRoot;
import de.bmw.smard.modeller.statemachineset.ExecutionConfig;
import de.bmw.smard.modeller.statemachineset.GeneralInfo;
import de.bmw.smard.modeller.statemachineset.KeyValuePairUserConfig;
import de.bmw.smard.modeller.statemachineset.Output;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EObjectValidator;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage
 * @generated
 */
public class StatemachinesetValidator extends EObjectValidator {
    /**
     * The cached model package
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final StatemachinesetValidator INSTANCE = new StatemachinesetValidator();

    /**
     * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic
     * {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.common.util.Diagnostic#getSource()
     * @see org.eclipse.emf.common.util.Diagnostic#getCode()
     * @generated
     */
    public static final String DIAGNOSTIC_SOURCE = "de.bmw.smard.modeller.statemachineset";

    /**
     * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Description' of 'State Machine Set'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int STATE_MACHINE_SET__IS_VALID_HAS_VALID_DESCRIPTION = 1;

    /**
     * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Log Level' of 'Execution Config'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int EXECUTION_CONFIG__IS_VALID_HAS_VALID_LOG_LEVEL = 2;

    /**
     * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is Valid has Valid Sorter Value' of 'Execution Config'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int EXECUTION_CONFIG__IS_VALID_HAS_VALID_SORTER_VALUE = 3;

    /**
     * A constant with a fixed name that can be used as the base value for additional hand written constants.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 3;

    /**
     * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

    /**
     * Creates an instance of the switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachinesetValidator() {
        super();
    }

    /**
     * Returns the package of this validator switch.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EPackage getEPackage() {
        return StatemachinesetPackage.eINSTANCE;
    }

    /**
     * Calls <code>validateXXX</code> for the corresponding classifier of the model.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
        switch (classifierID) {
            case StatemachinesetPackage.DOCUMENT_ROOT:
                return validateDocumentRoot((DocumentRoot) value, diagnostics, context);
            case StatemachinesetPackage.GENERAL_INFO:
                return validateGeneralInfo((GeneralInfo) value, diagnostics, context);
            case StatemachinesetPackage.OUTPUT:
                return validateOutput((Output) value, diagnostics, context);
            case StatemachinesetPackage.STATE_MACHINE_SET:
                return validateStateMachineSet((StateMachineSet) value, diagnostics, context);
            case StatemachinesetPackage.KEY_VALUE_PAIR_USER_CONFIG:
                return validateKeyValuePairUserConfig((KeyValuePairUserConfig) value, diagnostics, context);
            case StatemachinesetPackage.EXECUTION_CONFIG:
                return validateExecutionConfig((ExecutionConfig) value, diagnostics, context);
            case StatemachinesetPackage.BUS_MESSAGE_REFERABLE:
                return validateBusMessageReferable((BusMessageReferable) value, diagnostics, context);
            default:
                return true;
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateDocumentRoot(DocumentRoot documentRoot, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate_EveryDefaultConstraint(documentRoot, diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateGeneralInfo(GeneralInfo generalInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate_EveryDefaultConstraint(generalInfo, diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateOutput(Output output, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate_EveryDefaultConstraint(output, diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateStateMachineSet(StateMachineSet stateMachineSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
        if (!validate_NoCircularContainment(stateMachineSet, diagnostics, context)) return false;
        boolean result = validate_EveryMultiplicityConforms(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryDataValueConforms(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryProxyResolves(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_UniqueID(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryKeyUnique(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(stateMachineSet, diagnostics, context);
        if (result || diagnostics != null) result &= validateStateMachineSet_isValid_hasValidDescription(stateMachineSet, diagnostics, context);
        return result;
    }

    /**
     * Validates the isValid_hasValidDescription constraint of '<em>State Machine Set</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateStateMachineSet_isValid_hasValidDescription(StateMachineSet stateMachineSet, DiagnosticChain diagnostics,
            Map<Object, Object> context) {
        return stateMachineSet.isValid_hasValidDescription(diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateKeyValuePairUserConfig(KeyValuePairUserConfig keyValuePairUserConfig, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate_EveryDefaultConstraint(keyValuePairUserConfig, diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateExecutionConfig(ExecutionConfig executionConfig, DiagnosticChain diagnostics, Map<Object, Object> context) {
        if (!validate_NoCircularContainment(executionConfig, diagnostics, context)) return false;
        boolean result = validate_EveryMultiplicityConforms(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryDataValueConforms(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryProxyResolves(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_UniqueID(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryKeyUnique(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validateExecutionConfig_isValid_hasValidLogLevel(executionConfig, diagnostics, context);
        if (result || diagnostics != null) result &= validateExecutionConfig_isValid_hasValidSorterValue(executionConfig, diagnostics, context);
        return result;
    }

    /**
     * Validates the isValid_hasValidLogLevel constraint of '<em>Execution Config</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateExecutionConfig_isValid_hasValidLogLevel(ExecutionConfig executionConfig, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return executionConfig.isValid_hasValidLogLevel(diagnostics, context);
    }

    /**
     * Validates the isValid_hasValidSorterValue constraint of '<em>Execution Config</em>'.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateExecutionConfig_isValid_hasValidSorterValue(ExecutionConfig executionConfig, DiagnosticChain diagnostics,
            Map<Object, Object> context) {
        return executionConfig.isValid_hasValidSorterValue(diagnostics, context);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public boolean validateBusMessageReferable(BusMessageReferable busMessageReferable, DiagnosticChain diagnostics, Map<Object, Object> context) {
        return validate_EveryDefaultConstraint(busMessageReferable, diagnostics, context);
    }

    /**
     * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ResourceLocator getResourceLocator() {
        // TODO
        // Specialize this to return a resource locator for messages specific to this validator.
        // Ensure that you remove @generated or mark it @generated NOT
        return super.getResourceLocator();
    }

} //StatemachinesetValidator
