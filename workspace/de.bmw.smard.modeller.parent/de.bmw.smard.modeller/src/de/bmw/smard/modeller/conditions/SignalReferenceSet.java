package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Reference Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalReferenceSet#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalReferenceSet()
 * @model
 * @generated
 */
public interface SignalReferenceSet extends EObject {
    /**
     * Returns the value of the '<em><b>Messages</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractMessage}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Messages</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Messages</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalReferenceSet_Messages()
     * @model containment="true"
     * @generated
     */
    EList<AbstractMessage> getMessages();

    /**
     * @param message
     * @return
     * @generated NOT
     */
    boolean addMessage(AbstractMessage message);

} // SignalReferenceSet
