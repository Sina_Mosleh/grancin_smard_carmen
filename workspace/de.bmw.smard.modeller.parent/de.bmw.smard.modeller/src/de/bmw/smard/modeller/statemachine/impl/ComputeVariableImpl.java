package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.math.Assignment;
import de.bmw.smard.common.math.BinaryOperation;
import de.bmw.smard.common.math.NumericLiteral;
import de.bmw.smard.common.math.StringLiteral;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.common.math.UnaryOperation;
import de.bmw.smard.common.math.VariableReference;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.statemachine.ComputeVariable;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.statemachineset.util.ExecutionConfigFlagDeterminator;
import de.bmw.smard.modeller.util.ActionExpressionUtils;
import de.bmw.smard.modeller.util.Operation;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.VariableManager;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Compute Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl#getActionExpression <em>Action Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl#getTarget <em>Target</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ComputeVariableImpl#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComputeVariableImpl extends AbstractActionImpl implements ComputeVariable {
    /**
     * The default value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionExpression()
     * @generated
     * @ordered
     */
    protected static final String ACTION_EXPRESSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionExpression()
     * @generated
     * @ordered
     */
    protected String actionExpression = ACTION_EXPRESSION_EDEFAULT;

    /**
     * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTarget()
     * @generated
     * @ordered
     */
    protected ValueVariable target;

    /**
     * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperands()
     * @generated
     * @ordered
     */
    protected EList<IComputeVariableActionOperand> operands;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ComputeVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.COMPUTE_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getActionExpression() {
        return actionExpression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        EList<String> variableNames = ActionExpressionUtils.extractReadVariableNames(getActionExpression(), getOperands(), true);
        for(String varName : variableNames){
            AbstractVariable var =  VariableManager.getInstance(this).getVariableByName(varName);
            if(var != null){
                messages.addAll(var.getAllReferencedBusMessages());
            }
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActionExpression(String newActionExpression) {
        String oldActionExpression = actionExpression;
        actionExpression = newActionExpression;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION, oldActionExpression, actionExpression));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ValueVariable getTarget() {
        if (target != null && target.eIsProxy()) {
            InternalEObject oldTarget = (InternalEObject) target;
            target = (ValueVariable) eResolveProxy(oldTarget);
            if (target != oldTarget) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.COMPUTE_VARIABLE__TARGET, oldTarget, target));
            }
        }
        return target;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ValueVariable basicGetTarget() {
        return target;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTarget(ValueVariable newTarget) {
        ValueVariable oldTarget = target;
        target = newTarget;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.COMPUTE_VARIABLE__TARGET, oldTarget, target));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<IComputeVariableActionOperand> getOperands() {
        if (operands == null) {
            operands =
                    new EObjectContainmentEList<IComputeVariableActionOperand>(IComputeVariableActionOperand.class, this, StatemachinePackage.COMPUTE_VARIABLE__OPERANDS);
        }
        return operands;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidOperands = true;
        String errorMessageIdentifier = "_Validation_Statemachine_ComputeVariable_operands";
        String messageToSubstitute = null;

        if (target == null) {
            return false; // no need to write custom err msg as this constraint is expressed in ecore moodel
        }

        // if target is Double, all operands must be numeric
        if (target.getDataType() == DataType.DOUBLE && operands != null) {
            for (IComputeVariableActionOperand op : operands) {
                if (op.get_EvaluationDataType() != DataType.DOUBLE) {
                    messageToSubstitute = "target is double => all operands must evaluate to double";
                    hasValidOperands = false;
                }
            }
        }

        if (!hasValidOperands && diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.error()
                    .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                    .code(StatemachineValidator.COMPUTE_VARIABLE__IS_VALID_HAS_VALID_OPERANDS)
                    .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                    .data(this)
                    .build());
        }
        return hasValidOperands;
    }

    /**
     * <!-- begin-user-doc --> - not an assignment - all operands are of
     * compatible type - keywords: either system variable, or signal/value
     * variable or OP to reference child operands <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidActionExpression = true;
        String errorMessageIdentifier = "_Validation_Statemachine_ComputeVariable_ActionExpression";
        String messageToSubstitute = null;
        Term term = null;

        // if TemplateContext, assume everything is valid -> generated Statemachines will be validated then
        if (!TemplateUtils.getTemplateCategorization(this).isExportable()) {
            return true;
        }

        ValueVariable targetVariable = getTarget();
        if (targetVariable != null) {
            boolean parseStringLiterals = DataType.STRING.equals(targetVariable.getDataType());
            try {
                // parse StringLiterals for ComputeVariable if target is STRING
                term = MathParser.parse(actionExpression, operands, parseStringLiterals);
            } catch (ActionExpressionParseException parseExc) {
                messageToSubstitute = parseExc.getMessage();
                hasValidActionExpression = false;
            }

            if (hasValidActionExpression && term == null) {
                messageToSubstitute = "missing action expression. (Use \"OP1\",\"OP2\", ... to reference child operators.)";
                hasValidActionExpression = false;
            } else if (term != null) {
                if (!ActionExpressionUtils.isValidTermWithResolvedVariableReferences(term, Collections.emptyMap())) {
                    errorMessageIdentifier = "_Validation_global_Action_ActionExpression_ComputeVariable_StringConcat_NoPlusOperator";
                    messageToSubstitute = actionExpression;
                    hasValidActionExpression = false;
                }
                if (term instanceof Assignment) {
                    messageToSubstitute = "assignment not allowed in action expression";
                    hasValidActionExpression = false;
                } else if ((term instanceof VariableReference
                    || term instanceof Operation
                    || term instanceof UnaryOperation
                    || term instanceof BinaryOperation
                    || term instanceof NumericLiteral
                    || term instanceof StringLiteral) == false) {
                        messageToSubstitute = "action expression term is of wrong type: " + term.getClass().getSimpleName();
                        hasValidActionExpression = false;
                    }
            }

            if (term != null)
                ActionExpressionUtils.checkAndWarnForDeprecatedSystemVariableUsage(
                        term.getReferencedVariableNames(), actionExpression, this, diagnostics, context);

        } else { // target is null
            errorMessageIdentifier = "_Validation_Statemachine_ComputeVariable_target";
            hasValidActionExpression = false;
        }

        if (!hasValidActionExpression && diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.error()
                    .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                    .code(StatemachineValidator.COMPUTE_VARIABLE__IS_VALID_HAS_VALID_ACTION_EXPRESSION)
                    .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                    .data(this)
                    .build());
        }
        return hasValidActionExpression;
    }

    /**
     * <!-- begin-user-doc --> required operands' datatype depends on target or
     * already given operands <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        if (target != null)
            return target.getDataType();

        for (IOperand operand : getOperands()) {
            if (operand.get_EvaluationDataType() != null)
                return operand.get_EvaluationDataType();
        }

        // if getOperands was empty
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.addAll(getOperands());
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
                return ((InternalEList<?>) getOperands()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
                return getActionExpression();
            case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
                if (resolve) return getTarget();
                return basicGetTarget();
            case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
                return getOperands();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
                setActionExpression((String) newValue);
                return;
            case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
                setTarget((ValueVariable) newValue);
                return;
            case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
                getOperands().clear();
                getOperands().addAll((Collection<? extends IComputeVariableActionOperand>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
                setActionExpression(ACTION_EXPRESSION_EDEFAULT);
                return;
            case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
                setTarget((ValueVariable) null);
                return;
            case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
                getOperands().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.COMPUTE_VARIABLE__ACTION_EXPRESSION:
                return ACTION_EXPRESSION_EDEFAULT == null ? actionExpression != null : !ACTION_EXPRESSION_EDEFAULT.equals(actionExpression);
            case StatemachinePackage.COMPUTE_VARIABLE__TARGET:
                return target != null;
            case StatemachinePackage.COMPUTE_VARIABLE__OPERANDS:
                return operands != null && !operands.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (actionExpression: ");
        result.append(actionExpression);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public DataType get_EvaluationDataType() {
        return null;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        return ActionExpressionUtils.extractReadVariableNames(actionExpression, operands, true);
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        // use getTarget() instead of this.target to avoid null values
        if (getTarget() != null && StringUtils.nullOrEmpty(getTarget().getName()) == false) {
            return ECollections.singletonEList(getTarget().getName());
        }
        throw new IllegalStateException("called getWriteVariables on invalid configured compute variable: " +
                "no target variable or target variable has no name");
    }


} // ComputeVariableImpl
