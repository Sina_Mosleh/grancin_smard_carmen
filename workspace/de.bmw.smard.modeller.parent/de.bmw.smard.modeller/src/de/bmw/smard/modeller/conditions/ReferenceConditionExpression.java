package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Condition Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ReferenceConditionExpression#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getReferenceConditionExpression()
 * @model
 * @generated
 */
public interface ReferenceConditionExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Condition</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Condition</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Condition</em>' reference.
     * @see #setCondition(Condition)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getReferenceConditionExpression_Condition()
     * @model required="true"
     * @generated
     */
    Condition getCondition();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ReferenceConditionExpression#getCondition <em>Condition</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Condition</em>' reference.
     * @see #getCondition()
     * @generated
     */
    void setCondition(Condition value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidReferencedCondition(DiagnosticChain diagnostics, Map<Object, Object> context);

} // ReferenceConditionExpression
