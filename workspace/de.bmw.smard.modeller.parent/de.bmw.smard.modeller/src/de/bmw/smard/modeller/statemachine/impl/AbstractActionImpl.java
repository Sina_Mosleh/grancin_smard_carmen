package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.impl.BaseClassWithIDImpl;
import de.bmw.smard.modeller.statemachine.AbstractAction;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Trigger;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Arrays;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object * '<em><b>Abstract Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl#getDescription <em>Description</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl#getTrigger <em>Trigger</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.AbstractActionImpl#getTriggerTmplParam <em>Trigger Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractActionImpl extends BaseClassWithIDImpl implements AbstractAction {
    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTrigger()
     * @generated
     * @ordered
     */
    protected static final Trigger TRIGGER_EDEFAULT = Trigger.ON_ENTRY;

    /**
     * The cached value of the '{@link #getTrigger() <em>Trigger</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTrigger()
     * @generated
     * @ordered
     */
    protected Trigger trigger = TRIGGER_EDEFAULT;

    /**
     * The default value of the '{@link #getTriggerTmplParam() <em>Trigger Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTriggerTmplParam()
     * @generated
     * @ordered
     */
    protected static final String TRIGGER_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTriggerTmplParam() <em>Trigger Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTriggerTmplParam()
     * @generated
     * @ordered
     */
    protected String triggerTmplParam = TRIGGER_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractActionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.ABSTRACT_ACTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION, oldDescription, description));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Trigger getTrigger() {
        return trigger;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTrigger(Trigger newTrigger) {
        Trigger oldTrigger = trigger;
        trigger = newTrigger == null ? TRIGGER_EDEFAULT : newTrigger;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__TRIGGER, oldTrigger, trigger));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTriggerTmplParam() {
        return triggerTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTriggerTmplParam(String newTriggerTmplParam) {
        String oldTriggerTmplParam = triggerTmplParam;
        triggerTmplParam = newTriggerTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM, oldTriggerTmplParam, triggerTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidTrigger(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.ABSTRACT_ACTION__IS_VALID_HAS_VALID_TRIGGER)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(trigger)
                        .error("_Validation_Statemachine_Action_Trigger_IsNull")
                        .build())

                .with(Validators.checkTemplate(trigger)
                        .templateType(Trigger.TEMPLATEDEFINED)
                        .tmplParam(triggerTmplParam)
                        .containsParameterError("_Validation_Statemachine_Action_Trigger_ContainsPlaceholders")
                        .tmplParamIsNullError("_Validation_Statemachine_Action_TriggerTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Statemachine_Action_TriggerTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(Trigger.ON_ENTRY, Trigger.ON_EXIT), "_Validation_Statemachine_Action_Trigger_IllegalTrigger")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<String> getReadVariables() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<String> getWriteVariables() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
                return getDescription();
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
                return getTrigger();
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
                return getTriggerTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
                setDescription((String) newValue);
                return;
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
                setTrigger((Trigger) newValue);
                return;
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
                setTriggerTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
                setDescription(DESCRIPTION_EDEFAULT);
                return;
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
                setTrigger(TRIGGER_EDEFAULT);
                return;
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
                setTriggerTmplParam(TRIGGER_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ABSTRACT_ACTION__DESCRIPTION:
                return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER:
                return trigger != TRIGGER_EDEFAULT;
            case StatemachinePackage.ABSTRACT_ACTION__TRIGGER_TMPL_PARAM:
                return TRIGGER_TMPL_PARAM_EDEFAULT == null ? triggerTmplParam != null : !TRIGGER_TMPL_PARAM_EDEFAULT.equals(triggerTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (description: ");
        result.append(description);
        result.append(", trigger: ");
        result.append(trigger);
        result.append(", triggerTmplParam: ");
        result.append(triggerTmplParam);
        result.append(')');
        return result.toString();
    }

} // AbstractActionImpl
