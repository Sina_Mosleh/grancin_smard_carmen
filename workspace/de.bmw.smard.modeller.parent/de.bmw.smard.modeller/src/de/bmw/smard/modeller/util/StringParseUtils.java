package de.bmw.smard.modeller.util;

import java.util.List;

public class StringParseUtils
{
 public static String parseIntListOrRange(String pattern, String type, boolean mandatory, boolean templateAllowed, boolean isHexAllowed, Integer minValue, Integer maxValue, List<String> errorMessages, List<String> warningMessages)
 {
     /*
      * PatternParameters ::= PatternParameter [ { PatternParameterSeparator } PatternParameters ] PatternParameterSeparator ::= ','
      * | ';' | '/' PatternParameter ::= PatternVal | '[' PatternRange ']' tPatternRange ::= PatternVal | PatternVal {
      * PatternRangeSeparator } PatternVal PatternRangeSeparator ::= '-' | '..' PatternVal ::= <pattern value as int from 0 to 65535>
      */
     String res = "";
     
     if ( pattern != null && pattern.trim().length() != 0 )
     {
         pattern = pattern.trim();
         
         if (!templateAllowed)
         {
             if ( TemplateUtils.containsParameters( pattern ) )
             {
                 errorMessages.add( type + " must not contain template placeholders." );
             }
     
         }

         boolean isRange = false;
         
         // PatternParameters ::= PatternParameter [ { PatternParameterSeparator } PatternParameters ]
         String[] patternParameters = pattern.split( "[,;/]" );
         for ( String patternParameter : patternParameters )
         {
             patternParameter = patternParameter.trim();

             // detect ranges
             // PatternParameter ::= PatternVal | '[' PatternRange ']'
             String patternParameterContentString;
             if ( patternParameter.startsWith( "[" ) && patternParameter.endsWith( "]" ) )
             {
                 // PatternRange -> remove brackets
                 patternParameterContentString = patternParameter.substring( 1, patternParameter.length() - 1 );
             }
             else
             {
                 // PatternVal
                 patternParameterContentString = patternParameter;
             }

             patternParameterContentString = patternParameterContentString.trim();

             // split range values
             // PatternRange ::= PatternVal | PatternVal '-' PatternVal
             String[] patternRangeParts = patternParameterContentString.split( "-" );
             int[] patternRangeValues = new int[patternRangeParts.length];
             for ( int i = 0; i < patternRangeParts.length; i++ )
             {
                 String patternString = patternRangeParts[i].trim();

                 if (isHexAllowed && patternString.toLowerCase().startsWith( "0x" ))
                 {
                     try
                     {
                         patternRangeValues[i] = Integer.parseInt(patternString.substring(2), 16);
                     }
                     catch ( NumberFormatException e )
                     {
                         errorMessages.add( type + " must be an integer or hex value, not '" + patternString + "'." );
                     }
                 }
                 else
                 {
                     try
                     {
                         patternRangeValues[i] = Integer.parseInt( patternString );
                     }
                     catch ( NumberFormatException e )
                     {
                         if (!templateAllowed)
                         {
                             errorMessages.add( type + " must be an integer, not '" + patternString + "'." );
                         }
                     }
                 }
             }
             if ( patternRangeValues.length == 1 )
             {
                 // patternVal ok
             }
             else if ( patternRangeValues.length == 2 )
             {
                 isRange = true;

                 // patternRange
                 if ( patternRangeValues[1] < patternRangeValues[0] )
                 {
                     errorMessages.add( type + " range must have first value less or equal to second value ''"
                         + patternParameterContentString + "'." );
                 }
                 if (minValue != null && patternRangeValues[1] < minValue)
                 {
                     errorMessages.add( "Left value of " + type + " range must be greater than '"
                                     + minValue + "." );
                 }
                 if (maxValue != null && patternRangeValues[1] > maxValue)
                 {
                     errorMessages.add( "Left value of " + type + " range must be lower than '"
                                     + maxValue + "." );
                 }
             }
             else
             {
                 // something else
                 errorMessages.add( type + " must be a single value or a range, not '" + patternParameterContentString
                     + "'." );
             }
         }

         if ( isRange == true )
         {
             warningMessages.add( type
                 + " contains a range value. The evaluation process in the engine can therefore take more time." );
         }

         res = pattern;
     }
     else if (mandatory == true)
     {
         errorMessages.add( type + " must not be null" );
     }
     return res;
 }
 
 public static int countMatches(String value, String match)
 {
     return (value.length() - value.replace(match, "").length())/match.length();
 }
}
