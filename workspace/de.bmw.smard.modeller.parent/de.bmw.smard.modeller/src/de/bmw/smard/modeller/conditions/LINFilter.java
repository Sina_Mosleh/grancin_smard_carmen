package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LIN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.LINFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LINFilter#getFrameId <em>Frame Id</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLINFilter()
 * @model
 * @generated
 */
public interface LINFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message Id Range</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message Id Range</em>' attribute.
     * @see #setMessageIdRange(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLINFilter_MessageIdRange()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIdRange();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LINFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message Id Range</em>' attribute.
     * @see #getMessageIdRange()
     * @generated
     */
    void setMessageIdRange(String value);

    /**
     * Returns the value of the '<em><b>Frame Id</b></em>' attribute.
     * The default value is <code>"0x0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Frame Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Frame Id</em>' attribute.
     * @see #setFrameId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLINFilter_FrameId()
     * @model default="0x0" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getFrameId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LINFilter#getFrameId <em>Frame Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Frame Id</em>' attribute.
     * @see #getFrameId()
     * @generated
     */
    void setFrameId(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

} // LINFilter
