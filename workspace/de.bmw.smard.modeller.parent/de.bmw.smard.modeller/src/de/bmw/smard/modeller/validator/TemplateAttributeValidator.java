package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.Enumerator;

public abstract class TemplateAttributeValidator {

    public interface TemplateTypeStep<T> {
        TmplParamStep<T> templateType(Enumerator templateType);
    }

    public interface TmplParamStep<T> {
        ContainsParameterErrorStep<T> tmplParam(String tmplParam);
    }

    public interface ContainsParameterErrorStep<T> {
        NullErrorStep<T> containsParameterError(String errorId);
    }

    public interface NullErrorStep<T> {
        NoPlaceholderErrorStep<T> tmplParamIsNullError(String errorId);
    }

    public interface NoPlaceholderErrorStep<T> {
        T noPlaceholderError(String errorId);
    }

    public abstract static class Builder<T, S> extends Validator implements
            TemplateTypeStep,
            TmplParamStep,
            NullErrorStep,
            ContainsParameterErrorStep,
            NoPlaceholderErrorStep {

        private T value;
        private String tmplParam;
        private Enumerator templateType;
        private String containsParameterErrorId;
        private String isNullErrorId;
        private String noPlaceHolderErrorId;

        public T getValue() {
            return value;
        }

        protected Builder(T value) {
            this.value = value;
        }

        public TmplParamStep templateType(Enumerator templateType) {
            this.templateType = templateType;
            return this;
        }

        @Override
        public ContainsParameterErrorStep tmplParam(String tmplParam) {
            this.tmplParam = tmplParam;
            return this;
        }

        @Override
        public NoPlaceholderErrorStep tmplParamIsNullError(String errorId) {
            this.isNullErrorId = errorId;
            return this;
        }

        @Override
        public NullErrorStep containsParameterError(String errorId) {
            this.containsParameterErrorId = errorId;
            return this;
        }

        @Override
        public S noPlaceholderError(String errorId) {
            this.noPlaceHolderErrorId = errorId;
            return (S)this;
        }

        protected boolean validate(BaseValidator baseValidator) {

            boolean isValid = true;
            boolean exportable = TemplateUtils.getTemplateCategorization(baseValidator.getObject()).isExportable();
            if (value != null) {
                if (exportable) {
                    if(isTemplateType()) {
                        attachError(baseValidator, containsParameterErrorId);
                        isValid = false;
                    } else {
                        isValid = validateValue(baseValidator, exportable);
                    }
                } else {
                    if (isTemplateType()) {
                        if (tmplParam == null) {
                            attachError(baseValidator, isNullErrorId);
                            isValid = false;
                        } else {
                            String substitution = TemplateUtils.checkValidPlaceholderName(tmplParam);
                            if (!StringUtils.nullOrEmpty(substitution)) {
                                attachError(baseValidator, noPlaceHolderErrorId, substitution);
                                isValid = false;
                            }
                        }
                    } else {
                        isValid = validateValue(baseValidator, exportable);
                    }
                }
            }

            return isValid;
        }

        protected abstract boolean validateValue(BaseValidator baseValidator, boolean exportable);

        private boolean isTemplateType() {
            // if value equals templateType or value contains parameters we have to check the tmplParam
            return (value == templateType) || ((value instanceof String) && TemplateUtils.containsParameters((String)value));
        }
    }

}