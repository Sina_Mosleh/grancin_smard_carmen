package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Bus Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractBusMessage()
 * @model abstract="true"
 * @generated
 */
public interface AbstractBusMessage extends AbstractMessage {
    /**
     * Returns the value of the '<em><b>Bus Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Bus Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Bus Id</em>' attribute.
     * @see #setBusId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractBusMessage_BusId()
     * @model dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getBusId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusId <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Bus Id</em>' attribute.
     * @see #getBusId()
     * @generated
     */
    void setBusId(String value);

    /**
     * Returns the value of the '<em><b>Bus Id Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Bus Id Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Bus Id Tmpl Param</em>' attribute.
     * @see #setBusIdTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractBusMessage_BusIdTmplParam()
     * @model
     * @generated
     */
    String getBusIdTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Bus Id Tmpl Param</em>' attribute.
     * @see #getBusIdTmplParam()
     * @generated
     */
    void setBusIdTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Bus Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Bus Id Range</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Bus Id Range</em>' attribute.
     * @see #setBusIdRange(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractBusMessage_BusIdRange()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getBusIdRange();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage#getBusIdRange <em>Bus Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Bus Id Range</em>' attribute.
     * @see #getBusIdRange()
     * @generated
     */
    void setBusIdRange(String value);

    /**
     * Returns the value of the '<em><b>Filters</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractFilter}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Filters</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Filters</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractBusMessage_Filters()
     * @model containment="true"
     * @generated
     */
    EList<AbstractFilter> getFilters();

    /**
     * @param filter
     * @return
     * @generated NOT
     */
    boolean addFilter(AbstractFilter filter);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_isOsiLayerConform(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    boolean isNeedsEthernet();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    boolean isNeedsErrorFrames();

} // AbstractBusMessage
