package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FlexRayCycleValidator implements ValueValidator {

    private String repetition;

    public FlexRayCycleValidator(String repetition) {
        this.repetition = repetition;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        String errorMessageIdentifier = null;

        try {
            int cycleOffsetInt = TemplateUtils.getIntNotPlaceholder(value, false);
            if(cycleOffsetInt < 0) {
                errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleOffset_IsLowerZero";
            }
            if(cycleOffsetInt > 63) {
                errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleOffset_IsGreaterSixtyThree";
            }

            int cycleRepetitionInt = TemplateUtils.getIntNotPlaceholder(repetition, false);
            if(cycleRepetitionInt <= cycleOffsetInt) {
                errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleOffset_MustBeLessThanRepetition";
            }
        } catch (NumberFormatException e) {
            errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleOffset_NotANumber";
        }

        if(errorMessageIdentifier != null) {
            if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier, value);
            }
            return false;
        }
        return true;
    }
}
