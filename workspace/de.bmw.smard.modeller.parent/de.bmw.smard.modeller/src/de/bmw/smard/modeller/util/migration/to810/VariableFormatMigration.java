package de.bmw.smard.modeller.util.migration.to810;

import java.util.ArrayList;
import java.util.List;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.migration.MigrationMarkerUtil;
import org.eclipse.emf.ecore.impl.EEnumLiteralImpl;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.FormatDataType;
import de.bmw.smard.modeller.conditions.VariableFormat;
import de.bmw.smard.modeller.conditions.util.VariableFormatConverter;
import de.bmw.smard.modeller.util.VariableFormats;

public class VariableFormatMigration extends CustomMigration {

    private static final String SIGNAL_VARIABLE = "conditions.SignalVariable";
    private static final String VALUE_VARIABLE = "conditions.ValueVariable";
    private static final String FORMAT = "format";
    private static final String VARIABLE_FORMAT = ConditionsPackage.eINSTANCE.getVariable_VariableFormat().getName();
    private static final String CONDITIONS_VARIABLE_FORMAT = "conditions.VariableFormat";
    private static final String DATATYPE = ConditionsPackage.eINSTANCE.getVariable_DataType().getName();
    private static final String DIGITS = ConditionsPackage.eINSTANCE.getVariableFormat_Digits().getName();
    private static final String BASE_DATA_TYPE = ConditionsPackage.eINSTANCE.getVariableFormat_BaseDataType().getName();
    private static final String UPPER_CASE = ConditionsPackage.eINSTANCE.getVariableFormat_UpperCase().getName();
    private static final String TEMPLATEPARAM = ConditionsPackage.eINSTANCE.getVariable_VariableFormatTmplParam().getName();

    @Override
    public void migrateBefore(Model model, Metamodel metamodel) {
    }

    @Override
    public void migrateAfter(Model model, Metamodel metamodel) {
        List<Instance> variables = new ArrayList<>();
        variables.addAll(model.getAllInstances(SIGNAL_VARIABLE));
        variables.addAll(model.getAllInstances(VALUE_VARIABLE));
        iterateVariables(variables, model);
    }

    private void iterateVariables(List<Instance> variables, Model model) {
        for (Instance var : variables) {
            EEnumLiteralImpl dataType = var.get(DATATYPE);            
            if (dataType == null || dataType.getLiteral().equals(DataType.STRING.getLiteral()))
                continue;
            String formatString = var.get(FORMAT);
            if (StringUtils.isNotBlank(formatString)) {
                Instance formatInstance = model.newInstance(CONDITIONS_VARIABLE_FORMAT);
                VariableFormat format = VariableFormatConverter.parseToFormat(formatString);
                setVariableFormat(var, formatInstance, format);
                if(format.getBaseDataType() == FormatDataType.TEMPLATEDEFINED) {
                    //Replace # because old tmpl-Param was written between # but new
                    //tmpl-param must not contain #
                    if(TemplateUtils.findParameters(formatString).size() > 1)
                        MigrationMarkerUtil.createWarning("There was more than one Template-Param for Format given. The Parameters will be converted to a single Parameter.");

                    String tmplParam = formatString.replaceAll("#", "");
                    var.set(TEMPLATEPARAM, tmplParam);
                }
            }
        }
    }

    private void setVariableFormat(Instance variable, Instance formatInstance, VariableFormat format) {
        formatInstance.set(BASE_DATA_TYPE, format.getBaseDataType());
        formatInstance.set(DIGITS, format.getDigits());
        formatInstance.set(UPPER_CASE, format.isUpperCase());
        variable.set(VARIABLE_FORMAT, formatInstance);
    }

}
