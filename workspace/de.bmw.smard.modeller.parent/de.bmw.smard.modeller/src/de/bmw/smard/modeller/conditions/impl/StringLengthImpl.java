package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.IStringOperand;
import de.bmw.smard.modeller.conditions.StringLength;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Length</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StringLengthImpl#getStringOperand <em>String Operand</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StringLengthImpl extends EObjectImpl implements StringLength {
    /**
     * The cached value of the '{@link #getStringOperand() <em>String Operand</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStringOperand()
     * @generated
     * @ordered
     */
    protected IStringOperand stringOperand;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StringLengthImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.STRING_LENGTH;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getStringOperand() {
        return stringOperand;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetStringOperand(IStringOperand newStringOperand, NotificationChain msgs) {
        IStringOperand oldStringOperand = stringOperand;
        stringOperand = newStringOperand;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_LENGTH__STRING_OPERAND, oldStringOperand, newStringOperand);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStringOperand(IStringOperand newStringOperand) {
        if (newStringOperand != stringOperand) {
            NotificationChain msgs = null;
            if (stringOperand != null)
                msgs = ((InternalEObject) stringOperand).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.STRING_LENGTH__STRING_OPERAND, null,
                        msgs);
            if (newStringOperand != null)
                msgs = ((InternalEObject) newStringOperand).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.STRING_LENGTH__STRING_OPERAND, null,
                        msgs);
            msgs = basicSetStringOperand(newStringOperand, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STRING_LENGTH__STRING_OPERAND, newStringOperand, newStringOperand));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        return DataType.STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.add(getStringOperand());
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        return ECollections.emptyEList();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        return ECollections.emptyEList();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
                return basicSetStringOperand(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
                return getStringOperand();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
                setStringOperand((IStringOperand) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
                setStringOperand((IStringOperand) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STRING_LENGTH__STRING_OPERAND:
                return stringOperand != null;
        }
        return super.eIsSet(featureID);
    }

} //StringLengthImpl
