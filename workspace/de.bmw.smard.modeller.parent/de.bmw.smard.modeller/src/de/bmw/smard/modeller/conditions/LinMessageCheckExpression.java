package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lin Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getType <em>Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getMessageIDs <em>Message IDs</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression()
 * @model extendedMetaData="kind='empty'"
 * @generated
 */
public interface LinMessageCheckExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Busid</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * WARNING: The bus id is filled by a special popup editor. The template parameter name is entered separately.
     * The value used here to indicate that a template parameter should be used is the value of {@link TemplateUtils#TEMPLATE_DEFINED_PLACEHOLDER_NAME}.
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Busid</em>' attribute.
     * @see #isSetBusid()
     * @see #unsetBusid()
     * @see #setBusid(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression_Busid()
     * @model default="0" unsettable="true" dataType="de.bmw.smard.modeller.conditions.LongOrTemplatePlaceholder" required="true"
     *        extendedMetaData="kind='attribute' name='busid' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getBusid();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Busid</em>' attribute.
     * @see #isSetBusid()
     * @see #unsetBusid()
     * @see #getBusid()
     * @generated
     */
    void setBusid(String value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetBusid()
     * @see #getBusid()
     * @see #setBusid(String)
     * @generated
     */
    void unsetBusid();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusid <em>Busid</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Busid</em>' attribute is set.
     * @see #unsetBusid()
     * @see #getBusid()
     * @see #setBusid(String)
     * @generated
     */
    boolean isSetBusid();

    /**
     * Returns the value of the '<em><b>Type</b></em>' attribute.
     * The default value is <code>"ANY"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CheckType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #setType(CheckType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression_Type()
     * @model default="ANY" required="true"
     *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    CheckType getType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getType <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #getType()
     * @generated
     */
    void setType(CheckType value);

    /**
     * Returns the value of the '<em><b>Message IDs</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message IDs</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message IDs</em>' attribute.
     * @see #setMessageIDs(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression_MessageIDs()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIDs();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getMessageIDs <em>Message IDs</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message IDs</em>' attribute.
     * @see #getMessageIDs()
     * @generated
     */
    void setMessageIDs(String value);

    /**
     * Returns the value of the '<em><b>Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type Tmpl Param</em>' attribute.
     * @see #setTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression_TypeTmplParam()
     * @model
     * @generated
     */
    String getTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type Tmpl Param</em>' attribute.
     * @see #getTypeTmplParam()
     * @generated
     */
    void setTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Busid Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Busid Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Busid Tmpl Param</em>' attribute.
     * @see #setBusidTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLinMessageCheckExpression_BusidTmplParam()
     * @model
     * @generated
     */
    String getBusidTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Busid Tmpl Param</em>' attribute.
     * @see #getBusidTmplParam()
     * @generated
     */
    void setBusidTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context);

} // LinMessageCheckExpression
