package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CAN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getMessageIdRange <em>Message Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameId <em>Frame Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlag <em>Rxtx Flag</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlagTmplParam <em>Rxtx Flag Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifier <em>Ext Identifier</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameType <em>Frame Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameTypeTmplParam <em>Frame Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter()
 * @model
 * @generated
 */
public interface CANFilter extends AbstractFilter {

    /**
     * Returns the value of the '<em><b>Message Id Range</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message Id Range</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message Id Range</em>' attribute.
     * @see #setMessageIdRange(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_MessageIdRange()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIdRange();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getMessageIdRange <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message Id Range</em>' attribute.
     * @see #getMessageIdRange()
     * @generated
     */
    void setMessageIdRange(String value);

    /**
     * Returns the value of the '<em><b>Frame Id</b></em>' attribute.
     * The default value is <code>"0x0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Frame Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Frame Id</em>' attribute.
     * @see #setFrameId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_FrameId()
     * @model default="0x0" dataType="de.bmw.smard.modeller.conditions.HexOrIntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getFrameId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameId <em>Frame Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Frame Id</em>' attribute.
     * @see #getFrameId()
     * @generated
     */
    void setFrameId(String value);

    /**
     * Returns the value of the '<em><b>Rxtx Flag</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Rxtx Flag</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Rxtx Flag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @see #setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_RxtxFlag()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    RxTxFlagTypeOrTemplatePlaceholderEnum getRxtxFlag();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlag <em>Rxtx Flag</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Rxtx Flag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @see #getRxtxFlag()
     * @generated
     */
    void setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Rxtx Flag Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Rxtx Flag Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Rxtx Flag Tmpl Param</em>' attribute.
     * @see #setRxtxFlagTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_RxtxFlagTmplParam()
     * @model
     * @generated
     */
    String getRxtxFlagTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getRxtxFlagTmplParam <em>Rxtx Flag Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Rxtx Flag Tmpl Param</em>' attribute.
     * @see #getRxtxFlagTmplParam()
     * @generated
     */
    void setRxtxFlagTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Ext Identifier</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ext Identifier</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ext Identifier</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @see #setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_ExtIdentifier()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    CanExtIdentifierOrTemplatePlaceholderEnum getExtIdentifier();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifier <em>Ext Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ext Identifier</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @see #getExtIdentifier()
     * @generated
     */
    void setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ext Identifier Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ext Identifier Tmpl Param</em>' attribute.
     * @see #setExtIdentifierTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_ExtIdentifierTmplParam()
     * @model
     * @generated
     */
    String getExtIdentifierTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ext Identifier Tmpl Param</em>' attribute.
     * @see #getExtIdentifierTmplParam()
     * @generated
     */
    void setExtIdentifierTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Frame Type</b></em>' attribute.
     * The default value is <code>"Standard"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Frame Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Frame Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum
     * @see #setFrameType(CANFrameTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_FrameType()
     * @model default="Standard"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    CANFrameTypeOrTemplatePlaceholderEnum getFrameType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameType <em>Frame Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Frame Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CANFrameTypeOrTemplatePlaceholderEnum
     * @see #getFrameType()
     * @generated
     */
    void setFrameType(CANFrameTypeOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Frame Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Frame Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Frame Type Tmpl Param</em>' attribute.
     * @see #setFrameTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFilter_FrameTypeTmplParam()
     * @model
     * @generated
     */
    String getFrameTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CANFilter#getFrameTypeTmplParam <em>Frame Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Frame Type Tmpl Param</em>' attribute.
     * @see #getFrameTypeTmplParam()
     * @generated
     */
    void setFrameTypeTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidRxTxFlag(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidExtIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context);
} // CANFilter
