package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.common.data.ObserverValueRangeHelper;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.ObserverValueRange;
import de.bmw.smard.modeller.conditions.ValueVariableObserver;
import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import java.util.ArrayList;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Value Variable Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ValueVariableObserverImpl extends AbstractObserverImpl implements ValueVariableObserver {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ValueVariableObserverImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VALUE_VARIABLE_OBSERVER;
    }

    @Override
    /** @generated NOT */
    public boolean isValid_hasValidValueRanges(EList<String> errorMessages, EList<String> warningMessages) {

        // only validate normal and generated conditions files
        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            boolean assumeIntegers = false;
            double rangeConversionFactor = 1.0;
            double rangeConversionOffset = 0.0;

            List<String> rangesAsStrings = new ArrayList<String>();
            if (valueRanges != null) {
                for (ObserverValueRange obsValRange : valueRanges) {
                    if (obsValRange != null) {
                        rangesAsStrings.add(obsValRange.getValue());
                    }
                }
            }
            if (!rangesAsStrings.isEmpty()) {
                ObserverValueRangeHelper.validateValueRangeTexts(rangesAsStrings, rangeConversionFactor,
                        rangeConversionOffset, assumeIntegers, errorMessages, warningMessages);
            }
        }

        // for the purpose of reducing the value range gap warnings, this method is called by VariableSetImpl
        // which then works with the error- and warningMessages
        return errorMessages.isEmpty() && warningMessages.isEmpty();
    }

} // ValueVariableObserverImpl
