package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.base.exception.SmardRuntimeException;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.Transition;
import org.eclipse.emf.ecore.EObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CyclicDependencyException extends SmardRuntimeException {

    private Object causeForCyclicDependency;
    /**
     * cyclic dependency between state machines
     * @param dependentStateMachine2PriorStateMachines
     */
    public CyclicDependencyException(
            Map<StateMachine, Set<StateMachine>> dependentStateMachine2PriorStateMachines) {
        super(MessageFormat.format("CyclicDependencyException {0}", dependentStateMachine2PriorStateMachines));
        this.causeForCyclicDependency = dependentStateMachine2PriorStateMachines;
    }

    /**
     * returns a list of names of the dependent state machines
     */
    @Override
    public String getMessage() {
        if(this.causeForCyclicDependency != null) {
        	List<String> dependentStateMachines = new ArrayList<String>();
        	
            if(this.causeForCyclicDependency instanceof Map) {
                for(Object stateMachine : ((Map) this.causeForCyclicDependency).keySet()) {
                    dependentStateMachines.add(((StateMachine) stateMachine).getName());
                }
            } else if(this.causeForCyclicDependency instanceof AbstractState) {
            	EObject parent = ((AbstractState) this.causeForCyclicDependency).eContainer();
            	if(parent instanceof StateMachine) {
            		dependentStateMachines.add(((StateMachine) parent).getName());
            	}
            } else if(this.causeForCyclicDependency instanceof Transition) {
            	EObject parent = ((Transition) this.causeForCyclicDependency).eContainer();
            	if(parent instanceof StateMachine) {
            		dependentStateMachines.add(((StateMachine) parent).getName());
            	}
            }
            
            if(dependentStateMachines.size() > 0) {
            	String result = String.join(",", dependentStateMachines);
            	return "[" + result + "]";
            } else {
            	return super.getMessage();
            }
        } else {
            return super.getMessage();
        }
    }
    
    public String getCauseForCyclicDependency() {
    	if(this.causeForCyclicDependency == null) {
    		return "";
    	} else if(this.causeForCyclicDependency instanceof AbstractState) {
    		return "caused by State: " + ((AbstractState) this.causeForCyclicDependency).getName();
    	} else if(this.causeForCyclicDependency instanceof Transition) {
    		return "caused by Transition: " + ((Transition) this.causeForCyclicDependency).getName();
    	} else if(this.causeForCyclicDependency instanceof Map) {
    		String result = "";
 
    		Map<StateMachine, Set<StateMachine>> stateMachine2DependentStateMachines = 
    				(Map<StateMachine, Set<StateMachine>>) this.causeForCyclicDependency;
    		List<String> concernedReadVariables = new ArrayList<String>();
    		
    		for(StateMachine stateMachine : stateMachine2DependentStateMachines.keySet()) {
    			List<String> readVariables = stateMachine.getReadVariables();
    			
    			Set<StateMachine> dependentStateMachines = 
    					stateMachine2DependentStateMachines.get(stateMachine);
    			for(StateMachine dependentStateMachine : dependentStateMachines) {
    				
    				// get the write variables and check, if statemachine writes one of the read variables
    				// this is therefore the cause for the cyclic dependency
    				List<String> writeVariables = dependentStateMachine.getWriteVariables();
    				for(String writeVariable : writeVariables) {
    					if(readVariables.contains(writeVariable)) {
    						if(!concernedReadVariables.contains(writeVariable)) {
    							concernedReadVariables.add(writeVariable);
    						}
    					}
    				}
    			}
    		}
    		result = String.join(",", concernedReadVariables);
    		return "caused by following variables [" + result + "]";
    	} 
    	return null;
    }
}