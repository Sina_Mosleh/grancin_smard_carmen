package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.Extract;
import de.bmw.smard.modeller.conditions.IComputeVariableActionOperand;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand;
import de.bmw.smard.modeller.conditions.IStringOperand;
import de.bmw.smard.modeller.conditions.RegexOperation;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extract</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl#getRegex <em>Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl#getDynamicRegex <em>Dynamic Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl#getStringToExtract <em>String To Extract</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ExtractImpl#getGroupIndex <em>Group Index</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExtractImpl extends EObjectImpl implements Extract {
    /**
     * The default value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected static final String REGEX_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected String regex = REGEX_EDEFAULT;

    /**
     * The cached value of the '{@link #getDynamicRegex() <em>Dynamic Regex</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDynamicRegex()
     * @generated
     * @ordered
     */
    protected IStringOperand dynamicRegex;

    /**
     * The cached value of the '{@link #getStringToExtract() <em>String To Extract</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStringToExtract()
     * @generated
     * @ordered
     */
    protected IStringOperand stringToExtract;

    /**
     * The default value of the '{@link #getGroupIndex() <em>Group Index</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getGroupIndex()
     * @generated
     * @ordered
     */
    protected static final int GROUP_INDEX_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getGroupIndex() <em>Group Index</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getGroupIndex()
     * @generated
     * @ordered
     */
    protected int groupIndex = GROUP_INDEX_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ExtractImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.EXTRACT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getStringToExtract() {
        return stringToExtract;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetStringToExtract(IStringOperand newStringToExtract, NotificationChain msgs) {
        IStringOperand oldStringToExtract = stringToExtract;
        stringToExtract = newStringToExtract;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__STRING_TO_EXTRACT, oldStringToExtract, newStringToExtract);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStringToExtract(IStringOperand newStringToExtract) {
        if (newStringToExtract != stringToExtract) {
            NotificationChain msgs = null;
            if (stringToExtract != null)
                msgs = ((InternalEObject) stringToExtract).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.EXTRACT__STRING_TO_EXTRACT, null,
                        msgs);
            if (newStringToExtract != null)
                msgs = ((InternalEObject) newStringToExtract).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.EXTRACT__STRING_TO_EXTRACT, null,
                        msgs);
            msgs = basicSetStringToExtract(newStringToExtract, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__STRING_TO_EXTRACT, newStringToExtract, newStringToExtract));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getRegex() {
        return regex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setRegex(String newRegex) {
        String oldRegex = regex;
        regex = newRegex;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__REGEX, oldRegex, regex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getGroupIndex() {
        return groupIndex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setGroupIndex(int newGroupIndex) {
        int oldGroupIndex = groupIndex;
        groupIndex = newGroupIndex;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__GROUP_INDEX, oldGroupIndex, groupIndex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getDynamicRegex() {
        return dynamicRegex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetDynamicRegex(IStringOperand newDynamicRegex, NotificationChain msgs) {
        IStringOperand oldDynamicRegex = dynamicRegex;
        dynamicRegex = newDynamicRegex;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__DYNAMIC_REGEX, oldDynamicRegex, newDynamicRegex);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDynamicRegex(IStringOperand newDynamicRegex) {
        if (newDynamicRegex != dynamicRegex) {
            NotificationChain msgs = null;
            if (dynamicRegex != null)
                msgs = ((InternalEObject) dynamicRegex).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.EXTRACT__DYNAMIC_REGEX, null, msgs);
            if (newDynamicRegex != null)
                msgs = ((InternalEObject) newDynamicRegex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.EXTRACT__DYNAMIC_REGEX, null, msgs);
            msgs = basicSetDynamicRegex(newDynamicRegex, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.EXTRACT__DYNAMIC_REGEX, newDynamicRegex, newDynamicRegex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.REGEX_OPERATION__IS_VALID_HAS_VALID_REGEX)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.regex()
                        .withRegexValues(this.regex, this.dynamicRegex)
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        return DataType.STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.add(getStringToExtract());
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        return DataType.STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        if (getStringToExtract() == null) {
            return ECollections.emptyEList();
        }
        return getStringToExtract().getReadVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        if (getStringToExtract() == null) {
            return ECollections.emptyEList();
        }
        return getStringToExtract().getWriteVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                return basicSetDynamicRegex(null, msgs);
            case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
                return basicSetStringToExtract(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT__REGEX:
                return getRegex();
            case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                return getDynamicRegex();
            case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
                return getStringToExtract();
            case ConditionsPackage.EXTRACT__GROUP_INDEX:
                return getGroupIndex();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT__REGEX:
                setRegex((String) newValue);
                return;
            case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                setDynamicRegex((IStringOperand) newValue);
                return;
            case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
                setStringToExtract((IStringOperand) newValue);
                return;
            case ConditionsPackage.EXTRACT__GROUP_INDEX:
                setGroupIndex((Integer) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT__REGEX:
                setRegex(REGEX_EDEFAULT);
                return;
            case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                setDynamicRegex((IStringOperand) null);
                return;
            case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
                setStringToExtract((IStringOperand) null);
                return;
            case ConditionsPackage.EXTRACT__GROUP_INDEX:
                setGroupIndex(GROUP_INDEX_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.EXTRACT__REGEX:
                return REGEX_EDEFAULT == null ? regex != null : !REGEX_EDEFAULT.equals(regex);
            case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                return dynamicRegex != null;
            case ConditionsPackage.EXTRACT__STRING_TO_EXTRACT:
                return stringToExtract != null;
            case ConditionsPackage.EXTRACT__GROUP_INDEX:
                return groupIndex != GROUP_INDEX_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == BusMessageReferable.class) {
            switch (derivedFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == ISignalComparisonExpressionOperand.class) {
            switch (derivedFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == IComputeVariableActionOperand.class) {
            switch (derivedFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == IStringOperand.class) {
            switch (derivedFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == RegexOperation.class) {
            switch (derivedFeatureID) {
                case ConditionsPackage.EXTRACT__REGEX:
                    return ConditionsPackage.REGEX_OPERATION__REGEX;
                case ConditionsPackage.EXTRACT__DYNAMIC_REGEX:
                    return ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX;
                default:
                    return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == BusMessageReferable.class) {
            switch (baseFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == ISignalComparisonExpressionOperand.class) {
            switch (baseFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == IComputeVariableActionOperand.class) {
            switch (baseFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == IStringOperand.class) {
            switch (baseFeatureID) {
                default:
                    return -1;
            }
        }
        if (baseClass == RegexOperation.class) {
            switch (baseFeatureID) {
                case ConditionsPackage.REGEX_OPERATION__REGEX:
                    return ConditionsPackage.EXTRACT__REGEX;
                case ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX:
                    return ConditionsPackage.EXTRACT__DYNAMIC_REGEX;
                default:
                    return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (regex: ");
        result.append(regex);
        result.append(", groupIndex: ");
        result.append(groupIndex);
        result.append(')');
        return result.toString();
    }

} //ExtractImpl
