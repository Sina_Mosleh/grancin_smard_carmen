package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getSignalReferencesSet <em>Signal References Set</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getVariableSet <em>Variable Set</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getConditionSet <em>Condition Set</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getConditionsDocument()
 * @model extendedMetaData="name='conditionsDocument'"
 * @generated
 */
public interface ConditionsDocument extends EObject {
    /**
     * Returns the value of the '<em><b>Signal References Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal References Set</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal References Set</em>' containment reference.
     * @see #setSignalReferencesSet(SignalReferenceSet)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getConditionsDocument_SignalReferencesSet()
     * @model containment="true"
     * @generated
     */
    SignalReferenceSet getSignalReferencesSet();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getSignalReferencesSet <em>Signal References Set</em>}' containment
     * reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal References Set</em>' containment reference.
     * @see #getSignalReferencesSet()
     * @generated
     */
    void setSignalReferencesSet(SignalReferenceSet value);

    /**
     * Returns the value of the '<em><b>Variable Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Variable Set</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Variable Set</em>' containment reference.
     * @see #setVariableSet(VariableSet)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getConditionsDocument_VariableSet()
     * @model containment="true"
     * @generated
     */
    VariableSet getVariableSet();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getVariableSet <em>Variable Set</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Variable Set</em>' containment reference.
     * @see #getVariableSet()
     * @generated
     */
    void setVariableSet(VariableSet value);

    /**
     * Returns the value of the '<em><b>Condition Set</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Condition Set</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Condition Set</em>' containment reference.
     * @see #setConditionSet(ConditionSet)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getConditionsDocument_ConditionSet()
     * @model containment="true"
     * @generated
     */
    ConditionSet getConditionSet();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.ConditionsDocument#getConditionSet <em>Condition Set</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Condition Set</em>' containment reference.
     * @see #getConditionSet()
     * @generated
     */
    void setConditionSet(ConditionSet value);

} // ConditionsDocument
