package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.RangeIdValidator;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.IStringOperand;

import org.eclipse.emf.common.util.Enumerator;


public class Validators {

    private Validators() {
    }

    public static IntListOrRangeValidator.NameStep intListOrRange(String value) {
        return IntListOrRangeValidator.builder(value);
    }

    public static NotNullValidator.ErrorStep notNull(Object value) {
        return NotNullValidator.builder(value);
    }
    
    public static RegexValidator.RegexValuesStep regex() {
    	return RegexValidator.builder();
    }

    public static PredicateValidator.AndStep when(boolean test) {
        return new PredicateValidator.Builder(test);
    }

    public static TemplateAttributeValidator.TmplParamStep<StringTemplateAttributeValidator.IllegalValueErrorStep> checkTemplate(String value) {
        return StringTemplateAttributeValidator.builder(value);
    }

    public static TemplateAttributeValidator.TemplateTypeStep<EnumTemplateAttributeValidator.IllegalValueErrorStep> checkTemplate(Enumerator value) {
        return EnumTemplateAttributeValidator.builder(value);
    }

    public static TemplateAttributeValidator.TemplateTypeStep<BooleanTemplateAttributeValidator.IllegalValueErrorStep> checkTemplate(BooleanOrTemplatePlaceholderEnum value) {
        return BooleanTemplateAttributeValidator.builder(value);
    }

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> stringTemplate(String value) {
        return StringTemplateValidator.builder(value);
    }

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> intTemplate(String value) {
        return IntTemplateValidator.builder(value);
    }

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> numberTemplate(String value) {
        return NumberTemplateValidator.builder(value);
    }

    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> doubleTemplate(String value) {
        return DoubleTemplateValidator.builder(value);
    }
    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> byteTemplate(String value) {
    	return ByteTemplateValidator.builder(value);
    }
    
    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> rangeIdTemplate(String value) {
        return RangeIdTemplateValidator.builder(value, RangeIdValidator::validate);
    }


    public static TemplateValidator.ContainsParameterErrorStep<ValueTemplateValidator.IllegalValueErrorStep> rangeIdDoubleTemplate(String value) {
        return RangeIdTemplateValidator.builder(value, RangeIdValidator::validateDouble);
    }
}
