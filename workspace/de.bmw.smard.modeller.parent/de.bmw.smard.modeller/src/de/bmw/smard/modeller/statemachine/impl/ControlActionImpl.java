package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.statemachine.ControlAction;
import de.bmw.smard.modeller.statemachine.ControlType;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Control Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ControlActionImpl#getControlType <em>Control Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ControlActionImpl#getStateMachines <em>State Machines</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ControlActionImpl#getObservers <em>Observers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControlActionImpl extends AbstractActionImpl implements ControlAction {
    /**
     * The default value of the '{@link #getControlType() <em>Control Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getControlType()
     * @generated
     * @ordered
     */
    protected static final ControlType CONTROL_TYPE_EDEFAULT = ControlType.ON;

    /**
     * The cached value of the '{@link #getControlType() <em>Control Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getControlType()
     * @generated
     * @ordered
     */
    protected ControlType controlType = CONTROL_TYPE_EDEFAULT;

    /**
     * The cached value of the '{@link #getStateMachines() <em>State Machines</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStateMachines()
     * @generated
     * @ordered
     */
    protected EList<StateMachine> stateMachines;

    /**
     * The cached value of the '{@link #getObservers() <em>Observers</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getObservers()
     * @generated
     * @ordered
     */
    protected EList<AbstractObserver> observers;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ControlActionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.CONTROL_ACTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ControlType getControlType() {
        return controlType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setControlType(ControlType newControlType) {
        ControlType oldControlType = controlType;
        controlType = newControlType == null ? CONTROL_TYPE_EDEFAULT : newControlType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.CONTROL_ACTION__CONTROL_TYPE, oldControlType, controlType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<StateMachine> getStateMachines() {
        if (stateMachines == null) {
            stateMachines = new EObjectResolvingEList<StateMachine>(StateMachine.class, this, StatemachinePackage.CONTROL_ACTION__STATE_MACHINES);
        }
        return stateMachines;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractObserver> getObservers() {
        if (observers == null) {
            observers = new EObjectResolvingEList<AbstractObserver>(AbstractObserver.class, this, StatemachinePackage.CONTROL_ACTION__OBSERVERS);
        }
        return observers;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.CONTROL_ACTION__CONTROL_TYPE:
                return getControlType();
            case StatemachinePackage.CONTROL_ACTION__STATE_MACHINES:
                return getStateMachines();
            case StatemachinePackage.CONTROL_ACTION__OBSERVERS:
                return getObservers();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.CONTROL_ACTION__CONTROL_TYPE:
                setControlType((ControlType) newValue);
                return;
            case StatemachinePackage.CONTROL_ACTION__STATE_MACHINES:
                getStateMachines().clear();
                getStateMachines().addAll((Collection<? extends StateMachine>) newValue);
                return;
            case StatemachinePackage.CONTROL_ACTION__OBSERVERS:
                getObservers().clear();
                getObservers().addAll((Collection<? extends AbstractObserver>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.CONTROL_ACTION__CONTROL_TYPE:
                setControlType(CONTROL_TYPE_EDEFAULT);
                return;
            case StatemachinePackage.CONTROL_ACTION__STATE_MACHINES:
                getStateMachines().clear();
                return;
            case StatemachinePackage.CONTROL_ACTION__OBSERVERS:
                getObservers().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.CONTROL_ACTION__CONTROL_TYPE:
                return controlType != CONTROL_TYPE_EDEFAULT;
            case StatemachinePackage.CONTROL_ACTION__STATE_MACHINES:
                return stateMachines != null && !stateMachines.isEmpty();
            case StatemachinePackage.CONTROL_ACTION__OBSERVERS:
                return observers != null && !observers.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (controlType: ");
        result.append(controlType);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        return ECollections.emptyEList();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        return ECollections.emptyEList();
    }

} //ControlActionImpl
