package de.bmw.smard.modeller.conditions.impl;

import com.google.common.collect.Lists;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.FlexChannelType;
import de.bmw.smard.modeller.conditions.FlexRayFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.BaseValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flex Ray Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getMessageIdRange <em>Message Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getSlotId <em>Slot Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getCycleOffset <em>Cycle Offset</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getCycleRepetition <em>Cycle Repetition</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getChannel <em>Channel</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayFilterImpl#getChannelTmplParam <em>Channel Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlexRayFilterImpl extends AbstractFilterImpl implements FlexRayFilter {
    /**
     * The default value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_ID_RANGE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected String messageIdRange = MESSAGE_ID_RANGE_EDEFAULT;

    /**
     * The default value of the '{@link #getSlotId() <em>Slot Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSlotId()
     * @generated
     * @ordered
     */
    protected static final String SLOT_ID_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getSlotId() <em>Slot Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSlotId()
     * @generated
     * @ordered
     */
    protected String slotId = SLOT_ID_EDEFAULT;

    /**
     * The default value of the '{@link #getCycleOffset() <em>Cycle Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCycleOffset()
     * @generated
     * @ordered
     */
    protected static final String CYCLE_OFFSET_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getCycleOffset() <em>Cycle Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCycleOffset()
     * @generated
     * @ordered
     */
    protected String cycleOffset = CYCLE_OFFSET_EDEFAULT;

    /**
     * The default value of the '{@link #getCycleRepetition() <em>Cycle Repetition</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCycleRepetition()
     * @generated
     * @ordered
     */
    protected static final String CYCLE_REPETITION_EDEFAULT = "1";

    /**
     * The cached value of the '{@link #getCycleRepetition() <em>Cycle Repetition</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCycleRepetition()
     * @generated
     * @ordered
     */
    protected String cycleRepetition = CYCLE_REPETITION_EDEFAULT;

    /**
     * The default value of the '{@link #getChannel() <em>Channel</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChannel()
     * @generated
     * @ordered
     */
    protected static final FlexChannelType CHANNEL_EDEFAULT = FlexChannelType.A;

    /**
     * The cached value of the '{@link #getChannel() <em>Channel</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChannel()
     * @generated
     * @ordered
     */
    protected FlexChannelType channel = CHANNEL_EDEFAULT;

    /**
     * The default value of the '{@link #getChannelTmplParam() <em>Channel Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChannelTmplParam()
     * @generated
     * @ordered
     */
    protected static final String CHANNEL_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getChannelTmplParam() <em>Channel Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getChannelTmplParam()
     * @generated
     * @ordered
     */
    protected String channelTmplParam = CHANNEL_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FlexRayFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.FLEX_RAY_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageIdRange() {
        return messageIdRange;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageIdRange(String newMessageIdRange) {
        String oldMessageIdRange = messageIdRange;
        messageIdRange = newMessageIdRange;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__MESSAGE_ID_RANGE, oldMessageIdRange, messageIdRange));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSlotId() {
        return slotId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSlotId(String newSlotId) {
        String oldSlotId = slotId;
        slotId = newSlotId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__SLOT_ID, oldSlotId, slotId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCycleOffset() {
        return cycleOffset;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCycleOffset(String newCycleOffset) {
        String oldCycleOffset = cycleOffset;
        cycleOffset = newCycleOffset;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__CYCLE_OFFSET, oldCycleOffset, cycleOffset));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCycleRepetition() {
        return cycleRepetition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCycleRepetition(String newCycleRepetition) {
        String oldCycleRepetition = cycleRepetition;
        cycleRepetition = newCycleRepetition;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__CYCLE_REPETITION, oldCycleRepetition, cycleRepetition));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexChannelType getChannel() {
        return channel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setChannel(FlexChannelType newChannel) {
        FlexChannelType oldChannel = channel;
        channel = newChannel == null ? CHANNEL_EDEFAULT : newChannel;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__CHANNEL, oldChannel, channel));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getChannelTmplParam() {
        return channelTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setChannelTmplParam(String newChannelTmplParam) {
        String oldChannelTmplParam = channelTmplParam;
        channelTmplParam = newChannelTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM, oldChannelTmplParam, channelTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {

        BaseValidator.WithStep validator = Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE)
                .object(eContainer)
                .diagnostic(diagnostics);

        if (StringUtils.isNotBlank(messageIdRange)) {
            validator.with(Validators.stringTemplate(messageIdRange)
                    .containsParameterError("_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_ContainsPlaceholders")
                    .invalidPlaceholderError("_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.flexRayFrameIdRange())
                    .build());
        } else {
            validator.with(Validators.stringTemplate(slotId)
                    .containsParameterError("_Validation_Conditions_CANLINFlexRayFilter_FrameId_ContainsPlaceholders")
                    .invalidPlaceholderError("_Validation_Conditions_CANLINFlexRayFilter_FrameId_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.flexRaySlotId())
                    .build())

                    .with(Validators.notNull(slotId)
                            .error("_Validation_Conditions_FlexRayFilter_SlotId_MustNotBeNull")
                            .build());

        }

        return validator.validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidChannelType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        if (StringUtils.isBlank(messageIdRange)) {

            List<FlexChannelType> legalChannelTypes = Lists.newArrayList(
                    FlexChannelType.BOTH,
                    FlexChannelType.A,
                    FlexChannelType.B);

            return Validator.builder()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CHANNEL_TYPE)
                    .object(this)
                    .diagnostic(diagnostics)

                    .with(Validators.checkTemplate(channel)
                            .templateType(FlexChannelType.TEMPLATE_DEFINED)
                            .tmplParam(channelTmplParam)
                            .containsParameterError("_Validation_Conditions_FlexRayFilter_ChannelType_ContainsParameters")
                            .tmplParamIsNullError("_Validation_Conditions_FlexRayFilter_ChannelTypeTmplParam_IsNull")
                            .noPlaceholderError("_Validation_Conditions_FlexRayFilter_ChannelTypeTmplParam_NotAValidPlaceholderName")
                            .illegalValueError(legalChannelTypes, "_Validation_Conditions_FlexRayFilter_ChannelType_IllegalChannelType")
                            .build())

                    .with(Validators.notNull(channel)
                            .error("_Validation_Conditions_FlexRayFilter_ChannelType_IsNull")
                            .build())

                    .validate();
        }

        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidCycleOffset(DiagnosticChain diagnostics, Map<Object, Object> context) {

        if (StringUtils.isBlank(messageIdRange)) {
            return Validator.builder()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CYCLE_OFFSET)
                    .object(this)
                    .diagnostic(diagnostics)

                    .with(Validators.intTemplate(cycleOffset)
                            .containsParameterError("_Validation_Conditions_FlexRayFilter_CycleOffset_IllegalCycleOffset")
                            .invalidPlaceholderError("_Validation_Conditions_FlexRayFilter_CycleOffset_NotAValidPlaceholderName")
                            .illegalValueError(ValueValidators.flexRayCycle(cycleRepetition))
                            .build())

                    // if messageIdRange is null, repetition and offset must not be null
                    .with(Validators
                            .notNull(cycleOffset)
                            .error("_Validation_Conditions_FlexRayFilter_CycleOffset_MustNotBeNull")
                            .build())

                    .validate();
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidCycleRepeatInterval(DiagnosticChain diagnostics, Map<Object, Object> context) {

        if (StringUtils.isBlank(messageIdRange)) {
            return Validator.builder()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.FLEX_RAY_FILTER__IS_VALID_HAS_VALID_CYCLE_OFFSET)
                    .object(this)
                    .diagnostic(diagnostics)

                    .with(Validators.intTemplate(cycleRepetition)
                            .containsParameterError("_Validation_Conditions_FlexRayFilter_CycleRepeatInterval_IllegalCycleRepeatInterval")
                            .invalidPlaceholderError("_Validation_Conditions_FlexRayFilter_CycleRepeatInterval_NotAValidPlaceholderName")
                            .illegalValueError(ValueValidators.flexRayRepetition())
                            .build())

                    // if messageIdRange is null, repetition and offset must not be null
                    .with(Validators
                            .notNull(cycleRepetition)
                            .error("_Validation_Conditions_FlexRayFilter_CycleRepeatInterval_MustNotBeNull")
                            .build())

                    .validate();
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_FILTER__MESSAGE_ID_RANGE:
                return getMessageIdRange();
            case ConditionsPackage.FLEX_RAY_FILTER__SLOT_ID:
                return getSlotId();
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_OFFSET:
                return getCycleOffset();
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_REPETITION:
                return getCycleRepetition();
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL:
                return getChannel();
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM:
                return getChannelTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__SLOT_ID:
                setSlotId((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_OFFSET:
                setCycleOffset((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_REPETITION:
                setCycleRepetition((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL:
                setChannel((FlexChannelType) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM:
                setChannelTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange(MESSAGE_ID_RANGE_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__SLOT_ID:
                setSlotId(SLOT_ID_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_OFFSET:
                setCycleOffset(CYCLE_OFFSET_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_REPETITION:
                setCycleRepetition(CYCLE_REPETITION_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL:
                setChannel(CHANNEL_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM:
                setChannelTmplParam(CHANNEL_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_FILTER__MESSAGE_ID_RANGE:
                return MESSAGE_ID_RANGE_EDEFAULT == null ? messageIdRange != null : !MESSAGE_ID_RANGE_EDEFAULT.equals(messageIdRange);
            case ConditionsPackage.FLEX_RAY_FILTER__SLOT_ID:
                return SLOT_ID_EDEFAULT == null ? slotId != null : !SLOT_ID_EDEFAULT.equals(slotId);
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_OFFSET:
                return CYCLE_OFFSET_EDEFAULT == null ? cycleOffset != null : !CYCLE_OFFSET_EDEFAULT.equals(cycleOffset);
            case ConditionsPackage.FLEX_RAY_FILTER__CYCLE_REPETITION:
                return CYCLE_REPETITION_EDEFAULT == null ? cycleRepetition != null : !CYCLE_REPETITION_EDEFAULT.equals(cycleRepetition);
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL:
                return channel != CHANNEL_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_FILTER__CHANNEL_TMPL_PARAM:
                return CHANNEL_TMPL_PARAM_EDEFAULT == null ? channelTmplParam != null : !CHANNEL_TMPL_PARAM_EDEFAULT.equals(channelTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (messageIdRange: ");
        result.append(messageIdRange);
        result.append(", slotId: ");
        result.append(slotId);
        result.append(", cycleOffset: ");
        result.append(cycleOffset);
        result.append(", cycleRepetition: ");
        result.append(cycleRepetition);
        result.append(", channel: ");
        result.append(channel);
        result.append(", channelTmplParam: ");
        result.append(channelTmplParam);
        result.append(')');
        return result.toString();
    }

} //FlexRayFilterImpl
