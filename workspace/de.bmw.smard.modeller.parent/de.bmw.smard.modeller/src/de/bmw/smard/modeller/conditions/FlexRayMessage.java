package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flex Ray Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayMessage()
 * @model
 * @generated
 */
public interface FlexRayMessage extends AbstractBusMessage {
    /**
     * Returns the value of the '<em><b>Flex Ray Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Flex Ray Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Flex Ray Filter</em>' containment reference.
     * @see #setFlexRayFilter(FlexRayFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFlexRayMessage_FlexRayFilter()
     * @model containment="true" required="true"
     * @generated
     */
    FlexRayFilter getFlexRayFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.FlexRayMessage#getFlexRayFilter <em>Flex Ray Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Flex Ray Filter</em>' containment reference.
     * @see #getFlexRayFilter()
     * @generated
     */
    void setFlexRayFilter(FlexRayFilter value);

} // FlexRayMessage
