package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class BitPatternComparatorPatternValidator implements ValueValidator {

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		String errorMessageIdentifier = patternValid(value);
		if (errorMessageIdentifier != null && baseValidator != null) {
			baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
		}
		
		return errorMessageIdentifier == null;
	}

	private String patternValid(String pattern) {
		String errorMessageIdentifier = null;
		if (pattern != null) {
			if (pattern.length() > 54) {
				errorMessageIdentifier = "_Validation_Conditions_BitPatternComparatorValue_BitPattern_GreaterFiftyFour";
			} else if (!pattern.matches("[01X]*")) {
				errorMessageIdentifier = "_Validation_Conditions_BitPatternComparatorValue_BitPattern_ContainsInvalidNumbersOrCharacters";
			}
		}
		return errorMessageIdentifier;
	}
}
