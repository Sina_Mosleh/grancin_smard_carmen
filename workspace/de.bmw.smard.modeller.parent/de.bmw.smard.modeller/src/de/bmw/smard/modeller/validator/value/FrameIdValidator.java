package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.ModellerDataConversions;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FrameIdValidator implements ValueValidator {

    private static final String FRAME_ID_NULL = "_Validation_Conditions_CANLINFlexRayFilter_FrameId_IsNull";
    private static final String FRAME_ID_ZERO = "_Validation_Conditions_CANLINFlexRayFilter_FrameId_Zero";
    private static final String FRAME_ID_NOT_A_NUMBER = "_Validation_Conditions_CANLINFlexRayFilter_FrameId_NotANumber";

    private final boolean nullOrEmptyAllowed;

    public static FrameIdValidator lin() {
        return create(false);
    }

    public static FrameIdValidator flexRay() {
        return create(false);
    }

    public static FrameIdValidator can(boolean isErrorFrame) {
        // special case CANFilter -> if frameType is errorFrame, frameId can be zero
        // SMARD-736 if frameType is ERROR_FRAME every input for frameId is allowed
        return create(isErrorFrame);
    }

    private static FrameIdValidator create(boolean nullOrEmptyAllowed) {
        return new FrameIdValidator(nullOrEmptyAllowed);
    }

    private FrameIdValidator(boolean nullOrEmptyAllowed) {
        this.nullOrEmptyAllowed = nullOrEmptyAllowed;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        String errorId = null;

        if (!nullOrEmptyAllowed && StringUtils.isBlank(value)) {
            errorId = FRAME_ID_NULL;
        } else {
            try {
                Integer intFromString = ModellerDataConversions.getIntFromHexOrNumber(value);

                // SMARD-2473 frameId may not be zero
                if (!nullOrEmptyAllowed && intFromString != null && intFromString == 0) {
                    errorId = FRAME_ID_ZERO;
                }
            } catch (NumberFormatException ex) {
                errorId = FRAME_ID_NOT_A_NUMBER;
            }
        }

        if (errorId != null) {
            if (baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorId, value);
            }
            return false;
        }
        return true;
    }
}
