package de.bmw.smard.modeller.validator;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IStringOperand;

public class RegexValidator {
	
	private RegexValidator() {}
	
	static RegexValuesStep builder() {
		return new Builder();
	}

	public interface RegexValuesStep {
        Build withRegexValues(String regex, IStringOperand dynamicRegex);
    }

	public static class Builder extends Validator implements RegexValuesStep {

		private String regex;
		private IStringOperand dynamicRegex;

		Builder() {}

		@Override
		public Build withRegexValues(String regex, IStringOperand dynamicRegex) {
			this.regex = regex;
            this.dynamicRegex = dynamicRegex;
			return this;
		}

		@Override
		protected boolean validate(BaseValidator baseValidator) {
			String errorMessageIdentifier = null;
			String substitions = null;

			// if present, validate dynamicRegex, else validate textualRegex
			if(dynamicRegex != null) {
				// validate dynamic regex
				if(!(DataType.STRING == dynamicRegex.get_EvaluationDataType())) {
					errorMessageIdentifier = "_Validation_Conditions_RegexOperation_DynamicRegex_InvalidDataType";
				}
			} else {
				// validate textual regex
				if (StringUtils.nullOrEmpty(regex)) {
					errorMessageIdentifier = "_Validation_Conditions_RegexOperation_Regex_MustNotBeNull";
				} else {
					try {
						Pattern.compile(regex);
					} catch (PatternSyntaxException e) {
						errorMessageIdentifier = "_Validation_Conditions_RegexOperation_Regex_UnableToParse";
						substitions = e.getMessage();
					}
				}
			}

			if(errorMessageIdentifier != null) {
				attachError(baseValidator, errorMessageIdentifier, substitions);
				return false;
			}

			return true;
		}

	}

	/**
	 * validate Expression with a Variable as dynamicRegex and a String as regex
	 * if dynamicRegex is set, it overrules the textual regex
	 * @param dynamicRegex
	 * @param regex
	 * @return
	 */
	public boolean isValidRegex(IStringOperand dynamicRegex, String regex) {
		// if present, validate dynamicRegex, else validate textualRegex
		if(dynamicRegex != null) {
			// validate dynamic regex
			return DataType.STRING == dynamicRegex.get_EvaluationDataType();
		} else {
			// validate textual regex
			if (StringUtils.nullOrEmpty(regex)) {
				return false;
			}

			try {
				Pattern.compile(regex);
			} catch (PatternSyntaxException e) {
				return false;
			}
			return true;
		}
	}

}
