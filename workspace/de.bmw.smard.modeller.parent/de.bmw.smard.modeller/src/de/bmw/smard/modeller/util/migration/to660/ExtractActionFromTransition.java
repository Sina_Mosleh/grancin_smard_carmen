package de.bmw.smard.modeller.util.migration.to660;

import org.eclipse.emf.edapt.migration.CustomMigration;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;


public class ExtractActionFromTransition extends CustomMigration {
	
	private Map<Instance,Instance> transitions2Actions = new HashMap<Instance,Instance>();
	private EAttribute actionType;
	private EAttribute actionTypeTmplParam;
	private EAttribute actionExpression;
		
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel)
			throws MigrationException {

		actionType = metamodel.getEAttribute("statemachine.Transition.actionType");
		actionTypeTmplParam = metamodel.getEAttribute("statemachine.Transition.actionTypeTmplParam");
		actionExpression = metamodel.getEAttribute("statemachine.Transition.actionExpression");
		
		for (Instance transition : model.getAllInstances("statemachine.Transition")) {
			if (!(((EEnumLiteral)transition.get(actionType)).getName().equalsIgnoreCase("Empty"))) {
				Instance action = model.newInstance("statemachine.Action");
				action.set("actionType", (EEnumLiteral)transition.get(actionType));
				action.set("actionTypeTmplParam", transition.get(actionTypeTmplParam));
				action.set("actionExpression", transition.get(actionExpression));
				
				transitions2Actions.put(transition, action);
			}
		}
	}
	
	
	@Override
	public void migrateAfter(Model model, Metamodel metamodel)
			throws MigrationException {	
		for (Instance transition2beMigrated : transitions2Actions.keySet()) {
			transition2beMigrated.set("action", transitions2Actions.get(transition2beMigrated));
		}
	}


}
