package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.INumericOperand;
import de.bmw.smard.modeller.conditions.IOperand;
import de.bmw.smard.modeller.conditions.ParseNumericToString;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Parse Numeric To String</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ParseNumericToStringImpl#getNumericOperandToBeParsed <em>Numeric Operand To Be Parsed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ParseNumericToStringImpl extends EObjectImpl implements ParseNumericToString {
    /**
     * The cached value of the '{@link #getNumericOperandToBeParsed() <em>Numeric Operand To Be Parsed</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNumericOperandToBeParsed()
     * @generated
     * @ordered
     */
    protected INumericOperand numericOperandToBeParsed;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ParseNumericToStringImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PARSE_NUMERIC_TO_STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public INumericOperand getNumericOperandToBeParsed() {
        return numericOperandToBeParsed;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetNumericOperandToBeParsed(INumericOperand newNumericOperandToBeParsed, NotificationChain msgs) {
        INumericOperand oldNumericOperandToBeParsed = numericOperandToBeParsed;
        numericOperandToBeParsed = newNumericOperandToBeParsed;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, oldNumericOperandToBeParsed, newNumericOperandToBeParsed);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNumericOperandToBeParsed(INumericOperand newNumericOperandToBeParsed) {
        if (newNumericOperandToBeParsed != numericOperandToBeParsed) {
            NotificationChain msgs = null;
            if (numericOperandToBeParsed != null)
                msgs = ((InternalEObject) numericOperandToBeParsed).eInverseRemove(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, null, msgs);
            if (newNumericOperandToBeParsed != null)
                msgs = ((InternalEObject) newNumericOperandToBeParsed).eInverseAdd(this,
                        EOPPOSITE_FEATURE_BASE - ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, null, msgs);
            msgs = basicSetNumericOperandToBeParsed(newNumericOperandToBeParsed, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED, newNumericOperandToBeParsed, newNumericOperandToBeParsed));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        return DataType.DOUBLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {
        EList<IOperand> ops = new BasicEList<IOperand>();
        ops.add(getNumericOperandToBeParsed());
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_EvaluationDataType() {
        return DataType.STRING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getReadVariables() {
        if (getNumericOperandToBeParsed() == null) {
            return ECollections.emptyEList();
        }
        return getNumericOperandToBeParsed().getReadVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<String> getWriteVariables() {
        if (getNumericOperandToBeParsed() == null) {
            return ECollections.emptyEList();
        }
        return getNumericOperandToBeParsed().getWriteVariables();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
                return basicSetNumericOperandToBeParsed(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
                return getNumericOperandToBeParsed();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
                setNumericOperandToBeParsed((INumericOperand) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
                setNumericOperandToBeParsed((INumericOperand) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PARSE_NUMERIC_TO_STRING__NUMERIC_OPERAND_TO_BE_PARSED:
                return numericOperandToBeParsed != null;
        }
        return super.eIsSet(featureID);
    }

} //ParseNumericToStringImpl
