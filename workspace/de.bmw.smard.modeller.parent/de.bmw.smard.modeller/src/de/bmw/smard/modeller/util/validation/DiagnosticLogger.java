package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.slf4j.Logger;

import java.util.Iterator;

public class DiagnosticLogger {

    private final Logger logger;
    private final BasicDiagnostic diagnostic;

    public DiagnosticLogger(BasicDiagnostic diagnostic, Logger logger) {
        this.diagnostic = diagnostic;
        this.logger = logger;
    }

    public void logDiagMessages(int logLevel, int limitOfMsgsToLog) {
        logDiagMessage(diagnostic, logLevel);

        int count = 0;
        if (count < limitOfMsgsToLog) { // if there is a limit...
            int howManyLeft = diagnostic.getChildren().size() - limitOfMsgsToLog;
            // errors first
            Diagnostic child = null;
            if (logLevel < Diagnostic.ERROR) {
                for (Iterator<Diagnostic> i = diagnostic.getChildren().iterator(); i.hasNext(); ) {
                    child = i.next();
                    if (child.getSeverity() >= Severity.ERROR.value()) {
                        if (count++ >= limitOfMsgsToLog) {
                            break;
                        }
                        logDiagMessage(child, logLevel);
                    }
                }
            }

            // warnings second
            if (logLevel <= Diagnostic.WARNING) {
                for (Iterator<Diagnostic> i = diagnostic.getChildren().iterator(); i.hasNext(); ) {
                    child = i.next();
                    if (child.getSeverity() <= Severity.WARNING.value()) {
                        if (count++ >= limitOfMsgsToLog) {
                            break;
                        }
                        logDiagMessage(child, logLevel);
                    }
                }
            }

            if (howManyLeft > 0) {
                String msg = "omitted " + howManyLeft + " error or warning messages!";
                logMessage(msg, Diagnostic.INFO);
            }

        } else {
            // no limit -> log all of them
            for (Iterator<Diagnostic> i = diagnostic.getChildren().iterator(); i.hasNext(); ) {
                logDiagMessage((Diagnostic) i.next(), logLevel);
            }
        }
    }
    /**
     * logs diag's message only if its loglevel is greater or equal to parameter logLevel (log level filtering!)
     */
    private void logDiagMessage(Diagnostic diag, int logLevel) {
        if (diag == null) {
            logMessage("SMARDModelValidator::logDiagMessage diag null", Diagnostic.WARNING);
            return;
        }

        if (diag.getSeverity() >= logLevel) {
            logMessage(diag.getMessage(), diag.getSeverity());
        }
    }

    /**
     * logs message using log4j logger in engine context, else using PluginActivator's logging feature
     */
    private void logMessage(String message, int logLevel) {
        if (Platform.isRunning()) {
            PluginActivator.logMessage(logLevel, message);
        } else {
            if (logLevel == Diagnostic.INFO || logLevel == Diagnostic.OK) {
                logger.info(message);
            } else if (logLevel == Diagnostic.WARNING) {
                logger.warn(message);
            } else if (logLevel == Diagnostic.ERROR || logLevel == Diagnostic.CANCEL) {
                logger.error(message);
            }
        }
    }
}
