/**
 * <copyright>
 * </copyright>
 * $Id$
 */
package de.bmw.smard.modeller.statemachineset.util;

import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import java.util.Map;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatemachinesetXMLProcessor extends XMLProcessor {

    /**
     * Public constructor to instantiate the helper.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public StatemachinesetXMLProcessor() {
        super((EPackage.Registry.INSTANCE));
        StatemachinesetPackage.eINSTANCE.eClass();
    }

    /**
     * Register for "*" and "xml" file extensions the StatemachinesetResourceFactoryImpl factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected Map<String, Resource.Factory> getRegistrations() {
        if (registrations == null) {
            super.getRegistrations();
            registrations.put(XML_EXTENSION, new StatemachinesetResourceFactoryImpl());
            registrations.put(STAR_EXTENSION, new StatemachinesetResourceFactoryImpl());
        }
        return registrations;
    }

} //StatemachinesetXMLProcessor
