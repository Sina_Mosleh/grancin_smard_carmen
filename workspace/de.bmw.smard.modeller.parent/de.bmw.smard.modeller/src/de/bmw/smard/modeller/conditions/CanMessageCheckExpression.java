package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Can Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getType <em>Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getMessageIDs <em>Message IDs</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlag <em>Rxtx Flag</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam <em>Rxtx Flag Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifier <em>Ext Identifier</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression()
 * @model extendedMetaData="name='messageCheckCondition' kind='empty'"
 * @generated
 */
public interface CanMessageCheckExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Busid</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Busid</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Busid</em>' attribute.
     * @see #isSetBusid()
     * @see #unsetBusid()
     * @see #setBusid(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_Busid()
     * @model default="0" unsettable="true" dataType="de.bmw.smard.modeller.conditions.LongOrTemplatePlaceholder" required="true"
     *        extendedMetaData="kind='attribute' name='busid' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='special'"
     * @generated
     */
    String getBusid();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Busid</em>' attribute.
     * @see #isSetBusid()
     * @see #unsetBusid()
     * @see #getBusid()
     * @generated
     */
    void setBusid(String value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetBusid()
     * @see #getBusid()
     * @see #setBusid(String)
     * @generated
     */
    void unsetBusid();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusid <em>Busid</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Busid</em>' attribute is set.
     * @see #unsetBusid()
     * @see #getBusid()
     * @see #setBusid(String)
     * @generated
     */
    boolean isSetBusid();

    /**
     * Returns the value of the '<em><b>Type</b></em>' attribute.
     * The default value is <code>"ANY"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CheckType}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #setType(CheckType)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_Type()
     * @model default="ANY" required="true"
     *        extendedMetaData="kind='attribute' name='type' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    CheckType getType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getType <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CheckType
     * @see #getType()
     * @generated
     */
    void setType(CheckType value);

    /**
     * Returns the value of the '<em><b>Message IDs</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Message IDs</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Message IDs</em>' attribute.
     * @see #setMessageIDs(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_MessageIDs()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getMessageIDs();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getMessageIDs <em>Message IDs</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Message IDs</em>' attribute.
     * @see #getMessageIDs()
     * @generated
     */
    void setMessageIDs(String value);

    /**
     * Returns the value of the '<em><b>Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type Tmpl Param</em>' attribute.
     * @see #setTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_TypeTmplParam()
     * @model
     * @generated
     */
    String getTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getTypeTmplParam <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type Tmpl Param</em>' attribute.
     * @see #getTypeTmplParam()
     * @generated
     */
    void setTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Busid Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Busid Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Busid Tmpl Param</em>' attribute.
     * @see #setBusidTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_BusidTmplParam()
     * @model
     * @generated
     */
    String getBusidTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getBusidTmplParam <em>Busid Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Busid Tmpl Param</em>' attribute.
     * @see #getBusidTmplParam()
     * @generated
     */
    void setBusidTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Rxtx Flag</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Rxtx Flag</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Rxtx Flag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @see #setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_RxtxFlag()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    RxTxFlagTypeOrTemplatePlaceholderEnum getRxtxFlag();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlag <em>Rxtx Flag</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Rxtx Flag</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.RxTxFlagTypeOrTemplatePlaceholderEnum
     * @see #getRxtxFlag()
     * @generated
     */
    void setRxtxFlag(RxTxFlagTypeOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Rxtx Flag Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Rxtx Flag Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Rxtx Flag Type Tmpl Param</em>' attribute.
     * @see #setRxtxFlagTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_RxtxFlagTypeTmplParam()
     * @model
     * @generated
     */
    String getRxtxFlagTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getRxtxFlagTypeTmplParam <em>Rxtx Flag Type Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Rxtx Flag Type Tmpl Param</em>' attribute.
     * @see #getRxtxFlagTypeTmplParam()
     * @generated
     */
    void setRxtxFlagTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Ext Identifier</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ext Identifier</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ext Identifier</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @see #setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_ExtIdentifier()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    CanExtIdentifierOrTemplatePlaceholderEnum getExtIdentifier();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifier <em>Ext Identifier</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ext Identifier</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.CanExtIdentifierOrTemplatePlaceholderEnum
     * @see #getExtIdentifier()
     * @generated
     */
    void setExtIdentifier(CanExtIdentifierOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Ext Identifier Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ext Identifier Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ext Identifier Tmpl Param</em>' attribute.
     * @see #setExtIdentifierTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCanMessageCheckExpression_ExtIdentifierTmplParam()
     * @model
     * @generated
     */
    String getExtIdentifierTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression#getExtIdentifierTmplParam <em>Ext Identifier Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ext Identifier Tmpl Param</em>' attribute.
     * @see #getExtIdentifierTmplParam()
     * @generated
     */
    void setExtIdentifierTmplParam(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidRxTxFlag(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidExtIdentifier(DiagnosticChain diagnostics, Map<Object, Object> context);

} // CanMessageCheckExpression
