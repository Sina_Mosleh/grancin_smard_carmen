package de.bmw.smard.modeller.validator;

public interface Build {
    Validator build();
}
