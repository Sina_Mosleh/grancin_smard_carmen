package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getStringExpression()
 * @model abstract="true"
 * @generated
 */
public interface StringExpression extends Expression {
} // StringExpression
