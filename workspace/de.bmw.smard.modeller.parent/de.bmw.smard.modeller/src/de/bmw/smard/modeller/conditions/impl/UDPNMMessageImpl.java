package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.UDPFilter;
import de.bmw.smard.modeller.conditions.UDPNMFilter;
import de.bmw.smard.modeller.conditions.UDPNMMessage;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validation.Severity;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UDPNM Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.UDPNMMessageImpl#getUdpnmFilter <em>Udpnm Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UDPNMMessageImpl extends AbstractBusMessageImpl implements UDPNMMessage {
    /**
     * The cached value of the '{@link #getUdpnmFilter() <em>Udpnm Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getUdpnmFilter()
     * @generated
     * @ordered
     */
    protected UDPNMFilter udpnmFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UDPNMMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.UDPNM_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public UDPNMFilter getUdpnmFilter() {
        return udpnmFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetUdpnmFilter(UDPNMFilter newUdpnmFilter, NotificationChain msgs) {
        UDPNMFilter oldUdpnmFilter = udpnmFilter;
        udpnmFilter = newUdpnmFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, oldUdpnmFilter, newUdpnmFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUdpnmFilter(UDPNMFilter newUdpnmFilter) {
        if (newUdpnmFilter != udpnmFilter) {
            NotificationChain msgs = null;
            if (udpnmFilter != null)
                msgs = ((InternalEObject) udpnmFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, null, msgs);
            if (newUdpnmFilter != null)
                msgs = ((InternalEObject) newUdpnmFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, null, msgs);
            msgs = basicSetUdpnmFilter(newUdpnmFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER, newUdpnmFilter, newUdpnmFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidUDPFilter(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean hasValidUDPFilter = true;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";

        boolean udpFilterFound = false;
        AbstractFilter udpFilter = null;
        for (AbstractFilter filter : this.getFilters()) {
            if (filter instanceof UDPFilter) {
                udpFilterFound = true;
                udpFilter = filter;
            }
        }

        if (udpFilterFound) {
            String sourcePort = ((UDPFilter) udpFilter).getSourcePort();
            if (!(sourcePort.equalsIgnoreCase("30500"))) {
                hasValidUDPFilter = false;
                errorMessageIdentifier = "_Validation_Conditions_UDPNMMessage_UDPFilterWrongSourcePort";
            }
        } else {
            hasValidUDPFilter = false;
            errorMessageIdentifier = "_Validation_Conditions_UDPNMMessage_NoUDPFilter";
        }

        if (!hasValidUDPFilter) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.builder(Severity.ERROR)
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.UDPNM_MESSAGE__IS_VALID_HAS_VALID_UDP_FILTER)
                        .messageId(PluginActivator.getInstance(), errorMessageIdentifier)
                        .data(this)
                        .build());
            }
        }

        return hasValidUDPFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
                return basicSetUdpnmFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
                return getUdpnmFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
                setUdpnmFilter((UDPNMFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
                setUdpnmFilter((UDPNMFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.UDPNM_MESSAGE__UDPNM_FILTER:
                return udpnmFilter != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * @generated NOT
     */
    @Override
    public AbstractFilter getPrimaryFilter() {
        return udpnmFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //UDPNMMessageImpl
