package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TCP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getSequenceNumber <em>Sequence Number</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getAcknowledgementNumber <em>Acknowledgement Number</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getTcpFlags <em>Tcp Flags</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamValidPayloadOffset <em>Stream Valid Payload Offset</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter()
 * @model
 * @generated
 */
public interface TCPFilter extends TPFilter {

    /**
     * Returns the value of the '<em><b>Sequence Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sequence Number</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Sequence Number</em>' attribute.
     * @see #setSequenceNumber(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_SequenceNumber()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getSequenceNumber();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getSequenceNumber <em>Sequence Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Sequence Number</em>' attribute.
     * @see #getSequenceNumber()
     * @generated
     */
    void setSequenceNumber(String value);

    /**
     * Returns the value of the '<em><b>Acknowledgement Number</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Acknowledgement Number</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Acknowledgement Number</em>' attribute.
     * @see #setAcknowledgementNumber(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_AcknowledgementNumber()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getAcknowledgementNumber();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getAcknowledgementNumber <em>Acknowledgement Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Acknowledgement Number</em>' attribute.
     * @see #getAcknowledgementNumber()
     * @generated
     */
    void setAcknowledgementNumber(String value);

    /**
     * Returns the value of the '<em><b>Tcp Flags</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Tcp Flags</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Tcp Flags</em>' attribute.
     * @see #setTcpFlags(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_TcpFlags()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getTcpFlags();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getTcpFlags <em>Tcp Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Tcp Flags</em>' attribute.
     * @see #getTcpFlags()
     * @generated
     */
    void setTcpFlags(String value);

    /**
     * Returns the value of the '<em><b>Stream Analysis Flags</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stream Analysis Flags</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Stream Analysis Flags</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
     * @see #setStreamAnalysisFlags(StreamAnalysisFlagsOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_StreamAnalysisFlags()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    StreamAnalysisFlagsOrTemplatePlaceholderEnum getStreamAnalysisFlags();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Stream Analysis Flags</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum
     * @see #getStreamAnalysisFlags()
     * @generated
     */
    void setStreamAnalysisFlags(StreamAnalysisFlagsOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Stream Analysis Flags Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stream Analysis Flags Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Stream Analysis Flags Tmpl Param</em>' attribute.
     * @see #setStreamAnalysisFlagsTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_StreamAnalysisFlagsTmplParam()
     * @model
     * @generated
     */
    String getStreamAnalysisFlagsTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags Tmpl Param</em>}'
     * attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Stream Analysis Flags Tmpl Param</em>' attribute.
     * @see #getStreamAnalysisFlagsTmplParam()
     * @generated
     */
    void setStreamAnalysisFlagsTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Stream Valid Payload Offset</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Stream Valid Payload Offset</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Stream Valid Payload Offset</em>' attribute.
     * @see #setStreamValidPayloadOffset(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTCPFilter_StreamValidPayloadOffset()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getStreamValidPayloadOffset();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TCPFilter#getStreamValidPayloadOffset <em>Stream Valid Payload Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Stream Valid Payload Offset</em>' attribute.
     * @see #getStreamValidPayloadOffset()
     * @generated
     */
    void setStreamValidPayloadOffset(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSourcePort(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidDestinationPort(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSequenceNumber(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidAcknowledgementNumber(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidTcpFlag(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStreamAnalysisFlag(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidStreamValidPayloadOffset(DiagnosticChain diagnostics, Map<Object, Object> context);
} // TCPFilter
