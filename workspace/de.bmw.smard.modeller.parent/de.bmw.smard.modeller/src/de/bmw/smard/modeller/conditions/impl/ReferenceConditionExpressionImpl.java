package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.Condition;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.Expression;
import de.bmw.smard.modeller.conditions.NotExpression;
import de.bmw.smard.modeller.conditions.ReferenceConditionExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reference Condition Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.ReferenceConditionExpressionImpl#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReferenceConditionExpressionImpl extends ExpressionImpl implements ReferenceConditionExpression {
    /**
     * The cached value of the '{@link #getCondition() <em>Condition</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCondition()
     * @generated
     * @ordered
     */
    protected Condition condition;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ReferenceConditionExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.REFERENCE_CONDITION_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Condition getCondition() {
        if (condition != null && condition.eIsProxy()) {
            InternalEObject oldCondition = (InternalEObject) condition;
            condition = (Condition) eResolveProxy(oldCondition);
            if (condition != oldCondition) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION, oldCondition, condition));
            }
        }
        return condition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Condition basicGetCondition() {
        return condition;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCondition(Condition newCondition) {
        Condition oldCondition = condition;
        condition = newCondition;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION, oldCondition, condition));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getCondition() != null) {
            return getCondition().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * adding this method is not included in edapt history
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidReferencedCondition(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValid_hasValidReferencedCondition = true;
        String errorMessageIdentifier = "";

        // referenced condition may not be empty
        if (getCondition() != null) {
            if (getCondition().getExpression() == null) {
                errorMessageIdentifier = "_Validation_Conditions_ReferenceConditionExpression_ConditionIsEmpty";
                isValid_hasValidReferencedCondition = false;
            } else {
                // condition must not reference itself
                if (!isValid_conditionReferencesItself()) {
                    errorMessageIdentifier = "_Validation_Conditions_ReferenceConditionExpression_SelfReference";
                    isValid_hasValidReferencedCondition = false;
                } else {
                    // no cyclic references allowed
                    if (!isValid_hasNoCyclicReference(this.condition)) {
                        errorMessageIdentifier = "_Validation_Conditions_ReferenceConditionExpression_ReferencesReferenceCondition";
                        isValid_hasValidReferencedCondition = false;
                    }
                }
            }
        }

        if (!isValid_hasValidReferencedCondition) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(ConditionsValidator.REFERENCE_CONDITION_EXPRESSION__IS_VALID_HAS_VALID_REFERENCED_CONDITION)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier)
                        .data(this)
                        .build());
            }
        }

        return isValid_hasValidReferencedCondition;
    }

    private boolean isValid_conditionReferencesItself() {
        if (this.eContainer() instanceof Condition) {
            if (((Condition) this.eContainer()).equals(this.condition)) {
                return false;
            }
        }
        return true;
    }

    private boolean isValid_hasNoCyclicReference(Condition cond) {
        List<Condition> conditions = new ArrayList<Condition>();

        conditions.add(cond);

        Expression expression = cond.getExpression();
        return checkNestedExpressions(expression, conditions);
    }

    private boolean checkNestedExpressions(Expression expression, List<Condition> conditions) {
        if (expression == null) {
            return true;
        }

        if (expression instanceof ReferenceConditionExpression) {
            Condition cond = ((ReferenceConditionExpression) expression).getCondition();
            if (conditions.contains(cond)) {
                return false;
            }
            conditions.add(cond);

            Expression referencedByConditionExpression = cond.getExpression();
            return checkNestedExpressions(referencedByConditionExpression, conditions);
        } else if (expression instanceof LogicalExpressionImpl) {
            for (Expression logicalExpressionChild : ((LogicalExpressionImpl) expression).getExpressions()) {
                return checkNestedExpressions(logicalExpressionChild, conditions);
            }
        } else if (expression instanceof NotExpression) {
            Expression notExpression = ((NotExpression) expression).getExpression();
            return checkNestedExpressions(notExpression, conditions);
        }

        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION:
                if (resolve) return getCondition();
                return basicGetCondition();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION:
                setCondition((Condition) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION:
                setCondition((Condition) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.REFERENCE_CONDITION_EXPRESSION__CONDITION:
                return condition != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<AbstractState> getStateDependencies() {
        return getCondition().getStateDependencies();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<Transition> getTransitionDependencies() {
        return getCondition().getTransitionDependencies();
    }
} //ReferenceConditionExpressionImpl
