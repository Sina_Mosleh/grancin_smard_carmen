package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDEntryFlagsOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum SomeIPSDEntryFlagsOrTemplatePlaceholderEnum implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(-1, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>SDF UNICAST</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #SDF_UNICAST_VALUE
     * @generated
     * @ordered
     */
    SDF_UNICAST(0, "SDF_UNICAST", "SDF_UNICAST"),

    /**
     * The '<em><b>SDF REBOOT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #SDF_REBOOT_VALUE
     * @generated
     * @ordered
     */
    SDF_REBOOT(1, "SDF_REBOOT", "SDF_REBOOT"),

    /**
     * The '<em><b>ALL</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ALL_VALUE
     * @generated
     * @ordered
     */
    ALL(2, "ALL", "ALL");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = -1;

    /**
     * The '<em><b>SDF UNICAST</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>SDF UNICAST</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #SDF_UNICAST
     * @model
     * @generated
     * @ordered
     */
    public static final int SDF_UNICAST_VALUE = 0;

    /**
     * The '<em><b>SDF REBOOT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>SDF REBOOT</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #SDF_REBOOT
     * @model
     * @generated
     * @ordered
     */
    public static final int SDF_REBOOT_VALUE = 1;

    /**
     * The '<em><b>ALL</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ALL</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ALL
     * @model
     * @generated
     * @ordered
     */
    public static final int ALL_VALUE = 2;

    /**
     * An array of all the '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final SomeIPSDEntryFlagsOrTemplatePlaceholderEnum[] VALUES_ARRAY =
            new SomeIPSDEntryFlagsOrTemplatePlaceholderEnum[] {
                    TEMPLATE_DEFINED,
                    SDF_UNICAST,
                    SDF_REBOOT,
                    ALL,
            };

    /**
     * A public read-only list of all the '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<SomeIPSDEntryFlagsOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPSDEntryFlagsOrTemplatePlaceholderEnum get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SomeIPSDEntryFlagsOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPSDEntryFlagsOrTemplatePlaceholderEnum getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            SomeIPSDEntryFlagsOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Some IPSD Entry Flags Or Template Placeholder Enum</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static SomeIPSDEntryFlagsOrTemplatePlaceholderEnum get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case SDF_UNICAST_VALUE:
                return SDF_UNICAST;
            case SDF_REBOOT_VALUE:
                return SDF_REBOOT;
            case ALL_VALUE:
                return ALL;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private SomeIPSDEntryFlagsOrTemplatePlaceholderEnum(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
