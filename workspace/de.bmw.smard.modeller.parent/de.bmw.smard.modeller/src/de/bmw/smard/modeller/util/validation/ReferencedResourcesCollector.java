package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.conditions.ConditionsDocument;
import de.bmw.smard.modeller.conditions.util.ConditionsResourceImpl;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.util.StatemachineResourceImpl;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import java.util.*;

public class ReferencedResourcesCollector {
	
	/**
	 * collect all EObjects that are referenced by StateMachineSet sms and additionalConditionDocs
	 * @param sms
	 * @param additionalConditionDocs
	 * @return a distinct Set<EObject> of all objects referenced by the StateMachineSet and additionalConditionDocs
	 */
	public static Set<EObject> getRelevantObjectsForStatemachineSet(StateMachineSet sms, 
			Set<ConditionsDocument> additionalConditionDocs) {
	    Set<EObject> relevantObjects = new HashSet<>();
	    List<Resource> reffedResources = new ArrayList<>();
	    for (StateMachine sm : sms.getStateMachines()) {
	        if (sm.eResource() != null) {
	            reffedResources.addAll(collectDistinctAllReferencedResources(sm.eResource()));
	        }
	    }
	
	    for (AbstractObserver obs : sms.getObservers()) {
	        if (obs.eResource() != null) {
	            reffedResources.addAll(collectDistinctAllReferencedResources(obs.eResource()));
	        }
	    }
	
	    for (ConditionsDocument conditionsDocument : additionalConditionDocs) {
	        if (conditionsDocument.eResource() != null && !reffedResources.contains(conditionsDocument.eResource())) {
				reffedResources.addAll(collectDistinctAllReferencedResources(conditionsDocument.eResource()));
	        }
	    }
	
	    // SMARD-1728 duplicate Resources lead to supposed duplicate Variables in project
	    // filter duplicates to avoid false invalidations of unique variable names
	    reffedResources = getDistinctResources(reffedResources);
	
	    for (Resource res : reffedResources) {
	        EObject r00t = res.getContents().get(0);
			relevantObjects.add(r00t);
	    }
	
	    return relevantObjects;
	}


	/**
	 * search for other resources containing referenced objects
	 */
	private static List<Resource> collectDistinctAllReferencedResources(Resource resource) {
	    List<Resource> result = new ArrayList<>();
	    result.add(resource);
	    int sizeBefore = 0;
	
	    while (sizeBefore < result.size()) {
	        sizeBefore = result.size();
	        List<Resource> resources = new ArrayList<>(result);
	        for (Resource res : resources) {
	            for (Iterator<EObject> j = res.getAllContents(); j.hasNext(); ) {
	                EObject currentObject = j.next();
	                for (Object object : currentObject.eCrossReferences()) {
	                    EObject eObject = (EObject) object;
	                    Resource otherResource = eObject.eResource();
	
	                    if (resource instanceof ConditionsResourceImpl
	                            && otherResource instanceof StatemachineResourceImpl) {
	                        // SMARD-2009 if we look through a conditions file do not add dependencies on
	                        // statemachine files
	                    } else {
	                        if (otherResource != null && !resultContainsResource(result, otherResource)) {
	                            result.add(otherResource);
	                        }
	                    }
	
	                }
	            }
	        }
	    }
	    return result;
	}

	/**
	 * get rid of duplicate entries in List<Resource>
	 * @param originalResources
	 * @return
	 */
	private static List<Resource> getDistinctResources(List<Resource> originalResources) {
	    List<Resource> distinctResources = new ArrayList<>();
	
	    for (Resource originalResource : originalResources) {
	        if (!resultContainsResource(distinctResources, originalResource)) {
	            distinctResources.add(originalResource);
	        }
	    }
	    return distinctResources;
	}


	private static boolean resultContainsResource(List<Resource> resultResources, Resource resource) {
        for (Resource resultResource : resultResources) {
            if (resultResource.getURI().equals(resource.getURI())) {
                return true;
            }
        }

        return false;
    }


}
