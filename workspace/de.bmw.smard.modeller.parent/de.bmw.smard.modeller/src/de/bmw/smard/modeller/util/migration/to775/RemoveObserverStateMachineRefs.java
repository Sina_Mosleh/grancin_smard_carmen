package de.bmw.smard.modeller.util.migration.to775;

import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

import java.util.Collection;

public class RemoveObserverStateMachineRefs extends CustomMigration {

	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		for(Instance stateMachineSet : model.getAllInstances("statemachineset.StateMachineSet")) {
			Collection<Instance> obsRefs = stateMachineSet.get("observerReferences");
			if(obsRefs != null) {
				obsRefs.stream()
					.map(ref -> ref.get("observer"))
					.filter(obs -> obs instanceof Instance)
					.filter(obs -> isValidObserver((Instance) obs))
					.distinct()					
					.forEach(obs -> stateMachineSet.add("observers", obs));				
			}
			Collection<Instance> smsRefs = stateMachineSet.get("stateMachineReferences");			
			if(smsRefs != null) {
				smsRefs.stream()				
					.map(ref -> ref.get("stateMachine"))
					.filter(sm -> sm instanceof Instance)
					.filter(sm -> isValidStateMachine((Instance) sm))
					.distinct()					
					.forEach(sm -> stateMachineSet.add("stateMachines", sm));
			}			
		}
	}
	
	private boolean isValidObserver(Instance observer) {
		try {
			// Observer without name is not valid
			return observer.get("name") != null;
		} catch(Throwable ignore) {
			return false;
		}
	}

	private boolean isValidStateMachine(Instance stateMachine) {
		try {
			// StateMachine without initialState is not valid (we cannot look at the name because of an default value)
			return stateMachine.get("initialState") != null;
		} catch(Throwable ignore) {
			return false;
		}
	}

}
