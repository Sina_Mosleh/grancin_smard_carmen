package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.PluginResultExtractStrategy;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Plugin Result Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PluginResultExtractStrategyImpl#getResultRange <em>Result Range</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PluginResultExtractStrategyImpl extends ExtractStrategyImpl implements PluginResultExtractStrategy {
    /**
     * The default value of the '{@link #getResultRange() <em>Result Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getResultRange()
     * @generated
     * @ordered
     */
    protected static final String RESULT_RANGE_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getResultRange() <em>Result Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getResultRange()
     * @generated
     * @ordered
     */
    protected String resultRange = RESULT_RANGE_EDEFAULT;

    /**
     * This is true if the Result Range attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean resultRangeESet;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PluginResultExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PLUGIN_RESULT_EXTRACT_STRATEGY;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getResultRange() {
        return resultRange;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setResultRange(String newResultRange) {
        String oldResultRange = resultRange;
        resultRange = newResultRange;
        boolean oldResultRangeESet = resultRangeESet;
        resultRangeESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE, oldResultRange, resultRange, !oldResultRangeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetResultRange() {
        String oldResultRange = resultRange;
        boolean oldResultRangeESet = resultRangeESet;
        resultRange = RESULT_RANGE_EDEFAULT;
        resultRangeESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE, oldResultRange, RESULT_RANGE_EDEFAULT, oldResultRangeESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetResultRange() {
        return resultRangeESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidResultRange(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PLUGIN_RESULT_EXTRACT_STRATEGY__IS_VALID_HAS_VALID_RESULT_RANGE)
                .object(this)
                .diagnostic(diagnostics)

                // a missing resultRange is valid, states Jochen Sommer in SMARD-2329

                .with(Validators.rangeIdDoubleTemplate(resultRange)
                        .containsParameterError("_Validation_Conditions_PluginResultExtractStrategy_ResultRange_ContainsParameters")
                        .invalidPlaceholderError("_Validation_Conditions_PluginResultExtractStrategy_ResultRange_NotAValidPlaceholder")
                        .illegalValueError("_Validation_Conditions_PluginResultExtractStrategy_ResultRange_IllegalResultRange")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE:
                return getResultRange();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE:
                setResultRange((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE:
                unsetResultRange();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PLUGIN_RESULT_EXTRACT_STRATEGY__RESULT_RANGE:
                return isSetResultRange();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (resultRange: ");
        if (resultRangeESet) result.append(resultRange);
        else result.append("<unset>");
        result.append(')');
        return result.toString();
    }

} //PluginResultExtractStrategyImpl
