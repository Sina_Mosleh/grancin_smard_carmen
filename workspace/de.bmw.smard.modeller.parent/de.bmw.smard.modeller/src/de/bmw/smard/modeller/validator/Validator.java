package de.bmw.smard.modeller.validator;

import de.bmw.smard.modeller.validation.Severity;

public abstract class Validator implements Build {

    public static BaseValidator.SourceStep builder() {
        return BaseValidator.builder();
    }

    @Override
    public Validator build() {
        return this;
    }

    protected abstract boolean validate(BaseValidator diagnostics);

    void attachError(BaseValidator baseValidator, String errorId) {
        attachError(baseValidator, errorId, null);
    }

    void attachError(BaseValidator baseValidator, String errorId, String substitution) {
        attach(baseValidator, Severity.ERROR, errorId, substitution);
    }

    void attachWarn(BaseValidator baseValidator, String warningId, String substition) {
        attach(baseValidator, Severity.WARNING, warningId, substition);
    }

    void attach(BaseValidator baseValidator, Severity severity, String messageId, String substition) {
        baseValidator.attach(severity, messageId, substition);
    }
}
