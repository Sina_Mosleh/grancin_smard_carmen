package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.LINFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.BaseValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LIN Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LINFilterImpl#getMessageIdRange <em>Message Id Range</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LINFilterImpl#getFrameId <em>Frame Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LINFilterImpl extends AbstractFilterImpl implements LINFilter {
    /**
     * The default value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_ID_RANGE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMessageIdRange() <em>Message Id Range</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIdRange()
     * @generated
     * @ordered
     */
    protected String messageIdRange = MESSAGE_ID_RANGE_EDEFAULT;

    /**
     * The default value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFrameId()
     * @generated
     * @ordered
     */
    protected static final String FRAME_ID_EDEFAULT = "0x0";

    /**
     * The cached value of the '{@link #getFrameId() <em>Frame Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFrameId()
     * @generated
     * @ordered
     */
    protected String frameId = FRAME_ID_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected LINFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.LIN_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageIdRange() {
        return messageIdRange;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageIdRange(String newMessageIdRange) {
        String oldMessageIdRange = messageIdRange;
        messageIdRange = newMessageIdRange;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE, oldMessageIdRange, messageIdRange));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFrameId() {
        return frameId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFrameId(String newFrameId) {
        String oldFrameId = frameId;
        frameId = newFrameId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_FILTER__FRAME_ID, oldFrameId, frameId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidFrameIdOrFrameIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {

        BaseValidator.WithStep validator = Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.LIN_FILTER__IS_VALID_HAS_VALID_FRAME_ID_OR_FRAME_ID_RANGE)
                .object(eContainer)
                .diagnostic(diagnostics);

        if (StringUtils.isNotBlank(messageIdRange)) {
            // if frameIdRange is given, check if frameIdRange is valid
            validator.with(Validators.stringTemplate(messageIdRange)
                    .containsParameterError("_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_ContainsPlaceholders")
                    .invalidPlaceholderError("_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.linFrameIdRange())
                    .build());
        } else {
            validator.with(Validators.stringTemplate(frameId)
                    .containsParameterError("_Validation_Conditions_CANLINFlexRayFilter_FrameId_ContainsPlaceholders")
                    .invalidPlaceholderError("_Validation_Conditions_CANLINFlexRayFilter_FrameId_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.linFrameId())
                    .build());
        }

        return validator.validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
                return getMessageIdRange();
            case ConditionsPackage.LIN_FILTER__FRAME_ID:
                return getFrameId();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange((String) newValue);
                return;
            case ConditionsPackage.LIN_FILTER__FRAME_ID:
                setFrameId((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
                setMessageIdRange(MESSAGE_ID_RANGE_EDEFAULT);
                return;
            case ConditionsPackage.LIN_FILTER__FRAME_ID:
                setFrameId(FRAME_ID_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LIN_FILTER__MESSAGE_ID_RANGE:
                return MESSAGE_ID_RANGE_EDEFAULT == null ? messageIdRange != null : !MESSAGE_ID_RANGE_EDEFAULT.equals(messageIdRange);
            case ConditionsPackage.LIN_FILTER__FRAME_ID:
                return FRAME_ID_EDEFAULT == null ? frameId != null : !FRAME_ID_EDEFAULT.equals(frameId);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (messageIdRange: ");
        result.append(messageIdRange);
        result.append(", frameId: ");
        result.append(frameId);
        result.append(')');
        return result.toString();
    }

} //LINFilterImpl
