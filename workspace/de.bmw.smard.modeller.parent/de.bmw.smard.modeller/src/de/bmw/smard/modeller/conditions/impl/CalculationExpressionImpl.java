package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.math.*;
import de.bmw.smard.common.math.VariableReference;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.util.ActionExpressionUtils;
import de.bmw.smard.modeller.util.Operation;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.VariableManager;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Calculation Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl#getExpression <em>Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.CalculationExpressionImpl#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CalculationExpressionImpl extends ComparatorSignalImpl implements CalculationExpression {


    /**
     * The default value of the '{@link #getExpression() <em>Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpression()
     * @generated
     * @ordered
     */
    protected static final String EXPRESSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getExpression() <em>Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExpression()
     * @generated
     * @ordered
     */
    protected String expression = EXPRESSION_EDEFAULT;

    /**
     * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getOperands()
     * @generated
     * @ordered
     */
    protected EList<INumericOperand> operands;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CalculationExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.CALCULATION_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getExpression() {
        return expression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExpression(String newExpression) {
        String oldExpression = expression;
        expression = newExpression;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CALCULATION_EXPRESSION__EXPRESSION, oldExpression, expression));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<INumericOperand> getOperands() {
        if (operands == null) {
            operands = new EObjectContainmentEList<INumericOperand>(INumericOperand.class, this, ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS);
        }
        return operands;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValid = true;

        String errorMessageIdentifier = "_Validation_Conditions_CalculationExpression_Expression";

        String messageToSubstitute = null;
        Term term = null;

        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            try {
                term = MathParser.parse(expression, operands);
            } catch (ActionExpressionParseException e) {
                isValid = false;
                messageToSubstitute = e.getMessage();
            }

            if (isValid && term == null) {
                messageToSubstitute = "missing action expression. (Use \"OP1\",\"OP2\", ... to reference child operators.)";
                isValid = false;
            }
            if (isValid && (term instanceof Assignment)) {
                messageToSubstitute = "assignment not allowed in action expression";
                isValid = false;
            }

            if (isValid && ((term instanceof VariableReference
                || term instanceof Operation
                || term instanceof UnaryOperation
                || term instanceof BinaryOperation)
                || term instanceof NumericLiteral) == false) {
                messageToSubstitute = "action expression term is of wrong type";
                isValid = false;
            }
        } else {
            // in template context
            if (TemplateUtils.containsParameters(this.expression)) {
                messageToSubstitute = TemplateUtils.checkValidPlaceholderNameWithHashtags(this.expression);
                if (StringUtils.isNotBlank(messageToSubstitute)) {
                    errorMessageIdentifier = "_Validation_Conditions_CalculationExpression_Expression_NotAValidPlaceholderName";
                    isValid = false;
                }
            }
        }
        if (term != null)
            ActionExpressionUtils.checkAndWarnForDeprecatedSystemVariableUsage(
                    term.getReferencedVariableNames(), expression, this, diagnostics, context);


        if (!isValid && diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.error()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.CALCULATION_EXPRESSION__IS_VALID_HAS_VALID_EXPRESSION)
                    .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                    .data(this)
                    .build());
        }

        return isValid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context) {
        boolean isValid = true;
        boolean isDouble = false;
        String errMsgID = "_Validation_Conditions_CalculationExpression_Operands";

        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            if (operands != null && !operands.isEmpty()) {
                for (IOperand op : operands) {
                    isDouble = op.get_EvaluationDataType() == DataType.DOUBLE;
                    isValid &= isDouble;
                }
            }
        } else {
            //todo validation template context
        }

        if (!isValid && diagnostics != null) {
            diagnostics.add(DiagnosticBuilder.error()
                    .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                    .code(ConditionsValidator.CALCULATION_EXPRESSION__IS_VALID_HAS_VALID_OPERANDS)
                    .messageId(PluginActivator.INSTANCE, errMsgID)
                    .data(this)
                    .build());
        }
        return isValid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public DataType get_OperandDataType() {
        return DataType.DOUBLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public EList<IOperand> get_Operands() {

        EList<INumericOperand> numOps = getOperands();
        EList<IOperand> ops = new BasicEList<IOperand>(numOps.size());
        ops.addAll(numOps);
        return ops;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS:
                return ((InternalEList<?>) getOperands()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.CALCULATION_EXPRESSION__EXPRESSION:
                return getExpression();
            case ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS:
                return getOperands();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.CALCULATION_EXPRESSION__EXPRESSION:
                setExpression((String) newValue);
                return;
            case ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS:
                getOperands().clear();
                getOperands().addAll((Collection<? extends INumericOperand>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CALCULATION_EXPRESSION__EXPRESSION:
                setExpression(EXPRESSION_EDEFAULT);
                return;
            case ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS:
                getOperands().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CALCULATION_EXPRESSION__EXPRESSION:
                return EXPRESSION_EDEFAULT == null ? expression != null : !EXPRESSION_EDEFAULT.equals(expression);
            case ConditionsPackage.CALCULATION_EXPRESSION__OPERANDS:
                return operands != null && !operands.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        EList<String> variableNames = ActionExpressionUtils.extractVariableNames(getExpression());
        for (String varName : variableNames) {
            AbstractVariable var = VariableManager.getInstance(this).getVariableByName(varName);
            if (var != null) {
                messages.addAll(var.getAllReferencedBusMessages());
            }
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (Expression: ");
        result.append(expression);
        result.append(')');
        return result.toString();
    }

    /** generated NOT */
    @Override
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        return ActionExpressionUtils.extractReadVariableNames(expression, operands, false);
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        return ECollections.emptyEList();
    }


} //CalculationExpressionImpl
