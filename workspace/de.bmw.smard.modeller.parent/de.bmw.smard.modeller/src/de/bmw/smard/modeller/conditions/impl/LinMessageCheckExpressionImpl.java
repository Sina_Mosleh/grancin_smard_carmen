package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.CheckType;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.LinMessageCheckExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Arrays;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Lin Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl#getBusid <em>Busid</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl#getType <em>Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl#getMessageIDs <em>Message IDs</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.LinMessageCheckExpressionImpl#getBusidTmplParam <em>Busid Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinMessageCheckExpressionImpl extends ExpressionImpl implements LinMessageCheckExpression {
    /**
     * The default value of the '{@link #getBusid() <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusid()
     * @generated
     * @ordered
     */
    protected static final String BUSID_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getBusid() <em>Busid</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusid()
     * @generated
     * @ordered
     */
    protected String busid = BUSID_EDEFAULT;

    /**
     * This is true if the Busid attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean busidESet;

    /**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected static final CheckType TYPE_EDEFAULT = CheckType.ANY;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected CheckType type = TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getMessageIDs() <em>Message IDs</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIDs()
     * @generated
     * @ordered
     */
    protected static final String MESSAGE_IDS_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMessageIDs() <em>Message IDs</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessageIDs()
     * @generated
     * @ordered
     */
    protected String messageIDs = MESSAGE_IDS_EDEFAULT;

    /**
     * The default value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String TYPE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String typeTmplParam = TYPE_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getBusidTmplParam() <em>Busid Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusidTmplParam()
     * @generated
     * @ordered
     */
    protected static final String BUSID_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getBusidTmplParam() <em>Busid Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusidTmplParam()
     * @generated
     * @ordered
     */
    protected String busidTmplParam = BUSID_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected LinMessageCheckExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.LIN_MESSAGE_CHECK_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusid() {
        return busid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusid(String newBusid) {
        String oldBusid = busid;
        busid = newBusid;
        boolean oldBusidESet = busidESet;
        busidESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID, oldBusid, busid, !oldBusidESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetBusid() {
        String oldBusid = busid;
        boolean oldBusidESet = busidESet;
        busid = BUSID_EDEFAULT;
        busidESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID, oldBusid, BUSID_EDEFAULT, oldBusidESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetBusid() {
        return busidESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public CheckType getType() {
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setType(CheckType newType) {
        CheckType oldType = type;
        type = newType == null ? TYPE_EDEFAULT : newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE, oldType, type));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMessageIDs() {
        return messageIDs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMessageIDs(String newMessageIDs) {
        String oldMessageIDs = messageIDs;
        messageIDs = newMessageIDs;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS, oldMessageIDs, messageIDs));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTypeTmplParam() {
        return typeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTypeTmplParam(String newTypeTmplParam) {
        String oldTypeTmplParam = typeTmplParam;
        typeTmplParam = newTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM, oldTypeTmplParam, typeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusidTmplParam() {
        return busidTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusidTmplParam(String newBusidTmplParam) {
        String oldBusidTmplParam = busidTmplParam;
        busidTmplParam = newBusidTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM, oldBusidTmplParam, busidTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(busid)
                        .error("_Validation_Conditions_LinMessageCheckExpression_BusId_IsNull")
                        .build())

                .with(Validators.when(!isSetBusid())
                        .error("_Validation_Conditions_LinMessageCheckExpression_BusId_IsNull")
                        .build())

                .with(Validators.checkTemplate(busid)
                        .tmplParam(busidTmplParam)
                        .containsParameterError("_Validation_Conditions_LinMessageCheckExpression_BusId_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_LinMessageCheckExpression_BusIdTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_LinMessageCheckExpression_BusIdTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.busId(LinMessageCheckExpression.class.getSimpleName()))
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidMessageIdRange(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID_RANGE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(messageIDs)
                        .error("_Validation_Conditions_LinMessageCheckExpression_MessageIdRange_IsNull")
                        .build())

                .with(Validators.rangeIdTemplate(messageIDs)
                        .containsParameterError("_Validation_Conditions_LinMessageCheckExpression_MessageIdRange_ContainsParameters")
                        .invalidPlaceholderError("_Validation_Conditions_LinMessageCheckExpression_MessageIdRange_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_LinMessageCheckExpression_MessageIdRange_IllegalMessageId")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.LIN_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(type)
                        .error("_Validation_Conditions_LinMessageCheckExpression_CheckType_IsNull")
                        .build())

                .with(Validators.checkTemplate(type)
                        .templateType(CheckType.TEMPLATE_DEFINED)
                        .tmplParam(typeTmplParam)
                        .containsParameterError("_Validation_Conditions_LinMessageCheckExpression_CheckType_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_LinMessageCheckExpression_CheckTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_LinMessageCheckExpression_CheckTypeTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(CheckType.ALL, CheckType.ANY),
                                "_Validation_Conditions_LinMessageCheckExpression_CheckType_IllegalCheckType")
                        .build())

                .validate();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID:
                return getBusid();
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE:
                return getType();
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
                return getMessageIDs();
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                return getTypeTmplParam();
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
                return getBusidTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID:
                setBusid((String) newValue);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE:
                setType((CheckType) newValue);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
                setMessageIDs((String) newValue);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                setTypeTmplParam((String) newValue);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
                setBusidTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID:
                unsetBusid();
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE:
                setType(TYPE_EDEFAULT);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
                setMessageIDs(MESSAGE_IDS_EDEFAULT);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                setTypeTmplParam(TYPE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
                setBusidTmplParam(BUSID_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID:
                return isSetBusid();
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE:
                return type != TYPE_EDEFAULT;
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__MESSAGE_IDS:
                return MESSAGE_IDS_EDEFAULT == null ? messageIDs != null : !MESSAGE_IDS_EDEFAULT.equals(messageIDs);
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                return TYPE_TMPL_PARAM_EDEFAULT == null ? typeTmplParam != null : !TYPE_TMPL_PARAM_EDEFAULT.equals(typeTmplParam);
            case ConditionsPackage.LIN_MESSAGE_CHECK_EXPRESSION__BUSID_TMPL_PARAM:
                return BUSID_TMPL_PARAM_EDEFAULT == null ? busidTmplParam != null : !BUSID_TMPL_PARAM_EDEFAULT.equals(busidTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (busid: ");
        if (busidESet) result.append(busid);
        else result.append("<unset>");
        result.append(", type: ");
        result.append(type);
        result.append(", messageIDs: ");
        result.append(messageIDs);
        result.append(", typeTmplParam: ");
        result.append(typeTmplParam);
        result.append(", busidTmplParam: ");
        result.append(busidTmplParam);
        result.append(')');
        return result.toString();
    }

} //LinMessageCheckExpressionImpl
