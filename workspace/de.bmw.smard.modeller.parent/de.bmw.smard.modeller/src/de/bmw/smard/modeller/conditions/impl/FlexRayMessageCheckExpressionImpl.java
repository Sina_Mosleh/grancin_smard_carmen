package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.CheckType;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.FlexRayHeaderFlagSelection;
import de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.BaseValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Flex Ray Message Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getBusId <em>Bus Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getPayloadPreamble <em>Payload Preamble</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getZeroFrame <em>Zero Frame</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getSyncFrame <em>Sync Frame</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getStartupFrame <em>Startup Frame</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getNetworkMgmt <em>Network Mgmt</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getFlexrayMessageId <em>Flexray Message Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getPayloadPreambleTmplParam <em>Payload Preamble Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getZeroFrameTmplParam <em>Zero Frame Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getSyncFrameTmplParam <em>Sync Frame Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getStartupFrameTmplParam <em>Startup Frame Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getNetworkMgmtTmplParam <em>Network Mgmt Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getBusIdTmplParam <em>Bus Id Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getType <em>Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.FlexRayMessageCheckExpressionImpl#getTypeTmplParam <em>Type Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FlexRayMessageCheckExpressionImpl extends ExpressionImpl implements FlexRayMessageCheckExpression {
    /**
     * The default value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusId()
     * @generated
     * @ordered
     */
    protected static final String BUS_ID_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getBusId() <em>Bus Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusId()
     * @generated
     * @ordered
     */
    protected String busId = BUS_ID_EDEFAULT;

    /**
     * The default value of the '{@link #getPayloadPreamble() <em>Payload Preamble</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPayloadPreamble()
     * @generated
     * @ordered
     */
    protected static final FlexRayHeaderFlagSelection PAYLOAD_PREAMBLE_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

    /**
     * The cached value of the '{@link #getPayloadPreamble() <em>Payload Preamble</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPayloadPreamble()
     * @generated
     * @ordered
     */
    protected FlexRayHeaderFlagSelection payloadPreamble = PAYLOAD_PREAMBLE_EDEFAULT;

    /**
     * The default value of the '{@link #getZeroFrame() <em>Zero Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getZeroFrame()
     * @generated
     * @ordered
     */
    protected static final FlexRayHeaderFlagSelection ZERO_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

    /**
     * The cached value of the '{@link #getZeroFrame() <em>Zero Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getZeroFrame()
     * @generated
     * @ordered
     */
    protected FlexRayHeaderFlagSelection zeroFrame = ZERO_FRAME_EDEFAULT;

    /**
     * The default value of the '{@link #getSyncFrame() <em>Sync Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSyncFrame()
     * @generated
     * @ordered
     */
    protected static final FlexRayHeaderFlagSelection SYNC_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

    /**
     * The cached value of the '{@link #getSyncFrame() <em>Sync Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSyncFrame()
     * @generated
     * @ordered
     */
    protected FlexRayHeaderFlagSelection syncFrame = SYNC_FRAME_EDEFAULT;

    /**
     * The default value of the '{@link #getStartupFrame() <em>Startup Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartupFrame()
     * @generated
     * @ordered
     */
    protected static final FlexRayHeaderFlagSelection STARTUP_FRAME_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

    /**
     * The cached value of the '{@link #getStartupFrame() <em>Startup Frame</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartupFrame()
     * @generated
     * @ordered
     */
    protected FlexRayHeaderFlagSelection startupFrame = STARTUP_FRAME_EDEFAULT;

    /**
     * The default value of the '{@link #getNetworkMgmt() <em>Network Mgmt</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNetworkMgmt()
     * @generated
     * @ordered
     */
    protected static final FlexRayHeaderFlagSelection NETWORK_MGMT_EDEFAULT = FlexRayHeaderFlagSelection.IGNORE;

    /**
     * The cached value of the '{@link #getNetworkMgmt() <em>Network Mgmt</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNetworkMgmt()
     * @generated
     * @ordered
     */
    protected FlexRayHeaderFlagSelection networkMgmt = NETWORK_MGMT_EDEFAULT;

    /**
     * The default value of the '{@link #getFlexrayMessageId() <em>Flexray Message Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFlexrayMessageId()
     * @generated
     * @ordered
     */
    protected static final String FLEXRAY_MESSAGE_ID_EDEFAULT = "<Channel>.<SlotID>.<Offset>.<Repetition>";

    /**
     * The cached value of the '{@link #getFlexrayMessageId() <em>Flexray Message Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFlexrayMessageId()
     * @generated
     * @ordered
     */
    protected String flexrayMessageId = FLEXRAY_MESSAGE_ID_EDEFAULT;

    /**
     * The default value of the '{@link #getPayloadPreambleTmplParam() <em>Payload Preamble Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPayloadPreambleTmplParam()
     * @generated
     * @ordered
     */
    protected static final String PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getPayloadPreambleTmplParam() <em>Payload Preamble Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPayloadPreambleTmplParam()
     * @generated
     * @ordered
     */
    protected String payloadPreambleTmplParam = PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getZeroFrameTmplParam() <em>Zero Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getZeroFrameTmplParam()
     * @generated
     * @ordered
     */
    protected static final String ZERO_FRAME_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getZeroFrameTmplParam() <em>Zero Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getZeroFrameTmplParam()
     * @generated
     * @ordered
     */
    protected String zeroFrameTmplParam = ZERO_FRAME_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getSyncFrameTmplParam() <em>Sync Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSyncFrameTmplParam()
     * @generated
     * @ordered
     */
    protected static final String SYNC_FRAME_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSyncFrameTmplParam() <em>Sync Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSyncFrameTmplParam()
     * @generated
     * @ordered
     */
    protected String syncFrameTmplParam = SYNC_FRAME_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getStartupFrameTmplParam() <em>Startup Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartupFrameTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STARTUP_FRAME_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getStartupFrameTmplParam() <em>Startup Frame Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStartupFrameTmplParam()
     * @generated
     * @ordered
     */
    protected String startupFrameTmplParam = STARTUP_FRAME_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getNetworkMgmtTmplParam() <em>Network Mgmt Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNetworkMgmtTmplParam()
     * @generated
     * @ordered
     */
    protected static final String NETWORK_MGMT_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getNetworkMgmtTmplParam() <em>Network Mgmt Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getNetworkMgmtTmplParam()
     * @generated
     * @ordered
     */
    protected String networkMgmtTmplParam = NETWORK_MGMT_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdTmplParam()
     * @generated
     * @ordered
     */
    protected static final String BUS_ID_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getBusIdTmplParam() <em>Bus Id Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getBusIdTmplParam()
     * @generated
     * @ordered
     */
    protected String busIdTmplParam = BUS_ID_TMPL_PARAM_EDEFAULT;

    /**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected static final CheckType TYPE_EDEFAULT = CheckType.ANY;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected CheckType type = TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String TYPE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTypeTmplParam() <em>Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String typeTmplParam = TYPE_TMPL_PARAM_EDEFAULT;

    /**
     * the valid values for a channel in a flexray message check
     * 
     * @generated NOT
     */
    private static final Set<String> VALID_CHANNEL_STRINGS = new HashSet<String>(Arrays.asList(new String[] {
            "A", "B", "AB"
    }));

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected FlexRayMessageCheckExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.FLEX_RAY_MESSAGE_CHECK_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusId() {
        return busId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusId(String newBusId) {
        String oldBusId = busId;
        busId = newBusId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID, oldBusId, busId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayHeaderFlagSelection getPayloadPreamble() {
        return payloadPreamble;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setPayloadPreamble(FlexRayHeaderFlagSelection newPayloadPreamble) {
        FlexRayHeaderFlagSelection oldPayloadPreamble = payloadPreamble;
        payloadPreamble = newPayloadPreamble == null ? PAYLOAD_PREAMBLE_EDEFAULT : newPayloadPreamble;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE, oldPayloadPreamble, payloadPreamble));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayHeaderFlagSelection getZeroFrame() {
        return zeroFrame;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setZeroFrame(FlexRayHeaderFlagSelection newZeroFrame) {
        FlexRayHeaderFlagSelection oldZeroFrame = zeroFrame;
        zeroFrame = newZeroFrame == null ? ZERO_FRAME_EDEFAULT : newZeroFrame;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME, oldZeroFrame, zeroFrame));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayHeaderFlagSelection getSyncFrame() {
        return syncFrame;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSyncFrame(FlexRayHeaderFlagSelection newSyncFrame) {
        FlexRayHeaderFlagSelection oldSyncFrame = syncFrame;
        syncFrame = newSyncFrame == null ? SYNC_FRAME_EDEFAULT : newSyncFrame;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME, oldSyncFrame, syncFrame));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayHeaderFlagSelection getStartupFrame() {
        return startupFrame;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStartupFrame(FlexRayHeaderFlagSelection newStartupFrame) {
        FlexRayHeaderFlagSelection oldStartupFrame = startupFrame;
        startupFrame = newStartupFrame == null ? STARTUP_FRAME_EDEFAULT : newStartupFrame;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME, oldStartupFrame, startupFrame));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public FlexRayHeaderFlagSelection getNetworkMgmt() {
        return networkMgmt;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNetworkMgmt(FlexRayHeaderFlagSelection newNetworkMgmt) {
        FlexRayHeaderFlagSelection oldNetworkMgmt = networkMgmt;
        networkMgmt = newNetworkMgmt == null ? NETWORK_MGMT_EDEFAULT : newNetworkMgmt;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT, oldNetworkMgmt, networkMgmt));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFlexrayMessageId() {
        return flexrayMessageId;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFlexrayMessageId(String newFlexrayMessageId) {
        String oldFlexrayMessageId = flexrayMessageId;
        flexrayMessageId = newFlexrayMessageId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID, oldFlexrayMessageId, flexrayMessageId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getPayloadPreambleTmplParam() {
        return payloadPreambleTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setPayloadPreambleTmplParam(String newPayloadPreambleTmplParam) {
        String oldPayloadPreambleTmplParam = payloadPreambleTmplParam;
        payloadPreambleTmplParam = newPayloadPreambleTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM, oldPayloadPreambleTmplParam, payloadPreambleTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getZeroFrameTmplParam() {
        return zeroFrameTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setZeroFrameTmplParam(String newZeroFrameTmplParam) {
        String oldZeroFrameTmplParam = zeroFrameTmplParam;
        zeroFrameTmplParam = newZeroFrameTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM, oldZeroFrameTmplParam, zeroFrameTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSyncFrameTmplParam() {
        return syncFrameTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSyncFrameTmplParam(String newSyncFrameTmplParam) {
        String oldSyncFrameTmplParam = syncFrameTmplParam;
        syncFrameTmplParam = newSyncFrameTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM, oldSyncFrameTmplParam, syncFrameTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStartupFrameTmplParam() {
        return startupFrameTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStartupFrameTmplParam(String newStartupFrameTmplParam) {
        String oldStartupFrameTmplParam = startupFrameTmplParam;
        startupFrameTmplParam = newStartupFrameTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM, oldStartupFrameTmplParam, startupFrameTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getNetworkMgmtTmplParam() {
        return networkMgmtTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setNetworkMgmtTmplParam(String newNetworkMgmtTmplParam) {
        String oldNetworkMgmtTmplParam = networkMgmtTmplParam;
        networkMgmtTmplParam = newNetworkMgmtTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM, oldNetworkMgmtTmplParam, networkMgmtTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getBusIdTmplParam() {
        return busIdTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setBusIdTmplParam(String newBusIdTmplParam) {
        String oldBusIdTmplParam = busIdTmplParam;
        busIdTmplParam = newBusIdTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM, oldBusIdTmplParam, busIdTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public CheckType getType() {
        return type;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setType(CheckType newType) {
        CheckType oldType = type;
        type = newType == null ? TYPE_EDEFAULT : newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE, oldType, type));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTypeTmplParam() {
        return typeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTypeTmplParam(String newTypeTmplParam) {
        String oldTypeTmplParam = typeTmplParam;
        typeTmplParam = newTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM, oldTypeTmplParam, typeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidBusId(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_BUS_ID)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(busId)
                        .error("_Validation_Conditions_FlexRayMessageCheckExpression_BusId_IsNull")
                        .build())

                .with(Validators.checkTemplate(busId)
                        .tmplParam(busIdTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_BusId_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_BusIdTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_BusIdTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.busId(FlexRayMessageCheckExpression.class.getSimpleName()))
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidCheckType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_CHECK_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(type)
                        .error("_Validation_Conditions_FlexRayMessageCheckExpression_CheckType_IsNull")
                        .build())

                .with(Validators.checkTemplate(type)
                        .templateType(CheckType.TEMPLATE_DEFINED)
                        .tmplParam(typeTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_CheckType_ContainsParamters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_CheckTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_CheckTypeTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(CheckType.ALL, CheckType.ANY),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_CheckType_IllegalCheckType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStartupFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STARTUP_FRAME)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(startupFrame)
                        .templateType(FlexRayHeaderFlagSelection.TEMPLATE_DEFINED)
                        .tmplParam(startupFrameTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_StartupFrame_ContainsParamters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_StartupFrameTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_StartupFrameTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(
                                FlexRayHeaderFlagSelection.IGNORE,
                                FlexRayHeaderFlagSelection.YES,
                                FlexRayHeaderFlagSelection.NO),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_StartupFrame_IllegalStartupFrameType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSyncFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_SYNC_FRAME)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(syncFrame)
                        .templateType(FlexRayHeaderFlagSelection.TEMPLATE_DEFINED)
                        .tmplParam(syncFrameTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_SyncFrame_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_SyncFrameTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_SyncFrameTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(
                                FlexRayHeaderFlagSelection.IGNORE,
                                FlexRayHeaderFlagSelection.YES,
                                FlexRayHeaderFlagSelection.NO),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_SyncFrame_IllegalSyncFrameType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidZeroFrame(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_ZERO_FRAME)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(zeroFrame)
                        .templateType(FlexRayHeaderFlagSelection.TEMPLATE_DEFINED)
                        .tmplParam(zeroFrameTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_ZeroFrame_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_ZeroFrameTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_ZeroFrameTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(
                                FlexRayHeaderFlagSelection.IGNORE,
                                FlexRayHeaderFlagSelection.YES,
                                FlexRayHeaderFlagSelection.NO),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_ZeroFrame_IllegalZeroFrameType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidPayloadPreamble(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_PAYLOAD_PREAMBLE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(payloadPreamble)
                        .templateType(FlexRayHeaderFlagSelection.TEMPLATE_DEFINED)
                        .tmplParam(payloadPreambleTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_PayloadPreamble_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_PayloadPreambleTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_PayloadPreambleTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(
                                FlexRayHeaderFlagSelection.IGNORE,
                                FlexRayHeaderFlagSelection.YES,
                                FlexRayHeaderFlagSelection.NO),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_PayloadPreamble_IllegalPayloadPreambleType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidNetworkMgmt(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_NETWORK_MGMT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.checkTemplate(networkMgmt)
                        .templateType(FlexRayHeaderFlagSelection.TEMPLATE_DEFINED)
                        .tmplParam(networkMgmtTmplParam)
                        .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_NetworkMgmt_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_FlexRayMessageCheckExpression_NetworkMgmtTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_NetworkMgmtTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(Arrays.asList(
                                FlexRayHeaderFlagSelection.IGNORE,
                                FlexRayHeaderFlagSelection.YES,
                                FlexRayHeaderFlagSelection.NO),
                                "_Validation_Conditions_FlexRayMessageCheckExpression_NetworkMgmt_IllegalNetworkMgmtType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidMessageId(DiagnosticChain diagnostics, Map<Object, Object> context) {
        BaseValidator.WithStep validator = Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_MESSAGE_ID)
                .object(eContainer)
                .diagnostic(diagnostics);

        if (StringUtils.isNotBlank(flexrayMessageId)) {
            validator.with(Validators.stringTemplate(flexrayMessageId)
                    .containsParameterError("_Validation_Conditions_FlexRayMessageCheckExpression_MessageId_ContainsParameters")
                    .invalidPlaceholderError("_Validation_Conditions_FlexRayMessageCheckExpression_MessageId_NotAValidPlaceholderName")
                    .illegalValueError(ValueValidators.flexRayFrameIdRange())
                    .build());
        } else {
            validator.with(Validators.when(StringUtils.nullOrEmpty(flexrayMessageId))
                    .error("_Validation_Conditions_FlexRayMessageCheckExpression_MessageId_IsNull")
                    .build());

        }

        return validator.validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
                return getBusId();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
                return getPayloadPreamble();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
                return getZeroFrame();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
                return getSyncFrame();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
                return getStartupFrame();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
                return getNetworkMgmt();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
                return getFlexrayMessageId();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
                return getPayloadPreambleTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
                return getZeroFrameTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
                return getSyncFrameTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
                return getStartupFrameTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
                return getNetworkMgmtTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
                return getBusIdTmplParam();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
                return getType();
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                return getTypeTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
                setBusId((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
                setPayloadPreamble((FlexRayHeaderFlagSelection) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
                setZeroFrame((FlexRayHeaderFlagSelection) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
                setSyncFrame((FlexRayHeaderFlagSelection) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
                setStartupFrame((FlexRayHeaderFlagSelection) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
                setNetworkMgmt((FlexRayHeaderFlagSelection) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
                setFlexrayMessageId((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
                setPayloadPreambleTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
                setZeroFrameTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
                setSyncFrameTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
                setStartupFrameTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
                setNetworkMgmtTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
                setBusIdTmplParam((String) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
                setType((CheckType) newValue);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                setTypeTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
                setBusId(BUS_ID_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
                setPayloadPreamble(PAYLOAD_PREAMBLE_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
                setZeroFrame(ZERO_FRAME_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
                setSyncFrame(SYNC_FRAME_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
                setStartupFrame(STARTUP_FRAME_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
                setNetworkMgmt(NETWORK_MGMT_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
                setFlexrayMessageId(FLEXRAY_MESSAGE_ID_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
                setPayloadPreambleTmplParam(PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
                setZeroFrameTmplParam(ZERO_FRAME_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
                setSyncFrameTmplParam(SYNC_FRAME_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
                setStartupFrameTmplParam(STARTUP_FRAME_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
                setNetworkMgmtTmplParam(NETWORK_MGMT_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
                setBusIdTmplParam(BUS_ID_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
                setType(TYPE_EDEFAULT);
                return;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                setTypeTmplParam(TYPE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID:
                return BUS_ID_EDEFAULT == null ? busId != null : !BUS_ID_EDEFAULT.equals(busId);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE:
                return payloadPreamble != PAYLOAD_PREAMBLE_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME:
                return zeroFrame != ZERO_FRAME_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME:
                return syncFrame != SYNC_FRAME_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME:
                return startupFrame != STARTUP_FRAME_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT:
                return networkMgmt != NETWORK_MGMT_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__FLEXRAY_MESSAGE_ID:
                return FLEXRAY_MESSAGE_ID_EDEFAULT == null ? flexrayMessageId != null : !FLEXRAY_MESSAGE_ID_EDEFAULT.equals(flexrayMessageId);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__PAYLOAD_PREAMBLE_TMPL_PARAM:
                return PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT == null ? payloadPreambleTmplParam != null : !PAYLOAD_PREAMBLE_TMPL_PARAM_EDEFAULT
                        .equals(payloadPreambleTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__ZERO_FRAME_TMPL_PARAM:
                return ZERO_FRAME_TMPL_PARAM_EDEFAULT == null ? zeroFrameTmplParam != null : !ZERO_FRAME_TMPL_PARAM_EDEFAULT.equals(zeroFrameTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__SYNC_FRAME_TMPL_PARAM:
                return SYNC_FRAME_TMPL_PARAM_EDEFAULT == null ? syncFrameTmplParam != null : !SYNC_FRAME_TMPL_PARAM_EDEFAULT.equals(syncFrameTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__STARTUP_FRAME_TMPL_PARAM:
                return STARTUP_FRAME_TMPL_PARAM_EDEFAULT == null ? startupFrameTmplParam != null : !STARTUP_FRAME_TMPL_PARAM_EDEFAULT
                        .equals(startupFrameTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__NETWORK_MGMT_TMPL_PARAM:
                return NETWORK_MGMT_TMPL_PARAM_EDEFAULT == null ? networkMgmtTmplParam != null : !NETWORK_MGMT_TMPL_PARAM_EDEFAULT.equals(networkMgmtTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__BUS_ID_TMPL_PARAM:
                return BUS_ID_TMPL_PARAM_EDEFAULT == null ? busIdTmplParam != null : !BUS_ID_TMPL_PARAM_EDEFAULT.equals(busIdTmplParam);
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE:
                return type != TYPE_EDEFAULT;
            case ConditionsPackage.FLEX_RAY_MESSAGE_CHECK_EXPRESSION__TYPE_TMPL_PARAM:
                return TYPE_TMPL_PARAM_EDEFAULT == null ? typeTmplParam != null : !TYPE_TMPL_PARAM_EDEFAULT.equals(typeTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (busId: ");
        result.append(busId);
        result.append(", payloadPreamble: ");
        result.append(payloadPreamble);
        result.append(", zeroFrame: ");
        result.append(zeroFrame);
        result.append(", syncFrame: ");
        result.append(syncFrame);
        result.append(", startupFrame: ");
        result.append(startupFrame);
        result.append(", networkMgmt: ");
        result.append(networkMgmt);
        result.append(", flexrayMessageId: ");
        result.append(flexrayMessageId);
        result.append(", payloadPreambleTmplParam: ");
        result.append(payloadPreambleTmplParam);
        result.append(", zeroFrameTmplParam: ");
        result.append(zeroFrameTmplParam);
        result.append(", syncFrameTmplParam: ");
        result.append(syncFrameTmplParam);
        result.append(", startupFrameTmplParam: ");
        result.append(startupFrameTmplParam);
        result.append(", networkMgmtTmplParam: ");
        result.append(networkMgmtTmplParam);
        result.append(", busIdTmplParam: ");
        result.append(busIdTmplParam);
        result.append(", type: ");
        result.append(type);
        result.append(", typeTmplParam: ");
        result.append(typeTmplParam);
        result.append(')');
        return result.toString();
    }

} //FlexRayMessageCheckExpressionImpl
