package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class NamingConventionValidator implements ValueValidator {

    private final String errorMessageIdentifier;

    public NamingConventionValidator(String errorMessageIdentifier) {
        this.errorMessageIdentifier = errorMessageIdentifier;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {
        Collection<String> messagesToSubstitute = new ArrayList<>();
        if(StringUtils.isBlank(value)) {
            messagesToSubstitute.add("Name is empty");
        }

        if(!StringUtils.isValidFileName(value)) {
            messagesToSubstitute.add("Name must not contain special characters");
        }

        if(!Character.isJavaIdentifierStart(value.charAt(0))) {
            messagesToSubstitute.add("Name must start with letter or underscore");
        }

        for(int i = 1; i < value.length(); i++) {
            if(!Character.isJavaIdentifierPart(value.charAt(i))) {
                messagesToSubstitute.add("Name contains invalid characters");
                break;
            }
        }

        if(!messagesToSubstitute.isEmpty()) {
            if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier,
                        String.join(", ", messagesToSubstitute));
            }
            return false;
        }

        return true;
    }
}
