package de.bmw.smard.modeller.util.migration;

import de.bmw.smard.base.util.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;

/**
 * Util class for creation of ProblemView entries during Migration
 * usually IResource.createMarker(s) is used, but during Migration there is no
 * IResource available -> use IWorkspaceRoot instead
 * @author s.riethig
 *
 */
public class MigrationMarkerUtil {
	
	private static final Logger LOGGER = Logger.getLogger(MigrationMarkerUtil.class);

	public static void createInfo(String message) {
		createMarker(IMarker.SEVERITY_INFO, message);
	}
	
	public static void createWarning(String message) {
		createMarker(IMarker.SEVERITY_WARNING, message);
	}
	
	public static void createError(String message) {
		createMarker(IMarker.SEVERITY_ERROR, message);
	}
	
	private static void createMarker(int severity, String message) {
		try {
			if(!StringUtils.nullOrEmpty(message)) {
				IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
				IMarker marker = workspaceRoot.createMarker(IMarker.PROBLEM);
				marker.setAttribute(IMarker.SEVERITY, severity);
				marker.setAttribute(IMarker.MESSAGE, message);
				// delete marker on Modeller close
				marker.setAttribute(IMarker.TRANSIENT, true);
			}
		} catch (CoreException e) {
			LOGGER.error("Unable to create ProblemView entries", e);
		} catch (IllegalStateException e) {
			LOGGER.warn("Could not create ProblemView entry for message: " + message);
		}
	}
}
