package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.impl.ConditionsPackageImpl;
import de.bmw.smard.modeller.statemachine.AbstractAction;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.Action;
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty;
import de.bmw.smard.modeller.statemachine.ComputeVariable;
import de.bmw.smard.modeller.statemachine.ControlAction;
import de.bmw.smard.modeller.statemachine.ControlType;
import de.bmw.smard.modeller.statemachine.DocumentRoot;
import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.ShowVariable;
import de.bmw.smard.modeller.statemachine.SmardTraceElement;
import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.statemachine.StateMachine;
import de.bmw.smard.modeller.statemachine.StateType;
import de.bmw.smard.modeller.statemachine.StatemachineFactory;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.Transition;
import de.bmw.smard.modeller.statemachine.Trigger;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import de.bmw.smard.modeller.statemachineset.impl.StatemachinesetPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class StatemachinePackageImpl extends EPackageImpl implements StatemachinePackage {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass documentRootEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractStateEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass transitionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stateMachineEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass actionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass initialStateEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass stateEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass abstractActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass computeVariableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass showVariableEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass smardTraceElementEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass controlActionEClass = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum triggerEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum stateTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum actionTypeNotEmptyEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum controlTypeEEnum = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private EDataType intOrTemplatePlaceholderEDataType = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
     * package URI value.
     * <p>
     * Note: the correct way to create the package is via the static
     * factory method {@link #init init()}, which also performs
     * initialization of the package, or returns the registered package,
     * if one already exists.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#eNS_URI
     * @see #init()
     * @generated
     */
    private StatemachinePackageImpl() {
        super(eNS_URI, StatemachineFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
     * <p>
     * This method is used to initialize {@link StatemachinePackage#eINSTANCE} when that field is accessed.
     * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static StatemachinePackage init() {
        if (isInited) return (StatemachinePackage) EPackage.Registry.INSTANCE.getEPackage(StatemachinePackage.eNS_URI);

        // Obtain or create and register package
        Object registeredStatemachinePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        StatemachinePackageImpl theStatemachinePackage =
                registeredStatemachinePackage instanceof StatemachinePackageImpl ? (StatemachinePackageImpl) registeredStatemachinePackage : new StatemachinePackageImpl();

        isInited = true;

        // Initialize simple dependencies
        XMLTypePackage.eINSTANCE.eClass();

        // Obtain or create and register interdependencies
        Object registeredPackage = EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);
        StatemachinesetPackageImpl theStatemachinesetPackage =
                (StatemachinesetPackageImpl) (registeredPackage instanceof StatemachinesetPackageImpl ? registeredPackage : StatemachinesetPackage.eINSTANCE);
        registeredPackage = EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);
        ConditionsPackageImpl theConditionsPackage =
                (ConditionsPackageImpl) (registeredPackage instanceof ConditionsPackageImpl ? registeredPackage : ConditionsPackage.eINSTANCE);

        // Create package meta-data objects
        theStatemachinePackage.createPackageContents();
        theStatemachinesetPackage.createPackageContents();
        theConditionsPackage.createPackageContents();

        // Initialize created meta-data
        theStatemachinePackage.initializePackageContents();
        theStatemachinesetPackage.initializePackageContents();
        theConditionsPackage.initializePackageContents();

        // Register package validator
        EValidator.Registry.INSTANCE.put(theStatemachinePackage,
                new EValidator.Descriptor() {
                    @Override
                    public EValidator getEValidator() {
                        return StatemachineValidator.INSTANCE;
                    }
                });

        // Mark meta-data to indicate it can't be changed
        theStatemachinePackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(StatemachinePackage.eNS_URI, theStatemachinePackage);
        return theStatemachinePackage;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getDocumentRoot() {
        return documentRootEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getDocumentRoot_Mixed() {
        return (EAttribute) documentRootEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_XMLNSPrefixMap() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_XSISchemaLocation() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getDocumentRoot_StateMachine() {
        return (EReference) documentRootEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractState() {
        return abstractStateEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractState_Name() {
        return (EAttribute) abstractStateEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractState_Out() {
        return (EReference) abstractStateEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractState_In() {
        return (EReference) abstractStateEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractState_Description() {
        return (EAttribute) abstractStateEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAbstractState_Actions() {
        return (EReference) abstractStateEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractState_StateType() {
        return (EAttribute) abstractStateEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getTransition() {
        return transitionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransition_Name() {
        return (EAttribute) transitionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransition_From() {
        return (EReference) transitionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransition_To() {
        return (EReference) transitionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransition_Conditions() {
        return (EReference) transitionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransition_Description() {
        return (EAttribute) transitionEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransition_LogLevel() {
        return (EAttribute) transitionEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getTransition_Rootcause() {
        return (EAttribute) transitionEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransition_Actions() {
        return (EReference) transitionEClass.getEStructuralFeatures().get(7);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getTransition_Environment() {
        return (EReference) transitionEClass.getEStructuralFeatures().get(8);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getStateMachine() {
        return stateMachineEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStateMachine_Name() {
        return (EAttribute) stateMachineEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStateMachine_InitialState() {
        return (EReference) stateMachineEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStateMachine_States() {
        return (EReference) stateMachineEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getStateMachine_Transitions() {
        return (EReference) stateMachineEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getStateMachine_ActiveAtStart() {
        return (EAttribute) stateMachineEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAction() {
        return actionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAction_ActionType() {
        return (EAttribute) actionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAction_ActionExpression() {
        return (EAttribute) actionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAction_ActionTypeTmplParam() {
        return (EAttribute) actionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getAction_Environment() {
        return (EReference) actionEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getInitialState() {
        return initialStateEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getState() {
        return stateEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getState_StateTypeTmplParam() {
        return (EAttribute) stateEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAbstractAction() {
        return abstractActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractAction_Description() {
        return (EAttribute) abstractActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractAction_Trigger() {
        return (EAttribute) abstractActionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAbstractAction_TriggerTmplParam() {
        return (EAttribute) abstractActionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getComputeVariable() {
        return computeVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getComputeVariable_ActionExpression() {
        return (EAttribute) computeVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getComputeVariable_Target() {
        return (EReference) computeVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getComputeVariable_Operands() {
        return (EReference) computeVariableEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getShowVariable() {
        return showVariableEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getShowVariable_Format() {
        return (EAttribute) showVariableEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getShowVariable_Prefix() {
        return (EAttribute) showVariableEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getShowVariable_Postfix() {
        return (EAttribute) showVariableEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getShowVariable_RootCause() {
        return (EAttribute) showVariableEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getShowVariable_LogLevel() {
        return (EAttribute) showVariableEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getShowVariable_Variable() {
        return (EReference) showVariableEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getShowVariable_Environment() {
        return (EReference) showVariableEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getSmardTraceElement() {
        return smardTraceElementEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getControlAction() {
        return controlActionEClass;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getControlAction_ControlType() {
        return (EAttribute) controlActionEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getControlAction_StateMachines() {
        return (EReference) controlActionEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getControlAction_Observers() {
        return (EReference) controlActionEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getTrigger() {
        return triggerEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getStateType() {
        return stateTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getActionTypeNotEmpty() {
        return actionTypeNotEmptyEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getControlType() {
        return controlTypeEEnum;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EDataType getIntOrTemplatePlaceholder() {
        return intOrTemplatePlaceholderEDataType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public StatemachineFactory getStatemachineFactory() {
        return (StatemachineFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is
     * guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated) return;
        isCreated = true;

        // Create classes and their features
        documentRootEClass = createEClass(DOCUMENT_ROOT);
        createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
        createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
        createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
        createEReference(documentRootEClass, DOCUMENT_ROOT__STATE_MACHINE);

        abstractStateEClass = createEClass(ABSTRACT_STATE);
        createEAttribute(abstractStateEClass, ABSTRACT_STATE__NAME);
        createEReference(abstractStateEClass, ABSTRACT_STATE__OUT);
        createEReference(abstractStateEClass, ABSTRACT_STATE__IN);
        createEAttribute(abstractStateEClass, ABSTRACT_STATE__DESCRIPTION);
        createEReference(abstractStateEClass, ABSTRACT_STATE__ACTIONS);
        createEAttribute(abstractStateEClass, ABSTRACT_STATE__STATE_TYPE);

        transitionEClass = createEClass(TRANSITION);
        createEAttribute(transitionEClass, TRANSITION__NAME);
        createEReference(transitionEClass, TRANSITION__FROM);
        createEReference(transitionEClass, TRANSITION__TO);
        createEReference(transitionEClass, TRANSITION__CONDITIONS);
        createEAttribute(transitionEClass, TRANSITION__DESCRIPTION);
        createEAttribute(transitionEClass, TRANSITION__LOG_LEVEL);
        createEAttribute(transitionEClass, TRANSITION__ROOTCAUSE);
        createEReference(transitionEClass, TRANSITION__ACTIONS);
        createEReference(transitionEClass, TRANSITION__ENVIRONMENT);

        stateMachineEClass = createEClass(STATE_MACHINE);
        createEAttribute(stateMachineEClass, STATE_MACHINE__NAME);
        createEReference(stateMachineEClass, STATE_MACHINE__INITIAL_STATE);
        createEReference(stateMachineEClass, STATE_MACHINE__STATES);
        createEReference(stateMachineEClass, STATE_MACHINE__TRANSITIONS);
        createEAttribute(stateMachineEClass, STATE_MACHINE__ACTIVE_AT_START);

        actionEClass = createEClass(ACTION);
        createEAttribute(actionEClass, ACTION__ACTION_TYPE);
        createEAttribute(actionEClass, ACTION__ACTION_EXPRESSION);
        createEAttribute(actionEClass, ACTION__ACTION_TYPE_TMPL_PARAM);
        createEReference(actionEClass, ACTION__ENVIRONMENT);

        initialStateEClass = createEClass(INITIAL_STATE);

        stateEClass = createEClass(STATE);
        createEAttribute(stateEClass, STATE__STATE_TYPE_TMPL_PARAM);

        abstractActionEClass = createEClass(ABSTRACT_ACTION);
        createEAttribute(abstractActionEClass, ABSTRACT_ACTION__DESCRIPTION);
        createEAttribute(abstractActionEClass, ABSTRACT_ACTION__TRIGGER);
        createEAttribute(abstractActionEClass, ABSTRACT_ACTION__TRIGGER_TMPL_PARAM);

        computeVariableEClass = createEClass(COMPUTE_VARIABLE);
        createEAttribute(computeVariableEClass, COMPUTE_VARIABLE__ACTION_EXPRESSION);
        createEReference(computeVariableEClass, COMPUTE_VARIABLE__TARGET);
        createEReference(computeVariableEClass, COMPUTE_VARIABLE__OPERANDS);

        showVariableEClass = createEClass(SHOW_VARIABLE);
        createEAttribute(showVariableEClass, SHOW_VARIABLE__FORMAT);
        createEAttribute(showVariableEClass, SHOW_VARIABLE__PREFIX);
        createEAttribute(showVariableEClass, SHOW_VARIABLE__POSTFIX);
        createEAttribute(showVariableEClass, SHOW_VARIABLE__ROOT_CAUSE);
        createEAttribute(showVariableEClass, SHOW_VARIABLE__LOG_LEVEL);
        createEReference(showVariableEClass, SHOW_VARIABLE__VARIABLE);
        createEReference(showVariableEClass, SHOW_VARIABLE__ENVIRONMENT);

        smardTraceElementEClass = createEClass(SMARD_TRACE_ELEMENT);

        controlActionEClass = createEClass(CONTROL_ACTION);
        createEAttribute(controlActionEClass, CONTROL_ACTION__CONTROL_TYPE);
        createEReference(controlActionEClass, CONTROL_ACTION__STATE_MACHINES);
        createEReference(controlActionEClass, CONTROL_ACTION__OBSERVERS);

        // Create enums
        triggerEEnum = createEEnum(TRIGGER);
        stateTypeEEnum = createEEnum(STATE_TYPE);
        actionTypeNotEmptyEEnum = createEEnum(ACTION_TYPE_NOT_EMPTY);
        controlTypeEEnum = createEEnum(CONTROL_TYPE);

        // Create data types
        intOrTemplatePlaceholderEDataType = createEDataType(INT_OR_TEMPLATE_PLACEHOLDER);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This
     * method is guarded to have no affect on any invocation but its first.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized) return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Obtain other dependent packages
        ConditionsPackage theConditionsPackage = (ConditionsPackage) EPackage.Registry.INSTANCE.getEPackage(ConditionsPackage.eNS_URI);
        StatemachinesetPackage theStatemachinesetPackage = (StatemachinesetPackage) EPackage.Registry.INSTANCE.getEPackage(StatemachinesetPackage.eNS_URI);
        XMLTypePackage theXMLTypePackage = (XMLTypePackage) EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        abstractStateEClass.getESuperTypes().add(theConditionsPackage.getBaseClassWithID());
        abstractStateEClass.getESuperTypes().add(theConditionsPackage.getIVariableReaderWriter());
        abstractStateEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        transitionEClass.getESuperTypes().add(theConditionsPackage.getBaseClassWithID());
        transitionEClass.getESuperTypes().add(this.getSmardTraceElement());
        transitionEClass.getESuperTypes().add(theConditionsPackage.getIVariableReaderWriter());
        transitionEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        stateMachineEClass.getESuperTypes().add(theConditionsPackage.getBaseClassWithID());
        stateMachineEClass.getESuperTypes().add(theConditionsPackage.getIVariableReaderWriter());
        stateMachineEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        actionEClass.getESuperTypes().add(this.getAbstractAction());
        actionEClass.getESuperTypes().add(this.getSmardTraceElement());
        initialStateEClass.getESuperTypes().add(this.getAbstractState());
        stateEClass.getESuperTypes().add(this.getAbstractState());
        abstractActionEClass.getESuperTypes().add(theConditionsPackage.getBaseClassWithID());
        abstractActionEClass.getESuperTypes().add(theConditionsPackage.getIVariableReaderWriter());
        abstractActionEClass.getESuperTypes().add(theStatemachinesetPackage.getBusMessageReferable());
        computeVariableEClass.getESuperTypes().add(this.getAbstractAction());
        computeVariableEClass.getESuperTypes().add(theConditionsPackage.getIOperation());
        showVariableEClass.getESuperTypes().add(this.getAbstractAction());
        showVariableEClass.getESuperTypes().add(this.getSmardTraceElement());
        controlActionEClass.getESuperTypes().add(this.getAbstractAction());

        // Initialize classes and features; add operations and parameters
        initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null,
                IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getDocumentRoot_StateMachine(), this.getStateMachine(), null, "stateMachine", null, 0, 1, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
                IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

        initEClass(abstractStateEClass, AbstractState.class, "AbstractState", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractState_Name(), ecorePackage.getEString(), "name", null, 0, 1, AbstractState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractState_Out(), this.getTransition(), this.getTransition_From(), "out", null, 0, -1, AbstractState.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractState_In(), this.getTransition(), this.getTransition_To(), "in", null, 0, -1, AbstractState.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractState_Description(), ecorePackage.getEString(), "description", null, 0, 1, AbstractState.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAbstractState_Actions(), this.getAbstractAction(), null, "actions", null, 0, -1, AbstractState.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractState_StateType(), this.getStateType(), "stateType", "INFO", 1, 1, AbstractState.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        EOperation op = addEOperation(abstractStateEClass, ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        EGenericType g1 = createEGenericType(ecorePackage.getEMap());
        EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(abstractStateEClass, ecorePackage.getEBoolean(), "isValid_hasValidName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(transitionEClass, Transition.class, "Transition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getTransition_Name(), ecorePackage.getEString(), "name", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransition_From(), this.getAbstractState(), this.getAbstractState_Out(), "from", null, 1, 1, Transition.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransition_To(), this.getAbstractState(), this.getAbstractState_In(), "to", null, 1, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransition_Conditions(), theConditionsPackage.getCondition(), null, "conditions", null, 1, -1, Transition.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTransition_Description(), ecorePackage.getEString(), "description", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTransition_LogLevel(), this.getIntOrTemplatePlaceholder(), "logLevel", "0", 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getTransition_Rootcause(), ecorePackage.getEString(), "rootcause", null, 0, 1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransition_Actions(), this.getAbstractAction(), null, "actions", null, 0, -1, Transition.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getTransition_Environment(), theConditionsPackage.getAbstractVariable(), null, "environment", null, 0, 1, Transition.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(transitionEClass, ecorePackage.getEBoolean(), "isValid_hasValidDescription", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(transitionEClass, ecorePackage.getEBoolean(), "isValid_hasValidName", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(transitionEClass, ecorePackage.getEBoolean(), "isValid_hasValidLogLevel", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(transitionEClass, ecorePackage.getEBoolean(), "isValid_hasValidRootCause", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(transitionEClass, ecorePackage.getEBoolean(), "isValid_hasValidConditions", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(stateMachineEClass, StateMachine.class, "StateMachine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getStateMachine_Name(), ecorePackage.getEString(), "name", "StateMachine", 0, 1, StateMachine.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStateMachine_InitialState(), this.getInitialState(), null, "initialState", null, 1, 1, StateMachine.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStateMachine_States(), this.getState(), null, "states", null, 0, -1, StateMachine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
                IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getStateMachine_Transitions(), this.getTransition(), null, "transitions", null, 0, -1, StateMachine.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getStateMachine_ActiveAtStart(), ecorePackage.getEBoolean(), "activeAtStart", "true", 1, 1, StateMachine.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(actionEClass, Action.class, "Action", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAction_ActionType(), this.getActionTypeNotEmpty(), "actionType", "COMPUTE", 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAction_ActionExpression(), ecorePackage.getEString(), "actionExpression", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAction_ActionTypeTmplParam(), ecorePackage.getEString(), "actionTypeTmplParam", null, 0, 1, Action.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getAction_Environment(), theConditionsPackage.getAbstractVariable(), null, "environment", null, 0, 1, Action.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(actionEClass, ecorePackage.getEBoolean(), "isValid_hasValidActionExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(actionEClass, ecorePackage.getEBoolean(), "isValid_hasValidActionType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(initialStateEClass, InitialState.class, "InitialState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        op = addEOperation(initialStateEClass, ecorePackage.getEBoolean(), "isValid_hasValidNoInTransition", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(stateEClass, State.class, "State", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getState_StateTypeTmplParam(), ecorePackage.getEString(), "stateTypeTmplParam", null, 0, 1, State.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(stateEClass, ecorePackage.getEBoolean(), "isValid_hasValidType", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(abstractActionEClass, AbstractAction.class, "AbstractAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAbstractAction_Description(), ecorePackage.getEString(), "description", null, 0, 1, AbstractAction.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractAction_Trigger(), this.getTrigger(), "trigger", "ON_ENTRY", 1, 1, AbstractAction.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getAbstractAction_TriggerTmplParam(), ecorePackage.getEString(), "triggerTmplParam", null, 0, 1, AbstractAction.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        op = addEOperation(abstractActionEClass, ecorePackage.getEBoolean(), "isValid_hasValidTrigger", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(computeVariableEClass, ComputeVariable.class, "ComputeVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getComputeVariable_ActionExpression(), ecorePackage.getEString(), "actionExpression", null, 0, 1, ComputeVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getComputeVariable_Target(), theConditionsPackage.getValueVariable(), null, "target", null, 1, 1, ComputeVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getComputeVariable_Operands(), theConditionsPackage.getIComputeVariableActionOperand(), null, "operands", null, 0, -1,
                ComputeVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);

        op = addEOperation(computeVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidOperands", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(computeVariableEClass, ecorePackage.getEBoolean(), "isValid_hasValidActionExpression", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEDiagnosticChain(), "diagnostics", 0, 1, IS_UNIQUE, IS_ORDERED);
        g1 = createEGenericType(ecorePackage.getEMap());
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        g2 = createEGenericType(ecorePackage.getEJavaObject());
        g1.getETypeArguments().add(g2);
        addEParameter(op, g1, "context", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(showVariableEClass, ShowVariable.class, "ShowVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getShowVariable_Format(), ecorePackage.getEString(), "format", null, 0, 1, ShowVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getShowVariable_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, ShowVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getShowVariable_Postfix(), ecorePackage.getEString(), "postfix", null, 0, 1, ShowVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getShowVariable_RootCause(), ecorePackage.getEString(), "rootCause", null, 0, 1, ShowVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getShowVariable_LogLevel(), this.getIntOrTemplatePlaceholder(), "logLevel", null, 0, 1, ShowVariable.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getShowVariable_Variable(), theConditionsPackage.getVariable(), null, "variable", null, 1, 1, ShowVariable.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getShowVariable_Environment(), theConditionsPackage.getAbstractVariable(), null, "environment", null, 0, 1, ShowVariable.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(smardTraceElementEClass, SmardTraceElement.class, "SmardTraceElement", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

        addEOperation(smardTraceElementEClass, theXMLTypePackage.getString(), "getIdentifier", 0, 1, IS_UNIQUE, IS_ORDERED);

        op = addEOperation(smardTraceElementEClass, theXMLTypePackage.getString(), "getIdentifier", 0, 1, IS_UNIQUE, IS_ORDERED);
        addEParameter(op, ecorePackage.getEString(), "projectName", 0, 1, IS_UNIQUE, IS_ORDERED);

        initEClass(controlActionEClass, ControlAction.class, "ControlAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getControlAction_ControlType(), this.getControlType(), "controlType", "ON", 1, 1, ControlAction.class, !IS_TRANSIENT, !IS_VOLATILE,
                IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getControlAction_StateMachines(), this.getStateMachine(), null, "stateMachines", null, 0, -1, ControlAction.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getControlAction_Observers(), theConditionsPackage.getAbstractObserver(), null, "observers", null, 0, -1, ControlAction.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(triggerEEnum, Trigger.class, "Trigger");
        addEEnumLiteral(triggerEEnum, Trigger.TEMPLATEDEFINED);
        addEEnumLiteral(triggerEEnum, Trigger.ON_ENTRY);
        addEEnumLiteral(triggerEEnum, Trigger.ON_EXIT);

        initEEnum(stateTypeEEnum, StateType.class, "StateType");
        addEEnumLiteral(stateTypeEEnum, StateType.TEMPLATE_DEFINED);
        addEEnumLiteral(stateTypeEEnum, StateType.INFO);
        addEEnumLiteral(stateTypeEEnum, StateType.WARN);
        addEEnumLiteral(stateTypeEEnum, StateType.DEFECT);
        addEEnumLiteral(stateTypeEEnum, StateType.INIT);
        addEEnumLiteral(stateTypeEEnum, StateType.OK);

        initEEnum(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.class, "ActionTypeNotEmpty");
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.TEMPLATEDEFINED);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.COMPUTE);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.SHOWVARIABLE);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.FLEXRAY);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.CAN);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.LIN);
        addEEnumLiteral(actionTypeNotEmptyEEnum, ActionTypeNotEmpty.PLUGIN);

        initEEnum(controlTypeEEnum, ControlType.class, "ControlType");
        addEEnumLiteral(controlTypeEEnum, ControlType.ON);
        addEEnumLiteral(controlTypeEEnum, ControlType.OFF);

        // Initialize data types
        initEDataType(intOrTemplatePlaceholderEDataType, String.class, "IntOrTemplatePlaceholder", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

        // Create resource
        createResource(eNS_URI);

        // Create annotations
        // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
        createExtendedMetaDataAnnotations();
        // http://www.eclipse.org/edapt
        createEdaptAnnotations();
        // http:///de/bmw/smard/modeller/TemplateMetaData
        createTemplateMetaDataAnnotations();
    }

    /**
     * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createExtendedMetaDataAnnotations() {
        String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";
        addAnnotation(this,
                source,
                new String[] {
                        "qualified", "true"
                });
        addAnnotation(documentRootEClass,
                source,
                new String[] {
                        "name", "",
                        "kind", "mixed"
                });
        addAnnotation(getDocumentRoot_Mixed(),
                source,
                new String[] {
                        "kind", "elementWildcard",
                        "name", ":mixed"
                });
        addAnnotation(getDocumentRoot_XMLNSPrefixMap(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "xmlns:prefix"
                });
        addAnnotation(getDocumentRoot_XSISchemaLocation(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "xsi:schemaLocation"
                });
        addAnnotation(getDocumentRoot_StateMachine(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "stateMachine",
                        "namespace", "##targetNamespace"
                });
        addAnnotation(abstractStateEClass,
                source,
                new String[] {
                        "kind", "mixed",
                        "name", "AbstractState"
                });
        addAnnotation(getAbstractState_Name(),
                source,
                new String[] {
                        "kind", "attribute"
                });
        addAnnotation(getAbstractState_Out(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "out"
                });
        addAnnotation(getAbstractState_In(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "in"
                });
        addAnnotation(getAbstractState_Actions(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "Action"
                });
        addAnnotation(getAbstractState_StateType(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "StateType"
                });
        addAnnotation(getTransition_Name(),
                source,
                new String[] {
                        "kind", "attribute"
                });
        addAnnotation(getTransition_From(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "from"
                });
        addAnnotation(getTransition_To(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "to"
                });
        addAnnotation(getTransition_Conditions(),
                source,
                new String[] {
                        "kind", "attribute",
                        "name", "conditions"
                });
        addAnnotation(getTransition_Actions(),
                source,
                new String[] {
                        "kind", "element",
                        "name", "Action"
                });
        addAnnotation(stateMachineEClass,
                source,
                new String[] {
                        "kind", "element",
                        "name", "stateMachine"
                });
        addAnnotation(getStateMachine_Name(),
                source,
                new String[] {
                        "kind", "attribute"
                });
        addAnnotation(getStateMachine_States(),
                source,
                new String[] {
                        "kind", "element"
                });
        addAnnotation(getStateMachine_Transitions(),
                source,
                new String[] {
                        "kind", "element"
                });
        addAnnotation(actionEClass,
                source,
                new String[] {
                        "kind", "elementOnly",
                        "name", "Action"
                });
        addAnnotation(stateEClass,
                source,
                new String[] {
                        "name", "State"
                });
    }

    /**
     * Initializes the annotations for <b>http://www.eclipse.org/edapt</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createEdaptAnnotations() {
        String source = "http://www.eclipse.org/edapt";
        addAnnotation(this,
                source,
                new String[] {
                        "historyURI", "modeller.history"
                });
    }

    /**
     * Initializes the annotations for <b>http:///de/bmw/smard/modeller/TemplateMetaData</b>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void createTemplateMetaDataAnnotations() {
        String source = "http:///de/bmw/smard/modeller/TemplateMetaData";
        addAnnotation(getAbstractState_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractState_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractState_StateType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getTransition_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getTransition_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getTransition_LogLevel(),
                source,
                new String[] {
                        "attrType", "single"
                });
        addAnnotation(getTransition_Rootcause(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getStateMachine_Name(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAction_ActionType(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getAction_ActionExpression(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractAction_Description(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getAbstractAction_Trigger(),
                source,
                new String[] {
                        "attrType", "enum"
                });
        addAnnotation(getComputeVariable_ActionExpression(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getShowVariable_Format(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getShowVariable_Prefix(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getShowVariable_Postfix(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getShowVariable_RootCause(),
                source,
                new String[] {
                        "attrType", "multiple"
                });
        addAnnotation(getShowVariable_LogLevel(),
                source,
                new String[] {
                        "attrType", "single"
                });
    }

} //StatemachinePackageImpl
