package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Format Data Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getFormatDataType()
 * @model
 * @generated
 */
public enum FormatDataType implements Enumerator {
    /**
     * The '<em><b>Templatedefined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATEDEFINED(-1, "Templatedefined", "TemplateDefined"),

    /**
     * The '<em><b>Float</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #FLOAT_VALUE
     * @generated
     * @ordered
     */
    FLOAT(0, "Float", "Float"),

    /**
     * The '<em><b>Dec</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DEC_VALUE
     * @generated
     * @ordered
     */
    DEC(1, "Dec", "Dec"),

    /**
     * The '<em><b>Hex</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #HEX_VALUE
     * @generated
     * @ordered
     */
    HEX(2, "Hex", "Hex"),

    /**
     * The '<em><b>Bool</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #BOOL_VALUE
     * @generated
     * @ordered
     */
    BOOL(3, "Bool", "Bool"),

    /**
     * The '<em><b>Bin</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #BIN_VALUE
     * @generated
     * @ordered
     */
    BIN(4, "Bin", "Bin"),

    /**
     * The '<em><b>Oct</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #OCT_VALUE
     * @generated
     * @ordered
     */
    OCT(5, "Oct", "Oct"),

    /**
     * The '<em><b>Round</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ROUND_VALUE
     * @generated
     * @ordered
     */
    ROUND(6, "Round", "Round"),

    /**
     * The '<em><b>Ceil</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #CEIL_VALUE
     * @generated
     * @ordered
     */
    CEIL(7, "Ceil", "Ceil"),

    /**
     * The '<em><b>Hhmmss</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #HHMMSS_VALUE
     * @generated
     * @ordered
     */
    HHMMSS(8, "hhmmss", "hhmmss"),

    /**
     * The '<em><b>Ms</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #MS_VALUE
     * @generated
     * @ordered
     */
    MS(9, "ms", "ms"),

    /**
     * The '<em><b>IP</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #IP_VALUE
     * @generated
     * @ordered
     */
    IP(10, "IP", "IP"),

    /**
     * The '<em><b>Mac</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #MAC_VALUE
     * @generated
     * @ordered
     */
    MAC(11, "Mac", "Mac");

    /**
     * The '<em><b>Templatedefined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED
     * @model name="Templatedefined" literal="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATEDEFINED_VALUE = -1;

    /**
     * The '<em><b>Float</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #FLOAT
     * @model name="Float"
     * @generated
     * @ordered
     */
    public static final int FLOAT_VALUE = 0;

    /**
     * The '<em><b>Dec</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #DEC
     * @model name="Dec"
     * @generated
     * @ordered
     */
    public static final int DEC_VALUE = 1;

    /**
     * The '<em><b>Hex</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #HEX
     * @model name="Hex"
     * @generated
     * @ordered
     */
    public static final int HEX_VALUE = 2;

    /**
     * The '<em><b>Bool</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #BOOL
     * @model name="Bool"
     * @generated
     * @ordered
     */
    public static final int BOOL_VALUE = 3;

    /**
     * The '<em><b>Bin</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #BIN
     * @model name="Bin"
     * @generated
     * @ordered
     */
    public static final int BIN_VALUE = 4;

    /**
     * The '<em><b>Oct</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #OCT
     * @model name="Oct"
     * @generated
     * @ordered
     */
    public static final int OCT_VALUE = 5;

    /**
     * The '<em><b>Round</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ROUND
     * @model name="Round"
     * @generated
     * @ordered
     */
    public static final int ROUND_VALUE = 6;

    /**
     * The '<em><b>Ceil</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #CEIL
     * @model name="Ceil"
     * @generated
     * @ordered
     */
    public static final int CEIL_VALUE = 7;

    /**
     * The '<em><b>Hhmmss</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #HHMMSS
     * @model name="hhmmss"
     * @generated
     * @ordered
     */
    public static final int HHMMSS_VALUE = 8;

    /**
     * The '<em><b>Ms</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #MS
     * @model name="ms"
     * @generated
     * @ordered
     */
    public static final int MS_VALUE = 9;

    /**
     * The '<em><b>IP</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #IP
     * @model
     * @generated
     * @ordered
     */
    public static final int IP_VALUE = 10;

    /**
     * The '<em><b>Mac</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #MAC
     * @model name="Mac"
     * @generated
     * @ordered
     */
    public static final int MAC_VALUE = 11;

    /**
     * An array of all the '<em><b>Format Data Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final FormatDataType[] VALUES_ARRAY =
            new FormatDataType[] {
                    TEMPLATEDEFINED,
                    FLOAT,
                    DEC,
                    HEX,
                    BOOL,
                    BIN,
                    OCT,
                    ROUND,
                    CEIL,
                    HHMMSS,
                    MS,
                    IP,
                    MAC,
            };

    /**
     * A public read-only list of all the '<em><b>Format Data Type</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<FormatDataType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Format Data Type</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FormatDataType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            FormatDataType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Format Data Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FormatDataType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            FormatDataType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Format Data Type</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static FormatDataType get(int value) {
        switch (value) {
            case TEMPLATEDEFINED_VALUE:
                return TEMPLATEDEFINED;
            case FLOAT_VALUE:
                return FLOAT;
            case DEC_VALUE:
                return DEC;
            case HEX_VALUE:
                return HEX;
            case BOOL_VALUE:
                return BOOL;
            case BIN_VALUE:
                return BIN;
            case OCT_VALUE:
                return OCT;
            case ROUND_VALUE:
                return ROUND;
            case CEIL_VALUE:
                return CEIL;
            case HHMMSS_VALUE:
                return HHMMSS;
            case MS_VALUE:
                return MS;
            case IP_VALUE:
                return IP;
            case MAC_VALUE:
                return MAC;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private FormatDataType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

    /**
     * @generated NOT
     */
    public boolean hasCaseOption() {
        return this == HEX || this == BOOL || this == MAC;
    }

    /**
     * @generated NOT
     */
    public boolean hasDigits() {
        return (this == FLOAT || this == DEC || this == HEX || this == BIN || this == OCT || this == ROUND
            || this == CEIL);
    }


} //FormatDataType
