package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.SignalReferenceSet;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import java.util.Collection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Signal Reference Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.SignalReferenceSetImpl#getMessages <em>Messages</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SignalReferenceSetImpl extends EObjectImpl implements SignalReferenceSet {
    /**
     * The cached value of the '{@link #getMessages() <em>Messages</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMessages()
     * @generated
     * @ordered
     */
    protected EList<AbstractMessage> messages;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected SignalReferenceSetImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.SIGNAL_REFERENCE_SET;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<AbstractMessage> getMessages() {
        if (messages == null) {
            messages = new EObjectContainmentEList<AbstractMessage>(AbstractMessage.class, this, ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES);
        }
        return messages;
    }

    /**
     * @generated NOT
     */
    @Override
    public boolean addMessage(AbstractMessage message) {
        if (messages == null) {
            messages = new EObjectContainmentEList<AbstractMessage>(AbstractMessage.class, this, ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES);
        }
        if (!messages.contains(message)) {
            messages.add(message);
            return true;
        }
        return false;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES:
                return ((InternalEList<?>) getMessages()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES:
                return getMessages();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES:
                getMessages().clear();
                getMessages().addAll((Collection<? extends AbstractMessage>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES:
                getMessages().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.SIGNAL_REFERENCE_SET__MESSAGES:
                return messages != null && !messages.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //SignalReferenceSetImpl
