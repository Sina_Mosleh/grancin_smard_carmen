package de.bmw.smard.modeller.statemachine.impl;

import static de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty.COMPUTE;
import static de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty.SHOWVARIABLE;

import com.google.common.collect.Lists;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.PluginActionExpressionParser;
import de.bmw.smard.common.data.SendMessageActionExpressionParser;
import de.bmw.smard.common.data.ShowVariableActionExpressionParser;
import de.bmw.smard.common.data.Variables;
import de.bmw.smard.common.math.Assignment;
import de.bmw.smard.common.math.Term;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractVariable;
import de.bmw.smard.modeller.parser.MathParser;
import de.bmw.smard.modeller.statemachine.Action;
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.statemachine.util.StatemachineValidator;
import de.bmw.smard.modeller.util.*;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.apache.log4j.Logger;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl#getActionType <em>Action Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl#getActionExpression <em>Action Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl#getActionTypeTmplParam <em>Action Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ActionImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionImpl extends AbstractActionImpl implements Action {

    /**
     * @generated NOT
     **/
    private static final Logger LOGGER = Logger.getLogger(ActionImpl.class);

    /**
     * The default value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionType()
     * @generated
     * @ordered
     */
    protected static final ActionTypeNotEmpty ACTION_TYPE_EDEFAULT = ActionTypeNotEmpty.COMPUTE;

    /**
     * The cached value of the '{@link #getActionType() <em>Action Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionType()
     * @generated
     * @ordered
     */
    protected ActionTypeNotEmpty actionType = ACTION_TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionExpression()
     * @generated
     * @ordered
     */
    protected static final String ACTION_EXPRESSION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getActionExpression() <em>Action Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionExpression()
     * @generated
     * @ordered
     */
    protected String actionExpression = ACTION_EXPRESSION_EDEFAULT;

    /**
     * The default value of the '{@link #getActionTypeTmplParam() <em>Action Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String ACTION_TYPE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getActionTypeTmplParam() <em>Action Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getActionTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String actionTypeTmplParam = ACTION_TYPE_TMPL_PARAM_EDEFAULT;

    /**
     * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEnvironment()
     * @generated
     * @ordered
     */
    protected AbstractVariable environment;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    protected ActionImpl() {
        super();
        EMFUtils.assignNewUniqueObjectId(this);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        if (getEnvironment() != null) {
            messages.addAll(getEnvironment().getAllReferencedBusMessages());
        }
        String actionExpression = getActionExpression();
        if (getActionType() == SHOWVARIABLE) {
            String varName = (String) ShowVariableActionExpressionParser.parseShowVariableParameters(actionExpression)
                    .get(ShowVariableActionExpressionParser.PARAM_KEY_VALUE);
            AbstractVariable variable = VariableManager.getInstance(this).getVariableByName(varName);
            if (variable != null) {
                messages.addAll(variable.getAllReferencedBusMessages());
            }
        } else if (getActionType() == COMPUTE) {
            EList<String> variableNames = ActionExpressionUtils.extractVariableNames(actionExpression);
            for (String varName : variableNames) {
                AbstractVariable var = VariableManager.getInstance(this).getVariableByName(varName);
                if (var != null) {
                    messages.addAll(var.getAllReferencedBusMessages());
                }
            }
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.ACTION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public String getIdentifier() {
        return SmardTraceElementIdentifierHelper.getIdentifier(this);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public String getIdentifier(String projectName) {
        return SmardTraceElementIdentifierHelper.getIdentifier(this, projectName);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ActionTypeNotEmpty getActionType() {
        return actionType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActionType(ActionTypeNotEmpty newActionType) {
        ActionTypeNotEmpty oldActionType = actionType;
        actionType = newActionType == null ? ACTION_TYPE_EDEFAULT : newActionType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_TYPE, oldActionType, actionType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getActionExpression() {
        return actionExpression;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActionExpression(String newActionExpression) {
        String oldActionExpression = actionExpression;
        actionExpression = newActionExpression;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_EXPRESSION, oldActionExpression, actionExpression));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getActionTypeTmplParam() {
        return actionTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setActionTypeTmplParam(String newActionTypeTmplParam) {
        String oldActionTypeTmplParam = actionTypeTmplParam;
        actionTypeTmplParam = newActionTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM, oldActionTypeTmplParam, actionTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AbstractVariable getEnvironment() {
        if (environment != null && environment.eIsProxy()) {
            InternalEObject oldEnvironment = (InternalEObject) environment;
            environment = (AbstractVariable) eResolveProxy(oldEnvironment);
            if (environment != oldEnvironment) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.ACTION__ENVIRONMENT, oldEnvironment, environment));
            }
        }
        return environment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public AbstractVariable basicGetEnvironment() {
        return environment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEnvironment(AbstractVariable newEnvironment) {
        AbstractVariable oldEnvironment = environment;
        environment = newEnvironment;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.ACTION__ENVIRONMENT, oldEnvironment, environment));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidActionExpression(DiagnosticChain diagnostics, Map<Object, Object> context) {
        String errorMessageIdentifier = null;
        String messageToSubstitute = null;

        Term term = null;

        if (TemplateUtils.getTemplateCategorization(this).isExportable()) {
            // NOT template context

            if (TemplateUtils.containsParameters(actionExpression)) {
                errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression_ContainsParameters";
            } else if (actionType == ActionTypeNotEmpty.CAN || actionType == ActionTypeNotEmpty.LIN
                || actionType == ActionTypeNotEmpty.FLEXRAY) {
                    try {
                        SendMessageActionExpressionParser.parseParameters(actionType.getLiteral(), actionExpression);
                    } catch (ActionExpressionParseException e) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = e.getMessage();
                    }
                } else if (actionType == ActionTypeNotEmpty.SHOWVARIABLE) {
                    Map<String, Object> parseResult = ShowVariableActionExpressionParser
                            .parseShowVariableParameters(actionExpression);
                    if (parseResult.containsKey(ShowVariableActionExpressionParser.KEY_ERROR)) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = (String) parseResult.get(ShowVariableActionExpressionParser.KEY_ERROR);
                    }
                    String value = "value";
                    if (!parseResult.containsKey(value) || StringUtils.nullOrEmpty((String) parseResult.get(value))) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = "missing keyword " + value + " in show variable action expression";
                    } else {
                        value = (String) parseResult.get(value);
                        ActionExpressionUtils.checkAndWarnForDeprecatedSystemVariableUsage(Collections.singletonList(value),
                                actionExpression, this, diagnostics, context);
                    }

                } else if (actionType == ActionTypeNotEmpty.COMPUTE) {
                    try {
                        term = MathParser.parse(actionExpression, null);
                    } catch (ActionExpressionParseException e) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = "math parsing action expression failed: " + e.getMessage();
                    }

                    if (!(term instanceof Assignment)) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = "action expression term is not an assignment";
                    }

                    List<String> assignedVariableNames = null;
                    if (errorMessageIdentifier == null) {
                        assignedVariableNames = term.getAssignedVariableNames();
                        if (assignedVariableNames == null || assignedVariableNames.size() != 1) {
                            errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                            messageToSubstitute = "no variable to assign to";
                        }
                    }

                    if (errorMessageIdentifier == null) {
                        String variableName = assignedVariableNames.get(0);
                        if (Variables.isPreDefinedName(variableName)) {
                            errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                            messageToSubstitute = "can't assign to a system variable: " + variableName;
                        }
                    }
                } else if (actionType == ActionTypeNotEmpty.PLUGIN) {
                    PluginActionExpressionParser parser = PluginActionExpressionParser.parse(actionExpression);

                    if (parser.hasError()) {
                        errorMessageIdentifier = "_Validation_Statemachine_Action_ActionExpression";
                        messageToSubstitute = parser.getError();
                    }
                }
        }

        else {
            // IN template context
            // todo validation in template context
        }

        if (term != null) {
            ActionExpressionUtils.checkAndWarnForDeprecatedSystemVariableUsage(
                    term.getReferencedVariableNames(), actionExpression, this, diagnostics, context);
        }

        if (errorMessageIdentifier != null) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                        .code(StatemachineValidator.ACTION__IS_VALID_HAS_VALID_ACTION_EXPRESSION)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, messageToSubstitute)
                        .data(this)
                        .build());
            }
            return false;
        }
        return true;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public boolean isValid_hasValidActionType(DiagnosticChain diagnostics, Map<Object, Object> context) {

        List<ActionTypeNotEmpty> legalActionTypes = Lists.newArrayList(ActionTypeNotEmpty.COMPUTE,
                ActionTypeNotEmpty.CAN, ActionTypeNotEmpty.SHOWVARIABLE, ActionTypeNotEmpty.LIN,
                ActionTypeNotEmpty.FLEXRAY, ActionTypeNotEmpty.PLUGIN);
        if (actionType != ActionTypeNotEmpty.SHOWVARIABLE && environment != null) {
            try {
                IMarker marker = ResourcesPlugin.getWorkspace().getRoot().createMarker(IMarker.PROBLEM);
                marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
                marker.setAttribute(IMarker.MESSAGE,
                        "Environment is set but Action Type is not SHOWVARIABLE. Environment will be ignored!");
            } catch (Exception e) {
                // to be ignored
            }
        }
        return Validator.builder()
                .source(StatemachineValidator.DIAGNOSTIC_SOURCE)
                .code(StatemachineValidator.ACTION__IS_VALID_HAS_VALID_ACTION_TYPE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(actionType)
                        .error("_Validation_Statemachine_Action_ActionType_IsNull")
                        .build())

                .with(Validators.checkTemplate(actionType)
                        .templateType(ActionTypeNotEmpty.TEMPLATEDEFINED)
                        .tmplParam(actionTypeTmplParam)
                        .containsParameterError("_Validation_Statemachine_Action_ActionType_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Statemachine_Action_ActionTypeTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Statemachine_Action_ActionTypeTmplParam_NotAValidPlaceholderName")
                        .illegalValueError(legalActionTypes, "_Validation_Statemachine_Action_ActionType_IllegalActionType")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.ACTION__ACTION_TYPE:
                return getActionType();
            case StatemachinePackage.ACTION__ACTION_EXPRESSION:
                return getActionExpression();
            case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
                return getActionTypeTmplParam();
            case StatemachinePackage.ACTION__ENVIRONMENT:
                if (resolve) return getEnvironment();
                return basicGetEnvironment();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.ACTION__ACTION_TYPE:
                setActionType((ActionTypeNotEmpty) newValue);
                return;
            case StatemachinePackage.ACTION__ACTION_EXPRESSION:
                setActionExpression((String) newValue);
                return;
            case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
                setActionTypeTmplParam((String) newValue);
                return;
            case StatemachinePackage.ACTION__ENVIRONMENT:
                setEnvironment((AbstractVariable) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ACTION__ACTION_TYPE:
                setActionType(ACTION_TYPE_EDEFAULT);
                return;
            case StatemachinePackage.ACTION__ACTION_EXPRESSION:
                setActionExpression(ACTION_EXPRESSION_EDEFAULT);
                return;
            case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
                setActionTypeTmplParam(ACTION_TYPE_TMPL_PARAM_EDEFAULT);
                return;
            case StatemachinePackage.ACTION__ENVIRONMENT:
                setEnvironment((AbstractVariable) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.ACTION__ACTION_TYPE:
                return actionType != ACTION_TYPE_EDEFAULT;
            case StatemachinePackage.ACTION__ACTION_EXPRESSION:
                return ACTION_EXPRESSION_EDEFAULT == null ? actionExpression != null : !ACTION_EXPRESSION_EDEFAULT.equals(actionExpression);
            case StatemachinePackage.ACTION__ACTION_TYPE_TMPL_PARAM:
                return ACTION_TYPE_TMPL_PARAM_EDEFAULT == null ? actionTypeTmplParam != null : !ACTION_TYPE_TMPL_PARAM_EDEFAULT.equals(actionTypeTmplParam);
            case StatemachinePackage.ACTION__ENVIRONMENT:
                return environment != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (actionType: ");
        result.append(actionType);
        result.append(", actionExpression: ");
        result.append(actionExpression);
        result.append(", actionTypeTmplParam: ");
        result.append(actionTypeTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        // resolve proxies first
        actionType = getActionType();
        actionExpression = getActionExpression();

        if (StringUtils.nullOrEmpty(actionExpression))
            return ECollections.emptyEList();

        if (actionType.equals(ActionTypeNotEmpty.SHOWVARIABLE)) {
            EList<String> result = new BasicEList<String>();
            Map<String, Object> parseResult = ShowVariableActionExpressionParser
                    .parseShowVariableParameters(actionExpression);
            if (parseResult.containsKey(ShowVariableActionExpressionParser.PARAM_KEY_VALUE)) {
                result.add((String) parseResult.get(ShowVariableActionExpressionParser.PARAM_KEY_VALUE));
            }
            if (getEnvironment() != null) {
                result.add(environment.getName());
                result.addAll(environment.getReadVariables());
            }
            return result;
        } else if (actionType.equals(ActionTypeNotEmpty.COMPUTE)) {
            return ActionExpressionUtils.extractReadVariableNames(actionExpression, null, false);

        } else if (actionType.equals(ActionTypeNotEmpty.CAN) ||
            actionType.equals(ActionTypeNotEmpty.LIN) ||
            actionType.equals(ActionTypeNotEmpty.FLEXRAY)) {
                String variableName = SendMessageActionExpressionParser.parseParameters(
                        actionType.getLiteral(), actionExpression);
                return ECollections.singletonEList(variableName);

            } else if (actionType.equals(ActionTypeNotEmpty.PLUGIN)) {
                // TODO sr
            }

        return ECollections.emptyEList();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        // resolve proxies first
        actionType = getActionType();
        actionExpression = getActionExpression();

        if (actionExpression != null) {
            if (actionType.equals(ActionTypeNotEmpty.COMPUTE)) {
                Term term = null;
                try {
                    term = MathParser.parse(actionExpression, null);
                } catch (ActionExpressionParseException e) {
                    LOGGER.error(e.getMessage());
                }
                if (term instanceof Assignment) {
                    if (term.getAssignedVariableNames() != null && term.getAssignedVariableNames().size() == 1) {
                        String variableName = term.getAssignedVariableNames().get(0);
                        return ECollections.singletonEList(variableName);
                    }
                }
            }
        }
        return ECollections.emptyEList();
    }

} // ActionImpl
