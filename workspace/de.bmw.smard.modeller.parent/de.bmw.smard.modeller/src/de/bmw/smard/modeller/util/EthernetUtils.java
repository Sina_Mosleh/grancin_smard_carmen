package de.bmw.smard.modeller.util;

import java.util.regex.Pattern;

public final class EthernetUtils {
	
	private static final String IP_PART = "([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
    private static final String IP_ADDRESS_PATTERN = new StringBuilder()
		.append("^(").append(IP_PART).append("|(").append(IP_PART).append("-").append(IP_PART).append(")|[\\*])\\.")
		.append("(").append(IP_PART).append("|(").append(IP_PART).append("-").append(IP_PART).append(")|[\\*])\\.")
		.append("(").append(IP_PART).append("|(").append(IP_PART).append("-").append(IP_PART).append(")|[\\*])\\.")
		.append("(").append(IP_PART).append("|(").append(IP_PART).append("-").append(IP_PART).append(")|[\\*])$").toString();
    
    private static final String MAC_ADDRESS_PATTERN = "/^([0-9a-f]{2}([:-]|$)){6}$/i";

 
    /**
     * Validate ip address with regular expression
     * @param ip ip address for validation
     * @return true valid ip address, false invalid ip address
     */
    public static boolean checkIP(final String ip){
    	return Pattern.matches(IP_ADDRESS_PATTERN,ip);	    	 
    }
     
    public static boolean checkMAC(final String mac){
    	return Pattern.matches(MAC_ADDRESS_PATTERN,mac);	
    }

 	/** no instance */
 	EthernetUtils() {}

}
