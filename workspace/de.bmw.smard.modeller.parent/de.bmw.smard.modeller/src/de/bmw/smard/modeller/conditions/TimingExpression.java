package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timing Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime <em>Maxtime</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.TimingExpression#getMintime <em>Mintime</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTimingExpression()
 * @model extendedMetaData="name='timingCondition' kind='empty'"
 * @generated
 */
public interface TimingExpression extends Expression {
    /**
     * Returns the value of the '<em><b>Maxtime</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Maxtime</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Maxtime</em>' attribute.
     * @see #isSetMaxtime()
     * @see #unsetMaxtime()
     * @see #setMaxtime(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTimingExpression_Maxtime()
     * @model default="0" unsettable="true" dataType="de.bmw.smard.modeller.conditions.LongOrTemplatePlaceholder"
     *        extendedMetaData="kind='attribute' name='maxtime' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getMaxtime();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime <em>Maxtime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Maxtime</em>' attribute.
     * @see #isSetMaxtime()
     * @see #unsetMaxtime()
     * @see #getMaxtime()
     * @generated
     */
    void setMaxtime(String value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime <em>Maxtime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetMaxtime()
     * @see #getMaxtime()
     * @see #setMaxtime(String)
     * @generated
     */
    void unsetMaxtime();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMaxtime <em>Maxtime</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Maxtime</em>' attribute is set.
     * @see #unsetMaxtime()
     * @see #getMaxtime()
     * @see #setMaxtime(String)
     * @generated
     */
    boolean isSetMaxtime();

    /**
     * Returns the value of the '<em><b>Mintime</b></em>' attribute.
     * The default value is <code>"0"</code>.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Mintime</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Mintime</em>' attribute.
     * @see #isSetMintime()
     * @see #unsetMintime()
     * @see #setMintime(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getTimingExpression_Mintime()
     * @model default="0" unsettable="true" dataType="de.bmw.smard.modeller.conditions.LongOrTemplatePlaceholder"
     *        extendedMetaData="kind='attribute' name='mintime' namespace='##targetNamespace'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getMintime();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMintime <em>Mintime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Mintime</em>' attribute.
     * @see #isSetMintime()
     * @see #unsetMintime()
     * @see #getMintime()
     * @generated
     */
    void setMintime(String value);

    /**
     * Unsets the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMintime <em>Mintime</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #isSetMintime()
     * @see #getMintime()
     * @see #setMintime(String)
     * @generated
     */
    void unsetMintime();

    /**
     * Returns whether the value of the '{@link de.bmw.smard.modeller.conditions.TimingExpression#getMintime <em>Mintime</em>}' attribute is set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return whether the value of the '<em>Mintime</em>' attribute is set.
     * @see #unsetMintime()
     * @see #getMintime()
     * @see #setMintime(String)
     * @generated
     */
    boolean isSetMintime();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMinMaxTimes(DiagnosticChain diagnostics, Map<Object, Object> context);

} // TimingExpression
