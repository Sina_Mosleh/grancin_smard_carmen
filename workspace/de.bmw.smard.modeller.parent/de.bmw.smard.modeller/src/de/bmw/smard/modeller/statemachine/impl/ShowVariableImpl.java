package de.bmw.smard.modeller.statemachine.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractVariable;
import de.bmw.smard.modeller.conditions.Variable;
import de.bmw.smard.modeller.statemachine.ShowVariable;
import de.bmw.smard.modeller.statemachine.StatemachinePackage;
import de.bmw.smard.modeller.util.SmardTraceElementIdentifierHelper;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Show Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getFormat <em>Format</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getPrefix <em>Prefix</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getPostfix <em>Postfix</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getRootCause <em>Root Cause</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getLogLevel <em>Log Level</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getVariable <em>Variable</em>}</li>
 * <li>{@link de.bmw.smard.modeller.statemachine.impl.ShowVariableImpl#getEnvironment <em>Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ShowVariableImpl extends AbstractActionImpl implements ShowVariable {
    /**
     * The default value of the '{@link #getFormat() <em>Format</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFormat()
     * @generated
     * @ordered
     */
    protected static final String FORMAT_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getFormat() <em>Format</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getFormat()
     * @generated
     * @ordered
     */
    protected String format = FORMAT_EDEFAULT;

    /**
     * The default value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPrefix()
     * @generated
     * @ordered
     */
    protected static final String PREFIX_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getPrefix() <em>Prefix</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPrefix()
     * @generated
     * @ordered
     */
    protected String prefix = PREFIX_EDEFAULT;

    /**
     * The default value of the '{@link #getPostfix() <em>Postfix</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPostfix()
     * @generated
     * @ordered
     */
    protected static final String POSTFIX_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getPostfix() <em>Postfix</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getPostfix()
     * @generated
     * @ordered
     */
    protected String postfix = POSTFIX_EDEFAULT;

    /**
     * The default value of the '{@link #getRootCause() <em>Root Cause</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRootCause()
     * @generated
     * @ordered
     */
    protected static final String ROOT_CAUSE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getRootCause() <em>Root Cause</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRootCause()
     * @generated
     * @ordered
     */
    protected String rootCause = ROOT_CAUSE_EDEFAULT;

    /**
     * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLogLevel()
     * @generated
     * @ordered
     */
    protected static final String LOG_LEVEL_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getLogLevel()
     * @generated
     * @ordered
     */
    protected String logLevel = LOG_LEVEL_EDEFAULT;

    /**
     * This is true if the Log Level attribute has been set.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    protected boolean logLevelESet;

    /**
     * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getVariable()
     * @generated
     * @ordered
     */
    protected Variable variable;

    /**
     * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEnvironment()
     * @generated
     * @ordered
     */
    protected AbstractVariable environment;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ShowVariableImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        if (getVariable() != null) {
            messages.addAll(getVariable().getAllReferencedBusMessages());
        }
        if (getEnvironment() != null) {
            messages.addAll(getEnvironment().getAllReferencedBusMessages());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinePackage.Literals.SHOW_VARIABLE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public String getIdentifier() {
        return SmardTraceElementIdentifierHelper.getIdentifier(this);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public String getIdentifier(String projectName) {
        return SmardTraceElementIdentifierHelper.getIdentifier(this, projectName);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getFormat() {
        return format;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setFormat(String newFormat) {
        String oldFormat = format;
        format = newFormat;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__FORMAT, oldFormat, format));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getPrefix() {
        return prefix;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setPrefix(String newPrefix) {
        String oldPrefix = prefix;
        prefix = newPrefix;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__PREFIX, oldPrefix, prefix));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getPostfix() {
        return postfix;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setPostfix(String newPostfix) {
        String oldPostfix = postfix;
        postfix = newPostfix;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__POSTFIX, oldPostfix, postfix));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getRootCause() {
        return rootCause;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setRootCause(String newRootCause) {
        String oldRootCause = rootCause;
        rootCause = newRootCause;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__ROOT_CAUSE, oldRootCause, rootCause));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLogLevel() {
        return logLevel;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setLogLevel(String newLogLevel) {
        String oldLogLevel = logLevel;
        logLevel = newLogLevel;
        boolean oldLogLevelESet = logLevelESet;
        logLevelESet = true;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL, oldLogLevel, logLevel, !oldLogLevelESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void unsetLogLevel() {
        String oldLogLevel = logLevel;
        boolean oldLogLevelESet = logLevelESet;
        logLevel = LOG_LEVEL_EDEFAULT;
        logLevelESet = false;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.UNSET, StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL, oldLogLevel, LOG_LEVEL_EDEFAULT, oldLogLevelESet));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean isSetLogLevel() {
        return logLevelESet;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Variable getVariable() {
        if (variable != null && variable.eIsProxy()) {
            InternalEObject oldVariable = (InternalEObject) variable;
            variable = (Variable) eResolveProxy(oldVariable);
            if (variable != oldVariable) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.SHOW_VARIABLE__VARIABLE, oldVariable, variable));
            }
        }
        return variable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public Variable basicGetVariable() {
        return variable;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setVariable(Variable newVariable) {
        Variable oldVariable = variable;
        variable = newVariable;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__VARIABLE, oldVariable, variable));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AbstractVariable getEnvironment() {
        if (environment != null && environment.eIsProxy()) {
            InternalEObject oldEnvironment = (InternalEObject) environment;
            environment = (AbstractVariable) eResolveProxy(oldEnvironment);
            if (environment != oldEnvironment) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT, oldEnvironment, environment));
            }
        }
        return environment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public AbstractVariable basicGetEnvironment() {
        return environment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEnvironment(AbstractVariable newEnvironment) {
        AbstractVariable oldEnvironment = environment;
        environment = newEnvironment;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT, oldEnvironment, environment));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinePackage.SHOW_VARIABLE__FORMAT:
                return getFormat();
            case StatemachinePackage.SHOW_VARIABLE__PREFIX:
                return getPrefix();
            case StatemachinePackage.SHOW_VARIABLE__POSTFIX:
                return getPostfix();
            case StatemachinePackage.SHOW_VARIABLE__ROOT_CAUSE:
                return getRootCause();
            case StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL:
                return getLogLevel();
            case StatemachinePackage.SHOW_VARIABLE__VARIABLE:
                if (resolve) return getVariable();
                return basicGetVariable();
            case StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT:
                if (resolve) return getEnvironment();
                return basicGetEnvironment();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinePackage.SHOW_VARIABLE__FORMAT:
                setFormat((String) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__PREFIX:
                setPrefix((String) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__POSTFIX:
                setPostfix((String) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__ROOT_CAUSE:
                setRootCause((String) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL:
                setLogLevel((String) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__VARIABLE:
                setVariable((Variable) newValue);
                return;
            case StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT:
                setEnvironment((AbstractVariable) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinePackage.SHOW_VARIABLE__FORMAT:
                setFormat(FORMAT_EDEFAULT);
                return;
            case StatemachinePackage.SHOW_VARIABLE__PREFIX:
                setPrefix(PREFIX_EDEFAULT);
                return;
            case StatemachinePackage.SHOW_VARIABLE__POSTFIX:
                setPostfix(POSTFIX_EDEFAULT);
                return;
            case StatemachinePackage.SHOW_VARIABLE__ROOT_CAUSE:
                setRootCause(ROOT_CAUSE_EDEFAULT);
                return;
            case StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL:
                unsetLogLevel();
                return;
            case StatemachinePackage.SHOW_VARIABLE__VARIABLE:
                setVariable((Variable) null);
                return;
            case StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT:
                setEnvironment((AbstractVariable) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinePackage.SHOW_VARIABLE__FORMAT:
                return FORMAT_EDEFAULT == null ? format != null : !FORMAT_EDEFAULT.equals(format);
            case StatemachinePackage.SHOW_VARIABLE__PREFIX:
                return PREFIX_EDEFAULT == null ? prefix != null : !PREFIX_EDEFAULT.equals(prefix);
            case StatemachinePackage.SHOW_VARIABLE__POSTFIX:
                return POSTFIX_EDEFAULT == null ? postfix != null : !POSTFIX_EDEFAULT.equals(postfix);
            case StatemachinePackage.SHOW_VARIABLE__ROOT_CAUSE:
                return ROOT_CAUSE_EDEFAULT == null ? rootCause != null : !ROOT_CAUSE_EDEFAULT.equals(rootCause);
            case StatemachinePackage.SHOW_VARIABLE__LOG_LEVEL:
                return isSetLogLevel();
            case StatemachinePackage.SHOW_VARIABLE__VARIABLE:
                return variable != null;
            case StatemachinePackage.SHOW_VARIABLE__ENVIRONMENT:
                return environment != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (format: ");
        result.append(format);
        result.append(", prefix: ");
        result.append(prefix);
        result.append(", postfix: ");
        result.append(postfix);
        result.append(", rootCause: ");
        result.append(rootCause);
        result.append(", logLevel: ");
        if (logLevelESet) result.append(logLevel);
        else result.append("<unset>");
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getReadVariables() {
        EList<String> result = new BasicEList<String>();
        if (getVariable() != null) {
            result.add(variable.getName());
            result.addAll(variable.getReadVariables());
        }
        if (getEnvironment() != null) {
            result.add(environment.getName());
            result.addAll(environment.getReadVariables());
        }
        return result;
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<String> getWriteVariables() {
        return ECollections.emptyEList();
    }

} //ShowVariableImpl
