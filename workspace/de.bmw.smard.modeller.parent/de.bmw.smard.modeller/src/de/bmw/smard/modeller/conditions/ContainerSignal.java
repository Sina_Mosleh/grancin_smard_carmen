package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.ContainerSignal#getContainedSignals <em>Contained Signals</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getContainerSignal()
 * @model
 * @generated
 */
public interface ContainerSignal extends AbstractSignal {
    /**
     * Returns the value of the '<em><b>Contained Signals</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.AbstractSignal}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Contained Signals</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Contained Signals</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getContainerSignal_ContainedSignals()
     * @model containment="true"
     * @generated
     */
    EList<AbstractSignal> getContainedSignals();

} // ContainerSignal
