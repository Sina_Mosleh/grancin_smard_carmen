package de.bmw.smard.modeller.validation;

public enum Severity {
    OK(0),
    INFO(0x1),
    WARNING(0x2),
    ERROR(0x4),
    CANCEL(0x8);

    private final int value;

    public int value() {
        return value;
    }

    Severity(int value) {
        this.value = value;
    }
}
