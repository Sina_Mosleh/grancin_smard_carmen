package de.bmw.smard.modeller.validation;

import de.bmw.smard.base.util.Precondition;
import de.bmw.smard.modeller.statemachineset.StateMachineSet;

import java.io.File;

public class ValidatorConfig {

    private StateMachineSet stateMachineSet;
    private File source;

    public StateMachineSet getStateMachineSet() {
        return stateMachineSet;
    }

    private void setStateMachineSet(StateMachineSet stateMachineSet) {
        this.stateMachineSet = stateMachineSet;
    }

    public File getSource() {
        return source;
    }

    private void setSource(File source) {
        this.source = source;
    }

    private ValidatorConfig() {
    }

    public static ValidatorConfigBuilder builder() {
        return new ValidatorConfigBuilder();
    }

    public static class ValidatorConfigBuilder {

        private StateMachineSet stateMachineSet;
        private File source;


        public ValidatorConfigBuilder stateMachineSet(StateMachineSet stateMachineSet) {
            this.stateMachineSet = stateMachineSet;
            return this;
        }

        /***
         * Sets either source directory of project or artifact archive
         * @param source artifact archive or root directory of project
         * @return
         */
        public ValidatorConfigBuilder source(File source) {
            this.source = source;
            return this;
        }

        public ValidatorConfig build() {
            Precondition.notNull(stateMachineSet, "stateMachineSet must not be null");
            Precondition.notNull(source, "source must not be null");
            Precondition.check(source.exists(), "source %s does not exist", source);

            ValidatorConfig config = new ValidatorConfig();
            config.setStateMachineSet(stateMachineSet);
            config.setSource(source);
            return config;
        }
    }

}
