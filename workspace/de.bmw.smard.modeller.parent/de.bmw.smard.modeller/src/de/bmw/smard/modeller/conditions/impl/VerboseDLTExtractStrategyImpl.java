package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Verbose DLT Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VerboseDLTExtractStrategyImpl extends ExtractStrategyImpl implements VerboseDLTExtractStrategy {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected VerboseDLTExtractStrategyImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VERBOSE_DLT_EXTRACT_STRATEGY;
    }

} //VerboseDLTExtractStrategyImpl
