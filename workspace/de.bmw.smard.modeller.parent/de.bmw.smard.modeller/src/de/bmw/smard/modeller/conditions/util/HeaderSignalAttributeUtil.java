package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.interfaces.bus.MessageType;
import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.ConditionsFactory;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.config.OsiLayerConfig;
import de.bmw.smard.modeller.util.TemplateUtils;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeaderSignalAttributeUtil {

    private static final String ANNOTATION = "http:///de/bmw/smard/modeller/HeaderSignalAttribute";
    private static final String PAYLOAD_LENGTH = "payloadLength";
    private static final String TEMPLATE_DEFINED_ATTR = "TemplateDefined";
    private static final String ATTR_TYPE = "attrType";
    private static final String DLC_PREFIX = "DLC";

    private static List<AbstractFilter> possibleFilters;

    private static List<AbstractFilter> getPossibleFilters() {
        if (possibleFilters == null) {
            setPossibleFilters();
        }
        return possibleFilters;
    }

    private static void setPossibleFilters() {
        possibleFilters = Arrays.asList(
                ConditionsFactory.eINSTANCE.createCANFilter(),
                ConditionsFactory.eINSTANCE.createLINFilter(),
                ConditionsFactory.eINSTANCE.createFlexRayFilter(),
                ConditionsFactory.eINSTANCE.createEthernetFilter(),
                ConditionsFactory.eINSTANCE.createIPv4Filter(),
                ConditionsFactory.eINSTANCE.createTCPFilter(),
                ConditionsFactory.eINSTANCE.createUDPFilter(),
                ConditionsFactory.eINSTANCE.createUDPNMFilter(),
                ConditionsFactory.eINSTANCE.createSomeIPFilter(),
                ConditionsFactory.eINSTANCE.createSomeIPSDFilter(),
                ConditionsFactory.eINSTANCE.createDLTFilter(),
                ConditionsFactory.eINSTANCE.createNonVerboseDLTFilter(),
                ConditionsFactory.eINSTANCE.createPluginFilter());
    }

    /**
     * @param message
     * @return list of attributes applicable for a header signal
     */
    public static List<String> getAttributesForMessage(AbstractMessage message) {
        List<String> result = new ArrayList<>();

        result.addAll(HeaderSignalStaticAttributes.getStaticAttributes(MessageType.UNKNOWN_BUS_NAME).keySet());
        if (!TemplateUtils.getTemplateCategorization(message).isExportable()) {
            result.add(TEMPLATE_DEFINED_ATTR);
        }

        List<AbstractFilter> applicableFilters = getApplicableFilters(message);
        for (AbstractFilter filter : applicableFilters) {
            MessageType messageType = OsiLayerConfig.getMessageType(filter);
            for (EAttribute attribute : filter.eClass().getEAllAttributes()) {
                if (isAvailableAsHeaderSignalAttribute(attribute)) {
                    if (PAYLOAD_LENGTH.equals(attribute.getName())) {
                        result.add(HeaderSignalPayloadLengthQualifier.getName(messageType));
                    } else {
                        result.add(attribute.getName());
                    }
                }
            }

            // also add static attribute for this message type
            if (HeaderSignalStaticAttributes.getStaticAttributes(messageType) != null) {
                result.addAll(
                        HeaderSignalStaticAttributes.getStaticAttributes(messageType).keySet());
            }
        }

        return result;
    }

    /**
     * @param message
     * @return list of map consisting of a message type and its associated attributes
     */
    public static List<Map<MessageType, List<String>>> getAttributesForMessageSorted(AbstractMessage message) {
        List<Map<MessageType, List<String>>> result = new ArrayList<>();

        // add BudId, Timestamp and - if reasonable - TemplateDefined
        Map<MessageType, List<String>> attributeWithMessageType = new HashMap<>();
        attributeWithMessageType.put(
                MessageType.UNKNOWN_BUS_NAME,
                new ArrayList<String>(HeaderSignalStaticAttributes.getStaticAttributes(MessageType.UNKNOWN_BUS_NAME).keySet()));

        if (!TemplateUtils.getTemplateCategorization(message).isExportable()) {
            List<String> specialAttributes = attributeWithMessageType.get(MessageType.UNKNOWN_BUS_NAME);
            specialAttributes.add(TEMPLATE_DEFINED_ATTR);
        }
        result.add(attributeWithMessageType);

        // add attributes for applicable filters
        List<AbstractFilter> applicableFilters = getApplicableFilters(message);
        for (AbstractFilter filter : applicableFilters) {
            Map<MessageType, List<String>> attributesWithMessageType = new HashMap<>();
            MessageType messageType = OsiLayerConfig.getMessageType(filter);
            attributesWithMessageType.put(messageType, new ArrayList<>());

            // add attributes of filter if they are available as header signal attributes
            for (EAttribute attribute : filter.eClass().getEAllAttributes()) {
                if (isAvailableAsHeaderSignalAttribute(attribute)) {
                    if (PAYLOAD_LENGTH.equals(attribute.getName())) {
                        attributesWithMessageType.get(messageType).add(HeaderSignalPayloadLengthQualifier.getName(messageType));
                    } else {
                        attributesWithMessageType.get(messageType).add(attribute.getName());
                    }
                }
            }

            // add static attributes for this message type (if there are any)
            if (HeaderSignalStaticAttributes.getStaticAttributes(messageType) != null) {
                attributesWithMessageType.get(messageType)
                        .addAll(
                                HeaderSignalStaticAttributes.getStaticAttributes(messageType).keySet());
            }

            result.add(attributesWithMessageType);
        }

        // sort categories: BASE - CAN - LIN - etc.
        result.sort(new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                Map<MessageType, List<String>> map1 = (Map<MessageType, List<String>>) o1;
                Map<MessageType, List<String>> map2 = (Map<MessageType, List<String>>) o2;
                MessageType m1 = map1.keySet().iterator().next();
                MessageType m2 = map2.keySet().iterator().next();

                // UNKNOWN_BUS_NAME equals the category "BASE" -> has to be on top
                if (m1.equals(MessageType.UNKNOWN_BUS_NAME)) {
                    return 1;
                }
                if (m2.equals(MessageType.UNKNOWN_BUS_NAME)) {
                    return 1;
                }
                return m1.compareTo(m2);
            }

        });

        return result;
    }

    /**
     * every header attribute specifies its datatype in its annotation details
     *
     * @param message
     * @param attribute
     * @return
     */
    public static DataType getDataType(AbstractMessage message, String attribute) {
        if (HeaderSignalStaticAttributes.getKeySet().contains(attribute)) {
            return HeaderSignalStaticAttributes.get(attribute);
        }

        // iterate over all applicable filters and their header attributes to get to the annotation
        List<AbstractFilter> applicableFilters = getApplicableFilters(message);
        for (AbstractFilter filter : applicableFilters) {
            for (EAttribute attributeOfFilter : filter.eClass().getEAllAttributes()) {
                if (attributeOfFilter.getName().equals(attribute) ||
                    (HeaderSignalPayloadLengthQualifier.getPayloadLengthPrefixes().contains(attributeOfFilter.getName())) &&
                        isStringAttribute(attribute)) {
                    // the datatype of an attribute is stored in the details of the annotation
                    EAnnotation headerSignalAnnotation =
                            attributeOfFilter.getEAnnotation(ANNOTATION);
                    EMap<String, String> details = headerSignalAnnotation.getDetails();
                    if (details.containsKey(ATTR_TYPE)) {
                        String value = details.get(ATTR_TYPE);
                        return DataType.getByName(value);
                    }
                }
            }
        }
        return null;
    }

    private static boolean isStringAttribute(String attribute) {
        return StringUtils.isNotBlank(attribute) &&
            (attribute.startsWith(PAYLOAD_LENGTH) || attribute.startsWith(DLC_PREFIX));
    }

    public static List<AbstractFilter> getApplicableFilters(AbstractMessage message) {
        List<AbstractFilter> applicableFilters = new ArrayList<>();

        applicableFilters.addAll(alreadyExistingFilters(message));

        for (AbstractFilter filter : getPossibleFilters()) {
            if (OsiLayerConfig.canAddFilterToMessage(filter, message)) {
                applicableFilters.add(filter);
            }
        }

        return applicableFilters;
    }

    private static List<AbstractFilter> alreadyExistingFilters(AbstractMessage message) {
        List<AbstractFilter> result = new ArrayList<>();

        result.add(message.getPrimaryFilter());

        if (message instanceof AbstractBusMessage) {
            result.addAll(((AbstractBusMessage) message).getFilters());
        }

        return result;
    }

    private static boolean isAvailableAsHeaderSignalAttribute(EAttribute attribute) {
        EAnnotation headerSignalAnnotation = attribute.getEAnnotation(ANNOTATION);
        return headerSignalAnnotation != null;
    }

}
