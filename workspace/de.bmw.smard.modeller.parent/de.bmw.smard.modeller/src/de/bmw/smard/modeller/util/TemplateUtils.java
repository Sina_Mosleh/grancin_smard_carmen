package de.bmw.smard.modeller.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.common.data.DataConversions;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;

/**
 * helper class dealing with templates.
 * <p>
 * The basic folder structure is as follows: 
 * <CODE>
 *   ProjectDir
 *     \StateMachineTemplates
 *       \generated
 *       \Templates
 * </CODE>
 * Within the StateMachineTemplates/Templates folder the structure for a template state machine is as follows:
 * <CODE>
 *   ProjectDir\StateMachineTemplates\Templates
 *     \MyTemplateName
 *       \Conditions
 *         \MyTemplateName.conditions
 *       \CSV
 *         \MyTemplateName.csv
 *       \StateMachines
 *         \MyTemplateName.statemachine
 *         \MyTemplateName.statemachine_diagram
 * </CODE> 
 * Within the ObserverTemplates/Templates folder the structure for a template observer is as follows:
 * <CODE>
 *   ProjectDir\ObserverTemplates\Templates
 *     \MyTemplateName
 *       \Conditions
 *         \MyTemplateName.conditions
 *       \CSV
 *         \MyTemplateName.csv
 * </CODE> 
 */
public class TemplateUtils
{

	private static final String DELIMITERS_ARE_UNEVEN = "Delimiters of property value are uneven: ";

	/**
	 * the folder name for the parent directory of both templates and generated statemachines,
	 * this folder lies directly within the project directory
	 */
	public static final String FOLDER_NAME_STATEMACHINE_TEMPLATES = "StateMachineTemplates";

    /**
     * the folder name for the parent directory of both templates and generated observers,
     * this folder lies directly within the project directory
     */
    public static final String FOLDER_NAME_OBSERVER_TEMPLATES = "ObserverTemplates";

	/**
	 * the folder name for the parent directory of generated templates, lies directly within {@link #FOLDER_NAME_STATEMACHINE_TEMPLATES}
	 */
	public static final String FOLDER_NAME_GENERATED = "generated";
	
	/**
	 * the folder name for the parent directory of actual templates, lies directly within {@link #FOLDER_NAME_STATEMACHINE_TEMPLATES}
	 */
	public static final String FOLDER_NAME_TEMPLATES = "Templates";
	
	/**
	 * the folder name for the conditions directory in a template directory 
	 */
	public static final String FOLDER_NAME_CONDITIONS = "Conditions";
	
	/**
	 * the folder name for the CSV directory in a template directory 
	 */
	public static final String FOLDER_NAME_CSV = "CSV";
	
	/**
	 * the folder name for the statemachine directory in a template directory 
	 */
	public static final String FOLDER_NAME_STATEMACHINES = "StateMachines";

	/**
	 * the literal value of the enum value that means the attribute should be template defined 
	 */
	public static final String ENUM_LITERAL_TEMPLATE_DEFINED = "TemplateDefined";
	
	/**
	 * String used in special situations when we need a value in a string property that is not freely user editable 
	 * to indicate that we have a template defined value
	 * (with the parameter name defined in a different property.)
	 * <p>
	 * Note that the value contains # which allows the use of {@link #containsParameters(String)} or {@link #isPlaceholder(String)}
	 * to detect this value and allows use in the IntOrTemplatePlaceholder data types.
	 * <p>
	 * Note that you should use equals when comparing this value, since when loading a model from disk the constant will not be used.
	 * 
	 * @see #TEMPLATE_DEFINED_PLACEHOLDER_LABEL
	 */
	public static final String TEMPLATE_DEFINED_PLACEHOLDER_NAME = "#Template-defined#";
	
	/**
	 * the text to display for {@link #TEMPLATE_DEFINED_PLACEHOLDER_NAME},
	 * used in special handling code.
	 * 
	 * @see #TEMPLATE_DEFINED_PLACEHOLDER_NAME
	 */
	public static final String TEMPLATE_DEFINED_PLACEHOLDER_LABEL = "Template-defined";

	/**
	 * the url that is used in the ecore model annotation 
	 * (and correspondingly in {@link EStructuralFeature#getEAnnotation(String)})
	 * to access the template processing meta information.
	 * The used keys and values are defined here as constants with the name pattern 
	 * <CODE>TEMPLATE_METADATA_KEY*</CODE>
	 * and <CODE>TEMPLATE_METADATA_VALUE*</CODE>
	 */
	public static final String TEMPLATE_METADATA_URL = "http:///de/bmw/smard/modeller/TemplateMetaData";
	
	/**
	 * the key for information about the processing of an attribute in the {@link #TEMPLATE_METADATA_URL}.
	 * Possible values are those i:
	 * <UL>
	 * <LI>{@value #TEMPLATE_METADATA_VALUE_ATTRTYPE_NONE} (default)
	 * <LI>{@value #TEMPLATE_METADATA_VALUE_ATTRTYPE_SINGLE}
	 * <LI>{@value #TEMPLATE_METADATA_VALUE_ATTRTYPE_MULTIPLE}
	 * <LI>{@value #TEMPLATE_METADATA_VALUE_ATTRTYPE_ENUM}
	 * <LI>{@value #TEMPLATE_METADATA_VALUE_ATTRTYPE_SPECIAL}
	 * </UL> 
	 */
	public static final String TEMPLATE_METADATA_KEY_ATTRTYPE = "attrType";
	
    public static final String VALUE_MUST_BE_GIVEN = "value must be given";
    public static final String VALUE_MUST_NOT_BE_A_PLACEHOLDER = "value must not be a placeholder";
	
	/**
	 * categorization of an attribute with respect to its treatement in the template process.
	 * T
	 */
	public enum TemplateAttributeCharacterization 
	{
		
		/**
		 * the value for {@value #TEMPLATE_METADATA_KEY_ATTRTYPE}
		 * for an attribute that should not be ignored in special template processing.
		 * This corresponds to the default if no value is given.
		 */
		NONE("none"),
		
		/**
		 * the value for {@link #TEMPLATE_METADATA_KEY_ATTRTYPE}
		 * for an attribute that may contain a single template placeholder. The attribute type
		 * is something like IntOrTemplatePlaceholder with a java type of {@link String}.
		 */
		SINGLE("single"),
		
		/**
		 * the value for {@link #TEMPLATE_METADATA_KEY_ATTRTYPE}
		 * for an attribute that may contain multiple template placeholders. The attribute type
		 * is usually an unrestricted text string. 
		 */
		MULTIPLE("multiple"),
		
		/**
		 * the value for {@link #TEMPLATE_METADATA_KEY_ATTRTYPE}
		 * for an enum attribute that has one dedicated literal value 
		 * {@link #ENUM_LITERAL_TEMPLATE_DEFINED}.
		 * If this is used, the actual name of the placeholder is stored in an attribute
		 * that has the same name with a suffix of "TmplParam".
		 */
		ENUM("enum"),
		
		/**
		 * the value for {@value #TEMPLATE_METADATA_KEY_ATTRTYPE}
		 * for an enum attribute that need special treatment.
		 */
		SPECIAL("special");
		
		/**
		 * the literal value that is used as value in the {@link TemplateUtils#TEMPLATE_METADATA_KEY_ATTRTYPE} 
		 */
		private String mLiteralValue;
		
		private TemplateAttributeCharacterization(String pLiteralValue)
		{
			mLiteralValue = pLiteralValue;
		}
		
		public String getLiteralValue()
		{
			return mLiteralValue;
		}
	}
	
	/**
	 * Categorization of files with respect to templates
	 */
	public enum TemplateCategorization
	{
		/**
		 * the file is a template, i.e. it is in StateMachineTemplates/Templates folder,
		 * and may contain template parameter tags.
		 * It may refer to any other file.
		 */
		TEMPLATE,
		
		/**
		 * the file is generated from a template, i.e. it is in the StateMachineTemplate/generated folder,
		 * and may not contain template parameter tags. It may refer only to other GENERATED and NORMAL files.  
		 */
		GENERATED,
		
		/**
		 * the file is neither TEMPLATE nor GENERATED, i.e. it is most likely a manually created state machine or condition file.
		 * It may refer only to other GENERATED and NORMAL files.
		 */
		NORMAL;
		
		/**
		 * Checks if a reference from this template categorization to the given one is allowed.
		 * @param other the categorization to reference
		 * @return true if a reference is allowed
		 */
		public boolean allowsReferenceTo(TemplateCategorization other)
		{
			if (other == NORMAL) {
				return true;
			}
			if (this == TEMPLATE) {
				return other != GENERATED;
			}
			if (this == GENERATED) {
				return other != TEMPLATE;
			}
			return false;
		}

		/**
		 * @return true if a file with this categorization can be exported
		 */
		public boolean isExportable()
		{
			if (this==NORMAL || this==GENERATED)
			{
				return true;
			}
			return false;
		}
		
		/**
		 * @return true if a file with this categorization can include template placeholders
		 */
		public boolean allowsTemplateParameters()
		{
			if (this==TEMPLATE)
			{
				return true;
			}
			return false;
		}
		
		public List<TemplateCategorization> getReferencableTempleCats() 
		{
			List<TemplateCategorization> templeCats = new ArrayList<TemplateCategorization>();
			templeCats.add(this);			
			if ( this != NORMAL ) {
				templeCats.add(NORMAL);
			}
			return templeCats;
		}		
	}
	
	/**
	 * no instance
	 */
	private TemplateUtils() {}

	/**
	 * checks which category the file of the given object belongs to, 
	 * will default to {@link TemplateCategorization#NORMAL} if it cannot be determined.
	 * 
	 * @param obj an object in a file
	 * @return the corresponding template categorization, never null
	 */
	public static TemplateCategorization getTemplateCategorization(EObject obj)
	{
		if (obj==null)
		{
			return TemplateCategorization.NORMAL;
		}
		return getTemplateCategorization(obj.eResource());
	}

	private static String segmentsToString(URI uri) {
		assert uri != null;
		List<String> segments = uri.segmentsList();
		if ( segments != null) {
			StringBuilder sb = new StringBuilder ();
			for ( String segment : segments  ) {
				sb.append("/");
				sb.append(segment);
			}
			return sb.toString();
		}
		return "";
	}

	public static TemplateCategorization getTemplateCategorization(URI uri) {
		if (uri == null)
			return null;
		
		String uriSegments = segmentsToString(uri);
		if (StringUtils.nullOrEmpty(uriSegments))
			return null;
		
		return getTemplateCategorizationFor(uriSegments);
	}
	
	public static TemplateCategorization getTemplateCategorizationFor(String uriSegmentString) {
		assert !StringUtils.nullOrEmpty(uriSegmentString);
		
		if (uriSegmentString.contains(FOLDER_NAME_STATEMACHINE_TEMPLATES + "/" + FOLDER_NAME_TEMPLATES + "/")) {
			return TemplateCategorization.TEMPLATE;
		}

		if (uriSegmentString.contains(FOLDER_NAME_STATEMACHINE_TEMPLATES + "/" + FOLDER_NAME_GENERATED + "/")) {
			return TemplateCategorization.GENERATED;
		}

		if (uriSegmentString.contains(FOLDER_NAME_OBSERVER_TEMPLATES + "/" + FOLDER_NAME_TEMPLATES + "/")) {
			return TemplateCategorization.TEMPLATE;
		}

		if (uriSegmentString.contains(FOLDER_NAME_OBSERVER_TEMPLATES + "/" + FOLDER_NAME_GENERATED + "/")) {
			return TemplateCategorization.GENERATED;
		}
		
		return TemplateCategorization.NORMAL;
	}

	/**
	 * checks which category the file of the given resource belongs to,
	 * will default to {@link TemplateCategorization#NORMAL} if it cannot be determined.
	 *  
	 * @param emfResource the EMF resource
	 * @return the corresponding template categorization, never null
	 */
	public static TemplateCategorization getTemplateCategorization(Resource emfResource)
	{
		if (emfResource==null)
		{
			return TemplateCategorization.NORMAL;
		}
		

		URI uri = emfResource.getURI();
		if ( uri != null) {
			String uriString = segmentsToString(uri);

			if (!StringUtils.nullOrEmpty(uriString)) {
				return getTemplateCategorizationFor(uriString);
			} else {
				try {
					IResource eclipseResource = EMFUtils.getEclipseResource(emfResource);
					return getTemplateCategorization(eclipseResource);
				} catch (Exception e) {
					return TemplateCategorization.NORMAL;
				}
			}
		}
		
		return TemplateCategorization.NORMAL;
	}

	/**
	 * checks which category the file of the given resource belongs to,
	 * will default to {@link TemplateCategorization#NORMAL} if it cannot be determined.
	 * 
	 * @param eclipseResource the eclipse resource
	 * @return the corresponding template categorization, never null
	 */
	public static TemplateCategorization getTemplateCategorization(IResource eclipseResource)
	{
		if (eclipseResource==null)
		{
			return TemplateCategorization.NORMAL;
		}
		IPath path = eclipseResource.getProjectRelativePath();
		if (path==null || path.segmentCount()==0)
		{
			return TemplateCategorization.NORMAL;
		}
		if (!FOLDER_NAME_STATEMACHINE_TEMPLATES.equals(path.segment(0)) && !FOLDER_NAME_OBSERVER_TEMPLATES.equals(path.segment(0)) )
		{
			// not in the templates super directory
			return TemplateCategorization.NORMAL;
		}
		if (path.segmentCount()==1)
		{
			// directly in the templates super directory - this is not conforming to structure
			return TemplateCategorization.NORMAL;
		}
		if (FOLDER_NAME_GENERATED.equals(path.segment(1)))
		{
			// within StateMachineTemplates/generated
			return TemplateCategorization.GENERATED;
		}
		else if (FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
		{
			// within StateMachineTemplates/Templates
			return TemplateCategorization.TEMPLATE;
		}
		// other folder
		return TemplateCategorization.NORMAL;
	}
	
	/**
	 * Checks the configured metadata of the attribute in {@link #TEMPLATE_METADATA_URL} for {@link #TEMPLATE_METADATA_KEY_ATTRTYPE}.
	 * 
	 * @param pAttribute the attribute to check
	 * @return the categorization, never null
	 * @throws IllegalArgumentException if the attribute type key is given with an unknown value
	 */
	public static TemplateAttributeCharacterization getTemplateAttributeCharacterization(EAttribute pAttribute)
	{
		if (pAttribute==null)
		{
			return TemplateAttributeCharacterization.NONE;
		}
		EAnnotation metadata = pAttribute.getEAnnotation(TEMPLATE_METADATA_URL);
		if (metadata==null)
		{
			return TemplateAttributeCharacterization.NONE;
		}
		String attrTypeValue = metadata.getDetails().get(TEMPLATE_METADATA_KEY_ATTRTYPE);
		if (attrTypeValue==null || attrTypeValue.length()==0)
		{
			return TemplateAttributeCharacterization.NONE;
		}
		for (TemplateAttributeCharacterization characterization : TemplateAttributeCharacterization.values())
		{
			if (attrTypeValue.equals(characterization.getLiteralValue()))
			{
				return characterization;
			}
		}
		throw new IllegalArgumentException("Illegal value of attrType: "+attrTypeValue);
	}
	
	/**
	 * Checks if a complete string is placeholder string.
	 * That is the case if the string begins and ends with <CODE>#</CODE> (whitespace the end are skipped).
	 * This method is intended for values that may contain only one placeholder and otherwise a parsable number.
	 * Note that the strings <code>##</code> and <code>#</code> are considered to be a placeholder string, albeit with an invalid placeholder name.
	 * Note that a string with one <CODE>#</CODE> somewhere inside is not considered a placeholder by this method, in contradiction to what 
	 * {@link #containsParameters(String)} does.
	 * @param string the string to check
	 * @return true if the string is a placeholder string
	 */
	public static boolean isPlaceholder(String string)
	{
		if (string==null)
		{
			return false;
		}
		string = string.trim();
		if (string.startsWith("#") && string.endsWith("#"))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * checks if the given string contains placeholders.
	 * That is the case if the string contains at least one <CODE>#</CODE>.
	 * Note that strings with one or an odd number of <CODE>#</CODE> are considered to contain placeholders, albeit with an invalid placeholder name
	 * @param string the string to check
	 * @return true if the string contains at least one placeholder
	 */
	public static boolean containsParameters(String string)
	{
		if (string==null)
		{
			return false;
		}
		if (string.contains("#"))
		{
			return true;
		}
		return false;
	}

	/**
	 * checks if the given string is valid as a placeholder name.
	 * Only basic letters, underscores, and (except for the first position) digits.
	 *  
	 * @param placeholderName the string to use as a placeholder
	 * @return null if the name is valid as a placeholder or an error message if it is not
	 */
	public static String checkValidPlaceholderName(String placeholderName)
	{
		if (placeholderName==null || placeholderName.trim().length()==0)
		{
			return "Placeholder name must not be empty";
		}
		placeholderName = placeholderName.trim();
		for (int i = 0; i < placeholderName.length(); i++)
		{
			char c = placeholderName.charAt(i);
			if (!(     (c>='a' && c<='z')
					|| (c>='A' && c<='Z')
					|| (c=='_')
					|| (i>0 && c>='0' && c<='9')))
			{
				return "Placeholder name must consist only of letters, _ or (except for the first character) digits";
			}
		}
		return null;
	}
	
	/**
	 * checks if the given string is a valid placeholder name with a hashtag at start and end of string, then it checks if if actually is valid via
	 * checkValidPlaceholderName
	 * Only basic letters, underscores, and (except for the first position) digits.
	 *  
	 * @param placeholderName the string to use as a placeholder
	 * @return null if the name is valid as a placeholder or an error message if it is not
	 */
	public static String checkValidPlaceholderNameWithHashtags(String placeholderName)
	{	
		List<String> parameters = null;
		try {
			parameters = findParameters(placeholderName);
		} catch (NumberFormatException e) {
			return DELIMITERS_ARE_UNEVEN + placeholderName;
		}
		if(parameters != null && parameters.size() > 0) {
			for(String parameter : parameters) {
				String error = checkValidPlaceholderName(parameter);
				if(error != null && error.length() > 0) {
					return error;
				}
			}
		}
		return null;
	}

	/**
	 * @param string the name with surrounding <CODE>#</CODE>
	 * @return the trimmed name of the placeholder or null if it is not a placeholder
	 */
	public static String getPlaceholderName(String string)
	{
		if (string==null)
		{
			return null;
		}
		string = string.trim();
		if (string.startsWith("#") && string.endsWith("#") && string.length()>1)
		{
			String placeholderName = string.substring(1,string.length()-1).trim();
			return placeholderName;
		}
		return null;
	}

	/**
	 * checks if the given value string is a valid integer and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * integer that can be parsed by {@link Integer#parseInt(String)}.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getIntNotPlaceholder(String, boolean)
	 */
	public static String checkIntNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		try
		{
			Integer.parseInt(value);
			// ok
			return null;
		}
		catch (NumberFormatException ex)
		{
			return "value is not an integer";
		}
	}

	/**
	 * Returns the long value of the given string. Will fail with a {@link NumberFormatException}
	 * if {@link #checkIntNotPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the long value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not a long
	 * 
	 * @see #checkIntNotPlaceholder(String, boolean)
	 */
	public static Integer getIntNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		return Integer.parseInt(value.trim());
	}

	public static String checkBusIdRangePatternNotPlaceholder(String idRange2Check, boolean nullAllowed)
	{
		if (idRange2Check==null || idRange2Check.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		
		idRange2Check = idRange2Check.trim();
		
		if (idRange2Check=="*")
		{
			return "value '*' is not allowed";
		}
		
		if (containsParameters(idRange2Check))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		
		String[] rangeParameters = idRange2Check.split("[,;]");
		if (rangeParameters.length > 1)
		{
			// more values
			String errMsg = null;
	    	for (String rangeParameter : rangeParameters)
			{
	    		rangeParameter = rangeParameter.trim();
	    		if (idRange2Check=="*")
	    		{
	    			errMsg = "value '*' is not allowed";
	    			break;
	    		}
	    		else if (!containsRanges(rangeParameter, false))
				{
	    			try
	    			{
	    				Integer.parseInt(rangeParameter);
	    			}
	    			catch (Exception ex)
	    			{
	    				errMsg = "value is not a valid bus id range: only numbers are allowed";
	    			}
				}
			}
	    	return errMsg;	    	
		}
		else
		{
			// single value
			try
			{
				Integer.parseInt(idRange2Check);
				// ok
				return null;
			}
			catch (Exception ex)
			{
				return "value is not a valid bus id range: only numbers are allowed";
			}
		}
	}
	
    public static String checkByteOrTemplatePlaceholder(String value, boolean nullAllowed) {
        if(StringUtils.nullOrEmpty(value)) {
            if(nullAllowed) {
                return null;
            } else {
                return VALUE_MUST_BE_GIVEN;
            }
        }

        if (containsParameters(value)) {
            return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
        }

        try {
	        // int value [0;255] or two-byte hex string are allowed
	        int intValue = StringUtils.valueOf(value);
	        if(intValue < 0 || intValue > 255) {
	    		return "value must be between 0 and 255";
	    	} else {
	    		return null;
	    	}
        } catch(Exception e) {
        	return "invalid value for " + value;
        }
    }

	/**
	 * checks if the given value string is a valid VLAN id and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * VLAN id.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getVLANNotPlaceholder(String, boolean)
	 */
	public static String checkVLANIdPatternNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		
		if (containsRanges(value, false))
		{
			return null;
		}
		
		try
		{
			Integer.parseInt(value);
			// ok
			return null;
		}
		catch (Exception ex)
		{
			return "value is not a valid VLAN id";
		}
	}

	/**
	 * Checks if the string contains a range value
	 * @param value input string to check
	 * @param bracketsMandatory indicator, of brackets are mandatory
	 * @return true, if input string is a range value otherwise false
	 */
	private static String removeRangesBrackets(String value) 
	{
		// pr�fen, ob '[',']','-' oder '*' enthalten sind
		if (   value.startsWith("[") 
			&& value.endsWith("]") )
		{
			value = value.substring(1, value.length()-1);
		}
		return value;
	}

	/**
	 * Checks if the string contains a range value
	 * @param value input string to check
	 * @param bracketsMandatory indicator, of brackets are mandatory
	 * @return true, if input string is a range value otherwise false
	 */
	private static boolean containsRanges(String value, boolean bracketsMandatory) 
	{
		// pr�fen, ob '[',']','-' oder '*' enthalten sind
		if (   value.startsWith("[") 
			&& value.endsWith("]") )
		{
			value = value.substring(1, value.length()-1);
		}
		else if (true == bracketsMandatory)
		{
			return false;
		}
		
		if ( value.contains("-"))
		{
			String[] rangeValues = value.split("-");
			if ( rangeValues.length == 2 )
			{
				try 
				{
					if (value.toLowerCase().startsWith("0x"))
					{
						Integer.parseInt(rangeValues[0].substring(2), 16);
					}
					else
					{
						Integer.parseInt(rangeValues[0]);
					}
					if (value.toLowerCase().startsWith("0x"))
					{
						Integer.parseInt(rangeValues[1].substring(2), 16);
					}
					else
					{
						Integer.parseInt(rangeValues[1]);
					}
					return true;
				} catch (NumberFormatException e) 
				{
					return false;
				}
			}
		}
		else if ( value.equals("*") )
		{
			return true;
		}
		
		
		return false;
	}

	/**
	 * Returns the integer value of the given string. Will fail with a {@link NumberFormatException}
	 * if {@link #checkVLANNotPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the integer value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not an integer
	 * 
	 * @see #checkVLANNotPlaceholder(String, boolean)
	 */
	public static Integer getVLANNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		return Integer.parseInt(value);
	}
	
	/**
	 * checks if the given value string is a valid MAC address and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * MAC address.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getMACNotPlaceholder(String, boolean)
	 */
	public static String checkPortPatternNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}

		if (containsRanges(value,false))
		{
			return null;
		}
		
		try
		{
			Integer.parseInt(value);
			// ok
			return null;
		}
		catch (Exception ex)
		{
			return "value is not a valid Port address";
		}
	}
	
	/**
	 * checks if the given value string is a valid MAC address and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * MAC address.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getMACNotPlaceholder(String, boolean)
	 */
	public static String checkMACPatternNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		try
		{
			if (false == EthernetUtils.checkMAC(value))
				return "value is not a valid MAC address";
		}
		catch (Exception ex)
		{
			return "value is not a valid MAC address";
		}
		return null;
	}
	
	/**
	 * checks if the given value string is a valid IP address and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * IP address.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getIPNotPlaceholder(String, boolean)
	 */
	public static String checkIPPatternNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		
		String[] ipParts = value.split("\\.");
		for (int i=0;i<ipParts.length; i++)
		{
			if (containsRanges(ipParts[i], true))
			{
				return null;
			}
		}
		
		
		try
		{
			if( false == EthernetUtils.checkIP(value))
			{
				return "value is not a valid IP address: "+ value;
			}
		}
		catch (Exception ex)
		{
			return "value is not a valid IP address:" + value;
		}
		return null;
	}
	
	/**
	 * checks if the given value string is a valid long and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * long that can be parsed by {@link Long#parseLong(String)}.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getLongNotPlaceholder(String, boolean)
	 */
	public static String checkLongNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		try
		{
			Long.parseLong(value);
			// ok
			return null;
		}
		catch (NumberFormatException ex)
		{
			return "value is not a long number";
		}
	}

	/**
	 * Returns the long value of the given string. Will fail with a {@link NumberFormatException}
	 * if {@link #checkLongNotPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the long value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not a long
	 * 
	 * @see #checkLongNotPlaceholder(String, boolean)
	 */
	public static Long getLongNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		return Long.parseLong(value.trim());
	}

	/**
	 * checks if the given value string is a valid double and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * double that can be parsed by {@link Double#parseDouble(String)}.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getDoubleNotPlaceholder(String, boolean)
	 */
	public static String checkDoubleNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		try
		{
			Double.parseDouble(value);
			// ok
			return null;
		}
		catch (NumberFormatException ex)
		{
			return "value is not a double";
		}
	}

	/**
	 * checks if the given value string is a valid double or hex value and not a parameter string.
	 * If nullAllowed is true, then null or an empty string is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the string is empty or null and that is allowed, or the string is a valid
	 * double that can be parsed by {@link Double#parseDouble(String)}.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null or an empty string is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getDoubleNotPlaceholder(String, boolean)
	 */
	public static String isValidDoubleOrHexStringRepresentationWithoutPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		value = value.trim();
		if (containsParameters(value))
		{
			return VALUE_MUST_NOT_BE_A_PLACEHOLDER;
		}
		// try hex
		if (Pattern.matches(DataConversions.HEX_NUMBER_PATTERN_STRING,value)) {
			// ok
			return null;
		}
		// try double
		try
		{
			Double.parseDouble(value);
			// ok
			return null;
		}
		catch (NumberFormatException ex)
		{
			return "value is not a double or a hex";
		}
	}

	/**
	 * Returns the Double value of the given string. Will fail with a {@link NumberFormatException}
	 * if {@link #checkDoubleNotPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the double value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not a double
	 * 
	 * @see #checkDoubleNotPlaceholder(String, boolean)
	 */
	public static Double getDoubleNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		return Double.parseDouble(value.trim());
	}
	
	/**
	 * Returns the Long value of the given string. If the value is a hex string, it will be converted to double. 
	 * Will fail with a {@link NumberFormatException}
	 * if {@link #isValidDoubleOrHexStringRepresentationWithoutPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the double value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not a double
	 * 
	 * @see #checkLongOrHexNotPlaceholder(String, boolean)
	 */
	public static Long getLongOrHexNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		// JIRA #SMARD-444 behoben
		if (value.contains("."))
		{
			Double dval = Double.parseDouble(value.trim());
			return dval.longValue();
		}
		if (Pattern.matches(DataConversions.HEX_NUMBER_PATTERN_STRING, value))
		{
			// hex
			return DataConversions.getLongFromHexOrNumber(value);
		}
		return Long.parseLong(value.trim());
	}
	
	/**
	 * Returns the Double value of the given string. If the value is a hex string, it will be converted to double. 
	 * Will fail with a {@link NumberFormatException}
	 * if {@link #isValidDoubleOrHexStringRepresentationWithoutPlaceholder(String, boolean)} returns an error message.
	 * 
	 * @param value the string value
	 * @param nullAllowed if true, null or an empty string is a allowed (which will be returned as null)
	 * @return the double value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not a double
	 * 
	 * @see #isValidDoubleOrHexStringRepresentationWithoutPlaceholder(String, boolean)
	 */
	public static Double getDoubleOrHexNotPlaceholder(String value, boolean nullAllowed)
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		// JIRA #SMARD-444 behoben
		if (value.contains("."))
		{
			Double dval = Double.parseDouble(value.trim());
			return dval;
		}
		if (Pattern.matches(DataConversions.HEX_NUMBER_PATTERN_STRING, value))
		{
			// hex
			return DataConversions.getDoubleFromHexOrNumber(value);
		}
		return Double.parseDouble(value.trim());
	}
	
	/**
	 * checks if the given {@link BooleanOrTemplatePlaceholderEnum} value is a boolean value and not a parameter selection.
	 * If nullAllowed is true, then null or is allowed as input.
	 * The error message is designed so that it is possible to use it as a suffix after a description like "signal length: ".
	 * <P>
	 * If this method returns null, then either the value is null and that is allowed, or the value is a valid
	 * boolean that can be extracted by {@link #getBooleanNotPlaceholder(String, boolean)}.
	 * 
	 * @param value the input value
	 * @param nullAllowed if true, then null is allowed
	 * @return null if the input is ok, otherwise an error message 
	 * 
	 * @see #getBooleanNotPlaceholder(String, boolean)
	 */
	public static String checkBooleanNotPlaceholder(BooleanOrTemplatePlaceholderEnum value, boolean nullAllowed)
	{
		if (value==null)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return "value must be selected";
			}
		}
		if (BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED==value)
		{
			return "placeholder not allowed";
		}
		// the other values are ok
		return null;
	}

	/**
	 * Returns the Boolean value of the given string. Will fail with a {@link NumberFormatException}
	 * if {@link #checkBooleanNotPlaceholder(BooleanOrTemplatePlaceholderEnum, boolean)} returns an error message.
	 * 
	 * @param value the enum value
	 * @param nullAllowed if true, null is a allowed (which will be returned as null)
	 * @return the Boolean value (or null if that is allowed)
	 * @throws NumberFormatException if the value is not an Boolean
	 * 
	 * @see #checkBooleanNotPlaceholder(BooleanOrTemplatePlaceholderEnum, boolean)
	 */
	public static Boolean getBooleanNotPlaceholder(BooleanOrTemplatePlaceholderEnum value, boolean nullAllowed)
	{
		if (value==null)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				throw new NumberFormatException("null not allowed");
			}
		}
		if (BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED==value)
		{
			throw new NumberFormatException("Template-defined not allowed");
		} 
		else if (BooleanOrTemplatePlaceholderEnum.FALSE==value)
		{
			return Boolean.FALSE;
		}
		else if (BooleanOrTemplatePlaceholderEnum.TRUE==value)
		{
			return Boolean.TRUE;
		}
		else
		{
			throw new RuntimeException("Unexpected value: "+value);
		}
	}

	/**
	 * Checks if the given project contains the correct folder structure for templates.
	 * @param project the project
	 * @return true if all needed folders exist
	 * 
	 * @see TemplateUtils
	 */
	public static boolean containsCorrectProjectTemplateFolders(IProject project)
	{
		// SMARD-2379 do not check for Observer Template folders -> those are deactivated
		// correct template structure for observer
		/*IFolder observerTemplatesFolder = project.getFolder(FOLDER_NAME_OBSERVER_TEMPLATES);
		if (!observerTemplatesFolder.exists())
		{
			return false;
		}
		
		IFolder generatedFolder = observerTemplatesFolder.getFolder(FOLDER_NAME_GENERATED);
		if (!generatedFolder.exists())
		{
			return false;
		}
		IFolder templatesFolder = observerTemplatesFolder.getFolder(FOLDER_NAME_TEMPLATES);
		if (!templatesFolder.exists())
		{
			return false;
		}*/
		
		// correct template structure for observer
		IFolder statemachineTemplatesFolder = project.getFolder(FOLDER_NAME_STATEMACHINE_TEMPLATES);
        if (!statemachineTemplatesFolder.exists())
        {
            return false;
        }
        IFolder generatedObserverFolder = statemachineTemplatesFolder.getFolder(FOLDER_NAME_GENERATED);
        if (!generatedObserverFolder.exists())
        {
            return false;
        }
        IFolder templatesObserverFolder = statemachineTemplatesFolder.getFolder(FOLDER_NAME_TEMPLATES);
        if (!templatesObserverFolder.exists())
        {
            return false;
        }
		return true;
	}

	/**
	 * creates the folders in the project if they don't exist yet
	 * @param project the project
	 * @throws CoreException if one of the folder creations fails
	 */
	public static void createProjectTemplateFolders(IProject project) throws CoreException
	{
	    // create template structure for statemachines
		IProgressMonitor monitor = new NullProgressMonitor();
		IFolder statemachineTemplatesFolder = project.getFolder(FOLDER_NAME_STATEMACHINE_TEMPLATES);
		if (!statemachineTemplatesFolder.exists())
		{
			statemachineTemplatesFolder.create(false, true, monitor);
		}
		IFolder generatedFolder = statemachineTemplatesFolder.getFolder(FOLDER_NAME_GENERATED);
		if (!generatedFolder.exists())
		{
			generatedFolder.create(false, true, monitor);
		}
		IFolder templatesFolder = statemachineTemplatesFolder.getFolder(FOLDER_NAME_TEMPLATES);
		if (!templatesFolder.exists())
		{
			templatesFolder.create(false, true, monitor);
		}
	}

	/**
	 * checks if the given folder is the Templates folder of a project 
	 * @param folder the folder to check
	 * @return true if the folder is the Templates folder of a project
	 */
	public static boolean isTemplatesFolder(IFolder folder)
	{
		if (folder==null)
		{
			return false;
		}
		IPath path = folder.getProjectRelativePath();
		if (path==null || path.segmentCount()!=2)
		{
			return false;
		}
		if (!FOLDER_NAME_STATEMACHINE_TEMPLATES.equals(path.segment(0)))
		{
			// not in the templates super directory
			return false;
		}
        if (!FOLDER_NAME_OBSERVER_TEMPLATES.equals(path.segment(0)))
        {
            // not in the templates super directory
            return false;
        }
		if (FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
		{
			// within StateMachineTemplates/Templates
			return true;
		}
		return false;
	}

	/**
	 * checks if the given folder is the Statemachine Templates folder of a project 
	 * @param folder the folder to check
	 * @return true if the folder is the Templates folder of a project
	 */
	public static boolean isStatemachineTemplatesFolder(IFolder folder)
	{
		if (folder==null)
		{
			return false;
		}
		IPath path = folder.getProjectRelativePath();
		if (path==null || path.segmentCount()!=2)
		{
			return false;
		}
		if (!FOLDER_NAME_STATEMACHINE_TEMPLATES.equals(path.segment(0)))
		{
			// not in the templates super directory
			return false;
		}
		if (FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
		{
			// within StateMachineTemplates/Templates
			return true;
		}
		return false;
	}


    /**
     * checks if the given folder is the Observer Templates folder of a project 
     * @param folder the folder to check
     * @return true if the folder is the Templates folder of a project
     */
    public static boolean isObserverTemplatesFolder(IFolder folder)
    {
        if (folder==null)
        {
            return false;
        }
        IPath path = folder.getProjectRelativePath();
        if (path==null || path.segmentCount()!=2)
        {
            return false;
        }
        if (!FOLDER_NAME_OBSERVER_TEMPLATES.equals(path.segment(0)))
        {
            // not in the templates super directory
            return false;
        }
        if (FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
        {
            // within StateMachineTemplates/Templates
            return true;
        }
        return false;
    }

    /**
     * checks if the given folder is the Observer Templates folder of a project 
     * @param folder the folder to check
     * @return true if the folder is the Templates folder of a project
     */
    public static boolean isConditionDocumentInObserverTemplatesFolder(IFolder folder)
    {
        if (folder==null)
        {
            return false;
        }
        IPath path = folder.getProjectRelativePath();
        if (path==null || path.segmentCount()<4)
        {
            return false;
        }
        if (!FOLDER_NAME_OBSERVER_TEMPLATES.equals(path.segment(0)))
        {
            // not in the templates super directory
            return false;
        }
        if (!FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
        {
            // not within StateMachineTemplates/Templates
            return false;
        }
        if (FOLDER_NAME_CONDITIONS.equals(path.segment(3)))
        {
            // within StateMachineTemplates/Templates/xxx/Conditions
            return true;
        }
        return false;
    }

	/**
	 * Creates the folder structure for a template statemachine (intended within a StateMachineTemplates/Templates folder).
	 * Nothing happens if the folder structure does already exist.
	 * 
	 * @param templatesFolder the folder in which the sub folders should be created
	 * @param name the name of the statemachine, used as name of the direct sub folder
	 * @return the named template folder which is a direct sub folder of the given templatesFolder
	 * @throws CoreException in case an error occurs
	 * 
	 * @see TemplateUtils
	 */
	public static IFolder createTemplateStatemachineFolders(IFolder templatesFolder, String name) throws CoreException
	{
		IProgressMonitor monitor = new NullProgressMonitor();
		IFolder namedTemplateFolder = templatesFolder.getFolder(name);
		if (!namedTemplateFolder.exists())
		{
			namedTemplateFolder.create(false, true, monitor);
		}
		IFolder conditionsFolder = namedTemplateFolder.getFolder(FOLDER_NAME_CONDITIONS);
		if (!conditionsFolder.exists())
		{
			conditionsFolder.create(false, true, monitor);
		}
		IFolder csvFolder = namedTemplateFolder.getFolder(FOLDER_NAME_CSV);
		if (!csvFolder.exists())
		{
			csvFolder.create(false, true, monitor);
		}
		IFolder statemachinesFolder = namedTemplateFolder.getFolder(FOLDER_NAME_STATEMACHINES);
		if (!statemachinesFolder.exists())
		{
			statemachinesFolder.create(false, true, monitor);
		}
		return namedTemplateFolder;
	}
	
	/**
     * Creates the folder structure for a template observer (intended within a ObserverTemplates/Templates folder).
     * Nothing happens if the folder structure does already exist.
     * 
     * @param templatesFolder the folder in which the sub folders should be created
     * @param name the name of the observer, used as name of the direct sub folder
     * @return the named template folder which is a direct sub folder of the given templatesFolder
     * @throws CoreException in case an error occurs
     * 
     * @see TemplateUtils
     */
    public static IFolder createTemplateObserverFolders(IFolder templatesFolder, String name) throws CoreException
    {
        IProgressMonitor monitor = new NullProgressMonitor();
        IFolder namedTemplateFolder = templatesFolder.getFolder(name);
        if (!namedTemplateFolder.exists())
        {
            namedTemplateFolder.create(false, true, monitor);
        }
        IFolder conditionsFolder = namedTemplateFolder.getFolder(FOLDER_NAME_CONDITIONS);
        if (!conditionsFolder.exists())
        {
            conditionsFolder.create(false, true, monitor);
        }
        IFolder csvFolder = namedTemplateFolder.getFolder(FOLDER_NAME_CSV);
        if (!csvFolder.exists())
        {
            csvFolder.create(false, true, monitor);
        }
        return namedTemplateFolder;
    }

	/**
	 * searches for the generated folder (StateMachineTemplates/generated) in the given project.
	 * If the folder does not exist, null is returned.
	 * 
	 * @param project a project
	 * @return the folder or null in case of an error
	 */
	public static IFolder findGeneratedFolder(IProject project, boolean observer)
	{
		if (project==null)
		{
			return null;
		}
		IPath path = project.getProjectRelativePath();
		if (observer)
		{
		    path = path.append(FOLDER_NAME_OBSERVER_TEMPLATES);
		}
		else
		{
		    path = path.append(FOLDER_NAME_STATEMACHINE_TEMPLATES);
		}
		
		path = path.append(FOLDER_NAME_GENERATED);
		IFolder folder = project.getFolder( path);
		if (folder.exists())
		{
			return folder;
		}
		return null;
	}
	
	/**
	 * searches for the templates folder (StateMachineTemplates/Templates) in the given project.
	 * If the folder does not exist, null is returned.
	 * 
	 * @param project a project
	 * @return the folder or null in case of an error
	 */
	public static IFolder findTemplatesFolder(IProject project, boolean observer)
	{
		if (project==null)
		{
			return null;
		}
		IPath path = project.getProjectRelativePath();
		if (observer)
		{
		    path  = path.append(FOLDER_NAME_OBSERVER_TEMPLATES);
		}
		else
		{
		    path  = path.append(FOLDER_NAME_STATEMACHINE_TEMPLATES);
		}
		path = path.append(FOLDER_NAME_TEMPLATES);
		IFolder folder = project.getFolder( path);
		if (folder.exists())
		{
			return folder;
		}
		return null;
	}
	
	/**
	 * Searches for the named template directory folder that is parent to the given resource.
	 * The named template directory is a direct sub folder of the 
	 * StateMachineTemplates/Templates or StateMachineTemplates/generated directory.
	 * The result is parent of folders Conditions and StateMachines (and in case of a template a CSV folder).  
	 * If the folder does not exist, null is returned.
	 * 
	 * @param resource a resource within a template directory structure
	 * @return the named folder 
	 */
	public static IFolder findNamedTemplateParentDirectory(IResource resource)
	{
		if (resource==null)
		{
			return null;
		}
		IPath path = resource.getProjectRelativePath();
		if (path==null || path.segmentCount()<3)
		{
			// we need at least 3 segments (StateMachineTemplates / Templates / <NamedDirectory>)
		    // or (ObserverTemplates / Templates / <NamedDirectory>)
			return null;
		}
		if (!FOLDER_NAME_STATEMACHINE_TEMPLATES.equals(path.segment(0)) && !FOLDER_NAME_OBSERVER_TEMPLATES.equals(path.segment(0)))
		{
			// not in the templates super directory
			return null;
		}
		if (path.segmentCount()==1)
		{
			// directly in the templates super directory - this is not conforming to structure
			return null;
		}
		if (FOLDER_NAME_GENERATED.equals(path.segment(1)))
		{
			// within StateMachineTemplates/generated
			return resource.getProject().getFolder( path.removeLastSegments(Math.max(0,path.segmentCount()-3)));
		}
		else if (FOLDER_NAME_TEMPLATES.equals(path.segment(1)))
		{
			// within StateMachineTemplates/Templates
			return resource.getProject().getFolder( path.removeLastSegments(Math.max(0,path.segmentCount()-3)));
		}
		// other folder
		return null;
	}

	/**
	 * Searches for the conditions folder within a template directory structure.
	 * If the folder does not exist, null is returned.
	 * 
	 * @param resource a resource within a template directory structure
	 * @return the conditions folder in the template directory structure or null if the folder could not be found or the resource is not from a template.
	 */
	public static IFolder findConditionsFolder(IResource resource)
	{
		IFolder namedTemplateDirectory = findNamedTemplateParentDirectory(resource);
		if (namedTemplateDirectory==null)
		{
			return null;
		}
		IFolder conditionsFolder = namedTemplateDirectory.getFolder(FOLDER_NAME_CONDITIONS);
		if (conditionsFolder.exists())
		{
			return conditionsFolder;
		}
		return null;
	}

	/**
	 * Searches for the CSV folder within a template directory structure.
	 * If the folder does not exist, null is returned.
	 * 
	 * @param resource a resource within a template directory structure
	 * @return the CSV folder in the template directory structure or null if the folder could not be found or the resource is not from a template.
	 */
	public static IFolder findCsvFolder(IResource resource)
	{
		IFolder namedTemplateDirectory = findNamedTemplateParentDirectory(resource);
		if (namedTemplateDirectory==null)
		{
			return null;
		}
		IFolder csvFolder = namedTemplateDirectory.getFolder(FOLDER_NAME_CSV);
		if (csvFolder.exists())
		{
			return csvFolder;
		}
		return null;
	}

	/**
	 * Searches for the statemachines folder within a template directory structure.
	 * If the folder does not exist, null is returned.
	 * 
	 * @param resource a resource within a template directory structure
	 * @return the statemachines folder in the template directory structure or null if the folder could not be found or the resource is not from a template.
	 */
	public static IFolder findStatemachinesFolder(IResource resource)
	{
		IFolder namedTemplateDirectory = findNamedTemplateParentDirectory(resource);
		if (namedTemplateDirectory==null)
		{
			return null;
		}
		IFolder statemachineFolder = namedTemplateDirectory.getFolder(FOLDER_NAME_STATEMACHINES);
		if (statemachineFolder.exists())
		{
			return statemachineFolder;
		}
		return null;
	}

	/**
	 * A {@link String}-Parser for # symbols. The parameter will be inside two #
	 * symbols. For example it will parse the string #parameter1##parameter2# to
	 * a list of parameter1 and parameter2.
	 * 
	 * @param pText
	 *            which will be parsed
	 * @return a list of {@link} parameter names
	 * @throws NumberFormatException
	 *             if the number of delimiters is uneven
	 */
	public static List<String> findParameters(String pText) throws NumberFormatException {
		List<String> parameters = new ArrayList<String>();

		if(StringUtils.isBlank(pText)) {
			return parameters;
		}

		Scanner scanner = new Scanner(pText);
		scanner.useDelimiter("#");

		int numberOfCommas = pText.replaceAll("[^#]", "").length();

		if (numberOfCommas % 2 != 0) {
			scanner.close();
			throw new NumberFormatException(DELIMITERS_ARE_UNEVEN + pText);
		} else {
			if (!pText.substring(0, 1).equals("#")) 
				scanner.next();			

			for (int i=0; scanner.hasNext(); ++i) {
				String next = scanner.next();
				if (i%2==0)
					parameters.add(next);
			}
		}
		scanner.close();
		return parameters;
	}

	public static String checkCanIdRangePatternNotPlaceholder(
			String value, boolean nullAllowed) 
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return VALUE_MUST_BE_GIVEN;
			}
		}
		
		value = value.trim();
		
		String[] rangeParameters = value.split("[,;]");
		String errorvalue = "";
		if (rangeParameters.length > 0)
		{
			// more values
			for (String rangeParameter : rangeParameters)
			{
	    		rangeParameter = rangeParameter.trim();
	    		if (value=="*")
	    		{
	    			return "value '*' is not allowed";
	    		}
				else if (containsParameters(value))
				{
					return "value must not contain a placeholder";
				}
	    		else if (!containsRanges(rangeParameter, false))
				{
	    			errorvalue = rangeParameter;
	    			try
	    			{
	    				if (rangeParameter.toLowerCase().startsWith("0x"))
	    				{
	    					Integer.parseInt(rangeParameter.substring(2), 16);
	    				}
	    				else
	    				{
	    					Integer.parseInt(rangeParameter);
	    				}
	    			}
	    			catch (Exception ex)
	    			{
	    				return MessageFormat.format("value {0} is not a valid can message id range", errorvalue);
	    			}
				}
	    		else
	    		{
	    			rangeParameter = TemplateUtils.removeRangesBrackets(rangeParameter);
	    			String[] rangeValues = rangeParameter.split("[-]");
	    			if (rangeValues.length != 2)
	    			{
	    				return "value is not a valid can message id range (wrong count of values separated by '-')";
	    			}
	    			try
	    			{
	    				if (rangeValues[0].toLowerCase().startsWith("0x"))
	    				{
	    					Integer.parseInt(rangeValues[0].substring(2), 16);
	    				}
	    				else
	    				{
	    					Integer.parseInt(rangeValues[0]);
	    				}
	    				errorvalue = rangeValues[1];
	    				if (rangeValues[1].toLowerCase().startsWith("0x"))
	    				{
	    					Integer.parseInt(rangeValues[1].substring(2), 16);
	    				}
	    				else
	    				{
	    					Integer.parseInt(rangeValues[1]);
	    				}
	    			}
	    			catch (Exception ex)
	    			{
	    				return MessageFormat.format("value {0} is not a valid can message id range value", errorvalue);
	    			}
	    		}
			}
		}
		return null;
	}

	public static String checkLinIdRangePatternNotPlaceholder(
			String value, boolean nullAllowed) 
	{
		if (value==null || value.trim().length()==0)
		{
			if (nullAllowed)
			{
				return null;
			}
			else
			{
				return "value must be given";
			}
		}
		
		value = value.trim();
		
		String[] rangeParameters = value.split("[,;]");
		if (rangeParameters.length > 0)
		{
			// more values
			for (String rangeParameter : rangeParameters)
			{
	    		rangeParameter = rangeParameter.trim();
	    		if (value=="*")
	    		{
	    			return "value '*' is not allowed";
	    		}
				else if (containsParameters(value))
				{
					return "value must not contain a placeholder";
				}
	    		else if (!containsRanges(rangeParameter, false))
				{
	    			try
	    			{
	    				if (rangeParameter.toLowerCase().startsWith("0x"))
	    				{
	    					Integer.parseInt(rangeParameter.substring(2), 16);
	    				}
	    				else
	    				{
	    					Integer.parseInt(rangeParameter);
	    				}
	    			}
	    			catch (Exception ex)
	    			{
	    				return "value is not a valid lin message id range";
	    			}
				}
			}
		}
		return null;
	}

	public static boolean resourcesInTemplateContext(Resource... resources) {
		if (resources == null || resources.length == 0) {
		      throw new IllegalArgumentException("TemplateUtils::resourcesInTemplateContext resources null or empty");
		}
		
		// if any of the resources is not in template context, 
		for(Resource resource : resources) {
			if(getTemplateCategorization(resource).isExportable()) {
				return false;
			}
		}
		return true;
	}

	public static boolean resourcesInSameTemplateFolder(Resource resource0, Resource resource1) {
		// make sure both resources are in template folders
		if(getTemplateCategorization(resource0).isExportable() ||
				getTemplateCategorization(resource1).isExportable()) {
			return false;
		}
		
		// in which template folders are the resources contained
		String folder0 = resource0.getURI().segment(resource0.getURI().segmentCount() - 3);
		String folder1 = resource1.getURI().segment(resource0.getURI().segmentCount() - 3);
		
		// same folder?
		return folder0.equals(folder1);
	}

}
