package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IString Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIStringOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IStringOperand extends IOperand, IComputeVariableActionOperand {
} // IStringOperand
