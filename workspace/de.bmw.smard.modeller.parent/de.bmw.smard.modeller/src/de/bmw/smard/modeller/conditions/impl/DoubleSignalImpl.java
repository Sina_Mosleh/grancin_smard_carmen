package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.DoubleSignal;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Double Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl#getIgnoreInvalidValues <em>Ignore Invalid Values</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.DoubleSignalImpl#getIgnoreInvalidValuesTmplParam <em>Ignore Invalid Values Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DoubleSignalImpl extends AbstractSignalImpl implements DoubleSignal {
    /**
     * The default value of the '{@link #getIgnoreInvalidValues() <em>Ignore Invalid Values</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIgnoreInvalidValues()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum IGNORE_INVALID_VALUES_EDEFAULT = BooleanOrTemplatePlaceholderEnum.FALSE;
    /**
     * The cached value of the '{@link #getIgnoreInvalidValues() <em>Ignore Invalid Values</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIgnoreInvalidValues()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum ignoreInvalidValues = IGNORE_INVALID_VALUES_EDEFAULT;
    /**
     * The default value of the '{@link #getIgnoreInvalidValuesTmplParam() <em>Ignore Invalid Values Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIgnoreInvalidValuesTmplParam()
     * @generated
     * @ordered
     */
    protected static final String IGNORE_INVALID_VALUES_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getIgnoreInvalidValuesTmplParam() <em>Ignore Invalid Values Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIgnoreInvalidValuesTmplParam()
     * @generated
     * @ordered
     */
    protected String ignoreInvalidValuesTmplParam = IGNORE_INVALID_VALUES_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected DoubleSignalImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.DOUBLE_SIGNAL;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getIgnoreInvalidValues() {
        return ignoreInvalidValues;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setIgnoreInvalidValues(BooleanOrTemplatePlaceholderEnum newIgnoreInvalidValues) {
        BooleanOrTemplatePlaceholderEnum oldIgnoreInvalidValues = ignoreInvalidValues;
        ignoreInvalidValues = newIgnoreInvalidValues == null ? IGNORE_INVALID_VALUES_EDEFAULT : newIgnoreInvalidValues;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES, oldIgnoreInvalidValues, ignoreInvalidValues));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getIgnoreInvalidValuesTmplParam() {
        return ignoreInvalidValuesTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setIgnoreInvalidValuesTmplParam(String newIgnoreInvalidValuesTmplParam) {
        String oldIgnoreInvalidValuesTmplParam = ignoreInvalidValuesTmplParam;
        ignoreInvalidValuesTmplParam = newIgnoreInvalidValuesTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM, oldIgnoreInvalidValuesTmplParam, ignoreInvalidValuesTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES:
                return getIgnoreInvalidValues();
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM:
                return getIgnoreInvalidValuesTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES:
                setIgnoreInvalidValues((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM:
                setIgnoreInvalidValuesTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES:
                setIgnoreInvalidValues(IGNORE_INVALID_VALUES_EDEFAULT);
                return;
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM:
                setIgnoreInvalidValuesTmplParam(IGNORE_INVALID_VALUES_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES:
                return ignoreInvalidValues != IGNORE_INVALID_VALUES_EDEFAULT;
            case ConditionsPackage.DOUBLE_SIGNAL__IGNORE_INVALID_VALUES_TMPL_PARAM:
                return IGNORE_INVALID_VALUES_TMPL_PARAM_EDEFAULT == null ? ignoreInvalidValuesTmplParam != null : !IGNORE_INVALID_VALUES_TMPL_PARAM_EDEFAULT
                        .equals(ignoreInvalidValuesTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (ignoreInvalidValues: ");
        result.append(ignoreInvalidValues);
        result.append(", ignoreInvalidValuesTmplParam: ");
        result.append(ignoreInvalidValuesTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public DataType get_EvaluationDataType() {
        return DataType.DOUBLE;
    }

} //DoubleSignalImpl
