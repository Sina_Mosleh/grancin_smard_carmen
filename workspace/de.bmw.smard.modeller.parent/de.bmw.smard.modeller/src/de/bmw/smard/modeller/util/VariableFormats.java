package de.bmw.smard.modeller.util;

import de.bmw.smard.modeller.conditions.ConditionsFactory;
import de.bmw.smard.modeller.conditions.FormatDataType;
import de.bmw.smard.modeller.conditions.VariableFormat;

public class VariableFormats {

    private VariableFormats(){}

    public static VariableFormat defaultFormat(){
        VariableFormat variableFormat = ConditionsFactory.eINSTANCE.createVariableFormat();
        variableFormat.setBaseDataType(FormatDataType.FLOAT);
        variableFormat.setDigits(0);
        variableFormat.setUpperCase(false);
        return variableFormat;
    }
}
