package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.modeller.conditions.*;
import de.bmw.smard.modeller.statemachine.SmardTraceElement;
import de.bmw.smard.modeller.statemachineset.BusMessageReferable;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage
 * @generated
 */
public class ConditionsAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ConditionsPackage modelPackage;

    /**
     * Creates an instance of the adapter factory.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public ConditionsAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = ConditionsPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object.
     * <!-- begin-user-doc -->
     * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ConditionsSwitch<Adapter> modelSwitch =
            new ConditionsSwitch<Adapter>() {
                @Override
                public Adapter caseDocumentRoot(DocumentRoot object) {
                    return createDocumentRootAdapter();
                }

                @Override
                public Adapter caseConditionSet(ConditionSet object) {
                    return createConditionSetAdapter();
                }

                @Override
                public Adapter caseCondition(Condition object) {
                    return createConditionAdapter();
                }

                @Override
                public Adapter caseSignalComparisonExpression(SignalComparisonExpression object) {
                    return createSignalComparisonExpressionAdapter();
                }

                @Override
                public Adapter caseCanMessageCheckExpression(CanMessageCheckExpression object) {
                    return createCanMessageCheckExpressionAdapter();
                }

                @Override
                public Adapter caseLinMessageCheckExpression(LinMessageCheckExpression object) {
                    return createLinMessageCheckExpressionAdapter();
                }

                @Override
                public Adapter caseStateCheckExpression(StateCheckExpression object) {
                    return createStateCheckExpressionAdapter();
                }

                @Override
                public Adapter caseTimingExpression(TimingExpression object) {
                    return createTimingExpressionAdapter();
                }

                @Override
                public Adapter caseTrueExpression(TrueExpression object) {
                    return createTrueExpressionAdapter();
                }

                @Override
                public Adapter caseLogicalExpression(LogicalExpression object) {
                    return createLogicalExpressionAdapter();
                }

                @Override
                public Adapter caseReferenceConditionExpression(ReferenceConditionExpression object) {
                    return createReferenceConditionExpressionAdapter();
                }

                @Override
                public Adapter caseTransitionCheckExpression(TransitionCheckExpression object) {
                    return createTransitionCheckExpressionAdapter();
                }

                @Override
                public Adapter caseFlexRayMessageCheckExpression(FlexRayMessageCheckExpression object) {
                    return createFlexRayMessageCheckExpressionAdapter();
                }

                @Override
                public Adapter caseStopExpression(StopExpression object) {
                    return createStopExpressionAdapter();
                }

                @Override
                public Adapter caseComparatorSignal(ComparatorSignal object) {
                    return createComparatorSignalAdapter();
                }

                @Override
                public Adapter caseConstantComparatorValue(ConstantComparatorValue object) {
                    return createConstantComparatorValueAdapter();
                }

                @Override
                public Adapter caseCalculationExpression(CalculationExpression object) {
                    return createCalculationExpressionAdapter();
                }

                @Override
                public Adapter caseExpression(Expression object) {
                    return createExpressionAdapter();
                }

                @Override
                public Adapter caseVariableReference(VariableReference object) {
                    return createVariableReferenceAdapter();
                }

                @Override
                public Adapter caseConditionsDocument(ConditionsDocument object) {
                    return createConditionsDocumentAdapter();
                }

                @Override
                public Adapter caseVariableSet(VariableSet object) {
                    return createVariableSetAdapter();
                }

                @Override
                public Adapter caseVariable(Variable object) {
                    return createVariableAdapter();
                }

                @Override
                public Adapter caseSignalVariable(SignalVariable object) {
                    return createSignalVariableAdapter();
                }

                @Override
                public Adapter caseValueVariable(ValueVariable object) {
                    return createValueVariableAdapter();
                }

                @Override
                public Adapter caseVariableFormat(VariableFormat object) {
                    return createVariableFormatAdapter();
                }

                @Override
                public Adapter caseAbstractObserver(AbstractObserver object) {
                    return createAbstractObserverAdapter();
                }

                @Override
                public Adapter caseObserverValueRange(ObserverValueRange object) {
                    return createObserverValueRangeAdapter();
                }

                @Override
                public Adapter caseSignalObserver(SignalObserver object) {
                    return createSignalObserverAdapter();
                }

                @Override
                public Adapter caseSignalReferenceSet(SignalReferenceSet object) {
                    return createSignalReferenceSetAdapter();
                }

                @Override
                public Adapter caseSignalReference(SignalReference object) {
                    return createSignalReferenceAdapter();
                }

                @Override
                public Adapter caseISignalOrReference(ISignalOrReference object) {
                    return createISignalOrReferenceAdapter();
                }

                @Override
                public Adapter caseBitPatternComparatorValue(BitPatternComparatorValue object) {
                    return createBitPatternComparatorValueAdapter();
                }

                @Override
                public Adapter caseNotExpression(NotExpression object) {
                    return createNotExpressionAdapter();
                }

                @Override
                public Adapter caseIOperand(IOperand object) {
                    return createIOperandAdapter();
                }

                @Override
                public Adapter caseIComputeVariableActionOperand(IComputeVariableActionOperand object) {
                    return createIComputeVariableActionOperandAdapter();
                }

                @Override
                public Adapter caseIStringOperand(IStringOperand object) {
                    return createIStringOperandAdapter();
                }

                @Override
                public Adapter caseISignalComparisonExpressionOperand(ISignalComparisonExpressionOperand object) {
                    return createISignalComparisonExpressionOperandAdapter();
                }

                @Override
                public Adapter caseIOperation(IOperation object) {
                    return createIOperationAdapter();
                }

                @Override
                public Adapter caseIStringOperation(IStringOperation object) {
                    return createIStringOperationAdapter();
                }

                @Override
                public Adapter caseINumericOperand(INumericOperand object) {
                    return createINumericOperandAdapter();
                }

                @Override
                public Adapter caseINumericOperation(INumericOperation object) {
                    return createINumericOperationAdapter();
                }

                @Override
                public Adapter caseValueVariableObserver(ValueVariableObserver object) {
                    return createValueVariableObserverAdapter();
                }

                @Override
                public Adapter caseParseStringToDouble(ParseStringToDouble object) {
                    return createParseStringToDoubleAdapter();
                }

                @Override
                public Adapter caseStringExpression(StringExpression object) {
                    return createStringExpressionAdapter();
                }

                @Override
                public Adapter caseMatches(Matches object) {
                    return createMatchesAdapter();
                }

                @Override
                public Adapter caseExtract(Extract object) {
                    return createExtractAdapter();
                }

                @Override
                public Adapter caseSubstring(Substring object) {
                    return createSubstringAdapter();
                }

                @Override
                public Adapter caseParseNumericToString(ParseNumericToString object) {
                    return createParseNumericToStringAdapter();
                }

                @Override
                public Adapter caseStringLength(StringLength object) {
                    return createStringLengthAdapter();
                }

                @Override
                public Adapter caseAbstractMessage(AbstractMessage object) {
                    return createAbstractMessageAdapter();
                }

                @Override
                public Adapter caseSomeIPMessage(SomeIPMessage object) {
                    return createSomeIPMessageAdapter();
                }

                @Override
                public Adapter caseAbstractSignal(AbstractSignal object) {
                    return createAbstractSignalAdapter();
                }

                @Override
                public Adapter caseContainerSignal(ContainerSignal object) {
                    return createContainerSignalAdapter();
                }

                @Override
                public Adapter caseAbstractFilter(AbstractFilter object) {
                    return createAbstractFilterAdapter();
                }

                @Override
                public Adapter caseEthernetFilter(EthernetFilter object) {
                    return createEthernetFilterAdapter();
                }

                @Override
                public Adapter caseUDPFilter(UDPFilter object) {
                    return createUDPFilterAdapter();
                }

                @Override
                public Adapter caseTCPFilter(TCPFilter object) {
                    return createTCPFilterAdapter();
                }

                @Override
                public Adapter caseIPv4Filter(IPv4Filter object) {
                    return createIPv4FilterAdapter();
                }

                @Override
                public Adapter caseSomeIPFilter(SomeIPFilter object) {
                    return createSomeIPFilterAdapter();
                }

                @Override
                public Adapter caseForEachExpression(ForEachExpression object) {
                    return createForEachExpressionAdapter();
                }

                @Override
                public Adapter caseMessageCheckExpression(MessageCheckExpression object) {
                    return createMessageCheckExpressionAdapter();
                }

                @Override
                public Adapter caseSomeIPSDFilter(SomeIPSDFilter object) {
                    return createSomeIPSDFilterAdapter();
                }

                @Override
                public Adapter caseSomeIPSDMessage(SomeIPSDMessage object) {
                    return createSomeIPSDMessageAdapter();
                }

                @Override
                public Adapter caseDoubleSignal(DoubleSignal object) {
                    return createDoubleSignalAdapter();
                }

                @Override
                public Adapter caseStringSignal(StringSignal object) {
                    return createStringSignalAdapter();
                }

                @Override
                public Adapter caseDecodeStrategy(DecodeStrategy object) {
                    return createDecodeStrategyAdapter();
                }

                @Override
                public Adapter caseDoubleDecodeStrategy(DoubleDecodeStrategy object) {
                    return createDoubleDecodeStrategyAdapter();
                }

                @Override
                public Adapter caseExtractStrategy(ExtractStrategy object) {
                    return createExtractStrategyAdapter();
                }

                @Override
                public Adapter caseUniversalPayloadExtractStrategy(UniversalPayloadExtractStrategy object) {
                    return createUniversalPayloadExtractStrategyAdapter();
                }

                @Override
                public Adapter caseEmptyExtractStrategy(EmptyExtractStrategy object) {
                    return createEmptyExtractStrategyAdapter();
                }

                @Override
                public Adapter caseCANFilter(CANFilter object) {
                    return createCANFilterAdapter();
                }

                @Override
                public Adapter caseLINFilter(LINFilter object) {
                    return createLINFilterAdapter();
                }

                @Override
                public Adapter caseFlexRayFilter(FlexRayFilter object) {
                    return createFlexRayFilterAdapter();
                }

                @Override
                public Adapter caseDLTFilter(DLTFilter object) {
                    return createDLTFilterAdapter();
                }

                @Override
                public Adapter caseUDPNMFilter(UDPNMFilter object) {
                    return createUDPNMFilterAdapter();
                }

                @Override
                public Adapter caseCANMessage(CANMessage object) {
                    return createCANMessageAdapter();
                }

                @Override
                public Adapter caseLINMessage(LINMessage object) {
                    return createLINMessageAdapter();
                }

                @Override
                public Adapter caseFlexRayMessage(FlexRayMessage object) {
                    return createFlexRayMessageAdapter();
                }

                @Override
                public Adapter caseDLTMessage(DLTMessage object) {
                    return createDLTMessageAdapter();
                }

                @Override
                public Adapter caseUDPMessage(UDPMessage object) {
                    return createUDPMessageAdapter();
                }

                @Override
                public Adapter caseTCPMessage(TCPMessage object) {
                    return createTCPMessageAdapter();
                }

                @Override
                public Adapter caseUDPNMMessage(UDPNMMessage object) {
                    return createUDPNMMessageAdapter();
                }

                @Override
                public Adapter caseVerboseDLTMessage(VerboseDLTMessage object) {
                    return createVerboseDLTMessageAdapter();
                }

                @Override
                public Adapter caseUniversalPayloadWithLegacyExtractStrategy(UniversalPayloadWithLegacyExtractStrategy object) {
                    return createUniversalPayloadWithLegacyExtractStrategyAdapter();
                }

                @Override
                public Adapter caseAbstractBusMessage(AbstractBusMessage object) {
                    return createAbstractBusMessageAdapter();
                }

                @Override
                public Adapter casePluginFilter(PluginFilter object) {
                    return createPluginFilterAdapter();
                }

                @Override
                public Adapter casePluginMessage(PluginMessage object) {
                    return createPluginMessageAdapter();
                }

                @Override
                public Adapter casePluginSignal(PluginSignal object) {
                    return createPluginSignalAdapter();
                }

                @Override
                public Adapter casePluginStateExtractStrategy(PluginStateExtractStrategy object) {
                    return createPluginStateExtractStrategyAdapter();
                }

                @Override
                public Adapter casePluginResultExtractStrategy(PluginResultExtractStrategy object) {
                    return createPluginResultExtractStrategyAdapter();
                }

                @Override
                public Adapter caseEmptyDecodeStrategy(EmptyDecodeStrategy object) {
                    return createEmptyDecodeStrategyAdapter();
                }

                @Override
                public Adapter casePluginCheckExpression(PluginCheckExpression object) {
                    return createPluginCheckExpressionAdapter();
                }

                @Override
                public Adapter caseBaseClassWithID(BaseClassWithID object) {
                    return createBaseClassWithIDAdapter();
                }

                @Override
                public Adapter caseHeaderSignal(HeaderSignal object) {
                    return createHeaderSignalAdapter();
                }

                @Override
                public Adapter caseIVariableReaderWriter(IVariableReaderWriter object) {
                    return createIVariableReaderWriterAdapter();
                }

                @Override
                public Adapter caseIStateTransitionReference(IStateTransitionReference object) {
                    return createIStateTransitionReferenceAdapter();
                }

                @Override
                public Adapter caseTPFilter(TPFilter object) {
                    return createTPFilterAdapter();
                }

                @Override
                public Adapter caseBaseClassWithSourceReference(BaseClassWithSourceReference object) {
                    return createBaseClassWithSourceReferenceAdapter();
                }

                @Override
                public Adapter caseSourceReference(SourceReference object) {
                    return createSourceReferenceAdapter();
                }

                @Override
                public Adapter caseEthernetMessage(EthernetMessage object) {
                    return createEthernetMessageAdapter();
                }

                @Override
                public Adapter caseIPv4Message(IPv4Message object) {
                    return createIPv4MessageAdapter();
                }

                @Override
                public Adapter caseNonVerboseDLTMessage(NonVerboseDLTMessage object) {
                    return createNonVerboseDLTMessageAdapter();
                }

                @Override
                public Adapter caseStringDecodeStrategy(StringDecodeStrategy object) {
                    return createStringDecodeStrategyAdapter();
                }

                @Override
                public Adapter caseNonVerboseDLTFilter(NonVerboseDLTFilter object) {
                    return createNonVerboseDLTFilterAdapter();
                }

                @Override
                public Adapter caseVerboseDLTExtractStrategy(VerboseDLTExtractStrategy object) {
                    return createVerboseDLTExtractStrategyAdapter();
                }

                @Override
                public Adapter caseRegexOperation(RegexOperation object) {
                    return createRegexOperationAdapter();
                }

                @Override
                public Adapter casePayloadFilter(PayloadFilter object) {
                    return createPayloadFilterAdapter();
                }

                @Override
                public Adapter caseVerboseDLTPayloadFilter(VerboseDLTPayloadFilter object) {
                    return createVerboseDLTPayloadFilterAdapter();
                }

                @Override
                public Adapter caseAbstractVariable(AbstractVariable object) {
                    return createAbstractVariableAdapter();
                }

                @Override
                public Adapter caseComputedVariable(ComputedVariable object) {
                    return createComputedVariableAdapter();
                }

                @Override
                public Adapter caseStructureVariable(StructureVariable object) {
                    return createStructureVariableAdapter();
                }

                @Override
                public Adapter caseBusMessageReferable(BusMessageReferable object) {
                    return createBusMessageReferableAdapter();
                }

                @Override
                public Adapter caseSmardTraceElement(SmardTraceElement object) {
                    return createSmardTraceElementAdapter();
                }

                @Override
                public Adapter defaultCase(EObject object) {
                    return createEObjectAdapter();
                }
            };

    /**
     * Creates an adapter for the <code>target</code>.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }


    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DocumentRoot <em>Document Root</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DocumentRoot
     * @generated
     */
    public Adapter createDocumentRootAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ConditionSet <em>Condition Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ConditionSet
     * @generated
     */
    public Adapter createConditionSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Condition <em>Condition</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Condition
     * @generated
     */
    public Adapter createConditionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SignalComparisonExpression <em>Signal Comparison Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SignalComparisonExpression
     * @generated
     */
    public Adapter createSignalComparisonExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.CanMessageCheckExpression <em>Can Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.CanMessageCheckExpression
     * @generated
     */
    public Adapter createCanMessageCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.LinMessageCheckExpression <em>Lin Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.LinMessageCheckExpression
     * @generated
     */
    public Adapter createLinMessageCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StateCheckExpression <em>State Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StateCheckExpression
     * @generated
     */
    public Adapter createStateCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TimingExpression <em>Timing Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TimingExpression
     * @generated
     */
    public Adapter createTimingExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TrueExpression <em>True Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TrueExpression
     * @generated
     */
    public Adapter createTrueExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.LogicalExpression <em>Logical Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.LogicalExpression
     * @generated
     */
    public Adapter createLogicalExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ReferenceConditionExpression <em>Reference Condition
     * Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ReferenceConditionExpression
     * @generated
     */
    public Adapter createReferenceConditionExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TransitionCheckExpression <em>Transition Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TransitionCheckExpression
     * @generated
     */
    public Adapter createTransitionCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression <em>Flex Ray Message Check
     * Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessageCheckExpression
     * @generated
     */
    public Adapter createFlexRayMessageCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StopExpression <em>Stop Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StopExpression
     * @generated
     */
    public Adapter createStopExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ComparatorSignal <em>Comparator Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ComparatorSignal
     * @generated
     */
    public Adapter createComparatorSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ConstantComparatorValue <em>Constant Comparator Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ConstantComparatorValue
     * @generated
     */
    public Adapter createConstantComparatorValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.CalculationExpression <em>Calculation Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.CalculationExpression
     * @generated
     */
    public Adapter createCalculationExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Expression <em>Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Expression
     * @generated
     */
    public Adapter createExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VariableReference <em>Variable Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VariableReference
     * @generated
     */
    public Adapter createVariableReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ConditionsDocument <em>Document</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ConditionsDocument
     * @generated
     */
    public Adapter createConditionsDocumentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VariableSet <em>Variable Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VariableSet
     * @generated
     */
    public Adapter createVariableSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Variable <em>Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Variable
     * @generated
     */
    public Adapter createVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SignalVariable <em>Signal Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SignalVariable
     * @generated
     */
    public Adapter createSignalVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ValueVariable <em>Value Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ValueVariable
     * @generated
     */
    public Adapter createValueVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractObserver <em>Abstract Observer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractObserver
     * @generated
     */
    public Adapter createAbstractObserverAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ObserverValueRange <em>Observer Value Range</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ObserverValueRange
     * @generated
     */
    public Adapter createObserverValueRangeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SignalObserver <em>Signal Observer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SignalObserver
     * @generated
     */
    public Adapter createSignalObserverAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SignalReferenceSet <em>Signal Reference Set</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SignalReferenceSet
     * @generated
     */
    public Adapter createSignalReferenceSetAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SignalReference <em>Signal Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SignalReference
     * @generated
     */
    public Adapter createSignalReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ISignalOrReference <em>ISignal Or Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ISignalOrReference
     * @generated
     */
    public Adapter createISignalOrReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.BitPatternComparatorValue <em>Bit Pattern Comparator Value</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.BitPatternComparatorValue
     * @generated
     */
    public Adapter createBitPatternComparatorValueAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.NotExpression <em>Not Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.NotExpression
     * @generated
     */
    public Adapter createNotExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IOperand <em>IOperand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IOperand
     * @generated
     */
    public Adapter createIOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IComputeVariableActionOperand <em>ICompute Variable Action
     * Operand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IComputeVariableActionOperand
     * @generated
     */
    public Adapter createIComputeVariableActionOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IStringOperand <em>IString Operand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IStringOperand
     * @generated
     */
    public Adapter createIStringOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand <em>ISignal Comparison
     * Expression Operand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ISignalComparisonExpressionOperand
     * @generated
     */
    public Adapter createISignalComparisonExpressionOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IOperation <em>IOperation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IOperation
     * @generated
     */
    public Adapter createIOperationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IStringOperation <em>IString Operation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IStringOperation
     * @generated
     */
    public Adapter createIStringOperationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.INumericOperand <em>INumeric Operand</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.INumericOperand
     * @generated
     */
    public Adapter createINumericOperandAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.INumericOperation <em>INumeric Operation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.INumericOperation
     * @generated
     */
    public Adapter createINumericOperationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ValueVariableObserver <em>Value Variable Observer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ValueVariableObserver
     * @generated
     */
    public Adapter createValueVariableObserverAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ParseStringToDouble <em>Parse String To Double</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ParseStringToDouble
     * @generated
     */
    public Adapter createParseStringToDoubleAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StringExpression <em>String Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StringExpression
     * @generated
     */
    public Adapter createStringExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Matches <em>Matches</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Matches
     * @generated
     */
    public Adapter createMatchesAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Extract <em>Extract</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Extract
     * @generated
     */
    public Adapter createExtractAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.Substring <em>Substring</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.Substring
     * @generated
     */
    public Adapter createSubstringAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ParseNumericToString <em>Parse Numeric To String</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ParseNumericToString
     * @generated
     */
    public Adapter createParseNumericToStringAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StringLength <em>String Length</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StringLength
     * @generated
     */
    public Adapter createStringLengthAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractMessage <em>Abstract Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractMessage
     * @generated
     */
    public Adapter createAbstractMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SomeIPMessage <em>Some IP Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SomeIPMessage
     * @generated
     */
    public Adapter createSomeIPMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractSignal <em>Abstract Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractSignal
     * @generated
     */
    public Adapter createAbstractSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ContainerSignal <em>Container Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ContainerSignal
     * @generated
     */
    public Adapter createContainerSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractFilter <em>Abstract Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractFilter
     * @generated
     */
    public Adapter createAbstractFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.EthernetFilter <em>Ethernet Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.EthernetFilter
     * @generated
     */
    public Adapter createEthernetFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UDPFilter <em>UDP Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UDPFilter
     * @generated
     */
    public Adapter createUDPFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TCPFilter <em>TCP Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TCPFilter
     * @generated
     */
    public Adapter createTCPFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IPv4Filter <em>IPv4 Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IPv4Filter
     * @generated
     */
    public Adapter createIPv4FilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SomeIPFilter <em>Some IP Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SomeIPFilter
     * @generated
     */
    public Adapter createSomeIPFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ForEachExpression <em>For Each Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ForEachExpression
     * @generated
     */
    public Adapter createForEachExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.MessageCheckExpression <em>Message Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.MessageCheckExpression
     * @generated
     */
    public Adapter createMessageCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter <em>Some IPSD Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDFilter
     * @generated
     */
    public Adapter createSomeIPSDFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SomeIPSDMessage <em>Some IPSD Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDMessage
     * @generated
     */
    public Adapter createSomeIPSDMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DoubleSignal <em>Double Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DoubleSignal
     * @generated
     */
    public Adapter createDoubleSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StringSignal <em>String Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StringSignal
     * @generated
     */
    public Adapter createStringSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DecodeStrategy <em>Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DecodeStrategy
     * @generated
     */
    public Adapter createDecodeStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DoubleDecodeStrategy <em>Double Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DoubleDecodeStrategy
     * @generated
     */
    public Adapter createDoubleDecodeStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ExtractStrategy <em>Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ExtractStrategy
     * @generated
     */
    public Adapter createExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy <em>Universal Payload Extract
     * Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadExtractStrategy
     * @generated
     */
    public Adapter createUniversalPayloadExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.EmptyExtractStrategy <em>Empty Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.EmptyExtractStrategy
     * @generated
     */
    public Adapter createEmptyExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.CANFilter <em>CAN Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.CANFilter
     * @generated
     */
    public Adapter createCANFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.LINFilter <em>LIN Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.LINFilter
     * @generated
     */
    public Adapter createLINFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.FlexRayFilter <em>Flex Ray Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.FlexRayFilter
     * @generated
     */
    public Adapter createFlexRayFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DLTFilter <em>DLT Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DLTFilter
     * @generated
     */
    public Adapter createDLTFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UDPNMFilter <em>UDPNM Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UDPNMFilter
     * @generated
     */
    public Adapter createUDPNMFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.CANMessage <em>CAN Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.CANMessage
     * @generated
     */
    public Adapter createCANMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.LINMessage <em>LIN Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.LINMessage
     * @generated
     */
    public Adapter createLINMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.FlexRayMessage <em>Flex Ray Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.FlexRayMessage
     * @generated
     */
    public Adapter createFlexRayMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.DLTMessage <em>DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.DLTMessage
     * @generated
     */
    public Adapter createDLTMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UDPMessage <em>UDP Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UDPMessage
     * @generated
     */
    public Adapter createUDPMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.RegexOperation <em>Regex Operation</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.RegexOperation
     * @generated
     */
    public Adapter createRegexOperationAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TCPMessage <em>TCP Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TCPMessage
     * @generated
     */
    public Adapter createTCPMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter <em>Verbose DLT Payload Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter
     * @generated
     */
    public Adapter createVerboseDLTPayloadFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractVariable <em>Abstract Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractVariable
     * @generated
     */
    public Adapter createAbstractVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StructureVariable <em>Structure Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StructureVariable
     * @generated
     */
    public Adapter createStructureVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachineset.BusMessageReferable <em>Bus Message Referable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachineset.BusMessageReferable
     * @generated
     */
    public Adapter createBusMessageReferableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.ComputedVariable <em>Computed Variable</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.ComputedVariable
     * @generated
     */
    public Adapter createComputedVariableAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VariableFormat <em>Variable Format</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VariableFormat
     * @generated
     */
    public Adapter createVariableFormatAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PayloadFilter <em>Payload Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PayloadFilter
     * @generated
     */
    public Adapter createPayloadFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UDPNMMessage <em>UDPNM Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UDPNMMessage
     * @generated
     */
    public Adapter createUDPNMMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VerboseDLTMessage <em>Verbose DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTMessage
     * @generated
     */
    public Adapter createVerboseDLTMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy <em>Universal Payload
     * With Legacy Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.UniversalPayloadWithLegacyExtractStrategy
     * @generated
     */
    public Adapter createUniversalPayloadWithLegacyExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.AbstractBusMessage <em>Abstract Bus Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.AbstractBusMessage
     * @generated
     */
    public Adapter createAbstractBusMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginFilter <em>Plugin Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginFilter
     * @generated
     */
    public Adapter createPluginFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginMessage <em>Plugin Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginMessage
     * @generated
     */
    public Adapter createPluginMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginSignal <em>Plugin Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginSignal
     * @generated
     */
    public Adapter createPluginSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginStateExtractStrategy <em>Plugin State Extract
     * Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginStateExtractStrategy
     * @generated
     */
    public Adapter createPluginStateExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginResultExtractStrategy <em>Plugin Result Extract
     * Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginResultExtractStrategy
     * @generated
     */
    public Adapter createPluginResultExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.EmptyDecodeStrategy <em>Empty Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.EmptyDecodeStrategy
     * @generated
     */
    public Adapter createEmptyDecodeStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.PluginCheckExpression <em>Plugin Check Expression</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.PluginCheckExpression
     * @generated
     */
    public Adapter createPluginCheckExpressionAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.BaseClassWithID <em>Base Class With ID</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithID
     * @generated
     */
    public Adapter createBaseClassWithIDAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.HeaderSignal <em>Header Signal</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.HeaderSignal
     * @generated
     */
    public Adapter createHeaderSignalAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IVariableReaderWriter <em>IVariable Reader Writer</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IVariableReaderWriter
     * @generated
     */
    public Adapter createIVariableReaderWriterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IStateTransitionReference <em>IState Transition Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IStateTransitionReference
     * @generated
     */
    public Adapter createIStateTransitionReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.TPFilter <em>TP Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.TPFilter
     * @generated
     */
    public Adapter createTPFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.BaseClassWithSourceReference <em>Base Class With Source
     * Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.BaseClassWithSourceReference
     * @generated
     */
    public Adapter createBaseClassWithSourceReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.SourceReference <em>Source Reference</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.SourceReference
     * @generated
     */
    public Adapter createSourceReferenceAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.EthernetMessage <em>Ethernet Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.EthernetMessage
     * @generated
     */
    public Adapter createEthernetMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.IPv4Message <em>IPv4 Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.IPv4Message
     * @generated
     */
    public Adapter createIPv4MessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTMessage <em>Non Verbose DLT Message</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTMessage
     * @generated
     */
    public Adapter createNonVerboseDLTMessageAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.StringDecodeStrategy <em>String Decode Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.StringDecodeStrategy
     * @generated
     */
    public Adapter createStringDecodeStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.NonVerboseDLTFilter <em>Non Verbose DLT Filter</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.NonVerboseDLTFilter
     * @generated
     */
    public Adapter createNonVerboseDLTFilterAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy <em>Verbose DLT Extract Strategy</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.conditions.VerboseDLTExtractStrategy
     * @generated
     */
    public Adapter createVerboseDLTExtractStrategyAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link de.bmw.smard.modeller.statemachine.SmardTraceElement <em>Smard Trace Element</em>}'.
     * <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases anyway.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see de.bmw.smard.modeller.statemachine.SmardTraceElement
     * @generated
     */
    public Adapter createSmardTraceElementAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case.
     * <!-- begin-user-doc -->
     * This default implementation returns null.
     * <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} //ConditionsAdapterFactory
