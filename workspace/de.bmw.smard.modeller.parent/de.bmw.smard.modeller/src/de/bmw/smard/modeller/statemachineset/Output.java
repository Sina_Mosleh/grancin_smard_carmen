package de.bmw.smard.modeller.statemachineset;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachineset.Output#getTargetFilePath <em>Target File Path</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getOutput()
 * @model extendedMetaData="name='output' kind='empty'"
 * @generated
 */
public interface Output extends EObject {
    /**
     * Returns the value of the '<em><b>Target File Path</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Target File Path</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Target File Path</em>' attribute.
     * @see #setTargetFilePath(String)
     * @see de.bmw.smard.modeller.statemachineset.StatemachinesetPackage#getOutput_TargetFilePath()
     * @model dataType="org.eclipse.emf.ecore.xml.type.String"
     *        extendedMetaData="kind='attribute' name='targetFilePath'"
     * @generated
     */
    String getTargetFilePath();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.statemachineset.Output#getTargetFilePath <em>Target File Path</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Target File Path</em>' attribute.
     * @see #getTargetFilePath()
     * @generated
     */
    void setTargetFilePath(String value);

} // Output
