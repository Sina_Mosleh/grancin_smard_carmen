package de.bmw.smard.modeller.util.validation;

import de.bmw.smard.common.data.PluginActionExpressionParser;
import de.bmw.smard.common.math.exceptions.ActionExpressionParseException;
import de.bmw.smard.common.plugin.Plugin;
import de.bmw.smard.common.plugin.PluginRegistry;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.PluginFilter;
import de.bmw.smard.modeller.statemachine.Action;
import de.bmw.smard.modeller.statemachine.ActionTypeNotEmpty;
import de.bmw.smard.modeller.util.validation.SMARDModelValidator.ValidationContext;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

import java.util.*;

public class PluginValidator implements IModelValidator {

	private Map<ValidationContext, Boolean> applicableContexts = new HashMap<>();
	private final PluginRegistry pluginRegistry;

	public PluginValidator(PluginRegistry pluginRegistry) {

		applicableContexts.put(ValidationContext.EXPORT_VALIDATION_CONTEXT, Boolean.TRUE);

		this.pluginRegistry = pluginRegistry;
	}

	@Override
	public boolean validate(Set<EObject> objects2Validate, DiagnosticChain diagChain, Map<Object, Object> context) {
		if (objects2Validate == null)
			return false;

		boolean result = true;
		for (EObject eObject : objects2Validate) {
			result = validate(eObject, diagChain, context) && result;
		}
		return result;
	}

	@Override
	public boolean validate(EObject object2Validate, DiagnosticChain diagChain, Map<Object,Object> context) {
		if (object2Validate == null)
			return false;
		boolean result = true;
		String errMessageID;

		if (object2Validate instanceof Action) {
			errMessageID = "_Validation_global_Action_ActionExpression_Plugin";
			Action action = (Action) object2Validate;
			if (action.getActionType() == ActionTypeNotEmpty.PLUGIN) {
				String actionExpression = action.getActionExpression();

				PluginActionExpressionParser parser = PluginActionExpressionParser.parse(actionExpression);
				if (parser.hasError()) {
					result = false;
					diagChain.add(DiagnosticBuilder.error()
							.messageId(PluginActivator.INSTANCE, errMessageID, parser.getError())
							.data(object2Validate)
							.build());
				} else {
					String name = parser.getName();
					String version = parser.getVersion();
					String method = parser.getMethod();
					List<String> errMsg = new ArrayList<>();
					result = validatePluginActionExpression(name, version, method, parser.getPluginVariables(), action, errMsg);
					if (!result) {
						diagChain.add(DiagnosticBuilder.error()
								.messageId(PluginActivator.INSTANCE, errMessageID, errMsg)
								.data(object2Validate)
								.build());

					}
				}
			}
		}

		else {
			errMessageID = "_Validation_global_Plugin";

			if(object2Validate instanceof PluginFilter) {
				PluginFilter filter = (PluginFilter) object2Validate;
				if (!pluginRegistry.contains(filter.getPluginName(), filter.getPluginVersion())) {
					diagChain.add(DiagnosticBuilder.error()
					.messageId(PluginActivator.INSTANCE, errMessageID, filter.getPluginName())
					.data(object2Validate)
					.build());

					result = false;
				}
			}
		}

		return result;
	}

	/**
	 * 
	 * @param pPluginName
	 * @param pPluginVersion
	 * @param pMethod
	 * @param pPluginVariableNames
	 * @param pUsingObject
	 * @return
	 * @throws ActionExpressionParseException
	 * @generated NOT
	 */
	private boolean validatePluginActionExpression(String pPluginName, String pPluginVersion, String pMethod,
			Vector<String> pPluginVariableNames, Action pUsingObject, List<String> errMsgs)
			throws ActionExpressionParseException {
		boolean resultOk = true;

		// pr�fen, ob Plugin bereits geladen wurde: wenn nicht dann versuchen zu
		// laden
		// => Scanne Plugins-Verzeichnis nach Eintrag <plugin
		// name>+"_"+<version>+".jar"( - ohne Leerzeichen) ==
		// pActionExpression + ".jar"
		// wenn Laden nicht erfolgreich

		final String unknown_plugin_var_used = "Unknown plugin variables used.";
		Plugin plugin = pluginRegistry.getPlugin(pPluginName, pPluginVersion);
		if (plugin == null) {
			errMsgs.add("The plugin '" + pPluginName + "'" + (pPluginVersion != null ? (" " + pPluginVersion) : "")
					+ " is not registered.");
			resultOk = false;
		} else if (pPluginVariableNames.size() > 0) {

			Map<String, Class<?>> variables = plugin.getVariables();
			if (variables == null || variables.size() == 0) {
				errMsgs.add(unknown_plugin_var_used);
				resultOk = false;
			} else {
				StringBuilder sb = new StringBuilder(unknown_plugin_var_used);
				for (String var : pPluginVariableNames) {
					if (var.contains("[") && var.contains("]")) {
						var = var.substring(0, var.indexOf("["));
					}
					if (variables.containsKey(var) == false) {
						sb.append(resultOk ? ": " : ", ");
						sb.append(var);
						resultOk = false;
					}
				}
				if (!resultOk) {
					errMsgs.add(sb.toString());
				}
			}
		}
		return resultOk;
	}

	@Override
	public boolean accept4Validation(Object objToValidate) {
		for (Class clazz : getRelevantTypes()) {
			if (clazz.isAssignableFrom(objToValidate.getClass()))
				return true;
		}
		return false;
	}

	@Override
	public boolean accept4Validation(Class classToValidate) {
		for (Class clazz : getRelevantTypes()) {
			if (clazz.isAssignableFrom(classToValidate))
				return true;
		}
		return false;
	}

	@Override
	public Set<Class> getRelevantTypes() {
		Set<Class> relevantTypes = new HashSet<Class>();
		relevantTypes.add(Action.class);

		relevantTypes.add(PluginFilter.class);
		// TODO SR
//		relevantTypes.add(PluginStateSignal.class);
//		relevantTypes.add(PluginResultSignal.class);
		return relevantTypes;
	}

	@Override
	public boolean isApplicableIn(ValidationContext context) {
		Boolean value = applicableContexts.get(context);
		if (value != null)
			return value;
		// todo mj warning
		return false;
	}
}
