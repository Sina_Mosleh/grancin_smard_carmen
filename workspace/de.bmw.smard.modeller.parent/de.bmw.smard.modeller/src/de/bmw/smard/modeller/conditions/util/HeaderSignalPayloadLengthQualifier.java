package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.common.interfaces.bus.MessageType;

import java.util.Arrays;
import java.util.Collection;

public class HeaderSignalPayloadLengthQualifier {

    private static final String DLC_PREFIX = "DLC";
    private static final String PAYLOAD_LENGTH_PREFIX = "payloadLength";
    private static final String UNDERSCORE = "_";

    public static String getName(MessageType messageType) {
        String result = "";
        if (messageType == null) {
            return result;
        }

        if (MessageType.CAN.equals(messageType)
            || MessageType.LIN.equals(messageType)
            || MessageType.FLEXRAY.equals(messageType)) {
            result = DLC_PREFIX;
        } else {
            result = PAYLOAD_LENGTH_PREFIX;
        }

        result = result + UNDERSCORE + messageType.name();
        return result;
    }

    public static String getPayloadLengthPrefix() {
        return PAYLOAD_LENGTH_PREFIX;
    }

    public static Collection<String> getPayloadLengthPrefixes() {
        return Arrays.asList(PAYLOAD_LENGTH_PREFIX, DLC_PREFIX);
    }
}
