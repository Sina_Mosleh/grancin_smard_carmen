package de.bmw.smard.modeller.statemachineset.impl;

import de.bmw.smard.modeller.statemachineset.Output;
import de.bmw.smard.modeller.statemachineset.StatemachinesetPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Output</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.statemachineset.impl.OutputImpl#getTargetFilePath <em>Target File Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OutputImpl extends EObjectImpl implements Output {
    /**
     * The default value of the '{@link #getTargetFilePath() <em>Target File Path</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTargetFilePath()
     * @generated
     * @ordered
     */
    protected static final String TARGET_FILE_PATH_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTargetFilePath() <em>Target File Path</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTargetFilePath()
     * @generated
     * @ordered
     */
    protected String targetFilePath = TARGET_FILE_PATH_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected OutputImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return StatemachinesetPackage.Literals.OUTPUT;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTargetFilePath() {
        return targetFilePath;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTargetFilePath(String newTargetFilePath) {
        String oldTargetFilePath = targetFilePath;
        targetFilePath = newTargetFilePath;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, StatemachinesetPackage.OUTPUT__TARGET_FILE_PATH, oldTargetFilePath, targetFilePath));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case StatemachinesetPackage.OUTPUT__TARGET_FILE_PATH:
                return getTargetFilePath();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case StatemachinesetPackage.OUTPUT__TARGET_FILE_PATH:
                setTargetFilePath((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case StatemachinesetPackage.OUTPUT__TARGET_FILE_PATH:
                setTargetFilePath(TARGET_FILE_PATH_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case StatemachinesetPackage.OUTPUT__TARGET_FILE_PATH:
                return TARGET_FILE_PATH_EDEFAULT == null ? targetFilePath != null : !TARGET_FILE_PATH_EDEFAULT.equals(targetFilePath);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (targetFilePath: ");
        result.append(targetFilePath);
        result.append(')');
        return result.toString();
    }

} //OutputImpl
