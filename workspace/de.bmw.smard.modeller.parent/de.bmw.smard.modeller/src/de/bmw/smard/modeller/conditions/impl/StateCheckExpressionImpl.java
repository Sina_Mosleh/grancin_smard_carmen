package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.BooleanOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.StateCheckExpression;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.statemachine.AbstractState;
import de.bmw.smard.modeller.statemachine.InitialState;
import de.bmw.smard.modeller.statemachine.State;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>State Check Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl#getCheckStateActive <em>Check State Active</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl#getCheckState <em>Check State</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.StateCheckExpressionImpl#getCheckStateActiveTmplParam <em>Check State Active Tmpl Param</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StateCheckExpressionImpl extends ExpressionImpl implements StateCheckExpression {
    /**
     * The default value of the '{@link #getCheckStateActive() <em>Check State Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCheckStateActive()
     * @generated
     * @ordered
     */
    protected static final BooleanOrTemplatePlaceholderEnum CHECK_STATE_ACTIVE_EDEFAULT = BooleanOrTemplatePlaceholderEnum.TRUE;

    /**
     * The cached value of the '{@link #getCheckStateActive() <em>Check State Active</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCheckStateActive()
     * @generated
     * @ordered
     */
    protected BooleanOrTemplatePlaceholderEnum checkStateActive = CHECK_STATE_ACTIVE_EDEFAULT;

    /**
     * The cached value of the '{@link #getCheckState() <em>Check State</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCheckState()
     * @generated
     * @ordered
     */
    protected State checkState;

    /**
     * The default value of the '{@link #getCheckStateActiveTmplParam() <em>Check State Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @see #getCheckStateActiveTmplParam()
     * @generated
     * @ordered
     */
    protected static final String CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCheckStateActiveTmplParam() <em>Check State Active Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #getCheckStateActiveTmplParam()
     * @generated
     * @ordered
     */
    protected String checkStateActiveTmplParam = CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StateCheckExpressionImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.STATE_CHECK_EXPRESSION;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public BooleanOrTemplatePlaceholderEnum getCheckStateActive() {
        return checkStateActive;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCheckStateActive(BooleanOrTemplatePlaceholderEnum newCheckStateActive) {
        BooleanOrTemplatePlaceholderEnum oldCheckStateActive = checkStateActive;
        checkStateActive = newCheckStateActive == null ? CHECK_STATE_ACTIVE_EDEFAULT : newCheckStateActive;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE, oldCheckStateActive, checkStateActive));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        if (getCheckState() != null) {
            return getCheckState().getAllReferencedBusMessages();
        }
        return new BasicEList<AbstractBusMessage>();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public State getCheckState() {
        if (checkState != null && checkState.eIsProxy()) {
            InternalEObject oldCheckState = (InternalEObject) checkState;
            checkState = (State) eResolveProxy(oldCheckState);
            if (checkState != oldCheckState) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE, oldCheckState, checkState));
            }
        }
        return checkState;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public State basicGetCheckState() {
        return checkState;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCheckState(State newCheckState) {
        State oldCheckState = checkState;
        checkState = newCheckState;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE, oldCheckState, checkState));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getCheckStateActiveTmplParam() {
        return checkStateActiveTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCheckStateActiveTmplParam(String newCheckStateActiveTmplParam) {
        String oldCheckStateActiveTmplParam = checkStateActiveTmplParam;
        checkStateActiveTmplParam = newCheckStateActiveTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM, oldCheckStateActiveTmplParam, checkStateActiveTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStatesActive(DiagnosticChain diagnostics, Map<Object, Object> context) {

        BooleanOrTemplatePlaceholderEnum activeStates = getCheckStateActive();

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(activeStates)
                        .error("_Validation_Conditions_StateCheckExpression_StatesActive_IsNull")
                        .build())

                .with(Validators.checkTemplate(activeStates)
                        .templateType(BooleanOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(checkStateActiveTmplParam)
                        .containsParameterError("_Validation_Conditions_StateCheckExpression_StatesActive_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_StateCheckExpression_StatesActiveTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_StateCheckExpression_StatesActiveTmplParam_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_StateCheckExpression_StatesActive_IllegalStatesActive")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidState(DiagnosticChain diagnostics, Map<Object, Object> context) {

        State state = getCheckState();

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.STATE_CHECK_EXPRESSION__IS_VALID_HAS_VALID_STATE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(state instanceof InitialState)
                        .error("_Validation_Conditions_StateCheckExpression_CheckState_IsInitialState")
                        .build())

                .with(Validators.when(TemplateUtils.getTemplateCategorization(this).isExportable())
                        .and(!TemplateUtils.getTemplateCategorization(getCheckState()).isExportable())
                        .error("_Validation_Conditions_StateCheckExpression_CheckState_IsTemplateState")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
                return getCheckStateActive();
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
                if (resolve) return getCheckState();
                return basicGetCheckState();
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
                return getCheckStateActiveTmplParam();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
                setCheckStateActive((BooleanOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
                setCheckState((State) newValue);
                return;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
                setCheckStateActiveTmplParam((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
                setCheckStateActive(CHECK_STATE_ACTIVE_EDEFAULT);
                return;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
                setCheckState((State) null);
                return;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
                setCheckStateActiveTmplParam(CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE:
                return checkStateActive != CHECK_STATE_ACTIVE_EDEFAULT;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE:
                return checkState != null;
            case ConditionsPackage.STATE_CHECK_EXPRESSION__CHECK_STATE_ACTIVE_TMPL_PARAM:
                return CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT == null ? checkStateActiveTmplParam != null : !CHECK_STATE_ACTIVE_TMPL_PARAM_EDEFAULT
                        .equals(checkStateActiveTmplParam);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (checkStateActive: ");
        result.append(checkStateActive);
        result.append(", checkStateActiveTmplParam: ");
        result.append(checkStateActiveTmplParam);
        result.append(')');
        return result.toString();
    }

    /**
     * @generated NOT
     */
    @Override
    public EList<AbstractState> getStateDependencies() {
        return ECollections.singletonEList((AbstractState) getCheckState());
    }

} // StateCheckExpressionImpl
