package de.bmw.smard.modeller.conditions.util;

import de.bmw.smard.modeller.conditions.FormatDataType;
import de.bmw.smard.modeller.conditions.VariableFormat;
import de.bmw.smard.modeller.conditions.impl.VariableFormatImpl;
import de.bmw.smard.modeller.util.TemplateUtils;

public class VariableFormatConverter {
    public static VariableFormat parseToFormat(String formatString) {
        VariableFormat format = new VariableFormatImpl();
        String regex = "(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)";
        String[] split = formatString.split(regex, 2);
        int digits = 0;
        String dataTypeString;
        if (split.length > 1) {
            dataTypeString = split[0];
            digits = Integer.valueOf(split[1]);
        } else {
            dataTypeString = formatString;
        }
        FormatDataType dataType = getDataType(dataTypeString);
        format.setBaseDataType(dataType);
        format.setUpperCase(isUpperCase(dataTypeString, dataType));
        format.setDigits(digits);
        return format;
    }

    private static boolean isUpperCase(String format, FormatDataType dataType) {
        if (!(dataType == FormatDataType.HEX || dataType == FormatDataType.BOOL || dataType == FormatDataType.MAC))
            return false;
        for (Character c : format.toCharArray()) {
            if (Character.isLowerCase(c))
                return false;
        }
        return true;
    }

    private static FormatDataType getDataType(String format) {
        if (format.equalsIgnoreCase(FormatDataType.FLOAT.toString())) {
            return FormatDataType.FLOAT;
        } else if (format.equalsIgnoreCase(FormatDataType.HEX.toString()) || format.equalsIgnoreCase("x")) {
            return FormatDataType.HEX;
        } else if (format.equalsIgnoreCase(FormatDataType.BOOL.toString()) || format.equalsIgnoreCase("b")) {
            return FormatDataType.BOOL;
        } else if (format.equalsIgnoreCase(FormatDataType.DEC.toString()) || format.equalsIgnoreCase("d")
            || format.equalsIgnoreCase("decimal")) {
                return FormatDataType.DEC;
            } else if (format.equalsIgnoreCase(FormatDataType.BIN.toString()) || format.equalsIgnoreCase("binary")) {
                return FormatDataType.BIN;
            } else if (format.equalsIgnoreCase(FormatDataType.OCT.toString()) || format.equalsIgnoreCase("o")
                || format.equalsIgnoreCase("octal")) {
                    return FormatDataType.OCT;
                } else if (format.equalsIgnoreCase(FormatDataType.HHMMSS.toString())) {
                    return FormatDataType.HHMMSS;
                } else if (format.equalsIgnoreCase(FormatDataType.MS.toString())) {
                    return FormatDataType.MS;
                } else if (format.equalsIgnoreCase(FormatDataType.IP.toString())) {
                    return FormatDataType.IP;
                } else if (format.equalsIgnoreCase(FormatDataType.MAC.toString())) {
                    return FormatDataType.MAC;
                } else if (format.equalsIgnoreCase(FormatDataType.CEIL.toString())) {
                    return FormatDataType.CEIL;
                } else if (format.equalsIgnoreCase(FormatDataType.ROUND.toString())) {
                    return FormatDataType.ROUND;
                } else if (TemplateUtils.checkValidPlaceholderNameWithHashtags(format) == null) {
                    return FormatDataType.TEMPLATEDEFINED;
                }
        return FormatDataType.FLOAT;
    }
}
