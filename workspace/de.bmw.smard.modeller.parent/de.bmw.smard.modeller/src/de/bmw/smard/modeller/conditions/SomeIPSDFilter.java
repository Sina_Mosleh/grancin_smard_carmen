package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IPSD Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlags <em>Flags</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlagsTmplParam <em>Flags Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdType <em>Sd Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdTypeTmplParam <em>Sd Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getInstanceId <em>Instance Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getTtl <em>Ttl</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMajorVersion <em>Major Version</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMinorVersion <em>Minor Version</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getEventGroupId <em>Event Group Id</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexFirstOption <em>Index First Option</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexSecondOption <em>Index Second Option</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberFirstOption <em>Number First Option</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberSecondOption <em>Number Second Option</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getServiceId_SomeIPSD <em>Service Id Some IPSD</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter()
 * @model
 * @generated
 */
public interface SomeIPSDFilter extends AbstractFilter {
    /**
     * Returns the value of the '<em><b>Flags</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Flags</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Flags</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
     * @see #setFlags(SomeIPSDEntryFlagsOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_Flags()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    SomeIPSDEntryFlagsOrTemplatePlaceholderEnum getFlags();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlags <em>Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Flags</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDEntryFlagsOrTemplatePlaceholderEnum
     * @see #getFlags()
     * @generated
     */
    void setFlags(SomeIPSDEntryFlagsOrTemplatePlaceholderEnum value);

    /**
     * Returns the value of the '<em><b>Sd Type Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sd Type Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Sd Type Tmpl Param</em>' attribute.
     * @see #setSdTypeTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_SdTypeTmplParam()
     * @model
     * @generated
     */
    String getSdTypeTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdTypeTmplParam <em>Sd Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Sd Type Tmpl Param</em>' attribute.
     * @see #getSdTypeTmplParam()
     * @generated
     */
    void setSdTypeTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Instance Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Instance Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Instance Id</em>' attribute.
     * @see #setInstanceId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_InstanceId()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getInstanceId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getInstanceId <em>Instance Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Instance Id</em>' attribute.
     * @see #getInstanceId()
     * @generated
     */
    void setInstanceId(String value);

    /**
     * Returns the value of the '<em><b>Ttl</b></em>' attribute.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Ttl</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Ttl</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum
     * @see #setTtl(TTLOrTemplatePlaceHolderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_Ttl()
     * @model annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    TTLOrTemplatePlaceHolderEnum getTtl();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getTtl <em>Ttl</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Ttl</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.TTLOrTemplatePlaceHolderEnum
     * @see #getTtl()
     * @generated
     */
    void setTtl(TTLOrTemplatePlaceHolderEnum value);

    /**
     * Returns the value of the '<em><b>Major Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Major Version</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Major Version</em>' attribute.
     * @see #setMajorVersion(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_MajorVersion()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getMajorVersion();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMajorVersion <em>Major Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Major Version</em>' attribute.
     * @see #getMajorVersion()
     * @generated
     */
    void setMajorVersion(String value);

    /**
     * Returns the value of the '<em><b>Minor Version</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Minor Version</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Minor Version</em>' attribute.
     * @see #setMinorVersion(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_MinorVersion()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getMinorVersion();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getMinorVersion <em>Minor Version</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Minor Version</em>' attribute.
     * @see #getMinorVersion()
     * @generated
     */
    void setMinorVersion(String value);

    /**
     * Returns the value of the '<em><b>Event Group Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Event Group Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Event Group Id</em>' attribute.
     * @see #setEventGroupId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_EventGroupId()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getEventGroupId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getEventGroupId <em>Event Group Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Event Group Id</em>' attribute.
     * @see #getEventGroupId()
     * @generated
     */
    void setEventGroupId(String value);

    /**
     * Returns the value of the '<em><b>Index First Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Index First Option</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Index First Option</em>' attribute.
     * @see #setIndexFirstOption(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_IndexFirstOption()
     * @model dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getIndexFirstOption();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexFirstOption <em>Index First Option</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Index First Option</em>' attribute.
     * @see #getIndexFirstOption()
     * @generated
     */
    void setIndexFirstOption(String value);

    /**
     * Returns the value of the '<em><b>Index Second Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Index Second Option</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Index Second Option</em>' attribute.
     * @see #setIndexSecondOption(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_IndexSecondOption()
     * @model dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getIndexSecondOption();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getIndexSecondOption <em>Index Second Option</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Index Second Option</em>' attribute.
     * @see #getIndexSecondOption()
     * @generated
     */
    void setIndexSecondOption(String value);

    /**
     * Returns the value of the '<em><b>Number First Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Number First Option</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Number First Option</em>' attribute.
     * @see #setNumberFirstOption(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_NumberFirstOption()
     * @model dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getNumberFirstOption();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberFirstOption <em>Number First Option</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Number First Option</em>' attribute.
     * @see #getNumberFirstOption()
     * @generated
     */
    void setNumberFirstOption(String value);

    /**
     * Returns the value of the '<em><b>Number Second Option</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Number Second Option</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Number Second Option</em>' attribute.
     * @see #setNumberSecondOption(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_NumberSecondOption()
     * @model dataType="de.bmw.smard.modeller.conditions.IntOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getNumberSecondOption();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getNumberSecondOption <em>Number Second Option</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Number Second Option</em>' attribute.
     * @see #getNumberSecondOption()
     * @generated
     */
    void setNumberSecondOption(String value);

    /**
     * Returns the value of the '<em><b>Service Id Some IPSD</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Service Id Some IPSD</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Service Id Some IPSD</em>' attribute.
     * @see #setServiceId_SomeIPSD(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_ServiceId_SomeIPSD()
     * @model dataType="de.bmw.smard.modeller.conditions.HexOrIntOrNullOrTemplatePlaceholder"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='single'"
     * @generated
     */
    String getServiceId_SomeIPSD();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getServiceId_SomeIPSD <em>Service Id Some IPSD</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Service Id Some IPSD</em>' attribute.
     * @see #getServiceId_SomeIPSD()
     * @generated
     */
    void setServiceId_SomeIPSD(String value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidInstanceId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidEventGroupId(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidFlags(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSdType(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMajorVersion(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidMinorVersion(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidIndexFirstOption(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidIndexSecondOption(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidNumberFirstOption(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidNumberSecondOption(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * Returns the value of the '<em><b>Flags Tmpl Param</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Flags Tmpl Param</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Flags Tmpl Param</em>' attribute.
     * @see #setFlagsTmplParam(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_FlagsTmplParam()
     * @model
     * @generated
     */
    String getFlagsTmplParam();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getFlagsTmplParam <em>Flags Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Flags Tmpl Param</em>' attribute.
     * @see #getFlagsTmplParam()
     * @generated
     */
    void setFlagsTmplParam(String value);

    /**
     * Returns the value of the '<em><b>Sd Type</b></em>' attribute.
     * The default value is <code>"ALL"</code>.
     * The literals are from the enumeration {@link de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Sd Type</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Sd Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
     * @see #setSdType(SomeIPSDTypeOrTemplatePlaceholderEnum)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDFilter_SdType()
     * @model default="ALL"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     *        annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='enum'"
     * @generated
     */
    SomeIPSDTypeOrTemplatePlaceholderEnum getSdType();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDFilter#getSdType <em>Sd Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Sd Type</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.SomeIPSDTypeOrTemplatePlaceholderEnum
     * @see #getSdType()
     * @generated
     */
    void setSdType(SomeIPSDTypeOrTemplatePlaceholderEnum value);

} // SomeIPSDFilter
