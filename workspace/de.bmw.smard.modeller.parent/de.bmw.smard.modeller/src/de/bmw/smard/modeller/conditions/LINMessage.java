package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LIN Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.LINMessage#getLinFilter <em>Lin Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLINMessage()
 * @model
 * @generated
 */
public interface LINMessage extends AbstractBusMessage {
    /**
     * Returns the value of the '<em><b>Lin Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Lin Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Lin Filter</em>' containment reference.
     * @see #setLinFilter(LINFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getLINMessage_LinFilter()
     * @model containment="true" required="true"
     * @generated
     */
    LINFilter getLinFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.LINMessage#getLinFilter <em>Lin Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Lin Filter</em>' containment reference.
     * @see #getLinFilter()
     * @generated
     */
    void setLinFilter(LINFilter value);

} // LINMessage
