package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Calculation Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.CalculationExpression#getExpression <em>Expression</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.CalculationExpression#getOperands <em>Operands</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCalculationExpression()
 * @model
 * @generated
 */
public interface CalculationExpression extends ComparatorSignal, IOperation {
    /**
     * Returns the value of the '<em><b>Expression</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Expression</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Expression</em>' attribute.
     * @see #setExpression(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCalculationExpression_Expression()
     * @model annotation="http:///de/bmw/smard/modeller/TemplateMetaData attrType='multiple'"
     * @generated
     */
    String getExpression();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.CalculationExpression#getExpression <em>Expression</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Expression</em>' attribute.
     * @see #getExpression()
     * @generated
     */
    void setExpression(String value);

    /**
     * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
     * The list contents are of type {@link de.bmw.smard.modeller.conditions.INumericOperand}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Operands</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Operands</em>' containment reference list.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCalculationExpression_Operands()
     * @model containment="true"
     * @generated
     */
    EList<INumericOperand> getOperands();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidExpression(DiagnosticChain diagnostics, Map<Object, Object> context);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidOperands(DiagnosticChain diagnostics, Map<Object, Object> context);

} // CalculationExpression
