package de.bmw.smard.modeller.validator.value;

import java.util.Arrays;
import java.util.Collection;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FlexRayRepetitionValidator implements ValueValidator {
	
	private static final Collection<Integer> VALID_REPETITIONS = Arrays.asList(1, 2, 4, 5, 8, 10, 16, 20, 32, 40, 50, 64);

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        String errorMessageIdentifier = null;

        try {
            int cycleRepeatIntervalInt = TemplateUtils.getIntNotPlaceholder(value, false);
            if(!VALID_REPETITIONS.contains(cycleRepeatIntervalInt)) {
            	errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleRepeatInterval_IllegalCycleRepeatInterval";
            }
        } catch (NumberFormatException e) {
            errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_CycleRepeatInterval_NotANumber";
        }

        if(!StringUtils.nullOrEmpty(errorMessageIdentifier)) {
            if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier, value);
            }
            return false;
        }

        return true;
    }
}
