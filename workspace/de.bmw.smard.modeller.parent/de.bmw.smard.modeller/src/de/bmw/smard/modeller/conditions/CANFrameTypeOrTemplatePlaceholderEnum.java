package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getCANFrameTypeOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum CANFrameTypeOrTemplatePlaceholderEnum implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(0, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>Undefined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #UNDEFINED_VALUE
     * @generated
     * @ordered
     */
    UNDEFINED(1, "Undefined", "Undefined"),

    /**
     * The '<em><b>Standard</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #STANDARD_VALUE
     * @generated
     * @ordered
     */
    STANDARD(2, "Standard", "Standard"),

    /**
     * The '<em><b>Error Frame</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ERROR_FRAME_VALUE
     * @generated
     * @ordered
     */
    ERROR_FRAME(3, "ErrorFrame", "ErrorFrame"),

    /**
     * The '<em><b>Remote Frame</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #REMOTE_FRAME_VALUE
     * @generated
     * @ordered
     */
    REMOTE_FRAME(4, "RemoteFrame", "RemoteFrame");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = 0;

    /**
     * The '<em><b>Undefined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Undefined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #UNDEFINED
     * @model name="Undefined"
     * @generated
     * @ordered
     */
    public static final int UNDEFINED_VALUE = 1;

    /**
     * The '<em><b>Standard</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Standard</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #STANDARD
     * @model name="Standard"
     * @generated
     * @ordered
     */
    public static final int STANDARD_VALUE = 2;

    /**
     * The '<em><b>Error Frame</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Error Frame</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ERROR_FRAME
     * @model name="ErrorFrame"
     * @generated
     * @ordered
     */
    public static final int ERROR_FRAME_VALUE = 3;

    /**
     * The '<em><b>Remote Frame</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Remote Frame</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #REMOTE_FRAME
     * @model name="RemoteFrame"
     * @generated
     * @ordered
     */
    public static final int REMOTE_FRAME_VALUE = 4;

    /**
     * An array of all the '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final CANFrameTypeOrTemplatePlaceholderEnum[] VALUES_ARRAY =
            new CANFrameTypeOrTemplatePlaceholderEnum[] {
                    TEMPLATE_DEFINED,
                    UNDEFINED,
                    STANDARD,
                    ERROR_FRAME,
                    REMOTE_FRAME,
            };

    /**
     * A public read-only list of all the '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<CANFrameTypeOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static CANFrameTypeOrTemplatePlaceholderEnum get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            CANFrameTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static CANFrameTypeOrTemplatePlaceholderEnum getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            CANFrameTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>CAN Frame Type Or Template Placeholder Enum</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static CANFrameTypeOrTemplatePlaceholderEnum get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case UNDEFINED_VALUE:
                return UNDEFINED;
            case STANDARD_VALUE:
                return STANDARD;
            case ERROR_FRAME_VALUE:
                return ERROR_FRAME;
            case REMOTE_FRAME_VALUE:
                return REMOTE_FRAME;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private CANFrameTypeOrTemplatePlaceholderEnum(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //CANFrameTypeOrTemplatePlaceholderEnum
