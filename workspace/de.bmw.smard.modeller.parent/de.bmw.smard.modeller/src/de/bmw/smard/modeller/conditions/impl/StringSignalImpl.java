package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.StringSignal;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringSignalImpl extends AbstractSignalImpl implements StringSignal {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected StringSignalImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.STRING_SIGNAL;
    }

    /**
     * @generated NOT
     */
    @Override
    public DataType get_EvaluationDataType() {
        return DataType.STRING;
    }
} //StringSignalImpl
