package de.bmw.smard.modeller.util;

/**
 * Created by j.schmid on 26.01.2017.
 */
public class ModelLoaderException extends Exception {

    private String filename;

    public ModelLoaderException(String message, String filename) {
        super(message);
        this.filename = filename;
    }

    public ModelLoaderException(String message, String filename, Throwable cause) {
        super(message, cause);
        this.filename = filename;
    }
}
