package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FlexRaySlotIdValidator implements ValueValidator {

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		String errorMessageIdentifier = null;

		if(StringUtils.isBlank(value)) {
			errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_SlotId_MustNotBeNull";
			if(baseValidator != null) {
				baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
			}
			return false;
		}

		int slotId;
		try {
			if(value.startsWith("0x")) {
				slotId = Integer.parseInt(value.substring(2), 16);
			} else {
				slotId = Integer.parseInt(value);
			}
		} catch (NumberFormatException e) {
			errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_SlotId_NotANumber";
			if(baseValidator != null) {
				baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
			}
			return false;
		}

		if(slotId < 1) {
			errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_SlotId_IsLowerOne";
		} else if(slotId > 2047) {
			errorMessageIdentifier = "_Validation_Conditions_FlexRayFilter_SlotId_IsGreater2047";
		}

		if(errorMessageIdentifier != null) {
            if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier, value);
            }
			return false;
		}
		return true;
	}

}
