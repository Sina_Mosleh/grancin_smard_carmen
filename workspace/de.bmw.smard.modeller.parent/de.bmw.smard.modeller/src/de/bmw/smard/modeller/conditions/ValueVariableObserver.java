package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Value Variable Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getValueVariableObserver()
 * @model
 * @generated
 */
public interface ValueVariableObserver extends AbstractObserver {
} // ValueVariableObserver
