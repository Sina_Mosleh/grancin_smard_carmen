package de.bmw.smard.modeller.conditions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Verbose DLT Extract Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getVerboseDLTExtractStrategy()
 * @model
 * @generated
 */
public interface VerboseDLTExtractStrategy extends ExtractStrategy {
} // VerboseDLTExtractStrategy
