package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.DiagnosticChain;

import java.util.Map;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Signal Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SignalReference#getSignal <em>Signal</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalReference()
 * @model
 * @generated
 */
public interface SignalReference extends ComparatorSignal, ISignalOrReference, INumericOperand, IStringOperand {
    /**
     * Returns the value of the '<em><b>Signal</b></em>' reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Signal</em>' reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Signal</em>' reference.
     * @see #setSignal(ISignalOrReference)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSignalReference_Signal()
     * @model required="true"
     * @generated
     */
    ISignalOrReference getSignal();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SignalReference#getSignal <em>Signal</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Signal</em>' reference.
     * @see #getSignal()
     * @generated
     */
    void setSignal(ISignalOrReference value);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    boolean isValid_hasValidSignal(DiagnosticChain diagnostics, Map<Object, Object> context);

} // SignalReference
