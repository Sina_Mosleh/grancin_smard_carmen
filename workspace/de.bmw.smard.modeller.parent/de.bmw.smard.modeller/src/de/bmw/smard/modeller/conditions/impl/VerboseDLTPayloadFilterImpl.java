package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.VerboseDLTPayloadFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import java.util.Collection;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Verbose DLT Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl#getRegex <em>Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.VerboseDLTPayloadFilterImpl#getContainsAny <em>Contains Any</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VerboseDLTPayloadFilterImpl extends AbstractFilterImpl implements VerboseDLTPayloadFilter {
    /**
     * The default value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected static final String REGEX_EDEFAULT = "";

    /**
     * The cached value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected String regex = REGEX_EDEFAULT;

    /**
     * The cached value of the '{@link #getContainsAny() <em>Contains Any</em>}' attribute list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getContainsAny()
     * @generated
     * @ordered
     */
    protected EList<String> containsAny;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected VerboseDLTPayloadFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.VERBOSE_DLT_PAYLOAD_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getRegex() {
        return regex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setRegex(String newRegex) {
        String oldRegex = regex;
        regex = newRegex;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__REGEX, oldRegex, regex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<String> getContainsAny() {
        if (containsAny == null) {
            containsAny = new EDataTypeUniqueEList<String>(String.class, this, ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY);
        }
        return containsAny;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.VERBOSE_DLT_PAYLOAD_FILTER__IS_VALID_HAS_VALID_REGEX)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(StringUtils.isBlank(regex))
                        .error("_Validation_Conditions_VerboseDLTPayloadFilter_Regex_IsNull")
                        .build())

                .with(Validators.stringTemplate(regex)
                        .containsParameterError("_Validation_Conditions_VerboseDLTPayloadFilter_Regex_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_VerboseDLTPayloadFilter_Regex_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.validRegexValue("_Validation_Conditions_VerboseDLTPayloadFilter_Regex_IsInvalid"))
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__REGEX:
                return getRegex();
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY:
                return getContainsAny();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__REGEX:
                setRegex((String) newValue);
                return;
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY:
                getContainsAny().clear();
                getContainsAny().addAll((Collection<? extends String>) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__REGEX:
                setRegex(REGEX_EDEFAULT);
                return;
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY:
                getContainsAny().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__REGEX:
                return REGEX_EDEFAULT == null ? regex != null : !REGEX_EDEFAULT.equals(regex);
            case ConditionsPackage.VERBOSE_DLT_PAYLOAD_FILTER__CONTAINS_ANY:
                return containsAny != null && !containsAny.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (regex: ");
        result.append(regex);
        result.append(", containsAny: ");
        result.append(containsAny);
        result.append(')');
        return result.toString();
    }

} //VerboseDLTPayloadFilterImpl
