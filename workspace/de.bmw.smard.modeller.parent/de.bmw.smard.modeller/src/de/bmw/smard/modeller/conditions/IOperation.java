package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IOperation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getIOperation()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IOperation extends IOperand, ISignalComparisonExpressionOperand {
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model
     * @generated
     */
    DataType get_OperandDataType();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model many="false"
     * @generated
     */
    EList<IOperand> get_Operands();

} // IOperation
