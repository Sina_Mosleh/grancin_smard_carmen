package de.bmw.smard.modeller.statemachine.util;

import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.statemachine.DocumentRoot;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.BasicResourceHandler;
import org.eclipse.emf.ecore.xml.type.AnyType;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Map;

public class StatemachineResourceHandler extends BasicResourceHandler {

    @Override
    public void postLoad(XMLResource resource, InputStream inputStream,
            Map<?, ?> options) {
        super.postLoad(resource, inputStream, options);

        for (Map.Entry<EObject, AnyType> entry : resource.getEObjectToExtensionMap().entrySet()) {
            System.out.println(entry.getKey().getClass().getSimpleName() + ": " + entry.getKey().toString());
        }
        printResource(resource);


    }

    @Override
    public void preLoad(XMLResource resource, InputStream inputStream,
            Map<?, ?> options) {
        super.preLoad(resource, inputStream, options);
    }


    private void printResource(XMLResource resource) {
        if (resource.getContents().size() != 1) {
            String msg = MessageFormat.format("Contents of {0} does not have the one expected element but {1}",
                    resource.getURI(),
                    resource.getContents().size());
            PluginActivator.logMessage(IStatus.ERROR, msg);
            throw new RuntimeException(msg);
        }

        EObject root = resource.getContents().get(0);
        if (!(root instanceof DocumentRoot)) {
            String msg = MessageFormat.format("Contents of {0} is not a Conditions-DocumentRoot but {1}",
                    resource.getURI(), root.getClass());
            PluginActivator.logMessage(IStatus.ERROR, msg);
            throw new RuntimeException(msg);
        }
        DocumentRoot documentRoot = (DocumentRoot) root;
        if (documentRoot.getStateMachine() != null) {
            System.out.println(documentRoot.getStateMachine());
        }
    }

}
