package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getEmptyDecodeStrategy()
 * @model
 * @generated
 */
public interface EmptyDecodeStrategy extends DecodeStrategy {
} // EmptyDecodeStrategy
