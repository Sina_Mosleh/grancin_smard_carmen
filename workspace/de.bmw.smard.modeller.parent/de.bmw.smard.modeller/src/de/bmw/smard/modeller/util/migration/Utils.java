package de.bmw.smard.modeller.util.migration;

import org.eclipse.emf.edapt.spi.migration.Instance;

public class Utils {
	
	/** 
	 * will turn a strings first letter from uppercase to lowercase
	 * sample: UPPERCASE -> uPPERCASE 
	 */
	public static String decapitalize(String string) {
	    if (string == null || string.length() == 0) {
	        return string;
	    }
	    char c[] = string.toCharArray();
	    c[0] = Character.toLowerCase(c[0]);
	    return new String(c);
	}
	
	/** 
	 * 	using this avoids setting empty values -> eases debugging a bit 
	 */
	public static void setIfNotNull(Instance source, String attribNameSource, Instance target, String attribNameTarget) {
		if ( (source != null) && (target != null) && (attribNameSource != null) && !attribNameSource.isEmpty() && (attribNameTarget != null) && !attribNameTarget.isEmpty() &&
			(source.get(attribNameSource) instanceof String) && !((String)source.get(attribNameSource)).isEmpty() )  {
				target.set(attribNameTarget, source.get(attribNameSource));
			}
	}

}
