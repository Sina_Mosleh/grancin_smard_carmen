package de.bmw.smard.modeller.util;

import de.bmw.smard.common.data.ShowVariableActionExpressionParser;
import de.bmw.smard.modeller.conditions.AbstractObserver;
import de.bmw.smard.modeller.statemachine.*;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.ecore.resource.Resource;

import java.util.Collection;
import java.util.Map;

public class SmardTraceElementIdentifierHelper {
    private static final String PROJECT_DEFAULT = "project";
	private static final String STATEMACHINE_DEFAULT = "statemachine";
	private static final String VARNAME_DEFAULT = "SHOWVARIABLE_variableName_NotFound";
	private static final String DOT = ".";

	// mostly used from within Modeller context
	public static String getIdentifier(SmardTraceElement smardTraceElement) {
		String projectName = getProjectName(smardTraceElement.eResource());
        return getIdentifier(smardTraceElement, projectName);
    }

	public static String getIdentifier(SmardTraceElement smardTraceElement, String projectName) {
        if(smardTraceElement instanceof Transition) {
            return createIdentifier((Transition) smardTraceElement, projectName);
        } else if(smardTraceElement instanceof AbstractObserver) {
            return createIdentifier((AbstractObserver) smardTraceElement, projectName);
        } else if(smardTraceElement instanceof Action &&
            ((Action) smardTraceElement).getActionType().equals(ActionTypeNotEmpty.SHOWVARIABLE)) {
            return createIdentifier((Action) smardTraceElement, projectName);
        }

        return "";
    }

    private static String createIdentifier(Transition transition, String projectName) {
        String statemachineName = STATEMACHINE_DEFAULT;

        Object statemachine = transition.eContainer();
        if(statemachine instanceof StateMachine) {
            statemachineName = ((StateMachine) statemachine).getName();
        }

        return projectName + DOT + statemachineName + DOT + transition.getName();
    }

    private static String createIdentifier(AbstractObserver observer, String projectName) {
        return projectName + DOT + observer.getName();
    }

    private static String createIdentifier(Action action, String projectName) {
        String statemachineName = STATEMACHINE_DEFAULT;

        Object state = action.eContainer();
        if(state instanceof State) {
            Object statemachine = ((State) state).eContainer();
            if(statemachine instanceof StateMachine) {
                statemachineName = ((StateMachine) statemachine).getName();
            }
        }

        return projectName + DOT + statemachineName + DOT +
                getVariableNameFromActionExpression(action.getActionExpression());
    }

    private static String getProjectName(Resource resource) {
        String result = PROJECT_DEFAULT;
        IProject project = EMFUtils.getEclipseProject(resource);
        if(project != null) {
        	result = project.getName();
        }
        return result;
    }

    private static String getVariableNameFromActionExpression(String actionExpression) {
        String variableName = VARNAME_DEFAULT;
        Map<String, Object> parseResult = ShowVariableActionExpressionParser.parseShowVariableParameters(actionExpression);

        if(parseResult != null && !parseResult.isEmpty()) {
            Collection<String> keys = parseResult.keySet();
            for(String key : keys) {
                if(key.equalsIgnoreCase("value")) {
                	String value = (String) parseResult.get(key);
                	if(value != null && !value.isEmpty()) {
                		variableName = (String) parseResult.get(key);
                	}
                }
            }
        }

        return variableName;
    }

}
