package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.AbstractMessage;
import de.bmw.smard.modeller.conditions.AbstractSignal;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.DecodeStrategy;
import de.bmw.smard.modeller.conditions.ExtractStrategy;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl#getName <em>Name</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl#getDecodeStrategy <em>Decode Strategy</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.AbstractSignalImpl#getExtractStrategy <em>Extract Strategy</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractSignalImpl extends BaseClassWithSourceReferenceImpl implements AbstractSignal {

    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getDecodeStrategy() <em>Decode Strategy</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDecodeStrategy()
     * @generated
     * @ordered
     */
    protected DecodeStrategy decodeStrategy;

    /**
     * The cached value of the '{@link #getExtractStrategy() <em>Extract Strategy</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getExtractStrategy()
     * @generated
     * @ordered
     */
    protected ExtractStrategy extractStrategy;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected AbstractSignalImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.ABSTRACT_SIGNAL;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_SIGNAL__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public DecodeStrategy getDecodeStrategy() {
        return decodeStrategy;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetDecodeStrategy(DecodeStrategy newDecodeStrategy, NotificationChain msgs) {
        DecodeStrategy oldDecodeStrategy = decodeStrategy;
        decodeStrategy = newDecodeStrategy;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY, oldDecodeStrategy, newDecodeStrategy);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDecodeStrategy(DecodeStrategy newDecodeStrategy) {
        if (newDecodeStrategy != decodeStrategy) {
            NotificationChain msgs = null;
            if (decodeStrategy != null)
                msgs = ((InternalEObject) decodeStrategy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY,
                        null, msgs);
            if (newDecodeStrategy != null)
                msgs = ((InternalEObject) newDecodeStrategy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY,
                        null, msgs);
            msgs = basicSetDecodeStrategy(newDecodeStrategy, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY, newDecodeStrategy, newDecodeStrategy));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ExtractStrategy getExtractStrategy() {
        return extractStrategy;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetExtractStrategy(ExtractStrategy newExtractStrategy, NotificationChain msgs) {
        ExtractStrategy oldExtractStrategy = extractStrategy;
        extractStrategy = newExtractStrategy;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY, oldExtractStrategy, newExtractStrategy);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setExtractStrategy(ExtractStrategy newExtractStrategy) {
        if (newExtractStrategy != extractStrategy) {
            NotificationChain msgs = null;
            if (extractStrategy != null)
                msgs = ((InternalEObject) extractStrategy).eInverseRemove(this, ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL, ExtractStrategy.class,
                        msgs);
            if (newExtractStrategy != null)
                msgs = ((InternalEObject) newExtractStrategy).eInverseAdd(this, ConditionsPackage.EXTRACT_STRATEGY__ABSTRACT_SIGNAL, ExtractStrategy.class,
                        msgs);
            msgs = basicSetExtractStrategy(newExtractStrategy, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY, newExtractStrategy, newExtractStrategy));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public AbstractMessage getMessage() {
        return (AbstractMessage) this.eContainer();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public EList<AbstractBusMessage> getAllReferencedBusMessages() {
        EList<AbstractBusMessage> messages = new BasicEList<AbstractBusMessage>();
        if (getMessage() != null && getMessage() instanceof AbstractBusMessage) {
            messages.add((AbstractBusMessage) getMessage());
        }
        return messages;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public DataType get_EvaluationDataType() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<String> getReadVariables() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<String> getWriteVariables() {
        // TODO: implement this method
        // Ensure that you remove @generated or mark it @generated NOT
        throw new UnsupportedOperationException();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                if (extractStrategy != null)
                    msgs = ((InternalEObject) extractStrategy).eInverseRemove(this,
                            EOPPOSITE_FEATURE_BASE - ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY, null, msgs);
                return basicSetExtractStrategy((ExtractStrategy) otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
                return basicSetDecodeStrategy(null, msgs);
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                return basicSetExtractStrategy(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__NAME:
                return getName();
            case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
                return getDecodeStrategy();
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                return getExtractStrategy();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__NAME:
                setName((String) newValue);
                return;
            case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
                setDecodeStrategy((DecodeStrategy) newValue);
                return;
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                setExtractStrategy((ExtractStrategy) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__NAME:
                setName(NAME_EDEFAULT);
                return;
            case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
                setDecodeStrategy((DecodeStrategy) null);
                return;
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                setExtractStrategy((ExtractStrategy) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ABSTRACT_SIGNAL__NAME:
                return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
            case ConditionsPackage.ABSTRACT_SIGNAL__DECODE_STRATEGY:
                return decodeStrategy != null;
            case ConditionsPackage.ABSTRACT_SIGNAL__EXTRACT_STRATEGY:
                return extractStrategy != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (name: ");
        result.append(name);
        result.append(')');
        return result.toString();
    }

} //AbstractSignalImpl
