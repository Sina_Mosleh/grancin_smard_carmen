package de.bmw.smard.modeller.validator.value;

import java.text.MessageFormat;
import java.util.*;

import de.bmw.smard.base.util.RangeIdValidator;
import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class FlexRayFrameIdRangeValidator implements ValueValidator {

    private static final List<String> VALID_CHANNEL_STRINGS = Arrays.asList("A","B","AB");

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        boolean isValid = true;

        String[] messageParameters = value.split(";");
        for (String messageParameter : messageParameters) {
            messageParameter = messageParameter.trim();
            // messageParameter ::= channels '.' slots '.' offsets '.' repetition
            String[] messageParameterSplit = messageParameter.split("\\.");

            if (messageParameterSplit.length != 4) {
                String substitution = MessageFormat.format("Flex Ray Message Ids must be given in format " +
                        "<Channel>.<SlotID>.<Offset>.<Repetition>;... - could not match '{0}'", messageParameter);
                baseValidator.attach(Severity.ERROR,
                        "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalFrameIdRange", substitution);
                return false;
            }

            String channelValidationError = checkChannelOfFlexRayFrameIdRange(messageParameterSplit);
            if(!StringUtils.nullOrEmpty(channelValidationError)) {
                baseValidator.attach(Severity.ERROR,
                        "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalChannelOfFrameIdRange",
                        channelValidationError);
                isValid = false;
            }

            String slotValidationError = checkSlotOfFlexRayFrameIdRange(messageParameterSplit, baseValidator);
            if(!StringUtils.nullOrEmpty(slotValidationError)) {
                baseValidator.attach(Severity.ERROR,
                        "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalSlotIdOfFrameIdRange",
                        slotValidationError);
                isValid = false;
            }

            // repetition must be checked first, cause a valid repetition is needed for offset validation
            String repetitionValidationError = checkRepetitionOfFlexRayFrameIdRange(messageParameterSplit);
            if(!StringUtils.nullOrEmpty(repetitionValidationError)) {
                baseValidator.attach(Severity.ERROR,
                        "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalRepetitionFrameIdRange",
                        repetitionValidationError);
                return false;
            }

            String offsetValidationError = checkOffsetOfFlexRayFrameIdRange(messageParameterSplit, baseValidator);
            if(!StringUtils.nullOrEmpty(offsetValidationError)) {
                baseValidator.attach(Severity.ERROR,
                        "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalOffsetOfFrameIdRange",
                        offsetValidationError);
                isValid = false;
            }
        }

        return isValid;
    }

    private String checkChannelOfFlexRayFrameIdRange(String[] messageParameterSplit) {
        // channels ::= 'A' | 'B' | 'AB' | '[A,B]' | '[A,AB]' | '[B,AB]' | '[A,B,AB]'
        // (we're more tolerant in the parser regarding order and repetition)
        String channelsString = messageParameterSplit[0].trim();
        if (isRange(channelsString)) {
            String rangeContentString = channelsString.substring(1, channelsString.length() - 1);
            for (String channelString : rangeContentString.split("[,]")) {
                channelString = channelString.trim();
                if (!VALID_CHANNEL_STRINGS.contains(channelString)) {
                    return MessageFormat.format("Channel identifier in range must be A, B, or AB, not ''{0}''",
                            channelString);
                }
            }
        } else {
            if (!VALID_CHANNEL_STRINGS.contains(channelsString)) {
                return MessageFormat.format("Channel identifier must be A, B, AB, or a range like [A,B,AB], not ''{0}''",
                        channelsString);
            }
        }

        return "";
    }

    private String checkSlotOfFlexRayFrameIdRange(String[] messageParameterSplit, BaseValidator baseValidator) {
        String slotsString = messageParameterSplit[1].trim();
        if (isRange(slotsString)) {
            String slotsContentString = slotsString.substring(1, slotsString.length() - 1);
            String errorMessage = RangeIdValidator.validate(slotsContentString,true);
            if (!StringUtils.nullOrEmpty(errorMessage)) {
                return "Slot Id is invalid: " + errorMessage;
            }
        } else {
            FlexRaySlotIdValidator slotIdValidator = new FlexRaySlotIdValidator();
            if(!slotIdValidator.validate(slotsString, baseValidator)) {
                return MessageFormat.format("Slot Id is invalid {0}", slotsString);
            }
        }

        return "";
    }

    private String checkOffsetOfFlexRayFrameIdRange(String[] messageParameterSplit, BaseValidator baseValidator) {
        String offsetsString = messageParameterSplit[2].trim();
        String repetitionsString = messageParameterSplit[3].trim();
        FlexRayCycleValidator cycleValidator = new FlexRayCycleValidator(repetitionsString);

        if (isRange(offsetsString)) {
            String offsetsContentString = offsetsString.substring(1, offsetsString.length() - 1);
            String errorMessage = RangeIdValidator.validate(offsetsContentString,false);
            if (!StringUtils.nullOrEmpty(errorMessage)) {
                return "Offset is invalid: " + errorMessage;
            } else {
                // validate every value in range with FlexRayCycleValidator
                errorMessage = validateRangeValueInOffset(baseValidator, offsetsString, cycleValidator, offsetsContentString);
                if (!StringUtils.nullOrEmpty(errorMessage))
                    return errorMessage;
            }
        } else {
            if(!cycleValidator.validate(offsetsString, baseValidator)) {
                return MessageFormat.format("Offset is invalid {0}", offsetsString);
            }
        }

        return "";
    }

    private String validateRangeValueInOffset(BaseValidator baseValidator, String offsetsString, FlexRayCycleValidator cycleValidator, String offsetsContentString) {
        String invalidOffsetPattern = "Invalid offset {0} in range {1}";
        List<String> allValuesInOffsetRange = new ArrayList<>();

        // collect all separate values in range, and if it's a range-pattern, add the min- and maxValue to allValuesInOffsetRange
        List<String> commaSeparatedOffsets = Arrays.asList(offsetsContentString.split(","));
        for(String commaSeparatedOffset : commaSeparatedOffsets) {
            // range pattern in range
            if(commaSeparatedOffset.contains("-")) {
                List<String> rangeOffset = Arrays.asList(commaSeparatedOffset.trim().split("-"));
                if (rangeOffset.size() != 2) {
                    return MessageFormat.format(invalidOffsetPattern, commaSeparatedOffset, offsetsString);
                }
                allValuesInOffsetRange.addAll(rangeOffset);
            // single value in range
            } else {
                allValuesInOffsetRange.add(commaSeparatedOffset);
            }
        }

        // validate all separate values that were found
        for(String valueInRange : allValuesInOffsetRange) {
            if(!cycleValidator.validate(valueInRange, baseValidator)) {
                return MessageFormat.format(invalidOffsetPattern, valueInRange, offsetsString);
            }
        }

        return null;
    }

    private String checkRepetitionOfFlexRayFrameIdRange(String[] messageParameterSplit) {
        String repetitionString = messageParameterSplit[3].trim();
        FlexRayRepetitionValidator repetitionValidator = new FlexRayRepetitionValidator();
        if(!repetitionValidator.validate(repetitionString, null)) {
            return MessageFormat.format("Repetition is invalid {0}", repetitionString);
        }

        return "";
    }

    private boolean isRange(String parameter) {
         return parameter.startsWith("[") && parameter.endsWith("]");
    }
}