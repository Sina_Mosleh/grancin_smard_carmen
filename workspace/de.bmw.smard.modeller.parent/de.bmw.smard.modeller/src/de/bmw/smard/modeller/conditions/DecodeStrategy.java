package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decode Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getDecodeStrategy()
 * @model abstract="true"
 * @generated
 */
public interface DecodeStrategy extends EObject {
} // DecodeStrategy
