package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Class With ID</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.BaseClassWithID#getId <em>Id</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getBaseClassWithID()
 * @model abstract="true"
 * @generated
 */
public interface BaseClassWithID extends EObject {
    /**
     * Returns the value of the '<em><b>Id</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Id</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Id</em>' attribute.
     * @see #setId(String)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getBaseClassWithID_Id()
     * @model id="true" required="true"
     *        extendedMetaData="kind='element'"
     * @generated
     */
    String getId();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.BaseClassWithID#getId <em>Id</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Id</em>' attribute.
     * @see #getId()
     * @generated
     */
    void setId(String value);

} // BaseClassWithID
