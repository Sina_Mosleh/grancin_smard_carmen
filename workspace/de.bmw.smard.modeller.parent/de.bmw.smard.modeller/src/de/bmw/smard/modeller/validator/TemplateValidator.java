package de.bmw.smard.modeller.validator;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;

public class TemplateValidator {

    public interface ContainsParameterErrorStep<T> {
        InvalidPlaceholderErrorStep<T> containsParameterError(String errorId);
    }

    public interface InvalidPlaceholderErrorStep<T> {
        T invalidPlaceholderError(String errorId);
    }

    public abstract static class Builder<T> extends Validator implements
            ContainsParameterErrorStep,
            InvalidPlaceholderErrorStep {

        private String value;
        private String containsParameterErrorId;
        private String invalidPlaceholderErrorId;

        public String getValue() {
            return value;
        }

        Builder(String value) {
            this.value = value;
        }

        @Override
        public InvalidPlaceholderErrorStep<T> containsParameterError(String errorId) {
            this.containsParameterErrorId = errorId;
            return this;
        }

        @Override
        public T invalidPlaceholderError(String errorId) {
            this.invalidPlaceholderErrorId = errorId;
            return (T)this;
        }

        @Override
        protected boolean validate(BaseValidator baseValidator) {
            boolean isValid = true;

            if (StringUtils.isNotBlank(value)) {
                if (baseValidator.isExportable()) {
                    if (TemplateUtils.containsParameters(value)) {
                        attachError(baseValidator, containsParameterErrorId);
                        isValid = false;
                    } else {
                        isValid = validateValue(baseValidator, true);
                    }
                } else {
                    if (TemplateUtils.containsParameters(value)) {
                        String substitution = TemplateUtils.checkValidPlaceholderNameWithHashtags(value);
                        if (!StringUtils.nullOrEmpty(substitution)) {
                            attachError(baseValidator, invalidPlaceholderErrorId, substitution);
                            isValid = false;
                        } else {
                            // if we have a valid parameter we can not check the value
                            //isValid = validateValue(baseValidator, false);
                        }
                    } else {
                        isValid = validateValue(baseValidator, false);
                    }
                }
            }

            return isValid;
        }

        protected abstract boolean validateValue(BaseValidator diagnostics, boolean exportable);
    }
}
