package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IPSD Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDMessage()
 * @model
 * @generated
 */
public interface SomeIPSDMessage extends AbstractBusMessage {

    /**
     * Returns the value of the '<em><b>Some IPSD Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Some IPSD Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Some IPSD Filter</em>' containment reference.
     * @see #setSomeIPSDFilter(SomeIPSDFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPSDMessage_SomeIPSDFilter()
     * @model containment="true" required="true"
     * @generated
     */
    SomeIPSDFilter getSomeIPSDFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPSDMessage#getSomeIPSDFilter <em>Some IPSD Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Some IPSD Filter</em>' containment reference.
     * @see #getSomeIPSDFilter()
     * @generated
     */
    void setSomeIPSDFilter(SomeIPSDFilter value);
} // SomeIPSDMessage
