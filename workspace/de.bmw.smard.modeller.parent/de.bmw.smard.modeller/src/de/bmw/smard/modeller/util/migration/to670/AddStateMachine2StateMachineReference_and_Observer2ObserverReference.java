package de.bmw.smard.modeller.util.migration.to670;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;


public class AddStateMachine2StateMachineReference_and_Observer2ObserverReference extends CustomMigration {

	private EAttribute statemachinePath;
	private EReference statemachine;
	
	private EAttribute observerReferenceName;
	private EAttribute abstractObserverName;
	
	private EReference observer;
	private Map<String,Instance> name2Observer = new HashMap<String,Instance>();
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel)
			throws MigrationException {
		
		statemachinePath = metamodel.getEAttribute("statemachineset.StateMachineReference.path");
		
		observerReferenceName = metamodel.getEAttribute("statemachineset.ObserverReference.name");
		abstractObserverName = metamodel.getEAttribute("conditions.SignalObserver.name");
		
		for (Instance observer : model.getAllInstances("conditions.AbstractObserver")) {
			name2Observer.put((String) observer.get(abstractObserverName), observer);			
		}
	}
	
	@Override
	public void migrateAfter(Model model, Metamodel metamodel)
			throws MigrationException {	
		
		Set<Instance> trashBucket4Orphans = new HashSet<Instance>();
		
		// 									syntax: packageShortName . TypeName . attributeName
		statemachine = metamodel.getEReference("statemachineset.StateMachineReference.stateMachine");
	
		boolean hasFound = false;
		for (Instance stateMachineReference : model.getAllInstances("statemachineset.StateMachineReference")) {
			hasFound = false;
			for (Instance sm : model.getAllInstances("statemachine.StateMachine")) {
				if (sm.getResource().getUri().toString().endsWith( stateMachineReference.get(statemachinePath).toString() ) ) {
					stateMachineReference.set(statemachine, sm);
					hasFound = true;
					break;
				}
			}
			if (!hasFound) {
				trashBucket4Orphans.add(stateMachineReference);
			}
		}
	
		
		observer = metamodel.getEReference("statemachineset.ObserverReference.observer");
		for(Instance obsRef : model.getAllInstances("statemachineset.ObserverReference")) {
			Instance obs = name2Observer.get( (String) obsRef.get(observerReferenceName) );
			if (null != obs) {
				obsRef.set(observer, obs);
			} else {
				trashBucket4Orphans.add(obsRef);
			}			
		}
		
		// finally delete orphans from model
		for (Instance instance : trashBucket4Orphans) {
			model.delete(instance);		
		}	
	}
	
}
