package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.modeller.validator.BaseValidator;

public interface ValueValidator {

    boolean validate(String value, BaseValidator baseValidator);
}
