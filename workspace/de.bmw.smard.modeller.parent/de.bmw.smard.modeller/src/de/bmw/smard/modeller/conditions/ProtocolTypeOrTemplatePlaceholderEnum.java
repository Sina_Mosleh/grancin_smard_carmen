package de.bmw.smard.modeller.conditions;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Protocol Type Or Template Placeholder Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getProtocolTypeOrTemplatePlaceholderEnum()
 * @model
 * @generated
 */
public enum ProtocolTypeOrTemplatePlaceholderEnum implements Enumerator {
    /**
     * The '<em><b>Template Defined</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATE_DEFINED(0, "TemplateDefined", "TemplateDefined"),

    /**
     * The '<em><b>UDP</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #UDP_VALUE
     * @generated
     * @ordered
     */
    UDP(1, "UDP", "UDP"),

    /**
     * The '<em><b>TCP</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TCP_VALUE
     * @generated
     * @ordered
     */
    TCP(2, "TCP", "TCP"),

    /**
     * The '<em><b>ICMP</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ICMP_VALUE
     * @generated
     * @ordered
     */
    ICMP(3, "ICMP", "ICMP"),

    /**
     * The '<em><b>ALL</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ALL_VALUE
     * @generated
     * @ordered
     */
    ALL(4, "ALL", "ALL");

    /**
     * The '<em><b>Template Defined</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>Template Defined</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATE_DEFINED
     * @model name="TemplateDefined"
     * @generated
     * @ordered
     */
    public static final int TEMPLATE_DEFINED_VALUE = 0;

    /**
     * The '<em><b>UDP</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>UDP</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #UDP
     * @model
     * @generated
     * @ordered
     */
    public static final int UDP_VALUE = 1;

    /**
     * The '<em><b>TCP</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>TCP</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #TCP
     * @model
     * @generated
     * @ordered
     */
    public static final int TCP_VALUE = 2;

    /**
     * The '<em><b>ICMP</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ICMP</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ICMP
     * @model
     * @generated
     * @ordered
     */
    public static final int ICMP_VALUE = 3;

    /**
     * The '<em><b>ALL</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>ALL</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ALL
     * @model
     * @generated
     * @ordered
     */
    public static final int ALL_VALUE = 4;

    /**
     * An array of all the '<em><b>Protocol Type Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final ProtocolTypeOrTemplatePlaceholderEnum[] VALUES_ARRAY =
            new ProtocolTypeOrTemplatePlaceholderEnum[] {
                    TEMPLATE_DEFINED,
                    UDP,
                    TCP,
                    ICMP,
                    ALL,
            };

    /**
     * A public read-only list of all the '<em><b>Protocol Type Or Template Placeholder Enum</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<ProtocolTypeOrTemplatePlaceholderEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Protocol Type Or Template Placeholder Enum</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ProtocolTypeOrTemplatePlaceholderEnum get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ProtocolTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Protocol Type Or Template Placeholder Enum</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ProtocolTypeOrTemplatePlaceholderEnum getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            ProtocolTypeOrTemplatePlaceholderEnum result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Protocol Type Or Template Placeholder Enum</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static ProtocolTypeOrTemplatePlaceholderEnum get(int value) {
        switch (value) {
            case TEMPLATE_DEFINED_VALUE:
                return TEMPLATE_DEFINED;
            case UDP_VALUE:
                return UDP;
            case TCP_VALUE:
                return TCP;
            case ICMP_VALUE:
                return ICMP;
            case ALL_VALUE:
                return ALL;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private ProtocolTypeOrTemplatePlaceholderEnum(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //ProtocolTypeOrTemplatePlaceholderEnum
