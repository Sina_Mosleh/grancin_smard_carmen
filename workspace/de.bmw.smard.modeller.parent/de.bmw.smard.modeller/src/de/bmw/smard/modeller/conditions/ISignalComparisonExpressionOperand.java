package de.bmw.smard.modeller.conditions;

import de.bmw.smard.modeller.statemachineset.BusMessageReferable;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ISignal Comparison Expression Operand</b></em>'.
 * <!-- end-user-doc -->
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getISignalComparisonExpressionOperand()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISignalComparisonExpressionOperand extends IOperand, BusMessageReferable {
} // ISignalComparisonExpressionOperand
