package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.PluginActivator;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.IPv4Filter;
import de.bmw.smard.modeller.conditions.ProtocolTypeOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.util.validation.DiagnosticBuilder;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPv4 Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl#getProtocolType <em>Protocol Type</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl#getProtocolTypeTmplParam <em>Protocol Type Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl#getSourceIP <em>Source IP</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl#getDestIP <em>Dest IP</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.IPv4FilterImpl#getTimeToLive <em>Time To Live</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IPv4FilterImpl extends AbstractFilterImpl implements IPv4Filter {
    /**
     * The default value of the '{@link #getProtocolType() <em>Protocol Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolType()
     * @generated
     * @ordered
     */
    protected static final ProtocolTypeOrTemplatePlaceholderEnum PROTOCOL_TYPE_EDEFAULT = ProtocolTypeOrTemplatePlaceholderEnum.ALL;
    /**
     * The cached value of the '{@link #getProtocolType() <em>Protocol Type</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolType()
     * @generated
     * @ordered
     */
    protected ProtocolTypeOrTemplatePlaceholderEnum protocolType = PROTOCOL_TYPE_EDEFAULT;
    /**
     * The default value of the '{@link #getProtocolTypeTmplParam() <em>Protocol Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolTypeTmplParam()
     * @generated
     * @ordered
     */
    protected static final String PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getProtocolTypeTmplParam() <em>Protocol Type Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getProtocolTypeTmplParam()
     * @generated
     * @ordered
     */
    protected String protocolTypeTmplParam = PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT;
    /**
     * The default value of the '{@link #getSourceIP() <em>Source IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSourceIP()
     * @generated
     * @ordered
     */
    protected static final String SOURCE_IP_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getSourceIP() <em>Source IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSourceIP()
     * @generated
     * @ordered
     */
    protected String sourceIP = SOURCE_IP_EDEFAULT;
    /**
     * The default value of the '{@link #getDestIP() <em>Dest IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDestIP()
     * @generated
     * @ordered
     */
    protected static final String DEST_IP_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getDestIP() <em>Dest IP</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDestIP()
     * @generated
     * @ordered
     */
    protected String destIP = DEST_IP_EDEFAULT;
    /**
     * The default value of the '{@link #getTimeToLive() <em>Time To Live</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTimeToLive()
     * @generated
     * @ordered
     */
    protected static final String TIME_TO_LIVE_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getTimeToLive() <em>Time To Live</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTimeToLive()
     * @generated
     * @ordered
     */
    protected String timeToLive = TIME_TO_LIVE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected IPv4FilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.IPV4_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ProtocolTypeOrTemplatePlaceholderEnum getProtocolType() {
        return protocolType;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setProtocolType(ProtocolTypeOrTemplatePlaceholderEnum newProtocolType) {
        ProtocolTypeOrTemplatePlaceholderEnum oldProtocolType = protocolType;
        protocolType = newProtocolType == null ? PROTOCOL_TYPE_EDEFAULT : newProtocolType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE, oldProtocolType, protocolType));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getProtocolTypeTmplParam() {
        return protocolTypeTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setProtocolTypeTmplParam(String newProtocolTypeTmplParam) {
        String oldProtocolTypeTmplParam = protocolTypeTmplParam;
        protocolTypeTmplParam = newProtocolTypeTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM, oldProtocolTypeTmplParam, protocolTypeTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSourceIP() {
        return sourceIP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSourceIP(String newSourceIP) {
        String oldSourceIP = sourceIP;
        sourceIP = newSourceIP;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__SOURCE_IP, oldSourceIP, sourceIP));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDestIP() {
        return destIP;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDestIP(String newDestIP) {
        String oldDestIP = destIP;
        destIP = newDestIP;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__DEST_IP, oldDestIP, destIP));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTimeToLive() {
        return timeToLive;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTimeToLive(String newTimeToLive) {
        String oldTimeToLive = timeToLive;
        timeToLive = newTimeToLive;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE, oldTimeToLive, timeToLive));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSourceIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateIpAddress(
                sourceIP,
                "Source IP",
                "_Validation_Conditions_IPv4Filter_SourceIp_IllegalSourceIp",
                "_Validation_Conditions_IPv4Filter_SourceIp_WARNING",
                ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_SOURCE_IP_ADDRESS,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDestinationIpAddress(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return validateIpAddress(
                destIP,
                "Dest IP",
                "_Validation_Conditions_IPv4Filter_DestinationIp_IllegalDestinationIp",
                "_Validation_Conditions_IPv4Filter_DestinationIp_WARNING",
                ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_DESTINATION_IP_ADDRESS,
                diagnostics);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    private boolean validateIpAddress(String ipAddress, String type, String errorMessageId, String warningMessageId, int code, DiagnosticChain diagnostics) {
        boolean isValid = true;
        String errorMessageToSubstitute = null;
        String errorMessageIdentifier = "_Validation_GenericErrorMessage";
        String warningMessageIdentifier = null;
        String warningMessageToSubstitute = null;
        List<String> errorMessages = new ArrayList<String>();
        List<String> warningMessages = new ArrayList<String>();

        boolean templateAllowed = TemplateUtils.getTemplateCategorization(this).allowsTemplateParameters();
        getValidatedIP(ipAddress, type, templateAllowed, errorMessages, warningMessages);
        if (errorMessages != null && errorMessages.size() > 0) {
            errorMessageIdentifier = errorMessageId;
            errorMessageToSubstitute = StringUtils.join(errorMessages, ";");
            isValid = false;
        }
        if (warningMessages != null && warningMessages.size() > 0) {
            warningMessageIdentifier = warningMessageId;
            warningMessageToSubstitute = StringUtils.join(warningMessages, ";");
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.warn()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(code)
                        .messageId(PluginActivator.INSTANCE, warningMessageIdentifier, warningMessageToSubstitute)
                        .data(this)
                        .build());
            }
        }
        if (!isValid) {
            if (diagnostics != null) {
                diagnostics.add(DiagnosticBuilder.error()
                        .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                        .code(code)
                        .messageId(PluginActivator.INSTANCE, errorMessageIdentifier, errorMessageToSubstitute)
                        .data(this)
                        .build());
            }
            return isValid;
        }
        return isValid;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidTimeToLive(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.IPV4_FILTER__IS_VALID_HAS_VALID_TIME_TO_LIVE)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(timeToLive)
                        .name(ConditionsPackage.Literals.IPV4_FILTER__TIME_TO_LIVE.getName())
                        .warning("_Validation_Conditions_IPv4Filter_TimeToLive_WARNING")
                        .error("_Validation_Conditions_IPv4Filter_TimeToLive_IllegalTimeToLive")
                        .min(0)
                        .max(255)
                        .build())

                .validate();
    }

    /**
     * @generated NOT
     */
    private static String getValidatedIP(String ipPatternString, String type, boolean templateAllowed,
            List<String> errorMessages, List<String> warningMessages) {
        /*
         * ipParameters ::= ipParameter [ { ipParameterSeparator } ipParameters ] ipParameterSeparator ::= ',' | ';' |
         * '/' ipParameter ::= ipValPattern '.' ipValPattern '.' ipValPattern '.' ipValPattern ipValPattern ::= ipVal |
         * '*' | '[' ipRange ']' ipRange ::= ipVal | ipVal { ipRangeSeparator } ipVal ipRangeSeparator ::= '-' | '..'
         * ipVal ::= <ip address value as int from 0 to 255>
         */
        String res = "";

        // Value is optional
        if (ipPatternString != null && ipPatternString.trim().length() != 0) {
            if (!templateAllowed) {
                // ... and must not contain template placeholders
                if (TemplateUtils.containsParameters(ipPatternString)) {
                    errorMessages.add(type + " address must not contain template placeholders.");
                }
            } else {
                return ipPatternString;
            }


            // ipParameters ::= ipParameter [ { ipParameterSeparator } ipParameters ]
            String[] ipParameters = ipPatternString.split("[,;/]");
            for (String ipParameter : ipParameters) {
                ipParameter = ipParameter.trim();

                // ipParameter ::= ipValPattern '.' ipValPattern '.' ipValPattern '.' ipValPattern
                String[] ipValPattern = ipParameter.split("\\.", 4);
                if (ipValPattern.length != 4) {
                    errorMessages.add(type + " address must have 4 parts splitted by a '.', not '" + ipParameter
                            + "'.");
                } else {
                    checkIPValPattern(ipValPattern[0], type, "1", errorMessages, warningMessages);
                    checkIPValPattern(ipValPattern[1], type, "2", errorMessages, warningMessages);
                    checkIPValPattern(ipValPattern[2], type, "3", errorMessages, warningMessages);
                    checkIPValPattern(ipValPattern[3], type, "4", errorMessages, warningMessages);
                }
            }
            res = ipPatternString;
        }
        return res;
    }

    /**
     * checks a pattern for a part of the IPaddress
     * 
     * @param ipPartPattern
     * @generated NOT
     */
    private static void checkIPValPattern(String ipPartPattern, String type, String number, List<String> errorMessages,
            List<String> warningMessages) {
        /*
         * ipValPattern ::= ipVal | '*' | '[' ipRange ']' ipRange ::= ipVal | ipVal '-' ipVal ipVal ::= &lt;ip
         * address value as int from 0 to 255&gt;
         */

        // Value is mandatory
        if (ipPartPattern == null || ipPartPattern.isEmpty()) {
            errorMessages.add(type + " address part " + number + " must not be null.");
        } else {
            ipPartPattern = ipPartPattern.trim();

            boolean isRange = false;


            // '*'
            if (ipPartPattern.equals("*")) {
                // ok
                isRange = true;
            }
            // range value
            else if (ipPartPattern.startsWith("[") && ipPartPattern.endsWith("]")) {
                ipPartPattern = ipPartPattern.trim();
                ipPartPattern = ipPartPattern.substring(1, ipPartPattern.length() - 1);
                String[] ipPartPatternParts = ipPartPattern.split("-");
                int[] ipPartRangeValues = new int[ipPartPatternParts.length];
                for (int i = 0; i < ipPartPatternParts.length; i++) {
                    String ipPartRangeString = ipPartPatternParts[i].trim();
                    try {
                        ipPartRangeValues[i] = Integer.parseInt(ipPartRangeString);

                        if (ipPartRangeValues[i] < 0 || ipPartRangeValues[i] > 256) {
                            errorMessages.add(type + " address part range " + number + " must be in range 0..255, not '"
                                    + ipPartRangeString + "'.");
                        }
                    } catch (NumberFormatException e) {
                        errorMessages.add(type + " address part range " + number + " value must be an integer, not '"
                                + ipPartRangeString + "'.");
                    }

                }

                if (ipPartRangeValues.length == 1) {
                    // single value ok
                } else if (ipPartRangeValues.length == 2) {
                    isRange = true;

                    // range value
                    if (ipPartRangeValues[1] < ipPartRangeValues[0]) {
                        errorMessages.add(type
                                + " address part range " + number + " must have first value less or equal to second value: '"
                                + ipPartPattern + "'.");
                    }
                } else {
                    // something else
                    errorMessages.add(type + " address part " + number + " must be a single value, '*', or a range, not '"
                            + ipPartPattern + "'.");
                }

            }
            // single value
            else {
                int ipPartRangeValue;
                try {
                    ipPartRangeValue = Integer.parseInt(ipPartPattern);

                    if (ipPartRangeValue < 0 || ipPartRangeValue > 256) {
                        errorMessages.add(type + " address part " + number + " must be in range 0..255, not: '" + ipPartPattern
                                + "'.");
                    }
                } catch (NumberFormatException e) {
                    errorMessages.add(type + " address part " + number + " must be an integer, not '" + ipPartPattern + "'.");
                }
            }

            if (isRange == true) {
                warningMessages.add(type
                        + " address part " + number + " contains a range value. The evaluation process in the engine can therefore take more time.");
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
                return getProtocolType();
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
                return getProtocolTypeTmplParam();
            case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
                return getSourceIP();
            case ConditionsPackage.IPV4_FILTER__DEST_IP:
                return getDestIP();
            case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
                return getTimeToLive();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
                setProtocolType((ProtocolTypeOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
                setProtocolTypeTmplParam((String) newValue);
                return;
            case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
                setSourceIP((String) newValue);
                return;
            case ConditionsPackage.IPV4_FILTER__DEST_IP:
                setDestIP((String) newValue);
                return;
            case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
                setTimeToLive((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
                setProtocolType(PROTOCOL_TYPE_EDEFAULT);
                return;
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
                setProtocolTypeTmplParam(PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
                setSourceIP(SOURCE_IP_EDEFAULT);
                return;
            case ConditionsPackage.IPV4_FILTER__DEST_IP:
                setDestIP(DEST_IP_EDEFAULT);
                return;
            case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
                setTimeToLive(TIME_TO_LIVE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE:
                return protocolType != PROTOCOL_TYPE_EDEFAULT;
            case ConditionsPackage.IPV4_FILTER__PROTOCOL_TYPE_TMPL_PARAM:
                return PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT == null ? protocolTypeTmplParam != null : !PROTOCOL_TYPE_TMPL_PARAM_EDEFAULT
                        .equals(protocolTypeTmplParam);
            case ConditionsPackage.IPV4_FILTER__SOURCE_IP:
                return SOURCE_IP_EDEFAULT == null ? sourceIP != null : !SOURCE_IP_EDEFAULT.equals(sourceIP);
            case ConditionsPackage.IPV4_FILTER__DEST_IP:
                return DEST_IP_EDEFAULT == null ? destIP != null : !DEST_IP_EDEFAULT.equals(destIP);
            case ConditionsPackage.IPV4_FILTER__TIME_TO_LIVE:
                return TIME_TO_LIVE_EDEFAULT == null ? timeToLive != null : !TIME_TO_LIVE_EDEFAULT.equals(timeToLive);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (protocolType: ");
        result.append(protocolType);
        result.append(", protocolTypeTmplParam: ");
        result.append(protocolTypeTmplParam);
        result.append(", sourceIP: ");
        result.append(sourceIP);
        result.append(", destIP: ");
        result.append(destIP);
        result.append(", timeToLive: ");
        result.append(timeToLive);
        result.append(')');
        return result.toString();
    }

} //IPv4FilterImpl
