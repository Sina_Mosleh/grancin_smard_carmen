package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.EthernetFilter;
import de.bmw.smard.modeller.conditions.EthernetMessage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ethernet Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.EthernetMessageImpl#getEthernetFilter <em>Ethernet Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EthernetMessageImpl extends AbstractBusMessageImpl implements EthernetMessage {
    /**
     * The cached value of the '{@link #getEthernetFilter() <em>Ethernet Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getEthernetFilter()
     * @generated
     * @ordered
     */
    protected EthernetFilter ethernetFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected EthernetMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.ETHERNET_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EthernetFilter getEthernetFilter() {
        return ethernetFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetEthernetFilter(EthernetFilter newEthernetFilter, NotificationChain msgs) {
        EthernetFilter oldEthernetFilter = ethernetFilter;
        ethernetFilter = newEthernetFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER, oldEthernetFilter, newEthernetFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEthernetFilter(EthernetFilter newEthernetFilter) {
        if (newEthernetFilter != ethernetFilter) {
            NotificationChain msgs = null;
            if (ethernetFilter != null)
                msgs = ((InternalEObject) ethernetFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER,
                        null, msgs);
            if (newEthernetFilter != null)
                msgs = ((InternalEObject) newEthernetFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER,
                        null, msgs);
            msgs = basicSetEthernetFilter(newEthernetFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER, newEthernetFilter, newEthernetFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER:
                return basicSetEthernetFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER:
                return getEthernetFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER:
                setEthernetFilter((EthernetFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER:
                setEthernetFilter((EthernetFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.ETHERNET_MESSAGE__ETHERNET_FILTER:
                return ethernetFilter != null;
        }
        return super.eIsSet(featureID);
    }


    @Override
    public AbstractFilter getPrimaryFilter() {
        return ethernetFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsEthernet() {
        return true;
    }
} //EthernetMessageImpl
