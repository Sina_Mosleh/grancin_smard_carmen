package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.DataType;
import de.bmw.smard.modeller.conditions.IStringOperand;
import de.bmw.smard.modeller.conditions.Matches;
import de.bmw.smard.modeller.conditions.RegexOperation;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Matches</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.MatchesImpl#getRegex <em>Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.MatchesImpl#getDynamicRegex <em>Dynamic Regex</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.MatchesImpl#getStringToCheck <em>String To Check</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchesImpl extends StringExpressionImpl implements Matches {
    /**
     * The default value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected static final String REGEX_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getRegex() <em>Regex</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getRegex()
     * @generated
     * @ordered
     */
    protected String regex = REGEX_EDEFAULT;

    /**
     * The cached value of the '{@link #getDynamicRegex() <em>Dynamic Regex</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getDynamicRegex()
     * @generated
     * @ordered
     */
    protected IStringOperand dynamicRegex;

    /**
     * The cached value of the '{@link #getStringToCheck() <em>String To Check</em>}' containment reference.
     * <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #getStringToCheck()
     * @generated
     * @ordered
     */
    protected IStringOperand stringToCheck;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected MatchesImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.MATCHES;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getRegex() {
        return regex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setRegex(String newRegex) {
        String oldRegex = regex;
        regex = newRegex;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__REGEX, oldRegex, regex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getStringToCheck() {
        return stringToCheck;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetStringToCheck(IStringOperand newStringToCheck, NotificationChain msgs) {
        IStringOperand oldStringToCheck = stringToCheck;
        stringToCheck = newStringToCheck;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__STRING_TO_CHECK, oldStringToCheck, newStringToCheck);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStringToCheck(IStringOperand newStringToCheck) {
        if (newStringToCheck != stringToCheck) {
            NotificationChain msgs = null;
            if (stringToCheck != null)
                msgs = ((InternalEObject) stringToCheck).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__STRING_TO_CHECK, null, msgs);
            if (newStringToCheck != null)
                msgs = ((InternalEObject) newStringToCheck).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__STRING_TO_CHECK, null, msgs);
            msgs = basicSetStringToCheck(newStringToCheck, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__STRING_TO_CHECK, newStringToCheck, newStringToCheck));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IStringOperand getDynamicRegex() {
        return dynamicRegex;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetDynamicRegex(IStringOperand newDynamicRegex, NotificationChain msgs) {
        IStringOperand oldDynamicRegex = dynamicRegex;
        dynamicRegex = newDynamicRegex;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__DYNAMIC_REGEX, oldDynamicRegex, newDynamicRegex);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDynamicRegex(IStringOperand newDynamicRegex) {
        if (newDynamicRegex != dynamicRegex) {
            NotificationChain msgs = null;
            if (dynamicRegex != null)
                msgs = ((InternalEObject) dynamicRegex).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__DYNAMIC_REGEX, null, msgs);
            if (newDynamicRegex != null)
                msgs = ((InternalEObject) newDynamicRegex).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.MATCHES__DYNAMIC_REGEX, null, msgs);
            msgs = basicSetDynamicRegex(newDynamicRegex, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.MATCHES__DYNAMIC_REGEX, newDynamicRegex, newDynamicRegex));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidRegex(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.REGEX_OPERATION__IS_VALID_HAS_VALID_REGEX)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.regex()
                        .withRegexValues(this.regex, this.dynamicRegex)
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStringToCheck(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.MATCHES__IS_VALID_HAS_VALID_STRING_TO_CHECK)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.when(stringToCheck == null || stringToCheck.get_EvaluationDataType() != DataType.STRING)
                        .error("_Validation_Conditions_Matches_StringToCheck")
                        .build())
                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                return basicSetDynamicRegex(null, msgs);
            case ConditionsPackage.MATCHES__STRING_TO_CHECK:
                return basicSetStringToCheck(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.MATCHES__REGEX:
                return getRegex();
            case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                return getDynamicRegex();
            case ConditionsPackage.MATCHES__STRING_TO_CHECK:
                return getStringToCheck();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.MATCHES__REGEX:
                setRegex((String) newValue);
                return;
            case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                setDynamicRegex((IStringOperand) newValue);
                return;
            case ConditionsPackage.MATCHES__STRING_TO_CHECK:
                setStringToCheck((IStringOperand) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.MATCHES__REGEX:
                setRegex(REGEX_EDEFAULT);
                return;
            case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                setDynamicRegex((IStringOperand) null);
                return;
            case ConditionsPackage.MATCHES__STRING_TO_CHECK:
                setStringToCheck((IStringOperand) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.MATCHES__REGEX:
                return REGEX_EDEFAULT == null ? regex != null : !REGEX_EDEFAULT.equals(regex);
            case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                return dynamicRegex != null;
            case ConditionsPackage.MATCHES__STRING_TO_CHECK:
                return stringToCheck != null;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
        if (baseClass == RegexOperation.class) {
            switch (derivedFeatureID) {
                case ConditionsPackage.MATCHES__REGEX:
                    return ConditionsPackage.REGEX_OPERATION__REGEX;
                case ConditionsPackage.MATCHES__DYNAMIC_REGEX:
                    return ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX;
                default:
                    return -1;
            }
        }
        return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
        if (baseClass == RegexOperation.class) {
            switch (baseFeatureID) {
                case ConditionsPackage.REGEX_OPERATION__REGEX:
                    return ConditionsPackage.MATCHES__REGEX;
                case ConditionsPackage.REGEX_OPERATION__DYNAMIC_REGEX:
                    return ConditionsPackage.MATCHES__DYNAMIC_REGEX;
                default:
                    return -1;
            }
        }
        return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (regex: ");
        result.append(regex);
        result.append(')');
        return result.toString();
    }

} // MatchesImpl
