package de.bmw.smard.modeller.validator.value;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class RegexValueValidator implements ValueValidator {
	
	private final String errorMessageIdentifier;
	
	public RegexValueValidator(String errorMessageIdentifier) {
		this.errorMessageIdentifier = errorMessageIdentifier;
	}

	@Override
	public boolean validate(String value, BaseValidator baseValidator) {
		if(StringUtils.isBlank(value)) {
			if(baseValidator != null) {
        		baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
        	}
            return false;
		}
		
        try {
            Pattern.compile(value);
        } catch (PatternSyntaxException e) {
        	if(baseValidator != null) {
        		baseValidator.attach(Severity.ERROR, errorMessageIdentifier, null);
        	}
            return false;
        }

        return true;
	}

}
