package de.bmw.smard.modeller.statemachine;

import org.eclipse.emf.common.util.Enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Trigger</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see de.bmw.smard.modeller.statemachine.StatemachinePackage#getTrigger()
 * @model
 * @generated
 */
public enum Trigger implements Enumerator {
    /**
     * The '<em><b>TEMPLATEDEFINED</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED_VALUE
     * @generated
     * @ordered
     */
    TEMPLATEDEFINED(-1, "TEMPLATEDEFINED", "TEMPLATEDEFINED"),
    /**
     * The '<em><b>ON ENTRY</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ON_ENTRY_VALUE
     * @generated
     * @ordered
     */
    ON_ENTRY(0, "ON_ENTRY", "ON_ENTRY"),

    /**
     * The '<em><b>ON EXIT</b></em>' literal object.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #ON_EXIT_VALUE
     * @generated
     * @ordered
     */
    ON_EXIT(1, "ON_EXIT", "ON_EXIT");

    /**
     * The '<em><b>TEMPLATEDEFINED</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #TEMPLATEDEFINED
     * @model
     * @generated
     * @ordered
     */
    public static final int TEMPLATEDEFINED_VALUE = -1;

    /**
     * The '<em><b>ON ENTRY</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>On Entry</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ON_ENTRY
     * @model
     * @generated
     * @ordered
     */
    public static final int ON_ENTRY_VALUE = 0;

    /**
     * The '<em><b>ON EXIT</b></em>' literal value.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of '<em><b>On Exit</b></em>' literal object isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @see #ON_EXIT
     * @model
     * @generated
     * @ordered
     */
    public static final int ON_EXIT_VALUE = 1;

    /**
     * An array of all the '<em><b>Trigger</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final Trigger[] VALUES_ARRAY =
            new Trigger[] {
                    TEMPLATEDEFINED,
                    ON_ENTRY,
                    ON_EXIT,
            };

    /**
     * A public read-only list of all the '<em><b>Trigger</b></em>' enumerators.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<Trigger> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Trigger</b></em>' literal with the specified literal value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static Trigger get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Trigger result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Trigger</b></em>' literal with the specified name.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static Trigger getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            Trigger result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Trigger</b></em>' literal with the specified integer value.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static Trigger get(int value) {
        switch (value) {
            case TEMPLATEDEFINED_VALUE:
                return TEMPLATEDEFINED;
            case ON_ENTRY_VALUE:
                return ON_ENTRY;
            case ON_EXIT_VALUE:
                return ON_EXIT;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    private Trigger(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string representation.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} //Trigger
