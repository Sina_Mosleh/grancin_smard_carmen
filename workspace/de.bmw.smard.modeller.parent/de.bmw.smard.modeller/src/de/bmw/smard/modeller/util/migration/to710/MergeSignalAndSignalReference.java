package de.bmw.smard.modeller.util.migration.to710;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.edapt.migration.CustomMigration;
import org.eclipse.emf.edapt.migration.MigrationException;
import org.eclipse.emf.edapt.spi.migration.Instance;
import org.eclipse.emf.edapt.spi.migration.Metamodel;
import org.eclipse.emf.edapt.spi.migration.Model;

public class MergeSignalAndSignalReference extends CustomMigration {

	private Map<Instance, Instance> signalReferencesToMigrate = new HashMap<Instance, Instance>();
	private Map<Instance, Instance> signalVariablesToMigrate = new HashMap<Instance, Instance>();
	private List<Instance> signalChecksToMigrate = new ArrayList<Instance>();
	private List<Instance> signalComparisonExpressionsToMigrate = new ArrayList<Instance>();
	
	// container of SignalReferenceUse
	private List<Instance> signalReferenceUsesToMigrate = new ArrayList<Instance>();
	
	
	
	@Override
	public void migrateBefore(Model model, Metamodel metamodel) throws MigrationException {
		
		EList<Instance> condDocs = model.getAllInstances("conditions.ConditionsDocument");
		for (Instance singleCondDoc : condDocs) {
			if (null == getSignalReferenceSet(singleCondDoc)) {
				singleCondDoc.add("signalReferencesSet", model.newInstance("conditions.SignalReferenceSet") );
			}
		}
		
		// collect all SignalReferences
		for(Instance signalReference : model.getAllInstances("conditions.SignalReference")) {
			Instance signal = signalReference.get("signal");
			signalReferencesToMigrate.put(signalReference, signal);
		}
		
		// collect all SignalVariables
		for(Instance signalVariable : model.getAllInstances("conditions.SignalVariable")) {
			Instance signal = signalVariable.get("signal");
			signalVariablesToMigrate.put(signalVariable, signal);
		}
		
		// collect all SignalChecks
		for(Instance signalCheck : model.getAllInstances("conditions.SignalCheck")) {
			signalChecksToMigrate.add(signalCheck);
		}
		
		// collect all SignalComparisonExpressions
		for(Instance signalComparisonExpression : model.getAllInstances("conditions.SignalComparisonExpression")) {
			signalComparisonExpressionsToMigrate.add(signalComparisonExpression);
		}
		
		// collect all Extracts
		for(Instance extract : model.getAllInstances("conditions.Extract")) {
			signalReferenceUsesToMigrate.add(extract);
		}
		
		// collect all Matches
		for(Instance matches : model.getAllInstances("conditions.Matches")) {
			signalReferenceUsesToMigrate.add(matches);
		}
		
		// collect all ParseNumericToString
		for(Instance parseNumericToString : model.getAllInstances("conditions.ParseNumericToString")) {
			signalReferenceUsesToMigrate.add(parseNumericToString);
		}
		
		// collect all ParseStringToDouble
		for(Instance parseStringToDouble : model.getAllInstances("conditions.ParseStringToDouble")) {
			signalReferenceUsesToMigrate.add(parseStringToDouble);
		}
		
		// collect all StringLength
		for(Instance stringLength : model.getAllInstances("conditions.StringLength")) {
			signalReferenceUsesToMigrate.add(stringLength);
		}
		
		// collect all Substring
		for(Instance substring : model.getAllInstances("conditions.Substring")) {
			signalReferenceUsesToMigrate.add(substring);
		}
	}

	@Override
	public void migrateAfter(Model model, Metamodel metamodel) throws MigrationException {
				
		Instance signalReference;
		Instance signal;
		
		Instance signalReferenceSet;
		
		Instance signalVariable;
		Instance signalOrSignalReferenceUse;
		Instance signalReferenceUse;
		
		// 0. "migrate" SignalChecks
		// -> just take the Signal contained in the SignalChecks SignalReference
		// and put it into SignalChecks Signal
		// the migration (fusion) of SignalReference and Signal takes place in step 4
 		for(Instance signalCheck : signalChecksToMigrate) {
			signalReference = signalCheck.get("signalReference");
			if(signalReference != null) {
				signal = signalReference.get("signal");
			
				if(signal != null) {
					signalCheck.remove("signalReference", signalReference);
					signalCheck.set("signal", signal);
					
					signal = null;
				}
				signalReference = null;
			}
			
		}
		
		// 1. "migrate" SignalComparisonExpression
		for(Instance signalComparisonExpression : signalComparisonExpressionsToMigrate) {
			List<Instance> comparatorSignals = signalComparisonExpression.get("comparatorSignal");
			for(Instance comparatorSignal : comparatorSignals) {
				if(comparatorSignal.instanceOf("conditions.SignalReferenceUse")) {
					eliminateSignalReference(comparatorSignal);
				} else if(comparatorSignal.instanceOf("conditions.Signal")) {
					// if the comparatorSignal is a user-defined Signal, it has to be moved to SignalReferenceSet
					// and has to be referenced by a SignalReferenceUse
					signalReferenceSet = getSignalReferenceSet(signalComparisonExpression);
					
					// if there are files without a signalReferenceSet create one first
					if (signalReferenceSet == null) {
						signalReferenceSet = model.newInstance("conditions.SignalReferenceSet");
						Instance r00t = signalComparisonExpression.getResource().getRootInstances().get(0);
						r00t.add("signalReferencesSet", signalReferenceSet);							
					}
					signalReferenceSet.add("signals", comparatorSignal);
					
					signalComparisonExpression.remove("comparatorSignal", comparatorSignal);
					
					comparatorSignal.set("id", createNewUniqueObjectIdForSignal());
					signalReferenceSet.add("signals", comparatorSignal);
					
					signalReferenceUse = model.newInstance("conditions.SignalReferenceUse");
					signalReferenceUse.add("signal", comparatorSignal);
					signalComparisonExpression.add("comparatorSignal", signalReferenceUse);
				}
			}
		}
		
		// 2. "migrate" SignalReferenceUses
		for(Instance instance : signalReferenceUsesToMigrate) {
			signalReferenceUse = null;
			if(instance.instanceOf("conditions.Extract") || instance.instanceOf("conditions.Substring")) {
				signalReferenceUse = instance.get("stringToExtract");
			} else if(instance.instanceOf("conditions.Matches")) {
				signalReferenceUse = instance.get("stringToCheck");
			} else if(instance.instanceOf("conditions.ParseNumericToString")) {
				signalReferenceUse = instance.get("numericOperandToBeParsed");
			} else if(instance.instanceOf("conditions.ParseStringToDouble")) {
				signalReferenceUse = instance.get("stringToParse");
			} else if(instance.instanceOf("conditions.StringLength")) {
				signalReferenceUse = instance.get("stringOperand");
			}
			eliminateSignalReference(signalReferenceUse);
		}
		
		
		// 3. migrate SignalVariables
		// there are two use cases: 
		// 3.1 SignalVariable with a user-defined Signal
		// 3.2 SignalVariable with a child SignalReferenceUse (which points to an existing SignalReference)
		Iterator<Entry<Instance, Instance>> iterator = signalVariablesToMigrate.entrySet().iterator();
		while(iterator.hasNext()) {
			Map.Entry<Instance, Instance> pair = (Map.Entry<Instance, Instance>) iterator.next();
			
			signalVariable = pair.getKey();
			signalOrSignalReferenceUse = pair.getValue();
			
			if(signalOrSignalReferenceUse != null) {

				signalReferenceSet = getSignalReferenceSet(signalVariable);
				
				// 3.1
				if(signalOrSignalReferenceUse.instanceOf("conditions.Signal")) {
					// add existing Signal to SignalReferenceSet and point it back to the SignalVariable
					
					signalVariable.remove("signal", signalOrSignalReferenceUse);
					signalReferenceSet.add("signals", signalOrSignalReferenceUse);
					
					signalOrSignalReferenceUse.set("id", createNewUniqueObjectIdForSignal());
					
					signalVariable.set("signal", signalOrSignalReferenceUse);

				// 3.2
				} else if(signalOrSignalReferenceUse.instanceOf("conditions.SignalReferenceUse")) {
					// there already is a Signal that's getting referenced in this SignalVariable
					
					signalReference = signalOrSignalReferenceUse.get("signalReference");
					
					if(signalReference != null) {
						signal = signalReference.get("signal");
						signalOrSignalReferenceUse.remove("signalReference", signalReference); //remove SignalReference from SignalReferenceUse
						signalVariable.remove("signal", signalOrSignalReferenceUse); //remove SignalReferenceUse from SignalVariable					
						signalVariable.set("signal", signal);
					}
					
					model.delete(signalOrSignalReferenceUse);
					
					signal = null;
					signalReference = null;
				}
			}
			
		}

		// 4. migrate existing SignalReferences 
		iterator = signalReferencesToMigrate.entrySet().iterator();
		while(iterator.hasNext()) {
			Map.Entry<Instance, Instance> pair = (Map.Entry<Instance, Instance>) iterator.next();
			
			signalReference = pair.getKey();
			signal = pair.getValue();
			
			signalReferenceSet = getSignalReferenceSet(signalReference);
			
			if(signal != null) {
				signal.set("id", createNewUniqueObjectIdForSignal());
				signal.set("name", signalReference.get("name"));
				signal.set("description", signalReference.get("description"));
				
				signalReference.remove("signal", signal);
				signalReferenceSet.add("signals", signal);
			}
			
			signalReferenceSet.remove("signalReferences", signalReference);
			model.delete(signalReference);
		}
	}
	
	/**
	 * at 7.0.0 Signals didn't have id's but were referenced via the id of
	 * their parent SignalReferences
	 * for 7.1.0 Signal get their own unique id's and are referenced directly
	 * @return
	 */
	private String createNewUniqueObjectIdForSignal() {
		String newUniqueObjectId = "sig." + UUID.randomUUID().toString();
		return newUniqueObjectId;
	}
	
	/**
	 * 7.0.0
	 * xxxExpression
	 * |- 	SignalReferenceUse
	 * 		|-	SignalReference
	 * 			|- Signal
	 * 
	 * for 7.1.0 the SignalReference is eliminated and the Signal is a direct 
	 * child of SignalReferenceUse
	 * @param signalReferenceUse
	 */
	private void eliminateSignalReference(Instance signalReferenceUse) {
		Instance signalReference;
		Instance signal;
		
		if(signalReferenceUse != null && signalReferenceUse.instanceOf("conditions.SignalReferenceUse")) {
			signalReference = signalReferenceUse.get("signalReference");
			
			if(signalReference != null) {
				signal = signalReference.get("signal");
				
				if(signal != null) {
					signalReferenceUse.set("signal", signal);
					
					signal = null;
				}
				signalReference = null;
			}
		}
	}
	
	/**
	 * algorithm to find the root of the document "ConditionDocument"
	 * from which you can find the SignalReferenceSet
	 * @return
	 */
	private Instance getSignalReferenceSet(Instance instance) {
		Instance documentRoot = instance;
		documentRoot = instance.getResource().getRootInstances().get(0);	
		Instance signalReferenceSet = documentRoot.get("signalReferencesSet");
		
		return signalReferenceSet;
	}

}
