package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.StreamAnalysisFlagsOrTemplatePlaceholderEnum;
import de.bmw.smard.modeller.conditions.TCPFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TCP Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getSequenceNumber <em>Sequence Number</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getAcknowledgementNumber <em>Acknowledgement Number</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getTcpFlags <em>Tcp Flags</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getStreamAnalysisFlags <em>Stream Analysis Flags</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getStreamAnalysisFlagsTmplParam <em>Stream Analysis Flags Tmpl Param</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.TCPFilterImpl#getStreamValidPayloadOffset <em>Stream Valid Payload Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TCPFilterImpl extends TPFilterImpl implements TCPFilter {
    /**
     * The default value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSequenceNumber()
     * @generated
     * @ordered
     */
    protected static final String SEQUENCE_NUMBER_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getSequenceNumber() <em>Sequence Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getSequenceNumber()
     * @generated
     * @ordered
     */
    protected String sequenceNumber = SEQUENCE_NUMBER_EDEFAULT;
    /**
     * The default value of the '{@link #getAcknowledgementNumber() <em>Acknowledgement Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAcknowledgementNumber()
     * @generated
     * @ordered
     */
    protected static final String ACKNOWLEDGEMENT_NUMBER_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getAcknowledgementNumber() <em>Acknowledgement Number</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getAcknowledgementNumber()
     * @generated
     * @ordered
     */
    protected String acknowledgementNumber = ACKNOWLEDGEMENT_NUMBER_EDEFAULT;
    /**
     * The default value of the '{@link #getTcpFlags() <em>Tcp Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTcpFlags()
     * @generated
     * @ordered
     */
    protected static final String TCP_FLAGS_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getTcpFlags() <em>Tcp Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getTcpFlags()
     * @generated
     * @ordered
     */
    protected String tcpFlags = TCP_FLAGS_EDEFAULT;
    /**
     * The default value of the '{@link #getStreamAnalysisFlags() <em>Stream Analysis Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamAnalysisFlags()
     * @generated
     * @ordered
     */
    protected static final StreamAnalysisFlagsOrTemplatePlaceholderEnum STREAM_ANALYSIS_FLAGS_EDEFAULT = StreamAnalysisFlagsOrTemplatePlaceholderEnum.ALL;
    /**
     * The cached value of the '{@link #getStreamAnalysisFlags() <em>Stream Analysis Flags</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamAnalysisFlags()
     * @generated
     * @ordered
     */
    protected StreamAnalysisFlagsOrTemplatePlaceholderEnum streamAnalysisFlags = STREAM_ANALYSIS_FLAGS_EDEFAULT;
    /**
     * The default value of the '{@link #getStreamAnalysisFlagsTmplParam() <em>Stream Analysis Flags Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamAnalysisFlagsTmplParam()
     * @generated
     * @ordered
     */
    protected static final String STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getStreamAnalysisFlagsTmplParam() <em>Stream Analysis Flags Tmpl Param</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamAnalysisFlagsTmplParam()
     * @generated
     * @ordered
     */
    protected String streamAnalysisFlagsTmplParam = STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT;
    /**
     * The default value of the '{@link #getStreamValidPayloadOffset() <em>Stream Valid Payload Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamValidPayloadOffset()
     * @generated
     * @ordered
     */
    protected static final String STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT = null;
    /**
     * The cached value of the '{@link #getStreamValidPayloadOffset() <em>Stream Valid Payload Offset</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getStreamValidPayloadOffset()
     * @generated
     * @ordered
     */
    protected String streamValidPayloadOffset = STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected TCPFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.TCP_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSequenceNumber(String newSequenceNumber) {
        String oldSequenceNumber = sequenceNumber;
        sequenceNumber = newSequenceNumber;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER, oldSequenceNumber, sequenceNumber));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getAcknowledgementNumber() {
        return acknowledgementNumber;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAcknowledgementNumber(String newAcknowledgementNumber) {
        String oldAcknowledgementNumber = acknowledgementNumber;
        acknowledgementNumber = newAcknowledgementNumber;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER, oldAcknowledgementNumber, acknowledgementNumber));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTcpFlags() {
        return tcpFlags;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTcpFlags(String newTcpFlags) {
        String oldTcpFlags = tcpFlags;
        tcpFlags = newTcpFlags;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__TCP_FLAGS, oldTcpFlags, tcpFlags));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public StreamAnalysisFlagsOrTemplatePlaceholderEnum getStreamAnalysisFlags() {
        return streamAnalysisFlags;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStreamAnalysisFlags(StreamAnalysisFlagsOrTemplatePlaceholderEnum newStreamAnalysisFlags) {
        StreamAnalysisFlagsOrTemplatePlaceholderEnum oldStreamAnalysisFlags = streamAnalysisFlags;
        streamAnalysisFlags = newStreamAnalysisFlags == null ? STREAM_ANALYSIS_FLAGS_EDEFAULT : newStreamAnalysisFlags;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS, oldStreamAnalysisFlags, streamAnalysisFlags));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStreamAnalysisFlagsTmplParam() {
        return streamAnalysisFlagsTmplParam;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStreamAnalysisFlagsTmplParam(String newStreamAnalysisFlagsTmplParam) {
        String oldStreamAnalysisFlagsTmplParam = streamAnalysisFlagsTmplParam;
        streamAnalysisFlagsTmplParam = newStreamAnalysisFlagsTmplParam;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM, oldStreamAnalysisFlagsTmplParam, streamAnalysisFlagsTmplParam));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getStreamValidPayloadOffset() {
        return streamValidPayloadOffset;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setStreamValidPayloadOffset(String newStreamValidPayloadOffset) {
        String oldStreamValidPayloadOffset = streamValidPayloadOffset;
        streamValidPayloadOffset = newStreamValidPayloadOffset;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET, oldStreamValidPayloadOffset, streamValidPayloadOffset));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSourcePort(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_SOURCE_PORT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(sourcePort)
                        .name(ConditionsPackage.Literals.TP_FILTER__SOURCE_PORT.getName())
                        .warning("_Validation_Conditions_TCPFilter_SourcePort_WARNING")
                        .error("_Validation_Conditions_TCPFilter_SourcePort_IllegalSourcePort")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidDestinationPort(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_DESTINATION_PORT)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(destPort)
                        .name(ConditionsPackage.Literals.TP_FILTER__DEST_PORT.getName())
                        .warning("_Validation_Conditions_TCPFilter_DestinationPort_WARNING")
                        .error("_Validation_Conditions_TCPFilter_DestinationPort_IllegalDestinationPort")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidSequenceNumber(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_SEQUENCE_NUMBER)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(sequenceNumber)
                        .name(ConditionsPackage.Literals.TCP_FILTER__SEQUENCE_NUMBER.getName())
                        .warning("_Validation_Conditions_TCPFilter_SequenceNumber_WARNING")
                        .error("_Validation_Conditions_TCPFilter_SequenceNumber_IllegalSequenceNumber")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidAcknowledgementNumber(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_ACKNOWLEDGEMENT_NUMBER)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(acknowledgementNumber)
                        .name(ConditionsPackage.Literals.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER.getName())
                        .warning("_Validation_Conditions_TCPFilter_AcknowledgementNumber_WARNING")
                        .error("_Validation_Conditions_TCPFilter_AcknowledgementNumber_IllegalAcknowledgementNumber")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidTcpFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_TCP_FLAG)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(tcpFlags)
                        .name(ConditionsPackage.Literals.TCP_FILTER__TCP_FLAGS.getName())
                        .warning("_Validation_Conditions_TCPFilter_TcpFlag_WARNING")
                        .error("_Validation_Conditions_TCPFilter_TcpFlag_IllegalTcpFlag")
                        .min(0)
                        .max(255)
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStreamAnalysisFlag(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_STREAM_ANALYSIS_FLAG)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.notNull(streamAnalysisFlags)
                        .error("_Validation_Conditions_TCPFilter_StreamAnalysisFlag_IsNull")
                        .build())

                .with(Validators.checkTemplate(streamAnalysisFlags)
                        .templateType(StreamAnalysisFlagsOrTemplatePlaceholderEnum.TEMPLATE_DEFINED)
                        .tmplParam(streamAnalysisFlagsTmplParam)
                        .containsParameterError("_Validation_Conditions_TCPFilter_StreamAnalysisFlag_ContainsParameters")
                        .tmplParamIsNullError("_Validation_Conditions_TCPFilter_StreamAnalysisFlagTmplParam_IsNull")
                        .noPlaceholderError("_Validation_Conditions_TCPFilter_StreamAnalysisFlagTmplParam_NotAValidPlaceholderName")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidStreamValidPayloadOffset(DiagnosticChain diagnostics, Map<Object, Object> context) {

        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.TCP_FILTER__IS_VALID_HAS_VALID_STREAM_VALID_PAYLOAD_OFFSET)
                .object(this)
                .diagnostic(diagnostics)

                .with(Validators.intListOrRange(streamValidPayloadOffset)
                        .name(ConditionsPackage.Literals.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET.getName())
                        .warning("_Validation_Conditions_TCPFilter_StreamValidPayloadOffset_WARNING")
                        .error("_Validation_Conditions_TCPFilter_StreamValidPayloadOffset_IllegalStreamValidPayloadOffset")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
                return getSequenceNumber();
            case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
                return getAcknowledgementNumber();
            case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
                return getTcpFlags();
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
                return getStreamAnalysisFlags();
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
                return getStreamAnalysisFlagsTmplParam();
            case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
                return getStreamValidPayloadOffset();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
                setSequenceNumber((String) newValue);
                return;
            case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
                setAcknowledgementNumber((String) newValue);
                return;
            case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
                setTcpFlags((String) newValue);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
                setStreamAnalysisFlags((StreamAnalysisFlagsOrTemplatePlaceholderEnum) newValue);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
                setStreamAnalysisFlagsTmplParam((String) newValue);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
                setStreamValidPayloadOffset((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
                setSequenceNumber(SEQUENCE_NUMBER_EDEFAULT);
                return;
            case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
                setAcknowledgementNumber(ACKNOWLEDGEMENT_NUMBER_EDEFAULT);
                return;
            case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
                setTcpFlags(TCP_FLAGS_EDEFAULT);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
                setStreamAnalysisFlags(STREAM_ANALYSIS_FLAGS_EDEFAULT);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
                setStreamAnalysisFlagsTmplParam(STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT);
                return;
            case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
                setStreamValidPayloadOffset(STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.TCP_FILTER__SEQUENCE_NUMBER:
                return SEQUENCE_NUMBER_EDEFAULT == null ? sequenceNumber != null : !SEQUENCE_NUMBER_EDEFAULT.equals(sequenceNumber);
            case ConditionsPackage.TCP_FILTER__ACKNOWLEDGEMENT_NUMBER:
                return ACKNOWLEDGEMENT_NUMBER_EDEFAULT == null ? acknowledgementNumber != null : !ACKNOWLEDGEMENT_NUMBER_EDEFAULT.equals(acknowledgementNumber);
            case ConditionsPackage.TCP_FILTER__TCP_FLAGS:
                return TCP_FLAGS_EDEFAULT == null ? tcpFlags != null : !TCP_FLAGS_EDEFAULT.equals(tcpFlags);
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS:
                return streamAnalysisFlags != STREAM_ANALYSIS_FLAGS_EDEFAULT;
            case ConditionsPackage.TCP_FILTER__STREAM_ANALYSIS_FLAGS_TMPL_PARAM:
                return STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT == null ? streamAnalysisFlagsTmplParam != null : !STREAM_ANALYSIS_FLAGS_TMPL_PARAM_EDEFAULT
                        .equals(streamAnalysisFlagsTmplParam);
            case ConditionsPackage.TCP_FILTER__STREAM_VALID_PAYLOAD_OFFSET:
                return STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT == null ? streamValidPayloadOffset != null : !STREAM_VALID_PAYLOAD_OFFSET_EDEFAULT
                        .equals(streamValidPayloadOffset);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (sequenceNumber: ");
        result.append(sequenceNumber);
        result.append(", acknowledgementNumber: ");
        result.append(acknowledgementNumber);
        result.append(", tcpFlags: ");
        result.append(tcpFlags);
        result.append(", streamAnalysisFlags: ");
        result.append(streamAnalysisFlags);
        result.append(", streamAnalysisFlagsTmplParam: ");
        result.append(streamAnalysisFlagsTmplParam);
        result.append(", streamValidPayloadOffset: ");
        result.append(streamValidPayloadOffset);
        result.append(')');
        return result.toString();
    }

} //TCPFilterImpl
