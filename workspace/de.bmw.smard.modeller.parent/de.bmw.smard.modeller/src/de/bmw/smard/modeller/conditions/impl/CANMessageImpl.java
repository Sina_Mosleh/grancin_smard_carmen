package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractFilter;
import de.bmw.smard.modeller.conditions.CANFilter;
import de.bmw.smard.modeller.conditions.CANMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CAN Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.CANMessageImpl#getCanFilter <em>Can Filter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CANMessageImpl extends AbstractBusMessageImpl implements CANMessage {
    /**
     * The cached value of the '{@link #getCanFilter() <em>Can Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getCanFilter()
     * @generated
     * @ordered
     */
    protected CANFilter canFilter;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CANMessageImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.CAN_MESSAGE;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public CANFilter getCanFilter() {
        return canFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetCanFilter(CANFilter newCanFilter, NotificationChain msgs) {
        CANFilter oldCanFilter = canFilter;
        canFilter = newCanFilter;
        if (eNotificationRequired()) {
            ENotificationImpl notification =
                    new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE__CAN_FILTER, oldCanFilter, newCanFilter);
            if (msgs == null) msgs = notification;
            else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCanFilter(CANFilter newCanFilter) {
        if (newCanFilter != canFilter) {
            NotificationChain msgs = null;
            if (canFilter != null)
                msgs = ((InternalEObject) canFilter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CAN_MESSAGE__CAN_FILTER, null, msgs);
            if (newCanFilter != null)
                msgs = ((InternalEObject) newCanFilter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ConditionsPackage.CAN_MESSAGE__CAN_FILTER, null, msgs);
            msgs = basicSetCanFilter(newCanFilter, msgs);
            if (msgs != null) msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.CAN_MESSAGE__CAN_FILTER, newCanFilter, newCanFilter));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case ConditionsPackage.CAN_MESSAGE__CAN_FILTER:
                return basicSetCanFilter(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.CAN_MESSAGE__CAN_FILTER:
                return getCanFilter();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.CAN_MESSAGE__CAN_FILTER:
                setCanFilter((CANFilter) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CAN_MESSAGE__CAN_FILTER:
                setCanFilter((CANFilter) null);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.CAN_MESSAGE__CAN_FILTER:
                return canFilter != null;
        }
        return super.eIsSet(featureID);
    }


    @Override
    public AbstractFilter getPrimaryFilter() {
        return canFilter;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    @Override
    public boolean isNeedsErrorFrames() {
        if (getPrimaryFilter() != null) {
            return getPrimaryFilter().isNeedsErrorFrames();
        }
        return false;
    }

} //CANMessageImpl
