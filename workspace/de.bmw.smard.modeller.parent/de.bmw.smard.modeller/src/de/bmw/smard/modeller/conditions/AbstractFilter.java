
package de.bmw.smard.modeller.conditions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.AbstractFilter#getPayloadLength <em>Payload Length</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractFilter()
 * @model abstract="true"
 * @generated
 */
public interface AbstractFilter extends BaseClassWithSourceReference {

    /**
     * Returns the value of the '<em><b>Payload Length</b></em>' attribute.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Payload Length</em>' attribute isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Payload Length</em>' attribute.
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getAbstractFilter_PayloadLength()
     * @model dataType="de.bmw.smard.modeller.conditions.LongOrTemplatePlaceholder" transient="true" changeable="false" derived="true"
     *        annotation="http:///de/bmw/smard/modeller/HeaderSignalAttribute attrType='Double'"
     * @generated
     */
    String getPayloadLength();

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @model kind="operation"
     * @generated
     */
    boolean isNeedsErrorFrames();
} // AbstractFilter
