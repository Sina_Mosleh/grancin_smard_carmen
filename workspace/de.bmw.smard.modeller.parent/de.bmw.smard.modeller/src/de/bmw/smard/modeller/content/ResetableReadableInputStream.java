package de.bmw.smard.modeller.content;

import org.eclipse.emf.ecore.resource.URIConverter;

import java.io.IOException;
import java.io.Reader;

/**
 * Eclipse closes the InputStream of this class but it is not implemented
 * Therfor it is not possible to get the encoding or bom of this stream
 *
 * This is because the special behavior for URIConverter.ReadableInputstream mentioned
 * in {@link:SafeRootXMLContentHandlerImpl} is NOT called (seems to be a bug)
 *
 * The InputStream::reset() call comes from
 * {@link:ContentHandlerImpl::getByteOrderMark}
 * {@link:XMLContentHandlerImpl::load} via {@link:XMLContentHandlerImpl::getCharset}
 */
public class ResetableReadableInputStream extends URIConverter.ReadableInputStream {

    ResetableReadableInputStream(Reader reader) {
        super(reader);
    }

    @Override
    public synchronized void reset() throws IOException {
        try {
            super.reset();
        } catch (IOException ioe) {

            // do nothing, this avoids throwing a IOException and throw the
            // detected BOM or encoding away
        }
        reader.reset();
    }
}
