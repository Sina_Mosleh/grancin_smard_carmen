package de.bmw.smard.modeller.validation;

import java.util.List;

public class ValidationResult {

    private final boolean success;
    private final List<ValidationMessage> messages;

    public boolean isSuccess() {
        return success;
    }

    public List<ValidationMessage> getMessages() {
        return messages;
    }

    ValidationResult(boolean success, List<ValidationMessage> messages) {
        this.success = success;
        this.messages = messages;
    }
}
