package de.bmw.smard.modeller.conditions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Some IP Message</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}</li>
 * </ul>
 *
 * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPMessage()
 * @model
 * @generated
 */
public interface SomeIPMessage extends AbstractBusMessage {

    /**
     * Returns the value of the '<em><b>Some IP Filter</b></em>' containment reference.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Some IP Filter</em>' containment reference isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Some IP Filter</em>' containment reference.
     * @see #setSomeIPFilter(SomeIPFilter)
     * @see de.bmw.smard.modeller.conditions.ConditionsPackage#getSomeIPMessage_SomeIPFilter()
     * @model containment="true" required="true"
     * @generated
     */
    SomeIPFilter getSomeIPFilter();

    /**
     * Sets the value of the '{@link de.bmw.smard.modeller.conditions.SomeIPMessage#getSomeIPFilter <em>Some IP Filter</em>}' containment reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Some IP Filter</em>' containment reference.
     * @see #getSomeIPFilter()
     * @generated
     */
    void setSomeIPFilter(SomeIPFilter value);
} // SomeIPMessage
