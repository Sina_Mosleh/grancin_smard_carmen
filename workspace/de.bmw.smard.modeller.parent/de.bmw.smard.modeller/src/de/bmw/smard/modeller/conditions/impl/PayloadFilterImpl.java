package de.bmw.smard.modeller.conditions.impl;

import de.bmw.smard.modeller.conditions.AbstractBusMessage;
import de.bmw.smard.modeller.conditions.ConditionsPackage;
import de.bmw.smard.modeller.conditions.PayloadFilter;
import de.bmw.smard.modeller.conditions.util.ConditionsValidator;
import de.bmw.smard.modeller.validator.Validator;
import de.bmw.smard.modeller.validator.Validators;
import de.bmw.smard.modeller.validator.value.ValueValidators;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import java.util.Map;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Payload Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl#getIndex <em>Index</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl#getMask <em>Mask</em>}</li>
 * <li>{@link de.bmw.smard.modeller.conditions.impl.PayloadFilterImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PayloadFilterImpl extends AbstractFilterImpl implements PayloadFilter {
    /**
     * The default value of the '{@link #getIndex() <em>Index</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIndex()
     * @generated
     * @ordered
     */
    protected static final String INDEX_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getIndex() <em>Index</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getIndex()
     * @generated
     * @ordered
     */
    protected String index = INDEX_EDEFAULT;

    /**
     * The default value of the '{@link #getMask() <em>Mask</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMask()
     * @generated
     * @ordered
     */
    protected static final String MASK_EDEFAULT = "0xFF";

    /**
     * The cached value of the '{@link #getMask() <em>Mask</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getMask()
     * @generated
     * @ordered
     */
    protected String mask = MASK_EDEFAULT;

    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final String VALUE_EDEFAULT = "0";

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected String value = VALUE_EDEFAULT;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected PayloadFilterImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ConditionsPackage.Literals.PAYLOAD_FILTER;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getIndex() {
        return index;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setIndex(String newIndex) {
        String oldIndex = index;
        index = newIndex;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PAYLOAD_FILTER__INDEX, oldIndex, index));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMask() {
        return mask;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMask(String newMask) {
        String oldMask = mask;
        mask = newMask;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PAYLOAD_FILTER__MASK, oldMask, mask));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getValue() {
        return value;
    }

    /**
     * @generated NOT
     */
    public AbstractBusMessage getMessage() {
        EObject parent = this.eContainer();
        if (parent instanceof AbstractBusMessage) {
            return (AbstractBusMessage) parent;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValue(String newValue) {
        String oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ConditionsPackage.PAYLOAD_FILTER__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidIndex(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PAYLOAD_FILTER__IS_VALID_HAS_VALID_INDEX)
                .object(this)
                .diagnostic(diagnostics)
                .with(Validators.intTemplate(this.index)
                        .containsParameterError("_Validation_Conditions_PayloadFilter_Index_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_PayloadFilter_Index_NotAValidPlaceholderName")
                        .illegalValueError(ValueValidators.greaterZero("_Validation_Conditions_PayloadFilter_Index_MustBePositiveInteger"))
                        .illegalValueError("_Validation_Conditions_PayloadFilter_Index_IllegalValue")
                        .build())

                .with(Validators.notNull(this.index)
                        .error("_Validation_Conditions_PayloadFilter_Index_IsNull")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidMask(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PAYLOAD_FILTER__IS_VALID_HAS_VALID_MASK)
                .object(this)
                .diagnostic(diagnostics)
                .with(Validators.byteTemplate(this.mask)
                        .containsParameterError("_Validation_Conditions_PayloadFilter_Mask_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_PayloadFilter_Mask_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_PayloadFilter_Mask_IllegalValue")
                        .build())

                .with(Validators.notNull(this.mask)
                        .error("_Validation_Conditions_PayloadFilter_Mask_IsNull")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated NOT
     */
    public boolean isValid_hasValidValue(DiagnosticChain diagnostics, Map<Object, Object> context) {
        return Validator.builder()
                .source(ConditionsValidator.DIAGNOSTIC_SOURCE)
                .code(ConditionsValidator.PAYLOAD_FILTER__IS_VALID_HAS_VALID_VALUE)
                .object(this)
                .diagnostic(diagnostics)
                .with(Validators.byteTemplate(this.value)
                        .containsParameterError("_Validation_Conditions_PayloadFilter_Value_ContainsPlaceholders")
                        .invalidPlaceholderError("_Validation_Conditions_PayloadFilter_Value_NotAValidPlaceholderName")
                        .illegalValueError("_Validation_Conditions_PayloadFilter_Value_IllegalValue")
                        .build())

                .with(Validators.notNull(this.value)
                        .error("_Validation_Conditions_PayloadFilter_Value_IsNull")
                        .build())

                .validate();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case ConditionsPackage.PAYLOAD_FILTER__INDEX:
                return getIndex();
            case ConditionsPackage.PAYLOAD_FILTER__MASK:
                return getMask();
            case ConditionsPackage.PAYLOAD_FILTER__VALUE:
                return getValue();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case ConditionsPackage.PAYLOAD_FILTER__INDEX:
                setIndex((String) newValue);
                return;
            case ConditionsPackage.PAYLOAD_FILTER__MASK:
                setMask((String) newValue);
                return;
            case ConditionsPackage.PAYLOAD_FILTER__VALUE:
                setValue((String) newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PAYLOAD_FILTER__INDEX:
                setIndex(INDEX_EDEFAULT);
                return;
            case ConditionsPackage.PAYLOAD_FILTER__MASK:
                setMask(MASK_EDEFAULT);
                return;
            case ConditionsPackage.PAYLOAD_FILTER__VALUE:
                setValue(VALUE_EDEFAULT);
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case ConditionsPackage.PAYLOAD_FILTER__INDEX:
                return INDEX_EDEFAULT == null ? index != null : !INDEX_EDEFAULT.equals(index);
            case ConditionsPackage.PAYLOAD_FILTER__MASK:
                return MASK_EDEFAULT == null ? mask != null : !MASK_EDEFAULT.equals(mask);
            case ConditionsPackage.PAYLOAD_FILTER__VALUE:
                return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy()) return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (index: ");
        result.append(index);
        result.append(", mask: ");
        result.append(mask);
        result.append(", value: ");
        result.append(value);
        result.append(')');
        return result.toString();
    }

} //PayloadFilterImpl
