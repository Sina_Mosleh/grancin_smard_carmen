package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

import java.util.function.BiFunction;

public class FrameIdRangeValidator implements ValueValidator {

    private final BiFunction<String, Boolean, String> checkFrameIdRangePattern;

    public static FrameIdRangeValidator lin() {
        return create(TemplateUtils::checkLinIdRangePatternNotPlaceholder);
    }

    public static FrameIdRangeValidator can() {
        return create(TemplateUtils::checkCanIdRangePatternNotPlaceholder);
    }

    private static FrameIdRangeValidator create(BiFunction<String, Boolean, String> checkFrameIdRangePattern) {
        return new FrameIdRangeValidator(checkFrameIdRangePattern);
    }

    private FrameIdRangeValidator(BiFunction<String, Boolean, String> checkFrameIdRangePattern) {
        this.checkFrameIdRangePattern = checkFrameIdRangePattern;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {
        String substitution = checkFrameIdRangePattern.apply(value, false);
        if (StringUtils.isNotBlank(substitution)) {
            if(baseValidator != null) {
                baseValidator.attach(Severity.ERROR, "_Validation_Conditions_CANLINFlexRayFilter_FrameIdRange_IllegalFrameIdRange", substitution);
            }
            return false;
        }
        return true;
    }
}
