package de.bmw.smard.modeller.validator.value;

import de.bmw.smard.base.util.StringUtils;
import de.bmw.smard.modeller.util.TemplateUtils;
import de.bmw.smard.modeller.validation.Severity;
import de.bmw.smard.modeller.validator.BaseValidator;

public class BusIdRangeValidator implements ValueValidator {

    private String interfaceName;

    BusIdRangeValidator(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    @Override
    public boolean validate(String value, BaseValidator baseValidator) {

        String errorMessageIdentifier = null;
        String substitution = null;
        Integer busIdRange = null;
        try {
            busIdRange = TemplateUtils.getIntNotPlaceholder(value, true);
        } catch (NumberFormatException e) {
        }

        if (busIdRange != null && busIdRange == 0) {
            errorMessageIdentifier = "_Validation_Conditions_" + interfaceName + "_BusIdRange_IsZero";
        } else if (value != null && value.trim().length() != 0) {
            substitution = TemplateUtils.checkBusIdRangePatternNotPlaceholder(value, false);
            if (!StringUtils.nullOrEmpty(substitution)) {
                errorMessageIdentifier = "_Validation_Conditions_AbstractBusMessage_BusIdRange_IllegalBusIdRange";
            }
        }

        if (!StringUtils.nullOrEmpty(errorMessageIdentifier)) {
            if (baseValidator != null) {
                baseValidator.attach(Severity.ERROR, errorMessageIdentifier, substitution);
            }
            return false;
        }
        return true;
    }
}
