package com.bmw.vv.tests

/*
 * '''<input>''' -> '''<output after code generation>'''
 */

interface TestBench {
//	val importFKeySearchPosition =
//	'''
//		
//		import KeySearch.KeySearchPos.*
//		import KeySearch.KeySearchPos.REQREPKeySearchPos.*
//		import KeySearch.KeySearchPos.PUBKeySearchPos.*
//	'''
	
	val ScenarioWithKeyWordsInTitle = 		
	'''
		Scenario: Method publishADARResults is not sent if setActive is deactivated (setActive equals false)
	'''
	->
	'''
	'''
}
