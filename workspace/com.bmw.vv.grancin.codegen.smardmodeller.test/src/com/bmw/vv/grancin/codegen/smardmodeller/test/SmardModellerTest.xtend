package com.bmw.vv.grancin.codegen.smardmodeller.test

import com.bmw.vv.grancin.File
import com.bmw.vv.grancin.codegen.smardmodeller.runtime.SmardModellerCreator
import com.bmw.vv.grancin.codegen.chrisna.runtime.ChrisnaFSMGenerator
import com.bmw.vv.client.RequestData
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith
import java.util.List

@ExtendWith(InjectionExtension)
@InjectWith(GrancinInjectorProvider)
class SmardModellerTest implements possibleScensToTestGrancinGenNChrisnaFSM {
	@Inject ParseHelper<File> parser
   
	def List<Integer> counting(File model){
		var totalmessages = 0
		var totalsignals = 0
		var totalfilters = 0
		var totalvariables = 0
		var totalconditions = 0
		var totalstates= 0
		var totaltransitions = 0
		
		val csa = new ChrisnaFSMGenerator(model.eResource)
    	
    	val requestData = new RequestData("q486705", "NTFuYTU4cmE=", "SP2021", "SP2021", "21-07-240", 45000, csa.getInterfaceName);
    	//requestData.request
    	
    	val sdmodel = new SmardModellerCreator('Sina Mosleh', 'no description', 'no topic', 'EE-900', csa.getVersion, csa, requestData)
    	
    	var conditions = sdmodel.conditionDocuGenerator
    	totalmessages = conditions?.conditionsDocument?.signalReferencesSet?.getMessages.length
    	for(mes : sdmodel?.signalreferenceset?.getMessages){
    		totalsignals = totalsignals + mes?.signals.length
    		totalfilters = totalfilters + 1
    	}
    	totalvariables = conditions?.conditionsDocument?.variableSet?.variables.length
    	totalconditions = conditions?.conditionSet?.conditions.length
    	
    	var stmachine = sdmodel?.stateMachineDocuGenerator
    	totalstates = stmachine?.stateMachineSet?.stateMachines?.head?.states.length + 1
    	totaltransitions = stmachine?.stateMachineSet?.stateMachines?.head?.transitions.length
    	
		val List<Integer> array = #[totalmessages, totalsignals, totalfilters, totalvariables, totalconditions, totalstates, totaltransitions]
		array
	} 
	
	@Test
	def void titleOnly() {
		val model = parser.parse(FeatureTitle.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 0)
		Assertions.assertTrue(res.get(4) == 0)
		Assertions.assertTrue(res.get(5) == 1)
		Assertions.assertTrue(res.get(6) == 0)
	}
	
	@Test
	def void titleAndDescription() {
		val model = parser.parse(
		'''
		@bla @blaBla
		Feature: ApprovalUnit
		Some description is informative''')
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 0)
		Assertions.assertTrue(res.get(4) == 0)
		Assertions.assertTrue(res.get(5) == 1)
		Assertions.assertTrue(res.get(6) == 0)
	}

	@Test
	def void oneScenarioOnly() {
		val model = parser.parse(FeatureTitle.key + '\n' + oneScenWithOneGiven.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 6)
		Assertions.assertTrue(res.get(4) == 4)
		Assertions.assertTrue(res.get(5) == 6)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void oneScenarioResultingToInitState() {
		val model = parser.parse(FeatureTitle.key + '\n' + oneScenResultingToInitState.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 3)
		Assertions.assertTrue(res.get(5) == 5)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void twoScenariosResultingToInitState() {
		val model = parser.parse(FeatureTitle.key + '\n' + twoScensResultingToInitState.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 10)
		Assertions.assertTrue(res.get(4) == 9)
		Assertions.assertTrue(res.get(5) == 11)
		Assertions.assertTrue(res.get(6) == 2)
	}
	
	@Test
	def void featureWithOneScenarioWithoutDoNothing() {
		val model = parser.parse(FeatureTitle.key + '\n' + scenWithOneScenWithoutDoNothing.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 5)
		Assertions.assertTrue(res.get(5) == 6)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void featureWithOneScenarioWithDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenWithDoNothing.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 5)
		Assertions.assertTrue(res.get(5) == 6)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void featureWithOneScenarioAlsoWithDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenAlsoWithDoNothing.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 5)
		Assertions.assertTrue(res.get(5) == 6)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void featureWithOneScenarioWithTwoDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithOneScenWithTwoDoNothing.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 4)
		Assertions.assertTrue(res.get(5) == 5)
		Assertions.assertTrue(res.get(6) == 1)
	}
	
	@Test
	def void featureWithTwoScenariosWithoutDoNothing() {
		val model = parser.parse(FeatureTitle.key + scenWithTwoScenWithoutDoNothing.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 10)
		Assertions.assertTrue(res.get(4) == 9)
		Assertions.assertTrue(res.get(5) == 11)
		Assertions.assertTrue(res.get(6) == 2)
	}
	
	@Test
	def void featureWithOnlyAThenStep() {
		val model = parser.parse(FeatureTitle.key + scenWithOnlyAThenStep.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 4)
		Assertions.assertTrue(res.get(5) == 5)
		Assertions.assertTrue(res.get(6) == 1)
	}
	@Test
	def void featureWithOnlyAGivenStep() {
		val model = parser.parse(FeatureTitle.key + featureWithOnlyAGivenStep.key)
		
		val res = counting(model)
		Assertions.assertTrue(res.get(0) == 0)//TODO
		Assertions.assertTrue(res.get(1) == 0)//TODO
		Assertions.assertTrue(res.get(2) == 0)//TODO
		Assertions.assertTrue(res.get(3) == 5)
		Assertions.assertTrue(res.get(4) == 4)
		Assertions.assertTrue(res.get(5) == 6)
		Assertions.assertTrue(res.get(6) == 1)
	}
}