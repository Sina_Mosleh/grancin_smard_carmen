# grancin_SMARD_CARMEN
# welcome to my master thesis journey
# graphviz online : dreampuf.github.io/GraphvizOnline/#digraph%20G%20%7B%0D%0A%0D%0A%20%20subgraph%20cluster_1%20%7B%0D%0A%20%20%20%20style%3Dfilled%3B%0D%0A%20%20%20%20%2F%2Fcolor%3Dblue%3B%0D%0A%20%20%20%20node%20%5Bcolor%3Dblue%5D%3B%0D%0A%20%20%20%20%2F%2Fgiven_inmethod_reproduction_satisfied%20->%20given_when_attribute_inmethod_reproduction_satisfied%20%5Blabel%3D"wait(5ms)%20%26%26%20a%3D%3D0%20%26%26%20b%3D%3D0%20%26%26%20re_scen2%3D%3D1%20%26%26%20setEntry2%3D%3D1"%5D%3B%0D%0A%20%20%20%20given_inmethod_satisfied%20->%20when_attribute_satisfied%20%5Blabel%3D"when_condition%20%3D%3D%20true"%5D%3B%0D%0A%20%20%20%20when_attribute_satisfied%20->%20given_when_satisfied%20%5B%5D%3B%0D%0A%20%20%20%20%2F%2Fgiven_when_attribute_inmethod_reproduction_satisfied%20->%20given_inmethod_reproduction_satisfied%20%5Blabel%3D"a!%3D0%20%7C%7C%20b!%3D0"%5D%3B%0D%0A%20%20%20%20given_when_satisfied%20->%20error%20%5Blabel%3D"timeout(2*period)"%5D%3B%0D%0A%20%20%20%20%0D%0A%20%20%20%20label%20%3D%20"given_when_process"%3B%0D%0A%20%20%20%20%2F%2Fcolor%3Dblue%0D%0A%20%20%7D%0D%0A%20%20%0D%0A%20%20subgraph%20cluster_3%20%7B%0D%0A%20%20%20%20%2F%2Fstyle%3Dfilled%3B%0D%0A%20%20%20%20%2F%2Fcolor%20%3D%20gray%3B%0D%0A%20%20%20%20node%20%5Bstyle%3Dfilled%2Ccolor%3Dlightgray%5D%3B%0D%0A%20%20%20%20given_events->%0D%0A%20%20%20%20given_inmethod_cluster->%0D%0A%20%20%20%20when_attribute_cluster%3B%0D%0A%20%20%20%20when_events%20->%20given_inmethod_cluster%3B%0D%0A%20%20%20%20label%20%3D%20"clustering"%3B%0D%0A%20%20%7D%0D%0A%20%20%0D%0A%20%20%2F%2Fstart%20->%20init%3B%0D%0A%20%20%2F%2Ferror%20->%20end%3B%0D%0A%0D%0A%20%20%2F%2Fstart%20%5Bshape%3DMdiamond%5D%3B%0D%0A%20%20%2F%2Fend%20%5Bshape%3DMsquare%5D%3B%0D%0A%7D%0D%0A
# Spelling Checker installation instructions: https://tex.stackexchange.com/questions/235313/how-to-add-spell-checker-to-texworks-on-windows
# SQLite Viewer: https://inloop.github.io/sqlite-viewer/
#additional links:
	BNE description and translating parameters from logical to technical: http://www.eurecom.fr/en/publication/3088/download/rs-publi-3088.pdf
	V-Model: https://www.sciencedirect.com/topics/engineering/v-model
	Automated Driving: https://www.sciencedirect.com/science/article/pii/B9780128128008000084
	Life Cycle V-Model: http://www.informatik.uni-bremen.de/gdpa/vmodel/vm2.htm
	Serverless Architecture: https://martinfowler.com/articles/serverless.html
	Software Architecture: https://martinfowler.com/architecture/
	type of architecture: https://medium.com/@nvashanin/types-of-software-architects-aa03e359d192
	https://medium.com/@nvashanin/types-of-software-architects-aa03e359d192
	What Exactly Does a System Architect Do?: https://www.bmc.com/blogs/system-architect/
	Systems Designer Responsibilities and Duties: https://www.greatsampleresume.com/job-responsibilities/systems-designer-responsibilities/
	Security Policy in System-on-Chip Designs: https://link-springer-com.eaccess.ub.tum.de/book/10.1007%2F978-3-319-93464-8
	https://link-springer-com.eaccess.ub.tum.de/content/pdf/10.1007%2F978-3-319-93464-8.pdf
	Inteligent Vehicles: https://ebookcentral-proquest-com.eaccess.ub.tum.de/lib/munchentech/reader.action?docID=5043173
	Developing and Managing Embedded Systems and Products: https://proquest-tech-safaribooksonline-de.eaccess.ub.tum.de/book/electrical-engineering/computer-engineering/9780124058798/firstchapter#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODAxMjQwNTg3OTglMkZjaHAwMDh0aXRsX2h0bWwmcXVlcnk9
	REST API Design Rulebook: https://proquest-tech-safaribooksonline-de.eaccess.ub.tum.de/book/web-development/9781449317904/firstchapter#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODE0NDkzMTc5MDQlMkZzZWN0aW9uX2ludHJvZHVjdGlvbl9yZXN0X2FwaXNfaHRtbCZxdWVyeT0=
	Verification and validation in Scientific Computing: https://ebookcentral-proquest-com.eaccess.ub.tum.de/lib/munchentech/reader.action?docID=581060#
	Verification, Validation, and Testing of Engineered Systems: https://proquest-tech-safaribooksonline-de.eaccess.ub.tum.de/book/engineering/9781118029312/firstchapter?reader=pf&readerfullscreen=&readerleftmenu=1#X2ludGVybmFsX1BGVmlldz94bWxpZD05NzgxMTE4MDI5MzEyJTJGMzAmX19pbWFnZXBhZ2VyZXNvbHV0aW9uPTEwMDAmcXVlcnk9Vi0lMjBNb2RlbA==
	Service-Oriented Architecture: Concepts, Technology, and Design: https://proquest-tech-safaribooksonline-de.eaccess.ub.tum.de/book/software-engineering-and-development/soa/0131858580/firstchapter?reader=pf&readerfullscreen=&readerleftmenu=1#X2ludGVybmFsX1BGVmlldz94bWxpZD0wMTMxODU4NTgwJTJGNDAmX19pbWFnZXBhZ2VyZXNvbHV0aW9uPTEwMDAmcXVlcnk9Vi0lMjBNb2RlbA==
	A Practical Guide to SysML, 3rd Edition: https://proquest-tech-safaribooksonline-de.eaccess.ub.tum.de/book/software-engineering-and-development/sysml/9780128002025/firstchapter
	Systems engineering handbook a guide for system life cycle processes and activities: https://opac-ub-tum-de.eaccess.ub.tum.de/TouchPoint/perma.do?methodToCall=quickSearch&q=1035%3D%22BV043600438%22+IN+[2]&fbt=E39F0C13C8CDC559047817F520460614
	The Role of Product Architecture in the facturing Firm: https://repository.upenn.edu/cgi/viewcontent.cgi?article=1228&context=mgmt_papers
	Difference Between Use Case and Test Case: https://techdifferences.com/difference-between-use-case-and-test-case.html
	Different levels of an architecture: https://www.softwaretestinghelp.com/the-difference-between-unit-integration-and-functional-testing/
	From a service-oriented architecture (SOA) to a microservices one: https://www.polarising.com/2019/05/from-a-service-oriented-architecture-soa-to-a-microservices-one/
	Why Service-Oriented Architecture Is Important: https://medium.com/@SoftwareDevelopmentCommunity/what-is-service-oriented-architecture-fa894d11a7ec
	Microservices Architecture vs Service Oriented Architecture (SOA): https://koukia.ca/microservices-architecture-vs-service-oriented-architecture-soa-d016e3a309e6
	Service-oriented architecture (SOA): http://eccosys-int.com/services/web-services/website-design/79-blog/95-what-is-service-oriented-architecture-soa
	API vs. SOA? Are They Different?: https://www.akana.com/blog/api-vs-soa-are-they-different
	What is V-model- advantages, disadvantages and when to use it?: http://tryqa.com/what-is-v-model-advantages-disadvantages-and-when-to-use-it/
	Software Specification: https://www.sciencedirect.com/topics/computer-science/software-specification
	Environment Modeling-Based Requirements Engineering for Software Intensive Systems: https://www.sciencedirect.com/book/9780128019542/environment-modeling-based-requirements-engineering-for-software-intensive-systems#book-info
	system specification: https://www.reference.com/world-view/system-specification-395eba13f70c8e06
	Structure and behavior: https://user.eng.umd.edu/~austin/enes489p/lecture-slides/2012-MA-Behavior-and-Structure.pdf
	System Behavior: https://www.oreilly.com/library/view/applying-uml-and/0130925691/0130925691_ch09lev1sec3.html
	